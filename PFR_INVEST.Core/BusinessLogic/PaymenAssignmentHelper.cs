﻿using System;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.Core.BusinessLogic
{
    /// <summary>
    /// Вспомогательный класс для обработки назначений платежа
    /// </summary>
    public static  class PaymentAssignmentHelper
    {
        /// <summary>
        /// Заменяет тэги в назначении платежа, указанными данными
        /// </summary>
        /// <param name="payAss">Строка назначения платежа</param>
        /// <param name="agreeDate">Дата договора СИ/ВР</param>
        /// <param name="agreeNum">Номер договора СИ/ВР</param>
        /// <param name="year">Год кампании по приему заявлений</param>
        /// <param name="quartal">Квартал</param>
        public static string ReplaceTagDataForSIVR(string payAss, DateTime? agreeDate = null, string agreeNum = "", string year = "", string quartal = "")
        {
            return payAss.Replace(@"{ДАТА ДОГОВОРА}", agreeDate.HasValue ? agreeDate.Value.ToString("dd.MM.yyyy") : "").Replace("{НОМЕР ДОГОВОРА}", agreeNum).Replace("{ГОД}", year).Replace("{КВАРТАЛ}", quartal);
        }

        public static string ReplaceTagDataForNPF(string paText, DateTime? regDate)
        {
            var quartal = regDate.HasValue ? DateTools.GetRomeQuarterFromMonth(regDate.Value.Month) : "";
            return paText.Replace(@"{ГОД}", regDate.HasValue ? regDate.Value.Year.ToString() : "").Replace(@"{КВАРТАЛ}", quartal);
        }
    }
}
