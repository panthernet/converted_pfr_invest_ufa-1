﻿using System.Collections.Generic;

namespace PFR_INVEST.Core.BusinessLogic
{
    public class ThemeItem
    {
        public const string DEFAULT_THEME = "Office2007Blue";

        public static List<ThemeItem> ThemesList = new List<ThemeItem>
            {
                new ThemeItem("Office2007Blue", "Голубая"), 
                new ThemeItem("Office2007Silver", "Серебристая"),
                new ThemeItem("Office2007Black", "Черная"),
                new ThemeItem("DeepBlue", "Синяя"),
                new ThemeItem("Xmas",             "Новогодняя"),
                new ThemeItem("MetropolisDark",   "Метрополис темная"),
                new ThemeItem("MetropolisLight",  "Метрополис светлая"),
                new ThemeItem("Office2010Blue",   "Голубая 2010"),
                new ThemeItem("Office2010Silver", "Серебристая 2010"),
                new ThemeItem("Office2010Black",  "Черная 2010"),
               // new ThemeItem("TouchlineDark", "Тачлайн темная") - тема для тач скрина, крупные контролы, наша разметка форм не подходит
            };


        public string Name { get; set; }
        public string StringId { get; set; }

        public ThemeItem(string id, string name)
        {
            StringId = id;
            Name = name;
        }
    }
}
