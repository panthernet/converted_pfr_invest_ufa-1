﻿using System;
namespace PFR_INVEST.Core.BusinessLogic
{
    public interface ICBRConnector
    {
        decimal GetCourseRate(long pCurrencyID, DateTime pDate);
    }
}
