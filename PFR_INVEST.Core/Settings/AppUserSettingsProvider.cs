﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Xml;

namespace PFR_INVEST.Core.Settings
{
    /// <summary>
    /// Реализует сеттинг-провайдер с сохранением всех настроек в локальном XML-файле
    /// </summary>	
    public class AppUserSettingsProvider : SettingsProvider, IApplicationSettingsProvider
    {
        private const string SETTINGS_ROOT = "Settings";
        private string _applicationName;
        private string _companyName;
        private XmlDocument _settingsXml;
        private XmlDocument _commonSettingsXml;
        public static string User;
        public static string Domain;

        public virtual string GetAppSettingsFilename()
        {
            var path = $"{Domain}_{User}.settings";
            return path;
        }

        public virtual string GetAppSettingsPath()
        {
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            //стандартный SettingsProvider заменяет проблеы, так что мы тоже делаем
            var path = Path.Combine(folder, CompanyName.Replace(" ","_"));
            var dir = new DirectoryInfo(path);
            if (!dir.Exists)
                dir.Create();
            return path;
        }

        public virtual string GetCommonAppSettingsPath()
        {
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            //стандартный SettingsProvider заменяет проблеы, так что мы тоже делаем
            var path = Path.Combine(folder, CompanyName.Replace(" ", "_"));

            var dir = new DirectoryInfo(path);
            if (!dir.Exists)
                dir.Create();

            return path;
        }

        #region Overrided public methods

        public override string ApplicationName
        {
            get
            {
                if (string.IsNullOrEmpty(_applicationName))
                {
                    _applicationName = Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                    _applicationName = _applicationName.Replace("vshost.", string.Empty);
                }
                return _applicationName;
            }
            set { }
        }

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection props)
        {
            if (File.Exists(Path.Combine(GetCommonAppSettingsPath(), GetAppSettingsFilename())))
            {
                return GetValues(CommonSettingsXML, props);
            }
            else
            {
                return GetValues(SettingsXML, props);
            }
        }

        public override void Initialize(string name, NameValueCollection col)
        {
            base.Initialize(ApplicationName, col);
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection propvals)
        {
            foreach (var value in propvals)
            {
                SetValue(((SettingsPropertyValue)value));
            }
            CommonSettingsXML.Save(Path.Combine(GetCommonAppSettingsPath(), GetAppSettingsFilename()));
            SettingsXML.Save(Path.Combine(GetAppSettingsPath(), GetAppSettingsFilename()));
        }

        #endregion

        #region Private members

        private XmlDocument SettingsXML
        {
            get
            {
                return GetXmlSettingsFile(ref _settingsXml, GetAppSettingsPath(), GetAppSettingsFilename());
            }
        }

        private XmlDocument CommonSettingsXML
        {
            get
            {
                return GetXmlSettingsFile(ref _commonSettingsXml, GetCommonAppSettingsPath(), GetAppSettingsFilename());
            }
        }

        private string CompanyName
        {
            get
            {
                if (String.IsNullOrEmpty(_companyName))
                {
                    var assembly = Assembly.GetEntryAssembly();
                    if (assembly != null)
                    {
                        object[] customAttributes = assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                        if ((customAttributes != null) && (customAttributes.Length > 0))
                        {
                            _companyName = ((AssemblyCompanyAttribute)customAttributes[0]).Company;
                        }

                        if (string.IsNullOrEmpty(_companyName))
                        {
                            _companyName = string.Empty;
                        }
                    }
                    else
                        _companyName = string.Empty;
                }
                return _companyName;
            }
        }

        private static XmlDocument GetXmlSettingsFile(ref XmlDocument doc, string appSettingsPath, string appSettingsFileName)
        {
            if (doc == null)
            {
                //doc = new XmlDocument();
                try
                {
                    var path = Path.Combine(appSettingsPath, appSettingsFileName);
                    if (File.Exists(path))
                    {
                        doc = new XmlDocument();
                        doc.Load(path);
                    }
                }
                catch
                {
                    doc = null;
                }
                if (doc == null) 
                {
                    doc = new XmlDocument();
                    var dec = doc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
                    doc.AppendChild(dec);
                    var nodeRoot = doc.CreateNode(XmlNodeType.Element, SETTINGS_ROOT, string.Empty);
                    doc.AppendChild(nodeRoot);
                }
            }
            return doc;
        }

        private static string GetValue(SettingsProperty setting, XmlNode doc)
        {
            var rootNode = GetRootNode(doc);
            if (rootNode != null)
            {
                var node = rootNode.SelectSingleNode(setting.Name);
                if (node != null) return node.InnerText;
            }
            return setting.DefaultValue != null ? setting.DefaultValue.ToString() : string.Empty;
        }

        private static SettingsPropertyValueCollection GetValues(XmlNode doc, SettingsPropertyCollection props)
        {
            var values = new SettingsPropertyValueCollection();
            foreach (var setting in props)
            {
                var value = new SettingsPropertyValue(((SettingsProperty)setting))
                {
                    IsDirty = false,
                    SerializedValue = GetValue(((SettingsProperty)setting), doc)
                };
                values.Add(value);
            }
            return values;
        }

        private static XmlNode GetRootNode(XmlNode doc)
        {
            return doc.SelectSingleNode(SETTINGS_ROOT);
        }

        private void SetValue(SettingsPropertyValue propVal)
        {
            var rootNode = GetRootNode(SettingsXML);
            if (rootNode != null)
            {
                var node = rootNode.SelectSingleNode(propVal.Name);
                var cValue = propVal.SerializedValue == null ? "" : propVal.SerializedValue.ToString();
                if (node != null)
                {
                    node.InnerText = cValue;
                }
                else
                {
                    var settingNode = SettingsXML.CreateElement(propVal.Name);
                    settingNode.InnerText = cValue;
                    GetRootNode(SettingsXML).AppendChild(settingNode);

                    var commonRoot = GetRootNode(CommonSettingsXML);
                    if (commonRoot != null)
                    {
                        var cNode = commonRoot.SelectSingleNode(propVal.Name);
                        if (cNode != null)
                            cNode.InnerText = cValue;
                        else
                        {
                            var cNewNode = CommonSettingsXML.CreateElement(propVal.Name);
                            cNewNode.InnerText = cValue;
                            commonRoot.AppendChild(cNewNode);
                        }
                    }
                }
            }
        }

        #endregion

        #region IApplicationSettingsProvider Members

        public SettingsPropertyValue GetPreviousVersion(SettingsContext context, SettingsProperty property)
        {
            throw new NotSupportedException();
        }

        public void Reset(SettingsContext context)
        {
            var settingNode = GetRootNode(CommonSettingsXML);
            if (settingNode != null)
            {
                settingNode.RemoveAll();
                CommonSettingsXML.Save(Path.Combine(GetCommonAppSettingsPath(), GetAppSettingsFilename()));
            }
        }

        public void Upgrade(SettingsContext context, SettingsPropertyCollection properties)
        {
            var settingsRootNode = GetRootNode(SettingsXML);
            if (settingsRootNode != null)
            {
                var upgradeProperties = new SettingsPropertyCollection();
                var rootNode = GetRootNode(CommonSettingsXML);
                if (rootNode == null)
                    upgradeProperties = properties;
                else
                    foreach (var sp in properties)
                    {
                        var setting = settingsRootNode.SelectSingleNode(((SettingsProperty)sp).Name);
                        var commonSetting = rootNode.SelectSingleNode(((SettingsProperty)sp).Name);
                        var upgradeAttribute = (setting != null && setting.Attributes.Count == 1) ? setting.Attributes[0] : null;
                        if (commonSetting == null ||
                                                    (setting != null &&
                                                    upgradeAttribute != null &&
                                                    upgradeAttribute.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase) &&
                                                    setting.InnerText != commonSetting.InnerText))
                            upgradeProperties.Add(((SettingsProperty)sp));
                    }
                if (upgradeProperties.Count > 0)
                {
                    var values = GetValues(SettingsXML, upgradeProperties);
                    SetPropertyValues(context, values);
                }
            }
        }

        #endregion
    }
}
