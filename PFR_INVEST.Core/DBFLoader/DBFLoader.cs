﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Text.RegularExpressions;
using PFR_INVEST.DataObjects;
using System.Configuration;
using dBASE.NET;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace PFR_INVEST.Core.DBFLoader
{
    public class DBFLoader2
    {
        private readonly Regex m_BDateRegex = new Regex(@"\bb\d{6}.dbf\b", RegexOptions.IgnoreCase);
        private readonly Regex m_TDateRegex = new Regex(@"\bt\d{6}.dbf\b", RegexOptions.IgnoreCase);
        private readonly Regex m_InDateRegex = new Regex(@"\bin\d{6}.dbf\b", RegexOptions.IgnoreCase);

        private const string m_BDatePattern = "b??????.dbf";
        private const string m_TDatePattern = "t??????.dbf";
        private const string m_InDatePattern = "in??????.dbf";
        
        private const string m_ZipPattern = "*.zip";
        private readonly string m_Folder;
        private readonly string m_File;
        
        public DBFLoader2(string p_folder): this(p_folder, string.Empty)
        {

        }

        public DBFLoader2(string p_folder, string p_file)
        {
            if (!Directory.Exists(p_folder))
                throw new DirectoryNotFoundException($"Папка '{p_folder}' не существует!");

            m_Folder = p_folder;
            m_File = p_file;
        }

        public List<ILoadedFromDBF> LoadOwnCapitalFromDBF(DateTime reportDate, string key)
        {
            if (string.IsNullOrEmpty(m_Folder) || string.IsNullOrEmpty(m_File)) return null;
            
            string filePath = Path.Combine(m_Folder, m_File);
            if (!File.Exists(filePath)) return null;

            var result = new List<ILoadedFromDBF>();
            try
            {
                result.AddRange(LoadOwnCapitalData(filePath, reportDate, key).Cast<ILoadedFromDBF>());
            }
            catch
            {
                return null;
            }
            return result;
        }

        private List<OwnCapitalHistory> LoadOwnCapitalData(string fileName, DateTime reportDate, string key)
        {
            var records = new List<OwnCapitalHistory>();
            var importDate = DateTime.Now;

            var dbf = new Dbf();
            dbf.Read(fileName);

            foreach (var rec in dbf.Records.Where(a => (string)a[1] == "000"))
            {
                var num = rec[0] == null ? null : (int?) int.Parse(rec[0].ToString());
                var summ = rec[2] == null ? 0 : (decimal?) decimal.Parse(rec[2].ToString());
                summ *= 1000;

                var r = new OwnCapitalHistory
                {
                    LegalEntityID = 0,
                    Key = key,
                    LicenseNumber = num.HasValue ? num.Value.ToString() : string.Empty,
                    Summ = summ.Value,
                    ImportDate = importDate,
                    ReportDate = reportDate,
                    IsImported = 1
                };

                records.Add(r);
            }

            /*

            //string query = String.Format("SELECT * FROM {0};", fileName);
            string query = String.Format("SELECT * FROM [{0}] WHERE C1 = '000';", Path.GetFileName(fileName));
            using (var command = new OleDbCommand(query, m_Connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int? num = ReadInt32Value(reader, 0);
                        var summ = ReadDecimalValue(reader, 2);
                        if(summ == null)
                            summ = 0;
                        else
                            summ = summ * 1000;

                        var rec = new OwnCapitalHistory
                        {
                            LegalEntityID = 0,
                            Key = key,
                            LicenseNumber = num != null && num.HasValue ? num.Value.ToString() : string.Empty,
                            Summ = summ.Value,
                            ImportDate = importDate,
                            ReportDate = reportDate,
                            IsImported = 1
                        };

                        records.Add(rec);
                    }
                }
            }
            */
            return records;
        }

    }

	public class DBFLoader : IDisposable
	{
        const string DEFAULT_OLEDB_PROVIDER = "Microsoft.Jet.OLEDB.4.0;";
		private readonly string m_Folder;
        private readonly string m_File;
		private OleDbConnection m_Connection;

		private readonly Regex m_BDateRegex = new Regex(@"\bb\d{6}.dbf\b", RegexOptions.IgnoreCase);
		private readonly Regex m_TDateRegex = new Regex(@"\bt\d{6}.dbf\b", RegexOptions.IgnoreCase);
		private readonly Regex m_InDateRegex = new Regex(@"\bin\d{6}.dbf\b", RegexOptions.IgnoreCase);

        private const string m_BDatePattern = "b??????.dbf";
        private const string m_TDatePattern = "t??????.dbf";
        private const string m_InDatePattern = "in??????.dbf";
        
        private const string m_ZipPattern = "*.zip";

        public DBFLoader(string p_folder): this(p_folder, string.Empty)        {

        }

        public DBFLoader(string p_folder, string p_file)
		{
            if (!Directory.Exists(p_folder))
                throw new DirectoryNotFoundException(string.Format("Папка '{0}' не существует.", m_Folder));

            m_Folder = p_folder;
            m_File = p_file;

            OpenConnection();
		}

        private void OpenConnection()
        {
            string provider = ConfigurationManager.AppSettings.Get("OLEDBProvider");
            if (!string.IsNullOrEmpty(provider))
            {
                try
                {
                    //m_Connection = new OleDbConnection(String.Format("Provider={0};Data Source=\"{1}\";Extended Properties=dBASE IV;User ID=Admin;Password=", provider, p_folder));
                    m_Connection = new OleDbConnection(String.Format("Provider={0};Data Source=\"{1}\";Extended Properties=dBASE IV;User ID=Admin;", provider, m_Folder));
                    m_Connection.Open();

                    return;
                }
                catch (Exception ex)
                {
                    if (string.Equals(provider, DEFAULT_OLEDB_PROVIDER, StringComparison.InvariantCultureIgnoreCase))
                        throw ex;
                }
            }

            m_Connection = new OleDbConnection(String.Format("Provider={0};Data Source=\"{1}\";Extended Properties=dBASE IV;User ID=Admin;", DEFAULT_OLEDB_PROVIDER, m_Folder));
            m_Connection.Open();
        }

		public enum LoadDBFResult : int
		{
			LoadDBFResultOK = 0,
			LoadDBFResultNoZips = 1
		}

        public List<ILoadedFromDBF> LoadOwnCapitalFromDBF(DateTime reportDate, string key)
        {
            if (string.IsNullOrEmpty(m_Folder) || string.IsNullOrEmpty(m_File)) return null;
            
            string filePath = Path.Combine(m_Folder, m_File);
            if (!File.Exists(filePath)) return null;

            var result = new List<ILoadedFromDBF>();
            try
            {
                result.AddRange(LoadOwnCapitalData(filePath, reportDate, key).Cast<ILoadedFromDBF>());
            }
            catch
            {
                return null;
            }
            return result;
        }

		public List<ILoadedFromDBF> LoadDBF()
		{
            var dirInfo = new DirectoryInfo(m_Folder);

            var result = new List<ILoadedFromDBF>();
            foreach (var zip in dirInfo.GetFiles(m_ZipPattern))
			{
				try
				{
					result.AddRange(LoadDBFFromZip(zip.FullName));
					File.Delete(zip.FullName);
				}
				catch
				{
					return null;
				}
			}

            foreach (var bDateFile in dirInfo.GetFiles(m_BDatePattern))
			{
				try
				{
					result.AddRange(LoadBDate(bDateFile.FullName).Cast<ILoadedFromDBF>());
					File.Delete(bDateFile.FullName);
				}
				catch
				{
					return null;
				}
			}

            foreach (var tDateFile in dirInfo.GetFiles(m_TDatePattern))
			{
				try
				{
					result.AddRange(LoadTDate(tDateFile.FullName).Cast<ILoadedFromDBF>());
					File.Delete(tDateFile.FullName);
				}
				catch
				{
					return null;
				}
			}

            foreach (var inDateFile in dirInfo.GetFiles(m_InDatePattern))
			{
				try
				{
					result.AddRange(LoadInDate(inDateFile.FullName).Cast<ILoadedFromDBF>());
					File.Delete(inDateFile.FullName);
				}
				catch
				{
					return null;
				}
			}

			return result;
		}

		private string ExtractFile(ZipFile zf, ZipEntry zipEntry)
		{
			byte[] buffer = new byte[4096];
            var zipStream = zf.GetInputStream(zipEntry);
			String extractedFilePath = Path.Combine(m_Folder, zipEntry.Name);
            using (var streamWriter = File.Create(extractedFilePath))
			{
				StreamUtils.Copy(zipStream, streamWriter, buffer);
			}
			return extractedFilePath;
		}

		private List<ILoadedFromDBF> LoadDBFFromZip(string zipName)
		{
            var result = new List<ILoadedFromDBF>();

            using (var zf = new ZipFile(File.OpenRead(zipName)))
			{
                foreach (var zipEntry in zf)
				{
					if (!((ZipEntry)zipEntry).IsFile)
					{
						continue;
					}

					string entryFileName = ((ZipEntry)zipEntry).Name;

					if (m_BDateRegex.IsMatch(entryFileName))
					{
						string extracted = ExtractFile(zf, ((ZipEntry)zipEntry));
						result.AddRange(LoadBDate(extracted).Cast<ILoadedFromDBF>());
						File.Delete(extracted);
					}

					if (m_TDateRegex.IsMatch(entryFileName))
					{
						string extracted = ExtractFile(zf, ((ZipEntry)zipEntry));
						result.AddRange(LoadTDate(extracted).Cast<ILoadedFromDBF>());
						File.Delete(extracted);
					}

					if (m_InDateRegex.IsMatch(entryFileName))
					{
						string extracted = ExtractFile(zf, ((ZipEntry)zipEntry));
						result.AddRange(LoadInDate(extracted).Cast<ILoadedFromDBF>());
						File.Delete(extracted);
					}
				}
			}

			return result;
		}

		#region Cell Readers
		private static string ReadStringValue(OleDbDataReader reader, int ordinal)
		{
			string result;
			try
			{
				result = reader.IsDBNull(ordinal) ? null : reader.GetString(ordinal);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		protected static int? ReadInt32Value(OleDbDataReader reader, int ordinal)
		{
			int? result;
			try
			{
				result = reader.IsDBNull(ordinal) ? (int?)null : (int)reader.GetDouble(ordinal);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		private static decimal? ReadDecimalValue(OleDbDataReader reader, int ordinal)
		{
			decimal? result;
			try
			{
				result = reader.IsDBNull(ordinal) ? (decimal?)null : (decimal)reader.GetDouble(ordinal);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		private static decimal? ReadDecimalValue(OleDbDataReader reader, string column)
		{
			decimal? result;
			try
			{
				int ordinal = reader.GetOrdinal(column);
				result = reader.IsDBNull(ordinal) ? (decimal?)null : (decimal)reader.GetDouble(ordinal);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		private static DateTime? ReadDateTimeValue(OleDbDataReader reader, int ordinal)
		{
			DateTime? result;
			try
			{
				result = reader.IsDBNull(ordinal) ? (DateTime?)null : reader.GetDateTime(ordinal);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		private static TimeSpan? ReadTimeSpanValue(OleDbDataReader reader, int ordinal)
		{
			TimeSpan? result;
			try
			{
				if (reader.IsDBNull(ordinal))
					result = (TimeSpan?)null;
				else
				{
					int d = (int)reader.GetDouble(ordinal);
					int h = d / 10000;
					int m = d % 10000 / 100;
					int s = d % 100;
					result = new TimeSpan(h, m, s);
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
		#endregion

        private List<OwnCapitalHistory> LoadOwnCapitalData(string fileName, DateTime reportDate, string key)
        {
            var records = new List<OwnCapitalHistory>();
            DateTime importDate = DateTime.Now;

            //string query = String.Format("SELECT * FROM {0};", fileName);
            string query = String.Format("SELECT * FROM [{0}] WHERE C1 = '000';", Path.GetFileName(fileName));
            using (var command = new OleDbCommand(query, m_Connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int? num = ReadInt32Value(reader, 0);
                        var summ = ReadDecimalValue(reader, 2);
                        if(summ == null)
                            summ = 0;
                        else
                            summ = summ * 1000;

                        var rec = new OwnCapitalHistory
                        {
                            LegalEntityID = 0,
                            Key = key,
                            LicenseNumber = num != null && num.HasValue ? num.Value.ToString() : string.Empty,
                            Summ = summ.Value,
                            ImportDate = importDate,
                            ReportDate = reportDate,
                            IsImported = 1
                        };

                        records.Add(rec);
                    }
                }
            }

            return records;
        }

		private List<BDate> LoadBDate(string fileName)
		{
            var bDates = new List<BDate>();

			//string query = String.Format("SELECT * FROM {0};", fileName);
			string query = String.Format("SELECT * FROM [{0}];", Path.GetFileName(fileName));
            using (var command = new OleDbCommand(query, m_Connection))
			{
                using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
                        var BDate = new BDate
                                                {
                                                    TradeDate = ReadDateTimeValue(reader, 0),
                                                    BoardIdText = ReadStringValue(reader, 1),
                                                    SecurityIdText = ReadStringValue(reader, 2),
                                                    Duration = ReadInt32Value(reader, 5),
                                                    Open = ReadDecimalValue(reader, 6),
                                                    Low = ReadDecimalValue(reader, 7),
                                                    High = ReadDecimalValue(reader, 8),
                                                    Close = ReadDecimalValue(reader, 9),
                                                    WAPrice = ReadDecimalValue(reader, 10),
                                                    TrendClose = ReadDecimalValue(reader, 11),
                                                    TrendWAP = ReadDecimalValue(reader, 12),
                                                    HighBID = ReadDecimalValue(reader, 13),
                                                    LowOffer = ReadDecimalValue(reader, 14),
                                                    Offer = ReadDecimalValue(reader, 16),
                                                    YieldClose = ReadDecimalValue(reader, 17),
                                                    YieldAtWAP = ReadDecimalValue(reader, 18),
                                                    Volume = ReadDecimalValue(reader, 19),
                                                    Value = ReadDecimalValue(reader, 20),
                                                    NumberOfTrades = ReadDecimalValue(reader, 22),
                                                    MarketPrice1 = ReadDecimalValue(reader, 23),
                                                    MarketPrice2 = ReadDecimalValue(reader, 24),
                                                    BID = ReadDecimalValue(reader, "BID")
                                                };
						bDates.Add(BDate);
					}
				}
			}

			return bDates;
		}

		private List<TDate> LoadTDate(string fileName)
		{
            var tDates = new List<TDate>();

			//string query = String.Format("SELECT * FROM {0};", fileName);
			string query = String.Format("SELECT * FROM [{0}];", Path.GetFileName(fileName));
            using (var command = new OleDbCommand(query, m_Connection))
			{
                using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
                        var BDate = new TDate
                                                {
                                                    TradeDate = ReadDateTimeValue(reader, 0),
                                                    Time = ReadTimeSpanValue(reader, 1),
                                                    SecurityID = ReadStringValue(reader, 2),
                                                    Name = ReadStringValue(reader, 3),
                                                    Price = ReadDecimalValue(reader, 4),
                                                    TrendPrice = ReadDecimalValue(reader, 5),
                                                    Yield = ReadDecimalValue(reader, 6),
                                                    Value = ReadDecimalValue(reader, 7),
                                                    Quantity = ReadDecimalValue(reader, 8),
                                                    WAPrice = ReadDecimalValue(reader, 9),
                                                    TrendWAP = ReadDecimalValue(reader, 10),
                                                    TotalValue = ReadDecimalValue(reader, 11),
                                                    TotalVolume = ReadDecimalValue(reader, 12),
                                                    AccInt = ReadDecimalValue(reader, 13),
                                                    SecNumberOfTrades = ReadDecimalValue(reader, 14),
                                                    NumberOfTrades = ReadDecimalValue(reader, 15)
                                                };
						tDates.Add(BDate);
					}
				}
			}

			return tDates;
		}

		private List<INDate> LoadInDate(string fileName)
		{
            var inDates = new List<INDate>();

			string query = String.Format("SELECT * FROM [{0}] WHERE LCASE(TRIM(SECURITYID)) = 'micex10index';", Path.GetFileName(fileName));
            using (var command = new OleDbCommand(query, m_Connection))
			{
                using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
                        var BDate = new INDate
                                                {
                                                    SecurityID = ReadStringValue(reader, 0),
                                                    TradeDate = ReadDateTimeValue(reader, 1),
                                                    Open = ReadDecimalValue(reader, 2),
                                                    High = ReadDecimalValue(reader, 3),
                                                    Low = ReadDecimalValue(reader, 4),
                                                    Close = ReadDecimalValue(reader, 5),
                                                    TrendClose = ReadDecimalValue(reader, 6),
                                                    Value = ReadDecimalValue(reader, 7),
                                                    ShortName = ReadStringValue(reader, 8),
                                                    EnglishName = ReadStringValue(reader, 9)
                                                };
						inDates.Add(BDate);
					}
				}
			}

			return inDates;
		}

		public void Dispose()
		{
			m_Connection.Close();
		}
	}
}
