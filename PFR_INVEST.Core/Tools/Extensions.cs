﻿using System;
using System.Linq;
using System.Reflection;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Core.Tools
{
    public static class Extensions
    {
        /// <summary>
        /// Возвращает распакованное содержимое файла не изменяя изначальные данные
        /// </summary>
        /// <param name="file">Файл</param>
        public static byte[] GetUncompressed(this ImportFileListItem file)
        {
            if (file.FileContent == null) return null;
            return file.IsCompressed ? GZipCompressor.Decompress(file.FileContent) : file.FileContent;
        }

        public static void TrimAllStringProperties(this object o)
        {
            var props = o.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(a => a.CanWrite && a.CanRead && a.PropertyType == typeof(string));
            foreach (var p in props)
            {
                var mget = p.GetGetMethod(false);
                var mset = p.GetSetMethod(false);
                // Get and set methods have to be public
                if (mget == null) { continue; }
                if (mset == null) { continue; }

                var value = (string) p.GetValue(o, null);
                if (!string.IsNullOrEmpty(value))
                {
                    p.SetValue(o, value.Trim(), null);
                }
            }
        }

        /// <summary>
        /// Возвращает содержимое файла в запакованном виде не изменяя изначальные данные
        /// </summary>
        /// <param name="file">Файл</param>
        public static byte[] GetCompressed(this ImportFileListItem file)
        {
            if (file.IsCompressed || file.FileContent == null) return file.FileContent;
            return GZipCompressor.Compress(file.FileContent);
        }

        /// <summary>
        /// Распаковывает содержимое файла, меняя изначальные данные
        /// </summary>
        /// <param name="file">Файл</param>
        public static void Decompress(this ImportFileListItem file)
        {
            if (!file.IsCompressed || file.FileContent == null) return;
            file.FileContent = GZipCompressor.Decompress(file.FileContent);
            file.IsCompressed = false;
        }

        /// <summary>
        /// Запаковывает содержимое файла, меняя изначальные данные
        /// </summary>
        /// <param name="file">Файл</param>
        public static void Compress(this ImportFileListItem file)
        {
            if (file.IsCompressed || file.FileContent == null) return;
            file.FileContent = GZipCompressor.Compress(file.FileContent);
            file.IsCompressed = true;
        }

        public static byte[] ToByteArray(this string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string ToStringFromBytes(this byte[] bytes)
        {
            var chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public static decimal TruncateDecimalPlaces(this decimal value, int places)
        {
            return Math.Floor(value*(decimal) Math.Pow(10, places))/(decimal) Math.Pow(10, places);
        }

    }
}
