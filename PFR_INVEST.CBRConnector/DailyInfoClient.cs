﻿
#if !WEBCLIENT
using PFR_INVEST.CBRConnector.CBRDailyInfoWebService;
#else
using PFR_INVEST.Web2.CBRConnector.ru.cbr.www;
#endif

namespace PFR_INVEST.CBRConnector
{
    internal static class DailyInfoClient
    {
        private static DailyInfo m_Client;
        public static DailyInfo Client
        {
            get
            {
                if (m_Client == null)
                    m_Client = new DailyInfo();
                return m_Client;
            }
        }
    }
}
