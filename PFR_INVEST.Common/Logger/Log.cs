﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.ServiceModel;
using System.Threading;

namespace PFR_INVEST.Common.Logger
{
    public class Log: IDisposable
    {
        private FileStream _stream;
        private StreamWriter _writer;
        private static readonly object Locker = new object();

        public Log(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            lock (Locker)
            {
                try
                {
                    var directory = Path.GetDirectoryName(path);
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var sourceFileName = GetValidPath(directory, Path.GetFileName(path));
                    var fileName = sourceFileName;
                    var started = false;
                    int maxRetries = 15;
                    int retriesCount = 0;
                    while (!started)
                    {
                        try
                        {
                            _stream = new FileStream(fileName, FileMode.Append);
                            started = true;
                        }
                        catch (IOException ex)
                        {
                            retriesCount++;
                            var ext = Path.GetExtension(fileName);
                            fileName = Path.Combine(directory, $"{Path.GetFileNameWithoutExtension(sourceFileName)}_{retriesCount}{ext}");
                            if (retriesCount == maxRetries)
                                throw;
                        }
                    }
                    _writer = new StreamWriter(_stream);
                    InitEventLog();
                }
                catch
                {
                    Dispose();
                }
            }
        }

        public static string GetValidPath(string path, string file)
        {
            Path.GetInvalidPathChars().ToList().ForEach(a =>
            {
                if(a == '\\' || a == '/') return;
                path = path.Replace(a, 'X');
            });
            Path.GetInvalidFileNameChars().ToList().ForEach(a => file = file.Replace(a, 'X'));
            return Path.Combine(path, file);
        }

        private EventLog _mEventLog;
        private void InitEventLog()
        {
			try
			{
				string logSource = "ПТК ДОКИП";

				if (!EventLog.SourceExists(logSource))
				{
					//Нужны права админа!!! Возможно надо сделать сетап 
					//с правами админа и custom action, который будет создавать EventSource
					//при инсталляции
					try
					{
						EventLog.CreateEventSource(logSource, "Application");
					}
					catch (Exception ex)
					{
						WriteException(ex);
					}
				}

				_mEventLog = new EventLog();
				_mEventLog.BeginInit();
				_mEventLog.EndInit();
				_mEventLog.Source = logSource;
			}
			catch (SecurityException) 
			{
				//Прекращаем спам ошибок, для случая ,когда нет прав для проверки EventLog.SourceExists
			}
			catch (Exception ex)
			{
				WriteException(ex);
			}
        }

        public void WriteSystemEventLog(string pSText, EventLogEntryType pT)
        {
            try
            {
                _mEventLog.WriteEntry(pSText, pT);
            }
            catch (Exception ex) {
                WriteLine(pSText);
                WriteException(ex);            
            }
        }

        private readonly Semaphore _semaphore = new Semaphore(1,1);

        public void WriteException(Exception e, string text = "")
        {
            if (e == null || _writer == null)
                return;

            _semaphore.WaitOne();
            try
            {
                _writer.Write("{0} {1} EXCEPTION {2} {3}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), text,
                    Environment.NewLine);
                _writer.Write(e.ToString());
                _writer.Write(Environment.NewLine);
                if (e is FaultException<ExceptionDetail>)
                {
                    var fe = (e as FaultException<ExceptionDetail>).Detail;
                    while (fe.InnerException != null)
                    {
                        fe = fe.InnerException;
                    }
                    _writer.Write((string) fe.ToString());
                    _writer.Write(Environment.NewLine);
                    _writer.Write((string) fe.StackTrace);
                    _writer.Write(Environment.NewLine);
                }
                else
                {
                    var ie = e;
                    while (ie.InnerException != null)
                    {
                        ie = ie.InnerException;
                    }
                    _writer.Write(ie == null ? e.StackTrace : ie.StackTrace);
                    _writer.Write(Environment.NewLine);
                }
                _writer.Flush();                
            }
            catch
            {
                // ignored
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public void WriteLine(string msg)
        {
            if (_writer == null) return;
            _semaphore.WaitOne();
            try
            {
                _writer.WriteLine("{0}: {1}", DateTime.Now, msg);
                _writer.Flush();
            }
            catch
            {
                // ignored
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public void Dispose()
        {
            try
            {
                _writer?.Dispose();
                _stream?.Dispose();
                _stream = null;
                _writer = null;
            }
            catch
            {
                // ignored
            }
        }
    }
}
