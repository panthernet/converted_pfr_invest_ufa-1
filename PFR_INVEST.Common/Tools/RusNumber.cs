//
// ������ ��� �������������� ����� � �������� ���� � �������.
// �������� - http://morpher.ru/SummaPropisyu.aspx
//
// ������ ���������� ������������� ���, ��� � ��� ����� ��� 
// �������� �������� �����, �� �������� ����� StringBuilder.
// �������� � 1,5 ���� ������� ����� �������� �� C#.
//
// �������:
// http://www.gotdotnet.ru/Downloads/Examples/772.aspx (C#, �������)
// http://rsdn.ru/article/files/dotnet/RusNumber.xml (C#)
// http://sources.ru/builder/faq/031.html (C++)
// http://delphiworld.narod.ru/base/sum_written_out9.html (Pascal)
//
// ����� ������� ���������� ��� ���� ���� decimal � double.
// ��� ��������������� ����� ���� decimal � �� ����� ��� ����� �������
// ����� ������� ����������� � ���� �������.
// ��� ���� ���� double ������������ ������������ ����� - �����.MaxDouble.
//

using System;
using System.Text;

namespace PFR_INVEST.Common.Tools
{
    /// <summary>
    /// ����� ��� ������ �������� ���� ��������: "������ ������ 00 ������".
    /// </summary>
    /// <example>
    /// �����.������� (100, ������.�����); // "��� ������ 00 ������"
    /// ������.�����.������� (123.45); // "��� �������� ��� ����� 45 ������"
    /// </example>
    public static class �����
    {
        /// <summary>
        /// ���������� ������� ����� � �������� ������ � <paramref name="result"/> ��������� �������.
        /// </summary>
        public static StringBuilder �������(decimal �����, ������ ������, StringBuilder result)
        {
            decimal ����� = Math.Floor(�����);
            uint ������� = (uint)((����� - �����) * 100);

            �����.�������(�����, ������.���������������, result);
            return ���������������(�������, ������, result);
        }

        /// <summary>
        /// ���������� ������� ����� � �������� ������ � <paramref name="result"/> ��������� �������.
        /// </summary>
        public static StringBuilder �������(double �����, ������ ������, StringBuilder result)
        {
            double ����� = Math.Floor(�����);

            // ��������� 100 �� ������ ��������� �������� ������ ����������
            // ��������, ����� ����� = 1234.51.
            uint ������� = (uint)(����� * 100) - (uint)(����� * 100);

            �����.�������(�����, ������.���������������, result);
            return ���������������(�������, ������, result);
        }

        private static StringBuilder ���������������(uint �������, ������ ������, StringBuilder result)
        {
            result.Append(' ');

            // ��� ������� ����������� �������, ��� ��������� �� ��� ������������������.
            result.Append(�������.ToString("00"));
            //result.AppendFormat ("{0:00}", �������);

            result.Append(' ');
            result.Append(�����.�����������(������.��������������, �������));

            return result;
        }

        /// <summary>
        /// ���������, �������� �� ����� ��� �������� ������ 
        /// <see cref="�����.������� (decimal, ������)"/>.
        /// </summary>
        /// <remarks>
        /// ����� ������ ���� ��������������� � ������ ��������� 
        /// �� ����� ���� ���� ����� �������.
        /// </remarks>
        /// <returns>
        /// �������� ����������� ����������� ��� null.
        /// </returns>
        public static string ��������������(decimal �����)
        {
            if (����� < 0) return "����� ������ ���� ���������������.";

            decimal ����� = Math.Floor(�����);
            decimal ������� = (����� - �����) * 100;

            if (Math.Floor(�������) != �������)
            {
                return "����� ������ ��������� �� ����� ���� ���� ����� �������.";
            }

            return null;
        }

        /// <summary>
        /// ���������� ������� �������� ����� ��������� �������.
        /// </summary>
        public static string �������(decimal n, ������ ������)
        {
            return �����.ApplyCaps(�������(n, ������, new StringBuilder()), ���������.���);
        }

        /// <summary>
        /// ���������� ������� �������� �����.
        /// </summary>
        public static string �������(decimal n, ������ ������, ��������� ���������)
        {
            return �����.ApplyCaps(�������(n, ������, new StringBuilder()), ���������);
        }

        /// <summary>
        /// ���������� ������� �������� ����� ��������� �������.
        /// </summary>
        public static string �������(double n, ������ ������)
        {
            return �����.ApplyCaps(�������(n, ������, new StringBuilder()), ���������.���);
        }

        /// <summary>
        /// ���������� ������� �������� �����.
        /// </summary>
        public static string �������(double n, ������ ������, ��������� ���������)
        {
            return �����.ApplyCaps(�������(n, ������, new StringBuilder()), ���������);
        }
    }

    /// <summary>
    /// ����� ��� �������������� ����� � ������� �� ������� �����.
    /// </summary>
    /// <example>
    /// �����.������� (1, ��������.�������); // "����"
    /// �����.������� (2, ��������.�������); // "���"
    /// �����.������� (21, ��������.�������); // "�������� ����"
    /// </example>
    /// <example>
    /// �����.������� (5, new ���������������� (
    ///  ��������.�������, "����", "�����", "������"), sb); // "���� ������"
    /// </example>
    public static class �����
    {
        /// <summary>
        /// �������� ������� ����� � ������������� �������� ���������.
        /// </summary>
        /// <param name="�����"> ����� ������ ���� �����, ���������������. </param>
        /// <param name="��"></param>
        /// <param name="result"> ���� ������������ ���������. </param>
        /// <returns> <paramref name="result"/> </returns>
        /// <exception cref="ArgumentException">
        /// ���� ����� ������ ���� ��� �� �����. 
        /// </exception>
        public static StringBuilder �������(decimal �����, I���������������� ��, StringBuilder result)
        {
            string error = ��������������(�����);
            if (error != null) throw new ArgumentException(error, "�����");

            // ������������� ������ �������� � ���� �������, ��� decimal.
            if (����� <= uint.MaxValue)
            {
                �������((uint)�����, ��, result);
            }
            else if (����� <= ulong.MaxValue)
            {
                �������((ulong)�����, ��, result);
            }
            else
            {
                MyStringBuilder mySb = new MyStringBuilder(result);

                decimal div1000 = Math.Floor(����� / 1000);
                ���������������������(div1000, 0, mySb);
                �������������((uint)(����� - div1000 * 1000), ��, mySb);
            }

            return result;
        }

        /// <summary>
        /// �������� ������� ����� � ������������� �������� ���������.
        /// </summary>
        /// <param name="�����"> 
        /// ����� ������ ���� �����, ���������������, �� ������� <see cref="MaxDouble"/>. 
        /// </param>
        /// <param name="��"></param>
        /// <param name="result"> ���� ������������ ���������. </param>
        /// <exception cref="ArgumentException">
        /// ���� ����� ������ ����, �� ����� ��� ������ <see cref="MaxDouble"/>. 
        /// </exception>
        /// <returns> <paramref name="result"/> </returns>
        /// <remarks>
        /// float �� ��������� ������������� � double, ������� ��� ���������� ��� float.
        /// � ���������� ������ ���������� �������� ����������� ���� ������� �
        /// ������, ���������� double.ToString ("R"), ������� � 17 �������� �����.
        /// </remarks>
        public static StringBuilder �������(double �����, I���������������� ��, StringBuilder result)
        {
            string error = ��������������(�����);
            if (error != null) throw new ArgumentException(error, "�����");

            if (����� <= uint.MaxValue)
            {
                �������((uint)�����, ��, result);
            }
            else if (����� <= ulong.MaxValue)
            {
                // ������� � ulong ����������� � ������� � 2 ���� �������.
                �������((ulong)�����, ��, result);
            }
            else
            {
                MyStringBuilder mySb = new MyStringBuilder(result);

                double div1000 = Math.Floor(����� / 1000);
                ���������������������(div1000, 0, mySb);
                �������������((uint)(����� - div1000 * 1000), ��, mySb);
            }

            return result;
        }

        /// <summary>
        /// �������� ������� ����� � ������������� �������� ���������.
        /// </summary>
        /// <returns> <paramref name="result"/> </returns>
        public static StringBuilder �������(ulong �����, I���������������� ��, StringBuilder result)
        {
            if (����� <= uint.MaxValue)
            {
                �������((uint)�����, ��, result);
            }
            else
            {
                MyStringBuilder mySb = new MyStringBuilder(result);

                ulong div1000 = ����� / 1000;
                ���������������������(div1000, 0, mySb);
                �������������((uint)(����� - div1000 * 1000), ��, mySb);
            }

            return result;
        }

        /// <summary>
        /// �������� ������� ����� � ������������� �������� ���������.
        /// </summary>
        /// <returns> <paramref name="result"/> </returns>
        public static StringBuilder �������(uint �����, I���������������� ��, StringBuilder result)
        {
            MyStringBuilder mySb = new MyStringBuilder(result);

            if (����� == 0)
            {
                mySb.Append("����");
                mySb.Append(��.�������);
            }
            else
            {
                uint div1000 = ����� / 1000;
                ���������������������(div1000, 0, mySb);
                �������������(����� - div1000 * 1000, ��, mySb);
            }

            return result;
        }

        /// <summary>
        /// ���������� � <paramref name="sb"/> ������� �����, ������� � ������ 
        /// �������� ������ �� ������ � ������� <paramref name="�����������"/>.
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="�����"></param>
        /// <param name="�����������">0 = ����� �����, 1 = ��������� � �.�.</param>
        /// <remarks>
        /// � ������ ��������� ��������, ����� ���������� ������ � StringBuilder 
        /// � ������ ������� - �� ������� ������� � �������.
        /// </remarks>
        static void ���������������������(decimal �����, int �����������, MyStringBuilder sb)
        {
            if (����� == 0) return; // ����� ��������

            // �������� � StringBuilder ������� ������� �������.
            decimal div1000 = Math.Floor(����� / 1000);
            ���������������������(div1000, ����������� + 1, sb);

            uint �������999 = (uint)(����� - div1000 * 1000);
            if (�������999 == 0) return;

            �������������(�������999, ������[�����������], sb);
        }

        static void ���������������������(double �����, int �����������, MyStringBuilder sb)
        {
            if (����� == 0) return; // ����� ��������

            // �������� � StringBuilder ������� ������� �������.
            double div1000 = Math.Floor(����� / 1000);
            ���������������������(div1000, ����������� + 1, sb);

            uint �������999 = (uint)(����� - div1000 * 1000);
            if (�������999 == 0) return;

            �������������(�������999, ������[�����������], sb);
        }

        static void ���������������������(ulong �����, int �����������, MyStringBuilder sb)
        {
            if (����� == 0) return; // ����� ��������

            // �������� � StringBuilder ������� ������� �������.
            ulong div1000 = ����� / 1000;
            ���������������������(div1000, ����������� + 1, sb);

            uint �������999 = (uint)(����� - div1000 * 1000);
            if (�������999 == 0) return;

            �������������(�������999, ������[�����������], sb);
        }

        static void ���������������������(uint �����, int �����������, MyStringBuilder sb)
        {
            if (����� == 0) return; // ����� ��������

            // �������� � StringBuilder ������� ������� �������.
            uint div1000 = ����� / 1000;
            ���������������������(div1000, ����������� + 1, sb);

            uint �������999 = ����� - div1000 * 1000;
            if (�������999 == 0) return;

            �������������(�������999, ������[�����������], sb);
        }

        #region �������������

        /// <summary>
        /// ��������� ������ ������ � ���������, ��������,
        /// "125 �����", "15 ������".
        /// ��� 0 ���������� ������ ������� ��������� � ���.��.
        /// </summary>
        private static void �������������(uint �������999, I���������������� �����, MyStringBuilder sb)
        {
            uint ����������� = �������999 % 10;
            uint ������������� = (�������999 / 10) % 10;
            uint ���������� = (�������999 / 100) % 10;

            sb.Append(�����[����������]);

            if ((�������999 % 100) != 0)
            {
                �������[�������������].�������(sb, �����������, �����.��������);
            }

            // �������� �������� ������ � ������ �����.
            sb.Append(�����������(�����, �������999));
        }

        #endregion

        #region ��������������

        /// <summary>
        /// ���������, �������� �� ����� ��� �������� ������ 
        /// <see cref="�������(decimal,I����������������,StringBuilder)"/>.
        /// </summary>
        /// <returns>
        /// �������� ����������� ����������� ��� null.
        /// </returns>
        public static string ��������������(decimal �����)
        {
            if (����� < 0)
                return "����� ������ ���� ������ ��� ����� ����.";

            if (����� != decimal.Floor(�����))
                return "����� �� ������ ��������� ������� �����.";

            return null;
        }

        /// <summary>
        /// ���������, �������� �� ����� ��� �������� ������ 
        /// <see cref="�������(double,I����������������,StringBuilder)"/>.
        /// </summary>
        /// <returns>
        /// �������� ����������� ����������� ��� null.
        /// </returns>
        public static string ��������������(double �����)
        {
            if (����� < 0)
                return "����� ������ ���� ������ ��� ����� ����.";

            if (����� != Math.Floor(�����))
                return "����� �� ������ ��������� ������� �����.";

            if (����� > MaxDouble)
            {
                return "����� ������ ���� �� ������ " + MaxDouble + ".";
            }

            return null;
        }

        #endregion

        #region �����������

        /// <summary>
        /// ����������� �������� ������� ��������� � ������.
        /// ��������, ������������ ������� (�����, �����, ������) 
        /// � ������ 23 ��� "�����", � � ������ 25 - "������".
        /// </summary>
        public static string �����������(I���������������� ����������������, uint �����)
        {
            uint ����������� = ����� % 10;
            uint ������������� = (����� / 10) % 10;

            if (������������� == 1) return ����������������.�������;
            switch (�����������)
            {
                case 1: return ����������������.��������;
                case 2:
                case 3:
                case 4: return ����������������.�������;
                default: return ����������������.�������;
            }
        }

        /// <summary>
        /// ����������� �������� ������� ��������� � ������.
        /// ��������, ������������ ������� (�����, �����, ������) 
        /// � ������ 23 ��� "�����", � � ������ 25 - "������".
        /// </summary>
        public static string �����������(I���������������� ����������������, decimal �����)
        {
            return �����������(����������������, (uint)(����� % 100));
        }

        #endregion

        #region �������

        static string ������������(uint �����, �������� ���)
        {
            return �����[�����].�������(���);
        }

        abstract class �����
        {
            public abstract string �������(�������� ���);
        }

        class ������������������������ : �����, I�����������������
        {
            public ������������������������(
                string �������,
                string �������,
                string �������,
                string �������������)
            {
                this.������� = �������;
                this.������� = �������;
                this.������� = �������;
                this.������������� = �������������;
            }

            public ������������������������(
                string ������������,
                string �������������)

                : this(������������, ������������, ������������, �������������)
            {
            }

            private readonly string �������;
            private readonly string �������;
            private readonly string �������;
            private readonly string �������������;

            #region I����������������� Members

            public string ������� { get { return this.�������; } }
            public string ������� { get { return this.�������; } }
            public string ������� { get { return this.�������; } }
            public string ������������� { get { return this.�������������; } }

            #endregion

            public override string �������(�������� ���)
            {
                return ���.�������������(this);
            }
        }

        class �������������������������� : �����
        {
            public ��������������������������(string �������)
            {
                this.������� = �������;
            }

            private readonly string �������;

            public override string �������(�������� ���)
            {
                return this.�������;
            }
        }

        private static readonly �����[] ����� = new �����[]
        {
            null,
            new ������������������������ ("����", "����", "����", "����"),
            new ������������������������ ("���", "���", "���", "����"),
            new ������������������������ ("���", "����"),
            new ������������������������ ("������", "�������"),
            new �������������������������� ("����"),
            new �������������������������� ("�����"),
            new �������������������������� ("����"),
            new �������������������������� ("������"),
            new �������������������������� ("������"),
        };

        #endregion
        #region �������

        static readonly �������[] ������� = new �������[]
        {
            new ������������� (),
            new ������������� (),
            new �������������� ("��������"),
            new �������������� ("��������"),
            new �������������� ("�����"),
            new �������������� ("���������"),
            new �������������� ("����������"),
            new �������������� ("���������"),
            new �������������� ("�����������"),
            new �������������� ("���������")
        };

        abstract class �������
        {
            public abstract void �������(MyStringBuilder sb, uint �����������, �������� ���);
        }

        class ������������� : �������
        {
            public override void �������(MyStringBuilder sb, uint �����������, �������� ���)
            {
                sb.Append(������������(�����������, ���));
            }
        }

        class ������������� : �������
        {
            static readonly string[] �������������� = new[]
            {
                "������",
                "�����������",
                "����������",
                "����������",
                "������������",
                "����������",
                "�����������",
                "����������",
                "������������",
                "������������"
            };

            public override void �������(MyStringBuilder sb, uint �����������, �������� ���)
            {
                sb.Append(��������������[�����������]);
            }
        }

        class �������������� : �������
        {
            public ��������������(string ���������������)
            {
                this.��������������� = ���������������;
            }

            private readonly string ���������������;

            public override void �������(MyStringBuilder sb, uint �����������, �������� ���)
            {
                sb.Append(this.���������������);

                if (����������� == 0)
                {
                    // ����� "��������", "��������" � �.�. �� ����� "����" (������)
                }
                else
                {
                    sb.Append(������������(�����������, ���));
                }
            }
        }

        #endregion
        #region �����

        static readonly string[] ����� = new[]
        {
            null,
            "���",
            "������",
            "������",
            "���������",
            "�������",
            "��������",
            "�������",
            "���������",
            "���������"
        };

        #endregion
        #region ������

        #region ����������

        class ���������� : I����������������
        {
            public string �������� { get { return "������"; } }
            public string ������� { get { return "������"; } }
            public string ������� { get { return "�����"; } }
            public �������� �������� { get { return ��������.�������; } }
        }

        #endregion
        #region �����

        class ����� : I����������������
        {
            readonly string ��������������;

            public �����(string ��������������)
            {
                this.�������������� = ��������������;
            }

            public string �������� { get { return this.��������������; } }
            public string ������� { get { return this.�������������� + "�"; } }
            public string ������� { get { return this.�������������� + "��"; } }
            public �������� �������� { get { return ��������.�������; } }
        }

        #endregion

        /// <summary>
        /// ����� - ������ �� 3 ����.  ���� ������ ������, �����, ��������� � �.�.
        /// </summary>
        static readonly I����������������[] ������ = new I����������������[]
        {
            new ���������� (),
            new ����� ("�������"),
            new ����� ("��������"),
            new ����� ("��������"),
            new ����� ("�����������"),
            new ����� ("�����������"),
            new ����� ("�����������"),
            new ����� ("����������"),
            new ����� ("���������"),
 
            // ��� ���������� ������� ��������� ���� �������� ���� decimal.
        };

        #endregion

        #region MaxDouble

        /// <summary>
        /// ������������ ����� ���� double, ������������ � ���� �������.
        /// </summary>
        /// <remarks>
        /// �������������� ������ �� ���������� ����������� �������.
        /// ���� �������� ��� ������, ��� ����� ������������� ���������.
        /// </remarks>
        public static double MaxDouble
        {
            get
            {
                if (maxDouble == 0)
                {
                    maxDouble = CalcMaxDouble();
                }

                return maxDouble;
            }
        }

        private static double maxDouble;

        static double CalcMaxDouble()
        {
            double max = Math.Pow(1000, ������.Length + 1);

            double d = 1;

            while (max - d == max)
            {
                d *= 2;
            }

            return max - d;
        }

        #endregion

        #region ��������������� ������

        #region �����

        #endregion
        #region MyStringBuilder

        /// <summary>
        /// ��������������� �����, ����������� <see cref="StringBuilder"/>.
        /// ����� �������� <see cref="MyStringBuilder.Append"/> ��������� �������.
        /// </summary>
        class MyStringBuilder
        {
            public MyStringBuilder(StringBuilder sb)
            {
                this.sb = sb;
            }

            readonly StringBuilder sb;
            bool insertSpace;

            /// <summary>
            /// ��������� ����� <paramref name="s"/>,
            /// �������� ����� ��� ������, ���� �����.
            /// </summary>
            public void Append(string s)
            {
                if (string.IsNullOrEmpty(s)) return;

                if (this.insertSpace)
                {
                    this.sb.Append(' ');
                }
                else
                {
                    this.insertSpace = true;
                }

                this.sb.Append(s);
            }

            public override string ToString()
            {
                return sb.ToString();
            }
        }

        #endregion

        #endregion

        #region ���������� ������ �������, ������������ string

        /// <summary>
        /// ���������� ������� ����� ��������� �������.
        /// </summary>
        public static string �������(decimal �����, I���������������� ��)
        {
            return �������(�����, ��, ���������.���);
        }

        /// <summary>
        /// ���������� ������� �����.
        /// </summary>
        public static string �������(decimal �����, I���������������� ��, ��������� ���������)
        {
            return ApplyCaps(�������(�����, ��, new StringBuilder()), ���������);
        }

        /// <summary>
        /// ���������� ������� ����� ��������� �������.
        /// </summary>
        public static string �������(double �����, I���������������� ��)
        {
            return �������(�����, ��, ���������.���);
        }

        /// <summary>
        /// ���������� ������� �����.
        /// </summary>
        public static string �������(double �����, I���������������� ��, ��������� ���������)
        {
            return ApplyCaps(�������(�����, ��, new StringBuilder()), ���������);
        }

        /// <summary>
        /// ���������� ������� ����� ��������� �������.
        /// </summary>
        public static string �������(ulong �����, I���������������� ��)
        {
            return �������(�����, ��, ���������.���);
        }

        /// <summary>
        /// ���������� ������� �����.
        /// </summary>
        public static string �������(ulong �����, I���������������� ��, ��������� ���������)
        {
            return ApplyCaps(�������(�����, ��, new StringBuilder()), ���������);
        }

        /// <summary>
        /// ���������� ������� ����� ��������� �������.
        /// </summary>
        public static string �������(uint �����, I���������������� ��)
        {
            return �������(�����, ��, ���������.���);
        }

        /// <summary>
        /// ���������� ������� �����.
        /// </summary>
        public static string �������(uint �����, I���������������� ��, ��������� ���������)
        {
            return ApplyCaps(�������(�����, ��, new StringBuilder()), ���������);
        }

        internal static string ApplyCaps(StringBuilder sb, ��������� ���������)
        {
            ���������.���������(sb);
            return sb.ToString();
        }

        #endregion
    }

    /// <summary>
    /// ��������� ����������� ��������� ����.
    /// </summary>
    public abstract class ���������
    {
        /// <summary>
        /// ��������� ��������� � <paramref name="sb"/>.
        /// </summary>
        public abstract void ���������(StringBuilder sb);

        class _��� : ���������
        {
            public override void ���������(StringBuilder sb)
            {
                for (int i = 0; i < sb.Length; ++i)
                {
                    sb[i] = char.ToUpperInvariant(sb[i]);
                }
            }
        }

        class _��� : ���������
        {
            public override void ���������(StringBuilder sb)
            {
            }
        }

        class _������ : ���������
        {
            public override void ���������(StringBuilder sb)
            {
                sb[0] = char.ToUpperInvariant(sb[0]);
            }
        }

        public static readonly ��������� ��� = new _���();
        public static readonly ��������� ��� = new _���();
        public static readonly ��������� ������ = new _������();
    }

    /// <summary>
    /// ��������� ��� ������ ��� ������������ ���� ������ ��������� - �������� � �������.
    /// �������� ��������� ��������������� ����� - �����, �������, ����.
    /// </summary>
    /// <remarks>
    /// ��������������, ��� �������� ������� ����� 100 �������. 
    /// </remarks>
    public class ������
    {
        /// <summary> </summary>
        public ������(I���������������� ��������, I���������������� �������)
        {
            this.�������� = ��������;
            this.������� = �������;
        }

        readonly I���������������� ��������;
        readonly I���������������� �������;

        /// <summary>
        /// �������� ������� ��������� ������ - �����, �������, ���� � �.�.
        /// </summary>
        public I���������������� ���������������
        {
            get { return this.��������; }
        }

        /// <summary>
        /// ������� ������� ��������� ������ - �������, �����, ��������� � �.�.
        /// </summary>
        public I���������������� ��������������
        {
            get { return this.�������; }
        }

        public static readonly ������ ����� = new ������(
            new ����������������(��������.�������, "�����", "�����", "������"),
            new ����������������(��������.�������, "�������", "�������", "������"));

        public static readonly ������ ������� = new ������(
            new ����������������(��������.�������, "������ ���", "������� ���", "�������� ���"),
            new ����������������(��������.�������, "����", "�����", "������"));

        public static readonly ������ ���� = new ������(
            new ����������������(��������.�������, "����", "����", "����"),
            new ����������������(��������.�������, "����", "�����", "������"));

        /// <summary>
        /// ���������� ������� ����� ��������� �������.
        /// </summary>
        public string �������(decimal �����)
        {
            return �����.�������(�����, this);
        }

        /// <summary>
        /// ���������� ������� ����� ��������� �������.
        /// </summary>
        public string �������(double �����)
        {
            return �����.�������(�����, this);
        }

        /// <summary>
        /// ���������� ������� �����.
        /// </summary>
        public string �������(decimal �����, ��������� ���������)
        {
            return �����.�������(�����, this, ���������);
        }

        /// <summary>
        /// ���������� ������� �����.
        /// </summary>
        public string �������(double �����, ��������� ���������)
        {
            return �����.�������(�����, this, ���������);
        }
    }

    /// <summary>
    /// �����, �������� �������� ����� ������� ��������� � ����� ����.
    /// </summary>
    public class ���������������� : I����������������
    {
        /// <summary> </summary>
        public ����������������(
            �������� ��������,
            string ��������,
            string �������,
            string �������)
        {
            this.�������� = ��������;
            this.�������� = ��������;
            this.������� = �������;
            this.������� = �������;
        }

        readonly �������� ��������;
        readonly string ��������;
        readonly string �������;
        readonly string �������;

        #region I���������������� Members

        string I����������������.��������
        {
            get { return this.��������; }
        }

        string I����������������.�������
        {
            get { return this.�������; }
        }

        string I����������������.�������
        {
            get { return this.�������; }
        }

        �������� I����������������.��������
        {
            get { return this.��������; }
        }

        #endregion
    }

    #region ��������

    /// <summary>
    /// ��������� ��� � �����.
    /// ����� ������������ � �������� ��������� "������� ���������" ������ 
    /// <see cref="�����.�������(decimal,I����������������,StringBuilder)"/>.
    /// ��������� ����� � ������ ������������ ���� � ���.
    /// </summary>
    /// <example>
    /// �����.������� (2, ��������.�������); // "���"
    /// �����.������� (2, ��������.�������); // "���"
    /// �����.������� (21, ��������.�������); // "�������� ����"
    /// </example>
    public abstract class �������� : I����������������
    {
        internal abstract string �������������(I����������������� �����);

        #region ����

        class _������� : ��������
        {
            internal override string �������������(I����������������� �����)
            {
                return �����.�������;
            }
        }

        class _������� : ��������
        {
            internal override string �������������(I����������������� �����)
            {
                return �����.�������;
            }
        }

        class _������� : ��������
        {
            internal override string �������������(I����������������� �����)
            {
                return �����.�������;
            }
        }

        class _������������� : ��������
        {
            internal override string �������������(I����������������� �����)
            {
                return �����.�������������;
            }
        }

        public static readonly �������� ������� = new _�������();
        public static readonly �������� ������� = new _�������();
        public static readonly �������� ������� = new _�������();
        public static readonly �������� ������������� = new _�������������();

        #endregion

        #region I���������������� Members

        �������� I����������������.��������
        {
            get { return this; }
        }

        string I����������������.��������
        {
            get { return null; }
        }

        string I����������������.�������
        {
            get { return null; }
        }

        string I����������������.�������
        {
            get { return null; }
        }

        #endregion
    }

    #region I�����������������

    internal interface I�����������������
    {
        string ������� { get; }
        string ������� { get; }
        string ������� { get; }
        string ������������� { get; }
    }

    #endregion

    #endregion

    #region ����������������

    /// <summary>
    /// ������������ ������� ��������� (��������, ����, �����)
    /// � �������� ��� ����������� ���������� ��� ������������
    /// ���� ������� � ������, � ������ - ��� �������-�������� �����
    /// � �������������� ��� / �����.
    /// </summary>
    public interface I����������������
    {
        /// <summary>
        /// ����� ������������� ������ ������������� �����.
        /// ����������� � ������������ "����":
        /// ���� ������, ���� �������, ���� �����, ���� ����� � �.�.
        /// </summary>
        string �������� { get; }

        /// <summary>
        /// ����� ������������ ������ ������������� �����.
        /// ����������� � ������������� "����, ���, ���, ������":
        /// ��� ������, ��� ��������, ��� �����, ���� ����� � �.�.
        /// </summary>
        string ������� { get; }

        /// <summary>
        /// ����� ������������ ������ �������������� �����.
        /// ����������� � ������������ "����, ����, �����, ����" � ��:
        /// ���� �����, ���� ���������, ���� ������, ���� ����� � �.�.
        /// </summary>
        string ������� { get; }

        /// <summary>
        /// ��� � ����� ������� ���������.
        /// </summary>
        �������� �������� { get; }
    }

    #endregion
}