﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.Common.Tools
{
    /// <summary>
    /// Класс для управления движками по работе с документами Word
    /// </summary>
    public class WordReportProxy: IDisposable
    {
        /// <summary>
        /// Менеджер отчетов Word. Выбирается параметром конструктора.
        /// </summary>
        public IWordReport ReportManager { get; private set; }

        public WordReportProxy(string filePath, WordReportManagerType type = WordReportManagerType.Interop, bool silent = true, bool visible = false)
        {
            if(type == WordReportManagerType.OpenXML)
                throw new NotSupportedException("Менеджер OpenXML не поддерживается");
            switch (type)
            {
                case WordReportManagerType.Interop:
                    ReportManager = new WordReportInterop(filePath, silent, visible);
                    break;
                default:
                    throw new Exception("Неизвестный тип менеджера");
            }
        }

        public void Dispose()
        {
            if(ReportManager != null)
                ReportManager.Dispose();
        }
    }

    public enum WordReportManagerType
    {
        Interop,
        OpenXML
    }

    public interface IWordReport: IDisposable
    {
        /// <summary>
        /// Get opened Word document 
        /// </summary>
        /// <typeparam name="T">Document class type</typeparam>
        T GetDocument<T>() where T : class;

        /// <summary>
        /// Populate pie chart in the template with the data
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="chartIndex">Chart index inside Word document (starts from 1)</param>
        /// <param name="visibleLabelsCount">Labels count to display starting from 1st data element</param>
        void PopulatePieChart(Dictionary<string, object> data, int chartIndex, int visibleLabelsCount, int dataStartColumn = 1);
        /// <summary>
        /// Populate bar charts
        /// </summary>
        /// <param name="data">Chart data rows collection</param>
        /// <param name="columns">List of chart column names (Excel view point)</param>
        /// <param name="chartIndex">Chart index in Word document (starts from 1)</param>
        /// <param name="dataStartColumn"> Default 1. Set to 2 if row names are NUMBERS.</param>
        void PopulateBarChart(Dictionary<string, List<object>> data, List<string> columns, int chartIndex, int dataStartColumn = 1);
        /// <summary>
        /// Save all changes to the document and close app
        /// </summary>
        void SaveAndClose();
        /// <summary>
        /// Save document and show word app
        /// </summary>
        void Show();
        /// <summary>
        /// Hide word app
        /// </summary>
        void Hide();
        /// <summary>
        /// Populate word table
        /// </summary>
        /// <param name="columns">Table columns data</param>
        /// <param name="rows">Table rows data</param>
        /// <param name="options">Table params</param>
        void PopulateTable(List<WordReportDataColumn> columns, Dictionary<string, List<object>> rows, PopulateTableOptions options);
        /// <summary>
        /// Find and replace text in the document
        /// </summary>
        /// <param name="findText">Search text</param>
        /// <param name="replaceWithText">Replace text</param>
        void FindAndReplace(object findText, object replaceWithText);
    }
}
