﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Marshal = System.Runtime.InteropServices.Marshal;
using Excel = Microsoft.Office.Interop.Excel;

namespace PFR_INVEST.Common.Tools
{
	public static class ExcelTools
	{
	    public static byte[] SafeReadFile(string filename)
	    {
	        using (var file = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
	        {
	            var buf = new byte[file.Length];
	            if (file.Length > int.MaxValue)
	            {
                    file.Read(buf, 0, int.MaxValue);
                    file.Read(buf, int.MaxValue, (int)file.Length - int.MaxValue);
	                return buf;
	            }
	            file.Read(buf,0, (int)file.Length);
	            return buf;
	        }
        }

		public static void ReleaseObject(object obj) // note ref!
		{
			// Do not catch an exception from this.
			// You may want to remove these guards depending on
			// what you think the semantics should be.
			if (obj != null && Marshal.IsComObject(obj))
			{
				Marshal.ReleaseComObject(obj);
			}
			// Since passed "by ref" this assingment will be useful
			// (It was not useful in the original, and neither was the
			//  GC.Collect.)
			obj = null;
		}

		/// <summary>
		/// Присваивает листам Экселя новые имена
		/// </summary>
		/// <param name="filename">Путь к файлу</param>
		/// <param name="names">Список новых имён для листов</param>
		public static void RenameExcelSheets(string filename, IList<string> names)
		{
			Excel.Application xl = null;
			Excel.Workbook xlBook = null;
			try
			{
				object oMissing = System.Reflection.Missing.Value;
				xl = new Excel.Application();			

				xlBook = (Excel.Workbook)xl.Workbooks.Open(filename, oMissing,
				  oMissing, oMissing, oMissing, oMissing, oMissing, oMissing
				 , oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);
				var list = xlBook.Worksheets.Cast<Excel.Worksheet>().ToList();
				var count = Math.Min((int) list.Count, names.Count);
				for (var i = 0; i < count; i++)
					list[i].Name = names[i];
				xlBook.Save();
				xl.Application.Workbooks.Close();
			}
			finally {				
				ReleaseObject(xlBook);
				// The Quit is done in the finally because we always
				// want to quit. It is no different than releasing RCWs.
				if (xl != null)
				{
					xl.Quit();
				}
				ReleaseObject(xl);   
			
			}
		}
	}
}
