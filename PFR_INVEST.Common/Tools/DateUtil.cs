﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace PFR_INVEST.Common.Tools
{
	public class DateUtil
	{
		#region GetMonthIndex()
        public static int GetMonthIndex(string month, bool orderDescending = false)
        {
            var target = (month ?? string.Empty).Trim().ToLower();
            var months = GetMonthNames(true);
            var index = months.IndexOf(target);
            if (orderDescending)
            {
                index = 13 - index;
            }
            return index;
        }

        private static List<string> _monthNames;
        private static List<string> _monthNamesLower;
        public static List<string> GetMonthNames(bool lowerCase = false)
        {
            if (_monthNames != null)
                return lowerCase ? _monthNamesLower : _monthNames;
            var ci = new CultureInfo("ru-RU");
            _monthNames = ci.DateTimeFormat.MonthNames.ToList();
            _monthNamesLower = _monthNames.Select(x => x.ToLower().Trim()).ToList();

            return lowerCase ? _monthNamesLower : _monthNames;
        }

		#endregion

        public static string GetMonthName(int index)
        {
            return GetMonthNames()[index];
        }

		public static DateTime? ParseImportDate(string dateS)
		{
			if (string.IsNullOrEmpty(dateS)) return null;
			DateTime dt;

			if (!DateTime.TryParse(dateS, out dt))
			{
				throw new Exception($"Не удалось получить дату из строки \"{dateS}\"");
			}

			return dt;
		}

		public static bool IsDateEquals(DateTime? dt1, DateTime? dt2)
		{
			if (dt1.HasValue && dt2.HasValue)
			{
				return dt1.Value.Date == dt2.Value.Date;
			}
			return false;
		}
	}
}
