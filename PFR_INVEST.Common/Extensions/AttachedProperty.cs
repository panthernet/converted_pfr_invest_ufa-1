﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.Common.Extensions
{
    public static class AttachedProperty
    {
        private static readonly Dictionary<WeakReference, Dictionary<string, object>> Values = new Dictionary<WeakReference, Dictionary<string, object>>();

        private static WeakReference GetKey(object o)
        {
            var weakReferenceList = new List<WeakReference>();
            try
            {
                foreach (var key in Values.Keys)
                {
                    if (!key.IsAlive)
                        weakReferenceList.Add(key);
                    else if (key.Target == o)
                        return key;
                }
                var key1 = new WeakReference(o);
                Values.Add(key1, new Dictionary<string, object>());
                return key1;
            }
            finally
            {
                foreach (var key in weakReferenceList)
                    Values.Remove(key);
            }
        }

        public static void SetAttachedProperty(this object o, string name, object value)
        {
            var key = GetKey(o);
            var dictionary = Values[key];
            if (dictionary.ContainsKey(name))
                dictionary[name] = value;
            else
                dictionary.Add(name, value);
        }

        public static object GetAttachedProperty(this object o, string name)
        {
            var key = GetKey(o);
            object obj;
            return Values[key].TryGetValue(name, out obj) ? obj : null;
        }
    }
}