﻿using System;

namespace PFR_INVEST.Common.Exceptions
{
    public class MSOfficeException : Exception
    {
        public MSOfficeException(string text)
            :base(text)
        {
        }
    }
}
