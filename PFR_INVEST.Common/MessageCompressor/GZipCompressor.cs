using System;
using System.IO;
using System.IO.Compression;

//using ICSharpCode.SharpZipLib.GZip;

namespace PFR_INVEST.Common.MessageCompressor
{
    /// <summary>
    /// Gzip-���������
    /// </summary>
    public static class GZipCompressor
    {
        /// <summary>
        /// �����
        /// </summary>
        /// <exception cref="ArgumentNullException"/>
        public static byte[] Compress(byte[] data)
        {
            //var perf = new PerfomanceTracker();
            try
            {
                if (data == null)
                {
                    throw new ArgumentNullException();
                }

                using (var ms = new MemoryStream())
                {
                    var compressStream = new DeflateStream(ms, CompressionMode.Compress);
                    //Stream compressStream = new GZipOutputStream(ms);
                    //((GZipOutputStream)compressStream).SetLevel(4);
                    compressStream.Write(data, 0, data.Length);
                    compressStream.Close();

                    return ms.ToArray();
                }
            }
            finally
            {
                //long time = perf.GetMSeconds();
            }
        }

        /// <summary>
        /// �����������
        /// </summary>
        /// <exception cref="ArgumentNullException"/>
        public static byte[] Decompress(byte[] data)
        {
            //var perf = new PerfomanceTracker();
            try
            {
                if (data == null)
                {
                    throw new ArgumentNullException();
                }

                using (var ms = new MemoryStream())
                {
                    //using (Stream decompressStream = new GZipInputStream(new MemoryStream(data)))
                    using (var decompressStream = new DeflateStream(new MemoryStream(data), CompressionMode.Decompress))
                    {
                        byte[] buffer = new byte[40960];


                        int size;
                        while ((size = decompressStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, size);
                        }
                        decompressStream.Close();

                    }
                    return ms.ToArray();
                }
            }
            finally
            {
                //long time = perf.GetMSeconds();
            }
        }
    }
}