﻿using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PFR_INVEST.Tests.Shared
{
    public static class Extensions
    {
        public static void Add<T1, T2>(this ConcurrentDictionary<T1, T2> dic, T1 index, T2 value)
        {
            if (!dic.TryAdd(index, value))
                dic[index] = value;
        }

        public static void Remove<T1, T2>(this ConcurrentDictionary<T1, T2> dic, T1 index)
        {
            T2 value;
            dic.TryRemove(index, out value);
        }


        public static bool IsRequired<T>(this T item, string propname) where T : class
        {
            var type = item.GetType();
            var property = type.GetProperties().FirstOrDefault(p => p.Name == propname && p.GetCustomAttributes(false)
                                                                        .Any(a => a.GetType() == typeof(RequiredAttribute)));
            return property != null;
        }
    }
}
