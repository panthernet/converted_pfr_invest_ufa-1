﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Timers;
using System.Web;
using db2connector;
using PFR_INVEST.Auth.ClientData;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.Tests.Shared
{
    public class WCFClientLight: IDisposable
    {
        private DataServiceClient _wcfClient; //ServiceSystem.CreateNewDataClient();
        public bool UseAutoRepair;
        private readonly Timer _pingTimer = new Timer();

        public ClientCredentials CurrentClientCredentials => _wcfClient.ClientCredentials;
        public ClientCredentials ClientCredentials { get; set; }
        public static readonly ClientAuthType AuthType;

        public WCFClientLight()
        {
            _pingTimer.AutoReset = true;
            _pingTimer.Interval = 30000;
            _pingTimer.Elapsed += PingTimer_Elapsed;
        }

        static WCFClientLight()
        {
            ServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
            AuthServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
            BLServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;

            switch (ConfigurationManager.AppSettings["AuthType"])
            {
                case "ECASA":
                    AuthType = ClientAuthType.ECASA;
                    break;
                case "ECASAHTTPS":
                    AuthType = ClientAuthType.ECASAHTTPS;
                    break;
                default:
                    AuthType = ClientAuthType.None;
                    break;
            }

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;

        }

        //public static ApplicationUser User => MyUserStore.Current.GetDefaultUser().Result;

        /*public static ApplicationUser GetCurrentUser()
        {
            var user = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(HttpContext.Current.User.Identity.GetUserId());
            return user;
        }

        //TODO надеемся что в хранилище первый юзер - то что надо!
        public static WCFClient Current => GetCurrentUser()?.Client;
        */
        internal static WCFClientLight CreateLightClient(string username, string password = null)
        {
            return new WCFClientLight().CreateLightClientInternal(username, password);
        }

        internal WCFClientLight CreateLightClientInternal(string username, string password = null)
        {
            _wcfClient = ServiceSystem.CreateNewDataClient(AuthType);
            _wcfClient.ClientCredentials.Windows.ClientCredential.UserName = username;
            var behavior = _wcfClient.Endpoint.Behaviors.Find<IDB2Connector>();
            if (behavior == null)
                _wcfClient.Endpoint.Behaviors.Add(new MessageCompressionAttribute(Compress.Reply | Compress.Request));
            _wcfClient.ClientCredentials.SupportInteractive = false;
            _wcfClient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
                X509CertificateValidationMode.None;
            _wcfClient.Open();
            _wcfClient.GetChannel().InitService(new object[]
            {
                username,
                password,
                null
            });
            return this;
        }

        private void PingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                _wcfClient?.GetChannel().GetServiceVersion();
            }
            catch (Exception ex)
            {
                //App.log.WriteException(ex, "При обращении к WCF сервису возникла ошибка.");
            }
        }

        public bool IsFaulted()
        {
            return _wcfClient == null || _wcfClient.State == CommunicationState.Faulted || _wcfClient.State == CommunicationState.Closed || !TestPing();
        }

        private bool TestPing()
        {
            try
            {
                //Simple call to check connection state
                _wcfClient.GetChannel().GetServiceVersion();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async void RepairConnection()
        {
            //пропускаем восстановление связи если приложение уже закрывается, больше обращений к сервису не требуется
            //if (App.IsClosing) return false;
            bool bError = false;
            try
            {
                if (_wcfClient.State == CommunicationState.Faulted)
                    bError = true;
                else
                {
                    //Simple call to check connection state
                    _wcfClient.GetChannel().GetServiceVersion();
                }

            }
            catch (Exception ex)
            {
                bError = true;
                //App.log.WriteException(ex, "Возникла ошибка при восстановлении соединения к WCF сервису.");
            }

            if (_wcfClient.State != CommunicationState.Opened || bError)
            {
               // if (ClientCredentials != null)
               //     CreateClientByCredentials();
            }
        }

        /*public void CreateClientByCredentials(string username)
        { 
            PingTimer.Stop();

            _wcfClient = ServiceSystem.CreateNewDataClient(AuthType);

            if (ClientCredentials != null)
            {
                _wcfClient.ClientCredentials.Windows.ClientCredential.UserName = ClientCredentials.Windows.ClientCredential.UserName;
                _wcfClient.ClientCredentials.Windows.ClientCredential.Password = ClientCredentials.Windows.ClientCredential.Password;
                _wcfClient.ClientCredentials.Windows.AllowedImpersonationLevel = ClientCredentials.Windows.AllowedImpersonationLevel;


                var behavior = _wcfClient.Endpoint.Behaviors.Find<IDB2Connector>();
                if (behavior == null)
                {
                    _wcfClient.Endpoint.Behaviors.Add(new MessageCompressionAttribute(Compress.Reply | Compress.Request));
                }

                // Скрытие системного окна аутентификации при неверном логине/пароле
                _wcfClient.ClientCredentials.SupportInteractive = false;
                _wcfClient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                _wcfClient.Open();
                ClientCredentials = CurrentClientCredentials;

                // устанавливаем некоторые параметры журналирования.
                Client.SetCurrentUserInformation(
                    ClientCredentials.Windows.ClientCredential.UserName,
                    Environment.UserName,
                    Environment.MachineName);

                try
                {
                    var iresult = Client.InitService(new object[]
                    {
                        ClientCredentials.Windows.ClientCredential.UserName,
                        ClientCredentials.Windows.ClientCredential.Password,
                        null
                    });
                    if (iresult.Response != AuthMsg.Success)
                        throw iresult.Response == AuthMsg.UserMustChangePassword
                            ? (Exception)new UserMustChangePasswordException((string)iresult.ResultValue)
                            : new InvalidLoginOrPasswordException((string)iresult.ResultValue);

                    AuthServiceSystem.GetDataServiceDelegate = () => _wcfClient.GetChannel();
                    var ap = new AuthProvider(username);
                    ap.RefreshUserSecurity();
                    try
                    {
                        if (!ap.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.User))
                        {
                            throw new NotUserException();
                        }
                    }
                    finally
                    {
                        AuthServiceSystem.GetDataServiceDelegate = null;
                    }

                }
                catch (NotUserException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    throw new AuthInitException { Ex = e };
                }
            }

            PingTimer.Start();
            _wcfClient.InnerChannel.Faulted += (sender, e) =>
            {
                RepairConnection();
            };
        }
        */

        /// <summary>
        /// Текущий канал общения с БД
        /// </summary>
        public IDB2Connector Client
        {
            get
            {
                if (_wcfClient == null) return null;
                if (!UseAutoRepair) return _wcfClient?.GetChannel();
                if (ClientCredentials == null) return _wcfClient.GetChannel();
                UseAutoRepair = false;
                try
                {
                    RepairConnection();
                }
                finally
                {
                    UseAutoRepair = true;
                }
                return _wcfClient.GetChannel();
            }
        }


        /// <summary>
        /// Метод-привязка для получения канала общения с БД для каждого отдельного юзера
        /// </summary>
        public static IDB2Connector GetDataServiceFunc()
        {
            return LightService.Get(HttpContext.Current == null ? null : HttpContext.Current.User.Identity.Name).Client;
        }

        public void Dispose()
        {
            ((IDisposable) _wcfClient)?.Dispose();
            _pingTimer?.Dispose();
        }
    }
}