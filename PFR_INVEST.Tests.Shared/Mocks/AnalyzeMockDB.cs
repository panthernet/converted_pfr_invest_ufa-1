﻿using System;
using System.Collections.Generic;
using System.Linq;
using db2connector;
using Moq;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.Tests.Shared.Mocks
{
    public class AnalyzeMockDB
    {
        //Имитация DB2Connector'a для тестирования
        private readonly Mock<IDB2Connector> _cntr = new Mock<IDB2Connector>();
        public IDB2Connector Connector => _cntr.Object;

        /// <summary>
        /// Возвращать пустые коллекции из репозитория
        /// </summary>
        public bool IsReturnEmptyList { get; set; }
        //справочники
        private List<SubjectSPN> _subjects = new List<SubjectSPN>();
        private List<ActivesKind> _activesKinds = new List<ActivesKind>();
        private List<FilingType> _filingTypes = new List<FilingType>();
        private List<StatementType> _statementTypes = new List<StatementType>();
        private List<PaymentKind> _paymentKinds = new List<PaymentKind>();
       
        private List<PaymentKind> paymentKinds
        {
            get
            {
                if (!_paymentKinds.Any())
                {
                    _paymentKinds = DataContainerFacade.GetList<PaymentKind>();
                }
                return _paymentKinds;
            }
        }
        private List<FilingType> filingTypes
        {
            get
            {
                if (!_filingTypes.Any())
                {
                    _filingTypes = DataContainerFacade.GetList<FilingType>();
                }
                return _filingTypes;
            }
        }
        private List<StatementType> statementTypes
        {
            get
            {
                if (!_statementTypes.Any())
                {
                    _statementTypes = DataContainerFacade.GetList<StatementType>();
                }
                return _statementTypes;
            }
        }
        private List<ActivesKind> activeskinds
        {
            get
            {
                if (!_activesKinds.Any())
                {
                    _activesKinds = DataContainerFacade.GetList<ActivesKind>();
                }
                return _activesKinds;
            }
        }

        private List<SubjectSPN> subjects
        {
            get
            {
                if (!_subjects.Any())
                    _subjects = DataContainerFacade.GetList<SubjectSPN>();
                return _subjects;
            }
        }
        private List<AnalyzeCondition> _conditions = new List<AnalyzeCondition>();

        private IEnumerable<AnalyzeCondition> conditions
        {
            get
            {
                if (!_conditions.Any())
                    _conditions = DataContainerFacade.GetList<AnalyzeCondition>();
                return _conditions;
            }
        }

        public AnalyzeMockDB()
        {
            IsReturnEmptyList = false;
            //справочник субъектов СПН
            _cntr.Setup(x => x.GetList(It.IsRegex(".*SubjectSPN"))).Returns(subjects);
            _cntr.Setup(x => x.GetList(It.IsRegex(".*ActivesKind"))).Returns(activeskinds);
            _cntr.Setup(x => x.GetList(It.IsRegex(".*StatementType"))).Returns(statementTypes);
            _cntr.Setup(x => x.GetList(It.IsRegex(".*FilingType"))).Returns(filingTypes);
            _cntr.Setup(x => x.GetList(It.IsRegex(".*PaymentKind"))).Returns(paymentKinds);
            //Популировать правила
            var lastOrDefault = conditions.LastOrDefault();
        }

        public AnalyzeCondition GetCondition(string className)
        {
            return conditions.First(
                        d => d.ClassName.Equals(className, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Заполнение данными мок-репозитория для отчетов 1.1 - 1.2
        ///Можно вызывать несколько раз подряд для имитации запросов за разные периоды
        /// </summary>
        /// <param name="yr">год</param>
        /// <param name="qr">квартал</param>
        /// <param name="act">№ постановления</param>
        /// <param name="isArch">архивный</param>
        public List<AnalyzeYieldfundsData> Set1_1_1_2Data(int yr, int? qr, short? act = null, bool isArch = false)
        {
            var result = new List<AnalyzeYieldfundsData>();
            foreach (var sb in subjects)
            {
                var sti = 1;
                int itr;

                if (!qr.HasValue)
                    itr = 4;
                else
                {
                    sti = itr = qr.Value;

                }

                for (int i = sti; i <= itr; i++)

                {
                    result.AddRange(new[]
                    {
                        new AnalyzeYieldfundsData() {Year = yr,Kvartal = i, ReportAct = act??140, Total = 1, SubjectId = sb.Id},
                    });
                }
            }

            _cntr.Setup(
            x =>
                x.GetYieldfundsDataByYearKvartal(yr, qr, act, false)).Returns(!IsReturnEmptyList ? result : new List<AnalyzeYieldfundsData>());

            return result;
        }



        /// <summary>
        /// Заполнение данными мок-репозитория для отчетов 2 - 3
        ///Можно вызывать несколько раз подряд для имитации запросов за разные периоды
        /// </summary>
        /// <param name="yr"></param>
        /// <param name="qr"></param>
        /// <param name="sub">с вычетом возврата в НПФ</param>
        public void Set2_3Data(int yr, int? qr, int sub)
        {
            var result = new List<AnalyzePensionfundtonpfData>();
            foreach (var sb in subjects.Where(s => s.Fondtype < 2))//субъекты для отчета 2-3
            {



                for (int i = 1; i <= 4; i++)

                {
                    result.AddRange(new[]
                    {
                            new AnalyzePensionfundtonpfData() {Year = yr, Kvartal = i, Total = 1, SubjectId = sb.Id},

                        });
                }
            }

            _cntr.Setup(
                x =>
                    x.GetPensionfundDataByYearKvartal(yr, qr, sub, false))
                    .Returns(!IsReturnEmptyList ? result : new List<AnalyzePensionfundtonpfData>());
        }

        /// <summary>
        /// Заполнение данными мок-репозитория для отчетов 4
        ///Можно вызывать несколько раз подряд для имитации запросов за разные периоды
        /// </summary>
        /// <param name="yr"></param>
        /// <param name="qr"></param>
        public void Set4Data(int yr, int? qr)
        {
            var result = new List<AnalyzeInsuredpersonData>();
            foreach (var sb in subjects.Where(s => s.Fondtype < 2))//субъекты для отчета 4
            {
                for (int i = 1; i <= 4; i++)

                {
                    result.AddRange(new[]
                    {
                        new AnalyzeInsuredpersonData() {Year = yr, Kvartal = i, Total = 1, SubjectId = sb.Id},

                    });
                }
            }
            _cntr.Setup(
                x =>
                    x.GetInsuredpersonDataByYearKvartal(yr, qr, false))
                    .Returns(!IsReturnEmptyList ? result : new List<AnalyzeInsuredpersonData>());
        }

        /// <summary>
        /// Заполнение данными мок-репозитория для отчетов 5
        ///Можно вызывать несколько раз подряд для имитации запросов за разные периоды
        /// </summary>
        /// <param name="yr"></param>
        /// <param name="qr"></param>
        public void Set5Data(int yr, int? qr)
        {
            var result = new List<AnalyzePensionplacementData>();
            foreach (var sb in subjects.Where(s => s.Fondtype < 2)) //субъекты для отчета 5
            {
                foreach (var kd in activeskinds)//виды активов
                {
                    for (int i = 1; i <= 4; i++)

                    {
                        result.AddRange(new[]
                        {
                            new AnalyzePensionplacementData() {Year = yr, Kvartal = i, Total = 1, SubjectId = sb.Id, ActivesKindId = kd.Id},

                        });
                    }
                }
            }
            _cntr.Setup(
                x =>
                    x.GetPensionplacementDataByYearKvartal(yr, qr, false))
                    .Returns(!IsReturnEmptyList ? result : new List<AnalyzePensionplacementData>());
        }

        /// <summary>
        /// Заполнение данными мок-репозитория для отчетов 6
        ///Можно вызывать несколько раз подряд для имитации запросов за разные периоды
        /// </summary>
        /// <param name="yr"></param>
        /// <param name="qr"></param>
        public void Set6Data(int yr, int? qr)
        {
            var result = new List<AnalyzeIncomingstatementsData>();
            foreach (var st in statementTypes) //типы заявлений
            {
                foreach (var ft in filingTypes)//способы подачи
                {
                    for (int i = 1; i <= 4; i++)

                    {
                        result.AddRange(new[]
                        {
                            new AnalyzeIncomingstatementsData() {Year = yr, Kvartal = i, Total = 1, FilingId = ft.Id, StatementId = st.Id},

                        });
                    }
                }
            }
            _cntr.Setup(
                x =>
                    x.GetIncomingstatementsDataByYearKvartal(yr, qr, false))
                    .Returns(!IsReturnEmptyList ? result : new List<AnalyzeIncomingstatementsData>());
        }

        /// <summary>
        /// Заполнение данными мок-репозитория для отчетов 7
        ///Можно вызывать несколько раз подряд для имитации запросов за разные периоды
        /// </summary>
        /// <param name="yr"></param>
        /// <param name="qr"></param>
        public void Set7Data(int yr, int? qr)
        {
            var result = new List<AnalyzePaymentyearrateData>();
            foreach (var st in paymentKinds) //типы заявлений
            {

                for (int i = 1; i <= 4; i++)

                {
                    result.AddRange(new[]
                    {
                            new AnalyzePaymentyearrateData() {Year = yr, Kvartal = i, Total = 1, PaymentkindId = st.Id},

                        });
                }

            }
            _cntr.Setup(
                x =>
                    x.GetPaymentyearrateDataByYearKvartal(yr, qr, false))
                    .Returns(!IsReturnEmptyList ? result : new List<AnalyzePaymentyearrateData>());
        }

    }
}
