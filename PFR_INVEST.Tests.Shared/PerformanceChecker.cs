﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PFR_INVEST.Tests.Shared
{
    public class PerformanceChecker : IDisposable
    {
        public int MaxExecutionTimeMs = 120000;
        private DateTime _mDtFrom;

        public PerformanceChecker()
        {
            _mDtFrom = DateTime.Now;
        }

        public void CheckPerformance()
        {
            CheckPerformance(MaxExecutionTimeMs);
        }

        public void CheckPerformance(int p_ms)
        {
            var dtNow = DateTime.Now;
            var diff = dtNow - _mDtFrom;
            if (diff.TotalMilliseconds > p_ms)
                Assert.Inconclusive($"Execution is too big, time is {diff.TotalMilliseconds:#.}, max time is {p_ms}");
            else
                Console.WriteLine(@"Execution time: " + diff.TotalMilliseconds);

            _mDtFrom = DateTime.Now;
        }

        public static PerformanceChecker New()
        {
            return new PerformanceChecker();
        }

        public static PerformanceChecker New(int maxTimeMS)
        {
            return new PerformanceChecker { MaxExecutionTimeMs = maxTimeMS };
        }

        public void Dispose()
        {
            this.CheckPerformance();
        }
    }
}
