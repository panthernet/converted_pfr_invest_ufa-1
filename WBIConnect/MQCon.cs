﻿using System;
using MQAX200;

namespace WBIConnect
{
    public class MQCon
    {
        //Object reference not set to an instance of an object.
        private MQAX200.MQQueue MyMqQueue;
        private MQAX200.MQQueueManager MyMqManager;
        private MQAX200.MQSession MyMqSession;
        public MQCon(String managerName)
        {
            this.MyMqSession = new MQAX200.MQSession();
            //this.MyMqSession.ExceptionThreshold = 3; 
            Object obj = this.MyMqSession.AccessQueueManager(managerName);

            if (!(obj is MQAX200.MQQueueManager))
                Console.WriteLine("Cannot connect to queue manager");

            else
            {
                //  Console.WriteLine("Yes! Got a Queue Manager.");
            }
            this.MyMqManager = (MQAX200.MQQueueManager)obj;
        }


        public void put_message(String mes, String queuename)
        {
            int OpenOptions;

            MQAX200.MQMessage Message;
            MQAX200.MQPutMessageOptions MessageOptions;
            try
            {
                Message = (MQAX200.MQMessage)this.MyMqSession.AccessMessage();
                if (MyMqManager != null)
                {
                    if (MyMqManager.IsConnected)
                    {
                        OpenOptions = (int)MQAX200.MQ.MQOO_INPUT_AS_Q_DEF + (int)MQAX200.MQ.MQOO_OUTPUT;
                        MyMqQueue = (MQAX200.MQQueue)this.MyMqManager.AccessQueue(queuename, OpenOptions, "", "", "");
                        if (MyMqQueue.IsOpen)
                            Console.WriteLine("Connected to " + MyMqManager.name + "." + MyMqQueue.name);
                    }
                }

                Message.WriteString(mes);

                MessageOptions = (MQAX200.MQPutMessageOptions)this.MyMqSession.AccessPutMessageOptions();

                MyMqQueue.ClearErrorCodes();
                MyMqQueue.Put(Message, MessageOptions);

                if (MyMqQueue.CompletionCode == 0)
                    Console.WriteLine("Put Succeeded.");

                else if (MyMqQueue.CompletionCode == 2)
                    Console.WriteLine("Put Failed (" + MyMqQueue.ReasonName + ")");
            }
            catch (Exception ex)
            {
                Console.WriteLine("error: " + ex.Message);
            }
        }
             public void put_message(String mes, String queuename, int Encoding, int Charset)
        {
            int OpenOptions;

            MQAX200.MQMessage Message;
            MQAX200.MQPutMessageOptions MessageOptions;
            try
            {
                Message = (MQAX200.MQMessage)this.MyMqSession.AccessMessage();
                if (MyMqManager != null)
                {
                    if (MyMqManager.IsConnected)
                    {
                        OpenOptions = (int)MQAX200.MQ.MQOO_INPUT_AS_Q_DEF + (int)MQAX200.MQ.MQOO_OUTPUT;
                        MyMqQueue = (MQAX200.MQQueue)this.MyMqManager.AccessQueue(queuename, OpenOptions, "", "", "");
                        if (MyMqQueue.IsOpen)
                            Console.WriteLine("Connected to " + MyMqManager.name + "." + MyMqQueue.name);
                    }
                }
                if (Charset > 0)
                {
                    Message.CharacterSet = Charset;
                }
                if (Encoding > 0)
                {
                    Message.Encoding = Encoding;
                }
                Message.WriteString(mes);
                MessageOptions = (MQAX200.MQPutMessageOptions)this.MyMqSession.AccessPutMessageOptions();
                MyMqQueue.ClearErrorCodes();
                MyMqQueue.Put(Message, MessageOptions);

                if (MyMqQueue.CompletionCode == 0)
                    Console.WriteLine("Put Succeeded.");

                else if (MyMqQueue.CompletionCode == 2)
                    Console.WriteLine("Put Failed (" + MyMqQueue.ReasonName + ")");
            }
            catch (Exception ex)
            {
                Console.WriteLine("error: " + ex.Message);
            }
        }
    }
}
