﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class EdoOdkF026Hib : EdoOdkF026
	{
        //[IgnoreDataMember]
        //[HibExtensionDataProperty]
        //public virtual ContractHib Contract { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group1Hib> F026Groups1 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group2Hib> F026Groups2 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group3Hib> F026Groups3 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group4Hib> F026Groups4 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group5Hib> F026Groups5 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group6Hib> F026Groups6 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group7Hib> F026Groups7 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group8Hib> F026Groups8 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group81Hib> F026Groups81 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group9Hib> F026Groups9 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group10Hib> F026Groups10 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group11Hib> F026Groups11 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026Group12Hib> F026Groups12 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026SubGroup1Hib> F026SubGroups1 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026SubGroup2Hib> F026SubGroups2 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F026SubGroup3Hib> F026SubGroups3 { get; set; }

	}
}
