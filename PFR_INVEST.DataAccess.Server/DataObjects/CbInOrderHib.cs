﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class CbInOrderHib : CbInOrder
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual CbOrderHib Order { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SecurityHib Security { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<CbInOrderHib> CbInOrderList { get; set; }
    }
}
