﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class OpfrRegisterHib: OpfrRegister
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<OpfrTransfer> Transfers { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Element Kind { get; set; }
    }
}
