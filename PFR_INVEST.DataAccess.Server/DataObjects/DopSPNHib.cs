﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class DopSPNHib:DopSPN
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual PortfolioHib Portfolio { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual LegalEntityHib LegalEntity { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual MonthHib Month { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual AddSPNHib AddSPN { get; set; }			
	}
}
