﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class FinregisterHib : Finregister
    {

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual FinregisterHib ParentFinregister { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual RegisterHib SelectedRegister { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual RegisterHib Register { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AccountHib CreditAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AccountHib DebitAccount { get; set; }

        

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AsgFinTr> DraftsList { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AsgFinTrExport> ExportDraftsList { get; set; }
    }
}
