﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class OfferPfrBankAccountSumHib : OfferPfrBankAccountSum
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DepClaimOfferHib Offer { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib PfrBankAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<PaymentHistory> PaymentHistory { get; set; }
    }
}
