﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class McProfitAbilityHib : McProfitAbility
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContractHib Contract { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual YearHib Year { get; set; }
    }
}
