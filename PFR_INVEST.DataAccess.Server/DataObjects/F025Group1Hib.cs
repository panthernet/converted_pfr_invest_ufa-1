﻿
namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    using PFR_INVEST.DataObjects;
    [HibData]
    public class F025Group1Hib : F025Group1
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual EdoOdkF025Hib F025 { get; set; }
    }
}
