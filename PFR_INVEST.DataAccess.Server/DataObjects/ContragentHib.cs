﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ContragentHib : Contragent
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AccountHib> Accounts { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityHib> LegalEntities { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<DepositHib> Deposits { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual StatusHib Status { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ReorganizationHib> Reorganizations { get; set; }

        public virtual string GetFullName()
        {
            var le = LegalEntities.FirstOrDefault();
            return le != null ? le.FormalizedNameFull : Name;
        }
    }
}
