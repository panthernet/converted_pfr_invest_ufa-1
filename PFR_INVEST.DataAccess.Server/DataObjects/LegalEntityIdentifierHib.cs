﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class LegalEntityIdentifierHib : LegalEntityIdentifier
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityHib> LegalEntities { get; set; }
    }
}
