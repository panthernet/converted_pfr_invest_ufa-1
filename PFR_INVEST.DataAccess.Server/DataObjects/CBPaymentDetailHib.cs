﻿using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class CBPaymentDetailHib : CBPaymentDetail
    {
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PaymentDetailHib PaymentDetail { get; set; }
    }
}
