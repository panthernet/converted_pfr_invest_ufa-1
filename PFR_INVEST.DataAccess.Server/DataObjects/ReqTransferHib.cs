﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ReqTransferHib : ReqTransfer
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SITransferHib TransferList { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Portfolio Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual KBK KBK { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccount PfrBankAccount { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<AsgFinTrHib> CommonPPList { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AsgFinTrExport> CommonExportPPList { get; set; }


		public virtual PPTransferListItem MapToPPTransfer()
		{
			var pp = new PPTransferListItem();
			(this as ReqTransfer).CopyTo(pp);
			if (this.CommonPPList.Any())
			{
				var ci = this.CommonPPList.Last();
				pp.CommonPPDate = ci.PayDate ?? this.PaymentOrderDate;
				pp.CommonPPNum = ci.PayNumber ?? this.PaymentOrderNumber;
			}
			else
			{
				pp.CommonPPDate = this.PaymentOrderDate;
				pp.CommonPPNum = this.PaymentOrderNumber;
			}
			return pp;
		}


		//nhibernate не умеет маппить унаследованные классы даже при прямом указании и приведении типа, поэтому нужно их подменять пересозданием для обновления
		public static ReqTransfer MapFromPPTransfer(PPTransferListItem ppTr)
		{
			var rt = new ReqTransfer();
			(ppTr as ReqTransfer).CopyTo(rt);
			return rt;
		}
	}
}
