﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;
using NHibernate;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DepClaimSelectParamsHib : DepClaimSelectParams
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<DepClaim2> DepClaim2List { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<DepClaimOffer> DepClaimOfferList { get; set; }


        [IgnoreDataMember]
        public virtual Status Status { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual Stock Stock { get; set; }

        public virtual void SetAuctionStatus(ISession session, Statuses status)
        {
            //session.SaveOrUpdate(a);// Too heavy, can fail 

            session.CreateQuery("update DepClaimSelectParams set AuctionStatusID = :status where ID = :id")
            .SetParameter("status", (int)status)
            .SetParameter("id", this.ID)
            .ExecuteUpdate();
        }
    }
}
