﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ERZLHib : ERZL
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual FZHib FZ { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ERZLNotifyHib> ERZLNotifiesList { get; set; }
    }
}
