﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class LegalEntityCourierHib : LegalEntityCourier
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityHib> LegalEntities { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityCourierCertificate> Certificates { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityCourierLetterOfAttorney> LetterOfAttorneys { get; set; }
        

    }
}
