﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class AddSPNHib : AddSPN
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib LegalEntity { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual MonthHib Month { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib PFRBankAccount { get; set; }
    }
}
