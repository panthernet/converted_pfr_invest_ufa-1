﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [HibData]
    public class EdoOdkF401402Hib : EdoOdkF401402
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<F401402UK> InfoEDO { get; set; }


    }
}
