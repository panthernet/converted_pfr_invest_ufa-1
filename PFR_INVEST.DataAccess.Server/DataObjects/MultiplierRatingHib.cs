﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    using System.Runtime.Serialization;

    [HibData]
    public class MultiplierRatingHib : MultiplierRating
    {
        [IgnoreDataMember]
        public virtual RatingAgencyHib RatingAgency { get; set; }

        [IgnoreDataMember]
        public virtual RatingHib Rating { get; set; }
    }
}
