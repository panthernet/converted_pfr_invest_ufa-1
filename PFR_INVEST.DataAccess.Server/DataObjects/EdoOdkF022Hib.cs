﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [Obsolete("Устаревший, использовать EdoOdkF025Hib", true)]
	[HibData]
	public class EdoOdkF022Hib : EdoOdkF022
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual ContractHib Contract { get; set; }
	}
}
