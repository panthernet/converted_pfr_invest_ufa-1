﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class KipRegisterHib : KipRegister
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual RepositoryImpExpFile ImportFile { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual RepositoryImpExpFile ExportFile { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual RepositoryImpExpFile ExportReceiptFile { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<KipLinksHib> Links { get; set; }
	}
}
