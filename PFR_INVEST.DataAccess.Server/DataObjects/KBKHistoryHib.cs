﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class KBKHistoryHib : KBKHistory
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual KBK KBK { get; set; }
    }
}
