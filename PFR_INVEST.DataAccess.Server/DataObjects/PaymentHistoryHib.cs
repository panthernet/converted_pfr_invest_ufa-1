﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class PaymentHistoryHib : PaymentHistory
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual OfferPfrBankAccountSumHib OfferPfrBankAccountSum { get; set; }
    }
}
