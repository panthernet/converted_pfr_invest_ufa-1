﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ERZLNotifyHib : ERZLNotify
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ERZLHib ERZL { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AccountHib CreditAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AccountHib DebitAccount { get; set; }
    }
}
