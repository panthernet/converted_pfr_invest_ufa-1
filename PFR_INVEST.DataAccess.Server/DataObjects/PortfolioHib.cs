﻿using System.Collections.Generic;
using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class PortfolioHib : Portfolio
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ReqTransfer> ReqTransfers { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<PortfolioPFRBankAccount> PortfolioToPFRBankAccounts { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AddSPNHib> AddSPNs { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ApsHib> Apss { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ApsHib> Aps_s { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<IncomesecHib> Incomesecs { get; set; }

        /// <summary>
        /// Это поле - реальный год из таблицы YEAR
        /// Нам просто повезло со схемой БД:)
        /// </summary>
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Year RealYear { get; set; }


        public virtual string GetRealYearName()
        {
            return RealYear == null ? PortfolioFullListItem.YEAR_NOT_SPECIFIED : RealYear.Name;
        }
    }
}
