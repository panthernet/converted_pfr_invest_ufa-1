﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ReorganizationHib : Reorganization
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContragentHib SourceContragent { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContragentHib ReceiverContragent { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ReorganizationTypeHib ReorganizationType { get; set; }
    }
}
