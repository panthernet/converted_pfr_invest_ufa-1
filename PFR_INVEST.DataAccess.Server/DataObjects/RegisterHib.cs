﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class RegisterHib : Register
    {
		[Obsolete("Use AsgFinTr portfolio instead ",true)]
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual FZHib FZ { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ApproveDocHib ApproveDoc { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ERZLHib ERZL { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual RegisterHib Return { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<FinregisterHib> FinregistersList { get; set; }
    }
}
