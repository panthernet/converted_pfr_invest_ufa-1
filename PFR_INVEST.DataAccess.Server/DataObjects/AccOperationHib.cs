﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class AccOperationHib : AccOperation
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib PFRBankAccount { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual PortfolioHib SourcePortfolio { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual PfrBankAccountHib SourcePFRBankAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual CurrencyHib Currency { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AccOperationKindHib Kind { get; set; }
    }
}
