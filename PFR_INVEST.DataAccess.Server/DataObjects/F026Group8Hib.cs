﻿
namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    using PFR_INVEST.DataObjects;
    [HibData]
    public class F026Group8Hib : F026Group8
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual EdoOdkF026Hib F026 { get; set; }
    }
}
