﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
   
	[HibData]
	public class EdoOdkF025Hib : EdoOdkF025
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual ContractHib Contract { get; set; }
        [HibExtensionDataProperty]
        public virtual IList<F025Group1Hib> F025Groups1 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group2Hib> F025Groups2 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group3Hib> F025Groups3 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group4Hib> F025Groups4 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group5Hib> F025Groups5 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group6Hib> F025Groups6 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group7Hib> F025Groups7 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group8Hib> F025Groups8 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group81Hib> F025Groups81 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group9Hib> F025Groups9 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group10Hib> F025Groups10 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group11Hib> F025Groups11 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025Group12Hib> F025Groups12 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025SubGroup1Hib> F025SubGroups1 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025SubGroup2Hib> F025SubGroups2 { get; set; }

        [HibExtensionDataProperty]
        public virtual IList<F025SubGroup3Hib> F025SubGroups3 { get; set; }
	}
}
