﻿using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class AccountHib : Account
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContragentHib Contragent { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AccountKindHib Kind { get; set; }
    }
}
