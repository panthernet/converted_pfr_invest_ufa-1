﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class MonthHib : Month
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<SIRegister> TransferRegisters { get; set; }
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AddSPNHib> AddSPNs { get; set; }
    }
}
