﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.DataAccess.Server.DataObjects.Analyze
{
    [HibData]
    public class AnalyzeYieldfundsDataHib : AnalyzeYieldfundsData
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AnalyzeYieldfundsReport AnalyzeYieldfundsReport { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SubjectSPN SubjectSPN { get; set; }
    }
}
