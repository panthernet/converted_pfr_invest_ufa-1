﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.DataAccess.Server.DataObjects.Analyze
{
    [HibData]
    public class AnalyzePensionplacementDataHib : AnalyzePensionplacementData
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ActivesKind ActivesKind { get; set; }
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AnalyzePensionplacementReport AnalyzePensionplacementReport { get; set; }
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SubjectSPN SubjectSPN { get; set; }
    }
}
