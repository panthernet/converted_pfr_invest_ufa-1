﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;
using System.Collections;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class StatusHib : Status
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<Contragent> Contragents { get; set; }
    }
}
