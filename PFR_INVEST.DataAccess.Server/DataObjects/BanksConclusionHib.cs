﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class BanksConclusionHib : BanksConclusion
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<BankConclusion> BanksList { get; set; }

    }
}
