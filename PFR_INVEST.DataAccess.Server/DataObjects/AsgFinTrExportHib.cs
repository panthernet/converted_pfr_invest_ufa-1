﻿using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class AsgFinTrExportHib : AsgFinTrExport
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual FinregisterHib Finregister { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib PFRBankAccount { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual PortfolioHib Portfolio { get; set; }
    }
}
