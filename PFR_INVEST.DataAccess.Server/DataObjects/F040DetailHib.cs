﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class F040DetailHib : F040Detail
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual EdoOdkF040 Document { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual DetailsKind DetailKind { get; set; }
	}
}
