﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Proxy;
using System.Collections;
using System.Reflection;
using Common.Caching;
using PFR_INVEST.DataObjects;
using NHibernate.Collection;

namespace PFR_INVEST.DataAccess.Server
{
	public class HibObjectsToDtoConverter
	{
        private static readonly Hashtable SInitedTypes = Hashtable.Synchronized(new Hashtable());
        private static readonly Dictionary<Type, List<PropertyInfo>> SProxyPropertiesByType = new Dictionary<Type, List<PropertyInfo>>();
        private static readonly Dictionary<Type, Type> SDataContractTypes = new Dictionary<Type, Type>();

	    public static T ConvertFromHibType<T>(object obj)
            where T: class
	    {
            var targetType = typeof(T);
	        var sourceType = obj.GetType();

            if (sourceType.GetCustomAttributes(typeof(HibDataAttribute), false).Length > 0)
            {
                if (!SInitedTypes.ContainsKey(sourceType))
                    AutoMapper.Mapper.CreateMap(sourceType, targetType);
                object dtoObject = AutoMapper.Mapper.Map(obj, sourceType, targetType);

                return (T) dtoObject;
            }
	        return null;
	    }

        public static object ConvertFromHibType(object obj)
        {
            var sourceType = obj.GetType();
            if (sourceType.GetCustomAttributes(typeof(HibDataAttribute), false).Length > 0)
            {
                var targetType = sourceType.BaseType;
                if (!SInitedTypes.ContainsKey(sourceType))
                    AutoMapper.Mapper.CreateMap(sourceType, targetType);
                object dtoObject = AutoMapper.Mapper.Map(obj, sourceType, targetType);

                return dtoObject;
            }
            return null;
        }

        public static Type GetDataContractType(Type type)
        {            
            Type res;
            if (SDataContractTypes.TryGetValue(type, out res))
                return res;

            if (type.IsAssignableFrom(typeof(IDataObjectLite)))
                res = type;
            else if (type.GetCustomAttributes(typeof(HibDataAttribute), false).Length > 0)
                res = type.BaseType;
#if WEBCLIENT
            else if (type.IsAssignableFrom(typeof(IEnumerable)))
#else
            else if (type.IsAssignableFrom(typeof(PersistentBag)))
#endif
                res = typeof(IList);

            SDataContractTypes[type] = res;

            return res;
        }

        public static object GetDtoObject(object obj, Type targetType)
        {
            if (obj == null)
                return null;

            if (obj is IDataObjectLite)
                return obj;
#if WEBCLIENT
            var bag = obj as IEnumerable;
#else
            var bag = obj as PersistentBag;
#endif
            if (bag != null)
            {
                var aSimpleList = new List<object>(bag.Cast<object>());
                return new ArrayList(aSimpleList);
            }

            var hibernateProxy = obj as INHibernateProxy;
            if (hibernateProxy != null)
            {
                var proxy = hibernateProxy;
                //не шлем объект если это не инициализированый прокси
                if (proxy.HibernateLazyInitializer.IsUninitialized == true)
                    return null;
                obj = proxy.HibernateLazyInitializer.GetImplementation();
            }

            var t = obj.GetType();

            bool bHibType = (t.GetCustomAttributes(typeof(HibDataAttribute), false).Length > 0);

            if (bHibType)
            {
                if (SInitedTypes.ContainsKey(t) == false)
                {
                    lock (SInitedTypes)
                    {
                        if (SInitedTypes.ContainsKey(t) == false)
                        {
                            AutoMapper.Mapper.CreateMap(t, t.BaseType);
                            FillProxyPropertiesMeta(t);
                            SInitedTypes.Add(t, true);
                        }
                    }
                }

                //все уже созданые объекты храняться в кеше
                /*object res = Common.Caching.InThreadAutoCacheHelper.InThreadAutoCacheObject<object, object>(
                    "HIB_OBJECTS_SERIALIZATION", obj, delegate()
                {

                    //Создание клиентского объекта
                    object dtoObject = AutoMapper.Mapper.Map(obj, t, t.BaseType);
                    FillProxyPropertiesValues(dtoObject, obj, t);
                    return dtoObject;

                });

                return res*/
                ;

                var htCache = InThreadAutoCacheHelper.InThreadAutoCacheObject<Hashtable, int>("HIB_OBJECTS_SERIALIZATION", 0, () => new Hashtable());

                if (htCache.ContainsKey(obj))
                {
                    return htCache[obj];
                }

                object dtoObject = AutoMapper.Mapper.Map(obj, t, t.BaseType);
                FillProxyPropertiesValues(dtoObject, obj, t);
                htCache[obj] = dtoObject;
                return dtoObject;
            }

            return obj;

        }

		private static void FillProxyPropertiesValues(object p_dto, object p_hib, Type p_type)
		{
            var dto = p_dto as BaseDataObject;
			if (dto == null)
				return;

            var aProps = SProxyPropertiesByType[p_type];

           // var tHib = p_hib.GetType();
            foreach (var prop in aProps)
			{
				object val = prop.GetValue(p_hib, null);
				if (val is INHibernateProxy)
					continue;

			    if ((val as AbstractPersistentCollection)?.WasInitialized == false)
			        continue;

				if (dto.ExtensionData == null)
					dto.ExtensionData = new Dictionary<string, object>();
				dto.ExtensionData[prop.Name] = val;

			}
		}


		private static void FillProxyPropertiesMeta(Type p_type)
		{
			var props = p_type.GetProperties();
            var aProxies = props.Where(p => p.GetCustomAttributes(typeof(HibExtensionDataPropertyAttribute), true).Length > 0).ToList();
		    SProxyPropertiesByType[p_type] = aProxies;
		}
	}
}
