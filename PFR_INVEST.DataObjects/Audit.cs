﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
	public class Audit : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual string UserID { get; set; }
		[DataMember]
		public virtual string OsID { get; set; }
		[DataMember]
		public virtual string HostName { get; set; }
		[DataMember]
		public virtual string EventCode { get; set; }
        [DataMember]
        public virtual string ObjectName { get; set; }
        [DataMember]
        public virtual string ObjectType { get; set; }
        [DataMember]
        public virtual string AdditionalInfo { get; set; }
        [DataMember]
		public virtual DateTime? EventDate { get; set; }
		[DataMember]
		public virtual TimeSpan? EventTime { get; set; }
		[DataMember]
		public virtual long? DocumentID { get; set; }
	}
}
