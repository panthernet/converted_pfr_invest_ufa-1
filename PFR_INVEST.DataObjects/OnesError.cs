﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    //TODO возможно не нужен уже, не исп
    public class OnesError : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long ImportID { get; set; }

        [DataMember]
        public virtual string Message { get; set; }
    }
}
