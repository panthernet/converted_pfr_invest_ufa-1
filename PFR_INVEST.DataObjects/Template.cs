﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Template : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual int UNID { get; set; }

        [DataMember]
        public virtual string FileName { get; set; }

        [DataMember]
        public virtual byte[] Body { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual long DepartmentID { get; set; }
    }
}
