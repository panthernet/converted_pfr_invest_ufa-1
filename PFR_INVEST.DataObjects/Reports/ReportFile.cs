﻿namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportFile
	{
		public enum Types
		{
			None = 0,
			Excel = 1,
			Word = 2,
			Pdf = 3,
			Image = 5
		}

		public string FileName { get; set; }
		public string Extension { get; set; }


		public Types Type { get; set; }
		public byte[] FileBody { get; set; }

		public string Error { get; set; }
		public bool HasError { get { return !string.IsNullOrEmpty(Error); } }
	}
}
