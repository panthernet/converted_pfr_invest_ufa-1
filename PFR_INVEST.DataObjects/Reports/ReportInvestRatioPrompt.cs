﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportInvestRatioPrompt
	{
		public long YearID { get; set; }
		public decimal Multiplier { get; set; }


		public Document.Types Type { get; set; }
	}
}
