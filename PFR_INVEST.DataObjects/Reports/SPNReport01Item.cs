﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
    public class SPNReport01Item
    {
        public string FName { get; set; }
        public string RegNum { get; set; }
        public decimal RSA { get; set; }
        public decimal SPNSended { get; set; }
        public decimal SCHA { get; set; }
        public decimal IncRub1 { get; set; }
        public decimal IncPerc1 { get; set; }
        public decimal IncRub2 { get; set; }
        public decimal IncPerc2 { get; set; }
        public decimal SPNSendedSum { get; set; }
        public decimal RSASum { get; set; }
        public decimal IncRubSum1 { get; set; }
        public decimal IncPercSum1 { get; set; }
        public decimal IncPercSum2 { get; set; }
        public decimal IncRubSum2 { get; set; }
        public decimal SCHASum { get; set; }
        public decimal MinVal { get; set; }
        public decimal MaxVal { get; set; }
    }
}
