﻿

namespace PFR_INVEST.DataObjects.Reports
{
    public class SPNReport20Item :SPNReportItemBase
    {
        //Q1
        public decimal StartYieldQ1 { get; set; }
        public decimal Prev12mYieldQ1 { get; set; }
        public decimal Prev3yYieldQ1 { get; set; }
        public decimal AverYieldQ1 { get; set; }
        public int CountContrQ1 { get; set; }
        //Q2   
        public decimal StartYieldQ2 { get; set; }
        public decimal Prev12mYieldQ2 { get; set; }
        public decimal Prev3yYieldQ2 { get; set; }
        public decimal AverYieldQ2 { get; set; }
        public int CountContrQ2 { get; set; }
        //Q3   
        public decimal StartYieldQ3 { get; set; }
        public decimal Prev12mYieldQ3 { get; set; }
        public decimal Prev3yYieldQ3 { get; set; }
        public decimal AverYieldQ3 { get; set; }
        public int CountContrQ3 { get; set; }
        //Q4   
        public decimal StartYieldQ4 { get; set; }
        public decimal Prev12mYieldQ4 { get; set; }
        public decimal Prev3yYieldQ4 { get; set; }
        public decimal AverYieldQ4 { get; set; }
        public int CountContrQ4 { get; set; }
    }
}
