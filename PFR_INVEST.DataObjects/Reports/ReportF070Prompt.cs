﻿namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportF070Prompt
	{
		public long YearID { get; set; }
		public int Quarter { get; set; }
		
		/// <summary>
		/// Единицы в отчёте, 1 - руб, 1000 - тыс. руб 
		/// </summary>
		public int Units { get; set; }

		public Document.Types Type { get; set; }
        public Person Person { get; set; }
    }
}
