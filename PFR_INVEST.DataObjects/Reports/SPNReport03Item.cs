﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
    public class SPNReport03Item 
    {
        public string FName { get; set; }
        public string RegNum { get; set; }
        public decimal AvgSCHA { get; set; }
        public decimal AvgSCHASum { get; set; }
    }
}
