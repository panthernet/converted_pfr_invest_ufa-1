﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
    public class SPNReportItemBase
    {
        public string FName { get; set; }
        public string RegNum { get; set; }
        public string PfName { get; set; }
        public DateTime RegDate { get; set; }
        public decimal IncomingPay { get; set; }
        public decimal OutgoingPay { get; set; }
        public decimal IncomingSum { get; set; }
        public decimal OutgoingSum { get; set; }
    }
}
