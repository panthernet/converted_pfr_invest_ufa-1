﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportIPUKPrompt
	{
		public long? ContractID { get; set; }
		public DateTime? DateFrom { get; set; }
		public DateTime? DateTo { get; set; }

		public Document.Types Type { get; set; }

		public ReportIPUKPrompt() 
		{
			Type = Document.Types.SI;
		}
	}
}
