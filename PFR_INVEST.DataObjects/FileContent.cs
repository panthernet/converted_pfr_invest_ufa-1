﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class FileContent : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual byte[] FileContentData { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }
    }
}
