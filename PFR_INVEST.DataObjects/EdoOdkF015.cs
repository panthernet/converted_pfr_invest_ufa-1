﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class EdoOdkF015 : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[IgnoreDataMember]
		public virtual decimal? NetWealthSum { get { return this.A090; } set { this.A090 = value; } }
		[DataMember]
		public virtual DateTime? ReportDate { get; set; }
		[DataMember]
		public virtual TimeSpan? ReportTime { get; set; }

		[IgnoreDataMember]
		public virtual DateTime? ReportDateTime
		{
			get
			{

                if (this.ReportDate.HasValue && this.ReportTime.HasValue)
                    return this.ReportDate.Value.Date.Add(this.ReportTime.Value);
                else if (this.ReportDate.HasValue)
                    return this.ReportDate.Value;
                else return null;
			}
		}
		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }
		[DataMember]
		public virtual string Name { get; set; }
		[DataMember]
		public virtual string INN { get; set; }
		[DataMember]
		public virtual string KPP { get; set; }
		[DataMember]
		public virtual long? ContractId { get; set; }
		[DataMember]
		public virtual decimal? A010 { get; set; }
		[DataMember]
		public virtual decimal? A020 { get; set; }
		[DataMember]
		public virtual decimal? A030 { get; set; }
		[DataMember]
		public virtual decimal? A031 { get; set; }
		[DataMember]
		public virtual decimal? A032 { get; set; }
		[DataMember]
		public virtual decimal? A033 { get; set; }
		[DataMember]
		public virtual decimal? A034 { get; set; }
		[DataMember]
		public virtual decimal? A035 { get; set; }
		[DataMember]
		public virtual decimal? A036 { get; set; }
		[DataMember]
		public virtual decimal? A037 { get; set; }
		[DataMember]
		public virtual decimal? A038 { get; set; }
		[DataMember]
		public virtual decimal? A039 { get; set; }
		[DataMember]
		public virtual decimal? A40 { get; set; }
		[DataMember]
		public virtual decimal? A41 { get; set; }
		[DataMember]
		public virtual decimal? A42 { get; set; }
		[DataMember]
		public virtual decimal? A43 { get; set; }
		[DataMember]
		public virtual decimal? A50 { get; set; }
		[DataMember]
		public virtual decimal? A60 { get; set; }
		[DataMember]
		public virtual decimal? A70 { get; set; }
		[DataMember]
		public virtual decimal? A071 { get; set; }
		[DataMember]
		public virtual decimal? A072 { get; set; }
		[DataMember]
		public virtual decimal? A073 { get; set; }
		[DataMember]
		public virtual decimal? A074 { get; set; }
		[DataMember]
		public virtual decimal? A075 { get; set; }
		[DataMember]
		public virtual decimal? A080 { get; set; }
		[DataMember]
		public virtual decimal? A090 { get; set; }
		[DataMember]
		public virtual string ApName { get; set; }
		[DataMember]
		public virtual string ApPost { get; set; }
		[DataMember]
		public virtual string RegNumberOut { get; set; }
		[DataMember]
		public virtual string RegNum { get; set; }
		[DataMember]
		public virtual DateTime? RegDate { get; set; }

	}
}
