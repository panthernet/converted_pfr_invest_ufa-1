﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ServiceItems
{
    [DataContract(IsReference = true)]
    public class SaveSIContractItem
    {
        [DataMember]
        public BankAccount BankAccount { get; set; }
        [DataMember]
        public Contract Contract { get; set; }
        [DataMember]
        public RewardCost RewardCost { get; set; }
        [DataMember]
        public ContrInvest ContrInvest { get; set; }

        [DataMember]
        public List<ContrInvestOBL> ContrInvestOBLList { get; set; }
        [DataMember]
        public List<ContrInvestAKC> ContrInvestAKCList { get; set; }
        [DataMember]
        public List<ContrInvestPAY> ContrInvestPAYList { get; set; }
    }
}
