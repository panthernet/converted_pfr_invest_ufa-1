﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ServiceItems
{
    [DataContract(IsReference = true)]
    public class SaveSIResultItem
    {
        [DataMember]
        public long ContragentId;

        [DataMember]
        public long LegalEntityId;

        [DataMember]
        public long? LicSdId;

        [DataMember]
        public long? LicDdId;

        [DataMember]
        public long? LicUkId;
    }
}
