﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ServiceItems
{
    [DataContract]
    public class PagedResult<T>
    {
        public long TotalRecords { get; set; }
        public int StartIndex { get; set; }

        public IList<T> Result { get; set; }
    }
}
