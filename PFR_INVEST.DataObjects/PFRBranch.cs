﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PFRBranch : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual long FEDO_ID { get; set; }

        [DataMember]
        public virtual string Address { get; set; }

        [DataMember]
        public virtual int? RegionNumber { get; set; }

		[DataMember]
		public virtual string INN { get; set; }

		[DataMember]
		public virtual string KPP { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        //[DataMember]
        public virtual string PassportRegionName{ get; set; }
    }
}
