﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class FEDO : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual int Special { get; set; }
    }
}
