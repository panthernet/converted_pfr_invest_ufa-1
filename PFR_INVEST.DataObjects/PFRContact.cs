﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PFRContact : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        private long _id;
        public virtual long ID { get { return _id; } set { _id = value; OnPropertyChanged("ID"); } }

        [DataMember]
        private string _fio;
        public virtual string FIO { get { return _fio; } set { _fio = value; OnPropertyChanged("FIO"); } }

        [DataMember]
        private string _post;
        public virtual string Post { get { return _post; } set { _post = value; OnPropertyChanged("Post"); } }

        [DataMember]
        private string _phone;
        public virtual string Phone { get { return _phone; } set { _phone = value; OnPropertyChanged("Phone"); } }

        [DataMember]
        private string _fax;
        public virtual string Fax { get { return _fax; } set { _fax = value; OnPropertyChanged("Fax"); } }

        [DataMember]
        private string _email;
        public virtual string Email { get { return _email; } set { _email = value; OnPropertyChanged("Email"); } }

        [DataMember]
        private long? _pfrbID;
        public virtual long? PFRB_ID { get { return _pfrbID; } set { _pfrbID = value; OnPropertyChanged("PFRB_ID"); } }

        [DataMember]
        public virtual long StatusID { get; set; }
    }
}
