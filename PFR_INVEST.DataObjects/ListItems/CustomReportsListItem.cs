﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class CustomReportsListItem
    {
        /// <summary>
        /// ID OrdReport
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// Портфель (год)
        /// </summary>
        [DataMember]
        public string PortfolioYear { get; set; }

        /// <summary>
        /// Номер поручения
        /// </summary>
        [DataMember]
        public string RegNum { get; set; }

        /// <summary>
        /// ЦБ
        /// </summary>
        [DataMember]
        private string securityName;
        public string SecurityName
        {
            get
            {
                return !string.IsNullOrEmpty(securityName) ? "Всего по " + securityName : "";
            }
            set
            {
                securityName = value;
            }
        }

        /// <summary>
        /// Дата поручения
        /// </summary>
        [DataMember]
        public DateTime? DateOrder { get; set; }

        /// <summary>
        /// Дата операции по счету ДЕПО
        /// </summary>
        [DataMember]
        public DateTime? DateDEPO { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        [DataMember]
        public long Count { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        [DataMember]
        public decimal Price { get; set; }

        /// <summary>
        /// SumNom
        /// </summary>
        [DataMember]
        public decimal SumNom { get; set; }

        /// <summary>
        /// Sum
        /// </summary>
        [DataMember]
        public decimal Sum { get; set; }

        /// <summary>
        /// SumWithoutNKD
        /// </summary>
        [DataMember]
        public decimal SumWithoutNKD { get; set; }

        /// <summary>
        /// Yield
        /// </summary>
        [DataMember]
        public decimal Yield { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        [DataMember]
        public string CurrencyName { get; set; }

        [DataMember]
        public string SecurityId { get; set; }
        [DataMember]
        public string KindName { get; set; }

        [DataMember]
        public decimal NotFadedNominal { get; set; }
        [DataMember]
        public decimal NKD { get; set; }

        [DataMember]
        public DateTime? FadeDate { get; set; }

        [DataMember]
        public long? CurrencyID { get; set; }
    }
}
