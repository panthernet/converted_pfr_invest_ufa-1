﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class OwnFundsListItem
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string Year { get; set; }

        [DataMember]
        public int? Quartal { get; set; }

        [DataMember]
        public decimal? Capital { get; set; }

        [DataMember]
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            var otherEntity = obj as OwnFundsListItem;
            if (ID == otherEntity.ID && Year == otherEntity.Year && Name == otherEntity.Name)
            {
                if (Quartal.HasValue != otherEntity.Quartal.HasValue)
                    return false;
                if (Quartal.HasValue && otherEntity.Quartal.HasValue && Quartal.Value != otherEntity.Quartal.Value)
                    return false;
                if (Capital.HasValue != otherEntity.Capital.HasValue)
                    return false;
                if (Capital.HasValue && otherEntity.Capital.HasValue && Capital.Value != otherEntity.Capital.Value)
                    return false;
            }

            return false;
        }
    }
}
