﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class MonthTransferWizardResultListItem
    {
        [DataMember]
        public virtual decimal? FactSum { get; set; }

        [DataMember]
        public virtual long? ContractID { get; set; }

        [DataMember]
        public virtual long QuantityZL { get; set; }

        [DataMember]
        public virtual decimal InvestmentIncome { get; set; }

        [DataMember]
        public virtual SIUKPlan UKPlan { get; set; }
    }
}
