﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class TempAllocationListItem
    {
        public static string TEMP_ALLOCATION_KIND_CB = "Ценные бумаги";
        public static string TEMP_ALLOCATION_KIND_DEPOSIT = "Депозиты";

        public TempAllocationListItem()
        {
            //this.OrdReport = report;
            //this.Rate = rate != null ? rate.Value : 0M;
            //this.HalfYear = DateTools.GetHalfYearInWordsForDate(this.OrdReport.DateOrder);
            //this.Quarter = DateTools.GetQarterYearInWordsForDate(this.OrdReport.DateOrder);
            //this.Month = DateTools.GetMonthInWordsForDate(this.OrdReport.DateOrder);

            IsExtra = false;
        }

        [DataMember]
        public string PortfolioYear { get; set; }

        [DataMember]
        public string Kind { get; set; }

        [IgnoreDataMember]
        public string OrderType
        {
            get
            {
                switch (OperationType)
                {
                    case enOperationType.DepositAllocate: return "размещение";
                    case enOperationType.DepositReturn: return "возврат";
                    case enOperationType.PaperBuy: return "покупка";
                    case enOperationType.PaperSell: return "продажа";

                    default: return string.Empty;
                }
            }
        }

        [DataMember]
        public int? Year { get; set; }

        [DataMember]
        public string HalfYear { get; set; }

        [DataMember]
        public string Quarter { get; set; }

        [DataMember]
        public string Month { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public string OrderRegNum { get; set; }

        [DataMember]
        public decimal? RubSumm { get; set; }

        [DataMember]
        public string CurrencyName { get; set; }

        [DataMember]
        public decimal? CurrSumm { get; set; }

        [DataMember]
        public string Comment { get; set; }

        /// <summary>
        /// Дата создания карточки
        /// </summary>
        [DataMember]
        public DateTime? DateDI { get; set; }

        public enum enOperationType
        {
            PaperSell = 11,
            PaperBuy = 12,

            DepositAllocate = 21,
            DepositReturn = 22
        }

        [DataMember]
        public enOperationType OperationType
        { get; set; }

        [DataMember]
        public long ID
        { get; set; }

        [DataMember]
        public bool IsExtra
        { get; set; }

        [IgnoreDataMember]
        public decimal? RubSummAllocate
        {
            get
            {
                switch (OperationType)
                {
                    case enOperationType.DepositAllocate:
                    case enOperationType.PaperBuy:
                        return RubSumm;
                    default: return null;
                }
            }
        }

        [IgnoreDataMember]
        public decimal? RubSummReturn
        {
            get
            {
                switch (OperationType)
                {
                    case enOperationType.DepositReturn:
                    case enOperationType.PaperSell:
                        return RubSumm;
                    default: return null;
                }
            }
        }

		[DataMember]
		public decimal? RubNKD { get;set;}

		[DataMember]
		public decimal? RubWithoutNKD { get; set; }


		[IgnoreDataMember]
		public decimal? RubNKDAllocate
		{
			get
			{
				switch (OperationType)
				{
					//case enOperationType.DepositAllocate:
					case enOperationType.PaperBuy:
						return RubNKD;
					default: return null;
				}
			}
		}

		[IgnoreDataMember]
		public decimal? RubNKDReturn
		{
			get
			{
				switch (OperationType)
				{
					//case enOperationType.DepositReturn:
					case enOperationType.PaperSell:
						return RubNKD;
					default: return null;
				}
			}
		}

		[IgnoreDataMember]
		public decimal? RubWithoutNKDAllocate
		{
			get
			{
				switch (OperationType)
				{
					//case enOperationType.DepositAllocate:
					case enOperationType.PaperBuy:
						return RubWithoutNKD;
					default: return null;
				}
			}
		}

		[IgnoreDataMember]
		public decimal? RubWithoutNKDReturn
		{
			get
			{
				switch (OperationType)
				{
					//case enOperationType.DepositReturn:
					case enOperationType.PaperSell:
						return RubWithoutNKD;
					default: return null;
				}
			}
		}
		
    }
}
