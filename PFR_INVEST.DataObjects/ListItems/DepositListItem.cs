﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DepositListItem : Deposit, IDataErrorInfo
    {
        public DepositListItem() : base() { }

        public DepositListItem(Deposit d, DepClaimOffer dco, DepClaim2 dc2, Element e)
            : this()
        {
            ID = d.ID;
            PortfolioID = null;
            PortfolioName = null;

            AccNum = null;
            Status = d.Status;
            StatusName = d.Status == null ? string.Empty : DepositIdentifier.GetDepositStatusText((long)d.Status);

            LocationDate = dc2.SettleDate;
            InterestVolume = dc2.Payment;

            BankName = dc2.FirmName;
            RegNum = dco.Number.ToString();
            LocationVolume = dc2.Amount;
            DepositYield = dc2.Rate;
            ReturnDate = dc2.ReturnDate;
            TermLocation = ReturnDate.Value.Date.Subtract(LocationDate.Value.Date).Days;

            ReturnDateActual = d.ReturnDateActual ?? dc2.ReturnDate;


            Comment = e == null ? null : e.Name;

            IsExtra = false;
        }

        public DepositListItem(long dID, long? dStatus, DateTime? dReturnDateActual,
                                long dcoNumber, 
                                DepClaim2 dc2, 
                                string eName, decimal? planPercSum)
            : this()
        {
            ID = dID;
            PortfolioID = null;
            PortfolioName = null;

            AccNum = null;
            Status = dStatus;
            StatusName = !dStatus.HasValue ? string.Empty : DepositIdentifier.GetDepositStatusText((long)dStatus);

            LocationDate = dc2.SettleDate;
            InterestVolume = !planPercSum.HasValue || planPercSum == 0 ? dc2.Payment : planPercSum.Value;

            BankName = dc2.FirmName;
            RegNum = dcoNumber.ToString();
            LocationVolume = dc2.Amount;
            DepositYield = dc2.Rate;
            ReturnDate = dc2.ReturnDate;
            TermLocation = ReturnDate.Value.Date.Subtract(LocationDate.Value.Date).Days;

            ReturnDateActual = dReturnDateActual ?? dc2.ReturnDate;


            Comment = eName;

            IsExtra = false;
        }


        [DataMember]
        public virtual bool IsReturn { get; set; }

        [DataMember]
        public virtual bool IsPercent { get; set; }


        [IgnoreDataMember]
        public DateTime? OperationDate
        {
            get { return IsReturn ? ReturnDate : LocationDate; }
        }

        [IgnoreDataMember]
        public decimal? AllocateVoulume
        {
            get { return IsReturn || IsPercent ? null : LocationVolume; }
        }


		[DataMember]
		public virtual decimal? ReturnSum { get; set; }

        [IgnoreDataMember]
        public decimal? ReturnVolume
        {
			get { return (IsReturn && !IsPercent) ? ReturnSum : null; }
        }

        [IgnoreDataMember]
        public virtual decimal? InterestVolumeFix
        {
			get { return IsPercent ? ReturnSum : null; } //InterestVolume
        }

        [IgnoreDataMember]
        public virtual DateTime? LocationDateFix
        {
            get { return (IsReturn && IsExtra) ? null : LocationDate; }
        }

        [DataMember]
        public virtual string Portfolio
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public virtual string PortfolioName
        {
            get { return Portfolio; }
            set { Portfolio = value; }
        }

        [DataMember]
        public virtual string Bank
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public virtual string BankName
        {
            get { return Bank; }
            set { Bank = value; }
        }

        [DataMember]
        public virtual string StatusName
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public virtual string StatusText
        {
            get { return StatusName; }
        }

        [IgnoreDataMember]
        public virtual string Year
        {
            get { return OperationDate == null ? "-" : OperationDate.Value.ToString("yyyy"); }
        }

        [IgnoreDataMember]
        public virtual int HalfYear
        {
            get
            {
                if (OperationDate == null)
                    return -1;
                if (OperationDate.Value.Month <= 6)
                    return 1;
                return 2;
            }
        }

        [IgnoreDataMember]
        public virtual string HalfYearText
        {
            get
            {
                switch (HalfYear)
                {
                    case 1: return "I полугодие";
                    case 2: return "II полугодие";
                    default: return null;
                }
            }
        }

        [IgnoreDataMember]
        public virtual int Quarter
        {
            get
            {
                if (OperationDate == null)
                    return -1;
                switch (OperationDate.Value.Month)
                {
                    case 1:
                    case 2:
                    case 3: return 1;
                    case 4:
                    case 5:
                    case 6: return 2;
                    case 7:
                    case 8:
                    case 9: return 3;
                    case 10:
                    case 11:
                    case 12: return 4;
                }

                throw new ArgumentException();
            }
        }

        [IgnoreDataMember]
        public virtual string QuarterText
        {
            get
            {
                switch (Quarter)
                {
                    case 1: return "I квартал";
                    case 2: return "II квартал";
                    case 3: return "III квартал";
                    case 4: return "IV квартал";
                    default: return null;
                }
            }
        }

        [IgnoreDataMember]
        public virtual string Month
        {
            get { return OperationDate == null ? "-" : OperationDate.Value.ToString("MMMM"); }
        }

        [IgnoreDataMember]
        public virtual string Day
        {
            get { return OperationDate == null ? "-" : OperationDate.Value.ToShortDateString(); }
        }

        [DataMember]
        public virtual string Comment
        {
            get;
            set;
        }

        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        public bool PaintCell
        {
            get { return ReturnDate <= DateTime.Now; }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    //case "RegNum":
                    //    if (string.IsNullOrEmpty(RegNum)) return "Это поле обязательное.";
                    //    break;
                    case "AccNum":
                        if (string.IsNullOrEmpty(AccNum)) return "Это поле обязательное.";
                        break;
                    case "LocationVolume":
                        if (!LocationVolume.HasValue) return "Это поле обязательное.";
                        if (LocationVolume.HasValue && LocationVolume.Value < 0) return "Неправильное значение.";
                        break;
                    case "DepositYield":
                        if (!DepositYield.HasValue) return "Это поле обязательное.";
                        if (DepositYield.HasValue && (DepositYield < 0)) return "Неправильное значение.";
                        break;
                }
                return null;
            }
        }

        /// <summary>
        /// Сумма процентов
        /// </summary>
        [DataMember]
        public virtual decimal? InterestVolume { get; set; }

        [IgnoreDataMember]
        public virtual decimal? TotalVolume
        {
            get
            {
                if (InterestVolume == null)
                    return LocationVolume;

                if (LocationVolume == null)
                    return InterestVolume;

                return InterestVolume + LocationVolume;
            }
        }


        /// <summary>
        /// "dc2.Rate"
        /// </summary>
        [DataMember]
        public virtual decimal? DepositYield { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        /// <summary>
        /// вычисляется по датам
        /// </summary>
        [DataMember]
        public virtual long? TermLocation { get; set; }

        /// <summary>
        /// Номер счета
        /// </summary>
        [DataMember]
        public virtual string AccNum { get; set; }


        /// <summary>
        /// Дублирование Depclaim2.Ammount
        /// </summary>
        [Obsolete("dc2.Amount (местами s.Sum) Не заполняется, нужно избавляться")]
        [DataMember]
        public virtual decimal? LocationVolume { get; set; }

        [Obsolete("dc2.SettleDate, нужно избавляться")]
        [DataMember]
        public virtual DateTime? LocationDate { get; set; }

        [Obsolete("dc2.ReturnDate, нужно избавляться")]
        [DataMember]
        public virtual DateTime? ReturnDate { get; set; }

        /// <summary>
        /// Дублирование Depclaim_Offer.Number
        /// </summary>
        [Obsolete("надо избавляться")]
        [DataMember]
        public virtual string RegNum { get; set; }


        [DataMember]
        public virtual bool IsExtra { get; set; }

    }
}
