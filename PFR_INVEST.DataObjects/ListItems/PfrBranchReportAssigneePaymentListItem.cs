﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PfrBranchReportAssigneePaymentListItem
    {
        public PfrBranchReportAssigneePaymentListItem(PfrBranchReportAssigneePayment ap, PfrBranchReport r, PFRBranch b, FEDO f)
        {
            ID = ap.ID;
            ReportId = ap.ReportId;

            TotalPayment = ap.TotalPayment;
            OwnReservePayment = ap.OwnReservePayment;
            PFRReservePayment = ap.PFRReservePayment;
            
            ReportRegionName = b.Name;
            ReportYear = r.PeriodYear;
            ReportMonth = "";
            if (r.PeriodMonth != null)
            {
                ReportMonth = DateTools.GetMonthInWordsForDate(r.PeriodMonth);
            }

            ReportFedoName = f.Name;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public long ReportId { get; set; }

        [DataMember]
        public virtual decimal TotalPayment { get; set; }

        [DataMember]
        public virtual decimal OwnReservePayment { get; set; }

        [DataMember]
        public virtual decimal PFRReservePayment { get; set; }
        
        [DataMember]
        public string ReportMonth { get; set; }

        [DataMember]
        public int ReportYear { get; set; }

        [DataMember]
        public string ReportRegionName { get; set; }

        [DataMember]
        public string ReportFedoName { get; set; }
    }
}
