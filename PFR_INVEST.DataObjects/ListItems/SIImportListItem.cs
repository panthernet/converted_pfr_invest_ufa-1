﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SIImportListItem: IKnownType
    {
        [DataMember]
        public virtual string UK { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual decimal? SumToUK { get; set; }

        [DataMember]
        public virtual decimal? SumFromUKTotal { get; set; }

        [DataMember]
        public virtual decimal? SpnFromUK { get; set; }

        [DataMember]
        public virtual decimal? InvestIncome { get; set; }

        [DataMember]
        public virtual int ZLCount { get; set; }

        [DataMember]
        public virtual bool IsMismatched { get; set; }
    }
}
