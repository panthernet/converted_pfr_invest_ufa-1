﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	using System.Collections.Generic;

	[DataContract]
	public class PortfolioFullListItem : BaseDataObject
	{
		public static readonly PortfolioFullListItem Empty = new PortfolioFullListItem();

		public PortfolioFullListItem()
			: base()
		{
		}


		public PortfolioFullListItem(Portfolio portfolio, LegalEntity bank, PfrBankAccount bankAcc, AccBankType accountType, PortfolioPFRBankAccount portfolioPFRBankAccount)
			: this(portfolio, bank, bankAcc, accountType, portfolioPFRBankAccount, null)
		{
		}

		public PortfolioFullListItem(Portfolio portfolio, LegalEntity bank, PfrBankAccount bankAcc, AccBankType accountType, PortfolioPFRBankAccount portfolioPFRBankAccount, Year year)
		{
			ID = portfolioPFRBankAccount != null ? portfolioPFRBankAccount.ID : 0;
			Portfolio = portfolio;
			BankLE = bank;
			PfrBankAccount = bankAcc;
			AccountType = accountType;
			RealYear = year;
			PortfolioPFRBankAccount = portfolioPFRBankAccount;
		}

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual AccBankType AccountType { get; set; }

		[DataMember]
		public virtual Portfolio Portfolio { get; set; }

		[DataMember]
		public virtual LegalEntity BankLE { get; set; }

		[DataMember]
		public virtual Year RealYear { get; set; }

		[DataMember]
		public virtual PfrBankAccount PfrBankAccount { get; set; }

		[DataMember]
		public virtual PortfolioPFRBankAccount PortfolioPFRBankAccount { get; set; }

		// нужны для совместимости со старым итемом.(около 20-30 форм)
		[IgnoreDataMember]
		public virtual string AccountNumber
		{
			get { return this.PfrBankAccount != null ? this.PfrBankAccount.AccountNumber : null; }
			set { if (this.PfrBankAccount != null) this.PfrBankAccount.AccountNumber = value; }
		}

		[IgnoreDataMember]
		public virtual long AccountID
		{
			get { return this.PfrBankAccount.ID; }
			set { this.PfrBankAccount.ID = value; }
		}

		[IgnoreDataMember]
		public virtual long PortfolioID
		{
			get { return this.Portfolio.ID; }
			set { this.Portfolio.ID = value; }
		}

		[IgnoreDataMember]
		public virtual Portfolio.Types PFType
		{
			get { return this.Portfolio != null ? this.Portfolio.TypeEl : Portfolio.Types.NoType; }
			set { if (this.Portfolio != null) this.Portfolio.TypeEl = value; }
		}

		[IgnoreDataMember]
		public virtual string PFTypeText
		{
			get { return this.Portfolio != null ? this.Portfolio.Type : string.Empty; }			
		}

		[IgnoreDataMember]
		public virtual string Bank
		{
			get { return this.BankLE != null ? this.BankLE.FormalizedName : null; }
			set { if (this.BankLE != null) this.BankLE.FormalizedName = value; }
		}

		[IgnoreDataMember]
		public virtual string PFName
		{
			get { return this.Portfolio != null ? this.Portfolio.Year : null; }
			set { if (this.Portfolio != null) this.Portfolio.Year = value; }
		}

		public const string YEAR_NOT_SPECIFIED = "Год не указан";
		[IgnoreDataMember]
		public virtual string Year
		{
			get { return this.RealYear != null ? this.RealYear.Name : YEAR_NOT_SPECIFIED; }
			set { if (this.RealYear != null) this.RealYear.Name = value; }
		}

	}
}
