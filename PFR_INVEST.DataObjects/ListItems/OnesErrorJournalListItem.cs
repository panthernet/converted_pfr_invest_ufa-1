﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    public class OnesErrorJournalListItem: OnesFileImport
    {
        [DataMember]
        public virtual int LogType { get; set; }

        [DataMember]
        public virtual string Message { get; set; }

        [DataMember]
        public virtual string LogTypeName {
            get
            {
                return LogType == (int)OnesJournal.LogTypes.Info ? "ИНФО" : "ОШИБКА";
            }
        }

    }
}
