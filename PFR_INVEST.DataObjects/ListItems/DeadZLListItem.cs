﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DeadZLListItem
    {
        private string[] months;

        /// <summary>
        /// ID ERZLNotify
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// ID ERZL
        /// </summary>
        [DataMember]
        public long? ERZL_ID { get; set; }

        /// <summary>
        /// ID контрагента
        /// </summary>
        [DataMember]
        public long ContragentID { get; set; }

        /// <summary>
        /// Наименование контрагента
        /// </summary>
        [DataMember]
        public string NPFName { get; set; }

        /// <summary>
        /// Количество ЗЛ
        /// </summary>
        [DataMember]
        public long? Count { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Месяц
        /// </summary>
        [DataMember]
        public string Month
        {
            get
            {
                months = "Январь.Февраль.Март.Апрель.Май.Июнь.Июль.Август.Сентябрь.Октябрь.Ноябрь.Декабрь".Split('.');
                return this.Date != null &&
                       this.Date != DateTime.MinValue ? this.months[this.Date.Value.Month - 1] : "";
            }

            set
            {
            }
        }

        /// <summary>
        /// Год
        /// </summary>
        [DataMember]
        public string Year
        {
            get
            {
                return this.Date != null &&
                       this.Date != DateTime.MinValue &&
                       this.Date.HasValue ? this.Date.Value.Year.ToString() : "";
            }

            set
            {
            }
        }

        /// <summary>
        /// ID кред. счета
        /// </summary>
        [DataMember]
        public long CregitAccID { get; set; }

        /// <summary>
        /// ID деб. счета
        /// </summary>
        [DataMember]
        public long DebetAccID { get; set; }

    }
}
