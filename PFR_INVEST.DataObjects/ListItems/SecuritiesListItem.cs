﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SecuritiesListItem
    {
        /// <summary>
        /// сущность Security 
        /// </summary>
        [DataMember]
        public Security Security { get; set; }

        /// <summary>
        /// сущность Currency 
        /// </summary>
        [DataMember]
        public SecurityKind SecurityKind { get; set; }

        /// <summary>
        /// сущность Currency 
        /// </summary>
        [DataMember]
        public Currency Currency { get; set; }

        /// <summary>
        /// ID цб 
        /// </summary>
        [DataMember]
        public long? ID 
        {
            get
            {
                return this.Security.ID;
            }

            set
            {
               
            }
        }

        /// <summary>
        /// Выпуск цб
        /// </summary>
        [DataMember]
        public string Name 
        {
            get
            {
                if (this.Security != null)
                    return this.Security.Name;
                else
                    return null;
            }

            set
            {
  
            }
        }
    }
}
