﻿
namespace PFR_INVEST.DataObjects.ListItems
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class AllocationRequestMaxSumPlacingListItem : BaseDataObject
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        private int _DepclaimselectparamsId { get; set; }
        public virtual int DepclaimselectparamsId
        {
            get { return _DepclaimselectparamsId; }
            set { _DepclaimselectparamsId = value; }
        }

        [DataMember]
        private DateTime _DocDate { get; set; }
        public virtual DateTime DocDate
        {
            get { return _DocDate; }
            set { _DocDate = value; }
        }

        [DataMember]
        private DateTime _DocTime { get; set; }
        public virtual DateTime DocTime
        {
            get { return _DocTime; }
            set { _DocTime = value; }
        }

        [DataMember]
        private DateTime _TradeDate { get; set; }
        public virtual DateTime TradeDate
        {
            get { return _TradeDate; }
            set { _TradeDate = value; }
        }

        [DataMember]
        private string _BoardId { get; set; }
        public virtual string BoardId
        {
            get { return _BoardId; }
            set { _BoardId = value; }
        }

        [DataMember]
        private int _Auctno { get; set; }
        public virtual int Auctno
        {
            get { return _Auctno; }
            set { _Auctno = value; }
        }

        [DataMember]
        private string _SecurityId { get; set; }
        public virtual string SecurityId
        {
            get { return _SecurityId; }
            set { _SecurityId = value; }
        }

        [DataMember]
        private decimal _Rate { get; set; }
        public virtual decimal Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }

        [DataMember]
        private decimal _Totval1 { get; set; }
        public virtual decimal Totval1
        {
            get { return _Totval1; }
            set { _Totval1 = value; }
        }
        [DataMember]
        private decimal _Totval1N { get; set; }
        public virtual decimal Totval1N
        {
            get { return _Totval1N; }
            set { _Totval1N = value; }
        }
        [DataMember]
        private decimal _Warate { get; set; }
        public virtual decimal Warate
        {
            get { return _Warate; }
            set { _Warate = value; }
        }

        [DataMember]
        private DateTime _SettleDate { get; set; }
        public virtual DateTime SettleDate
        {
            get { return _SettleDate; }
            set { _SettleDate = value; }
        }

        [DataMember]
        private DateTime _SettleDate2 { get; set; }
        public virtual DateTime SettleDate2
        {
            get { return _SettleDate2; }
            set { _SettleDate2 = value; }
        }

        [DataMember]
        private int _QuantPart { get; set; }
        public virtual int QuantPart
        {
            get { return _QuantPart; }
            set { _QuantPart = value; }
        }

        [DataMember]
        private int _QuantReq { get; set; }
        public virtual int QuantReq
        {
            get { return _QuantReq; }
            set { _QuantReq = value; }
        }



    }
}
