﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class F080DetailsListItem : BaseDataObject
	{
		public F080DetailsListItem() { }
		public F080DetailsListItem(F80Deal F080, DateTime? ReportOnDate, long? lContractNumbers)
		{
			this.F080 = F080;
            this.reportOnDate = ReportOnDate;
            this.lContractNumbers = lContractNumbers;
		}

        [DataMember]
        public virtual DateTime? reportOnDate { get; set; }
        [DataMember]
		public virtual long? lContractNumbers { get; set; }
		[DataMember]
		public virtual F80Deal F080 { get; set; }
    
        [IgnoreDataMember]
        public virtual long ID { get { return this.F080 == null ? 0 : this.F080.ID; } }
        [IgnoreDataMember]
        public virtual int? ReportYear { get { return !this.reportOnDate.HasValue ? null : (int?)this.reportOnDate.Value.Year; } }
        [IgnoreDataMember]
        public virtual string ReportOnDate { get { return this.reportOnDate.HasValue ? string.Format("{0}\t({1})", this.reportOnDate.Value.Date.ToShortDateString(), this.lContractNumbers.HasValue ? this.lContractNumbers.Value.ToString() : string.Empty) : string.Empty; } }
        [IgnoreDataMember]
        public virtual string UKName { get { return this.F080 == null ? null : this.F080.UKName; } }
        [IgnoreDataMember]
        public virtual string OperKind { get { return this.F080 == null ? null : this.F080.OperKind; } }
        [IgnoreDataMember]
        public virtual string ContractNumber { get { return this.F080 == null ? null : this.F080.ContractNumber; } }
        [IgnoreDataMember]
        public virtual string FactPrice { get { return this.F080 == null ? null : this.F080.FactPrice.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string MarketPrice { get { return this.F080 == null ? null : this.F080.MarketPrice.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string PriceDeviation { get { return this.F080 == null ? null : this.F080.PriceDeviation.Value.ToString("N2"); } }
    }
}
