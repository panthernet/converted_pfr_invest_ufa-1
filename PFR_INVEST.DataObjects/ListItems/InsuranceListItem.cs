﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class InsuranceListItem
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string LegalEntity { get; set; }

        [DataMember]
        public string RegNum { get; set; }

        [DataMember]
        public decimal Sum { get; set; }

        [DataMember]
        public decimal TotalInmanagement { get; set; }

        [IgnoreDataMember]
        public decimal RequiedInsuranceSum
        {
            get
            {
                if (TotalInmanagement >= 6000000000 || TotalInmanagement == 0)
                    return 300000000;
                else
                    return TotalInmanagement * 0.05M;
            }
        }

        [IgnoreDataMember]
        public decimal Discrepancy
        {
            get
            {
                return Sum - RequiedInsuranceSum;
            }
        }

        [IgnoreDataMember]
        public string Status
        {
            get
            {
                return Discrepancy >= 0 ? "Страховая сумма достаточна" : "Страховая сумма не достаточна";
            }
        }

        [DataMember]
        public DateTime CloseDate { get; set; }
    }
}
