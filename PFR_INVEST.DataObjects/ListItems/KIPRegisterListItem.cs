﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class KIPRegisterListItem:KipRegister
	{
		[DataMember]
		public RepositoryImpExpFile ImportFile { get; private set; }
		[DataMember]
		public RepositoryImpExpFile ExportFile { get; private set; }
		[DataMember]
		public RepositoryImpExpFile ExportReceiptFile { get; private set; }

		public KIPRegisterListItem(KipRegister register, RepositoryImpExpFile import, RepositoryImpExpFile export, RepositoryImpExpFile exportReceipt) 
		{
			register.CopyTo(this);
			this.ImportFile = import;
			this.ExportFile = export;
			this.ExportReceiptFile = exportReceipt;
		}
	}
}
