﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PaymentHistoryItem
    {
        [DataMember]
        public PaymentHistory Payment;

        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public string BankName { get; set; }

        [DataMember]
        public string PortfolioName { get; set; }


        [IgnoreDataMember]
        public string Currency { get { return CurrencyIdentifier.RUBLES; } }

        [IgnoreDataMember]
        public decimal Rate { get { return 1; } }

        [IgnoreDataMember]
        public string IncomeKind { get { return IncomeSecIdentifier.s_DepositsPercents; } }

    }
}