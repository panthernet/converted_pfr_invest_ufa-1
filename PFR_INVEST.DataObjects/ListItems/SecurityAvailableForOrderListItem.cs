﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SecurityAvailableForOrderListItem
    {
        public SecurityAvailableForOrderListItem(bool p_forBuyOrder)
        {
            ForBuyOrder = p_forBuyOrder;
        }

        [DataMember]
        public virtual long SecurityID
        {
            get
            {
                if (Security != null)
                    return Security.ID;
                else
                    return 0;
            }
            set { }
        }

        [DataMember]
        public virtual Security Security { get; set; }

        [DataMember]
        public virtual long AvailableCount { get; set; }

        [DataMember]
        public virtual bool ForBuyOrder { get; set; }

        [DataMember]
        public virtual string DisplayName
        {
            get
            {
                string secName = Security != null ? Security.Name : string.Empty;
                return ForBuyOrder ? secName : string.Format("{0} - {1}", secName, AvailableCount);
            }
            set { }
        }
    }
}
