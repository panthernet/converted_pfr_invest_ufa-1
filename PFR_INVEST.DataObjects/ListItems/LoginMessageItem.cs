﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    public class LoginMessageItem
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Title { get; set; }
        
        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public bool Ignore { get; set; }

        [DataMember]
        public long refID { get; set; }		

    }
}
