﻿using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Constants.Identifiers;
using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SIListItem
    {
#if WEBCLIENT
        /// <summary>
        /// Обяаательное поле идентификатор для веб-клиента
        /// </summary>
        public long ID { get; set; }

        public string ContragentImage =>
            ContragentStatusId == StatusIdentifier.Identifier.Active.ToLong() ? "~/Content/Images/greenlight.png" : "~/Content/Images/redlight.png";

#endif

        [DataMember]
        public virtual long LegalEntityID { get; set; }

        [DataMember]
        public virtual long ContactID { get; set; }

        [DataMember]
        public virtual string ContactFIO { get; set; }

        [DataMember]
        public virtual string ContactTel { get; set; }

        [DataMember]
        public virtual string ContactEMail { get; set; }

        [DataMember]
        public virtual string ContragentName { get; set; }

        [DataMember]
        public virtual long ContragentStatusId { get; set; }
      
        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual string FormalizedName { get; set; }

        [DataMember]
        public virtual string HeadFullName { get; set; }

        [DataMember]
        public virtual string LegalAddress { get; set; }

        [DataMember]
        public virtual string PostalAddress { get; set; }

        [DataMember]
        public virtual string Phone { get; set; }

        [DataMember]
        public virtual string EMail { get; set; }

        [DataMember]
        public virtual DateTime? CloseDate { get; set; }
    }
}
