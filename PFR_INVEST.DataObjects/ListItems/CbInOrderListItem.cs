﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class CbInOrderListItem
    {
        /// <summary>
        /// сущность CbInOrder 
        /// </summary>
        [DataMember]
        public CbInOrder CbInOrder { get; set; }

        /// <summary>
        /// сущность Security 
        /// </summary>
        [DataMember]
        public Security Security { get; set; }

        /// <summary>
        /// сущность Currency 
        /// </summary>
        [DataMember]
        public Currency Currency { get; set; }

        /// <summary>
        /// ID CbInOrder 
        /// </summary>
        [DataMember]
        public long ID
        {
            get
            {
                return CbInOrder.ID;
            }
            private set { }
        }

        /// <summary>
        /// ID поручения
        /// </summary>
        [DataMember]
        public long? OrderID
        {
            get
            {
                return CbInOrder.OrderID;
            }

            set
            {
                CbInOrder.OrderID = value;
            }
        }

        /// <summary>
        /// Выпуск цб
        /// </summary>
        [DataMember]
        public string SecurityName
        {
            get
            {
                if (Security != null)
                    return Security.Name;
                else
                    return null;
            }

            set
            {

            }
        }

        /// <summary>
        /// Общая стоимость
        /// </summary>
        [DataMember]
        public decimal? NomValue
        {
            get
            {
                return CbInOrder.NomValue;
            }

            set
            {
                CbInOrder.NomValue = value;
            }
        }

        /// <summary>
        /// Макс. общая стоимость ЦБ (с НКД)
        /// </summary>
        [DataMember]
        public decimal? MaxNomValue
        {
            get
            {
                return CbInOrder.MaxNomValue;
            }

            set
            {
                CbInOrder.MaxNomValue = value;
            }
        }

        /// <summary>
        /// Мин. общая стоимость ЦБ (с НКД)
        /// </summary>
        [DataMember]
        public decimal? MinValueNKD
        {
            get
            {
                return CbInOrder.MinValueNKD;
            }

            set
            {
                CbInOrder.MinValueNKD = value;
            }
        }

        /// <summary>
        /// Макс. цена (в %, без НКД)
        /// </summary>
        [DataMember]
        public decimal? MaxPrice
        {
            get
            {
                return CbInOrder.MaxPrice;
            }

            set
            {
                CbInOrder.MaxPrice = value;
            }
        }

        /// <summary>
        /// Мин. цена (в %, без НКД)
        /// </summary>
        [DataMember]
        public decimal? MinPrice
        {
            get
            {
                return CbInOrder.MinPrice;
            }

            set
            {
                CbInOrder.MinPrice = value;
            }
        }

        /// <summary>
        /// Количество
        /// </summary>
        [DataMember]
        public long? Count
        {
            get
            {
                return CbInOrder.Count;
            }

            set
            {
                CbInOrder.Count = value;
            }
        }

        /// <summary>
        /// Сумма денежных средств, руб
        /// </summary>
        [DataMember]
        public decimal? SumMoney
        {
            get
            {
                return CbInOrder.SumMoney;
            }

            set
            {
                CbInOrder.SumMoney = value;
            }
        }

        /// <summary>
        /// Фикс. цена покупки (в %)
        /// </summary>
        [DataMember]
        public decimal? FixPriceBay
        {
            get
            {
                return CbInOrder.FixPriceBay;
            }

            set
            {
                CbInOrder.FixPriceBay = value;
            }
        }

        /// <summary>
        /// Макс. цена покупки (в %)
        /// </summary>
        [DataMember]
        public decimal? MaxPriceBay
        {
            get
            {
                return CbInOrder.MaxPriceBay;
            }

            set
            {
                CbInOrder.MaxPriceBay = value;
            }
        }

        /// <summary>
        /// Мин. цена продажи (в %)
        /// </summary>
        [DataMember]
        public decimal? MinPriceSell
        {
            get
            {
                return CbInOrder.MinPriceSell;
            }

            set
            {
                CbInOrder.MinPriceSell = value;
            }
        }

    }
}
