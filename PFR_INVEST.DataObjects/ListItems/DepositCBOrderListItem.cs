﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DepositCBOrderListItem: BaseDataObject
    {
        [DataMember]
        public virtual string DepositContract { get; set; }
        [DataMember]
        public virtual string BankFullName { get; set; }
        [DataMember]
        public virtual string BankINN { get; set; }
        [DataMember]
        public virtual int SecurityCount { get; set; }
        [DataMember]
        public virtual decimal Sum { get; set; }
        [DataMember]
        public virtual decimal CouponIncome { get; set; }
        [DataMember]
        public virtual decimal Percent { get; set; }
        [DataMember]
        public virtual DateTime SettleDate { get; set; }
        [DataMember]
        public virtual DateTime ReturnDate { get; set; }
        [DataMember]
        public virtual string Periodity { get; set; }
        [DataMember]
        public virtual string BankShortName { get; set; }
    }
}