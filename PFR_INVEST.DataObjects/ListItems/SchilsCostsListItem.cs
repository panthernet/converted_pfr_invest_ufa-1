﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SchilsCostsListItem
    {
        [DataContract]
        public enum SchilsCostsListItemOpertion1Type
        {
            [EnumMember]
            Budget = 0,
            [EnumMember]
            Direction = 1
        }

        [DataContract]
        public enum SchilsCostsListItemOpertion2Type
        {
            [EnumMember]
            BudgetCreation = 0,
            [EnumMember]
            BudgetCorrection = 1,
            [EnumMember]
            Direction = 2,
            [EnumMember]
            Return = 3,
            [EnumMember]
            Transfer = 4
        }


        /// <summary>
        /// ID записи (уточнения, перечисления или возврата)
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// ID Бюджета
        /// </summary>
        [DataMember]
        public long BudgetID { get; set; }

        /// <summary>
        /// ID Распоряжения (Валиден только для перечисления или возврата)
        /// </summary>
        [DataMember]
        public long DirectionID { get; set; }


        /// <summary>
        /// ID Распоряжения, нужен для грида
        /// </summary>
        [DataMember]
        public long? ParentDirectionID { get; set; }

        /// <summary>
        /// Номер Распоряжения (Валиден только для перечисления или возврата)
        /// </summary>
        [DataMember]
        public string DirectionNumber { get; set; }

        /// <summary>
        /// Портфель
        /// </summary>
        [DataMember]
        public string Portfolio { get; set; }

        /// <summary>
        /// Тип операции 1
        /// </summary>
        [DataMember]
        public SchilsCostsListItemOpertion1Type Operation1Type { get; set; }

        /// <summary>
        /// Операция 1
        /// </summary>
        [DataMember]
        public string Operation1
        {
            get
            {
                if (Operation1Type == SchilsCostsListItemOpertion1Type.Budget)
                    return "Бюджет";
                if (Operation1Type == SchilsCostsListItemOpertion1Type.Direction)
                    return "Распоряжение " + DirectionNumber;
                return null;
            }
            private set { }
        }

        /// <summary>
        /// Тип операции 2
        /// </summary>
        [DataMember]
        public SchilsCostsListItemOpertion2Type Operation2Type { get; set; }

        /// <summary>
        /// Операция 2
        /// </summary>
        [DataMember]
        public string Operation2
        {
            get
            {
                if (Operation2Type == SchilsCostsListItemOpertion2Type.BudgetCorrection)
                    return "Уточнение";
                if (Operation2Type == SchilsCostsListItemOpertion2Type.BudgetCreation)
                    return "Создание";
                if (Operation2Type == SchilsCostsListItemOpertion2Type.Direction)
                    if (Operation3Type == SchilsCostsListItemOpertion2Type.Transfer)
                        //return "Распоряжение " + DirectionNumber; 
                        return "";
                    else
                        return "Распоряжение " + DirectionNumber; 
                if (Operation2Type == SchilsCostsListItemOpertion2Type.Transfer)
                    return "Перечисление";
                if (Operation2Type == SchilsCostsListItemOpertion2Type.Return)
                    return "Возврат";
                return null;
            }
            private set { }
        }

        /// <summary>
        /// Тип операции 3
        /// </summary>
        [DataMember]
        public SchilsCostsListItemOpertion2Type Operation3Type { get; set; }

        /// <summary>
        /// Операция 2
        /// </summary>
        [DataMember]
        public string Operation3
        {
            get
            {
                if (Operation3Type == SchilsCostsListItemOpertion2Type.BudgetCorrection)
                    return "Уточнение";
                if (Operation3Type == SchilsCostsListItemOpertion2Type.BudgetCreation)
                    return "Создание";
                if (Operation3Type == SchilsCostsListItemOpertion2Type.Direction)
                    return "Распоряжение";
                if (Operation3Type == SchilsCostsListItemOpertion2Type.Transfer)
                    return "Перечисление";
                if (Operation3Type == SchilsCostsListItemOpertion2Type.Return)
                    return "Возврат";
                return null;
            }
            private set { }
        }

        /// <summary>
        /// Номер
        /// </summary>
        [DataMember]
        public string Number { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Бюджет
        /// </summary>
        [DataMember]
        public decimal? Budget { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        [DataMember]
        public decimal? Sum { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [DataMember]
        public string Comment { get; set; }
    }
}
