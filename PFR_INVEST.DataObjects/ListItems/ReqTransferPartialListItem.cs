﻿using PFR_INVEST.DataObjects.Interfaces;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ReqTransferPartialListItem: IKnownType
    {
        [DataMember]
        public string LegalEntityName { get; set; }

        [DataMember]
        public long LegalEntityID { get; set; }

        [DataMember]
        public long ReqTransferID { get; set; }

        [DataMember]
        public decimal SumDiff { get; set; }

        [DataMember]
        public long ContractID { get; set; }
    }
}
