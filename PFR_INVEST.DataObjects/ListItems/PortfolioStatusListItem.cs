﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PortfolioStatusListItem
    {
        /// <summary>
        /// ID CbInOrder 
        /// </summary>
        [DataMember]
        public long? ID { get; set; }

        [DataMember]
        public string SecurityID { get; set; }

        /// <summary>
        /// Портфель (год)
        /// </summary>
        [DataMember]
        public string PortfolioYear { get; set; }

        /// <summary>
        /// Название валюты
        /// </summary>
        [DataMember]
        public string CurrencyName { get; set; }

        /// <summary>
        /// Вид ЦБ
        /// </summary>
        [DataMember]
        public string Kind { get; set; }

        /// <summary>
        /// Номер выпуска
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Кол-во ЦБ
        /// </summary>
        [DataMember]
        public long Count { get; set; }

        /// <summary>
        /// Объем по номиналу
        /// </summary>
        [DataMember]
        public decimal VolumeByNominal { get; set; }

        /// <summary>
        /// сумма с нкд
        /// </summary>
        [DataMember]
        public decimal SummNKD { get; set; }

        /// <summary>
        /// нкд
        /// </summary>
        [DataMember]
        public decimal NKD { get; set; }

        /// <summary>
        /// сумма без нкд
        /// </summary>
        [DataMember]
        public decimal SummWithoutNKD { get; set; }

        /// <summary>
        /// % от номинала
        /// </summary>
        [DataMember]
        public decimal NominalPercent { get; set; }

        /// <summary>
        /// Средневзв. доходность
        /// </summary>
        [DataMember]
        public decimal MidWealth { get; set; }

        [DataMember]
        public decimal MidPrice { get; set; }

        [DataMember]
        public DateTime? FadeDate { get; set; }

        [DataMember]
        public decimal SumRoubles { get; set; }
    }
}
