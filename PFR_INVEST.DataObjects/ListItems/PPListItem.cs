﻿using System;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class PPListItem : BaseDataObject
	{
		

		public PPListItem() { }

        public PPListItem(AsgFinTr transfer, string portfolio, long portfolioStatus)
            : this(transfer, portfolio, portfolioStatus, string.Empty)
        { }

		public PPListItem(AsgFinTr transfer, string portfolio, long portfolioStatus, string paymentDetails)
		{
			ID = transfer.ID;
			Portfolio = portfolio;
		    PortfolioStatus = portfolioStatus;
			Date = transfer.DraftPayDate;
			Direction = (AsgFinTr.Directions?)transfer.DirectionElID;
			PPNumber = transfer.DraftRegNum;
			PPDate = transfer.DraftDate;
			Sum = transfer.DraftAmount;
            PaymentDetails = paymentDetails;
		    Link = AsgFinTr.Links.NoLink;
		    if (transfer.FinregisterID != null)
		    {
		        Link = AsgFinTr.Links.NPF;
		    }
            else if (transfer.ReqTransferID != null)
            {
                Link = AsgFinTr.Links.UK;
            }
            else if (transfer.LinkElID != null)
            {
                Link = (AsgFinTr.Links) transfer.LinkElID;
            }
			SectionID = transfer.SectionElID;
		}

	    public long PortfolioStatus { get; set; }

	    public long ID { get; set; }
		public string Portfolio { get; set; }
		public int? Year => Date?.Year;
	    public DateTime? Date { get; set; }

		public AsgFinTr.Directions? Direction { get; set; }
		public string PPNumber { get; set; }
		public DateTime? PPDate { get; set; }
		public decimal? Sum { get; set; }
		public string PaymentDetails { get; set; }

		public AsgFinTr.Links Link { get; set; }

		public long? DirectionID => (long?)Direction;
	    public long LinkID => (long)Link;
	    public long? SectionID { get; set; } 
		public decimal? SumSaldo => (Direction == AsgFinTr.Directions.ToPFR) ? Sum : -Sum;
	}
}
