﻿
using System.Runtime.Serialization;
namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class AuctionRepositoryLinkItem
	{
		/// <summary>
		/// REPOSITORY_IMPEXP_FILE.ID
		/// </summary>
		[DataMember]
		public long RepositoryID { get; set; }

		/// <summary>
		/// REPOSITORY_IMPEXP_FILE.KEY
		/// </summary>
		[DataMember]
		public long RepositoryKey { get; set; }

		/// <summary>
		/// DEPCLAIMSELECTPARAMS.ID
		/// </summary>
		[DataMember]
		public long AuctionID { get; set; }
	}
}
