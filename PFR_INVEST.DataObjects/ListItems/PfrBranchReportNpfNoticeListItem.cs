﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PfrBranchReportNpfNoticeListItem
    {
        public PfrBranchReportNpfNoticeListItem(PfrBranchReportNpfNotice nn, LegalEntity le, PfrBranchReport r, PFRBranch b, FEDO f)
        {
            ID = nn.ID;
            ReportId = nn.BranchReportID;
            LegalEntityID = nn.LegalEntityID;

            NoticeTotal = nn.NoticeTotal;
            ContractTotal = nn.ContractTotal;
            NoticeNew = nn.NoticeNew;
            ContractNew = nn.ContractNew;
            NoticeExisting = nn.NoticeExisting;
            ContractExisting = nn.ContractExisting;
            NoticeToNpf = nn.NoticeToNpf;
            ContractToNpfReturned = nn.ContractToNpfReturned;
            ContractPtkSpu = nn.ContractPtkSpu;

            ReportRegionName = b.Name;
            ReportYear = r.PeriodYear;
            ReportMonth = "";
            if (r.PeriodMonth != null)
            {
                ReportMonth = DateTools.GetMonthInWordsForDate(r.PeriodMonth);
            }
            NpfName = le.FormalizedName;

            ReportFedoName = f.Name;
        }

        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public long ReportId { get; set; }
        
        [DataMember]
        public virtual long LegalEntityID { get; set; }

        [DataMember]
        public virtual long NoticeTotal { get; set; }
        [DataMember]
        public virtual long ContractTotal { get; set; }

        [DataMember]
        public virtual long NoticeNew { get; set; }
        [DataMember]
        public virtual long ContractNew { get; set; }

        [DataMember]
        public virtual long NoticeExisting { get; set; }
        [DataMember]
        public virtual long ContractExisting { get; set; }

        [DataMember]
        public virtual long NoticeToNpf { get; set; }
        [DataMember]
        public virtual long ContractToNpfReturned { get; set; }

        [DataMember]
        public virtual long ContractPtkSpu { get; set; }

        [DataMember]
        public string ReportMonth { get; set; }

        [DataMember]
        public int ReportYear { get; set; }

        [DataMember]
        public string ReportRegionName { get; set; }

        [DataMember]
        public string NpfName { get; set; }

        [DataMember]
        public string ReportFedoName { get; set; }
    }
}
