﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ReqTransferWithStatsListItem : BaseDataObject
    {
        public ReqTransferWithStatsListItem(ReqTransfer req, string op, string status)
        {
            ReqTransfer = req;
            OperationContent = op;
            Status = status;
        }

        [DataMember]
        public ReqTransfer ReqTransfer { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string OperationContent { get; set; }
    }
}
