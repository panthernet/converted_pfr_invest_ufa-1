﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract(IsReference = true)]
    public class AccOpListItem: IKnownType
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual decimal Summ { get; set; }

        [DataMember]
        public virtual decimal Curs { get; set; }

        public virtual decimal FinalSumm => Summ*Curs;

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }
    }
}
