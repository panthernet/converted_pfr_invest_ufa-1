﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    public class TemplateListItem : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Department { get; set; }

        /// <summary>
        /// Тип ЦБ
        /// </summary>
        [DataMember]
        public virtual long DepartmentID { get; set; }

        /// <summary>
        /// Наименование ЦБ
        /// </summary>
        [DataMember]
        public virtual int UNID { get; set; }

        /// <summary>
        /// Номер выпуска
        /// </summary>
        [DataMember]
        public virtual string FileName { get; set; }

        /// <summary>
        /// Количество ЦБ
        /// </summary>
        [DataMember]
        public virtual DateTime? Date { get; set; }
    }
}
