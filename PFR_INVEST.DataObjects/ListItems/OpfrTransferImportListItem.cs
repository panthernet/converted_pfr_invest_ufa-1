﻿namespace PFR_INVEST.DataObjects.ListItems
{
    public class OpfrTransferImportListItem
    {
        public string Opfr { get; set; }

        public decimal Kosgu261 { get; set; }

        public decimal Kosgu221 { get; set; }

        public decimal Kosgu226 { get; set; }

        public decimal? Sum
        {
            get
            {

                var sum = Kosgu221 + Kosgu226 + Kosgu261;
                return sum == 0 ? null : (decimal?) sum;
            }
        }

        public long BranchID { get; set; }
    }
}
