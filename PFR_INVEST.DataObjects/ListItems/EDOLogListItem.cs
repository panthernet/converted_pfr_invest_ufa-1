﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class EDOLogListItem:BaseDataObject
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public DateTime? LoadDate { get; set; }

        [DataMember]
        public DateTime? RegDate { get; set; }

        [DataMember]
        public string DocRegNum { get; set; }

        [DataMember]
        public long? DocID { get; set; }

        [DataMember]
        public string DocForm { get; set; }

        [DataMember]
        public string UKName { get; set; }

        [DataMember]
        public string ContractNum { get; set; }

        [DataMember]
        public string DocRegNumberOut { get; set; }

        [DataMember]
        public long? DocBodyId { get; set; }

        [DataMember]
        public string Exception { get; set; }

        [DataMember]
        public string Filename { get; set; }

        //Для удаления
        [IgnoreDataMember]
        public bool Selected { get; set; }
    }
}
