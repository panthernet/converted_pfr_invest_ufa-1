﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class F040DetailsListItem : BaseDataObject
	{
		public F040DetailsListItem() { }

        public F040DetailsListItem(F040DetailsListItemLite item)
        {
            this.Detail = new F040Detail(item.Detail);
            this.Year = item.Year;
            this.month = item.Month;
            this.ContractNumber = item.ContractNumber;
            this.UKName = item.UKName;
            this.DocumentsCount = item.DocumentsCount;
        }

        public F040DetailsListItem(F040Detail detail, string year, string month, string contractNumber, string ukName, string ukOldName, long? lDocumentsCount)
        {
            this.Detail = detail;
            this.Year = year;
            this.month = month;
            this.ContractNumber = contractNumber;
            this.UKName = LegalEntity.GetFormalizedNameFull(ukName, ukOldName);
            this.DocumentsCount = lDocumentsCount;
        }

        [IgnoreDataMember]
		public virtual long? EdoID { get { return this.Detail != null ? this.Detail.EdoID : null; } }
        [IgnoreDataMember]
        public virtual string Month
        {           
            get
            {
                return this.month;
            }

            //get { return string.Format("{0}\t({1})", this.month.PadRight(8, ' '), DocumentsCount.HasValue ? DocumentsCount.Value.ToString() : string.Empty); }
        }

		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual string ContractNumber { get; set; }
		[DataMember]
		public virtual string UKName { get; set; }
		[DataMember]
		public virtual string Year { get; set; }
        [DataMember]
        public virtual string month { get; set; }
		[DataMember]
		public virtual F040Detail Detail { get; set; }

        [IgnoreDataMember]
        public virtual List<F040Detail> Details { get; set; }

        [DataMember]
        public virtual long? DocumentsCount { get; set; }

        [DataMember]
        public virtual long? BuyCount { get; set; }
        [DataMember]
        public virtual decimal? BuyAmount { get; set; }
        [DataMember]
        public virtual long? SellCount { get; set; }
        [DataMember]
        public virtual decimal? SellAmount { get; set; }
	}

    [DataContract]
    public class F040DetailsListItemLite : IDataObjectLite
    {
        public F040DetailsListItemLite() { }
        
        public F040DetailsListItemLite(F040DetailLite detail, string year, string month, string contractNumber, string ukName, string ukOldName, long? lDocumentsCount)
        {
            this.Detail = detail;
            this.Year = year;
            this.month = month;
            this.ContractNumber = contractNumber;
            this.UKName = LegalEntity.GetFormalizedNameFull(ukName, ukOldName);
            this.DocumentsCount = lDocumentsCount;
        }

        [IgnoreDataMember]
        public long? EdoID { get { return this.Detail != null ? this.Detail.EdoID : null; } }
        [IgnoreDataMember]
        public string Month
        {
            get
            {
                return this.month;
            }

            //get { return string.Format("{0}\t({1})", this.month.PadRight(8, ' '), DocumentsCount.HasValue ? DocumentsCount.Value.ToString() : string.Empty); }
        }

        [DataMember(Name = "A")]
        public long ID { get; set; }
        [DataMember(Name = "B")]
        public string ContractNumber { get; set; }
        [DataMember(Name = "C")]
        public string UKName { get; set; }
        [DataMember(Name = "D")]
        public string Year { get; set; }
        [DataMember(Name = "E")]
        public string month { get; set; }
        [DataMember(Name = "F")]
        public F040DetailLite Detail { get; set; }

        [DataMember(Name = "G")]
        public long? DocumentsCount { get; set; }
    }
}
