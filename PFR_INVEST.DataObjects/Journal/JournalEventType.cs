﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Journal
{
    /// <summary>
    /// Типы событий журнала
    /// </summary>
    [DataContract(Namespace = "http://www.pfr.ru/types/")]
    public enum JournalEventType
    {
        [EnumMember]
        [Description("Просмотр")]
        SELECT = 0,
        [EnumMember]
        [Description("Создание")]
        INSERT = 1,
        [EnumMember]
        [Description("Изменение")]
        UPDATE = 2,
        [EnumMember]
        [Description("Удаление")]
        DELETE = 3,
        [EnumMember]
        [Description("Авторизация")]
        AUTHORIZATION = 4,
        [EnumMember]
        [Description("Выход из системы")]
        LOGOUT = 5,
        [EnumMember]
        [Description("Удаление(Постановка в архив)")]
        ARCHIVE = 6,
        [EnumMember]
        [Description("Откат шага")]
        ROLLBACK = 7,
		[EnumMember]
        [Description("Смена состояния")]
        CHANGE_STATE = 8,
        [EnumMember]
        [Description("Просмотр действий пользователей")]
        SHOW_JOURNAL_LOG = 9,
        [EnumMember]
        [Description("Просмотр отчета по действиям пользователей")]
        FILTER_JOURNAL_LOG = 10,
        [EnumMember]
        [Description("Назначение/снятие просмотр ролей")]
        ROLES_CHANGE = 11,
        [EnumMember]
        [Description("Просмотр пользователей")]
        USERS_CHANGES = 12,
        [EnumMember]
        [Description("Добавление пользователей")]
        USERS_ADDED = 13,
        [EnumMember]
        [Description("Удаление пользователей")]
        USERS_DELETED = 14,
        [EnumMember]
        [Description("Импорт данных")]
        IMPORT_DATA = 15,
        [EnumMember]
        [Description("Экспорт данных")]
        EXPORT_DATA = 16,
        [EnumMember]
        [Description("Восcтановление данных")]
        RESTORE_DATA = 17,
    }
}
