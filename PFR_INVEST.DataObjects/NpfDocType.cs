﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class NpfDocType : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string DocType { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }
    }
}
