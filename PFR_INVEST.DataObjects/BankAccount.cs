﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class BankAccount : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? PfrBranchID { get; set; }

        [DataMember]
        public virtual string LegalEntityName { get; set; }

        [DataMember]
        public virtual string AccountNumber { get; set; }

        [DataMember]
        public virtual string BankName { get; set; }

        [DataMember]
        public virtual string BankLocation { get; set; }

        [DataMember]
        public virtual string CorrespondentAccountNumber { get; set; }

        [DataMember]
        public virtual string CorrespondentAccountNote { get; set; }

        [DataMember]
        public virtual string BIK { get; set; }

        [DataMember]
        public virtual string INN { get; set; }

        [DataMember]
        public virtual string KPP { get; set; }

		[DataMember]
		public virtual long? LegalEntityID { get; set; }

        [DataMember]
        public virtual DateTime? CloseDate { get; set; }

        [DataMember]
        public virtual string FormalizedName { get; set; }
       
        [IgnoreDataMember]
        public virtual string CorrespondentAccountFull => string.Join(" ", new [] {CorrespondentAccountNumber, CorrespondentAccountNote}).Trim();

        public virtual bool IsEqual(BankAccount to)
        {
            return ID == to.ID && PfrBranchID == to.PfrBranchID && LegalEntityName == to.LegalEntityName && AccountNumber == to.AccountNumber && BankName == to.BankName
                   && BankLocation == to.BankLocation && CorrespondentAccountNumber == to.CorrespondentAccountNumber && CorrespondentAccountNote == to.CorrespondentAccountNote
                   && BIK == to.BIK && INN == to.INN && KPP == to.KPP && LegalEntityID == to.LegalEntityID && CloseDate == to.CloseDate && FormalizedName == to.FormalizedName;

        }
    }
}
