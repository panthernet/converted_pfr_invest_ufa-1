﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
    public class EdoOdkF060 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? ContractId { get; set; }

        [DataMember]
        public virtual long? YearId { get; set; }

        [DataMember]
        public virtual int? Quartal { get; set; }

        [DataMember]
        public virtual string InvestCaseName { get; set; }

        [DataMember]
        public virtual string Kpp { get; set; }


        [DataMember]
        public virtual decimal? SchaStart1 { get; set; }

        [DataMember]
        public virtual decimal? SchaStart2 { get; set; }

        [DataMember]
        public virtual decimal? PaymentReceived1 { get; set; }

        [DataMember]
        public virtual decimal? PaymentReceived2 { get; set; }

        [DataMember]
        public virtual decimal? PaymentTransfer1 { get; set; }

        [DataMember]
        public virtual decimal? PaymentTransfer2 { get; set; }

        [DataMember]
        public virtual decimal? Profit1 { get; set; }

        [DataMember]
        public virtual decimal? Profit2 { get; set; }

        [DataMember]
        public virtual decimal? Detained1 { get; set; }

        [DataMember]
        public virtual decimal? Detained2 { get; set; }

        [DataMember]
        public virtual decimal? DetainedUKReward1 { get; set; }

        [DataMember]
        public virtual decimal? DetainedUKReward2 { get; set; }

        [DataMember]
        public virtual decimal? SchaEnd1 { get; set; }

        [DataMember]
        public virtual decimal? SchaEnd2 { get; set; }

        [DataMember]
        public virtual string RegNumberOut { get; set; }

        [DataMember]
        public virtual string RegNumber { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }
    }
}
