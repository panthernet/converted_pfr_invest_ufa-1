﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportApplication741 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long AppealDistribution { get; set; }

        [DataMember]
        public virtual long AppealPaymentSummary { get; set; }

        [DataMember]
        public virtual long AppealPaymentCourt { get; set; }

        [DataMember]
        public virtual long AppealRefuse { get; set; }

        [DataMember]
        public virtual long ResolutionPayment { get; set; }

        [DataMember]
        public virtual long ResolutionPaymentAdditional { get; set; }

        [DataMember]
        public virtual long ResolutionPaymentRefuse { get; set; }
        
        [DataMember]
        public virtual long ReportId { get; set; }
    }
}

