﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class EdoOdkF040 : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual string ApName { get; set; }
		[DataMember]
		public virtual string ApPost { get; set; }
		[DataMember]
		public virtual string RegNum { get; set; }
		[DataMember]
		public virtual DateTime? RegDate { get; set; }
		[DataMember]
		public virtual long? ReportType { get; set; }
		[DataMember]
		public virtual DateTime? Writedate { get; set; }		
		[DataMember]
		public virtual long? IdOndate { get; set; }
		[DataMember]
		public virtual long? IdYear { get; set; }
		[DataMember]
		public virtual long? ContractId { get; set; }
		[DataMember]
		public virtual string RegNumberOut { get; set; }
		[DataMember]
		public virtual string Year { get; set; }
		[DataMember]
		public virtual string Month { get; set; }

        [DataMember]
        public virtual long? CustomBuyCount { get; set; }
        [DataMember]
        public virtual decimal? CustomBuyAmount { get; set; }
        [DataMember]
        public virtual long? CustomSellCount { get; set; }
        [DataMember]
        public virtual decimal? CustomSellAmount { get; set; }
        [DataMember]
        public virtual string Portfolio { get; set; }
    }
}
