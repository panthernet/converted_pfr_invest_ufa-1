﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OfferPfrBankAccountSum : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long OfferId { get; set; }

        [DataMember]
        public virtual long PortfolioId { get; set; }

        [DataMember]
        public virtual long PfrBankAccountId { get; set; }

        [DataMember]
        public virtual decimal Sum { get; set; }

        [DataMember]
        public virtual decimal? PlannedSum { get; set; }

        [IgnoreDataMember]
        public virtual bool HasPlannedSum => PlannedSum.HasValue;

        [IgnoreDataMember]
        public virtual string SumString => Sum.ToString("n2");

        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [IgnoreDataMember]
        public virtual Portfolio.Types Type => (Portfolio.Types) TypeValue;

        [DataMember]
        public virtual long TypeValue { get; set; }

        /// <summary>
        /// Устанавливается после обработки запроса на сервисе
        /// </summary>
        [DataMember]
        public virtual string TypeText { get; set; }

        [DataMember]
        public virtual string Year { get; set; }

        [DataMember]
        public virtual string AccountNum { get; set; }

        [IgnoreDataMember]
        public virtual bool IsDirty { get; set; }
    }
}
