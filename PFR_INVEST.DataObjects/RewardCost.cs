﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class RewardCost : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? ContractID { get; set; }

        [DataMember]
        public virtual decimal? MaxCost { get; set; }

        [DataMember]
        public virtual decimal? MaxPaySD { get; set; }

        [DataMember]
        public virtual decimal? AmountReward { get; set; }

        [DataMember]
        public virtual DateTime? ChDate { get; set; }

        [DataMember]
        public virtual string EndCh { get; set; }
    }
}
