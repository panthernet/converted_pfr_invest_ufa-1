﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReport : BaseDataObject
    {
        public PfrBranchReport()
            : base()
        {
            StatusID = 1;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long BranchID { get; set; }

        [DataMember]
        public virtual long ReportTypeID { get; set; }

        [DataMember]
        public virtual int PeriodYear { get; set; }

        [DataMember]
        public virtual int? PeriodMonth { get; set; }

        [DataMember]
        public virtual DateTime  CreatedDate { get; set; }

        [DataMember]
        public virtual string FileName { get; set; }

        [IgnoreDataMember]
        public virtual PfrBranchReportType.BranchReportType ReportType
        {
            get { return (PfrBranchReportType.BranchReportType)ReportTypeID; }
        }

        [IgnoreDataMember]
        public virtual PFRBranch Branch { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }
    }
}
