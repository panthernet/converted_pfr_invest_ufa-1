﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using PFR_INVEST.DataObjects.Interfaces;

    public class F026Group4 : BaseDataObject, IF026Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string SecurityClassification { get; set; }
		[DataMember]
		public virtual string StateRegNum { get; set; }
		[DataMember]
		public virtual decimal? AcquisitionPrice { get; set; }
		[DataMember]
		public virtual decimal? Quantity { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }		
	}
}
