﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{

    public class AccountKind : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }
        
        [DataMember] 
        public virtual string Measure { get; set; }
    }
}
