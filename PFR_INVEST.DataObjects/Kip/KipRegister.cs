﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	[DataContract]
	public class KipRegister : BaseDataObject
	{
		public enum ExportStatuses
		{
			Unknown = 0,
			Exported = 1,
			ExportedAndRejected = -1,
			ExportedAndAccepted = 2
		}


		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual long? ImportFileID { get; set; }

		[DataMember]
		public virtual long? ExportFileID { get; set; }

		[DataMember]
		public virtual long? ExportReceiptID { get; set; }


		[DataMember]
		public virtual long? SIRegisterID { get; set; }

		[DataMember]
		public virtual long? VRRegisterID { get; set; }

		[DataMember]
		public virtual long? NPFRegisterID { get; set; }

		


		[DataMember]
		public virtual long? ExportStatusID { get; set; }

		[IgnoreDataMember]
		public virtual ExportStatuses? ExportStatus
		{
			get { return (ExportStatuses?)ExportStatusID; }
			set { ExportStatusID = (long?)value; }
		}

		[IgnoreDataMember]
		public virtual string ExportStatusText 
		{
			get 
			{
				switch (ExportStatus) 
				{
					case null:
					case ExportStatuses.Unknown:
						return "не экспортировалось";
					case ExportStatuses.Exported:
						return "экспортировано, успешно передано в КИП";
					case ExportStatuses.ExportedAndRejected:
						return "экспортировано, пришла квитанция с ошибками";
					case ExportStatuses.ExportedAndAccepted:
						return "экспортировано, пришла квитанция";
					default:
						return string.Empty;
				}
			}
		}

		[DataMember]
		public virtual string DocNum { get; set; }

		[DataMember]
		public virtual string ExportDocNum { get; set; }

		[DataMember]
		public virtual string ReceiptDocNum { get; set; }

		[DataMember]
		public virtual string DocType { get; set; }

		[DataMember]
		public virtual DateTime DocDate { get; set; }


		[DataMember]
		public virtual DateTime Period { get; set; }

		[DataMember]
		public virtual string PeriodType { get; set; }

		
	}
}
