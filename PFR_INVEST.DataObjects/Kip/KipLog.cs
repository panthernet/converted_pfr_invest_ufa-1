﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class KipLog : BaseDataObject
	{

		public enum ErrorLevels
		{
			NoError = 0,	//0	Без ошибки
			CanNotRead = 1, //1	Невозможно прочитать файл
			//2	Ошибка аутентификации
			Logical = 3,		//3	Ошибка ФЛК
			Communication = 4 //4 Ошибки связи
		}

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual long? FileRepositoryID { get; set; }

		[DataMember]
		public virtual long? ReceiptRepositoryID { get; set; }

		[DataMember]
		public virtual DateTime Date { get; set; }

		[DataMember]
		public virtual TimeSpan Time { get; set; }

		[DataMember]
		public virtual long ErrorLevelCode { get; set; }

		[DataMember]
		public virtual string Meassage { get; set; }

		[IgnoreDataMember]
		public virtual DateTime LogTime
		{
			get { return Date.Add(Time); }
		}

		[IgnoreDataMember]
		public virtual ErrorLevels ErrorLevel
		{
			get { return (ErrorLevels)ErrorLevelCode; }
			set { ErrorLevelCode = (long)value; }
		}


		public KipLog()
		{
			Date = DateTime.Now;
			Time = DateTime.Now.TimeOfDay;
		}
	}
}
