﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
	public class AuditEvent:BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual string Code { get; set; }
		[DataMember]
		public virtual string Name { get; set; }
	}
}
