﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class ListPropertyCondition
    {
        public ListPropertyCondition()
        {
            Operation = "eq";
        }

        public ListPropertyCondition(string name, string operation, object[] values)
        {
            Operation = operation;
            Name = name;
            Values = values;
        }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public object Value { get; set; }

        [DataMember]
        public object[] Values { get; set; }

        /// <summary>
        /// Valid values are: "eq", "like", "neq", "in", "ge", "le", "gt", "lt", "IsNull", "IsNotNull"
        /// </summary>

        [DataMember]
        public string Operation { get; set; }

        public static ListPropertyCondition GreaterThan(string fieldName, object value)
        {
            return new ListPropertyCondition() { Name = fieldName, Value = value, Operation = "gt" };
        }

        public static ListPropertyCondition GreaterEqThan(string fieldName, object value)
        {
            return new ListPropertyCondition() { Name = fieldName, Value = value, Operation = "ge" };
        }


        public static ListPropertyCondition LessThan(string fieldName, object value)
        {
            return new ListPropertyCondition() { Name = fieldName, Value = value, Operation = "lt" };
        }

        public static ListPropertyCondition LessEqThan(string fieldName, object value)
        {
            return new ListPropertyCondition() { Name = fieldName, Value = value, Operation = "le" };
        }

        public static ListPropertyCondition In(string fieldName, object[] value)
        {
            return new ListPropertyCondition() { Name = fieldName, Values = value, Operation = "in" };
        }

        public static ListPropertyCondition Equal(string fieldName, object value)
        {
            return new ListPropertyCondition() { Name = fieldName, Value = value, Operation = "eq" };
        }

        public static ListPropertyCondition NotEqual(string fieldName, object value)
        {
            return new ListPropertyCondition() { Name = fieldName, Value = value, Operation = "neq" };
        }

        public static ListPropertyCondition IsNull(string fieldName)
        {
            return new ListPropertyCondition() { Name = fieldName, Operation = "isnull" };
        }

        public static ListPropertyCondition IsNotNull(string fieldName)
        {
            return new ListPropertyCondition() { Name = fieldName, Operation = "isnotnull" };
        }
    }



}
