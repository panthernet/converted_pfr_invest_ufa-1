﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Cost : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? DIDate { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual string Kind { get; set; }

        [DataMember]
        public virtual long? AccountID { get; set; }

        [DataMember]
        public virtual long? CurrencyID { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        public virtual decimal? Sum { get; set; }

        [DataMember]
        public virtual decimal? Total { get; set; }

        [DataMember]
        public virtual DateTime? PaymentDate { get; set; }

        [DataMember]
        public virtual string PaymentNumber { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual decimal? PaymentSumm { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual long? BankAccountID { get; set; }
    }
}

