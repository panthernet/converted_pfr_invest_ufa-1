﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{ 
    public class Account
        : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }
        
        [DataMember] 
        public virtual string Name { get; set; }
        
        [DataMember]
        public virtual long? ContragentID { get; set; }
        
        [DataMember]
        public virtual long? AccountTypeID { get; set; }
    }
}
