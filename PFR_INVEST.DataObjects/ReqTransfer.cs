﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects
{
	[DataContract(IsReference = true)]
    public class ReqTransfer : BaseDataObject, IMarkedAsDeleted
    {
        public ReqTransfer()
        {
            DIDate = DateTime.Today;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? TransferListID { get; set; }

        [DataMember]
        public virtual decimal? Sum { get; set; }

        [DataMember]
        public virtual decimal? SumBeforeOperation { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        public virtual string PaymentOrderNumber { get; set; }

        [DataMember]
		[Obsolete("use Sum only", true)]
        public virtual decimal? PaymentOrderSum { get; set; }

        [DataMember]
        public virtual DateTime? PaymentOrderDate { get; set; }

        [DataMember]
        public virtual string RequestNumber { get; set; }

        [DataMember]
        public virtual string ClaimNumber { get; set; }

        [DataMember]
        public virtual DateTime? RequestCreationDate { get; set; }

        [DataMember]
        public virtual DateTime? RequestFormingDate { get; set; }

        [DataMember]
        public virtual DateTime? RequestDeliveryDate { get; set; }

        [DataMember]
        public virtual DateTime? SetSPNTransferDate { get; set; }

        [DataMember]
        public virtual DateTime? SPNDebitDate { get; set; }

        [DataMember]
        public virtual string ExecutionControl { get; set; }

        [DataMember]
        public virtual string TransferActNumber { get; set; }

        [DataMember]
        public virtual DateTime? TransferActDate { get; set; }

        [DataMember]
        public virtual DateTime? DIDate { get; set; }

        [DataMember]
        public virtual string TransferStatus { get; set; }

        [DataMember]
        public virtual decimal? InvestmentIncome { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual long? PFRBankAccountID { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual long? KBKID { get; set; }

        
        /// <summary>
        /// Количество ЗЛ на начало операции
        /// </summary>
        [DataMember]        
        public virtual long? ZLStartCount { get; set; }

        /// <summary>
        /// Количество ЗЛ которые перешли в результате операции
        /// </summary>
        [DataMember]
        public virtual long? ZLMoveCount { get; set; }


        [DataMember]
        public virtual int Unlinked { get; set; }


        /// <summary>
        /// Проверка, что статус "СПН перечислены" или выше
        /// </summary>
        /// <returns></returns>
        public virtual bool IsSpnTransferredOver()
        {
            string s = TransferStatus;

            return
               string.Equals(s, TransferStatusIdentifier.sSPNTransferred, StringComparison.OrdinalIgnoreCase)
            || string.Equals(s, TransferStatusIdentifier.sDemandReceivedUK, StringComparison.OrdinalIgnoreCase)
            || string.Equals(s, TransferStatusIdentifier.sActSigned, StringComparison.OrdinalIgnoreCase)
            || string.Equals(s, "Требование вручено", StringComparison.OrdinalIgnoreCase);// String from old code. Obsolete(?)
        }

        /// <summary>
        /// Поле для группировки при экспорте в 1С
        /// </summary>
        [IgnoreDataMember]
        public virtual string GroupClaimNumber { get { return string.IsNullOrEmpty(ClaimNumber) ? "1" : ClaimNumber; } }
    }
}
