﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract(IsReference = true)]
    public class F050CBInfo : BaseDataObject
    {
        public F050CBInfo() { }

        public F050CBInfo(long id, long? edoID, int? typeID, string type, string name, string num, decimal? count, decimal? price)
        {
            ID = id;
            EdoID = edoID;
            TypeID = typeID;
            Type = type;
            Name = name;
            Num = num;
            Count = count;
            Price = price;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? EdoID { get; set; }

        /// <summary>
        /// ID Тип ЦБ
        /// </summary>
        [DataMember]
        public virtual int? TypeID { get; set; }
        /// <summary>
        /// Тип ЦБ
        /// </summary>
        [DataMember]
        public virtual string Type { get; set; }


        /// <summary>
        /// Наименование ЦБ (Эмитент)
        /// </summary>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Номер выпуска
        /// </summary>
        [DataMember]
        public virtual string Num { get; set; }

        /// <summary>
        /// Количество ЦБ
        /// </summary>
        [DataMember]
        public virtual decimal? Count { get; set; }

        /// <summary>
        /// Рыночная стоимость ЦБ
        /// </summary>
        [DataMember]
        public virtual decimal? Price { get; set; }


    }

    [DataContract(Name="F050")]
    public class F050CBInfoLite : IDataObjectLite
    {
        public F050CBInfoLite() { }

        public F050CBInfoLite(F050CBInfo item)
        {
            ID = item.ID;
            EdoID = item.EdoID;
            TypeID = item.TypeID;
            Type = item.Type;
            Name = item.Name;
            Num = item.Num;
            Count = item.Count;
            Price = item.Price;
        }

        public F050CBInfoLite(long id, long? edoID, int? typeID, string type, string name, string num, decimal? count, decimal? price)
        {
            ID = id;
            EdoID = edoID;
            TypeID = typeID;
            Type = type;
            Name = name;
            Num = num;
            Count = count;
            Price = price;
        }

        [DataMember(Name = "A")]
        public long ID { get; set; }

        [DataMember(Name = "B")]
        public long? EdoID { get; set; }

        /// <summary>
        /// ID Тип ЦБ
        /// </summary>
        [DataMember(Name = "C")]
        public int? TypeID { get; set; }
        /// <summary>
        /// Тип ЦБ
        /// </summary>
        [DataMember(Name = "D")]
        public string Type { get; set; }


        /// <summary>
        /// Наименование ЦБ (Эмитент)
        /// </summary>
        [DataMember(Name = "E")]
        public string Name { get; set; }

        /// <summary>
        /// Номер выпуска
        /// </summary>
        [DataMember(Name = "F")]
        public string Num { get; set; }

        /// <summary>
        /// Количество ЦБ
        /// </summary>
        [DataMember(Name = "G")]
        public decimal? Count { get; set; }

        /// <summary>
        /// Рыночная стоимость ЦБ
        /// </summary>
        [DataMember(Name = "H")]
        public decimal? Price { get; set; }


    }
}
