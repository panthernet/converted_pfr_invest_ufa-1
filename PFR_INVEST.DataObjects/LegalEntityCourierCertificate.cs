﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class LegalEntityCourierCertificate : BaseDataObject, ICloneable, IMarkedAsDeleted
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		private long _LegalEntityCourierID;
		public virtual long LegalEntityCourierID
		{
			get { return _LegalEntityCourierID; }
			set { if (_LegalEntityCourierID != value) { _LegalEntityCourierID = value; OnPropertyChanged("LegalEntityCourierID"); } }
		}

		[DataMember]
		private string _Signature;
		public virtual string Signature
		{
			get { return _Signature; }
			set { if (_Signature != value) { _Signature = value; OnPropertyChanged("Signature"); } }
		}

		[DataMember]
		private DateTime? _ExpireDate;
		public virtual DateTime? ExpireDate
		{
			get { return _ExpireDate; }
			set { if (_ExpireDate != value) { _ExpireDate = value; OnPropertyChanged("ExpireDate"); } }
		}

		[DataMember]
		private DateTime? _RegisterDate = DateTime.Now;
		public virtual DateTime? RegisterDate
		{
			get { return _RegisterDate; }
			set { if (_RegisterDate != value) { _RegisterDate = value; OnPropertyChanged("RegisterDate"); } }
		}

		[DataMember]
		private long _StatusID;
		public virtual long StatusID
		{
			get { return _StatusID; }
			set { if (_StatusID != value) { _StatusID = value; OnPropertyChanged("StatusID"); } }
		}

		/// <summary>
		/// Ссылка на биржу (ELEMENT) 
		/// </summary>
		[DataMember]
		public virtual long? StockID { get; set; }

		[DataMember]
		private string _stockName;
		public virtual string StockName
		{
			get { return _stockName; }
			set { if (_stockName != value) { _stockName = value; OnPropertyChanged("StockName"); } }
		}

		public override string this[string columnName]
		{
			get
			{				
				switch (columnName)
				{
					case "Signature": return Signature.ValidateMaxLength(128);
					case "RegisterDate": return RegisterDate.ValidateRequired();
					case "ExpireDate": return ExpireDate.ValidateRequired();
				}
				return null;
			}
		}


		public virtual object Clone()
		{
			return new LegalEntityCourierCertificate()
			{
				ID = this.ID,
				LegalEntityCourierID = this.LegalEntityCourierID,
				Signature = this.Signature,
				ExpireDate = this.ExpireDate,
				RegisterDate = this.RegisterDate,
				StatusID = this.StatusID,
				StockID = this.StockID,
				StockName = this.StockName
			};
		}
	}
}
