﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
	public class MarketPrice : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		private long? _SecurityId;
		public virtual long? SecurityId
		{
			get { return _SecurityId; }
			set { if (_SecurityId != value) { _SecurityId = value; OnPropertyChanged("SecurityId"); } }
		}

        [DataMember]
        private int? _StatusID;
        public virtual int? StatusID
        {
            get { return _StatusID; }
            set { if (_StatusID != value) { _StatusID = value; OnPropertyChanged("StatusID"); } }
        }

		[DataMember]
		private DateTime? _Date;
		public virtual DateTime? Date
		{
			get { return _Date; }
			set { if (_Date != value) { _Date = value; OnPropertyChanged("Date"); } }
		}

		[DataMember]
		private decimal? _WeightedAveragePrice;
		public virtual decimal? WeightedAveragePrice
		{
			get { return _WeightedAveragePrice; }
			set { if (_WeightedAveragePrice != value) { _WeightedAveragePrice = value; OnPropertyChanged("WeightedAveragePrice"); } }
		}

		[DataMember]
		private decimal? _Price;
		public virtual decimal? Price
		{
			get { return _Price; }
			set { if (_Price != value) { _Price = value; OnPropertyChanged("Price"); } }
		}

		[DataMember]
		private decimal? _Profit;
		public virtual decimal? Profit
		{
			get { return _Profit; }
			set { if (_Profit != value) { _Profit = value; OnPropertyChanged("Profit"); } }
		}

		[DataMember]
		private decimal? _RepaymentProfit;
		public virtual decimal? RepaymentProfit
		{
			get { return _RepaymentProfit; }
			set { if (_RepaymentProfit != value) { _RepaymentProfit = value; OnPropertyChanged("RepaymentProfit"); } }
		}

		[DataMember]
		private decimal? _CouponProfit;
		public virtual decimal? CouponProfit
		{
			get { return _CouponProfit; }
			set { if (_CouponProfit != value) { _CouponProfit = value; OnPropertyChanged("CouponProfit"); } }
		}

		[DataMember]
		private decimal? _CloseAsk;
		public virtual decimal? CloseAsk
		{
			get { return _CloseAsk; }
			set { if (_CloseAsk != value) { _CloseAsk = value; OnPropertyChanged("CloseAsk"); } }
		}

		[DataMember]
		private decimal? _CloseBid;
		public virtual decimal? CloseBid
		{
			get { return _CloseBid; }
			set { if (_CloseBid != value) { _CloseBid = value; OnPropertyChanged("CloseBid"); } }
		}

		[DataMember]
		private decimal? _YtmAsk;
		public virtual decimal? YtmAsk
		{
			get { return _YtmAsk; }
			set { if (_YtmAsk != value) { _YtmAsk = value; OnPropertyChanged("YtmAsk"); } }
		}

		[DataMember]
		private decimal? _YtmBid;
		public virtual decimal? YtmBid
		{
			get { return _YtmBid; }
			set { if (_YtmBid != value) { _YtmBid = value; OnPropertyChanged("YtmBid"); } }
		}


		[DataMember]
		public virtual string SecurityName { get; set; }

	}
}
