﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;
using PFR_INVEST.DataObjects.Attributes;

namespace PFR_INVEST.DataObjects
{
    [DataContract(IsReference = true)]
    [UseRestore(typeof(OpfrTransfer))]
    [Serializable]
    public class OpfrTransfer: IMarkedAsDeleted, IKnownType
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime DIDate { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual long PfrBranchID { get; set; }

        [DataMember]
        public virtual decimal Sum { get; set; }

        [DataMember]
        public virtual long OpfrRegisterID { get; set; }

        [DataMember]
        public virtual long OpfrCostID { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual long Status { get; set; }

        public OpfrTransfer()
        {
            Status = (long)Element.SpecialDictionaryItems.OpfrTransferStatusInitial;
            DIDate = DateTime.Now.Date;
            StatusID = 1;
        }
    }
}
