﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class EDOLog : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual DateTime? LoadDate { get; set; }

        [DataMember]
		public virtual TimeSpan? LoadTime { get; set; }

		[DataMember]
		public virtual string DocForm { get; set; }

		[DataMember]
		public virtual string DocTable { get; set; }

		[DataMember]
		public virtual long? DocId { get; set; }

		[DataMember]
		public virtual long? ExeptId { get; set; }

		[DataMember]
		public virtual string ExeptName { get; set; }

		[DataMember]
		public virtual string DocRegNumberOut { get; set; }

		[DataMember]
		public virtual string DocRegNum { get; set; }

		[DataMember]
		public virtual DateTime? RegDate { get; set; }

		[DataMember]
		public virtual long? DocBodyId { get; set; }

		[DataMember]
		public virtual string UKName { get; set; }

        [DataMember]
        public virtual string ContractNum { get; set; }
        
        [DataMember]
        public virtual string Filename { get; set; }
    }
}
