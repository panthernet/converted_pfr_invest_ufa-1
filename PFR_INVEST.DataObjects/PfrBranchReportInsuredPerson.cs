﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportInsuredPerson : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long Summary { get; set; }

        [DataMember]
        public virtual long TerritoryPersonalAppealSum { get; set; }

        [DataMember]
        public virtual long TerritoryPersonalAppealGroup1 { get; set; }

        [DataMember]
        public virtual long TerritoryPersonalAppealGroup2 { get; set; }

        [DataMember]
        public virtual long TerritoryPersonalAppealGroup3 { get; set; }

        [DataMember]
        public virtual long TerritoryOtherSum { get; set; }

        [DataMember]
        public virtual long TerritoryOtherGroup1 { get; set; }

        [DataMember]
        public virtual long TerritoryOtherGroup2 { get; set; }

        [DataMember]
        public virtual long TerritoryOtherGroup3 { get; set; }

        [DataMember]
        public virtual long Gosuslugi { get; set; }

        [DataMember]
        public virtual long Mfc { get; set; }

        [DataMember]
        public virtual long TypeId { get; set; }

        [DataMember]
        public virtual long Agency { get; set; }

        [DataMember]
        public virtual long ReportId { get; set; }
    }
}
