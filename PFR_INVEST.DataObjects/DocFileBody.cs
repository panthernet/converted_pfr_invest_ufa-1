﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class DocFileBody : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string FileName { get; set; }

        [DataMember]
        public virtual long? FileContentId { get; set; }

        [DataMember]
        public virtual long DocumentID { get; set; }
    }
}
