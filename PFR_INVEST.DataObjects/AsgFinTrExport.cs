﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    /// <summary>
    /// Модель ПП для экспорта данных (включает данные по портфелю и КБК)
    /// </summary>
    public class AsgFinTrExport : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? FinregisterID { get; set; }

        /// <summary>
        /// Сумма ПП
        /// </summary>
        [DataMember]
        public virtual decimal? DraftAmount { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual long? DirectionElID { get; set; }

        [DataMember]
        public virtual long? KBKID { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual string PortfolioName { get; set; }

        [DataMember]
        public virtual string KBK { get; set; }

    }
}