﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using System;

    [Obsolete("Устаревший, использовать F025Group5", true)]
	public class F022Group5 : BaseDataObject, IF022Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string SecurityClassification { get; set; }
		[DataMember]
		public virtual string StateRegNum { get; set; }
		[DataMember]
		public virtual decimal? MarketPrice { get; set; }
		[DataMember]
		public virtual decimal? Quantity { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }

	}
}
