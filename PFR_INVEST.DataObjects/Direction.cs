﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Direction : BaseDataObject
    {
        public Direction()
        {
            DIDate = DateTime.Today;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? BudgetID { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Дата проведения операции (создания карточки).
        /// </summary>
        [DataMember]
        public virtual DateTime? DIDate { get; set; }

        [DataMember]
        public virtual string Recipient { get; set; }

        [DataMember]
        public virtual string Number { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }
    }
}
