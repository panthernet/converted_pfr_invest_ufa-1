﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportContent_InsuredPerson : PfrBranchReportImportContentBase
    {
        [DataMember]
        public List<PfrBranchReportInsuredPerson> Report;
        
        public void Add(PfrBranchReportInsuredPerson reportRow)
        {
            Report.Add(reportRow);
        }
    }
}
