﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportContent_CertificationAgreement : PfrBranchReportImportContentBase
    {
        [DataMember]
        public List<PfrBranchReportCertificationAgreement> Report;

        public void Add(PfrBranchReportCertificationAgreement reportRow)
        {
            Report.Add(reportRow);
        }
    }
}
