﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportContent_SignatureContract : PfrBranchReportImportContentBase
    {
        [DataMember]
        public List<PfrBranchReportSignatureContract> ItemsCredit;

        [DataMember]
        public List<PfrBranchReportSignatureContract> ItemsNpf;

        public void AddCredit(PfrBranchReportSignatureContract reportRow)
        {
            ItemsCredit.Add(reportRow);
        }

        public void AddNpf(PfrBranchReportSignatureContract reportRow)
        {
            ItemsNpf.Add(reportRow);
        }
    }
}
