﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportRequest : BaseDataObject
    {
        [DataMember]
        public PfrBranchReportType.BranchReportType ReportType { get; set; }

        [DataMember]
        public PfrBranchReportImportContentBase[] Items;

        [DataMember]
        public bool IsForceUpdateDuplicate { get; set; }
    }
}
