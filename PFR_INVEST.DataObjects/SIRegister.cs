﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SIRegister : BaseDataObject, IMarkedAsDeleted
    {
			// 1 из УК в ПФР
			// 2 из ПФР в УК
			// 3 из ГУК ВР в ПФР
			// 4 из ПФР в ГУК ВР
		public enum Directions 
		{
			/// <summary>
			/// из УК в ПФР
			/// </summary>
			FromUK = 1,
			/// <summary>
			/// из ПФР в УК
			/// </summary>
			ToUK = 2,
			/// <summary>
			/// из ГУК ВР в ПФР
			/// </summary>
			FromGUK = 3,
			/// <summary>
			/// из ПФР в ГУК ВР
			/// </summary>
			ToGUK = 4
		}

		public enum Operations 
		{			
			/// <summary>
			/// 1 Перераспределение СПН 
			/// </summary>   
            SPNRedistribution = 1,     
			/// <summary>
			/// 2 ДСВ Перераспределение СПН
			/// </summary>  
            SPNRedistributionDSV = 2,   
			/// <summary>
			/// 3 МСК Перераспределение СПН  
			/// </summary>  
            SPNRedistributionMSK = 3, 
			/// <summary>
			/// 4 Возврат СПН - расторжение 
			/// </summary>  
            SPNReturn  = 4,  
			/// <summary>
			/// 5 Расформирование портфеля СПН года
			/// </summary>   
      		SPNYearPortfolioDissolution =5,
			/// <summary>
			/// 6 ДСВ Расформирование портфеля квартала 
			/// </summary>   
 			DSVQuarterPortfolioDissolution = 6,
			/// <summary>
			/// 7 МСК Расформирование портфеля полугодия 
			/// </summary>   
			MSKHalfYearPortfolioDissolution = 7,
			/// <summary>
			/// 8 Выплаты правопреемникам
			/// </summary>    
            DeadZL = 8,
			/// <summary>
			///  9 ДСВ софинансирование 
			/// </summary>
            DSVCofinancing = 9,       
			/// <summary>
			/// 10 Отзыв средств на финансирование НЧТП 
			/// </summary>  
			WithdrawNCTP =10,
			/// <summary>
			/// 11 Отзыв средств на финансирование СПВ
			/// </summary>   
			WithdrawSPV = 11,
			/// <summary>
			/// 12 Отзыв средств на финансирование ЕВ 
			/// </summary> 
     		WithdrawEV = 12,
			// 13 Финансирование выплат                   
			// 14 Средства, ошибочно переданные           
			// 15 Средства умерших ЗЛ                     
			/// <summary>
			/// 16 МСК умерших ЗЛ  
			/// </summary>
            MSKDeadZL = 16,            
			// 17 Передача СПН в доверительное управление 
            SPNTransferToControl = 17
			// 30 ДСВ умерших ЗЛ                          
			// 31 ОПС умерших ЗЛ 
          



		}

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string RegisterKind { get; set; }

        [DataMember]
        public virtual string RegisterNumber { get; set; }

        [DataMember]
        public virtual DateTime? RegisterDate { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long? OperationID { get; set; }

        [DataMember]
        public virtual long? OperationTypeID { get; set; }

        [DataMember]
        public virtual long? DirectionID { get; set; }

        [DataMember]
        public virtual long? CompanyYearID { get; set; }

        [DataMember]
        public virtual long? CompanyMonthID { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual string PayAssignment { get; set; }

    }
}
