﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class BanksConclusion : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long ElementID { get; set; }

        [DataMember]
        public virtual string ElementName { get; set; }

        [DataMember]
        private DateTime _importDate;
        public virtual DateTime ImportDate
        {
            get { return _importDate; }
            set
            {
                if (_importDate != value)
                {
                    _importDate = value;
                    OnPropertyChanged("ImportDate");
                }
            }
        }

    }
}
