﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class INDate : BaseDataObject, ILoadedFromDBF
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? TradeDate { get; set; }

        [DataMember]
        public virtual string SecurityID { get; set; }

        [DataMember]
        public virtual decimal? Open { get; set; }

        [DataMember]
        public virtual decimal? Low { get; set; }

        [DataMember]
        public virtual decimal? High { get; set; }

        [DataMember]
        public virtual decimal? Close { get; set; }

        [DataMember]
        public virtual decimal? TrendClose { get; set; }

        [DataMember]
        public virtual decimal? Value { get; set; }

        [DataMember]
        public virtual string ShortName { get; set; }

        [DataMember]
        public virtual string EnglishName { get; set; }
    }
}
