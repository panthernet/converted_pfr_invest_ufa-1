﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class EdoOdkF080 : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }
		[DataMember]
		public virtual long? ContractId { get; set; }
		[DataMember]
		public virtual string UkPerson { get; set; }
		[DataMember]
		public virtual string SdPerson { get; set; }
		[DataMember]
		public virtual string RegNum { get; set; }
		[DataMember]
		public virtual DateTime? RegDate { get; set; }
        [DataMember]
        public virtual string InvestCaseName { get; set; }
	}
}
