﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects
{
    /// <summary>
    /// View Deposit Data
    /// </summary>
    [DataContract]
    public class DepositInfo : BaseDataObject
    {
        public enum enStatus
        {
            Unknown = 0,
            Placed = 1,
            Returned = 2,
            PartiallyReturned = 3,
            ReturnedWithViolation = 4
        };

        [DataMember]
        public virtual long DepositID { get; set; }

        [DataMember]
        public virtual long DepClaimOfferID { get; set; }

        [DataMember]
        public virtual string Dc2_FirmName { get; set; }

        [DataMember]
        public virtual string Bank_PfrAgr { get; set; }

        [DataMember]
        public virtual DateTime? Bank_PfrAgrDate { get; set; }

        [DataMember]
        public virtual string Dc2_FirmID { get; set; }

        [DataMember]
        public virtual string Bank_CorrAcc { get; set; }

        [DataMember]
        public virtual DateTime Dco_Date { get; set; }

        [DataMember]
        public virtual long Dco_Number { get; set; }

        [DataMember]
        public virtual decimal Dc2_Amount { get; set; }

        [DataMember]
        public virtual decimal Dc2_Rate { get; set; }

        [DataMember]
        public virtual decimal Dc2_Payment { get; set; }

        [DataMember]
        public virtual string CurrencyName { get; set; }

        [DataMember]
        public virtual DateTime Dc2_SettleDate { get; set; }

        [DataMember]
        public virtual DateTime Dc2_ReturnDate { get; set; }

        [DataMember]
        public virtual DateTime? D_ReturnDateActual { get; set; }

        [DataMember]
        public virtual long? Status { get; set; }

        [IgnoreDataMember]
        public string AgreementAttributes
        { get { return string.Format("№{0}, {1:dd.MM.yyyy}", Bank_PfrAgr, Bank_PfrAgrDate); } }


        [DataMember]
        public virtual string Comment { get; set; }

        [IgnoreDataMember]
        public virtual string StatusText
        {
            get
            {
                if (Status == null)
                    return null;
                return DepositIdentifier.GetDepositStatusText(Status.Value);
            }
        }

        [IgnoreDataMember]
        public virtual enStatus StatusValue
        { get { return Status == null ?enStatus.Unknown : (enStatus)Status; } }

        [IgnoreDataMember]
        public virtual string Dco_DateText
        {
            get
            {
                return string.Format("{0:dd}.{0:MM}.{0:yyyy}", Dco_Date);
            }
        }

        [IgnoreDataMember]
        public virtual string Dc2_SettleDateText
        {
            get
            {
                return string.Format("{0:dd}.{0:MM}.{0:yyyy}", Dc2_SettleDate);
            }
        }

        [IgnoreDataMember]
        public virtual string Dc2_ReturnDateText
        {
            get
            {
                return string.Format("{0:dd}.{0:MM}.{0:yyyy}", Dc2_ReturnDate);
            }
        }

        [IgnoreDataMember]
        public virtual string D_ReturnDateActualText
        {
            get
            {
                if (D_ReturnDateActual == null)
                    return null;
                return string.Format("{0:dd}.{0:MM}.{0:yyyy}", D_ReturnDateActual);
            }
        }


        [DataMember]
        public virtual List<DepositAllocationInfo> DepositAllocationInfoList { get; set; }

        [DataMember]
        public virtual List<DepositAllocationInfo> DepositReturnInfoList { get; set; }

        [DataMember]
        public virtual List<DepositAllocationInfo> DepositPercentInfoList { get; set; }
    }

    [DataContract]
    public class DepositAllocationInfo : BaseDataObject
    {
        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual DateTime? ActualDate { get; set; }

        [DataMember]
        public virtual string PortfolioName { get; set; }

        [DataMember]
        public virtual string PortfolioType { get; set; }

        [DataMember]
        public virtual string Year { get; set; }

        [DataMember]
        public virtual string AccountNumber { get; set; }

        [DataMember]
        public virtual decimal Amount { get; set; }

    }
}
