﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PFR_INVEST.DataObjects.Helpers
{
    public static class FileHelper
    {
        public static string TrimName(string file, int maxLenght) 
        {
            if(string.IsNullOrEmpty(file))
                return file;
            var fullName = Path.GetFileName(file);
            if (fullName.Length <= maxLenght)
                return fullName;

            var name = Path.GetFileNameWithoutExtension(file);
            var ext = Path.GetExtension(file);

            name = name.Substring(0, maxLenght - ext.Length);
            return name + ext;
        }
    }
}
