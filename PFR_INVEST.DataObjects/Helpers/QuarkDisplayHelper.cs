﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.Helpers
{
    public sealed class QuarkDisplayHelper
    {
        
        private static List<Quark> _quarkSource;
        private static List<Quark> _quarkSourceLazy = _quarkSource ?? _quarkSource = DataContainerFacade.GetList<Quark>();
        static QuarkDisplayHelper()
        {
            //_quarkSource = new Lazy<List<Quark>>(() => );
        }

        public static string GetQuarkDisplayValue(int quarkNum)
        {
            if (quarkNum < 1 || quarkNum > 4 || _quarkSource.FirstOrDefault(x => x.Index == quarkNum) == null)
                return "[не определено]";

            return _quarkSource.First(x => x.Index == quarkNum).Name;
           
        }
    }
}
