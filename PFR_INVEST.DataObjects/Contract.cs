﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Attributes;

namespace PFR_INVEST.DataObjects
{
    [DeleteLogCaption("Договор СИ")]
    public class Contract : BaseDataObject, IMarkedAsDeleted, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }

        [DataMember]
        public virtual long? BankAccountID { get; set; }

        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual string FormalizedName { get; set; }

		/// <summary>
		/// Номер договора
		/// </summary>
        [DataMember]
        public virtual string ContractNumber { get; set; }

        [DataMember]
        public virtual DateTime? ContractDate { get; set; }

        [DataMember]
        public virtual decimal? OperateSum { get; set; }

        [DataMember]
        public virtual string PortfolioName { get; set; }

		

        /// <summary>
        /// Дата окончания договора
        /// </summary>
        [DataMember]
        public virtual DateTime? ContractEndDate { get; set; }

        /// <summary>
        /// Дата окончания договора с учетом доп. соглашения
        /// </summary>
        [DataMember]        
        public virtual DateTime? ContractSupAgreementEndDate { get; set; }

        [DataMember]
        public virtual long? ContractNameID { get; set; }

        [DataMember]
        public virtual DateTime? DissolutionDate { get; set; }

        [DataMember]
        public virtual string DissolutionBase { get; set; }

        [DataMember]
        public virtual long? DocumentTypeID { get; set; }

		/// <summary>
		/// Номер докумнта, НЕ номер договора
		/// </summary>
        [DataMember]
        public virtual string DocumentNumber { get; set; }

        [DataMember]
        public virtual DateTime? DocumentDate { get; set; }

        [DataMember]
        public virtual string OtherInfo { get; set; }

        /// <summary>
        /// Старое поле статуса, перенаправляется в StatusID
        /// Статус по дефолту = 0, поэтому чекается в SaveOnlyEntity, SaveEntity и факадном Save
        /// </summary>
        [IgnoreDataMember]
        public virtual int Status { get { return (int)StatusID; } set { StatusID = value; } }

        /// <summary>
        /// Поле статуса согласно интерфейсу IMarkedAsDeleted
        /// </summary>
        [DataMember]        
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual int TypeID { get; set; }

    }
}
