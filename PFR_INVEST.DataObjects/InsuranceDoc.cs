﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class InsuranceDoc : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? ContractID { get; set; }

        [DataMember]
        public virtual decimal? Sum { get; set; }

        [DataMember]
        public virtual string Insurance { get; set; }

        [DataMember]
        public virtual string ReqNum { get; set; }

        [DataMember]
        public virtual string DocSum { get; set; }

        [DataMember]
        public virtual DateTime? ReqDate { get; set; }

        [DataMember]
        public virtual DateTime? ChangeDate { get; set; }

        [DataMember]
        public virtual DateTime? CloseDate { get; set; }

        [DataMember]
        public virtual int TypeID { get; set; }
    }
}
