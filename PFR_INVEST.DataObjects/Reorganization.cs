﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Reorganization : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long SourceContragentID { get; set; }

        [DataMember]
        public virtual DateTime ReorganizationDate { get; set; }

        [DataMember]
        public virtual DateTime? OrderDate { get; set; }

        [DataMember]
        public virtual long ReceiverContragentID { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual long ReorganizationTypeID { get; set; }

        [DataMember]
        public virtual string ReorganizationTypeName { get; set; }

        [DataMember]
        public virtual string ReceiverName { get; set; }

        [DataMember]
        public virtual string SourceName { get; set; }
    }
}
