﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PaymentAssignment : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long PartID { get; set; }

        [DataMember]
        public virtual long DirectionID { get; set; }

        [DataMember]
        public virtual long OperationContentID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

    }
}
