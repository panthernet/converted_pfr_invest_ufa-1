﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using PFR_INVEST.DataObjects.Interfaces;

    public class F026Group12 : BaseDataObject, IF026Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string FondManagerName { get; set; }
		[DataMember]
		public virtual string IssueNum { get; set; }
		[DataMember]
		public virtual decimal? MarketPrice { get; set; }
		[DataMember]
		public virtual decimal? Quantity { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
		[DataMember]
		public virtual string FondName { get; set; }
		[DataMember]
		public virtual string SecurityClassification { get; set; }
	}
}
