﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class F80Deal : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string Emitent { get; set; }
		[DataMember]
		public virtual DateTime? GrDate { get; set; }
		[DataMember]
		public virtual string GRN { get; set; }
		[DataMember]
		public virtual string TradeOrganizer { get; set; }
		[DataMember]
		public virtual string OperKind { get; set; }
		[DataMember]
		public virtual decimal? FactPrice { get; set; }
		[DataMember]
		public virtual decimal? MarketPrice { get; set; }
		[DataMember]
		public virtual decimal? PriceDeviation { get; set; }
		[DataMember]
		public virtual string Kind { get; set; }

		[IgnoreDataMember]
		public virtual int? ReportYear { get { return this.ReportOnDate.HasValue ? (int?)this.ReportOnDate.Value.Year : null; } }

		[DataMember]
		public virtual string UKName { get; set; }		
		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }
		[DataMember]
		public virtual string ContractNumber { get; set; }
        /// <summary>
        /// тип контракта для СИ или ВР
        /// </summary>
		[DataMember]
		public virtual int ContractType { get; set; }


	}
}
