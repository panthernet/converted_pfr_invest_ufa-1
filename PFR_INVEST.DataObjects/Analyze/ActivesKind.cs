﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class ActivesKind : BaseDataObject
    {
        [DataMember]
        public virtual int Id { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual int OrderId { get; set; }

    }
}