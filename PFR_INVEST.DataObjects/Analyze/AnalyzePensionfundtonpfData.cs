﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzePensionfundtonpfData : AnalyzeReportDataBase
    {
        public virtual int SubjectId { get; set; }
        //public virtual decimal? Total { get; set; }
        public virtual short IsSubtraction { get; set; }

        #region Поля только для чтения из БД
        public virtual string SubjectName { get; set; }
        public virtual SubjectSPN.FondType SubjectTypeFond { get; set; }
        public virtual int SubjOrderId { get; set; }
        //0 - без учета возврата, 1 - с учетом возврата

        #endregion

        public AnalyzePensionfundtonpfData(AnalyzePensionfundtonpfData d)
        {
            Id = d.Id;
            ReportId = d.ReportId;
            Total = d.Total;
            SubjectId = d.SubjectId;
            SubjectName = d.SubjectName;
            IsSubtraction = d.IsSubtraction;
            SubjectTypeFond = d.SubjectTypeFond;
            SubjOrderId = d.SubjOrderId;
            IsReadOnly = d.IsReadOnly;
        }

        public AnalyzePensionfundtonpfData()
        {
            
        }
    }
}
