﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{

    public class DelayedPaymentClaim : BaseDataObject, IMarkedAsDeleted
    {
        private long _ID;
        [DataMember]
        public virtual long ID
        {
            get { return _ID; }
            set { if (_ID != value) { _ID = value; OnPropertyChanged("ID"); } }
        }


        private string _UFONumber;
        [DataMember]
        public virtual string UFONumber
        {
            get { return _UFONumber; }
            set { if (_UFONumber != value) { _UFONumber = value; OnPropertyChanged("UFONumber"); } }
        }


        private DateTime _UFODate;
        [DataMember]
        public virtual DateTime UFODate
        {
            get { return _UFODate; }
            set { if (_UFODate != value) { _UFODate = value; OnPropertyChanged("UFODate"); } }
        }


        private string _LetterNumber;
        [DataMember]
        public virtual string LetterNumber
        {
            get { return _LetterNumber; }
            set { if (_LetterNumber != value) { _LetterNumber = value; OnPropertyChanged("LetterNumber"); } }
        }


        private DateTime? _LetterDate;
        [DataMember]
        public virtual DateTime? LetterDate
        {
            get { return _LetterDate; }
            set { if (_LetterDate != value) { _LetterDate = value; OnPropertyChanged("LetterDate"); } }
        }




        private long? _FZID;
        [DataMember]
        public virtual long? FZID
        {
            get { return _FZID; }
            set { if (_FZID != value) { _FZID = value; OnPropertyChanged("FZID"); } }
        }

        private string _FZName;
        /// <summary>
        /// Имя ФЗ, используется только для вычитки с базы
        /// </summary>
        [DataMember]
        public virtual string FZName
        {
            get { return _FZName; }
            set { if (_FZName != value) { _FZName = value; OnPropertyChanged("FZName"); } }
        }




        private string _Name;
        [DataMember]
        public virtual string Name
        {
            get { return _Name; }
            set { if (_Name != value) { _Name = value; OnPropertyChanged("Name"); } }
        }


        private long? _NpfID;
        [DataMember]
        public virtual long? NpfID
        {
            get { return _NpfID; }
            set { if (_NpfID != value) { _NpfID = value; OnPropertyChanged("NpfID"); } }
        }

        private string _NpfName;
        /// <summary>
        /// Имя НПФ, используется только для вычитки с базы
        /// </summary>
        [DataMember]
        public virtual string NpfName
        {
            get { return _NpfName; }
            set { if (_NpfName != value) { _NpfName = value; OnPropertyChanged("NpfName"); } }
        }

        private string _Inn;
        /// <summary>
        /// ИНН, используется только для вычитки с базы
        /// </summary>
        [DataMember]
        public virtual string Inn
        {
            get { return _Inn; }
            set { if (_Inn != value) { _Inn = value; OnPropertyChanged("Inn"); } }
        }

        private decimal? _Ammount;
        [DataMember]
        public virtual decimal? Ammount
        {
            get { return _Ammount; }
            set { if (_Ammount != value) { _Ammount = value; OnPropertyChanged("Ammount"); } }
        }


        private long? _ZLCount;
        [DataMember]
        public virtual long? ZLCount
        {
            get { return _ZLCount; }
            set { if (_ZLCount != value) { _ZLCount = value; OnPropertyChanged("ZLCount"); } }
        }


        private string _PRFNumber;
        [DataMember]
        public virtual string PRFNumber
        {
            get { return _PRFNumber; }
            set { if (_PRFNumber != value) { _PRFNumber = value; OnPropertyChanged("PRFNumber"); } }
        }


        private string _Comment;
        [DataMember]
        public virtual string Comment
        {
            get { return _Comment; }
            set { if (_Comment != value) { _Comment = value; OnPropertyChanged("Comment"); } }
        }


        private long _StatusID;
        [DataMember]
        public virtual long StatusID
        {
            get { return _StatusID; }
            set { if (_StatusID != value) { _StatusID = value; OnPropertyChanged("StatusID"); } }
        }

        private DateTime? _RestrictStartDate;
        [DataMember]
        public virtual DateTime? RestrictStartDate
        {
            get { return _RestrictStartDate; }
            set { if (_RestrictStartDate != value) { _RestrictStartDate = value; OnPropertyChanged("RestrictStartDate"); } }
        }


        private DateTime? _RestrictEndDate;
        [DataMember]
        public virtual DateTime? RestrictEndDate
        {
            get { return _RestrictEndDate; }
            set { if (_RestrictEndDate != value) { _RestrictEndDate = value; OnPropertyChanged("RestrictEndDate"); } }
        }

        private DateTime? _PfrDate;
        [DataMember]
        public virtual DateTime? PfrDate
        {
            get { return _PfrDate; }
            set { if (_PfrDate != value) { _PfrDate = value; OnPropertyChanged("PfrDate"); } }
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "UFONumber": return this.UFONumber.ValidateMaxLength(50);
                    case "LetterNumber": return this.LetterNumber.ValidateMaxLength(50);
                    case "Name": return this.Name.ValidateMaxLength(50) ?? this.Name.ValidateRequired();
                    case "PRFNumber": return this.PRFNumber.ValidateMaxLength(50);
                    case "Comment": return this.Comment.ValidateMaxLength(50);
                    case "FZID": return this.FZID.ValidateRequired();
                    case "Ammount": return this.Ammount.ValidateRequired() ?? IsValidAmmount();
                    case "ZLCount": return this.ZLCount.ValidateRequired();
                    case "NpfID": return this.NpfID.ValidateRequired();

                    default:
                        return base[columnName];
                }

            }
        }

        private string IsValidAmmount()
        {

            return this.Ammount > 9999999999999999999999999.99M ? "Превышено максимально допустимое значение" : null;

        }
    }
}
