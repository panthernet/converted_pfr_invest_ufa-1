﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.DataObjects
{
    [DataContract(IsReference = true)]
    public class LinkPPOPFRTransfer : IKnownType
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long PPID { get; set; }

        [DataMember]
        public virtual long TransferID { get; set; }
    }
}
