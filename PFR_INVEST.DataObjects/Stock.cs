﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class Stock : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long Key { get; set; }
		
        [DataMember]
        public virtual int? Order { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual DateTime? UpdateDate { get; set; }

        public enum StockID
		{
			MoscowStock = 1,
			SPVB = 2,
		}
    }
}
