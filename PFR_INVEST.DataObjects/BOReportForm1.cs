﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.DataObjects
{
    public class BOReportForm1: BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual int ReportType { get; set; }

        [DataMember]
        public virtual int Year { get; set; }

        [DataMember]
        public virtual int Quartal { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }

        [DataMember]
        public virtual DateTime CreateDate { get; set; }

        [DataMember]
        public virtual DateTime UpdateDate { get; set; }

        [DataMember]
        public virtual BOReportForm1Data Data { get; set; }

        [DataMember]
        public virtual List<BOReportForm1Sum> SumList { get; set; }

        [DataMember]
        public virtual int Status { get; set; }

        [DataMember]
        public virtual string OKUD { get; set; }

        [DataMember]
        public virtual long? FileID { get; set; }

        [IgnoreDataMember]
        public virtual string FileText => FileID.HasValue ? "Оригинал" : "";

        [IgnoreDataMember]
        public virtual bool IsActive => Status == 1;

        [IgnoreDataMember]
        public virtual string StatusTip => GetStatusName(IsApproved);

        public static string GetStatusName(int status)
        {
            return status == 2 ? "Находится на согласовании" : (status == 1 ? "Cогласовано" : "Не согласовано");
        }

        [IgnoreDataMember]
        public virtual string QuartalText
        {
            get
            {
                switch(Quartal)
                {
                    case 1:
                        return "I квартал";
                    case 2:
                        return "II квартал";
                    case 3:
                        return "III квартал";
                    case 4:
                        return "IV квартал";
                    default:
                        return "";
                }
            }
        }

        [IgnoreDataMember]
        public virtual string MonthText => Quartal == 0 ? "" : DateTools.Months[Quartal - 1];

        [DataMember]
        public virtual string OKATO { get; set; }
        [DataMember]
        public virtual string INN { get; set; }
        [DataMember]
        public virtual string OGRN { get; set; }
        [DataMember]
        public virtual string FullName { get; set; }
        [DataMember]
        public virtual string ShortName { get; set; }
        [DataMember]
        public virtual DateTime? RegDate { get; set; }
        [DataMember]
        public virtual string RegNum { get; set; }
        [DataMember]
        public virtual long PersonID { get; set; }
        [DataMember]
        public virtual long? PortfolioID { get; set; }
        [IgnoreDataMember]
        public virtual ReportTypeEnum RType => GetOkudTypeFromString(OKUD);


        [DataMember]
        public virtual int IsApproved1 { get; set; }
        [DataMember]
        public virtual int IsApproved2 { get; set; }
        [DataMember]
        public virtual string Approver1 { get; set; }
        [DataMember]
        public virtual string Approver2 { get; set; }
        [DataMember]
        public virtual string ApproverLogin1 { get; set; }
        [DataMember]
        public virtual string ApproverLogin2 { get; set; }
        [DataMember]
        public virtual DateTime? ApprovementDate1 { get; set; }
        [DataMember]
        public virtual DateTime? ApprovementDate2 { get; set; }

        public virtual int IsApproved 
        {
            get
            {
                switch (OKUD?.ToLower())
                {
                    case "0418001":
                    case "0418003":
                        return IsApproved1;
                    case "0418002":
                        return IsApproved1 == 2 || IsApproved2 == 2 ? 2 : (IsApproved1 == 1 && IsApproved2 == 1 ? 1 : 0);
                    default:
                        return 0;
                }
                
            }
        }


        public BOReportForm1()
        {
            Data = new BOReportForm1Data();
            SumList = new List<BOReportForm1Sum>();
        }

        public static string GetOkudString(ReportTypeEnum type)
        {
            switch (type)
            {
                case ReportTypeEnum.One:
                    return "0418001";
                case ReportTypeEnum.Two:
                    return "0418002";
                case ReportTypeEnum.Three:
                    return "0418003";
                case ReportTypeEnum.All:
                    return "0418001,0418002,0418003";
                default:
                    throw new Exception("Неизвестный ОКУД!");
            }
        }

        public static ReportTypeEnum GetOkudTypeFromString(string type)
        {
            switch (type)
            {
                case "0418001":
                    return ReportTypeEnum.One;
                case "0418002":
                    return ReportTypeEnum.Two;
                case "0418003":
                    return ReportTypeEnum.Three;
                case "Все":
                case "все":
                case "0418001,0418002,0418003":
                    return ReportTypeEnum.All;
                default:
                    throw new Exception("Неизвестный ОКУД!");
            }
        }

        public enum ReportTypeEnum
        {
            One,
            Two,
            Three,
            All
        }

        public enum ReportStatus
        {
            Declined = 0,
            Approved = 1,
            InProcess = 2,
        }

        public static List<string> GetOkudList(ReportTypeEnum reportType)
        {
            return new List<string> { "0418001", "0418002", "0418003" };
        }
    }

    public class BOReportForm1Data : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long ReportID { get; set; }

        [IgnoreDataMember]
        public virtual decimal Total1 => (Total2 ?? 0) + (Total3 ?? 0) + (Total4 ?? 0);

        [IgnoreDataMember]
        public virtual decimal Quartal1 => (Quartal2 ?? 0) + (Quartal3 ?? 0) + (Quartal4 ?? 0);

        private decimal? _total2;

        [DataMember]
        public virtual decimal? Total2
        {
            get { return _total2; }
            set { _total2 = value; OnPropertyChanged("Total2"); OnPropertyChanged("Total1"); }
        }

        private decimal? _quartal2;

        [DataMember]
        public virtual decimal? Quartal2
        {
            get { return _quartal2; }
            set { _quartal2 = value; OnPropertyChanged("Quartal2"); OnPropertyChanged("Quartal1"); }
        }

        private decimal? _total3;

        [DataMember]
        public virtual decimal? Total3
        {
            get { return _total3; }
            set { _total3 = value; OnPropertyChanged("Total3"); OnPropertyChanged("Total1"); }
        }

        private decimal? _quartal3;

        [DataMember]
        public virtual decimal? Quartal3
        {
            get { return _quartal3; }
            set { _quartal3 = value; OnPropertyChanged("Quartal3"); OnPropertyChanged("Quartal1"); }
        }

        private decimal? _total4;

        [DataMember]
        public virtual decimal? Total4
        {
            get { return _total4; }
            set { _total4 = value; OnPropertyChanged("Total4"); OnPropertyChanged("Total1"); }
        }

        private decimal? _quartal4;

        [DataMember]
        public virtual decimal? Quartal4
        {
            get { return _quartal4; }
            set { _quartal4 = value; OnPropertyChanged("Quartal4"); OnPropertyChanged("Quartal1"); }
        }

        private decimal? _total5;

        [DataMember]
        public virtual decimal? Total5
        {
            get { return _total5; }
            set { _total5 = value; OnPropertyChanged("Total5"); }
        }

        private decimal? _quartal5;

        [DataMember]
        public virtual decimal? Quartal5
        {
            get { return _quartal5; }
            set { _quartal5 = value; OnPropertyChanged("Quartal5"); }
        }

        [IgnoreDataMember]
        public virtual decimal Total6 => Total7 + (Total10 ?? 0) + (Total11 ?? 0);

        [IgnoreDataMember]
        public virtual decimal Quartal6 => Quartal7 + (Quartal10 ?? 0) + (Quartal11 ?? 0);

        [IgnoreDataMember]
        public virtual decimal Total7 => (Total8 ?? 0) + (Total9 ?? 0);

        [IgnoreDataMember]
        public virtual decimal Quartal7 => (Quartal8 ?? 0) + (Quartal9 ?? 0);

        private decimal? _total8;

        [DataMember]
        public virtual decimal? Total8
        {
            get { return _total8; }
            set { _total8 = value; OnPropertyChanged("Total8"); OnPropertyChanged("Total7"); OnPropertyChanged("Total6"); OnPropertyChanged("Total17"); }
        }

        private decimal? _quartal8;

        [DataMember]
        public virtual decimal? Quartal8
        {
            get { return _quartal8; }
            set { _quartal8 = value; OnPropertyChanged("Quartal8"); OnPropertyChanged("Quartal7"); OnPropertyChanged("Quartal6"); OnPropertyChanged("Quartal17"); }
        }

        private decimal? _total9;

        [DataMember]
        public virtual decimal? Total9
        {
            get { return _total9; }
            set { _total9 = value; OnPropertyChanged("Total9"); OnPropertyChanged("Total7"); OnPropertyChanged("Total6"); OnPropertyChanged("Total17"); }
        }

        private decimal? _quartal9;

        [DataMember]
        public virtual decimal? Quartal9
        {
            get { return _quartal9; }
            set { _quartal9 = value; OnPropertyChanged("Quartal9"); OnPropertyChanged("Quartal7"); OnPropertyChanged("Quartal6"); OnPropertyChanged("Quartal17"); }
        }

        private decimal? _total10;

        [DataMember]
        public virtual decimal? Total10
        {
            get { return _total10; }
            set { _total10 = value; OnPropertyChanged("Total10"); OnPropertyChanged("Total6"); OnPropertyChanged("Total17"); }
        }

        private decimal? _quartal10;

        [DataMember]
        public virtual decimal? Quartal10
        {
            get { return _quartal10; }
            set { _quartal10 = value; OnPropertyChanged("Quartal10"); OnPropertyChanged("Quartal6"); OnPropertyChanged("Quartal17"); }
        }

        private decimal? _total11;

        [DataMember]
        public virtual decimal? Total11
        {
            get { return _total11; }
            set { _total11 = value; OnPropertyChanged("Total11"); OnPropertyChanged("Total6"); OnPropertyChanged("Total17");}
        }

        private decimal? _quartal11;

        [DataMember]
        public virtual decimal? Quartal11
        {
            get { return _quartal11; }
            set { _quartal11 = value; OnPropertyChanged("Quartal11"); OnPropertyChanged("Quartal6"); OnPropertyChanged("Quartal17"); }
        }

        [IgnoreDataMember]
        public virtual decimal Total12 => (Total13 ?? 0) + (Total14 ?? 0) + (Total15 ?? 0) + (Total16 ?? 0);

        [IgnoreDataMember]
        public virtual decimal Quartal12 => (Quartal13 ?? 0) + (Quartal14 ?? 0) + (Quartal15 ?? 0) + (Quartal16 ?? 0);

        private decimal? _total13;

        [DataMember]
        public virtual decimal? Total13
        {
            get { return _total13; }
            set { _total13 = value; OnPropertyChanged("Total13"); OnPropertyChanged("Total12"); OnPropertyChanged("Total17"); }
        }

        private decimal? _quartal13;

        [DataMember]
        public virtual decimal? Quartal13
        {
            get { return _quartal13; }
            set { _quartal13 = value; OnPropertyChanged("Quartal13"); OnPropertyChanged("Quartal12"); OnPropertyChanged("Quartal17"); }
        }

        private decimal? _total14;

        [DataMember]
        public virtual decimal? Total14
        {
            get { return _total14; }
            set { _total14 = value; OnPropertyChanged("Total14"); OnPropertyChanged("Total12"); OnPropertyChanged("Total17"); }
        }

        private decimal? _quartal14;

        [DataMember]
        public virtual decimal? Quartal14
        {
            get { return _quartal14; }
            set { _quartal14 = value; OnPropertyChanged("Quartal14"); OnPropertyChanged("Quartal12"); OnPropertyChanged("Quartal17"); }
        }

        private decimal? _total15;

        [DataMember]
        public virtual decimal? Total15
        {
            get { return _total15; }
            set { _total15 = value; OnPropertyChanged("Total15"); OnPropertyChanged("Total12"); OnPropertyChanged("Total17"); }
        }

        private decimal? _quartal15;

        [DataMember]
        public virtual decimal? Quartal15
        {
            get { return _quartal15; }
            set { _quartal15 = value; OnPropertyChanged("Quartal15"); OnPropertyChanged("Quartal12"); OnPropertyChanged("Quartal17"); }
        }

        private decimal? _total16;

        [DataMember]
        public virtual decimal? Total16
        {
            get { return _total16; }
            set { _total16 = value; OnPropertyChanged("Total16"); OnPropertyChanged("Total12"); OnPropertyChanged("Total17"); }
        }

        private decimal? _quartal16;

        [DataMember]
        public virtual decimal? Quartal16
        {
            get { return _quartal16; }
            set { _quartal16 = value; OnPropertyChanged("Quartal16"); OnPropertyChanged("Quartal12"); OnPropertyChanged("Quartal17"); }
        }

        [IgnoreDataMember]
        public virtual decimal Total17 => Total6 - Total12;

        [IgnoreDataMember]
        public virtual decimal Quartal17 => Quartal6 - Quartal12;

        private decimal? _total18;

        [DataMember]
        public virtual decimal? Total18
        {
            get { return _total18; }
            set { _total18 = value; OnPropertyChanged("Total18"); }
        }

        //[DataMember]
        //public decimal? Quartal18 { get; set; }

        //[DataMember]
        // public decimal? Total19 { get; set; }
        private decimal? _quartal19;

        [DataMember]
        public virtual decimal? Quartal19
        {
            get { return _quartal19; }
            set { _quartal19 = value; OnPropertyChanged("Quartal19"); }
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Total18":
                        return !Total18.HasValue ? "Значение не задано!" : null;
                    case "Total19":
                        return !Total18.HasValue ? "Значение не задано!" : null;

                    default:
                        return base[columnName];
                }

            }
        }

        public BOReportForm1Data()
        {
            _total3 = _total4 = _quartal3 = _quartal4 = 0;
        }
    }

    public class BOReportForm1Sum : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long ReportID { get; set; }

        [DataMember]
        public virtual decimal Sum { get; set; }

        [DataMember]
        public virtual long PortfolioID { get; set; }

        [DataMember]
        public virtual string PortfolioName { get; set; }
    }

}
