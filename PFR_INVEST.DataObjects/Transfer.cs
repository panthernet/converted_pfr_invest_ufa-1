﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Transfer : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? DirectionID { get; set; }

        [DataMember]
        public virtual long? PFRBankAccountID { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        public virtual DateTime? DIDate { get; set; }

        [DataMember]
        public virtual decimal? Sum { get; set; }

        [DataMember]
        public virtual string Recipient { get; set; }

        [DataMember]
        public virtual string Number { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }
    }
}
