﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class ContrInvest : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? ContractID { get; set; }

        [DataMember]
        public virtual long? GCBRFMAX { get; set; }

        [DataMember]
        public virtual long? GCBRFMIN { get; set; }

        [DataMember]
        public virtual long? GCBSMAX { get; set; }

        [DataMember]
        public virtual long? GCBSMIN { get; set; }

        [DataMember]
        public virtual long? OBLMAX { get; set; }

        [DataMember]
        public virtual long? OBLMIN { get; set; }

        [DataMember]
        public virtual long? IPMAX { get; set; }

        [DataMember]
        public virtual long? IPMIN { get; set; }

        [DataMember]
        public virtual long? SRMAX { get; set; }

        [DataMember]
        public virtual long? SRMIN { get; set; }

        [DataMember]
        public virtual long? DEPMAX { get; set; }

        [DataMember]
        public virtual long? DEPMIN { get; set; }

        [DataMember]
        public virtual long? SRVALMAX { get; set; }

        [DataMember]
        public virtual long? SRVALMIN { get; set; }
    }
}
