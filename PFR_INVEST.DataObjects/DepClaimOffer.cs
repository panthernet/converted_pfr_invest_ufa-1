﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class DepClaimOffer : BaseDataObject
    {
        public enum Statuses
        {
            [Description("Не подписана")]
            NotSigned = 1,
            [Description("Подписана")]
            Signed = 2,
            [Description("Акцептована")]
            Accepted = 3,
            [Description("Не акцептована")]
            NotAccepted = 4
        }

        [IgnoreDataMember]
        public virtual string AuctionStatusText
        {
            get
            {
                return Status.GetDescription();
            }
        }

        /// <summary>
        /// Поле для группировки данных при экспорте в 1С
        /// </summary>
        [IgnoreDataMember]
        public virtual DateTime? GroupClaimDate { get { return ClaimDate ?? Date; } }
        /// <summary>
        /// Поле для группировки данных при экспорте в 1С
        /// </summary>
        [IgnoreDataMember]
        public virtual string GroupClaimNumber { get { return string.IsNullOrEmpty(ClaimNumber) ? "1" : ClaimNumber; } }

        [DataMember]
        public virtual long ID { get; set; }


        /// <summary>
        /// Номер договора банковского депозита
        /// </summary>
        [DataMember]
        public virtual long Number { get; set; }


        [DataMember]
        public virtual string ClaimNumber { get; set; }

        [DataMember]
        public virtual DateTime? ClaimDate { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }

        [IgnoreDataMember]
        public virtual Statuses Status
        {
            get { return (Statuses)this.StatusID; }
            set { this.StatusID = (int)value; }
        }

        [DataMember]
        public virtual int StatusID { get; set; }

        [DataMember]
        public virtual byte DepositsGenerated { get; set; }

         [IgnoreDataMember]
         public virtual bool IsDepositsGenerated
         {
             get
             {
                 if (DepositsGenerated == 0) return false;
                 return true;
             }
             set { DepositsGenerated = (byte) (value ? 1 : 0); }
         }

        [DataMember]
        public virtual long AuctionID { get; set; }

        [DataMember]
        public virtual long BankID { get; set; }

        [DataMember]
        public virtual long DepClaimID { get; set; }

    }
}
