﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OldNPFName : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }
       
        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual string ShortName { get; set; }

        [DataMember]
        public virtual string FormalizedName { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }
    }
}
