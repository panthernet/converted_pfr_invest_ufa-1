﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class BlUserSetting : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string UserLogin { get; set; }
       
        [DataMember]
        public virtual string Key { get; set; }

        [DataMember]
        public virtual string Value { get; set; }

        [DataMember]
        public virtual DateTime? ExpireDate { get; set; }

        [DataMember]
        public virtual DateTime? UpdateDate { get; set; }       
       
    }
}

