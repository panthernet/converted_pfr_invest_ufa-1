﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesFileExport : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long SessionID { get; set; }

        [DataMember]
        public virtual int EntityType { get; set; }

        [DataMember]
        public virtual long EntityID { get; set; }

        [DataMember]
        public virtual long ExportID { get; set; }

        public enum OnesExportType
        {
            Finregister = 0,
            ReqTransfer = 1,
            Deposit = 2,
            ReqTransferVr = 3,
            Opfr = 4
        }

        public virtual bool IsOfType(OnesExportType type)
        {
            return EntityType == (int) type;
        }
    }
}
