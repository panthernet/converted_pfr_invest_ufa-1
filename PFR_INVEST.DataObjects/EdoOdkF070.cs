﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class EdoOdkF070 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? ContractId { get; set; }

        [DataMember]
        public virtual long? YearId { get; set; }

        [DataMember]
        public virtual int? Quartal { get; set; }

        [DataMember]
        public virtual decimal? Profit1 { get; set; }

        [DataMember]
        public virtual decimal? Profit2 { get; set; }

        [DataMember]
        public virtual decimal? ActiveSale1 { get; set; }

        [DataMember]
        public virtual decimal? ActiveSale2 { get; set; }

        [DataMember]
        public virtual decimal? Dividend1 { get; set; }

        [DataMember]
        public virtual decimal? Dividend2 { get; set; }

        [DataMember]
        public virtual decimal? Proc1 { get; set; }

        [DataMember]
        public virtual decimal? Proc2 { get; set; }

        [DataMember]
        public virtual decimal? ActivReValue1 { get; set; }

        [DataMember]
        public virtual decimal? ActivReValue2 { get; set; }

        [DataMember]
        public virtual decimal? OtherProfit1 { get; set; }

        [DataMember]
        public virtual decimal? OtherProfit2 { get; set; }

        [DataMember]
        public virtual decimal? Retained1 { get; set; }

        [DataMember]
        public virtual decimal? Retained2 { get; set; }

        [DataMember]
        public virtual decimal? SdPay1 { get; set; }

        [DataMember]
        public virtual decimal? SdPay2 { get; set; }

        [DataMember]
        public virtual decimal? BrokerPay1 { get; set; }

        [DataMember]
        public virtual decimal? BrokerPay2 { get; set; }

        [DataMember]
        public virtual decimal? AuditPay1 { get; set; }

        [DataMember]
        public virtual decimal? AuditPay2 { get; set; }

        [DataMember]
        public virtual decimal? StrahPay1 { get; set; }

        [DataMember]
        public virtual decimal? StrahPay2 { get; set; }

        [DataMember]
        public virtual decimal? OtherPay1 { get; set; }

        [DataMember]
        public virtual decimal? OtherPay2 { get; set; }

        [DataMember]
        public virtual decimal? UKReward1 { get; set; }

        [DataMember]
        public virtual decimal? UKReward2 { get; set; }

        [DataMember]
        public virtual decimal? UKReward { get; set; }

        [DataMember]
        public virtual decimal? TranshTotal { get; set; }

        [DataMember]
        public virtual decimal? January { get; set; }

        [DataMember]
        public virtual decimal? February { get; set; }

        [DataMember]
        public virtual decimal? March { get; set; }

        [DataMember]
        public virtual decimal? April { get; set; }

        [DataMember]
        public virtual decimal? May { get; set; }

        [DataMember]
        public virtual decimal? June { get; set; }

        [DataMember]
        public virtual decimal? July { get; set; }

        [DataMember]
        public virtual decimal? August { get; set; }

        [DataMember]
        public virtual decimal? September { get; set; }

        [DataMember]
        public virtual decimal? October { get; set; }

        [DataMember]
        public virtual decimal? November { get; set; }

        [DataMember]
        public virtual decimal? December { get; set; }

        [DataMember]
        public virtual decimal? MaxUKPay { get; set; }

        [DataMember]
        public virtual decimal? FactUKPay { get; set; }

        [DataMember]
        public virtual decimal? MaxSDPay { get; set; }

        [DataMember]
        public virtual decimal? FactSDPay { get; set; }

        [DataMember]
        public virtual decimal? ProfitProc { get; set; }

        [DataMember]
        public virtual decimal? UKRewardProc { get; set; }

        [DataMember]
        public virtual string RegNumberOut { get; set; }

        [DataMember]
        public virtual string RegNumber { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }

        [DataMember]
        public virtual decimal? MidValue { get; set; }

        [DataMember]
        public virtual string InvestCaseName { get; set; }

        [DataMember]
        public virtual string Kpp { get; set; }

    }
}
