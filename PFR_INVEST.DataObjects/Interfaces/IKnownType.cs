﻿namespace PFR_INVEST.DataObjects.Interfaces
{
    /// <summary>
    /// Помечает класс, как известный для сериализатора и доступный для обмена с сервисом
    /// </summary>
    public interface IKnownType
    {
    }
}
