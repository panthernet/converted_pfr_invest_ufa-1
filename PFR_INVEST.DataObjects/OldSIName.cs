﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OldSIName : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }
       
        [DataMember]
        public virtual string OldFullName { get; set; }

        [DataMember]
        public virtual string OldShortName { get; set; }

        [DataMember]
        public virtual string OldFormalizedName { get; set; }

		[DataMember]
		public virtual string OldINN { get; set; }

        [DataMember]
        public virtual DateTime? AgreementDate { get; set; }

        [DataMember]
        public virtual string AgreementNumber { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }
    }
}
