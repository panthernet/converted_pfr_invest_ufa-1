﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OwnedFounds : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual decimal? Capital { get; set; }

        [DataMember]
        public virtual decimal? CountPay { get; set; }

        [DataMember]
        public virtual decimal? CountDogRez { get; set; }

        [DataMember]
        public virtual decimal? CountDogAccum { get; set; }

        [DataMember]
        public virtual decimal? CostPay { get; set; }

        [DataMember]
        public virtual decimal? CostRez { get; set; }

        [DataMember]
        public virtual decimal? CostInvestNPF { get; set; }

        [DataMember]
        public virtual decimal? CostInvestPFR { get; set; }

        [DataMember]
        public virtual int? Quartal { get; set; }

        [DataMember]
        public virtual long? YearID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }
    }
}
