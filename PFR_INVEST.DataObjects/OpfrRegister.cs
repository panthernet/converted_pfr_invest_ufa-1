﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.DataObjects
{
    [DataContract(IsReference = true)]
    [UseRestore(typeof(OpfrRegister))]
    [UseRestoreChild(typeof(OpfrTransfer), "OpfrRegisterID", "StatusID", 1)]
    public class OpfrRegister: IMarkedAsDeleted, IKnownType
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long KindID { get; set; }

        [DataMember]
        public virtual long RegTypeID { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }

        [DataMember]
        public virtual long MonthID { get; set; }

        [DataMember]
        public virtual long YearID { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual long? ImportPortfolioID { get; set; }

        [DataMember]
        public virtual long? FileID { get; set; }

        [DataMember]
        public virtual DateTime? TrancheDate { get; set; }

        [DataMember]
        public virtual string TrancheNum { get; set; }

    }
}
