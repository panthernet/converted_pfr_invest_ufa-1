﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PFR_INVEST.DataObjects.XMLModel.KIP
{
	[XmlRoot("ФайлПФР")]
	public class KIP_Import : KIP_DocumentBase
	{
		public const string RECALL_MSK = "I_RECALL_MSK_UK";
		public const string RECALL_MSK_TEXT = "РЕЕСТР_ОТЗЫВ_МСК_ИЗ_УК";
		public const string TRANS_MSK = "I_TRANS_MSK";
		public const string TRANS_MSK_TEXT = "РЕЕСТР_ПЕРЕДАЧА_МСК_НА_НЧТП";

		public static string GetDocTypeText(string code)
		{
			switch (code.ToUpper())
			{
				case RECALL_MSK: return RECALL_MSK_TEXT;
				case TRANS_MSK: return TRANS_MSK_TEXT;
				default: return code;
			}
		}

		public class ImportDocumentInfo : DocumentInfo
		{
			[XmlElement("КоличествоОрганизаций")]
			public CountInfo CountData { get; set; }
		}

		public class RegisterItem
		{
			public class OrganizationInfo
			{
				[XmlElement("КодОрганизации")]
				public string Code { get; set; }
				[XmlElement("НаименованиеОрганизации")]
				public string Name { get; set; }
				[XmlElement("КодПортфеля")]
				public string PortfolioCode { get; set; }
				[XmlElement("НаименованиеПортфеля")]
				public string PortfolioName { get; set; }
				[XmlElement("ИНН")]
				public string INN { get; set; }
				[XmlElement("ТипОрганизации")]
				public string OrgType { get; set; }
			}

			public class SummInfo
			{
				[XmlElement("ВзносыОПС")]
				public decimal OPS { get; set; }
				[XmlElement("ИДпоОПС")]
				public decimal OPSInvest { get; set; }
				[XmlElement("ВзносыДСВ")]
				public decimal DSV { get; set; }
				[XmlElement("ИДпоДСВ")]
				public decimal DSVInvest { get; set; }
				[XmlElement("ВзносыСофин")]
				public decimal SoFin { get; set; }
				[XmlElement("ИДпоСофин")]
				public decimal SoFinInvest { get; set; }
				[XmlElement("СредстваМСК")]
				public decimal MSK { get; set; }
				[XmlElement("ИДпоМСК")]
				public decimal MSKInvest { get; set; }
				[XmlElement("ВсегоСПН")]
				public decimal TotalSPN { get; set; }

				/// <summary>
				/// Сумма всех типов, без инвестдохода
				/// </summary>
				[XmlIgnore]
				public decimal SummWithoutInvest
				{
					get { return OPS + DSV + SoFin + MSK; }
				}

				/// <summary>
				/// Сумма всех инвестдоходов
				/// </summary>
				[XmlIgnore]
				public decimal SummInvest
				{
					get { return OPSInvest + DSVInvest + SoFinInvest + MSKInvest; }
				}
			}

			[XmlElement("НомерВпачке")]
			public int OrderNumber { get; set; }

			[XmlElement("Организация")]
			public OrganizationInfo Organization { get; set; }

			[XmlElement("КоличествоЗЛ")]
			public int ZLCount { get; set; }

			[XmlElement("Сумма")]
			public SummInfo Summ { get; set; }
		}


		[XmlElement("ПлатежныйРеестр")]
		public List<RegisterItem> Items { get; set; }

		[XmlElement("ОПИСЬ")]
		public ImportDocumentInfo Info { get; set; }

		public bool ValidateFields()
		{
			var sumUK = Items.Where(i => i.Organization.OrgType == "УК").Sum(i => i.Summ.TotalSPN);
			var sumNPF = Items.Where(i => i.Organization.OrgType == "НПФ").Sum(i => i.Summ.TotalSPN);
			var sumTotalUK = this.Info.CountData.TotalSumUK;
			var sumTotalNPF = this.Info.CountData.TotalSumNPF;

			if (sumUK != sumTotalUK)
				return false;
			if (sumNPF != sumTotalNPF)
				return false;

			return true;
		}

	}
}
