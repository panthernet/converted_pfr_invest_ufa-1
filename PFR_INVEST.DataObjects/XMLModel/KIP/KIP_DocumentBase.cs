﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Reflection;
using System.Xml;

namespace PFR_INVEST.DataObjects.XMLModel.KIP
{
	[XmlRoot("ФайлПФР")]
	public class KIP_DocumentBase
	{
		public static XmlReader GetShema(string shemaName)
		{
			Assembly thisAssembly = Assembly.GetAssembly(typeof(KIP_DocumentBase));
			string path = "PFR_INVEST.DataObjects.XMLModel.KIP.Shemas";
			return new XmlTextReader(thisAssembly.GetManifestResourceStream(path + "." + shemaName));
		}

		public class DocType
		{
			[XmlElement("Наименование")]
			public string Name { get; set; }
			[XmlElement("Код")]
			public string Code { get; set; }
		}

		/// <summary>
		/// Информация о связаном документе
		/// </summary>
		public class SourceDocumentInfo
		{
			[XmlElement("ТипДокумента")]
			public string Type { get; set; }

			[XmlIgnore]
			public DateTime Date { get; set; }

			[XmlElement("ДатаДокумента")]
			public string DateText
			{
				get { return Date.ToXmlString(XMLTools.KIP_DATE_FORMAT); }
				set { Date = value.ParseAsDate(XMLTools.KIP_DATE_FORMAT); }
			}

			[XmlElement("НомерДокумента")]
			public string DocNumber { get; set; }
		}

		public class CountInfo
		{
			[XmlElement("КоличествоУК")]
			public int CountUK { get; set; }
			[XmlElement("ОбщаяСуммаУК")]
			public decimal TotalSumUK { get; set; }
			[XmlElement("КоличествоНПФ")]
			public int CountNPF { get; set; }
			[XmlElement("ОбщаяСуммаНПФ")]
			public decimal TotalSumNPF { get; set; }

			public CountInfo()
			{
				TotalSumUK = TotalSumNPF = 0.00m;
			}
		}

		public class DocumentInfo
		{

			[XmlElement("ТипДокумента")]
			public DocType DocumentType { get; set; }

			[XmlIgnore]
			public DateTime Date { get; set; }

			[XmlElement("ДатаДокумента")]
			public string DateText
			{
				get { return Date.ToXmlString(XMLTools.KIP_DATE_FORMAT); }
				set { Date = value.ParseAsDate(XMLTools.KIP_DATE_FORMAT); }
			}

			[XmlElement("НомерДокумента")]
			public string DocNumber { get; set; }

			[XmlElement("ТипОперационногоПериода")]
			public string PeriodType { get; set; }

			[XmlIgnore]
			public DateTime Period { get; set; }

			[XmlElement("ОперационныйПериод")]
			public string PeriodText
			{
				get { return Period.ToXmlString(XMLTools.KIP_DATE_FORMAT); }
				set { Period = value.ParseAsDate(XMLTools.KIP_DATE_FORMAT); }
			}


		}


	}
}
