﻿using System.Xml.Serialization;

namespace PFR_INVEST.DataObjects.XMLModel.KIP
{
	[XmlRoot("ФайлПФР")]
	public class KIP_Receipt:KIP_DocumentBase
	{
		public const string VERIFIED_STEP2 = "I_VERIFIED_STEP2";
		public const string ERROR_STEP2 = "I_ERROR_STEP2";

		public class ReceiptDocumentInfo : DocumentInfo
		{
			[XmlElement("РеквизитыДокумента-основания")]
			public SourceDocumentInfo SourceDocument { get; set; }
		}


		[XmlElement("ОПИСЬ")]
		public ReceiptDocumentInfo Info { get; set; }
	}
}
