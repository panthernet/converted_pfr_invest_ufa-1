﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;
using PFR_INVEST.DataObjects.ListItems;


namespace PFR_INVEST.DataObjects.XMLModel.Auction
{
	/// <summary>
	/// Класс для сериализации "Ставка отсечения" для биржи СПВБ
	/// </summary>	
	[XmlRoot("SPCEX_DOC")]
	public class SPVB_CutoffRate : SPVB_BaseDocument
	{
		public class ActionItem
		{
			[XmlAttribute("reason")]
			public string Reason { get; set; }

			[XmlAttribute("price")]
			public decimal Rate { get; set; }
	
			[XmlAttribute("comment")]
			public string Comment { get; set; }
		}

		public class DocBodyWraper
		{
			[XmlElement("auction_command")]
			public DocBody Body { get; set; }
		}

		public class DocBody
		{
			[XmlIgnore]
			public DateTime SelectDate { get; set; }
			[XmlAttribute("limit_date")]
			public string SelectDateText
			{
				get { return SelectDate.ToXmlString(); }
				set { SelectDate = value.ParseAsDate(); }
			}

			[XmlAttribute("executor_phone")]
			public string ExecutorPhone { get; set; }

			[XmlAttribute("executor_name")]
			public string ExecutorName { get; set; }

			[XmlAttribute("records")]
			public int Count { get; set; }

			[XmlAttribute("finstr")]
			public string Code { get; set; }

			[XmlAttribute("max_vol")]
			public decimal MaxVolume { get; set; }

			[XmlElement("participant_limit")]
			public List<ActionItem> Actions { get; set; }

			

			public DocBody()
			{
				this.Actions = new List<ActionItem>();

			}

		}

		[Obsolete("Only for XML Serializer", true)]
		public SPVB_CutoffRate() : this(new DepClaimSelectParams()) { }

		public SPVB_CutoffRate(DepClaimSelectParams auction)
			: base(auction)
		{
			this.Sender = AddreseInfoWraper.PFR;
			this.Reciver = AddreseInfoWraper.SPVB;
			this.Document = new DocBodyWraper() { Body = new DocBody() { ExecutorPhone = "+74959820604", ExecutorName = "Пенсионный Фонд Российской Федерации" } };
			this.Document.Body.SelectDate = auction.SelectDate;
			this.Document.Body.Code = auction.SPVBCode;
			this.Document.Body.MaxVolume = auction.MaxInsuranceFee;

			this.Document.Body.Count = 1;
			this.Document.Body.Actions.Add(new ActionItem() { Reason="Y", Rate=auction.CutoffRate});
		}

		public override string FileType
		{
			get { return "AucCmd"; }
		}

		public override string DocType
		{
			get { return "auction_command"; }
		}

		[XmlElement("document")]
		public DocBodyWraper Document { get; set; }

	}
}
