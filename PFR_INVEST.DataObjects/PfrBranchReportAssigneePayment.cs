﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportAssigneePayment : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual decimal TotalPayment { get; set; }

        [DataMember]
        public virtual decimal OwnReservePayment { get; set; }

        [DataMember]
        public virtual decimal PFRReservePayment { get; set; }

        [DataMember]
        public virtual long ReportId { get; set; }
    }
}

