﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SIUKPlan : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? TransferID { get; set; }

        [DataMember]
        public virtual long? Transfer2ID { get; set; }

        [DataMember]
        public virtual long? TransferListID { get; set; }

        [DataMember]
        public virtual long? MonthID { get; set; }

        [DataMember]
        public virtual decimal? PlanSum { get; set; }

        [DataMember]
        public virtual decimal? FactSum { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        /// <summary>
        /// Возвращает True если список перечислений связан с ходя бы одним перечислением
        /// </summary>
        [IgnoreDataMember]
        public virtual bool HasReqTransfer { get { return TransferID != null || Transfer2ID != null; } }

        [IgnoreDataMember]
        public virtual bool IsTransferIdsNonZero { get { return TransferID != 0 || Transfer2ID != 0; } }

        /// <summary>
        /// Присваивает Плану УК перечисление, используя свободный слот
        /// </summary>
        /// <param name="id">Идентификатор пеерчисления</param>
        public virtual void SetTransferId(long id)
        {
            if (TransferID == null) TransferID = id;
            else if (Transfer2ID == null) Transfer2ID = id;
            else throw new Exception("План УК уже содержит ссылки на 2 пеерчисления! Проверьте логику!");
        }
        /// <summary>
        /// Удаляет связь Плана УК с перечислением
        /// </summary>
        /// <param name="id">Идентификатор пеерчисления</param>
        public virtual void UnsetTransferId(long id)
        {
            if (TransferID == id) TransferID = null;
            else if (Transfer2ID == id) Transfer2ID = null;
        }

        /// <summary>
        /// Добавление к фактической сумме плана УК при добавлении плану нового перечисления
        /// </summary>
        /// <param name="factSum">Значение суммы</param>
        public virtual void AddFactSum(decimal? factSum)
        {
            if(!factSum.HasValue) return;
            FactSum = FactSum + factSum ?? factSum.Value;
        }
    }
}
