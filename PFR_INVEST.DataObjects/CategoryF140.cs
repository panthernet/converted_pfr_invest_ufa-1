﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class CategoryF140 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string ViolationTypeCode { get; set; }

        [DataMember]
        public virtual string ViolationType { get; set; }

        [DataMember]
        public virtual string ViolationCharacterCode { get; set; }

        [DataMember]
        public virtual string ViolationCharacter { get; set; }
    }
}

