﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using PFR_INVEST.DataObjects.ServiceItems;

namespace PFR_INVEST.DataObjects
{
    public class DeleteDocumentEntry : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Operation { get; set; }

        [DataMember]
        public virtual string DocumentType { get; set; }

        [DataMember]
        public virtual string DocumentCaption { get; set; }

        [DataMember]
        public virtual long? DocumentID { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        [DataMember]
        public virtual string RelatedData { get; set; }
        
        public virtual DeletedDataItem Data { get; set; }

        public virtual void Unwrap()
        {
            Data = !string.IsNullOrEmpty(RelatedData) ? JsonConvert.DeserializeObject<DeletedDataItem>(RelatedData) : null;
        }
    }
}
