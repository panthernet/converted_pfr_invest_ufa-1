﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
    public class Quark : BaseDataObject
    {
        [DataMember]
        public virtual int Id { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual int FirstMonth { get; set; }
        [DataMember]
        public virtual int LastMonth { get; set; }
        [DataMember]
        public virtual int Index { get; set; }
    }
}
