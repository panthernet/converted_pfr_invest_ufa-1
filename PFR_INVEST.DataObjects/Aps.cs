﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    
    public class Aps : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? OperationDate { get; set; }

        [DataMember]
        public virtual DateTime? DocDate { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual decimal? Summ { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long? KindID { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual long? PFRBankAccountID { get; set; }

        [DataMember]
        public virtual long? SPortfolioID { get; set; }

        [DataMember]
        public virtual long? SPFRBankAccountID { get; set; }

    }
}
