﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class DepClaimSelectParams : BaseDataObject, IMarkedAsDeleted
    {
        public enum Statuses
        {
            [Description("Неизвестный")]
            Unknown = -1,
            [Description("Новый")]
            New = 1,
            [Description("Загружена Выписка из реестра заявок")]
            DepClaimImported = 2,
            [Description("Загружена Выписка из реестра заявок, подлежащих удовлетворению")]
            DepClaimConfirmImported = 3,
            [Description("Сгенерированы оферты")]
            OfferCreated = 4,
            [Description("Оферты подписаны")]
            OfferSigned = 5,
            [Description("Оферты акцептованы/не акцептованы")]
            OfferAcceptedOrNo = 6,
            [Description("Сформированы депозиты")]
            DepositSettled = 8
        }

        [IgnoreDataMember]
        public virtual string AuctionStepTip => GetAuctionStepTip(AuctionStep);

        public static string GetAuctionStepTip(int step)
        {
            switch (step)
            {
                case 0:
                    return "Новый";
                case 2:
                    return "Аукцион завершен";
                default:
                    return "В процессе";
            }
        }

        [IgnoreDataMember]
        public virtual int AuctionStep
        {
            get
            {
                switch (AuctionStatus)
                {
                    case Statuses.New:
                        return 0;
                    case Statuses.OfferCreated:
                    case Statuses.OfferSigned:
                    case Statuses.OfferAcceptedOrNo:
                    case Statuses.DepositSettled:
                        return 2;
                    default:
                        return 1;
                }
            }
        }

        public DepClaimSelectParams()
        {
            this.AuctionStatus = Statuses.New;
        }
        private int _DepClaim2Count;

        [DataMember]
        private int _step;
        public virtual int Step
        {
            get { return _step; }
            set { _step = value; OnPropertyChanged(nameof(Step)); }
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private int _auctionNum;
        public virtual int AuctionNum
        {
            get { return _auctionNum; }
            set
            {
                if (_auctionNum != value)
                {
                    _auctionNum = value;
                    OnPropertyChanged("AuctionNum");
                }
            }
        }

		[DataMember]
		private int _localNum;
		public virtual int LocalNum
		{
			get { return _localNum; }
			set
			{
				if (_localNum != value)
				{
					_localNum = value;
					OnPropertyChanged("LocalNum");
				}
			}
		}

        [DataMember]
        private int _maxDepClaimAmount = 5;
        public virtual int MaxDepClaimAmount
        {
            get { return _maxDepClaimAmount; }
            set
            {
                if (_maxDepClaimAmount != value)
                {
                    _maxDepClaimAmount = value;
                    OnPropertyChanged("MaxDepClaimAmount");
                }
            }
        }

        [DataMember]
        private int _period;
        public virtual int Period
        {
            get { return _period; }
            set
            {
                if (_period != value)
                {
                    _period = value;
                    OnPropertyChanged("Period");
                }
            }
        }

        [DataMember]
        private decimal _maxInsuranceFee;
        public virtual decimal MaxInsuranceFee
        {
            get { return _maxInsuranceFee; }
            set
            {
                if (_maxInsuranceFee != value)
                {
                    _maxInsuranceFee = value;
                    OnPropertyChanged("MaxInsuranceFee");
                }
            }
        }

        [DataMember]
        private decimal _minDepClaimVolume = 200;
        public virtual decimal MinDepClaimVolume
        {
            get { return _minDepClaimVolume; }
            set
            {
                if (_minDepClaimVolume != value)
                {
                    _minDepClaimVolume = value;
                    OnPropertyChanged("MinDepClaimVolume");
                }
            }
        }

        [DataMember]
        private decimal _minRate;
        public virtual decimal MinRate
        {
            get { return _minRate; }
            set
            {
                if (_minRate != value)
                {
                    _minRate = value;
                    OnPropertyChanged("MinRate");
                }
            }
        }

		[DataMember]
		private decimal _cutoffRate;
		public virtual decimal CutoffRate
        {
			get { return _cutoffRate; }
            set
            {
				if (_cutoffRate != value)
                {
					_cutoffRate = value;
					OnPropertyChanged("CutoffRate");
                }
            }
        }
		

        [DataMember]
        private DateTime _placementDate;
        public virtual DateTime PlacementDate
        {
            get { return _placementDate; }
            set
            {
                if (_placementDate != value)
                {
                    _placementDate = value;
                    OnPropertyChanged("PlacementDate");
                }
            }
        }

        [DataMember]
        private DateTime _returnDate;
        public virtual DateTime ReturnDate
        {
            get { return _returnDate; }
            set
            {
                if (_returnDate != value)
                {
                    _returnDate = value;
                    OnPropertyChanged("ReturnDate");
                }
            }
        }


        /// <summary>
        /// Дата проведения аукциона
        /// </summary>
        [DataMember]
        private DateTime _selectDate;
        public virtual DateTime SelectDate
        {
            get { return _selectDate; }
            set
            {
                if (_selectDate != value)
                {
                    _selectDate = value;
                    OnPropertyChanged("SelectDate");
                }
            }
        }

        [DataMember]
        private TimeSpan _depClaimAcceptEnd;
        public virtual TimeSpan DepClaimAcceptEnd
        {
            get { return _depClaimAcceptEnd; }
            set
            {
                if (_depClaimAcceptEnd != value)
                {
                    _depClaimAcceptEnd = value;

                    OnPropertyChanged("DepClaimAcceptEnd");
                }
            }
        }

        [DataMember]
        private TimeSpan _depClaimAcceptStart;
        public virtual TimeSpan DepClaimAcceptStart
        {
            get { return _depClaimAcceptStart; }
            set
            {
                if (_depClaimAcceptStart != value)
                {
                    _depClaimAcceptStart = value;
                    OnPropertyChanged("DepClaimAcceptStart");
                }
            }
        }

        [DataMember]
        private TimeSpan _depClaimListEnd;
        public virtual TimeSpan DepClaimListEnd
        {
            get { return _depClaimListEnd; }
            set
            {
                if (_depClaimListEnd != value)
                {
                    _depClaimListEnd = value;
                    OnPropertyChanged("DepClaimListEnd");
                }
            }
        }

        [DataMember]
        private TimeSpan _depClaimListStart;
        public virtual TimeSpan DepClaimListStart
        {
            get { return _depClaimListStart; }
            set
            {
                if (_depClaimListStart != value)
                {
                    _depClaimListStart = value;
                    OnPropertyChanged("DepClaimListStart");
                }
            }
        }

        [DataMember]
        private TimeSpan _filterEnd;
        public virtual TimeSpan FilterEnd
        {
            get { return _filterEnd; }
            set
            {
                if (_filterEnd != value)
                {
                    _filterEnd = value;
                    OnPropertyChanged("FilterEnd");
                }
            }
        }

        [DataMember]
        private TimeSpan _filterStart;
        public virtual TimeSpan FilterStart
        {
            get { return _filterStart; }
            set
            {
                if (_filterStart != value)
                {
                    _filterStart = value;
                    OnPropertyChanged("FilterStart");
                }
            }
        }

        [DataMember]
        private string _depClaimSelectLocation;
        public virtual string DepClaimSelectLocation
        {
            get { return _depClaimSelectLocation; }
            set
            {
                if (_depClaimSelectLocation != value)
                {
                    _depClaimSelectLocation = value;
                    OnPropertyChanged("DepClaimSelectLocation");
                }
            }
        }

        [DataMember]
        private TimeSpan _offerReceiveEnd;
        public virtual TimeSpan OfferReceiveEnd
        {
            get { return _offerReceiveEnd; }
            set
            {
                if (_offerReceiveEnd != value)
                {
                    _offerReceiveEnd = value;
                    OnPropertyChanged("OfferReceiveEnd");
                }
            }
        }

        [DataMember]
        private TimeSpan _offerReceiveStart;
        public virtual TimeSpan OfferReceiveStart
        {
            get { return _offerReceiveStart; }
            set
            {
                if (_offerReceiveStart != value)
                {
                    _offerReceiveStart = value;
                    OnPropertyChanged("OfferReceiveStart");
                }
            }
        }

        [DataMember]
        private TimeSpan _offerSendEnd;
        public virtual TimeSpan OfferSendEnd
        {
            get { return _offerSendEnd; }
            set
            {
                if (_offerSendEnd != value)
                {
                    _offerSendEnd = value;
                    OnPropertyChanged("OfferSendEnd");
                }
            }
        }

        [DataMember]
        private TimeSpan _offerSendStart;
        public virtual TimeSpan OfferSendStart
        {
            get { return _offerSendStart; }
            set
            {
                if (_offerSendStart != value)
                {
                    _offerSendStart = value;
                    OnPropertyChanged("OfferSendStart");
                }
            }
        }



        [IgnoreDataMember]
        public virtual Statuses AuctionStatus
        {
            get { return (Statuses)this.AuctionStatusID; }
            set { this.AuctionStatusID = (int)value; }
        }

        [IgnoreDataMember]
        public virtual string SecurityId => $"PFDEP_T{Period:D3}DT";


        [DataMember]
        public virtual int AuctionStatusID { get; set; }

        [DataMember]
        private long _statusID = 1;
        public virtual long StatusID
        {
            get { return _statusID; }
            set
            {
                if (_statusID != value)
                {
                    _statusID = value;
                    OnPropertyChanged("StatusID");
                }
            }
        }

		[DataMember]
		private long _stock = 1;
		public virtual long StockId
		{
			get { return _stock; }
			set
			{
				if (_stock != value)
				{
					_stock = value;
					OnPropertyChanged("StockId");
				}
			}
		}

        [DataMember]
        private string _stockName = null;
        public virtual string StockName
        {
            get { return _stockName; }
            set
            {
                if (_stockName != value)
                {
                    _stockName = value;
                    OnPropertyChanged("StockName");
                }
            }
        }

		[DataMember]
		private string _SPVBCode = null;
		/// <summary>
		/// Код аукциона на бирже СПВБ(используется для xml документов)
		/// </summary>
		public virtual string SPVBCode
		{
			get { return _SPVBCode; }
			set
			{
				if (_SPVBCode != value)
				{
					_SPVBCode = value;
					OnPropertyChanged("SPVBCode");
				}
			}
		}

        #region ClaimCounts

        private int _DepClaimMaxCount;
        /// <summary>
        ///  Количество заявок c максимальной суммой. Поле только для вычитки из базы.
        /// </summary>
        [DataMember]
        public virtual int DepClaimMaxCount
        {
            get { return _DepClaimMaxCount; }
            set { if (_DepClaimMaxCount != value) { _DepClaimMaxCount = value; OnPropertyChanged("DepClaimMaxCount"); } }
        }

        private int _DepClaimCommonCount;
        /// <summary>
        ///  Количество сводных заявок. Поле только для вычитки из базы.
        /// </summary>
        [DataMember]
        public virtual int DepClaimCommonCount
        {
            get { return _DepClaimCommonCount; }
            set { if (_DepClaimCommonCount != value) { _DepClaimCommonCount = value; OnPropertyChanged("DepClaimCommonCount"); } }
        }



        /// <summary>
        /// Количество заявок. Поле только для вычитки из базы.
        /// </summary>
        [DataMember]
        public virtual int DepClaim2Count
        {
            get { return _DepClaim2Count; }
            set { if (_DepClaim2Count != value) { _DepClaim2Count = value; OnPropertyChanged("DepClaim2Count"); } }
        }

        #endregion


        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Period": return Period.ValidateNonNegative();
                    case "MaxDepClaimAmount": return MaxDepClaimAmount.ValidateNonNegative();
                    case "MaxInsuranceFee": return MaxInsuranceFee.ValidateNonNegative();
                    case "MinDepClaimVolume": return MinDepClaimVolume.ValidateNonNegative();
                    case "MinRate": return MinRate.ValidateNonNegative();

                    default: return null;
                }
            }
        }

        [IgnoreDataMember]
        public virtual string AuctionStatusText
        {
            get
            {
                switch (AuctionStatus)
                {
                    case Statuses.New: return "Новый";
                    case Statuses.DepClaimImported:
                        if (DepClaimMaxCount == 0 && DepClaimCommonCount == 0)
                            return "Загружена Выписка из реестра заявок";

                        if (DepClaimMaxCount == 0 && DepClaimCommonCount > 0)
                            return "Загружены Выписка из реестра заявок и Сводный реестр заявок";

                        if (DepClaimMaxCount > 0 && DepClaimCommonCount == 0)
                            return "Загружены Выписка из реестра заявок и Сводный реестр заявок, с учетом максимальной суммы размещения";

                        if (DepClaimMaxCount > 0 && DepClaimCommonCount > 0)
                            return "Загружены Выписка из реестра заявок, Сводный реестр заявок и Сводный реестр заявок, с учетом максимальной суммы размещения";

                        return "";
                    case Statuses.DepClaimConfirmImported: return "Загружена Выписка из реестра заявок, подлежащих удовлетворению";
                    case Statuses.OfferCreated: return "Сгенерированы оферты";
                    case Statuses.OfferSigned: return "Оферты подписаны";
                    case Statuses.OfferAcceptedOrNo: return "Оферты акцептованы/не акцептованы";
                    case Statuses.DepositSettled: return "Сформированы депозиты";
                    default: return string.Empty;
                }
            }
        }

        public virtual bool CanImportDepClaim2()
        {
            return AuctionStatus == Statuses.New || 
                (AuctionStatus == Statuses.DepClaimImported &&
                 DepClaimMaxCount == 0 && 
                 DepClaimCommonCount == 0
                 );
        }

        public virtual bool CanImportDepClaim2Confirm()
        {
            return AuctionStatus == Statuses.New ||
                (AuctionStatus == Statuses.DepClaimImported &&
                 DepClaimMaxCount == 0 &&
                 DepClaimCommonCount == 0
                 );
        }

        
    }
}
