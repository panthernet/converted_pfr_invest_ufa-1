﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace PFR_INVEST.XmlSchemas
{
    public class DocumentBase
    {
        [XmlElement("DOC_REQUISITIONS")]
        public DocumentInfo Info { get; set; }


        public static XmlReader GetShema(string shemaName)
        {
            Assembly thisAssembly = Assembly.GetAssembly(typeof(DocumentBase));
            string path = "PFR_INVEST.XmlSchemas.Schemas";
            return new XmlTextReader(thisAssembly.GetManifestResourceStream(path + "." + shemaName));
        }

        public static XmlSchema GetShemaWithIncludes(string shemaName)
        {
            Assembly thisAssembly = Assembly.GetAssembly(typeof(DocumentBase));
            string path = "PFR_INVEST.XmlSchemas.Schemas";
            var stream = thisAssembly.GetManifestResourceStream(path + "." + shemaName);

            if (stream == null)
                throw new FileNotFoundException($"Файл '{shemaName}' не найден.");

            var schema = XmlSchema.Read(stream, null);

            foreach (var o in schema.Includes)
            {
                var xmlInclude = (XmlSchemaExternal) o;
                var includeStream = thisAssembly.GetManifestResourceStream(path + "." + xmlInclude.SchemaLocation);

                if (includeStream == null)
                    throw new FileNotFoundException($"Файл '{xmlInclude.SchemaLocation}' не найден.");

                xmlInclude.Schema = XmlSchema.Read(includeStream, null);
            }

            return schema;
        }
        

        public static bool IsValidFileName(string path)
        {
            path = Path.GetFileNameWithoutExtension(path);
            string[] splitPath = path.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

            if (splitPath.Length != 4)
            {
                return false;
            }
            string date = splitPath[2];
            DateTime dt;

            return date.Length == 6 && splitPath[3].Length == 9 && Regex.Match(splitPath[3], @"[a-zA-Z0-9]{9}").Success && DateTime.TryParse(
                $"{date.Substring(0, 2)}.{date.Substring(2, 2)}.{date.Substring(4, 2)}", new CultureInfo("ru-RU"), DateTimeStyles.None, out dt);
        }
    }
}
