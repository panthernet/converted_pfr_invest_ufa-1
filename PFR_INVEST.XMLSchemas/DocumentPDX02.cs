﻿using System;
using System.Xml.Serialization;

namespace PFR_INVEST.XmlSchemas
{
    [Serializable]
    [XmlRoot("MICEX_DOC")]
    public class DocumentPDX02 : DocumentBase
    {
        //[XmlElement("DOC_REQUISITIONS")]
        //public DocumentInfo Info { get; set; }

        [XmlElement("PDX02")]
        public PDX02Body Body { get; set; }        
    }

    [Serializable]
    public class PDX02Body
    {
        [XmlAttribute("VER")]
        public string Version { get; set; }

        [XmlElement("PDX02_REC")]
        public PDX02Record[] Records { get; set; }
    }

    [Serializable]
    public class PDX02Record
    {
        [XmlAttribute("FIRMID")]
        public string FIRMID { get; set; }

        [XmlAttribute("FIRMNAME")]
        public string FIRMNAME { get; set; }

        [XmlAttribute("RECNUM")]
        public long RECNUM { get; set; }

        [XmlAttribute("SECURITYID")]
        public string SECURITYID { get; set; }

        [XmlAttribute("RATE")]
        public decimal RATE { get; set; }

        [XmlAttribute("AMOUNT")]
        public decimal AMOUNT { get; set; }

        [XmlAttribute("SETTLEDATE")]
        public DateTime SETTLEDATE { get; set; }

        [XmlAttribute("PAYMENT")]
        public decimal PAYMENT { get; set; }

        [XmlAttribute("SETTLEDATE2")]
        public DateTime RETURNDATE { get; set; }

    }
}
