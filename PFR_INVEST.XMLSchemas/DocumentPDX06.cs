﻿using System;
using System.Xml.Serialization;

namespace PFR_INVEST.XmlSchemas
{
    [Serializable]
    [XmlRoot("MICEX_DOC")]
    public class DocumentPDX06:DocumentBase
    {
        [XmlElement("PDX06")]
        public PDX06Body Body { get; set; }  
    }

    [Serializable]
    public class PDX06Body
    {
        [XmlAttribute("VER")]
        public string Version { get; set; }

        [XmlElement("PDX06_TAB")]
        public PDX06Tab Tab { get; set; }

        [XmlIgnore]
        public PDX06Record[] Records 
        {
            get { return this.Tab.Board.Group.Records; }
        }
        
    }

    [Serializable]
    public class PDX06Tab
    {
        [XmlElement("BOARD")]
        public PDX06Board Board { get; set; }

        [XmlAttribute("TRADE_DATE")]
        public DateTime TradeDate { get; set; }
    }

    [Serializable]
    public class PDX06Board
    {
        [XmlElement("GROUP")]
        public PDX06Group Group { get; set; }
    }

    [Serializable]
    public class PDX06Group
    {
        [XmlAttribute("SECURITYID")]
        public string SecurityID { get; set; }

        [XmlAttribute("SUM_VALUE")]
        public string SummValue { get; set; }

        [XmlElement("PDX06_REC")]
        public PDX06Record[] Records { get; set; }
    }

    [Serializable]
    public class PDX06Record 
    {
        [XmlAttribute("FIRMID2")]
        public string FIRMID { get; set; }

        [XmlAttribute("FIRMNAME2")]
        public string FIRMNAME { get; set; }

        [XmlAttribute("REC_NUMBER")]
        public long RECNUM { get; set; }

		[XmlAttribute("ORDER_NUMBER2")]
		public long ORDER_NUMBER { get; set; }

        [XmlAttribute("SECURITYID")]
        public string SECURITYID { get; set; }

        [XmlAttribute("RATE")]
        public decimal RATE { get; set; }

        [XmlAttribute("VALUE")]
        public decimal AMOUNT { get; set; }

        [XmlAttribute("SETTLEDATE")]
        public DateTime SETTLEDATE { get; set; }

        [XmlAttribute("INTEREST")]
        public decimal PAYMENT { get; set; }

        [XmlAttribute("SETTLEDATE2")]
        public DateTime RETURNDATE { get; set; }

        [XmlAttribute("TERM")]
        public long TERM { get; set; }

        [XmlAttribute("PART2_SUM")]
        public decimal PART2_SUM { get; set; }
    }
}
