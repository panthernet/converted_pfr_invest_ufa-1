﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PFR_INVEST.SettingsService.Contract;

namespace PFR_INVEST.MetricService.BL
{
	public class UserSettingsServiceClient : System.ServiceModel.ClientBase<ISettingsService>
	{
		internal UserSettingsServiceClient()
			: this("WS2007HttpBinding_ISettingsService") { }

		internal UserSettingsServiceClient(string endpointConfigurationName)
			: base(endpointConfigurationName) 
		{
			this.Endpoint.Binding.CloseTimeout = TimeSpan.FromSeconds(10);
		}

		public ISettingsService Client
		{
			get
			{
				return base.Channel;
			}
		}
	}
}