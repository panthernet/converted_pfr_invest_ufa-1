﻿using System.Collections.Generic;
using System.Web.Http;
using PFR_INVEST.MetricService.BL;

namespace PFR_INVEST.MetricService.Controllers
{
	public class MetricController : ApiController
	{
		// GET api/metric
		public IEnumerable<MetricItem> Get()
		{
			return MetricFactory.GetMetric();
		}

		
	}
}