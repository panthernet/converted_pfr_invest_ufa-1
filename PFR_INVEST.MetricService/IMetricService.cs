﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using PFR_INVEST.MetricService.BL;

namespace PFR_INVEST.MetricService.Controllers
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMetricService" in both code and config file together.
	[ServiceContract]
	public interface IMetricService
	{
		[OperationContract]
		IEnumerable<MetricItem> GetMetric();
	}
}
