﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;

namespace DB2toPostgreSQLMigration
{
    class Program
    {
        private static ISession db2;
        private static ISession psql;

        static void Main(string[] args)
        {
            CreateDB2Factory();
            CreatePSQLFactory();
            Process();
        }

        private static ISession OpenPSQLSession()
        {
            return _psqlFactory.OpenSession();
        }

        private static ISession OpenDB2Session()
        {
            return _db2Factory.OpenSession();
        }

        private static void CreatePSQLFactory()
        {
            var configuration = new Configuration();
            var configFile = "hibernate.postgre.cfg.xml";
            var mapFolder = "Mapping/PostgreSQL/PFR_BASIC";

            configuration.Configure(Path.Combine(BASE_DIR, configFile));

            configuration.AddDirectory(new DirectoryInfo(Path.Combine(BASE_DIR, mapFolder)));
            _psqlFactory = configuration.BuildSessionFactory();
        }

        const string BASE_DIR = "../../../../db2connector";
        private static ISessionFactory _psqlFactory;
        private static ISessionFactory _db2Factory;

        private static void CreateDB2Factory()
        {
            var configuration = new Configuration();
            var configFile = "hibernate.db2.cfg.xml";
            var mapFolder = "Mapping/DB2/PFR_BASIC";

            configuration.Configure(Path.Combine(BASE_DIR, configFile));
            configuration.Properties["connection.connection_string"] = "DSN=PFR-INVEST;CurrentSchema=PFR_BASIC;LONGDATACOMPAT=1;";
            configuration.AddDirectory(new DirectoryInfo(Path.Combine(BASE_DIR, mapFolder)));
            _db2Factory = configuration.BuildSessionFactory();;
        }


        public static void Process()
        {
            Debug.WriteLine("MIG started!");


            /*
            BulkCopyType<AccBankType>();
            BulkCopyType<PaymentDetail>();
            BulkCopyType<Division>();
            BulkCopyType<PaymentAssignment>();
            BulkCopyType<Rating>();
            BulkCopyType<RatingAgency>();

            BulkCopyType<AccOperationKind>();
            BulkCopyType<AccountKind>();
            BulkCopyType<AdditionalDocumentInfo>();
            BulkCopyType<ApproveDoc>();
            BulkCopyType<ApsKind>();
            BulkCopyType<AttachClassific>();
            BulkCopyType<BankStockCode>();
            BulkCopyType<ContractName>();
            BulkCopyType<Currency>();
            BulkCopyType<Department>();
            BulkCopyType<DetailsKind>();
            BulkCopyType<Element>();
            BulkCopyType<SPNDirection>();
            BulkCopyType<DocumentClass>();
            BulkCopyType<DocType>();
            BulkCopyType<FEDO>();
            BulkCopyType<FZ>();
            BulkCopyType<KBK>();
            BulkCopyType<KBKHistory>();
            BulkCopyType<SecurityKind>();
            BulkCopyType<LegalStatus>();
            BulkCopyType<Month>();
            BulkCopyType<NpfDocType>();
            BulkCopyType<NPFErrorCode>();
            BulkCopyType<SPNOperation>();
            BulkCopyType<ReorganizationType>();
            BulkCopyType<RrzKind>();
            BulkCopyType<SIReportType>();
            BulkCopyType<Status>();
            BulkCopyType<Stock>();
            BulkCopyType<UKReportType>();
            BulkCopyType<VRReportType>();
            BulkCopyType<Year>();

            //post
            BulkCopyType<Portfolio>();*/

            //ADDSPN_TYPE
            Debug.WriteLine("MIG complete!");

        }


        private static void BulkCopyType<T>(bool preserveId = true)
            where T : class, IIdentifiable
        {
            Debug.WriteLine("MIG " + typeof(T).Name + "...");
            var list = new List<T>();
            using (var db2 = OpenDB2Session())
            {
                var nlist = db2.Query<T>().ToList();
                //превращаем хиб класс в базовый
                nlist.ForEach(a =>
                {
                    list.Add(HibObjectsToDtoConverter.GetDtoObject(a, typeof(T)) as T);
                });
            }

            using (var psql = OpenPSQLSession())
            {
                var idList = psql.CreateQuery(string.Format(@"select x.id from {0} x", typeof(T).Name)).List<long>();
                using (var t = psql.BeginTransaction())
                {
                    try
                    {
                        foreach (var a in list)
                            if (idList.Contains(a.ID)) psql.Update(a, a.ID);
                            else
                            {
                                if (preserveId)
                                    psql.Save(a, a.ID);
                                else psql.Save(a);
                            }
                        t.Commit();
                    }
                    catch { t.Rollback(); }
                }
                //var test = psql.CreateCriteria(typeof (T)).List<T>();
            }
        }
    }
}
