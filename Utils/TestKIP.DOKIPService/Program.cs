﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace TestKIP.DOKIPService
{
	class Program
	{
		static void Main(string[] args)
		{


			try
			{
				var baseFolder = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
				var zipFolder = Path.Combine(baseFolder, "InputFiles");

				var files = Directory.GetFiles(zipFolder, "*.zip");
				Console.WriteLine("Проверка сервиса ДОКИП");
				Console.WriteLine("Найдено {0} zip файлов", files.Length);
				if (files.Length > 0)
				{
					var client = new DOKIPService.serviceClient();
					foreach (var file in files)
					{
						Console.Write("Передача файла '{0}'", Path.GetFileName(file));
						var res = client.Register(new DOKIPService.RegisterRequest() { inputData = File.ReadAllBytes(file) });
						Console.WriteLine(" - {0}", res.outputData);
					}

					client.Close();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine();
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex.Message);
			}
			Console.Write("");
			Console.ReadKey();


		}
	}
}
