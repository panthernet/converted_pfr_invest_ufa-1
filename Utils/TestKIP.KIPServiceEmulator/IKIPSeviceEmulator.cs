﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestKIP.KIPServiceEmulator
{
	[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
	[System.ServiceModel.ServiceContractAttribute(Namespace = "http://www.rstyle.com/ipc/ws/RegisterService", ConfigurationName = "Iservice")]
	public interface Iservice
	{

		// CODEGEN: Generating message contract since the operation Register is neither RPC nor document wrapped.
		[System.ServiceModel.OperationContractAttribute(Action = "*", ReplyAction = "*")]
		[System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
		RegisterResponse1 Register(RegisterRequest1 request);
	}

	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.rstyle.com/ipc/ws/RegisterService")]
	public partial class RegisterRequest
	{

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary")]
		public byte[] inputData;
	}

	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.rstyle.com/ipc/ws/RegisterService")]
	public partial class RegisterResponse
	{

		/// <remarks/>
		public string outputData;
	}

	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
	[System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
	[System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
	public partial class RegisterRequest1
	{

		[System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.rstyle.com/ipc/ws/RegisterService", Order = 0)]
		public RegisterRequest RegisterRequest;

		public RegisterRequest1()
		{
		}

		public RegisterRequest1(RegisterRequest RegisterRequest)
		{
			this.RegisterRequest = RegisterRequest;
		}
	}

	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
	[System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
	[System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
	public partial class RegisterResponse1
	{

		[System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.rstyle.com/ipc/ws/RegisterService", Order = 0)]
		public RegisterResponse RegisterResponse;

		public RegisterResponse1()
		{
		}

		public RegisterResponse1(RegisterResponse RegisterResponse)
		{
			this.RegisterResponse = RegisterResponse;
		}
	}

	[System.ServiceModel.ServiceBehaviorAttribute(InstanceContextMode = System.ServiceModel.InstanceContextMode.PerCall, ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Single)]
	public class service : Iservice
	{

		public virtual RegisterResponse1 Register(RegisterRequest1 request)
		{
			throw new System.NotImplementedException();
		}
	}
}
