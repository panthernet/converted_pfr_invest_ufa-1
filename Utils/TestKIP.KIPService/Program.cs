﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using TestKIP.KIPService.KIPService;

namespace TestKIP.KIPService
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				var baseFolder = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
				var zipFolder = Path.Combine(baseFolder, "InputFiles");

				var files = Directory.GetFiles(zipFolder, "*.zip");
				Console.WriteLine("Проверка сервиса КИП");
				Console.WriteLine("Найдено {0} zip файлов", files.Length);
				if (files.Length > 0)
				{
					var client = new KIPService.serviceClient();
					foreach (var file in files)
					{
						Console.Write("Передача файла '{0}'", Path.GetFileName(file));
						var res = client.Register(new RegisterRequest() { inputData = File.ReadAllBytes(file) });
						Console.WriteLine(" - {0}", res.outputData);
					}

					client.Close();
				}
			}
			catch (Exception ex) 
			{
				Console.WriteLine();
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex.Message);				
			}
			Console.Write("");
			Console.ReadKey();
		}
	}
}
