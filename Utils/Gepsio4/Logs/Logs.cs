﻿using System;
using PFR_INVEST.Common.Logger;

namespace Gepsio4
{
    public static class GLogs
    {
        public static Log Logger;

        public static Logger Logger2;

        public static void LogEx(string mesage, Exception ex)
        {
            Logger?.WriteException(ex, mesage);
            Logger2?.Error(mesage, ex);
        }
    }
}
