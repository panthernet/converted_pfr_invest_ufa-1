﻿using System;
using System.Linq;
using System.ServiceModel;

namespace TestECASA.ServiceEmulator
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("!ВНИМАНИЕ! Для старта эмуляции сервиса необходимо запускать утилиту с правами админа.");
			Console.WriteLine();
			try
			{
                using (var host = new ServiceHost(typeof(ECASASeviceEmulator)))
				{
				    using (var wsdl = new ServiceHost(typeof (WSDLService)))
				    {
				        wsdl.AddServiceEndpoint(typeof (IWSDLService), new WebHttpBinding(), "VerifyAuthenticationWSService.wsdl");
				        wsdl.Open();
				        using (var es = new ServiceHost(typeof (EService)))
				        {
                            es.AddServiceEndpoint(typeof(IEService), new WebHttpBinding(), "eca-ws/pdp/");//"eca-ws/pdp/"
                            es.Open();
				            using (var map = new ServiceHost(typeof (MAPService)))
				            {
                                map.AddServiceEndpoint(typeof(IMAPService), new WebHttpBinding(), "eca-ws/map/");//"eca-ws/pdp/"
                                map.Open();

				                host.Open();

				                Console.WriteLine("The service is ready at {0}", host.BaseAddresses.FirstOrDefault());
				                Console.WriteLine("Press <Enter> to stop the service.");
				                Console.WriteLine();
				                Console.ReadLine();
				            }
				        }
				    }
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine();
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(ex.Message);
			}
			Console.Write("");
			Console.ReadKey();
		}
	}
}
