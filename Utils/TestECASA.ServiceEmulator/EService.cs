﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Newtonsoft.Json;

namespace TestECASA.ServiceEmulator
{
	[ServiceContract]
	public interface IEService 
	{
        [OperationContract]
        [WebGet(UriTemplate = "decide?userGuid={userGuid}&action={action}&resourceString={resString}&paramName={paramName}&otherParamName={otherParamName}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        bool Decide(string userGuid, string action, string resString, string paramName, string otherParamName);

       /* [OperationContract]
        [WebGet(UriTemplate = "decide?userGuid={userGuid}&action={action}&resourceString={resString}&paramName={paramName}")]
        string Decide(string userGuid, string action, string resString, string paramName);

        [OperationContract]
        [WebGet(UriTemplate = "decide?userGuid={userGuid}&action={action}&resourceString={resString}")]
        string Decide(string userGuid, string action, string resString);*/

        [OperationContract]
        [WebGet(UriTemplate = "getObligations?userGuid={userGuid}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        Obligation GetObligations(string userGuid);
    }
	
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "KIPSeviceEmulator" in both code and config file together.
	//[DispatchByBodyElementServiceBehaviorAttribute]
	public class EService : IEService
	{
	    public bool Decide(string userGuid, string action, string resString, string paramName, string otherParamName)
	    {
            return userGuid == "admin" ? true : false;
	    }
	
	   /* public string Decide(string userGuid, string action, string resString, string paramName)
	    {
            return Decide(userGuid, action, resString, null, null);
	    }

	    public string Decide(string userGuid, string action, string resString)
	    {
	        return Decide(userGuid, action, resString, null);
	    }*/

        public Obligation GetObligations(string userGuid)
	    {
	        if (userGuid == "admin")
	        {
	            var obj = new Obligation
	            {
                    id = "1111",
                    attributeList = new []
                    {
                        new ObligationAtr {name = "a1", value ="101"},
                        new ObligationAtr {name = "a2", value ="102"},
                    }
	            };
	            return obj;
	        }

	        return null;
	    }
    

    }

    [DataContract]
    public class Obligation
    {
        [DataMember]
        public string id;

        [DataMember]
        public ObligationAtr[] attributeList;
    }

    [DataContract]
    public class ObligationAtr
    {
        [DataMember]
        public string name;
        [DataMember]
        public string value;
    }

}
