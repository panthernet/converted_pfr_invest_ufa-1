﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using TestVIO.RESTServiceEmulator.Contracts;

namespace TestVIO.RESTServiceEmulator
{
    class Service: IService
    {
        public string GetStream(string contentNumber, string documentId)
        {
            var file = Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "Data"), Config.StreamDocumentName);

            if (File.Exists(file))
            {
                var content = File.ReadAllText(file, Encoding.GetEncoding(1251));

                return content.Trim();
            }
            
            return "";
        }
    }
}
