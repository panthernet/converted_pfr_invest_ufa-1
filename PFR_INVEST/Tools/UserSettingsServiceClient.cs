﻿using PFR_INVEST.SettingsService.Contract;

namespace PFR_INVEST.Tools
{
    public class UserSettingsServiceClient : System.ServiceModel.ClientBase<ISettingsService>
    {
        internal UserSettingsServiceClient() { }

        internal UserSettingsServiceClient(string endpointConfigurationName)
            : base(endpointConfigurationName) { }

        public ISettingsService Client
        {
            get
            {
                return base.Channel;
            }
        }
    }
}
