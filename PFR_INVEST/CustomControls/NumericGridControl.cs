﻿using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;

namespace PFR_INVEST
{
    /// <summary>
    /// Контрол с возможностью редактирования сумм, автозаменой точки на запятую и блокированием ввода запятой
    /// Использовать со стилем колонки N2ColumnStyle
    /// </summary>
    public class NumericGridControl: GridControl
    {
        public NumericGridControl()
        {
            /*DataObject.AddPastingHandler(this, (sender, e) =>
            {
                            var text = Clipboard.GetText().Replace(".", ",");
                if(decimal.TryParse(text, out _))
                    Clipboard.SetText(text);
                else e.Handled = true;
            });*/

            PastingFromClipboard += (sender, e) =>
            {
                var text = Clipboard.GetText().Replace(".", ",");
                if(decimal.TryParse(text, out _))
                    Clipboard.SetText(text);
                else e.Handled = true;
            };

            PreviewKeyDown += (sender, e) =>
            {
                if (Keyboard.Modifiers == ModifierKeys.Control && (e.Key == Key.V || e.Key == Key.A || e.Key == Key.C))
                    return;
                /*if (Keyboard.Modifiers == ModifierKeys.Shift && e.Key == Key.Oem2)
                {
                    var ch = GetCharFromKey(e.Key);
                    if (ch != '.')
                        e.Handled = true;
                    return;
                }*/

                if (e.Key == Key.OemPeriod || e.Key == Key.Oem2)
                {
                    if (Keyboard.Modifiers == ModifierKeys.Shift)
                    {
                        var ch = GetCharFromKey(e.Key);
                        if (ch != '.')
                        {
                            e.Handled = true;
                            return;
                        }
                    }

                    InsertStringIntoCurrentCell(sender, ",");
                    e.Handled = true;
                    return;
                }

                if (e.Key >= Key.D0 && e.Key <= Key.D9) ; // it`s number
                else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ; // it`s number
                else if (e.Key == Key.OemComma || e.Key == Key.Oem3 || e.Key == Key.Decimal)
                {
                    var gc = (GridControl) sender;
                    if (((TableView) gc.View).ActiveEditor is TextEdit te && !string.IsNullOrEmpty(te.Text))
                    {
                        //block if text already has delimiter
                        e.Handled = te.Text.Contains(",") || te.Text.Contains(".");
                        if (!e.Handled)
                        {
                            var ch = GetCharFromKey(e.Key);
                            if (ch != '.' && ch != ',')
                            {
                                e.Handled = true;
                                return;
                            }
                        }
                        return;
                    }

                } // it`s comma
                else if (e.Key == Key.Back || e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                         e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                         e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                         e.Key == Key.System) ; // it`s a system key (add other key here if you want to allow)
                else
                    e.Handled = true; // the key will sappressed
            };
        }

        public enum MapType : uint
        {
            MAPVK_VK_TO_VSC = 0x0,
            MAPVK_VSC_TO_VK = 0x1,
            MAPVK_VK_TO_CHAR = 0x2,
            MAPVK_VSC_TO_VK_EX = 0x3,
        }

        [DllImport("user32.dll")]
        public static extern int ToUnicode(
            uint wVirtKey,
            uint wScanCode,
            byte[] lpKeyState,
            [Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)] 
            StringBuilder pwszBuff,
            int cchBuff,
            uint wFlags);

        [DllImport("user32.dll")]
        public static extern bool GetKeyboardState(byte[] lpKeyState);

        [DllImport("user32.dll")]
        public static extern uint MapVirtualKey(uint uCode, MapType uMapType);

        public static char GetCharFromKey(Key key)
        {
            char ch = ' ';

            int virtualKey = KeyInterop.VirtualKeyFromKey(key);
            byte[] keyboardState = new byte[256];
            GetKeyboardState(keyboardState);

            uint scanCode = MapVirtualKey((uint)virtualKey, MapType.MAPVK_VK_TO_VSC);
            StringBuilder stringBuilder = new StringBuilder(2);

            int result = ToUnicode((uint)virtualKey, scanCode, keyboardState, stringBuilder, stringBuilder.Capacity, 0);
            switch (result)
            {
                case -1: 
                    break;
                case 0: 
                    break;
                case 1:
                {
                    ch = stringBuilder[0];
                    break;
                }
                default:
                {
                    ch = stringBuilder[0];
                    break;
                }
            }
            return ch;
        }

        private void InsertStringIntoCurrentCell(object sender, string value)
        {
            var gc = ((GridControl) sender);
            if (((TableView) gc.View).ActiveEditor is TextEdit te)
            {
                if(te.IsReadOnly || !te.IsEnabled) return;
                if (string.IsNullOrEmpty(te.Text))
                {
                    te.Text = value;
                    return;
                }

                if(te.Text.Contains(",") || te.Text.Contains(".")) return;

                if(te.Text.Length > te.CaretIndex)
                    te.Text = te.Text.Insert(te.CaretIndex, value);
            }
        }
    }
}
