﻿using System.Collections.Generic;
using DevExpress.Xpf.Grid;

namespace PFR_INVEST
{
    public static class GridControlExtensions
    {
        public static GridViewBase GetGridView(this GridControl grid)
        {
            return (GridViewBase)grid.View;
        }

        public static IList<GridColumn> GetGroupedColumns(this GridControl grid)
        {
            return ((GridViewBase)grid.View).GroupedColumns;
        }
			
		/// <summary>
		/// Возвращает для активной групы имя колонки, по которой производилась групировка
		/// Для Детейл грида
		/// </summary>
		/// <param name="grid">Грид Мастер</param>
		/// <returns></returns>
		public static string GetFocusedDetailGroupFieldName(this GridControl grid) 
		{
			if (grid == null) return null;
			var detail = grid.GetDetail(grid.View.FocusedRowHandle) as GridControl;
			if (detail == null) return null;
			return GetFocusedGroupFieldName(detail);
		}

		/// <summary>
		/// Возвращает для активной групы имя колонки, по которой производилась групировка
		/// </summary>	
		public static string GetFocusedGroupFieldName(this GridControl grid) 
		{
			var index = grid.GetRowLevelByRowHandle(grid.View.FocusedRowHandle);
			var groups = ((GridViewBase)grid.View).GroupedColumns;
			if (index < groups.Count)
				return groups[index].FieldName;
			else
				return null;
		}

		/// <summary>
		/// Возвращает данные из активной строки, для групы - первую строку
		/// Для Детейл грида
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="grid">Грид Мастер</param>
		/// <returns></returns>
		public static T GetFocusedDetailItem<T>(this GridControl grid) where T : class
		{
			if (grid == null) return null;
			var detail = grid.GetDetail(grid.View.FocusedRowHandle) as GridControl;
            if (detail == null)
            {
                return grid.View.FocusedRowData.Row as T;
            }

			return GetFocusedItem<T>(detail);
		}

		/// <summary>
		/// Возвращает данные из активной строки, для групы - первую строку
		/// </summary>		
		public static T GetFocusedItem<T>(this GridControl grid) where T:class
		{
			if (grid == null) return null;
			var row = grid.GetRow(grid.View.FocusedRowHandle) as T;
			return row;
		}
			
		
    }
}
