﻿using System.Configuration;

namespace PFR_INVEST
{
    public static partial class AppSettings
    {
        /// <summary>
        /// Включение/выключение отображения индикатора загрузки. Для показа: true
        /// </summary>
        public static bool LoadingDisabled
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                bool bLoadingDisabled;
                if (!bool.TryParse(ConfigurationManager.AppSettings.Get("LoadingDisabled"), out bLoadingDisabled))
                    bLoadingDisabled = false;
                return bLoadingDisabled;
            }
        }

        /// <summary>
        /// Показывать FPS отрисовки графики
        /// </summary>
        public static bool ShowFPS
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                bool bShowFPS;
                if (!bool.TryParse(ConfigurationManager.AppSettings.Get("ShowFPS"), out bShowFPS))
                    bShowFPS = false;
                return bShowFPS;
            }
        }

        /// <summary>
        /// Показывать название класса последнего открытого окна в статус баре. Для показа: false
        /// </summary>
        public static bool DisplayWindowClassName
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                bool bDisplayWindowClassName;
                if (!bool.TryParse(ConfigurationManager.AppSettings.Get("DisplayWindowClassName"), out bDisplayWindowClassName))
                    bDisplayWindowClassName = false;
                return bDisplayWindowClassName;
            }
        }


        public static bool DisplayLoginMessages
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                bool t;
                if (!bool.TryParse(ConfigurationManager.AppSettings.Get("DisplayLoginMessages"), out t))
                    t = true;
                return t;
            }
        }

        /// <summary>
        /// Отображать кнопку пересчета перечислений УК по договору
        /// </summary>
        public static bool DisplayReqTransferRecalcButton
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                bool bDisplayWindowClassName;
                if (!bool.TryParse(ConfigurationManager.AppSettings.Get("DisplayReqTransferRecalcButton"), out bDisplayWindowClassName))
                    bDisplayWindowClassName = false;
                return bDisplayWindowClassName;
            }
        }


        /// <summary>
        /// Адрес логина для аутентификации через ЕЦАСА
        /// </summary>
        public static string EcasaLoginUrl
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                return !string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("EcasaLoginUrl")) ? ConfigurationManager.AppSettings.Get("EcasaLoginUrl") : "";
            }
        }

        /// <summary>
        /// Включен ли сервис настроек. Для показа: false
        /// </summary>
        public static bool IsUserSettingsEnabled
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                bool bIsUserSettingsEnabled;
                if (!bool.TryParse(ConfigurationManager.AppSettings.Get("IsUserSettingsEnabled"), out bIsUserSettingsEnabled))
                    bIsUserSettingsEnabled = false;
                return bIsUserSettingsEnabled;
            }
        }

       
        /// <summary>
        /// Включено ли документирование работы AD. Для показа: false
        /// </summary>
        public static bool IsAuthDebugEnabled
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                bool bIsAuthDebugEnabled;
                if (!bool.TryParse(ConfigurationManager.AppSettings.Get("IsAuthDebugEnabled"), out bIsAuthDebugEnabled))
                    bIsAuthDebugEnabled = false;
                return bIsAuthDebugEnabled;
            }
        }
    }
}
