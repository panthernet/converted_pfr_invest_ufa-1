﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace PFR_INVEST.Converters
{
    public class BoolToAnyConverter<T>: IValueConverter
    {
        public T TrueResult { get; set; }
        public T FalseResult { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return FalseResult;
            return (bool) value ? TrueResult : FalseResult;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class BoolToBrushConverter : BoolToAnyConverter<Brush>
    {
    }

    public class BoolToStringConverter : BoolToAnyConverter<string>
    {
    }
}
