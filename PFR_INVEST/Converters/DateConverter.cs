﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;

namespace PFR_INVEST.Converters
{
    public class DateConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var type = (string)parameter;
            var val = value as DateTime?;
            if (val != null)
            {
                switch (type.ToLower())
                {
                    case "year":
                        return DateTools.GetYearInWordsForDate(val);
                    case "halfyear":
                        return DateTools.GetHalfYearInWordsForDate(val);
                    case "quarter":  
                        return DateTools.GetQarterYearInWordsForDate(val);
                    case "month":
                        return DateTools.GetMonthInWordsForDate(val);
                        
                    default: return val;
                }
            }
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }        
    }
}
