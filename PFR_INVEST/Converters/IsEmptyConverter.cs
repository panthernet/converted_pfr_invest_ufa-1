﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using System.Windows.Data;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.Converters
{

	public class IsEmptyConverter : MarkupExtension, IValueConverter
	{
		[ConstructorArgument("invert")]
		public bool Invert { get; set; }

		public IsEmptyConverter() { }

		public IsEmptyConverter(bool invert) { }

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			bool result = false;

			if (value == null)
				result = true;
			else if (string.IsNullOrWhiteSpace(value.ToString()))
				result = true;
			else
				result = false;

			return Invert ? !result : result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
