﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using PFR_INVEST.Views;
using IServiceProvider = System.IServiceProvider;

namespace PFR_INVEST.Helpers
{
    [ValueConversion(typeof(int), typeof(Visibility))]
    public class IntToVisibilityConverter : DependencyObject, IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((int)value) == 0 ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [ValueConversion(typeof(UserControl), typeof(Visibility))]
    public class ISearchGridToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var searchGrid = value as ISearchGrid;
            return searchGrid != null ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [ValueConversion(typeof(int), typeof(String))]
	public class NumberToMonth : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int) return culture.DateTimeFormat.GetMonthName((int)value);
            throw new NotSupportedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this; 
		}
	}

    [ValueConversion(typeof(object), typeof(String))]
    public class SimpleTypesToStringConverter : DependencyObject, IValueConverter
    {
        public static readonly DependencyProperty FormatStringProperty
            = DependencyProperty.Register("StringFormat", typeof(String), typeof(SimpleTypesToStringConverter));

        public String StringFormat
        {
            get
            {
                return (String)GetValue(FormatStringProperty);
            }
            set
            {
                SetValue(FormatStringProperty, value);
            }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Format(StringFormat, value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
