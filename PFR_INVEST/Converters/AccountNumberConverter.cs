﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PFR_INVEST.Helpers
{
	[ValueConversion(typeof(String), typeof(String))]
	public class AccountNumberConverter : IValueConverter
	{
		#region Implementation of IValueConverter

		// "\d{1} \d{2} \d{2} \d{3} \d{1} \d{4} \d{3} \d{4}"
		private readonly int[] _spaceIndexes = new [] { 1, 4, 7, 11, 13, 18, 22 };

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return null;
			
			var s = (string) value;
			foreach (var spaceIndex in _spaceIndexes)
			{
				if (spaceIndex < s.Length)
					s = s.Insert(spaceIndex, " ");
			}
			return s;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var stringValue = (string) value;
			return stringValue.Replace(" ", string.Empty);
		}

		#endregion
	}
}
