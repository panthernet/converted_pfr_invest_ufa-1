﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;
using System.Windows.Markup;
using DevExpress.Xpf.Editors.Helpers;

namespace PFR_INVEST.Converters
{
    public class BoolToValueConverter : MarkupExtension, IValueConverter
    {
        public object TrueValue { get; set; }
        public object FalseValue { get; set; }
        public object NullValue { get; set; }

        public BoolToValueConverter() : this(null, null, null) { }
        public BoolToValueConverter(object trueValue, object falseValue) : this(trueValue, falseValue, null) { }
        public BoolToValueConverter(object trueValue, object falseValue, object nullValue)
        {
            this.TrueValue = trueValue;
            this.FalseValue = falseValue;
            this.NullValue = nullValue;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool invert = "invert".Equals(parameter as string, StringComparison.OrdinalIgnoreCase);

            bool? val = value as bool?;
            if (val == null)
                return this.NullValue;
            if (invert)
                val = !val;

            return val.Value ? this.TrueValue : this.FalseValue;
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : BoolToValueConverter
    {
        public BoolToVisibilityConverter() : base(Visibility.Visible, Visibility.Collapsed) { }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToInvertVisibilityConverter : BoolToValueConverter
    {
        public BoolToInvertVisibilityConverter() : base(Visibility.Collapsed, Visibility.Visible) { }
    }

    [ValueConversion(typeof(bool), typeof(bool))]
    public class BoolInvertConverter : BoolToValueConverter
    {
        public BoolInvertConverter() : base(false, true) { }
        public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool? val = value as bool?;
            if (val == null)
                return null;

            return !val.Value;
        }
    }

    public class IdToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && System.Convert.ToInt64(value) != 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class IdToVisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && System.Convert.ToInt64(value) != 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    /// <summary>
    /// Конвертер для отображения decimal значений в TextEdit без дробной части
    /// </summary>
    public class Int2DecimalConverter : MarkupExtension, IValueConverter
    {
       
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                var result = value.TryConvertToDecimal().ToString(parameter as string);
                if (result.Contains(','))
                    return result.Split(',')[0];
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
    /// <summary>
    /// Конвертер ограничения ввода decimal значений до максимума int32
    /// </summary>
    public class Decimal2IntConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return value;
            
            Decimal decValue = 0;
            try
            {
                if (value is decimal)
                    decValue = int.Parse(value.ToString().Take(9).ToString());
                else
                {
                    decValue = value.TryConvertToDecimal();
                }
            }
            catch (OverflowException e)
            {
                return value; //decValue = int.MaxValue;
            }
            if (value is decimal)
                return decValue;
            else
            {
                return decValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value == null)
                return value;
            var valStr = value.ToString();
            int intResult = 0;
            if (valStr.Length > 9 && !int.TryParse(valStr, out intResult))
                return value.ToString().Substring(0, 9);
            else
                return value;

        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
    public class NoneConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class DecimalCustomFormatter : IValueConverter
    {
        public object Convert(object value, Type targetType, object
            parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                var result = value.TryConvertToDecimal().ToString(parameter as string);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
