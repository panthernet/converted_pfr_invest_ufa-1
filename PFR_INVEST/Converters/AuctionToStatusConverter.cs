﻿using System;
using System.Windows.Data;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Converters
{
    public class AuctionToStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is DepClaimSelectParams)
            {
                var auction = (DepClaimSelectParams) value;                
                switch (auction.AuctionStatus)
                {
                    case DepClaimSelectParams.Statuses.New: return "Новый";
                    case DepClaimSelectParams.Statuses.DepClaimImported:
                        if (auction.DepClaimMaxCount == 0 && auction.DepClaimCommonCount == 0)
                            return "Загружена Выписка из реестра заявок";
                        if (auction.DepClaimMaxCount == 0 && auction.DepClaimCommonCount > 0)
                            return "Загружены Выписка из реестра заявок и Сводный реестр заявок";
                        if (auction.DepClaimMaxCount > 0 && auction.DepClaimCommonCount == 0)
                            return "Загружены Выписка из реестра заявок и Сводный реестр заявок, с учетом максимальной суммы размещения";
                        if (auction.DepClaimMaxCount > 0 && auction.DepClaimCommonCount > 0)
                            return "Загружены Выписка из реестра заявок, Сводный реестр заявок и Сводный реестр заявок, с учетом максимальной суммы размещения";
                        return "";
                    case DepClaimSelectParams.Statuses.DepClaimConfirmImported: return "Загружена Выписка из реестра заявок, подлежащих удовлетворению";
                    case DepClaimSelectParams.Statuses.OfferCreated: return "Сгенерированы оферты";
                    case DepClaimSelectParams.Statuses.OfferSigned: return "Оферты подписаны";
                    case DepClaimSelectParams.Statuses.OfferAcceptedOrNo: return "Оферты акцептованы/не акцептованы";
                    case DepClaimSelectParams.Statuses.DepositSettled: return "Сформированы депозиты";
                    default: return string.Empty;
                }
            }
            return string.Empty;
            //throw new NotImplementedException("Only DepClaimSelectParams type supported");
        }

               
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
