using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PFR_INVEST.Helpers
{
	[ValueConversion(typeof(bool), typeof(Visibility))]
	public class CustomBooleanToVisibilityConverter : IValueConverter
	{
		#region Implementation of IValueConverter

		public Visibility True { get; set; } = Visibility.Visible;
		public Visibility False { get; set; } = Visibility.Collapsed;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value != null && (bool)value ? True : False;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}