﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.Helpers
{
    //[ValueConversion(typeof(DateTime), typeof(BitmapImage))]
    public class StatusToImageConverter : DependencyObject, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            System.Xml.XmlAttribute attribute;
            DateTime tValue;
            int iValue;
            if ((attribute = value as System.Xml.XmlAttribute) != null)
                value = attribute.Value;

            bool? isGreen = true;
            if (value is bool)
                isGreen = value as bool?;
                //проверка банка
            else if (value is LegalEntity)
            {
                var bank = (LegalEntity) value;
                isGreen = BankCalculator.IsActiveStatus(bank);
            }
            //проверка через дату расторжения договора
            else if (value != null && DateTime.TryParse(value.ToString(), out tValue))
            {
                if (ContragentHelper.IsContractMarkedRed(tValue))
                    isGreen = false;
            }
                //проверка через статус контрагента
            else if (value != null && int.TryParse(value.ToString(), out iValue))
                isGreen = iValue == StatusIdentifier.Identifier.Active.ToLong();

            var imgr = isGreen == true ? new BitmapImage(new Uri("/Images/greenlight.png", UriKind.Relative)) : new BitmapImage(new Uri("/Images/redlight.png", UriKind.Relative));
            return imgr;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return value == null ? null : value.ToString();
        }
    }

    public class DiscrepancyToImageConverter : DependencyObject, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            //decimal discrepancy = (decimal)value;
            //if (discrepancy < 0)
            //    flag = true;
            string status = (string)value;
            var flag = status.Equals("Страховая сумма достаточна", StringComparison.InvariantCultureIgnoreCase);
            return flag ? new BitmapImage(new Uri("/Images/greenlight.png", UriKind.Relative)) : new BitmapImage(new Uri("/Images/redlight.png", UriKind.Relative));
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return value == null ? null : value.ToString();
        }
    }
}