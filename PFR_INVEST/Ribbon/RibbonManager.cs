﻿using System;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Ribbon;
using Microsoft.Windows.Controls.Ribbon;
using PFR_INVEST.BusinessLogic;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.BusinessLogic.Misc;
namespace PFR_INVEST.Ribbon
{
    public enum RibbonStates
    {
        ListsState,
        NPFState,
        SIState,
        VRState,
        CBState,
        RegionsState,
        BackOfficeState,
        SettingsState,
        AnalyzeState
    }

    public class RibbonManager
    {
        private readonly RibbonControl _dxribbon;
        private readonly BarManager _bm;


        private RibbonStates _state = RibbonStates.ListsState;
        public RibbonStates State
        {
            get { return _state; }
            set { _state = value; UpdateState(value); }
        }

        private void UpdateState(RibbonStates newState)
        {
            // проверка доступа к табм, группам и кнопкам

            var isPureViewer = AP.Provider.CurrentUserSecurity.GetRoles()
                .Where(a => a != DOKIP_ROLE_TYPE.None && a != DOKIP_ROLE_TYPE.User).All(a => a.ToString().ToLower().EndsWith("viewer"));

            var hasViewers = AP.Provider.CurrentUserSecurity.GetRoles().Any(a => a.ToString().ToLower().EndsWith("viewer"));

            foreach (BarItemLinkBase item in ((ApplicationMenu)_dxribbon.ApplicationMenu).ItemLinks)
            {
                if (!(item is BarItemLink))
                    continue;

                bool accessDeniedItem = true;

                var baritem = _bm.Items[(item as BarItemLink).BarItemName];
                if (baritem == null) continue;
                string bartag = baritem.Tag?.ToString() ?? string.Empty;
                if (!string.IsNullOrEmpty(bartag))
                {
                    if (AP.Provider != null)
                    {
                        var roles = bartag.Split('|').Select(UserSecurity.GetRoleByADName).ToList();
                        if (roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role)))
                            accessDeniedItem = false;
                        if (hasViewers && accessDeniedItem && !bartag.StartsWith("NO_VIEWERS"))
                            accessDeniedItem = !AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);
                    }
                    else { if (((RibbonCommand)baritem.Command).LabelTitle == "Закрыть" || ((RibbonCommand)baritem.Command).LabelTitle == "Справка") accessDeniedItem = false; }
                }

                baritem.IsVisible = !accessDeniedItem;
            }

            bool selectedPage = false;
            foreach (var item in _dxribbon.Categories)
            {
                if (!(item is RibbonDefaultPageCategory)) continue;

                foreach (var tab in ((RibbonDefaultPageCategory)item).Pages)
                {
                    bool accessDeniedTab = true;
                    string tagstr = tab.Tag?.ToString() ?? string.Empty;
                    if (!string.IsNullOrEmpty(tagstr) && tagstr.Contains(newState.ToString()))
                    {
                        if (AP.Provider != null)
                        {
                            var roles = tagstr.Split('|').Select(UserSecurity.GetRoleByADName).ToList();
                            if (!isPureViewer && roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role)))
                                accessDeniedTab = false;
                            if (hasViewers && accessDeniedTab && !tagstr.StartsWith("NO_VIEWERS"))
                                accessDeniedTab = !roles.Contains(DOKIP_ROLE_TYPE.User) && !AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);
                        }
                    }

                    tab.IsVisible = !accessDeniedTab;
                    if (!tab.IsVisible) continue;

                    if (tab.IsVisible && !selectedPage)
                    {
                        _dxribbon.SelectedPage = tab;
                        selectedPage = true;
                    }

                    var toremove = new List<RibbonPageGroup>();
                    foreach (var group in tab.Groups)
                    {
                        //Шабанов
                        //accessDeniedGroup не использовался, закомментировал
                        bool accessDeniedGroup = true;
                        string grstr = group.Tag?.ToString() ?? string.Empty;
                        if (!string.IsNullOrEmpty(grstr))
                        {
                            if (AP.Provider != null)
                            {
                                var roles = grstr.Split('|').Select(UserSecurity.GetRoleByADName).ToList();
                                if (!isPureViewer && roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role)))
                                    accessDeniedGroup = false;
                                if (hasViewers && accessDeniedGroup && !grstr.StartsWith("NO_VIEWERS"))
                                    accessDeniedGroup = !roles.Contains(DOKIP_ROLE_TYPE.User) && !AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);
                            }
                        }

                        //group = (accessDeniedGroup) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                        if (accessDeniedGroup)
                        {
                            toremove.Add(group);
                            continue;
                        }

                        foreach (var it in group.ItemLinks)
                        {
                            if (it is BarItemLinkSeparator) continue;
                            var link = (BarItemLink)it;
                            var bi = _bm.Items[link.BarItemName];
                            if (bi == null) break;
                            string bistr = bi.Tag?.ToString() ?? string.Empty;
                            bool accessDeniedBtn = true;

                            if (!string.IsNullOrEmpty(bistr))
                            {
                                if (AP.Provider != null)
                                {
                                    var roles = bistr.Split('|').Select(UserSecurity.GetRoleByADName).ToList();
                                    if (!isPureViewer && roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role)))
                                        accessDeniedBtn = false;
                                    if (hasViewers && accessDeniedBtn && !bistr.StartsWith("NO_VIEWERS"))
                                        accessDeniedBtn = !roles.Contains(DOKIP_ROLE_TYPE.User) && !AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);
                                }
                            }

                            bi.IsVisible = !accessDeniedBtn;
                        }
                    }


                    //temp hack DX2010 issue
                    foreach (var gr in toremove)
                        tab.Groups.Remove(gr);
                }
            }

            if (WCFClient.AuthType != ClientAuthType.None)
                MainWindow.Instance.pRolesSET.IsVisible = false;
            if (!App.ServerSettings.ShowClientNotificationSettings)
                MainWindow.Instance.ribAlertSettings.IsVisible = false;
        }

        /// <summary>
        /// remove old param
        /// </summary>
        public RibbonManager(RibbonControl dribbon, BarManager barm)
        {
            _dxribbon = dribbon; _bm = barm;
        }

        public void SetVisible(string name, bool isVisible)
        {
            var item = _dxribbon.FindName(name) as BarButtonItem;
            if (item != null)
            {
                item.IsVisible = isVisible;

            }
        }
        /// <summary>
        /// Позволяет задавать State вкладкам внутри раздела Аналитика
        /// </summary>
        /// <returns></returns>
        public RibbonStateBL GetCurrentPageRibbonState()
        {
            RibbonStateBL result;
            Enum.TryParse(_dxribbon.SelectedPage.SerializationName, true, out result);
            return result;
        }

    }
}
