﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class F60ListView
    {
        public F60ListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, f60Grid);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (f60Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (f60Grid.View.FocusedRowHandle < 0)
                return;
            object value = f60Grid.GetCellValue(f60Grid.View.FocusedRowHandle, "ID");

            App.DashboardManager.OpenNewTab(typeof(F60View),
                ViewModelState.Edit,
                (long)
                    value, "Отчёт об инвестировании");
        }

        public long GetSelectedID()
        {
            if (f60Grid.View.FocusedRowHandle <= -10000)
                return 0;
            if (f60Grid.View.FocusedRowHandle >= 0)
                return (long)f60Grid.GetCellValue(f60Grid.View.FocusedRowHandle, "ID");
            return 0;
        }
    }
}
