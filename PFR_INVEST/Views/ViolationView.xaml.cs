﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ViolationView.xaml
    /// </summary>
    public partial class ViolationView : UserControl
    {
        public ViolationView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
