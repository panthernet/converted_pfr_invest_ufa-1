﻿using System;
using System.Windows;
using System.Windows.Threading;
using PFR_INVEST.Helpers;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SplashView.xaml
    /// </summary>
    public partial class Splash : Window
    {
        public Splash()
        {
            InitializeComponent();
            ThemesTool.UpdateLogo(splashImage);
            Loaded += Splash_Loaded;
        }

        void Splash_Loaded(object sender, RoutedEventArgs e)
        {
            IAsyncResult result = null;
            AsyncCallback initCompleted = delegate
            {
                App.Current.ApplicationInitialize.EndInvoke(result);
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)Close);
            };

            result = App.Current.ApplicationInitialize.BeginInvoke(this, initCompleted, null);
        }

        public void BeginFadeOut()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate { this.FormFadeOut.Begin(); });
        }

        private void FormFadeOut_Completed(object sender, EventArgs e)
        {
            //
        }
    }
}
