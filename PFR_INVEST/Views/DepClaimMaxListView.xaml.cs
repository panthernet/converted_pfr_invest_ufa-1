﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DevExpress.Xpf.Grid;
    using DataObjects.ListItems;

    /// <summary>
    /// Interaction logic for DepclaimMaxListView.xaml
    /// </summary>
    public partial class DepClaimMaxListView
    {
        public DepClaimMaxListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            tableView.FocusedRowHandleChanged += tableView_FocusedRowHandleChanged; 
        }

        private void tableView_FocusedRowHandleChanged(object sender, FocusedRowHandleChangedEventArgs e)
        {
            if (tableView.FocusedRowData.Row is DepClaimMaxListItem)
                SelectedDepClaimMaxHelper.Value = ((DepClaimMaxListItem) tableView.FocusedRowData.Row);
            else
                SelectedDepClaimMaxHelper.Value = null;
        }

    }
}
