﻿using System;
using DevExpress.Xpf.Grid;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for TransferFromNPFListView.xaml
    /// </summary>
    public partial class TransferFromNPFListView
    { 
        public TransferFromNPFListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, TransferToNPFGrid);
        }

        public bool IsTransferSelected()
        {
            return TransferToNPFGrid.View.FocusedRowData.Level >= TransferToNPFGrid.GetGroupedColumns().Count && TransferToNPFGrid.SelectedItem != null;
        }

        public long GetCurrFinRegID()
        {
            return Convert.ToInt64(TransferToNPFGrid.GetCellValue(TransferToNPFGrid.View.FocusedRowHandle, "FinregisterID"));
        }
        
        private void ListGrid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }

        private void ListGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "{0} \tдокумент: {1}     \r\n\t№ {2} от: {3}";
            if (!TransferToNPFGrid.IsValidRowHandle(e.RowHandle))
                return;

            if (e.Column.FieldName != "RegisterID")
                return;

            //string dispText = "";

            var currRow = (e.Row as TransferFromOrToNPFListItem);

            if (currRow != null)
            {
                e.DisplayText = string.Format(dispText,
                    currRow.RegisterID > 0 ? currRow.RegisterID.ToString() : string.Empty,
                    !string.IsNullOrEmpty(currRow.ApproveDocName) ? currRow.ApproveDocName : string.Empty,
                    !string.IsNullOrEmpty(currRow.RegNum?.Trim()) ? currRow.RegNum.Trim() : string.Empty,
                    currRow.PaymentOrderDate != null && currRow.PaymentOrderDate != DateTime.MinValue ? currRow.PaymentOrderDate.Value.ToShortDateString() : string.Empty
                    );
            }
            else
                e.DisplayText = string.Empty;
        }
    }
}
