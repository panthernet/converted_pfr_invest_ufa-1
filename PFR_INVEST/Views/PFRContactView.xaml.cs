﻿using System.Windows;
using PFR_INVEST.Helpers;
using PFR_INVEST.Views.Interface;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PFRContactView.xaml
    /// </summary>
    public partial class PFRContactView : IAutoParentPanelSize
    {
        public PFRContactView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        #region IAutoParentPanelSize
        public double DesiredWidth { get { return DevExpressHelper.PWIDTH_AUTO; } }

        protected override Size MeasureOverride(Size constraint)
        {
            return DevExpressHelper.MeasureParentPanel(this, base.MeasureOverride, DesiredWidth);
        }
        #endregion
    }
}
