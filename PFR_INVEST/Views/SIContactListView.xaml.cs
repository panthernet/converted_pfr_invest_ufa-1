﻿using System;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SIContactListView.xaml
    /// </summary>
    public partial class SIContactListView
    {
        public SIContactListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, SIContactsGrid);
        }

        public bool IsSISelected()
        {
            var lvl = SIContactsGrid.View.FocusedRowData.Level;
            if (lvl < SIContactsGrid.GetGroupedColumns().Count)
                return (SIContactsGrid.GetGroupedColumns()[lvl].FieldName == "FormalizedName");
            return false;
        }

        public long GetSelectedSIID()
        {
            return Convert.ToInt64(SIContactsGrid.GetCellValue(SIContactsGrid.View.FocusedRowHandle, "LegalEntityID"));
        }

        private void SIContactsGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (SIContactsGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            var row = SIContactsGrid.CurrentItem as SIListItem;

            if (row == null)
                return;

            App.DashboardManager.OpenNewTab(typeof(ContactView), ViewModelState.Edit, row.ContactID, row.LegalEntityID, "Редактировать контакт");
        }
    }
}
