﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class TransferView : UserControl
    {
        TransferViewModel Model => DataContext as TransferViewModel;

        public TransferView()
        {
            InitializeComponent();
            Loaded += SITransferView_Loaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        void SITransferView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Model!=null && Model.TransferRegister.GetDirection().isFromUKToPFR.GetValueOrDefault())
            {
                Grid.Columns[0].FieldName = "RequestCreationDate";
                Grid.Columns[0].Header = "Дата требования";
            }
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Model == null) return;;
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (Model.ID > 0)
            {
                long ID = GetSelectedReqTransferID();
                if (ID > 0)
                {
                    App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Edit, ID, "Перечисление");
                }
            }
            else
            {
                var param = new object[] { Model.TransferList, Grid.GetFocusedRow() , Model};
                App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Edit, param, "Перечисление");
            }
        }

        public long GetSelectedReqTransferID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }
    }
}
