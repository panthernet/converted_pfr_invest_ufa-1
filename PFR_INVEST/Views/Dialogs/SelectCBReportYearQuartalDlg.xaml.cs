﻿using System.Windows;
using PFR_INVEST.Helpers;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SuspendActivityDlg.xaml
    /// </summary>
    public partial class SelectCBReportYearQuartalDlg
    {
        protected SelectCBReportYearQuartalDlgViewModel Model => (SelectCBReportYearQuartalDlgViewModel)DataContext;

        public SelectCBReportYearQuartalDlg(bool forceOne, bool forMonthly = false)
        {
            InitializeComponent();
            if (forceOne) cboxOkud.IsReadOnly = true;

            if (forMonthly)
            {
                Title = "Сформировать новую информацию";
                dpOkud.Visibility = Visibility.Collapsed;
                dpQuarter.Visibility = Visibility.Collapsed;
            }
            else dpMonth.Visibility = Visibility.Collapsed;

            ModelInteractionHelper.SignUpForCloseRequest(this, null, isCancel =>
            {
                if (isCancel) return true;
                return true;
            });
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
