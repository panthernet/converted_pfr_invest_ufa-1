﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class LoginMessagesDlg
    {
        public LoginMessagesDlgViewModel Model => DataContext as LoginMessagesDlgViewModel;

        public LoginMessagesDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = Model.ShowNextTime;
        }

        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DialogResult = Model.ShowNextTime;
        }
    }
}
