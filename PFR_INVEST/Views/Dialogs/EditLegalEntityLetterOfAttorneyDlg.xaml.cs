﻿using System.Windows;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EditLegalEntityLetterOfAttorneyDlg.xaml
    /// </summary>
    public partial class EditLegalEntityLetterOfAttorneyDlg
    {
        private bool _isDialogCompleted;
        public LegalEntityLetterOfAttorneyDlgViewModel Model => DataContext as LegalEntityLetterOfAttorneyDlgViewModel;

        public EditLegalEntityLetterOfAttorneyDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = Model.IsDataChanged;
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = true;
            DialogResult = false;
        }

        public string SelectedStockName
        {
            get
            {
                //return StockCombo.SelectedText ?? string.Empty;
                var value = ((Element)(StockCombo.SelectedItem));
                return value.Name ?? string.Empty;
            }
        }

        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_isDialogCompleted)//Skip asking if flag is already set
            {
                return;
            }

            bool? x = Model.AskSaveOnClose();

            switch (x)
            {
                case null:
                    e.Cancel = true;
                    break;
                case false:
                    DialogResult = false;
                    break;
                default:
                    if (Model.CanSaveOnClose())
                        DialogResult = true;
                    else
                        e.Cancel = true;
                    break;
            }
        }
    }
}
