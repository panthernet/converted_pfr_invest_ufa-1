﻿using System.Windows;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for BankConclusionImportDlg.xaml
    /// </summary>
    public partial class BankConclusionImportDlg
    {
        public BankConclusionImportDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForDevExpressCustomColumnPopupSort(grid, "IntNumber", DevExpressColumnSortType.Int);
        }

        private void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
