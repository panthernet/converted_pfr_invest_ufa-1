﻿using System.Linq;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ExportDepClaimMaxListDlg.xaml
    /// </summary>
    public partial class ExportDepClaimMaxListDlg
    {
        public ExportDepClaimMaxListDlg()
        {
            InitializeComponent();
            if (new DepClaimMaxListViewModel(DepClaimMaxListItem.Types.Max).DepClaimMaxList.Any())
            {
                OKButton.IsEnabled = true;
                Message.Content = "Реестр сформирован:";
                DateEdit.SetVisible(true);
            }
            else
            {
                OKButton.IsEnabled = false;
                Message.Content = "Нет данных для экспорта";
                DateEdit.SetVisible(false);
            }

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
