﻿namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ExportDepClaimMaxListDlg.xaml
    /// </summary>
	public partial class ExportAuctionCutoffRateDlg
    {
		public ExportAuctionCutoffRateDlg()
        {
            InitializeComponent();           
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
