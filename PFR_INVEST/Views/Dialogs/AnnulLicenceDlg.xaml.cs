﻿using System.Windows;
using DevExpress.Xpf.Core;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AnnulLicenceDlg.xaml
    /// </summary>
    public partial class AnnulLicenceDlg
    {
        public AnnulLicenceDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (AnnulDate.EditValue != null)
                DialogResult = true;
            else
                DXMessageBox.Show("Введите дату аннулирования лицензии!", "Ошибка ввода данных!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
