﻿using System;
using System.Linq;
using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class SIRequestDlg
    {
        private class AddressTypeItem
        {
            public string Title {get; set;}
            public AddressType Value {get; set;}
        }

        public int Year => (int)(((Year) cbYear.SelectedItem)?.ID ?? 0 + 2000);
        public int Month => cbMonth.SelectedIndex + 1;

        public SIRequestDlg(bool showYear)
        {
            InitializeComponent();
            AddressTypeCombo.Items.Add(new AddressTypeItem { Value = AddressType.Post, Title = "Почтовый" });
            AddressTypeCombo.Items.Add(new AddressTypeItem { Value = AddressType.Fact, Title = "Фактический" });
            AddressTypeCombo.SelectedIndex = 0;
            if (showYear)
            {
                var yList = DataContainerFacade.GetList<Year>();
                cbYear.ItemsSource = yList;
                var curYear = DateTime.Today.Year - 2000;
                cbYear.SelectedItem = yList.FirstOrDefault(a => a.ID == curYear) ?? yList.FirstOrDefault();
                var mList = DateTools.Months;
                cbMonth.ItemsSource = mList;
                cbMonth.SelectedItem = mList[DateTime.Today.Month - 1];
            }
            else dpDates.Visibility = Visibility.Collapsed;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        public AddressType SelectedType => ((AddressTypeItem)AddressTypeCombo.SelectedItem).Value;
    }
}
