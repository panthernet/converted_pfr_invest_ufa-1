﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddDopDueDeadDlg.xaml
    /// </summary>
    public partial class AddDopDueDeadDlg
    {
        public AddDopDueDeadDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as DueDeadAccurateViewModel;
            if (vm != null)
            {
                //vm.SaveCard.Execute(null);
                DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
