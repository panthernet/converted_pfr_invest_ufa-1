﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for BanksVerificationDlg.xaml
    /// </summary>
    public partial class ExportDepClaimOfferAcceptedDlg : DXWindow
    {
        public ExportDepClaimOfferAcceptedDlg()
        {
            InitializeComponent();
        }

        private ExportDepClaimOfferAcceptedViewModel Model
        {
            get { return DataContext as ExportDepClaimOfferAcceptedViewModel; }

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

       
    }
}
