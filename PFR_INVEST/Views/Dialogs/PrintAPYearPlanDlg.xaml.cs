﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for FromUKEnterGetRequirementDateDlg.xaml
    /// </summary>
    public partial class PrintAPYearPlanDlg
    {
        public const string INVALID_YEAR_MSG = "Для выделенного года планы перечислений не создавались ранее!";
        public PrintAPYearPlanDlgViewModel Model => DataContext as PrintAPYearPlanDlgViewModel;

        public PrintAPYearPlanDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckIfSelectedYearIsValid())
                DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private bool CheckIfSelectedYearIsValid()
        {
            if (Model.IsSelectedYearValid())
                return true;

            ViewModelBase.DialogHelper.ShowAlert(INVALID_YEAR_MSG);
            return false;
        }
    }
}
