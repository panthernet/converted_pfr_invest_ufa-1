﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using PFR_INVEST.Tools;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class YearPlanPaymentDlg
    {
        private const string SELECT_FOLDER_DIALOG_DESC = "Выберите папку для сохранения.";
        private const string INVALID_SELECTED_FOLDER = "Неверно указана папка для сохранения!";
        private const string INITIAL_SELECTED_FOLDER = "C:\\";
        public string SelectedFolder;

        public YearPlanPaymentDlg()
        {
            InitializeComponent();
            txtSelectedFolder.EditValue = INITIAL_SELECTED_FOLDER;
            ThemesTool.SetCurrentTheme(this);
            DataContextChanged += YearPlanPaymentDlg_DataContextChanged;
        }

        private void YearPlanPaymentDlg_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext == null) return;
            ((YearPlanPaymentDlgViewModel) DataContext).UkListChanged += YearPlanPaymentDlg_UkListChanged;
            YearPlanPaymentDlg_UkListChanged(null, null);
        }

        private void YearPlanPaymentDlg_UkListChanged(object sender, System.EventArgs e)
        {
            cbUk.ItemsSource = null;
            cbUk.Items.Clear();
            cbUk.ItemsSource = ((YearPlanPaymentDlgViewModel) DataContext).UkList;
        }

        private static bool CheckDirectoryName(string folder)
        {
            if (Directory.Exists(folder))
                return folder == Path.GetFullPath(folder);
            return false;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            string folder = Convert.ToString(txtSelectedFolder.EditValue);
            if (!CheckDirectoryName(folder))
            {
                DXMessageBox.Show(INVALID_SELECTED_FOLDER, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SelectedFolder = folder;
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private string _lastSelectedFolder;

        private void SelecFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dlg = new FolderBrowserDialog { Description = SELECT_FOLDER_DIALOG_DESC, SelectedPath = _lastSelectedFolder })
            {

                if (Directory.Exists(txtSelectedFolder.Text))
                    dlg.SelectedPath = txtSelectedFolder.Text;

                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    txtSelectedFolder.EditValue = dlg.SelectedPath;
                _lastSelectedFolder = dlg.SelectedPath;
            }
        }
    }
}
