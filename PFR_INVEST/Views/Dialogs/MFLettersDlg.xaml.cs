﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using System.Windows.Controls;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddBranch.xaml
    /// </summary>
    public partial class MFLettersDlg
    {
        public MFLettersDlg(int type)
        {
            InitializeComponent();
            Tools.ThemesTool.SetCurrentTheme(this);

            foreach (var item in MainPanel.Children)
            {
                var dp = item as DockPanel;
                if (dp != null) dp.Visibility = Visibility.Collapsed;
            }

            switch (type)
            {
                case MFLettersViewModel.TYPE_MF19:
                    dRegNumDSP.Visibility = Visibility.Visible;
                    dRegNumApp.Visibility = Visibility.Visible;
                    dSumm.Visibility = Visibility.Visible;
                    dPerson.Visibility = Visibility.Visible;
                    dManager.Visibility = Visibility.Visible;
                    dManagerApp.Visibility = Visibility.Visible;
                    dSecurity.Visibility = Visibility.Visible;
                    dDiff.Visibility = Visibility.Visible;
                    Height = 630;
                    Width = 546;
                    dSummLabel.Content = "Сумма (тыс.руб.):";
                    Title = "Уточненная сумма к размещению";
                    break;
                case MFLettersViewModel.TYPE_MF20:
                    dRegNumDSP.Visibility = Visibility.Visible;
                    dRegNumApp.Visibility = Visibility.Visible;
                    dReportDate.Visibility = Visibility.Visible;
                    dPerson.Visibility = Visibility.Visible;
                    dManager.Visibility = Visibility.Visible;
                    Title = "Структура портфеля ПФР";
                    break;
                case MFLettersViewModel.TYPE_MF21:
                    dRegNumDSP.Visibility = Visibility.Visible;
                    dSumm.Visibility = Visibility.Visible;
                    dPerson.Visibility = Visibility.Visible;
                    dManager.Visibility = Visibility.Visible;
                    dSummLabel.Content = "Сумма (млрд.руб.):";
                    Title = "Предварительная сумма";
                    break;
                case MFLettersViewModel.TYPE_MF22:
                    dRegNumDSP.Visibility = Visibility.Visible;
                    dPortfolio.Visibility = Visibility.Visible;
                    dSumm.Visibility = Visibility.Visible;
                    dPerson.Visibility = Visibility.Visible;
                    dManager.Visibility = Visibility.Visible;
                    dSummLabel.Content = "Сумма (млрд.руб.):";
                    Title = "Планируемая продажа";
                    break;
                case MFLettersViewModel.TYPE_MF23:
                    dRegNumDSP.Visibility = Visibility.Visible;
                    dPortfolio.Visibility = Visibility.Visible;
                    dDateStart.Visibility = Visibility.Visible;
                    dDateEnd.Visibility = Visibility.Visible;
                    dDateStart.Visibility = Visibility.Visible;
                    dBankAgent.Visibility = Visibility.Visible;
                    dSumm.Visibility = Visibility.Visible;
                    dPerson.Visibility = Visibility.Visible;
                    dManager.Visibility = Visibility.Visible;
                    dSummLabel.Content = "Сумма (тыс.руб.):";
                    Title = "Фактическая выручка";
                    Height = 400;
                    break;
            }

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as MFLettersViewModel;
            if (vm != null)
                DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
