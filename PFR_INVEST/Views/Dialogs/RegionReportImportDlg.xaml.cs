﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.RegionsReport
{
    /// <summary>
    /// Interaction logic for RegionReportImportDlg.xaml
    /// </summary>
    public partial class RegionReportImportDlg
    {
        public RegionReportImportDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }
    }
}
