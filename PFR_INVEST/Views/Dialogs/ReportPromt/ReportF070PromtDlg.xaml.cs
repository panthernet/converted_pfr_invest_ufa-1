﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class ReportF070PromtDlg
    {
        public bool IsNameChanged { get; private set; }
		public bool IsReadOnly { get; private set; }

		public ReportF070PromtDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {  
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        } 
    }
}
