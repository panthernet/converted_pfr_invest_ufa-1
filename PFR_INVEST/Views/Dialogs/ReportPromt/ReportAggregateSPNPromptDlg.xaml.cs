﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.XtraPrinting.Native;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs.ReportPromt
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class ReportAggregateSPNPromptDlg
    {
        public string ReportName => "Журнал агрегированного учета СПН НПФ";

        public ICommand OkCommand { get; set; }

        public ObservableCollection<LegalEntity> NpfList { get; set; } = new ObservableCollection<LegalEntity>();
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public ObservableCollection<Element> OpList { get; set; } = new ObservableCollection<Element>();

        public List<string> DirList { get; set; } = new List<string> { "Все", "Передано из ПФР в НПФ", "Возвращено из НПФ в ПФР" };
        public List<string> AddNpfList { get; set; } = new List<string> { "Не включать значения дочерних НПФ", "Включать значения дочерних НПФ" };
        public List<string> GarantList { get; set; } = new List<string> { "Все", "Входят" };
        public List<string> DetailList { get; set; } = new List<string> { "Детализированный", "Без детализации" };
        
        public bool IsGarant => cboxGarant.SelectedIndex == 1;

        public string Direction
            =>
                cboxDirection.SelectedIndex == 0
                    ? null
                    : (cboxDirection.SelectedIndex == 1
                        ? RegisterIdentifier.PFRtoNPF
                        : RegisterIdentifier.NPFtoPFR);


        //result
        public long[] ResultNpfIds => listNpf.SelectedItems.Cast<LegalEntity>().Select(a => a.ID).ToArray();
        public long[] ResultOpList => GetOps();

        private long[] GetOps()
        {
            if (cboxDirection.SelectedIndex > 0)
                return listOp.SelectedItems.Cast<Element>().Select(a => a.ID).ToArray();
            var names = listOp.SelectedItems.Cast<Element>().Select(a => a.Name);
            return _fullOpList.Where(a=> names.Contains(a.Name)).Select(a=> a.ID).ToArray();
        }

        public string ResultDirection => Direction;
        public bool ResultAddNpf => cboxAdditional.SelectedIndex == 1;
        public bool IsExtended => cboxType.SelectedIndex == 0;


        public ReportAggregateSPNPromptDlg()
        {
            InitializeComponent();
            DataContext = this;

            DateFrom = DateTime.Today.Subtract(TimeSpan.FromDays(30));
            DateTo = DateTime.Today;
            UpdateNpfList();


            butSelectAllOp.Click += (sender, e) =>
            {
                if (listOp.SelectedItems.Count == OpList.Count)
                    listOp.SelectedItems.Clear();
                else listOp.SelectAll();
            };
            butSelectAllnpf.Click += (sender, e) =>
            {
                if (listNpf.SelectedItems.Count == NpfList.Count)
                    listNpf.SelectedItems.Clear();
                else listNpf.SelectAll();
            };
            OkCommand = new DelegateCommand(a=> listNpf.SelectedItems.Any() && listOp.SelectedItems.Any() && DateFrom.HasValue && DateTo.HasValue, a=> DialogResult = true);

            listNpf.SelectedIndexChanged += (sender, e) =>
            {
            };

            listOp.SelectedIndexChanged += (sender, e) =>
            {
            };

            cboxGarant.SelectedIndexChanged += (sender, args) =>
            {
                UpdateNpfList();
            };
            cboxDirection.SelectedIndexChanged += (sender, e) =>
            {
                UpdateOpList();
            };
            cboxDirection.SelectedIndex = 0;
                UpdateOpList();
        }

        private List<Element> _fullOpList;

        private void UpdateOpList()
        {
            OpList.Clear();
            switch (Direction)
            {
                case RegisterIdentifier.NPFtoPFR:
                    WCFClient.Client.GetElementByType(Element.Types.RegisterNPFtoPFR)
                        .ForEach(a=> OpList.Add(a));
                    _fullOpList = OpList.ToList();
                    break;
                case RegisterIdentifier.PFRtoNPF:
                    WCFClient.Client.GetElementByType(Element.Types.RegisterPFRtoNPF)
                        .ForEach(a => OpList.Add(a));
                    _fullOpList = OpList.ToList();
                    break;
                default:
                    WCFClient.Client.GetElementByType(Element.Types.RegisterNPFtoPFR)
                        .ForEach(a => OpList.Add(a));
                    WCFClient.Client.GetElementByType(Element.Types.RegisterPFRtoNPF)
                        .ForEach(a => OpList.Add(a));
                    _fullOpList = OpList.ToList();
                    OpList = new ObservableCollection<Element>(OpList.GroupBy(s => s.Name)
                        .Select(grp => grp.FirstOrDefault())
                        .OrderBy(s => s.Name));
                    break;
            }
        }

        private void UpdateNpfList()
        {
            NpfList.Clear();
            WCFClient.Client.GetNPFListLEHib(StatusFilter.NotDeletedFilter)
                .Where(a=> !IsGarant || a.GarantACBID == (long)LegalEntity.GarantACB.Included)
                .ForEach(a=> NpfList.Add(a));
            listNpf.SelectedItems.Clear();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
