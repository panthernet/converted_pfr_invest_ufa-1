﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for NPF2NPFDlg.xaml
    /// </summary>
    public partial class NPF2NPFDlg
    {
        public NPF2NPFDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void TextEdit_Spin(object sender, DevExpress.Xpf.Editors.SpinEventArgs e)
        {
            e.Handled = true;
        }
    }
}
