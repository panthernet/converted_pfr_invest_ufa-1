﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RegionReportImportView.xaml
    /// </summary>
    public partial class ImportSIDlg
    {
        public ImportSIDlg()
        {
            InitializeComponent();            
            ModelInteractionHelper.SignUpForCloseRequest(this);
            ModelInteractionHelper.SubscribeForLoadingIndicator(this);
        }
    }
}
