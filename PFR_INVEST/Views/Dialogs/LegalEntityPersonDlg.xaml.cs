﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class LegalEntityPersonDlg
    {
        private bool _isDialogCompleted;
        public BankCourierDlgViewModel Model => DataContext as BankCourierDlgViewModel;

        public LegalEntityPersonDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = Model.DataChanged();
            DialogResult = _isDialogCompleted;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = true;
            DialogResult = false;
        }

        private void DXWindow_Closing(object sender, CancelEventArgs e)
        {
            if (_isDialogCompleted)//Skip asking if flag is already set
                return;

            bool? x = Model.AskSaveOnClose();

            switch (x)
            {
                case null:
                    e.Cancel = true;
                    break;
                case false:
                    DialogResult = false;
                    break;
                default:
                    if (Model.CanSaveOnClose())
                        DialogResult = true;
                    else
                        e.Cancel = true;
                    break;
            }
        }

        private void LOAGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var x = oldLoAGrid.GetFocusedRow() as LegalEntityCourierLetterOfAttorney;
            if (x == null)
                return;

            Model.EditLOA(x);
        }


        private void CertificateGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var x = CertificatesGrid.GetFocusedRow() as LegalEntityCourierCertificate;
            if (x == null)
                return;

            Model.EditCertificate(x);
        }
    }
}
