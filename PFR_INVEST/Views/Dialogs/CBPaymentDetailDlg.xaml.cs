﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class CBPaymentDetailDlg
    {
        private CBPaymentDetailDlgViewModel Model => DataContext as CBPaymentDetailDlgViewModel;

        public CBPaymentDetailDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }

        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Model != null && Model.IsDataChanged && !ViewModelBase.DialogHelper.ShowConfirmation("При закрытии окна, все изменения сбросятся. Продолжить?", "Внимание"))
                e.Cancel = true;
        }
    }
}
