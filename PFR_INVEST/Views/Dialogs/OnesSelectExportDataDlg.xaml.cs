﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Data;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Grid;
using DevExpress.XtraGrid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SendEmailsToUKDlg.xaml
    /// </summary>
    public partial class OnesSelectExportDataDlg
    {
        private OnesSelectExportDataViewModel Model => DataContext as OnesSelectExportDataViewModel;
        private readonly OnesSettingsDef.IntGroup _group;

        public OnesSelectExportDataDlg(OnesSettingsDef.IntGroup group)
        {
            InitializeComponent();
            _group = group;
            Loaded += OnesSelectExportDataDlg_Loaded;
            GridSummaryItem gsi;
            switch (group)
            {
                case OnesSettingsDef.IntGroup.Npf:
                    grid.Columns.Add(new GridColumn { FieldName = "Item2", Header = "Реестр", Name = "c1_0", GroupIndex = 1, SortOrder = ColumnSortOrder.Descending, SortIndex = 1, SortMode = ColumnSortMode.Value });
                    grid.Columns.Add(new GridColumn { FieldName = "Item3", Header = "Статус", Name = "c1_1", GroupIndex = 2, SortIndex = 2, SortMode = ColumnSortMode.Value });
                    grid.Columns.Add(new GridColumn { FieldName = "Item4", Header = "Наименование", Name = "c1_2", SortMode = ColumnSortMode.Value });
                    grid.Columns.Add(new GridColumn { FieldName = "Item5", Header = "Сумма", Name = "c1_3", SortMode = ColumnSortMode.Custom, EditSettings = new TextEditSettings { DisplayFormat = "n2", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item6", Header = "Кол-во ЗЛ", SortMode = ColumnSortMode.Custom, Name = "c1_4", EditSettings = new TextEditSettings { DisplayFormat = "n", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item7", Header = "Номер", SortMode = ColumnSortMode.Value, Name = "c1_5" });
                    grid.Columns.Add(new GridColumn { FieldName = "Item8", Header = "Дата", SortMode = ColumnSortMode.Value, Name = "c1_6", EditSettings = new DateEditSettings { DisplayFormat = "dd.MM.yyyy", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    gsi = grid.GroupSummary.Add(SummaryItemType.Sum, "Item5");
                    gsi.DisplayFormat = "Сумма: {0:##,##0.00}";
                    gsi = grid.GroupSummary.Add(SummaryItemType.Sum, "Item6");
                    gsi.DisplayFormat = "Кол-во ЗЛ: {0:####################,#}";
                    c1.Header = "Содержание операции";
                    break;
                case OnesSettingsDef.IntGroup.Si:
                case OnesSettingsDef.IntGroup.Vr:
                    grid.Columns.Add(new GridColumn { FieldName = "Item2", Header = "Реестр", SortMode = ColumnSortMode.Value, Name = "c1_1", GroupIndex = 1, SortOrder = ColumnSortOrder.Descending, SortIndex = 1 });
                    grid.Columns.Add(new GridColumn { FieldName = "Item3", Header = "Статус", SortMode = ColumnSortMode.Value, Name = "c1_2", GroupIndex = 2 });
                    grid.Columns.Add(new GridColumn { FieldName = "Item4", Header = "Наименование", SortMode = ColumnSortMode.Value, Name = "c1_3", GroupIndex = 3 });
                    grid.Columns.Add(new GridColumn { FieldName = "Item5", Header = "Номер договора", SortMode = ColumnSortMode.Value, Name = "c1_4", GroupIndex = 4 });
                    grid.Columns.Add(new GridColumn { FieldName = "Item6", Header = "Кол-во ЗЛ", SortMode = ColumnSortMode.Custom, Name = "c1_5", EditSettings = new TextEditSettings { DisplayFormat = "n", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item7", Header = "Сумма", SortMode = ColumnSortMode.Custom, Name = "c1_6", EditSettings = new TextEditSettings { DisplayFormat = "n2", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item8", Header = "Инвестдоход", SortMode = ColumnSortMode.Custom, Name = "c1_7", EditSettings = new TextEditSettings { DisplayFormat = "n2", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    gsi = grid.GroupSummary.Add(SummaryItemType.Sum, "Item6");
                    gsi.DisplayFormat = "{0:n0}"; 
                    gsi = grid.GroupSummary.Add(SummaryItemType.Sum, "Item7");
                    gsi.DisplayFormat = "{0:##,##0.00}"; 
                    gsi = grid.GroupSummary.Add(SummaryItemType.Sum, "Item8");
                    gsi.DisplayFormat = "{0:##,##0.00}"; 
                    c1.Header = "Содержание операции";
                    break;
                case OnesSettingsDef.IntGroup.Depo:
                    c1.EditSettings = new DateEditSettings { DisplayFormat = "dd.MM.yyyy"};
                    c1.FieldName = "Date";
                    grid.Columns.Add(new GridColumn { FieldName = "Item2", Header = "Биржа", Name = "c1_1", SortMode = ColumnSortMode.Value });
                    grid.Columns.Add(new GridColumn { FieldName = "Item3", Header = "Статус", Name = "c1_2", SortMode = ColumnSortMode.Value });
                    grid.Columns.Add(new GridColumn { FieldName = "Item4", Header = "№ Оферты", Name = "c1_3", SortMode = ColumnSortMode.Value, EditSettings = new TextEditSettings { DisplayFormat = "n", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item5", Header = "Краткое наименование Участника", SortMode = ColumnSortMode.Value, Name = "c1_4" });
                    grid.Columns.Add(new GridColumn { FieldName = "Item6", Header = "Процентная ставка (% годовых)", SortMode = ColumnSortMode.Custom, Name = "c1_5", EditSettings = new TextEditSettings { DisplayFormat = "n2", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item7", Header = "Сумма (руб.)", Name = "c1_6", SortMode = ColumnSortMode.Custom, EditSettings = new TextEditSettings { DisplayFormat = "n2", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item8", Header = "Сумма процентов, рассчитанная исходя из указанной в заявке суммы (руб.)", SortMode = ColumnSortMode.Custom, Name = "c1_7", EditSettings = new TextEditSettings { DisplayFormat = "n2", HorizontalContentAlignment = EditSettingsHorizontalAlignment.Right } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item9", Header = "Дата размещения средств в депозит", SortMode = ColumnSortMode.Value, Name = "c1_8", EditSettings = new DateEditSettings { DisplayFormat = "dd.MM.yyyy" } });
                    grid.Columns.Add(new GridColumn { FieldName = "Item10", Header = "Дата возврата средств", SortMode = ColumnSortMode.Value, Name = "c1_9", EditSettings = new DateEditSettings { DisplayFormat = "dd.MM.yyyy" } });
                    
                    gsi = grid.GroupSummary.Add(SummaryItemType.Sum, "Item7");
                    gsi.DisplayFormat = "{0:##,##0.00}"; 
                    gsi = grid.GroupSummary.Add(SummaryItemType.Sum, "Item8");
                    gsi.DisplayFormat = "{0:##,##0.00}"; 
                    c1.Header = "Дата аукциона";
                    break;
                case OnesSettingsDef.IntGroup.Opfr:
                    c1.Header = "Направление";
                    grid.Columns.Add(new GridColumn { FieldName = "Item2", Header = "Распоряжение", Name = "c1_1", SortMode = ColumnSortMode.Value });
                    break;
            }
            view.ShowFilterPopup += view_ShowFilterPopup;
        }

        private void view_ShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (_group == OnesSettingsDef.IntGroup.Npf && (e.Column.FieldName == "Item5" || e.Column.FieldName == "Item6") || _group == OnesSettingsDef.IntGroup.Si || _group == OnesSettingsDef.IntGroup.Vr && (e.Column.FieldName == "Item6" || e.Column.FieldName == "Item7" || e.Column.FieldName == "Item8") || _group == OnesSettingsDef.IntGroup.Depo && (e.Column.FieldName == "Item4" || e.Column.FieldName == "Item6" || e.Column.FieldName == "Item7" || e.Column.FieldName == "Item8"))
                ComponentHelper.OnColumnPopupDecimal(sender, e);
        }

        private void OnesSelectExportDataDlg_Loaded(object sender, RoutedEventArgs e)
        {
            Focus();
            Model.RequestClose += Model_RequestClose;
        }

        private void Model_RequestClose(object sender, System.EventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void UpdateSelectionForAll(bool newIsSelected)
        {
            var list = GetDataRowHandles();
            for (int i = 0; i < list.Count; i++)
            {
                int rowHandle = grid.GetRowHandleByListIndex(i);
                grid.SetCellValue(rowHandle, "Selected", newIsSelected);
            }
        }

        private bool _mGridRowCheckBoxClicked;

        private void CheckEdit_Checked(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                UpdateSelectionForAll(true);
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Indeterminate(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                _headerCheckEdit.IsChecked = false;
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Unchecked(object sender, RoutedEventArgs e)
        {
            UpdateSelectionForAll(false);
            _mGridRowCheckBoxClicked = false;
        }

        private List<int> GetDataRowHandles()
        {
            var rowHandles = new List<int>();
            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                int rowHandle = grid.GetRowHandleByVisibleIndex(i);
                if (grid.IsGroupRowHandle(rowHandle))
                {
                    if (!grid.IsGroupRowExpanded(rowHandle))
                    {
                        rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                    }
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        private IEnumerable<int> GetDataRowHandlesInGroup(int groupRowHandle)
        {
            var rowHandles = new List<int>();
            for (int i = 0; i < grid.GetChildRowCount(groupRowHandle); i++)
            {
                int rowHandle = grid.GetChildRowHandle(groupRowHandle, i);
                if (grid.IsGroupRowHandle(rowHandle))
                {
                    rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        private CheckBox _headerCheckEdit;
        private void CheckEdit_Loaded(object sender, RoutedEventArgs e)
        {
            _headerCheckEdit = sender as CheckBox;
        }
    }
}
