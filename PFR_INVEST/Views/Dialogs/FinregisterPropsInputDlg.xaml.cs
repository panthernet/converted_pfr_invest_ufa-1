﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class FinregisterPropsInputDlg
    {
        private readonly DispatcherTimer _timer;

        private FinRegisterViewModel Model => DataContext as FinRegisterViewModel;

        private string _initialRegNum;
        private DateTime? _initialDate;
        private decimal? _initialCount;

        public FinregisterPropsInputDlg()
        {
            InitializeComponent();
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(200) };
            _timer.Tick += Tick;
            _timer.Start();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            if (Model != null)
                Model.Status = RegisterIdentifier.FinregisterStatuses.Issued;
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            Model.RegNum = _initialRegNum;
            Model.Date = _initialDate;
            Model.Count = _initialCount;
            DialogResult = false;
        }

        private void DXWindow_Closing(object sender, CancelEventArgs e)
        {
            if (Model != null && DialogResult == null)
            {
                switch (DXMessageBox.Show("Сохранить документ перед закрытием?", "Сохранение документа", MessageBoxButton.YesNo, MessageBoxImage.Question))
                {
                    case MessageBoxResult.Yes:
                        Model.SaveCard.Execute(null);
                        break;
                    case MessageBoxResult.No:
                        _timer.Stop();
                        Model.RegNum = _initialRegNum;
                        Model.Date = _initialDate;
                        Model.Count = _initialCount;
                        break;
                    default:
                        break;
                }
            }
        }

        private void DXWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _initialRegNum = Model.RegNum;
            _initialDate = Model.Date;
            _initialCount = Model.Count;
            if (Model.Date == null ||
                Model.Date != null && Model.Date.Value == DateTime.MinValue)
                Model.Date = DateTime.Now;
            //Model.RefreshProps();
            frPropsView.date.EditValue = Model.Date;
        }

        private void Tick(object state, EventArgs e)
        {
            OKButton.IsEnabled = Model["RegNum"] == null && Model["Date"] == null && Model["Count"] == null;
        }
    }
}
