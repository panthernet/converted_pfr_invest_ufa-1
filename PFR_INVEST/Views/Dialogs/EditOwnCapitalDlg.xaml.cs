﻿using System.ComponentModel;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EditOwnCapitalDlg.xaml
    /// </summary>
    public partial class EditOwnCapitalDlg
    {
        public EditOwnCapitalDlgViewModel Model => DataContext as EditOwnCapitalDlgViewModel;

        public EditOwnCapitalDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void DXWindow_Closing(object sender, CancelEventArgs e)
        {

            if (Model != null && DialogResult == null && Model.IsDataChanged)
            {
                if(DXMessageBox.Show("Сохранить документ перед закрытием?", "Сохранение документа", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                Model.SaveCard.Execute(null);
                DialogResult = true;
            }

            if (DialogResult == true && Model.HasImportedDuplicates())
                e.Cancel = true;
        
        }

    }
}
