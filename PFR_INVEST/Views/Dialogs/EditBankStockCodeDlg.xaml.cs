﻿using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EditLegalEntityHeadDlg.xaml
    /// </summary>
    public partial class EditBankStockCodeDlg
    {
        public EditBankStockCodeDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
			var vm = DataContext as BankStockCodeDlgViewModel;
			if (vm != null)
			{
				var error = vm.ValidateStockCodeUnique();
				if (error != null)
				{
					DXMessageBox.Show(error);
					return;
				}

				DialogResult = true;
			}
		}

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
