﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddSIContactDlg.xaml
    /// </summary>
    public partial class AddSIContactDlg
    {
        public AddSIContactDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
             var vm = DataContext as SIContactDlgViewModel;
             if (vm != null)
                 DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
