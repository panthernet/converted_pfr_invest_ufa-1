﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RegionReportImportView.xaml
    /// </summary>
    public partial class ImportOpfrTransfersDlg
    {
        public ImportOpfrTransfersDlg()
        {
            InitializeComponent();            
            ModelInteractionHelper.SignUpForCloseRequest(this);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, mainGrid, "Items");
        }
    }
}
