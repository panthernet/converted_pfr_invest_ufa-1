﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SIRequestMassDlg.xaml
    /// </summary>
    public partial class SIRequestMassDlg
    {
        private class AddressTypeItem
        {
            public string Title { get; set; }
            public AddressType Value { get; set; }
        }
        public int Year => (int)(((Year)cbYear.SelectedItem)?.ID ?? 0 + 2000);
        public int Month => cbMonth.SelectedIndex + 1;

        private const string SELECT_FOLDER_DIALOG_DESC = "Выберите папку для сохранения требований.";
        private const string INVALID_SELECTED_FOLDER = "Неверно указана папка для сохранения требований!";
        private const string INITIAL_SELECTED_FOLDER = "C:\\";
        public string SelectedFolder;

        public SIRequestMassDlg(bool showDates)
        {
            InitializeComponent();
            txtSelectedFolder.EditValue = INITIAL_SELECTED_FOLDER;
            AddressTypeCombo.Items.Add(new AddressTypeItem { Value = AddressType.Post, Title = "Почтовый" });
            AddressTypeCombo.Items.Add(new AddressTypeItem { Value = AddressType.Fact, Title = "Фактический" });
            AddressTypeCombo.SelectedIndex = 0;
            if (showDates)
            {
                var yList = DataContainerFacade.GetList<Year>();
                cbYear.ItemsSource = yList;
                var curYear = DateTime.Today.Year - 2000;
                cbYear.SelectedItem = yList.FirstOrDefault(a=> a.ID == curYear) ?? yList.FirstOrDefault();
                var mList = DateTools.Months;
                cbMonth.ItemsSource = mList;
                cbMonth.SelectedItem = mList[DateTime.Today.Month - 1];
            }
            else dpDates.Visibility = Visibility.Collapsed;
        }

        private static bool CheckDirectoryName(string folder)
        {
            return Directory.Exists(folder) && folder == Path.GetFullPath(folder);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            string folder = Convert.ToString(txtSelectedFolder.EditValue);
            if (!CheckDirectoryName(folder))
            {
                DXMessageBox.Show(INVALID_SELECTED_FOLDER, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SelectedFolder = folder;
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SelecFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dlg = new FolderBrowserDialog { Description = SELECT_FOLDER_DIALOG_DESC })
            {
                if (Directory.Exists(txtSelectedFolder.Text))
                    dlg.SelectedPath = txtSelectedFolder.Text;

                if(dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    txtSelectedFolder.EditValue = dlg.SelectedPath;
            }
        }

        public AddressType SelectedType => ((AddressTypeItem)AddressTypeCombo.SelectedItemValue).Value;
    }
}
