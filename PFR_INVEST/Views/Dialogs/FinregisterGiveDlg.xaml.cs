﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class FinregisterGiveDlg
    {
        private readonly DispatcherTimer _timer;
        private FinRegisterViewModel Model => DataContext as FinRegisterViewModel;
        private FinRegStateViewModel ModelReg => DataContext as FinRegStateViewModel;

        private string _initialFinNum;
        private DateTime? _initialFinDate;
        private string _initialFinMoveType;

        public FinregisterGiveDlg(bool isMassive = false)
        {
            InitializeComponent();
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(200) };
            _timer.Tick += Tick;
            _timer.Start();

            if (isMassive)
            {
                frGiveView.numPanel.Visibility = Visibility.Collapsed;
                Title = "Ввод реквизитов для группы финреестров";
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            if (Model != null)
                Model.Status = RegisterIdentifier.FinregisterStatuses.Transferred;
            if (ModelReg != null)
                ModelReg.Status = RegisterIdentifier.FinregisterStatuses.Transferred;

            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            if (Model != null)
            {
                Model.FinNum = _initialFinNum;
                Model.FinDate = _initialFinDate;
                Model.FinMoveType = _initialFinMoveType;
            }
            else
            {
                ModelReg.FinNum = _initialFinNum;
                ModelReg.FinDate = _initialFinDate;
                ModelReg.FinMoveType = _initialFinMoveType;
            }
            DialogResult = false;
        }

        private void DXWindow_Closing(object sender, CancelEventArgs e)
        {
            if (Model == null || DialogResult != null) return;
            switch (DXMessageBox.Show("Сохранить документ перед закрытием?", "Сохранение документа", MessageBoxButton.YesNo, MessageBoxImage.Question))
            {
                case MessageBoxResult.Yes:
                    Model.SaveCard.Execute(null);
                    break;
                case MessageBoxResult.No:
                    _timer.Stop();
                    Model.FinNum = _initialFinNum;
                    Model.FinDate = _initialFinDate;
                    Model.FinMoveType = _initialFinMoveType;
                    break;
            }
        }

        private void DXWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null)
            {
                _initialFinNum = Model.FinNum;
                Model.FinNum = WCFClient.Client.GetLeIdentifierCodeForFinregister(Model.ID, RegisterIdentifier.IsFromNPF(Model.RegisterKind));
                _initialFinDate = Model.FinDate;
                _initialFinMoveType = Model.FinMoveType;
                if (Model.FinDate == null ||
                    Model.FinDate != null && Model.FinDate.Value == DateTime.MinValue)
                    Model.FinDate = DateTime.Now;
                frGiveView.date.EditValue = Model.Date;
            }
            else
            {
                if (ModelReg.FinDate == null ||
                    ModelReg.FinDate != null && ModelReg.FinDate.Value == DateTime.MinValue)
                    ModelReg.FinDate = DateTime.Now;
                frGiveView.date.EditValue = ModelReg.Date;
            }
        }

        private void Tick(object state, EventArgs e)
        {
            if(Model != null)
                OKButton.IsEnabled = Model["FinNum"] == null && Model["FinDate"] == null && Model["FinMoveType"] == null;
            else OKButton.IsEnabled =  ModelReg["FinDate"] == null && ModelReg["FinMoveType"] == null;
        }
    }
}
