﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for OpenCBReportApprovementStatusDlg.xaml
    /// </summary>
    public partial class OpenCBReportApprovementStatusDlg
    {
        private OpenCBReportApprovementStatusDlgViewModel Model => (OpenCBReportApprovementStatusDlgViewModel) DataContext;

        public OpenCBReportApprovementStatusDlg()
        {
            InitializeComponent();
        }

        private void Refresh_OnClick(object sender, RoutedEventArgs e)
        {
            if (!Model.Refresh())
            {
                labelDate2.Visibility = labelName2.Visibility = labelStatus2.Visibility = Visibility.Collapsed;
            }else labelDate2.Visibility = labelName2.Visibility = labelStatus2.Visibility = Visibility.Visible;
        }

        private void Close_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
