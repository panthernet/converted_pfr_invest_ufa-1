﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SupAgreementView.xaml
    /// </summary>
    public partial class SupAgreementView : UserControl
    {
        public SupAgreementView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
        }
    }
}
