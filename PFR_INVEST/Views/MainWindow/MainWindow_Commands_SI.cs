﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Tools;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Common;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.Reports;
using PFR_INVEST.Tools;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.Views.Dialogs.Common;
using PFR_INVEST.Views.Dialogs.ReportPromt;

namespace PFR_INVEST
{
    public partial class MainWindow
    {
        #region Работа с ВР

        private void ShowTransferVRWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(Views.TransferWizard.TransferVRWizardView),
                                            ViewModelState.Read, 0, "Мастер перечислений ВР");
        }

        private void ShowVRTransferList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransfersVRListView), ViewModelState.Read, "Перечисления ВР");
        }

        private void ShowArchiveVRTransfersList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransfersVRArchListView), ViewModelState.Read, "Архив перечислений ВР");
        }

        private void CreateVRTransfer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lv = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (lv != null)
            {
                if (lv.IsRegisterSelected() && lv.GetSelectedRegisterID() > 0)
                    App.DashboardManager.OpenNewTab(typeof(TransferVRView),
                                                    ViewModelState.Create, lv.GetSelectedRegisterID(), "Создать список перечислений ВР", "Список перечислений ВР");
                return;
            }

            var lv2 = App.DashboardManager.GetActiveView() as TransfersVRArchListView;
            if (lv2 != null)
            {
                if ((lv2.IsRegisterSelected()) && (lv2.GetSelectedRegisterID() > 0))
                    App.DashboardManager.OpenNewTab(typeof(TransferVRView),
                                                    ViewModelState.Create, (long)(lv2.GetSelectedRegisterID() ?? 0), "Создать список перечислений ВР", "Список перечислений ВР");
                return;
            }

            var vm = App.DashboardManager.GetActiveViewModel() as SIVRRegisterVRViewModel;
            if (vm != null)
                App.DashboardManager.OpenNewTab(typeof(TransferVRView), ViewModelState.Create,
                                                vm.ID, "Создать список перечислений ВР", "Список перечислений ВР");
        }

        private void CreateVRTransfer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var lv = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (lv != null)
            {
                e.CanExecute = ((lv.IsRegisterSelected()) && (lv.GetSelectedRegisterID() > 0));
                return;
            }

            var lv2 = App.DashboardManager.GetActiveView() as TransfersVRArchListView;
            if (lv2 != null)
            {
                e.CanExecute = ((lv2.IsRegisterSelected()) && (lv2.GetSelectedRegisterID() > 0));
                return;
            }

            var vm = App.DashboardManager.GetActiveViewModel() as SIVRRegisterVRViewModel;
            e.CanExecute = vm != null && vm.ID > 0;
        }

        private void CreateVRReqTransfer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Create,
                                                view.GetSelectedTransferID(), "Создать перечисление ВР", "Перечисление Вр");
            }
            else
            {
                var view2 = App.DashboardManager.GetActiveView() as TransfersVRArchListView;
                if (view2 != null)
                {
                    App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Create,
                                                    (long)(view2.GetSelectedTransferID() ?? 0), "Создать перечисление ВР", "Перечисление ВР");
                }
                else
                {
                    var vm = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
                    if (vm.ID > 0)
                        App.DashboardManager.OpenNewTab(typeof(ReqTransferView),
                                                        ViewModelState.Create, vm.ID, "Создать перечисление ВР", "Перечисление ВР");
                    else //если Список перечислений еще не сохранен
                    {
                        if (vm.SelectedContract != null && vm.SelectedUK != null)
                        {
                            var param = new object[] { vm.TransferList, null, vm };
                            App.DashboardManager.OpenNewTab(typeof(ReqTransferView),
                                                            ViewModelState.Create, param, "Создать перечисление ВР", "Перечисление ВР");
                        }
                        else
                        {
                            new DialogHelper().ShowAlert("Для добавления перечисления необходимо выбрать УК и номер договора!");
                        }
                    }
                }
            }
        }

        private void CreateVRReqTransfer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                e.CanExecute = ((view.IsTransferSelected()) && (view.GetSelectedTransferID() > 0));
            }
            else
            {
                var view2 = App.DashboardManager.GetActiveView() as TransfersVRArchListView;
                if (view2 != null)
                {
                    e.CanExecute = ((view2.IsTransferSelected()) && (view2.GetSelectedTransferID() > 0));
                }
                else
                {
                    var vm = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
                    e.CanExecute = vm != null;
                }
            }
        }

        private void ShowZLTransfersVRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ZLMovementsVRListView), ViewModelState.Read, "Журнал движения ЗЛ ВР");
        }

        private void ShowSPNMovementsVRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SPNMovementsVRListView), ViewModelState.Read, "Журнал движения СПН ВР");
        }

        private void ShowSPNMovementsUKVRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SPNMovementsUKVRListView),
                                            ViewModelState.Read, "Журнал движения СПН ВР (УК)");
        }

        private void RecalcUKVRPortfolios_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RecalcUKVRPortfoliosView),
                                            ViewModelState.Read, "Пересчет портфелей УК ВР");
        }

        private void CreatePrintReqVR_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                e.CanExecute = TransferDirectionIdentifier.IsFromPFRToUK(view.GetSelectedDirection())
                               && ((view.IsRegisterSelected() && view.SelectedRegisterHasInitialSateTransfers())
                               || (view.IsReqTransferSelected() && TransferStatusIdentifier.IsStatusInitialState(view.GetSelectedStatus())));
                return;
            }

            var regVM = App.DashboardManager.GetActiveViewModel() as SIVRRegisterVRViewModel;
            if (regVM != null)
            {
                e.CanExecute = regVM.SelectedDirection.isFromUKToPFR != null &&
                               !regVM.SelectedDirection.isFromUKToPFR.Value &&
                               regVM.RegisterHasInitialSateTransfers;
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                e.CanExecute = trVM.Direction.isFromUKToPFR != null &&
                    !trVM.Direction.isFromUKToPFR.Value &&
                    trVM.IsTransferSaved &&
                    trVM.DocumentType == Document.Types.VR &&
                    TransferStatusIdentifier.IsStatusInitialState(trVM.TransferStatus);
            }
        }

        private void CreatePrintReqVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SIRegister register = null;
            ReqTransfer rTransfer = null;

            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                if (view.IsRegisterSelected())
                    register = DataContainerFacade.GetByID<SIRegister, long>(view.GetSelectedRegisterID());
                else
                    rTransfer = DataContainerFacade.GetByID<ReqTransfer>(view.GetSelectedReqTransferID());
            }

            //SIVRRegisterVRViewModel regVM = null;
            if (register == null && rTransfer == null)
            {
                var regVM = App.DashboardManager.GetActiveViewModel() as SIVRRegisterVRViewModel;
                if (regVM != null)
                    register = regVM.TransferRegister;
            }

            if (register == null && rTransfer == null)
            {
                var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
                if (trVM != null)
                {
                    rTransfer = trVM.Transfer;
                }
            }

            if (register == null && rTransfer == null)
                return;

            var vmi = new ReqTransferClaimInputDlgViewModel(true, register == null ? rTransfer.GetTransferList().GetTransferRegister().RegisterNumber : register.RegisterNumber);
            var dlg = new ReqTransferClaimInputDlg { DataContext = vmi, Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            if (dlg.ShowDialog() != true) return;

            var printReqTransfers = register == null ? new PrintReqTransfers(rTransfer, true, vmi.TrancheNumber, vmi.TrancheDate) : new PrintReqTransfers(register, true, vmi.TrancheNumber, vmi.TrancheDate);

            //var printReqTransfers = register == null ? new PrintReqTransfers(rTransfer, true) : new PrintReqTransfers(register, true);

            if (printReqTransfers.Print())
            {
                App.DashboardManager.RefreshListViewModels(new[]
                                                               {
                                                                   typeof (TransfersVRListViewModel),
                                                                   typeof (VRAssignPaymentsListViewModel),
                                                                   typeof (VRAssignPaymentsArchiveListViewModel),
                                                                   typeof (UKTransfersListViewModel), 
                                                               });

                var reqvm = App.DashboardManager.FindViewModel<ReqTransferViewModel>();
                if (reqvm != null && reqvm.ID > 0)
                {
                    var alreadyChanged = reqvm.IsDataChanged;
                    reqvm.TransferDate = DateTime.Today;
                    reqvm.LoadCardData(reqvm.ID);
                    reqvm.IsDataChanged = alreadyChanged;
                }

                var vm = App.DashboardManager.FindViewModel<TransferVRViewModel>();
                vm?.RefreshTransfers();
                var msg = register != null
                    ? $"Заявка на перечисления реестра № {register.ID}, {register.RegisterKind} сформирована"
                    : $"Заявка на перечисление № {rTransfer.ID}, {rTransfer.GetTransferList().GetTransferRegister().RegisterKind} сформирована";

                JournalLogger.LogChangeStateEvent(register == null ? typeof (ReqTransfer) : typeof(Register), msg,
                    register?.ID ?? rTransfer.ID);
            }
        }

        private void FromUKEnterTransferActDataVR_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                if (view.IsRegisterSelected())
                {
                    //Массовый ввод даты
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   view.SelectedRegisterHasReceivedByUKTransfers();
                    return;
                }

                e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                               view.IsReqTransferSelected() &&
                               TransferStatusIdentifier.IsStatusRequestReceivedByUK(view.GetSelectedStatus());
                return;
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.VR &&
                        TransferStatusIdentifier.IsStatusRequestReceivedByUK(trVM.Transfer.TransferStatus);
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
            if (trListVM?.SelectedTransferRequest == null)
                return;
            if (trListVM.IsTransferFromUKToPFR)
                e.CanExecute =
                    TransferStatusIdentifier.IsStatusRequestReceivedByUK(
                        trListVM.SelectedTransferRequest.TransferStatus);
        }

        private void EnterTransferActDataVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sirtIsDataChangeBeforeStatusChange = false;
            var dlgVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (dlgVM == null)
            {
                var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
                if (view != null)
                {
                    if (view.IsRegisterSelected())
                    {
                        var regN = view.GetSelectedRegisterNumber();
                        var l = view.GetTransfersInSelectedRegister();
                        if (!l.All(x => TransferStatusIdentifier.IsStatusRequestReceivedByUK(x.Status)))
                            new DialogHelper().ShowAlert("Не все перечисления выбранного реестра еще находятся в статусе \"Требование получено УК\"");
                        else
                        {
                            ProcessEnterTransferActDataForMultipleRegisters(l.Where(x => TransferStatusIdentifier.IsStatusRequestReceivedByUK(x.Status)).ToList());
                            new DialogHelper().ShowAlert("Даты Актов передачи введены всем перечислениям реестра {0}. Реестр перемещен в архив перечислений.", regN);
                        }
                        return;
                    }

                    if (view.GetSelectedReqTransferID() > 0)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(view.GetSelectedReqTransferID(), ViewModelState.Edit);
                        }
                    }
                }
                else
                {
                    var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
                    if (trListVM?.SelectedTransferRequest != null)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                        }
                    }
                }
            }
            else
                sirtIsDataChangeBeforeStatusChange = dlgVM.IsDataChanged;

            var contract = dlgVM?.Transfer.GetTransferList().GetContract();
            var legalEntity = contract.GetLegalEntity();

            var dlg = new EnterTransferActDateDlg();
            dlg.SetTitle(legalEntity.FormalizedName, contract?.ContractNumber);
            if (dlgVM?.Direction.isFromUKToPFR != null && dlgVM.Direction.isFromUKToPFR.Value)
            {
                if (dlgVM.Transfer.SPNDebitDate != null)
                    dlg.SetSPNDebitDate(dlgVM.Transfer.SPNDebitDate.Value);
            }
            dlg.DataContext = dlgVM;//.Transfer;
            dlg.Owner = this;
            ThemesTool.SetCurrentTheme(dlg);
            dlgVM.State = ViewModelState.Edit;

            if (dlg.ShowDialog()== true)
            {
                ProcessEnterTransferActDate(dlgVM, dlg.Title);
                JournalLogger.LogChangeStateEvent(dlgVM, "Введена дата акта передачи");
                //Обновляем списки и карточки
                App.DashboardManager.RefreshListViewModels(new[]
                                                               {
                                                                   typeof (TransfersVRListViewModel),
                                                                   typeof (TransfersVRArchListViewModel),
                                                                   typeof (VRAssignPaymentsListViewModel),
                                                                   typeof (VRAssignPaymentsArchiveListViewModel),
                                                                   typeof (ReqTransferView),
																   typeof (SPNMovementsVRListViewModel)
                                                               });

                var vm = App.DashboardManager.FindViewModel<TransferVRViewModel>();
                vm?.RefreshTransfers();
                var regVM = App.DashboardManager.FindViewModel<SIVRRegisterVRViewModel>();
                regVM?.RefreshTransferLists();
                var contractVM = App.DashboardManager.FindViewModel<SIContractViewModel>();
                if (contractVM != null)
                    contractVM.OperateSum = dlgVM.Contract.OperateSum.Value;
            }
            dlgVM.IsDataChanged = sirtIsDataChangeBeforeStatusChange;
        }

        private void ToUKEnterTransferActDataVR_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected())
                {
                    e.CanExecute = TransferDirectionIdentifier.IsFromPFRToUK(view.GetSelectedDirection()) &&
                                   TransferStatusIdentifier.IsStatusSPNTransfered(view.GetSelectedStatus());
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromPFRToUK)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.VR &&
                        TransferStatusIdentifier.IsStatusSPNTransfered(trVM.Transfer.TransferStatus) &&
                                   trVM.IsTransferFromPFRToUK;
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
            if (trListVM?.SelectedTransferRequest == null)
                return;
            e.CanExecute =
                TransferStatusIdentifier.IsStatusSPNTransfered(trListVM.SelectedTransferRequest.TransferStatus) &&
                !trListVM.IsTransferFromUKToPFR;
        }

        private void CreateVRRequest_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected())
                {
                    //Одино перечисление
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   TransferStatusIdentifier.IsStatusInitialState(view.GetSelectedStatus());
                    return;
                }
                if (view.IsRegisterSelected())
                {
                    //Массовая выгрузка
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   view.SelectedRegisterHasInitialSateTransfers();
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    //Одно перечисление
                    e.CanExecute = trVM.DocumentType == Document.Types.VR &&
                        TransferStatusIdentifier.IsStatusInitialState(trVM.Transfer.TransferStatus) &&
                                   trVM.Transfer.Sum.HasValue && !trVM.IsDataChanged;
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
            if (trListVM == null)
                return;
            if (trListVM.IsTransferFromUKToPFR)
            {
                //Одно перечисление
                e.CanExecute = trListVM.SelectedTransferRequest != null &&
                               trListVM.SelectedTransferRequest.ID > 0 &&
                               TransferStatusIdentifier.IsStatusInitialState(trListVM.SelectedTransferRequest.TransferStatus);
            }
        }

        private void CreateVRRequest_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ReqTransferViewModel trVM = null;

            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                if (view.IsRegisterSelected())
                {
                    //Массовая выгрузка
                    var lID = view.GetSelectedRegisterID();
                    if (lID <= 0)
                        return;
                    try
                    {
                        PrintManyRequests(trVM, lID, view.GetSelectedRegisterNumber(),
                            view.GetNumberOfInitialSateTransfersInSelectedRegister(), true);
                    }
                    catch (Exception ex)
                    {
                        CheckMSOException(ex);
                    }
                    return;
                }
                if (view.IsReqTransferSelected())
                {
                    //Одиночный реквест
                    var lID = view.GetSelectedReqTransferID();
                    if (lID > 0)
                    {
                        using (Loading.StartNew())
                        {
                            trVM = new ReqTransferViewModel(lID, ViewModelState.Edit);
                        }
                    }
                }
            }
            else
            {
                var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
                if (trListVM != null)
                {
                    using (Loading.StartNew())
                    {
                        trVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                    }
                }
            }

            if (trVM == null)
                trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;

            if (trVM == null) return;
            {
                try
                {
                    PrintOneRequest(trVM, true);
                }
                catch (Exception ex)
                {
                    CheckMSOException(ex);
                }
            }
        }

        private void FromUKEnterRequirementDataVR_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected() || view.IsRegisterSelected())
                {
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   view.GetSelectedStatuses().Any(TransferStatusIdentifier.IsStatusRequestFormed);
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.VR &&
                        TransferStatusIdentifier.IsStatusRequestFormed(trVM.Transfer.TransferStatus);
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
            if (trListVM?.SelectedTransferRequest == null)
                return;
            if (!trListVM.IsTransferFromUKToPFR)
                return;
            e.CanExecute =
                TransferStatusIdentifier.IsStatusRequestFormed(trListVM.SelectedTransferRequest.TransferStatus);
        }

        private void FromUKEnterRequirementDataVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlgVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            var listReqTransferID = new List<long>();
            var sirtIsDataChangeBeforeStatusChange = false;
            if (dlgVM == null)
            {
                var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
                if (view != null && view.IsRegisterSelected())
                {
                    listReqTransferID.AddRange(view.GetReqTransferIDBySelectedRegisterID(TransferStatusIdentifier.sDemandGenerated));
                    if (listReqTransferID.Any())
                    {
                        var id = listReqTransferID.First();
                        listReqTransferID.Remove(id);
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(id, ViewModelState.Edit);
                        }
                    }
                    else return;
                }
                else if (view != null && view.IsReqTransferSelected() && view.GetSelectedReqTransferID() > 0)
                {
                    using (Loading.StartNew())
                    {
                        dlgVM = new ReqTransferViewModel(view.GetSelectedReqTransferID(), ViewModelState.Edit);
                    }
                }
                else
                {
                    var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
                    if (trListVM != null)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                        }
                    }
                }
            }
            else
                sirtIsDataChangeBeforeStatusChange = dlgVM.IsDataChanged;

            dlgVM.RequestCreationDate = DateTime.Now;

            var dlg = new FromUKEnterRequirementDataDlg { Owner = this, DataContext = dlgVM };
            var c = dlgVM.Transfer.GetTransferList().GetContract();
            dlg.setTitle(c.GetLegalEntity().FormalizedName, c.ContractNumber);

            ThemesTool.SetCurrentTheme(dlg);

            if (dlg.ShowDialog() == true)
            {
                //DataContainerFacade.Save<ReqTransfer>(dlgVM.Transfer);
                var ed = dlgVM.Transfer.ExtensionData;
                dlgVM.Transfer.ExtensionData = null;
                DataContainerFacade.Save(dlgVM.Transfer);
                dlgVM.Transfer.ExtensionData = ed;

                foreach (var id in listReqTransferID)
                {
                    var reqTransfer = DataContainerFacade.GetByID<ReqTransfer, long>(id);
                    reqTransfer.RequestFormingDate = dlgVM.Transfer.RequestFormingDate;
                    reqTransfer.RequestNumber = dlgVM.Transfer.RequestNumber;
                    reqTransfer.TransferStatus = dlgVM.Transfer.TransferStatus;
                    DataContainerFacade.Save(reqTransfer);
                }

                JournalLogger.LogChangeStateEvent(dlgVM, "Введены реквизиты требования");

                App.DashboardManager.RefreshListViewModels(new[]
                                                               {
                                                                   typeof (TransfersVRListViewModel),
                                                                   typeof (VRAssignPaymentsListViewModel),
                                                                   typeof (VRAssignPaymentsArchiveListViewModel)
                                                               });

                var vm = App.DashboardManager.FindViewModel<TransferVRViewModel>();
                vm?.RefreshTransfers();
                var regVM = App.DashboardManager.FindViewModel<SIVRRegisterVRViewModel>();
                regVM?.RefreshTransferLists();
            }
            dlgVM.IsDataChanged = sirtIsDataChangeBeforeStatusChange;
        }

        private void FromUKEnterGetRequirementDateVR_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected())
                {
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   TransferStatusIdentifier.IsStatusSPNTransfered(view.GetSelectedStatus());
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.VR &&
                        TransferStatusIdentifier.IsStatusSPNTransfered(trVM.Transfer.TransferStatus);
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
            if (trListVM?.SelectedTransferRequest == null)
                return;
            if (trListVM.IsTransferFromUKToPFR)
                e.CanExecute =
                    TransferStatusIdentifier.IsStatusSPNTransfered(trListVM.SelectedTransferRequest.TransferStatus);
        }

        private void FromUKEnterGetRequirementDateVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sirtIsDataChangeBeforeStatusChange = false;
            var dlgVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (dlgVM == null)
            {
                var view = App.DashboardManager.GetActiveView() as TransfersVRListView;
                if (view != null && view.GetSelectedReqTransferID() > 0)
                {
                    using (Loading.StartNew())
                    {
                        dlgVM = new ReqTransferViewModel(view.GetSelectedReqTransferID(), ViewModelState.Edit);
                    }
                }
                else
                {
                    var trListVM = App.DashboardManager.GetActiveViewModel() as TransferVRViewModel;
                    if (trListVM != null && trListVM.SelectedTransferRequest != null)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                        }
                    }
                }
            }
            else
                sirtIsDataChangeBeforeStatusChange = dlgVM.IsDataChanged;

            var contract = dlgVM.Transfer.GetTransferList().GetContract();
            var legalEntity = contract.GetLegalEntity();

            var dlg = new FromUKEnterGetRequirementDateDlg();
            dlg.SetTitle(legalEntity.FormalizedName, contract.ContractNumber);
            dlg.Owner = this;
            dlg.DataContext = dlgVM;
            dlgVM.State = ViewModelState.Edit;
            ThemesTool.SetCurrentTheme(dlg);

            if (dlg.ShowDialog() == true)
            {
                DataContainerFacade.Save(dlgVM.Transfer);
                JournalLogger.LogChangeStateEvent(dlgVM, "Введена дата вручения требования");
                App.DashboardManager.RefreshListViewModels(new[]
                                                               {
                                                                   typeof (TransfersVRListViewModel),
                                                                   typeof (VRAssignPaymentsListViewModel),
                                                                   typeof (VRAssignPaymentsArchiveListViewModel)
                                                               });

                var vm = App.DashboardManager.FindViewModel<TransferVRViewModel>();
                vm?.RefreshTransfers();
                var regVM = App.DashboardManager.FindViewModel<SIVRRegisterVRViewModel>();
                regVM?.RefreshTransferLists();
            }
            dlgVM.IsDataChanged = sirtIsDataChangeBeforeStatusChange;
        }

        private void ShowNetWealthsVRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NetWealthsVRListView), ViewModelState.Read, "Чистые активы(СЧА) ВР");
        }

        private void ShowNetWealthsVRSumList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NetWealthsSumListView),
                ViewModelState.Read,
                new List<string> {"Чистые активы(СЧА) ВР совокупно", "Работа с ВР - Отчеты УК - Чистые активы(СЧА) совокупно"}, "Чистые активы(СЧА) ВР совокупно");
        }

        private void ShowActivesMarketVRCost_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(MarketCostVRListView),
                                            ViewModelState.Read, "Рыночная стоимость активов (РСА) ВР");
        }
        private void ShowActivesMarketVRCostScope_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(MarketCostScopeListView), ViewModelState.Read, "Рыночная стоимость активов ВР (РСА) совокупно");
        }

        private void ShowF40VRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F40VRListView), ViewModelState.Read, "Отчет по сделкам ВР");
        }

        private void ShowF50VRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F50VRListView), ViewModelState.Read, "Отчеты по ЦБ ВР");
        }

        private void ShowF60VRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F60VRListView), ViewModelState.Read, "Отчеты об инвестировании ВР");
        }

        private void ShowYieldsVRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(YieldsVRListView), ViewModelState.Read, "Доходность ВР");
        }

        private void ShowOwnFundsVRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OwnFundsListView), ViewModelState.Read, "Собственные средства ВР");
        }

        private void ShowF70VRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F70VRListView), ViewModelState.Read, "Отчеты о доходах ВР");
        }
        private void CreateYieldVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(YieldVRView), ViewModelState.Create, "Создать доходность УК ВР", "Доходность УК ВР");
        }

        private void ShowF80VRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F80VRListView), ViewModelState.Read, "Отчет о продаже покупке ЦБ ВР");
        }

        private void ShowViolationsVRList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ViolationsVRListView), ViewModelState.Read, "Нарушения ГУК ВР");
        }

        void ShowSuccessODKListVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(LoadedODKLogSuccessVRListView), ViewModelState.Read, "Журнал регистрации документов ЭДО ВР");
        }

        private void ShowInsuranceListVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<InsuranceVRListView, InsuranceVRListViewModel>("Договора страхования ВР", ViewModelState.Read);
        }

        private void ShowInsuranceArchListVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<InsuranceArchiveVRListView, InsuranceArchiveVRListViewModel>("Архив страховых договоров ВР", ViewModelState.Read);
        }

        private void CreateInsuranceVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(InsuranceVRView),
                                            ViewModelState.Create, (long)Document.Types.VR, "Добавить договор страхования ВР", "Договор страхования ВР");
        }

        private void ShowVRCorrespondenceList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<CorrespondenceVRListView, CorrespondenceVRListViewModel>("Корреспонденция", ViewModelState.Read);
        }

        private void ShowVRArchiveList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<CorrespondenceVRArchiveListView, CorrespondenceVRArchiveListViewModel>("Архив", ViewModelState.Read);
        }

        private void ShowVRControlList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<CorrespondenceVRControlListView, CorrespondenceVRControlListViewModel>("Контроль", ViewModelState.Read);
        }

        private void CreateVRDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<DocumentVRView, DocumentVRViewModel>(CorrespondenceListItem.s_Doc, ViewModelState.Create);
        }

        private void CreateVRLinkedDocument_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as CorrespondenceVRListView;
            if (view != null)
            {
                var row = view.Grid.CurrentItem as CorrespondenceListItemNew;
                if (row == null || row.IsAttach)
                    return;
                e.CanExecute = true;
            }
            else
            {
                var model = App.DashboardManager.GetActiveViewModel() as DocumentVRViewModel;
                if (model == null)
                    return;
                e.CanExecute = model.ID > 0;
            }
        }

        private void CreateVRLinkedDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as CorrespondenceVRListView;
            if (view != null)
            {
                var row = view.Grid.CurrentItem as CorrespondenceListItemNew;
                if (row == null || row.IsAttach)
                    return;
                App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentVRViewModel>(CorrespondenceListItem.s_AttachDoc, ViewModelState.Create, row.ID, ViewModelState.Create);
            }
            else
            {
                var model = App.DashboardManager.GetActiveViewModel() as DocumentVRViewModel;
                if (model == null)
                    return;
                App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentVRViewModel>(CorrespondenceListItem.s_AttachDoc, ViewModelState.Create, model.ID, ViewModelState.Create);
            }
        }

        private void ShowVRF402List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(VRF402ListView), ViewModelState.Read, "Список оплат");
        }

        private void ShowVRDemandList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(VRDemandListView), ViewModelState.Read, "Требования на оплату");
        }

        private void ShowVRF402PFRActions_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowF402PFRActions<VRF402ListView>();
        }

        private void ShowVRF402PFRActions_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.VR && CanShowF402PFRActions<VRF402ListView>();
            else
                e.CanExecute = CanShowF402PFRActions<VRF402ListView>();
        }

        private void CreateVROrderToPay_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.VR && CanCreateOrderToPay<VRF402ListView>();
            else
                e.CanExecute = CanCreateOrderToPay<VRF402ListView>();
        }

        private void VROrderSent_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.VR && CanOrderBeSent<VRF402ListView>();
            else
                e.CanExecute = CanOrderBeSent<VRF402ListView>();
        }

        private void VROrderDelivered_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.VR && CanOrderBeDelivered<VRF402ListView>();
            else
                e.CanExecute = CanOrderBeDelivered<VRF402ListView>();
        }

        private void VROrderPaid_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.VR && CanOrderBePaid<VRF402ListView>();
            else
                e.CanExecute = CanOrderBePaid<VRF402ListView>();
        }

        private void CreateVROrderToPay_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<VRF402ListView>(() => new CreateOrderToPayDlg(), id =>
            {
                var vm = new CreateOrderToPayViewModel(id);
                vm.PostOpen("Создание поручения на оплату");
                return vm;
            });
        }

        private void VROrderSent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<VRF402ListView>(() => new OrderSentDlg(), id =>
            {
                var vm = new OrderSentViewModel(id);
                vm.PostOpen("Поручение отправлено");
                return vm;
            });
        }

        private void VROrderDelivered_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<VRF402ListView>(() => new OrderDeliveredDlg(), id =>
            {
                var vm = new OrderDeliveredViewModel(id);
                vm.PostOpen("Поручение вручено");
                return vm;
            });
        }

        private void VROrderPaid_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<VRF402ListView>(() => new OrderPaidDlg(), id =>
            {
                var vm = new OrderPaidViewModel(id);
                vm.PostOpen("Поручение оплачено");
                return vm;
            });
        }

        private void ShowVRYearPlanWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(Views.TransferWizard.TransferVR8YearWizardView),
                                            ViewModelState.Read, 1, "Мастер годового плана ВР");
        }

        private void ShowVRYForYearPlanWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(Views.TransferWizard.TransferVR13YearWizardView),
                                            ViewModelState.Read, 1, "Мастер годового плана по операции \"Финансирование выплат\"");
        }

        private void ShowVRPlanCorrectionWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(Views.PlanCorrectionWizard.VRPlanCorrectionWizardView),
                                            ViewModelState.Read, 8, "Мастер корректировки плана ВР");
        }

        private void ShowVRMonthTransferCreationWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(Views.MonthTransferCreationWizard.VRMonthTransferCreationWizardView),
                                            ViewModelState.Read, 8, "Мастер создания перечислений на месяц ВР");
        }

        private void ShowVRAssignPaymentsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(VRAssignPaymentsListView),
                                            ViewModelState.Read, "Отзыв средств");
        }

        private void ShowVRAssignPaymentsArchiveList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(VRAssignPaymentsArchiveListView),
                                            ViewModelState.Read, "Архив отзыва средств");
        }

        private void CreateVRUKPaymentPlan_Executed(object sender, ExecutedRoutedEventArgs e)
        {            
            CreateUKPaymentPlan<VRAssignPaymentsListView>((int)Document.Types.VR);
        }

        private void CreateVRUKPaymentPlan_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CanCreateUKPaymentPlan<VRAssignPaymentsListView>();
        }

        private void PrintAPVRYearPlan_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new PrintAPYearPlanDlg();
            var vm = new VRPrintAPYearPlanDlgViewModel();
            if (!vm.HasYearsListByAllTypes())
                return;
            dlg.DataContext = vm;
            ThemesTool.SetCurrentTheme(dlg);
            vm.PostOpen(dlg.Title);
            if (dlg.ShowDialog() != true)
                return;
            try
            {
                vm.Print();
            }
            catch (Exception ex)
            {
                CheckMSOException(ex);
            }
        }

        private void PrintAPVRMonthPlan_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new PrintAPMonthPlanDlg();
            var vm = new VRPrintAPMonthPlanDlgViewModel();
            if (!vm.HasYearsListByAllOperations())
                return;
            dlg.DataContext = vm;
            ThemesTool.SetCurrentTheme(dlg);
            vm.PostOpen(dlg.Title);
            if (dlg.ShowDialog() != true)
                return;
            try
            {
                vm.Print();
            }
            catch (Exception ex)
            {
                CheckMSOException(ex);
            }
        }

        private void PrintVRYearPayment_Executed(object sender, ExecutedRoutedEventArgs e)
        {
			// Получаем наличие планов на год по каждому типу операции (пока их 2)
            List<Element> operationTypes;
            bool hasYears;
            using (Loading.StartNew())
            {
                operationTypes = WCFClient.Client.GetElementByType(Element.Types.RegisterOperationType);
                hasYears =
                    operationTypes.Select(operationType => WCFClient.Client.GetYearListCreateSIUKPlanByOperationType(Document.Types.VR, operationType.ID))
                        .Any(years => years.Any());
            }
            if (hasYears)
	        {
		        var dlg = new YearPlanPaymentDlg {Owner = this, Title = "Письмо о выплатах на год"};
                using (Loading.StartNew())
                {
                    var vm = new VRYearPlanPaymentDlgViewModel();
                    dlg.DataContext = vm;
                    vm.PostOpen(dlg.Title);
                }

                if (dlg.ShowDialog() != true)
                    return;
                using (Loading.StartNew())
                {
                    var dialogViewModel = dlg.DataContext as YearPlanPaymentDlgViewModel;
                    dialogViewModel?.Print(dlg.SelectedFolder);
                }
            }
	        else
				new DialogHelper().ShowError("\"Планы\" не созданы!");
        }

        private void ShowVRReports_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ReportsView), ViewModelState.Read, ReportFolderType.VR, "Отчеты");
        }

        private void ShowSPNReport01_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectDateAndBoolFlagViewModel(true, false);
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectDateAndBoolFlagDlg(), "СЧА-РСА-дата по ИП ГУК ВР",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN01(ukVm.DateValue.Value);
                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport02_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectPeriodAndBoolFlagViewModel(true, false);
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectPeriodAndBoolFlagDlg(), "Изменение СЧА - период",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN02(ukVm.DateValue.Value, ukVm.DateValue2.Value);
                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport03_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectPeriodAndBoolFlagViewModel(true, false);
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectPeriodAndBoolFlagDlg(), "СЧА среднее",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN03(ukVm.DateValue.Value, ukVm.DateValue2.Value);
                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport17_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectDateAndBoolFlagViewModel(true, false);
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectDateAndBoolFlagDlg(),
                    "Остаток средств ВР и средств пенсионных накоплений, предназначенных для финансирования СПВ (на дату)",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN17(ukVm.DateValue.Value);
                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport18_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectPeriodAndBoolFlagViewModel(true, flagLabel: "Отображать расторгнутые договоры");
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectPeriodAndBoolFlagDlg(),
                    "Движение средств ВР и средств пенсионных накоплений, предназначенных для финансирования СПВ за период",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN18(ukVm.DateValue.Value, ukVm.DateValue2.Value, ukVm.FlagValue);
                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport19_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectPeriodAndBoolFlagViewModel(true, false);
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectPeriodAndBoolFlagDlg(), "Динамика прироста",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN19(ukVm.DateValue.Value, ukVm.DateValue2.Value);
                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport20_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectYearMeasureViewModel();
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectYearMeasureView(), "Доходность совокупно годовая",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var items = new List<SPNReport20Item>();
                    var file = WCFClient.Client.CreateReportSPN20(ukVm.SelectedYear.Name, ukVm.SelectedMeasure.Value, out items);
                    foreach (var i in items)
                    {
                        Console.WriteLine(i);
                    }

                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport39_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectDateAndBoolFlagViewModel(true, false);
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectDateAndBoolFlagDlg(), "Совокупный инвестиционный портфель ГУК средствами ВР и СЧА",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN39(ukVm.DateValue.Value);
                    SaveReport(file);
                }
            }
        }

        private void ShowSPNReport40_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectDateAndBoolFlagViewModel(true, false);
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectDateAndBoolFlagDlg(), "Совокупный инвестиционный портфель ГУК средствами ВР",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportSPN40(ukVm.DateValue.Value);
                    SaveReport(file);
                }
            }
        }

        private void ShowVRInvDecl_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowInvDecl(new VRInvDeclDlgViewModel());
        }

        void ReportExportVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new ReportExportVRDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ReportExportVRDlgViewModel();
                dlg.DataContext = vm;
                vm.PostOpen(dlg.Title);
            }

            dlg.ShowDialog();
        }

        void ReportImportVR_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new ReportImportVRDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ReportImportVRDlgViewModel();
                dlg.DataContext = vm;
                vm.PostOpen(dlg.Title);
            }

            dlg.ShowDialog();
        }
        #endregion
    }
}
