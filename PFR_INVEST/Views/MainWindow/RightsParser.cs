﻿using System.Linq;
using System.Text;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Ribbon;
using DevExpress.XtraPrinting.Native;

namespace PFR_INVEST
{
    public static class RightsParser
    {

        internal static void Execute(RibbonDefaultPageCategory defCat, BarManager bm)
        {
            var sb = new StringBuilder();
            defCat.Pages.ForEach(page =>
            {
                sb.Append($"Вкладка: {page.Caption} --- ПРАВА: {ParseTag((string) page.Tag)}");
                sb.Append("\n");
                page.Groups.ForEach(group =>
                {
                    sb.Append($"    Группа: {group.Caption} --- ПРАВА: {ParseTag((string) page.Tag)}");
                    sb.Append("\n");
                    group.ItemLinks.ForEach(itemlink =>
                    {
                        if (itemlink is BarSubItemLink)
                        {
                            var item = (BarSubItem)bm.Items.FirstOrDefault(a => a.Name == ((BarSubItemLink)itemlink).BarItemName);
                            if (item != null)
                            {
                                sb.Append($"        Объект: {item.Content} --- ПРАВА: {ParseTag((string)item.Tag)}");
                                sb.Append("\n");
                                item.ItemLinks.ForEach(si =>
                                {
                                    var sitem = bm.Items.FirstOrDefault(a => a.Name == ((BarItemLink)si).BarItemName);
                                    if (sitem != null)
                                    {
                                        sb.Append($"            Объект: {sitem.Content} --- ПРАВА: {ParseTag((string)sitem.Tag)}");
                                        sb.Append("\n");
                                    }
                                });
                            }
                            else
                            {
                                sb.Append("        НЕ УДАЛОСЬ ОПРЕДЕЛИТЬ ОБЪЕКТ");
                                sb.Append("\n");
                            }

                        }

                        var link = itemlink as BarButtonItemLink;
                        if (link != null)
                        {
                            var item = (BarButtonItem)bm.Items.FirstOrDefault(a => a.Name == link.BarItemName);
                            sb.Append(item != null ? $"        Объект: {item.Content} --- ПРАВА: {ParseTag((string) item.Tag)}" : "         НЕ УДАЛОСЬ ОПРЕДЕЛИТЬ ОБЪЕКТ");
                            sb.Append("\n");
                        }
                    });
                });
                sb.Append("\n");
                sb.Append("\n");

            });

            var x = sb.ToString();

        }

        private static string ParseTag(string tag)
        {
            if (string.IsNullOrEmpty(tag)) return "";
            var sb = new StringBuilder();
            tag.Split('|').Where(a=> !string.IsNullOrEmpty(a)).ForEach(a =>
            {

                switch (a)
                {
                    case "PTK_DOKIP_USERS":
                        sb.Append("Пользователи ПТК ДОКИП");
                        break;
                    case "PTK_DOKIP_ADMINISTRATORS":
                        sb.Append("Администраторы ПТК ДОКИП");
                        break;

                    case "PTK_DOKIP_OSRP_WORKERS":
                        sb.Append("Обозреватели ОСРП, Сотрудники ОСРП");
                        break;
                    case "PTK_DOKIP_OSRP_DIRECTORY_EDITORS":
                        sb.Append("Редакторы справочников ОСРП");
                        break;
                    case "PTK_DOKIP_OSRP_MANAGERS":
                        sb.Append("Руководитель ОСРП");
                        break;

                    case "PTK_DOKIP_OKIP_WORKERS":
                        sb.Append("Обозреватели ОКИП, Сотрудники ОКИП");
                        break;
                    case "PTK_DOKIP_OKIP_DIRECTORY_EDITORS":
                        sb.Append("Редакторы справочников ОКИП");
                        break;
                    case "PTK_DOKIP_OKIP_MANAGERS":
                        sb.Append("Руководитель ОКИП");
                        break;

                    case "PTK_DOKIP_OFPR_WORKERS":
                        sb.Append("Обозреватели ОФПР, Сотрудники ОФПР");
                        break;
                    case "PTK_DOKIP_OFPR_DIRECTORY_EDITORS":
                        sb.Append("Редакторы справочников ОФПР");
                        break;
                    case "PTK_DOKIP_OFPR_MANAGERS":
                        sb.Append("Руководитель ОФПР");
                        break;

                    case "PTK_DOKIP_OARRS_WORKERS":
                        sb.Append("Обозреватели ОАРРС, Сотрудники ОАРРС");
                        break;
                    case "PTK_DOKIP_OARRS_DIRECTORY_EDITORS":
                        sb.Append("Редакторы справочников ОАРРС");
                        break;
                    case "PTK_DOKIP_OARRS_MANAGERS":
                        sb.Append("Руководитель ОАРРС");
                        break;

                    case "PTK_DOKIP_OVSI_WORKERS":
                        sb.Append("Обозреватели ОВСИ, Сотрудники ОВСИ");
                        break;
                    case "PTK_DOKIP_OVSI_DIRECTORY_EDITORS":
                        sb.Append("Редакторы справочников ОВСИ");
                        break;
                    case "PTK_DOKIP_OVSI_MANAGERS":
                        sb.Append("Руководитель ОВСИ");
                        break;

                    case "PTK_DOKIP_OUFV_WORKERS":
                        sb.Append("Обозреватели ОУиФВ, Сотрудники ОУиФВ");
                        break;
                    case "PTK_DOKIP_OUFV_DIRECTORY_EDITORS":
                        sb.Append("Редакторы справочников ОУиФВ");
                        break;
                    case "PTK_DOKIP_OUFV_MANAGERS":
                        sb.Append("Руководитель ОУиФВ");
                        break;

                    default:
                        return;
                }
                sb.Append(",");
            });
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
    }
}
