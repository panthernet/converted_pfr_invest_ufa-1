﻿using System;
using System.Linq;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Ribbon;
using PFR_INVEST.Tools;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.Views.Wizards.SpnDeposit;

namespace PFR_INVEST
{
	public partial class MainWindow
	{
	    private void BindCbAndDepositCommands()
	    {
            RibbonCommands.AddInfoToCBList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.AddInfoToCBList.Executed += AddInfoToCBListOnExecuted;
            RibbonCommands.InfoToCBList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.InfoToCBList.Executed += InfoToCBListOnExecuted;
            RibbonCommands.ExportInfoToCBList.CanExecute += ExportInfoToCBListOnCanExecute;
            RibbonCommands.ExportInfoToCBList.Executed += ExportInfoToCBListOnExecuted;
            RibbonCommands.ShowStocksList.Executed += ShowStocksListOnExecuted;
            RibbonCommands.ShowStocksList.CanExecute += CanExecuteIsTrue;
        }

	    private void ShowStocksListOnExecuted(object sender, ExecutedRoutedEventArgs e)
	    {
            App.DashboardManager.OpenNewTab(typeof(StockListView), ViewModelState.Read, "Биржи");
        }

        private static void ExportInfoToCBListOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
	    {
	        e.CanExecute = false;
	        var view = App.DashboardManager.GetActiveView() as InfoToCBListView;
	        if (view == null) return;
	        e.CanExecute = view.IsItemSelected;
	    }

	    private static void ExportInfoToCBListOnExecuted(object sender, ExecutedRoutedEventArgs e)
	    {
	        var view = App.DashboardManager.GetActiveViewModel() as InfoToCBListViewModel;
            ViewModelBase.DialogHelper.CBMonthlyReportExport(view?.SelectedItem?.ID ?? 0);
	    }

	    private static void InfoToCBListOnExecuted(object sender, ExecutedRoutedEventArgs e)
	    {
            App.DashboardManager.OpenNewTab(typeof(InfoToCBListView), ViewModelState.Read, "Информация в Банк России");
        }

	    private void AddInfoToCBListOnExecuted(object sender, ExecutedRoutedEventArgs e)
	    {
	        ViewModelBase.DialogHelper.CreateCBMonthlyReport();
	    }

	    private void ShowSpnDepositWizardOnExecuted(object sender, ExecutedRoutedEventArgs executedRoutedEventArgs)
        {
            App.DashboardManager.OpenNewTab(typeof(SpnDepositWizardView), ViewModelState.Create, "Мастер размещения СПН на депозиты");
        }

        private void CBReportWizardOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModelBase.DialogHelper.CreateCBReportWizard();

        }


        private bool ExportGroupDepClaim2Offer_CanExecute(object sender)
		{
			//var view = App.dashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;

			return (RibbonCommands.ExportSingleDepClaim2Offer as ICommand).CanExecute(null)
						   || (RibbonCommands.ExportAllDepClaim2Offer as ICommand).CanExecute(null)
						   || (RibbonCommands.ExportNotAcceptedDepClaimOffer as ICommand).CanExecute(null)
						   || (RibbonCommands.ExportContractsRegister as ICommand).CanExecute(null)
						   || (RibbonCommands.ExportDepClaimAllocation as ICommand).CanExecute(null);
		}

		#region ExportAllDepClaim2Offer

		private void ExportAllDepClaim2Offer_Executed(object sender, ICommand command)
		{
			if (!command.CanExecute(null))
				return;
			var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;
		    var dialog = new System.Windows.Forms.FolderBrowserDialog
		    {
		        Description = "Выберите папку для сохранения оферт",
		        SelectedPath = App.UserSettingsManager.UserSettings.Get<string>("Auction.Offer.ExportFolder")
		    };

		    var result = dialog.ShowDialog();
			if (result != System.Windows.Forms.DialogResult.OK)
				return;

            bool resultPrint = true;
            using (Loading.StartNew("Экспорт оферт..."))
            {
                var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(view.AuctionID.Value);
                var offers = auction.DepOfferList();
                foreach (var offer in offers)
                {
                    var model = new PrintDepClaimOffer(offer.ID) {Folder = dialog.SelectedPath, KeepOpen = false};
                    resultPrint = model.Print();
                    if (!resultPrint)
                        break;
                }
            }

            if (resultPrint) DXMessageBox.Show($"Все оферты сформированы и сохранены по пути '{dialog.SelectedPath}'");
			App.UserSettingsManager.UserSettings.Set("Auction.Offer.ExportFolder", dialog.SelectedPath, true);
		}

		private bool ExportAllDepClaim2Offer_CanExecute(object sender)
		{
			var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;
			// task 355.  аукцион находится в статусах №7, №8, №9 и №10
		    if (view == null) return false;
		    var s = view.AuctionStatus;
		    return s == DepClaimSelectParams.Statuses.OfferAcceptedOrNo || s == DepClaimSelectParams.Statuses.DepositSettled
		           || view.AuctionStatus == DepClaimSelectParams.Statuses.OfferCreated
		           || view.AuctionStatus == DepClaimSelectParams.Statuses.OfferSigned;
		}

		#endregion ExportAllDepClaim2Offer

		#region ExportDepClaimAllocation

		private static void ExportDepClaimAllocation_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null)) return;
		    var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;
		    var dlg = new ExportDepClaimAllocationDlg();
		    var dlgVM = new ExportDepClaimAllocationViewModel(view.AuctionID.Value);
		    if (!dlgVM.ValidateCanExport())
		        return;
		    dlg.DataContext = dlgVM;
		    dlg.Owner = App.mainWindow;
		    ThemesTool.SetCurrentTheme(dlg);
		    dlg.ShowDialog();
		}

		private static bool ExportDepClaimAllocation_CanExecute(object sender)
		{
			var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;
			var statuses = new DepClaimSelectParams.Statuses?[] 
			{ 
				DepClaimSelectParams.Statuses.OfferCreated,
				DepClaimSelectParams.Statuses.OfferSigned,
				DepClaimSelectParams.Statuses.OfferAcceptedOrNo,
				DepClaimSelectParams.Statuses.DepositSettled
			};
			return view?.AuctionID != null && statuses.Contains(view.AuctionStatus);
		}

		#endregion ExportDepClaimAllocation

		#region ExportSingleDepClaim2Offer

		private static void ExportSingleDepClaim2Offer_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null)) return;
		    var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;
		    var dialog = new System.Windows.Forms.FolderBrowserDialog
		    {
		        Description = "Выберите папку для сохранения оферты",
		        SelectedPath = App.UserSettingsManager.UserSettings.Get<string>("Auction.Offer.ExportFolder")
		    };
		    var result = dialog.ShowDialog();
		    if (result != System.Windows.Forms.DialogResult.OK) return;
            using (Loading.StartNew())
            {
                var model = new PrintDepClaimOffer(view.OfferID.Value) {Folder = dialog.SelectedPath, KeepOpen = true};
                model.Print();
            }

            App.UserSettingsManager.UserSettings.Set("Auction.Offer.ExportFolder", dialog.SelectedPath, true);
		}

		private static bool ExportSingleDepClaim2Offer_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;
			return vm?.OfferID != null;
		}

		#endregion ExportSingleDepClaim2Offer

		#region ExportNotAcceptedDepClaimOffer
		private void ExportNotAcceptedDepClaimOffer_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(true)) return;
		    var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProviderEx;
		    var dlg = new ExportNotAcceptedDepClaimOfferDlg() { Owner = this };
		    ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ExportNotAcceptedDepClaimOfferViewModel(view.Offer.ID);
                dlg.DataContext = vm;
            }

            dlg.ShowDialog();
		}

		private bool ExportNotAcceptedDepClaimOffer_CanExecute(object sender)
		{
			var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProviderEx;
			return view?.Offer != null && view.Offer.Status == DepClaimOffer.Statuses.NotAccepted;
		}

		#endregion ExportNotAcceptedDepClaimOffer

		#region ShowDepClaimMaxList

		private static void ShowDepClaimMaxList_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			App.DashboardManager.OpenNewTab(typeof(DepClaimMaxListView), ViewModelState.Read, "Сводные реестры заявок, с учетом максимальной суммы размещения");
		}

		private void ShowDepClaimMaxList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		#endregion ShowDepClaimMaxList

		#region ShowDepClaimList

		private void ShowDepClaimList_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			App.DashboardManager.OpenNewTab(typeof(DepClaimListView), ViewModelState.Read, "Сводный реестр заявок");
		}

		private void ShowDepClaimList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		#endregion

		#region ShowDepClaim2List

		private void ShowDepClaim2List_Executed(object sender, ICommand c)
		{
			App.DashboardManager.OpenNewTab(typeof(DepClaim2ListView), ViewModelState.Read, "Выписки из реестров заявок");
		}

		private bool ShowDepClaim2List_CanExecute(object sender)
		{
			return true;
		}

		#endregion ShowDepClaim2List

		#region BuildLimits

		void BuildLimits_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null)) return;
		    var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionSourceProvider;
		    if (!vm.AuctionID.HasValue) return;
		    var id = vm.AuctionID.Value;
		    ShowDialogForBankExport(false, vm.AuctionID.Value);
		}

		bool BuildLimits_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionSourceProvider;
			var vmS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
		    if (vm == null || vmS == null || vm.AuctionID == null || vm.AuctionStatus != DepClaimSelectParams.Statuses.New) return false;
		    if (vmS.IsMoscowStockSelected)
		        return true;
		    return vmS.IsSPVBStockSelected;
		}

		#endregion BuildLimits

		#region ExportDepClaimSelectParams

		void ExportDepClaimSelectParams_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null)) return;
		    var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionExportProvider;
		    var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(vm.AuctionID.Value);
		    if (auction == null || !auction.StockId.Equals((int)Stock.StockID.MoscowStock))
		    {
		        var m = new ExportAuctionInfoDlgViewModel(auction.ID);
		        var dlg = DialogHelper.PrepareDialog(new ExportAuctionInfoDlg(), m);

		        if (dlg.ShowDialog() != true) return;
		        var folder = m.Folder;
		        var id = vm.AuctionID.Value;
		        //Word
		        new PrintDepClaimSelectParams(id).Print(folder);
		        //Xml
		        SaveXML(m.Document, folder);
		    }
		    else
		        new PrintDepClaimSelectParams(vm.AuctionID.Value).Print();
		}


		bool ExportDepClaimSelectParams_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionExportProvider;
			var vmS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
			if (vm != null && vmS != null && vm.AuctionID != null && vmS.IsMoscowStockSelected)
				return true;
			return vm != null && vmS != null && vm.AuctionID != null && vmS.IsSPVBStockSelected;
		}
		#endregion ExportDepClaimSelectParams

		#region ExportCutoffRate

		void ExportCutoffRate_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionExportProvider;
		    var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(vm.AuctionID.Value);
		    if (auction != null && auction.StockId != (long) Stock.StockID.SPVB)
                return;
		    var model = new ExportAuctionCutoffRateDlgViewModel(auction.ID);
		    var dlg = DialogHelper.PrepareDialog(new ExportAuctionCutoffRateDlg(), model);
		    if (dlg.ShowDialog() == true)
		        SaveXML(model.Document, model.Folder);
		}


		bool ExportCutoffRate_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionExportProvider;
			var vmS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
			if (vm != null && vmS != null && vm.AuctionID != null && vmS.IsMoscowStockSelected)
				return false;
		    if (vm == null || vmS == null || vm.AuctionID == null || !vmS.IsSPVBStockSelected)
                return false;
		    var m = App.DashboardManager.GetActiveViewModel() as IDepClaim2ImportProvider;
		    if (m?.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimImported)
		        return true;
		    return false;
		}
		#endregion ExportCutoffRate

		#region ShowExportsDepclaimMax

		private void ShowExportsDepclaimMax_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var m = App.DashboardManager.GetActiveViewModel();
		    var auctionID = ((IDepClaimMaxExportProvider) m).AuctionID;
		    if (!auctionID.HasValue)
                return;
		    var dlg = new ExportDepClaimMaxListDlg();
		    var dlgVM = new ExportDepClaimMaxListViewModel(DepClaimMaxListItem.Types.Max, auctionID);
		    if (!dlgVM.ValidateCanExport())
		        return;
		    dlg.DataContext = dlgVM;
		    dlg.Owner = App.mainWindow;
		    ThemesTool.SetCurrentTheme(dlg);
		    dlgVM.PostOpen(dlg.Title);
		    dlg.ShowDialog();
		    dlgVM.State = ViewModelState.Export;
		}

		private bool ShowExportsDepclaimMax_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimMaxExportProvider;
			return vm != null && vm.IsMaxClaim && vm.AuctionID.HasValue && vm.DepClaimMaxCount > 0;
		}

		#endregion ShowExportsDepclaimMax

		#region ShowExportsDepclaim

		private void ShowExportsDepclaim_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var m = App.DashboardManager.GetActiveViewModel();
		    var auctionID = ((IDepClaimMaxExportProvider) m).AuctionID;
		    if (!auctionID.HasValue)
                return;
		    var dlg = new ExportDepClaimMaxListDlg() { Title = "Экспорт сводного реестра заявок, полученных от биржи" };
		    var dlgVM = new ExportDepClaimMaxListViewModel(DepClaimMaxListItem.Types.Common, auctionID);
		    if (!dlgVM.ValidateCanExport())
		        return;
		    dlg.DataContext = dlgVM;
		    dlg.Owner = App.mainWindow;
		    ThemesTool.SetCurrentTheme(dlg);
		    dlg.ShowDialog();
		}

		private bool ShowExportsDepclaim_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimMaxExportProvider;
			return vm != null && vm.IsCommonClaim && vm.AuctionID.HasValue && vm.DepClaimCommonCount > 0;
		}
		#endregion ShowExportsDepclaim

		#region ImportDepclaimMaxList

		private void ImportDepclaimMaxList_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var m = App.DashboardManager.GetActiveViewModel();
		    var auctionID = ((IDepClaimMaxImportProvider) m).AuctionID;
		    if (!auctionID.HasValue)
                return;
		    var dlg = new ImportDepClaimMaxDlg("Импорт сводного реестра заявок, с учетом максимальной суммы размещения") { Owner = this };
		    ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ImportDepClaimMaxViewModel(auctionID.Value, DepClaimMaxListItem.Types.Max);
                dlg.DataContext = vm;
                vm.PostOpen(dlg.Title);
                if (!vm.ValidateCanImport())
                    return;
            }

		    if (dlg.ShowDialog() != true)
                return;
		    var mL = App.DashboardManager.FindViewModelsList(typeof(DepClaimSelectParamsViewModel)).Where(item => item.ID == auctionID.Value);
		    foreach (var viewModelBase in mL)
		    {
		        var model = (DepClaimSelectParamsViewModel) viewModelBase;
		        model.RefreshAuction(auctionID.Value);
		    }
		}

		private bool ImportDepclaimMaxList_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimMaxImportProvider;
			return vm?.AuctionID != null && vm.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimImported;
		}
		#endregion ImportDepclaimMaxList

		#region ImportDepclaimList

		private void ImportDepclaimList_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var m = App.DashboardManager.GetActiveViewModel();
		    var auctionID = ((IDepClaimMaxImportProvider) m).AuctionID;
		    if (!auctionID.HasValue)
                return;
		    var dlg = new ImportDepClaimMaxDlg("Импорт сводного реестра заявок, полученных от биржи") { Owner = this };
		    ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ImportDepClaimMaxViewModel(auctionID.Value, DepClaimMaxListItem.Types.Common);
                vm.PostOpen(dlg.Title);
                dlg.DataContext = vm;
                if (!vm.ValidateCanImport())
                    return;
            }

            if (dlg.ShowDialog() != true)
                return;
		    var mL = App.DashboardManager.FindViewModelsList(typeof(DepClaimSelectParamsViewModel)).Where(item => item.ID == auctionID.Value);
		    foreach (var viewModelBase in mL)
		    {
		        var model = (DepClaimSelectParamsViewModel) viewModelBase;
		        model.RefreshAuction(auctionID.Value);
		    }
		}

		private bool ImportDepclaimList_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimMaxImportProvider;
			return vm?.AuctionID != null && vm.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimImported;
		}

		#endregion ImportDepclaimList

		#region ImportDepClaimBatchList

		private void ImportDepClaimBatchList_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var id = ((IDepClaimStockProvider) App.DashboardManager.GetActiveViewModel()).AuctionID;
		    if (!id.HasValue)
                return;
		    var dlg = new DepClaimBatchImportDlg("Импорт выписки из реестра заявок") { Owner = this };
		    ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ImportDepClaimBatchDlgViewModel(id.Value);
                dlg.DataContext = vm;

                if (!vm.ValidateCanImport())
                    return;
            }

            dlg.ShowDialog();
		}

		private bool ImportDepClaimBatchList_CanExecute(object sender)
		{
		    var m = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
		    return m?.AuctionID != null && (m.AuctionStatus == DepClaimSelectParams.Statuses.New && m.IsMoscowStockSelected);
		}

	    #endregion ImportDepClaim2List

		#region ImportDepClaim2List

		private void ImportDepClaim2List_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var mS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
		    var id = ((IDepClaim2ImportProvider) App.DashboardManager.GetActiveViewModel()).AuctionID;
		    if (!id.HasValue)
                return;
		    if (mS.IsMoscowStockSelected)
		    {
		        var dlg = new DepClaim2ImportDlg("Импорт выписки из реестра заявок") { Owner = this };
		        ThemesTool.SetCurrentTheme(dlg);
                using (Loading.StartNew())
                {
                    var vm = new ImportDepClaim2DlgViewModel(id.Value);
                    dlg.DataContext = vm;
                    if (!vm.ValidateCanImport())
                        return;
                }

                if (dlg.ShowDialog() == true) { }
		    }
		    else if (mS.IsSPVBStockSelected)
		    {
		        var dlg = new DepClaim2ImportDlg("Импорт сводного реестра заявок от СПВБ") { Owner = this };
		        ThemesTool.SetCurrentTheme(dlg);
                using (Loading.StartNew())
                {
                    var vm = new ImportDepClaim2_SPVBDlgViewModel(id.Value);
                    dlg.DataContext = vm;
                    if (!vm.ValidateCanImport())
                        return;
                }

                if (dlg.ShowDialog() == true) { }
		    }
		}

		private bool ImportDepClaim2List_CanExecute(object sender)
		{
			var m = App.DashboardManager.GetActiveViewModel() as IDepClaim2ImportProvider;
			var mS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
		    if (m?.AuctionID == null || mS == null)
                return false;
		    return (m.AuctionStatus == DepClaimSelectParams.Statuses.New ||
		            (m.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimImported && m.DepClaimCommonCount == 0 && m.DepClaimMaxCount == 0));
		}

		#endregion ImportDepClaim2List

		#region ExportDepClaim2List

		private void ExportDepClaim2List_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var m = App.DashboardManager.GetActiveViewModel();
		    var id = ((IDepClaim2ExportProvider) m).AuctionID;
		    if (!id.HasValue)
                return;
		    var dlg = new ExportDepClaim2Dlg() { Owner = this, Title = "Экспорт выписки из реестра заявок" };
		    ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ExportDepClaim2ViewModel(id.Value, ExportDepClaim2ViewModel.Modes.New);
                dlg.DataContext = vm;
                if (!vm.ValidateCanExport())
                    return;
                vm.PostOpen(dlg.Title);
            }

            dlg.ShowDialog();
		}

		private bool ExportDepClaim2List_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaim2ExportProvider;
			return vm?.AuctionStatus != null && vm.AuctionStatus != DepClaimSelectParams.Statuses.New;
		}

		#endregion ExportDepClaim2List

		#region ImportDepClaim2ConfirmList

		private void ImportDepClaim2ConfirmList_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var mS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
		    var id = ((IDepClaim2ConfirmImportProvider) App.DashboardManager.GetActiveViewModel()).AuctionID;
		    if (!id.HasValue)
                return;
		    if (mS.IsMoscowStockSelected)
            {
                DepClaim2ImportDlg dlg;
                using (Loading.StartNew())
                {
                    var vm = new ImportDepClaim2ConfirmDlgViewModel(id.Value);
                    dlg = DialogHelper.PrepareDialog(new DepClaim2ImportDlg("Импорт выписки из реестра заявок, подлежащих удовлетворению"), vm);
                    if (!vm.ValidateCanImport())
                        return;
                }

                if (dlg.ShowDialog() != true)
                    return;
		        var mL = App.DashboardManager.FindViewModelsList(typeof(DepClaimSelectParamsViewModel)).Where(item => item.ID == id.Value);
		        foreach (var viewModelBase in mL)
		        {
		            var model = (DepClaimSelectParamsViewModel) viewModelBase;
		            model.RefreshAuction(id.Value);
		        }
		    }
		    else if (mS.IsSPVBStockSelected)
		    {
                DepClaim2ImportDlg dlg;
                using (Loading.StartNew())
                {
                    var vm = new ImportDepClaim2Confirm_SPVBDlgViewModel(id.Value);
                    dlg = DialogHelper.PrepareDialog(new DepClaim2ImportDlg("Импорт документа с отобранными заявками"), vm);
                    if (!vm.ValidateCanImport())
                        return;
                }

                if (dlg.ShowDialog() != true)
                    return;
		        var mL = App.DashboardManager.FindViewModelsList(typeof(DepClaimSelectParamsViewModel)).Where(item => item.ID == id.Value);
		        foreach (var viewModelBase in mL)
		        {
		            var model = (DepClaimSelectParamsViewModel) viewModelBase;
		            model.RefreshAuction(id.Value);
		        }
		    }
		}

		private bool ImportDepClaim2ConfirmList_CanExecute(object sender)
		{
			var m = App.DashboardManager.GetActiveViewModel() as IDepClaim2ConfirmImportProvider;
			var mS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
		    if (m == null || mS == null)
                return false;
		    if (mS.IsMoscowStockSelected)
		        return (m.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimImported &&
		                m.DepClaimCommonCount > 0 &&
		                m.DepClaimMaxCount > 0)
		               || m.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimConfirmImported;
		    if (mS.IsSPVBStockSelected)
		        return m.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimImported 
		               || m.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimConfirmImported;
		    return false;
		}

		#endregion ImportDepClaim2ConfirmList

		#region ExportDepClaim2ConfirmList

		private void ExportDepClaim2ConfirmList_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var m = App.DashboardManager.GetActiveViewModel();
		    var id = ((IDepClaim2ConfirmExportProvider) m).AuctionID;
		    if (!id.HasValue)
                return;
		    var dlg = new ExportDepClaim2Dlg() { Owner = this, Title = "Экспорт выписки из реестра заявок, подлежащих удовлетворению" };
		    ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ExportDepClaim2ViewModel(id.Value, ExportDepClaim2ViewModel.Modes.Confirmed);
                dlg.DataContext = vm;
                if (!vm.ValidateCanExport())
                    return;
                vm.PostOpen(dlg.Title);
            }

            dlg.ShowDialog();
		}

		private bool ExportDepClaim2ConfirmList_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaim2ConfirmExportProvider;
			return vm?.AuctionID != null && vm.AuctionStatus != DepClaimSelectParams.Statuses.New && vm.AuctionStatus != DepClaimSelectParams.Statuses.DepClaimImported;
		}

		#endregion ExportDepClaim2ConfirmList

		#region ShowDepClaim2ConfirmList

		private void ShowDepClaim2ConfirmList_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			App.DashboardManager.OpenNewTab(typeof(DepClaim2ConfirmListView), ViewModelState.Read, "Выписки из реестров заявок, подлежащих удовлетворению");
		}

		private void ShowDepClaim2ConfirmList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		#endregion ShowDepClaim2ConfirmList

		#region CreateDepClaim2Offer

		bool CreateDepClaim2Offer_CanExecute(object sender)
		{
			var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferCreateProvider;
			return vm != null && (vm.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimConfirmImported || vm.CanGenerateOfferts);
		}

		void CreateDepClaim2Offer_Executed(object sender, ICommand command)
		{
		    if (!command.CanExecute(null))
                return;
		    var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferCreateProvider;
		    var model = new DepClaimOfferCreateViewModel(vm.AuctionID.Value);
		    model.CreateOffers();
		}

		#endregion CreateDepClaim2Offer

		#region ShowDepClaim2Offer

		void ShowDepClaim2Offer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		void ShowDepClaim2Offer_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			App.DashboardManager.OpenNewTab(typeof(DepClaim2OfferListView), ViewModelState.Read, "Список оферт");
		}
		#endregion ShowDepClaim2Offer
	}
}
