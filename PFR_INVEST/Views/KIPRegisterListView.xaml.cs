﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	public partial class KIPRegisterListView
	{
		public KIPRegisterListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }
	}
}
