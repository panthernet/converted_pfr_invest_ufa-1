﻿using System.Windows.Input;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using System.Windows;
    using System.Collections.Generic;

    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class NPFListView :  ISearchGrid
    {
        public NPFListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, npfListGrid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, npfListGrid);
        }

        public DevExpress.Xpf.Grid.GridControl GetGridControl()
        {
            return npfListGrid;
        }

        private void GridView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (npfListGrid.View.GetRowElementByMouseEventArgs(e) != null && npfListGrid.View.GetRowHandleByMouseEventArgs(e) == npfListGrid.View.FocusedRowHandle)
            {
                App.DashboardManager.OpenNewTab(typeof(NPFView), ViewModelState.Edit,
                        (long)npfListGrid.GetCellValue(npfListGrid.View.FocusedRowHandle, "LegalEntity.ID"), "Юридическое лицо");
            }
        }

        public NPFListItem GetSelectedDB2NPFListItem()
        {
            return npfListGrid.GetFocusedRow() as NPFListItem;
        }
               

        private void RunSearch() 
        {
            var grid = npfListGrid;
            var text = txtSearch.EditValue as string;
            //var cv = (IsTextMatchConverter)npfListGrid.Resources["IsTextMatchConverter"];
            

            if (string.IsNullOrWhiteSpace(text))
                return;

            if(!string.IsNullOrWhiteSpace(text))
                GridView.ExpandAllNodes();

            var focus = GridView.FocusedRowHandle;
            var founded = new List<int?>();

            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                int rowHandle = grid.GetRowHandleByVisibleIndex(i);
                if (!grid.IsGroupRowHandle(rowHandle))
                {
                    string name = (string)grid.GetCellValue(rowHandle, grid.Columns["LegalEntity.FormalizedNameFull"]);
                    if (name.ToUpper().Contains(text.ToUpper()))
                    {
                        founded.Add(rowHandle);                        
                    }
                }
            }
            if (founded.Any()) 
            {
                var toSelect = founded.FirstOrDefault(r => r > focus) ?? founded.First();
                GridView.FocusedRowHandle = toSelect ?? -1;
                GridView.ScrollIntoView(toSelect ?? -1);
            }
        } 

        private void cmdSearch_Click(object sender, RoutedEventArgs e)
        {
            RunSearch();            
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                RunSearch();   
            }
        }

        private void txtSearch_Clear_Click(object sender, RoutedEventArgs e)
        {
            txtSearch.EditValue = string.Empty;
            RunSearch(); 
        }

    }
}
