﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;
using PFR_INVEST.DataObjects.ListItems;
using System.Collections.Generic;
using System;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for F80ListView.xaml
    /// </summary>
    public partial class F80ListView
    {
        public F80ListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, f80Grid);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (f80Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

            if (f80Grid.View.FocusedRowHandle >= 0)
            {
                long identity = GetSelectedID();

                App.DashboardManager.OpenNewTab(typeof(F80View),
                                                ViewModelState.Edit,
                                                identity, "Отчёт о продаже покупке ЦБ");
            }
        }

        private void F80ListViewShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "reportOnDate") return;

            var source = (f80Grid.ItemsSource as List<F080DetailsListItem>);

            foreach (var o in e.ComboBoxEdit.ItemsSource as List<object>)
            {
                var item = o as CustomComboBoxItem;
                var editvalue = item?.EditValue as DateTime?;
                if (editvalue != null)
                {
                    var si = source.Find(x => x.reportOnDate.HasValue && x.reportOnDate.Value.Equals(editvalue.Value));
                    item.DisplayValue = si.ReportOnDate;
                }
            }
        }

        private void F80ListViewDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "reportOnDate")
            {
                var item = e.Row as F080DetailsListItem;
                if (item != null)
                {
                    e.DisplayText = item.ReportOnDate;
                }
            }
        }

        public long GetSelectedID()
        {
            if (f80Grid.View.FocusedRowHandle < 0) return 0;
            var item = f80Grid.CurrentItem as F080DetailsListItem;
            return item?.F080?.EdoID ?? 0;
        }

    }
}
