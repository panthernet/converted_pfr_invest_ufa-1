﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Editors.Helpers;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DepClaimSelectParamsListView.xaml
    /// </summary>
    public partial class DepClaimSelectParamsListView : UserControl
    {
        //TODO список подтормаживает из-за того, что модель выбирает данные при смене ActiveAuction, на основании которых определяется доступность операций над аукционом
        public DepClaimSelectParamsListView()
        {
            InitializeComponent();
            Grid.View.ShowFilterPopup += View_ShowFilterPopup;
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "DepClaimSelectParamsList");
        }

        private static void View_ShowFilterPopup(object sender, DevExpress.Xpf.Grid.FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "AuctionStep") return;
            var list = (e.ComboBoxEdit.ItemsSource as List<object>)?.ConvertAll(a => a as CustomComboBoxItem);
            list.Where(a => !a.DisplayValue.ToString().StartsWith("(")).ForEach(a => a.DisplayValue = DepClaimSelectParams.GetAuctionStepTip(int.Parse(a.DisplayValue.ToString())));
            e.ComboBoxEdit.ItemsSource = list;
        }

        public long GetSelectedRateID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }

        public bool IsRateSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (IsRateSelected())
            {
                long lID = GetSelectedRateID();
                if (lID > 0)
                {
                    App.DashboardManager.OpenNewTab(typeof(DepClaimSelectParamsView), ViewModelState.Edit, lID, "Уведомление о параметрах отбора заявок");
                }

            }
        }

        private void Icon_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Grid.SelectedItem == null) return;
            if(((ViewModelBase)DataContext).IsUnderViewerAccess) return;
            var id = (Grid.SelectedItem as DepClaimSelectParams).ID;
            ViewModelBase.DialogHelper.OpenSpnDepositWizard(id, (Grid.SelectedItem as DepClaimSelectParams).AuctionStatusID);
            e.Handled = true;
        }
    }
}
