﻿using System;
using System.Windows;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for CBPaymentDetailListView.xaml
    /// </summary>
    public partial class InfoToCBListView: IModelBindable
    {
        public Type ModelType => typeof(InfoToCBListViewModel);

        public bool IsItemSelected => Grid.View.FocusedRowHandle >= 0 && Grid.GetFocusedRow() is BOReportForm1;

        public InfoToCBListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "List");
           // ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
		    Grid.CustomColumnSort += Grid_CustomColumnSort;
            Grid.StartSorting +=GridOnStartSorting;
           // Grid.EndSorting
		}

        private void GridOnStartSorting(object sender, RoutedEventArgs e)
        {
        }

        private void On_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

            var item = Grid.GetFocusedRow() as BOReportForm1;
            if (item == null) return;
            //TODO open item ????

        }

        private static void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName.ToLower() == "year")
            {
                e.Result = e.SortOrder == ColumnSortOrder.Descending ? ((int)e.Value2).CompareTo((int)e.Value1) : ((int)e.Value1).CompareTo((int)e.Value2);
            }else 
                e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void View_OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }
    }
}
