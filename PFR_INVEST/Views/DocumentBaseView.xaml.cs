﻿using System;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views
{
    public abstract partial class DocumentBaseView : UserControl
    {
        private const string ATTACHMENT_UPLOAD_FAILED = "Вложение загрузить не удалось";
        private const string ATTACHMENT_SIZE_BIG = "Размер вложения должен быть меньше 4 мегабайт.\nВложение не сохранено.";
        private const string ATTACHMENT_DELETE_FAILED = "Вложение удалить не удалось";
        private const string ATTACHMENT_OPEN_FAILED = "Вложение открыть не удалось";

        protected DocumentBaseViewModel Model => DataContext as DocumentBaseViewModel;

        protected DocumentBaseView()
        {
            InitializeComponent();
            Loaded += SIDocumentView_Loaded;
            DataContextChanged += DocumentBaseView_DataContextChanged;
        }

        void DocumentBaseView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                if (Model.HasExecutorList) DockPanel.SetDock(personPicker, Dock.Top);
                if (Model.HasAddExecutionInfoList) DockPanel.SetDock(picker2, Dock.Top);
            }
        }

        void SIDocumentView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null)
            {
                Model.OnAttachmentDeleteFailed += Model_OnAttachmentDeleteFailed;
                Model.OnAttachmentOpenFailed += Model_OnAttachmentOpenFailed;
                Model.OnAttachmentSizeBig = Model_OnAttachmentSizeBig;
                Model.OnAttachmentUploadFailed += Model_OnAttachmentUploadFailed;
            }
        }

        protected void Model_OnAttachmentDeleteFailed(object sender, EventArgs e)
        {
            DXMessageBox.Show(ATTACHMENT_DELETE_FAILED, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        protected void Model_OnAttachmentOpenFailed(object sender, EventArgs e)
        {
            DXMessageBox.Show(ATTACHMENT_OPEN_FAILED, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        protected void Model_OnAttachmentSizeBig(object sender, EventArgs e)
        {
            DXMessageBox.Show(ATTACHMENT_SIZE_BIG, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        protected void Model_OnAttachmentUploadFailed(object sender, EventArgs e)
        {
            DXMessageBox.Show(ATTACHMENT_UPLOAD_FAILED, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        protected abstract void OpenLinkedItem(LinkedDocumentItem item);
        protected abstract void AddLinkedItem();

        private void grdLinkedDocs_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var doc = grdLinkedDocs.View.FocusedRow as LinkedDocumentItem;

            if (doc != null)
            {
                OpenLinkedItem(doc);
            }

        }

        private void btnAddLinkedDoc_Click(object sender, RoutedEventArgs e)
        {
            AddLinkedItem();

        }

        private void btnDeleteLinkedDoc_Click(object sender, RoutedEventArgs e)
        {
            LinkedDocumentItem item = null;
            if (grdLinkedDocs.IsValidRowHandle(grdLinkedDocs.View.FocusedRowHandle))
                item = grdLinkedDocs.GetFocusedRow() as LinkedDocumentItem;

            if (item != null)
            {
                if (DXMessageBox.Show("Удалить связанный документ?", "Удаление", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Model.DeleteLinkedDocument(item);
                }
            }
        }
    }
}
