﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for EnrollmentOtherView.xaml
    /// </summary>
    public partial class EnrollmentOtherView
    {
        public EnrollmentOtherView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private EnrollmentOtherViewModel Model => DataContext as EnrollmentOtherViewModel;

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            Model?.GetCourceManuallyOrFromCBRSite();
        }
    }
}
