﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for CostsRemunerationView.xaml
    /// </summary>
    public partial class CostsRemunerationView : UserControl
    {
        public CostsRemunerationView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
