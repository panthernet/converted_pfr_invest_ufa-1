﻿using System.Windows;
using PFR_INVEST.Helpers;
using PFR_INVEST.Views.Interface;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for MarketPriceView.xaml
    /// </summary>
    public partial class MarketPriceView : IAutoParentPanelSize
    {
        public MarketPriceView()
        {
            InitializeComponent();
        }

        #region IAutoParentPanelSize
        public double DesiredWidth { get { return DevExpressHelper.PWIDTH_AUTO; } }

        protected override Size MeasureOverride(Size constraint)
        {
            return DevExpressHelper.MeasureParentPanel(this, base.MeasureOverride, DesiredWidth);
        }
        #endregion
    }
}
