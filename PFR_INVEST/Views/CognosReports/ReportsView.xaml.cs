﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Navigation;
using PFR_INVEST.BusinessLogic.Tools;
using PFR_INVEST.Core.Properties;
using SHDocVw;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ReportsView.xaml
    /// </summary>
    public partial class ReportsView
    {
        private readonly ReportFolderType _mFolderType;
        private bool _mAuthorized;

        private Guid _webBrowserAppGuid = new Guid("0002DF05-0000-0000-C000-000000000046");
        private Guid _webBrowser2ClassGuid = typeof(IWebBrowser2).GUID;

        public ReportsView()
            : this(ReportFolderType.NPF)
        {
        }

        public ReportsView(ReportFolderType type)
        {
            InitializeComponent();
            _mFolderType = type;
            webBrowser.Navigated += webBrowser_Navigated;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var sett = new Settings(); sett.Reload();
            if (webBrowser.Document != null || WCFClient.ClientCredentials == null && !sett.CognosSSON)
                return;

            //var sett = new PFR_INVEST.Core.Properties.Settings(); sett.Reload();
            if (sett.CognosSSON)
            {
                var authHtml = CognosAuthorizationTool.GetCognosAuthHtml(WCFClient.ClientCredentials?.Windows.ClientCredential);
                webBrowser.NavigateToStream(new MemoryStream(new ASCIIEncoding().GetBytes(authHtml)));
            }
            else
            {
                webBrowser.Navigate(sett.CognosDictionaryUrl);
            }
        }

        private static void wbEvents2_NewWindow2(ref object ppDisp, ref bool cancel)
        {
            var wnd = new IEPopUpWindow
            {
                Visibility = Visibility.Hidden,
                Top = -10000,
                Left = -10000
            };
            wnd.Show();
            ppDisp = wnd.ActiveXWebBrowser.Application;
        }

        private bool _mHookInitialized;

        private void webBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            var sett = new Settings(); sett.Reload();
            if (webBrowser.Source == null || !sett.CognosSSON)
                return;

            if (!_mHookInitialized)
            {
                var serviceProvider = (IServiceProvider)webBrowser.Document;
                var myWebBrowser2 = (IWebBrowser2)serviceProvider.QueryService(ref _webBrowserAppGuid, ref _webBrowser2ClassGuid);
                myWebBrowser2.RegisterAsBrowser = true;
                var wbEvents2 = (DWebBrowserEvents2_Event)myWebBrowser2;
                wbEvents2.NewWindow2 += wbEvents2_NewWindow2;
                _mHookInitialized = true;
            }

            var sourceURL = webBrowser.Source.ToString();
            if (!_mAuthorized && sourceURL.Contains("gohome"))
            {
                var modifier = CognosAuthorizationTool.GetFolderModifier(_mFolderType);

                if (!string.IsNullOrEmpty(modifier))
                {
                    var endIndex = sourceURL.IndexOf("cc.xts&", StringComparison.Ordinal) + 7;
                    try
                    {
                        webBrowser.Source = new Uri(sourceURL.Substring(0, endIndex) + modifier);
                    }
                    catch
                    {
                        // ignored
                    }
                }
                _mAuthorized = true;
            }
        }
    }
}