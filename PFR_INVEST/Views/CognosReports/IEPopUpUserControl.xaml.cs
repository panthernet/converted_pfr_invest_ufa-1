﻿using System;
using System.Windows.Navigation;
using SHDocVw;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class IEPopUpUserControl : IDisposable
    {
        public delegate void CloseMeDelegate();
        public CloseMeDelegate CloseMe;
        private bool _mHookInitialized;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
        }

        ~IEPopUpUserControl()
        {
            Dispose(false);
        }
        public IEPopUpUserControl()
        {
            InitializeComponent();
            WebBrowser.Navigate(new Uri("about:blank"));
        }

        private void wbEvents2_NewWindow2(ref object ppDisp, ref bool cancel)
        {
            var wnd = new IEPopUpWindow
            {
                Visibility = System.Windows.Visibility.Hidden,
                Top = -10000,
                Left = -10000
            };
            wnd.Show();
            wnd.IEPopUpControl.WebBrowser.Navigate(new Uri("about:blank"));
            ppDisp = wnd.ActiveXWebBrowser.Application;
            CloseMe();
        }

        private void WebBrowserNavigated(object sender, NavigationEventArgs e)
        {
            if (!_mHookInitialized)
            {
                var myWebBrowser2 = (Parent as IEPopUpWindow)?.ActiveXWebBrowser;
                myWebBrowser2.RegisterAsBrowser = true;
                var wbEvents2 = (DWebBrowserEvents2_Event)myWebBrowser2;
                wbEvents2.NewWindow2 += wbEvents2_NewWindow2;
                wbEvents2.FileDownload += wbEvents2_FileDownload;
                wbEvents2.DownloadComplete += wbEvents2_DownloadComplete;
                _mHookInitialized = true;
            }
        }

        private bool _mFileDownloaded;

        private void wbEvents2_FileDownload(bool activeDocument, ref bool cancel)
        {
            _mFileDownloaded = true;
        }

        private void wbEvents2_DownloadComplete()
        {
            if (_mFileDownloaded)
                CloseMe();
        }
    }
}
