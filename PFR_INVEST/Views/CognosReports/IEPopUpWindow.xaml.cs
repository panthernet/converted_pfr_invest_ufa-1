﻿using System;
using SHDocVw;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class IEPopUpWindow
    {
        private Guid _webBrowserAppGuid = new Guid("0002DF05-0000-0000-C000-000000000046");
        private Guid _webBrowser2ClassGuid = typeof(IWebBrowser2).GUID;

        private bool IsMainWindow => ReferenceEquals(App.Current.MainWindow, this);

        public IWebBrowser2 ActiveXWebBrowser
        {
            get
            {
                var serviceProvider = (IServiceProvider)IEPopUpControl.WebBrowser.Document;
                var myWebBrowser2 = (IWebBrowser2)serviceProvider.QueryService(ref _webBrowserAppGuid, ref _webBrowser2ClassGuid);
                return myWebBrowser2;
            }
        }

        public IEPopUpWindow()
        {
            InitializeComponent();
            IEPopUpControl.CloseMe = CloseIfNotMainWindow;
            var sett = new Core.Properties.Settings(); sett.Reload();
            IEPopUpControl.WebBrowser.Navigate(new Uri(sett.CognosDictionaryUrl));
        }

        protected void CloseIfNotMainWindow()
        {
            if (!IsMainWindow)
                Close();
        }
    }
}
