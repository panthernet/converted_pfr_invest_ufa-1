﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for BalanceView.xaml
	/// </summary>
	public partial class CommonPPListView
	{
	    public CommonPPListViewModel Model => DataContext as CommonPPListViewModel;

        public CommonPPListView()
		{
			InitializeComponent();
			Loaded += ViewLoaded;
			ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "GridList");
        }

        private void ViewLoaded(object sender, RoutedEventArgs e)
		{
			if (Model == null)
				return;
            Grid.CustomColumnDisplayText += Grid_CustomColumnDisplayText;
		}

	    private void Grid_CustomColumnDisplayText(object sender, DevExpress.Xpf.Grid.CustomColumnDisplayTextEventArgs e)
        {
            if (!Grid.IsValidRowHandle(e.RowHandle))
                return;

            if (e.Column.FieldName == "Portfolio" && string.IsNullOrEmpty(e.DisplayText))
                e.DisplayText = " (Не задан)";
        }

        public DateTime? GetSelectedDate()
        {
            return Grid.IsGroupRowHandle(Grid.View.FocusedRowHandle) && Grid.GetFocusedGroupFieldName() == "Date" && ((PPListItem)Grid.GetRow(Grid.View.FocusedRowHandle)).PortfolioStatus == 2 ? (DateTime?)Grid.GetCellValue(Grid.View.FocusedRowHandle, "Date") : null;
        }

	    private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
			
			var item = Grid.CurrentItem as PPListItem;
		    if (item != null)
		    {
		        if (Model.IsOPFR)
		            DashboardManager.Instance.OpenNewTab(typeof(CommonPPOpfrView), ViewModelState.Edit, item.ID, "Платежное поручение ОПФР");
		        else DashboardManager.Instance.OpenNewTab(typeof(CommonPPView), ViewModelState.Edit, item.ID, Model.IsOPFR, "Платежное поручение НПФ/УК");
		    }
		}
	}
}
