﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ZLMovementsListView.xaml
    /// </summary>
    public partial class ZLMovementsListView
    {
        private bool _mGridRowMouseDoubleClicked;

        public ZLMovementsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (!IsTransferListSelected())
                return;
            _mGridRowMouseDoubleClicked = true;
            var lID = GetSelectedTransferListID();
            if (lID <= 0)
                return;
            if (DataContext is ZLMovementsSIListViewModel)
                App.DashboardManager.OpenNewTab(typeof(TransferSIView), ViewModelState.Edit, lID, "Список перечислений СИ");
            else
                App.DashboardManager.OpenNewTab(typeof(TransferVRView), ViewModelState.Edit, lID, "Список перечислений ВР");
        }

        public bool IsTransferListSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        public long GetSelectedTransferListID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }
    }
}
