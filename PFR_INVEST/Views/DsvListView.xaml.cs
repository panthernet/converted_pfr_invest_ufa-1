﻿using System.Windows.Input;
using DevExpress.XtraGrid;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Views.Interface;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DevExpress.Xpf.Grid;

    /// <summary>
    /// Interaction logic for DsvListView.xaml
    /// </summary>
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public partial class DsvListView : IListView<DueListItemClient>
    {

        //public bool isDSV { get; set; }
        public DsvListView()
        {
            InitializeComponent();
            Grid.Columns["Month"].SortMode = ColumnSortMode.Custom;
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "List");
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            string operation = GetSelectedOperation();
            long selectedID = GetSelectedID();
            string precision = GetSelectedPrecision();

            if (precision == "")
            {

                switch (operation)
                {
                    case DueDocKindIdentifier.DueDSV:
                        App.DashboardManager.OpenNewTab(typeof(DueView), ViewModelState.Edit,
                                                        selectedID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
                        break;
                    case DueDocKindIdentifier.Penalty:
                        App.DashboardManager.OpenNewTab(typeof(PenaltyView),
                                                        ViewModelState.Edit, selectedID, operation);
                        break;

                    case DueDocKindIdentifier.Prepayment:
                        App.DashboardManager.OpenNewTab(typeof(PrepaymentView),
                                                        ViewModelState.Edit, selectedID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
                        break;

                    case DueDocKindIdentifier.DueDeadDSV:
                        App.DashboardManager.OpenNewTab(typeof(DueDeadView),
                                                        ViewModelState.Edit, selectedID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
                        break;

                    case DueDocKindIdentifier.DueExcessDSV:
                        App.DashboardManager.OpenNewTab(typeof(DueExcessView),
                                                        ViewModelState.Edit, selectedID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
                        break;


                    case DueDocKindIdentifier.DueUndistributedDSV:
                        App.DashboardManager.OpenNewTab(typeof(DueUndistributedView),
                                                        ViewModelState.Edit, selectedID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
                        break;
                }
            }
            else
            {
                switch (precision)
                {
                    case DueDocKindIdentifier.PenaltyAccurate:
                        App.DashboardManager.OpenNewTab(typeof(PrecisePenaltyView), ViewModelState.Edit, selectedID, precision);
                        break;

                    case DueDocKindIdentifier.DueAccurate:
                        App.DashboardManager.OpenNewTab(typeof(MonthAccurateView), ViewModelState.Edit, selectedID, precision);
                        break;

                    case DueDocKindIdentifier.DueAccurateQuarter:
                    case DueDocKindIdentifier.DueDSVAccurateYear:
                    case DueDocKindIdentifier.PenaltyAccurateQuarter:
                        App.DashboardManager.OpenNewTab(typeof(QuarterAccurateView), ViewModelState.Edit, selectedID, (object) precision, precision);
                        break;

                    case DueDocKindIdentifier.PrepaymentAccurate:
                        App.DashboardManager.OpenNewTab(typeof(PrepaymentAccurate),
                                                        ViewModelState.Edit, selectedID, precision);
                        break;

                    case DueDocKindIdentifier.DueDeadAccurateDSV:
                        App.DashboardManager.OpenNewTab(typeof(DueDeadAccurateView), ViewModelState.Edit, selectedID, precision);
                        break;


                    case DueDocKindIdentifier.DueExcessAccurateDSV:
                        App.DashboardManager.OpenNewTab(typeof(DueExcessAccurateView), ViewModelState.Edit, selectedID, (object) precision, precision);
                        break;


                    case DueDocKindIdentifier.DueUndistributedAccurateDSV:
                        App.DashboardManager.OpenNewTab(typeof(DueUndistributedAccurateView),
                                                        ViewModelState.Edit, selectedID, precision);
                        break;

                    case DueDocKindIdentifier.Treasurity:
                        App.DashboardManager.OpenNewTab(typeof(TreasurityView), ViewModelState.Edit, selectedID, precision);
                        break;
                    case DueDocKindIdentifier.DueDSVAccurate:
						App.DashboardManager.OpenNewTab(typeof(MonthAccurateView), ViewModelState.Edit, selectedID, (object) precision, precision);
                        break;
                }
            }

        }

        public long GetSelectedID()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return (long)Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID");

            return 0;
        }

        public string GetSelectedPrecision()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return Grid.GetCellValue(Grid.View.FocusedRowHandle, "Precision").ToString();

            return string.Empty;
        }

        public string GetSelectedOperation()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return Grid.GetCellValue(Grid.View.FocusedRowHandle, "Operation").ToString();

            return string.Empty;
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
			//List<string> months = new List<string>(DateTools.Months);
			//int idx1 = months.IndexOf((e.Value1 ?? "").ToString().Trim());
			//int idx2 = months.IndexOf((e.Value2 ?? "").ToString().Trim());

			////Квартальные уточнения вконец
			//if (idx1 == -1) idx1 = 100;
			//if (idx2 == -1) idx2 = 100;

			//e.Result = Comparer<int>.Default.Compare(idx1, idx2);
			e.Result = ComponentHelper.OnColumnCustomSort(sender, e);

            e.Handled = true;
        }

        public DueListItemClient SelectedItem => Grid.CurrentItem  as DueListItemClient;

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}

        public bool IsDSVSelected()
        {
            if (Grid.SelectedItems.Count > 0 && SelectedItem != null)
                return string.IsNullOrEmpty(SelectedItem.Precision) && SelectedItem.Operation == DueDocKindIdentifier.DueDSV;
            return false;
        }

        public long? QuarterAddSPNID
        {
            get
            {
                var data = Grid.View.FocusedRowData.Row as DueListItemClient;

                if (data == null || (data.Operation != DueDocKindIdentifier.Due && data.Operation != DueDocKindIdentifier.DueDSV)) return null;
                if (data.SourceObject is AddSPN)
                    return data.ID;
                if (data.SourceObject is DopSPN)
                    return ((DopSPN) data.SourceObject).AddSpnID;
                return null;
            }
        }
    }
}
