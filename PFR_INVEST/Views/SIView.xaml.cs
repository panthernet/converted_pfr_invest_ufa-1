﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SIView.xaml
    /// </summary>
    public partial class SIView : UserControl
    {
        public SIView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public SIViewModel Model => DataContext as SIViewModel;

        private void ContractsListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (contractsListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (contractsListGrid.GetFocusedRow() != null)
            {
                App.DashboardManager.OpenNewTab(typeof(SIContractView), ViewModelState.Edit, (this.contractsListGrid.GetFocusedRow() as Contract).ID, "Договор СИ");
            }
        }

        private void OldSINamesListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (oldSINames.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (oldSINames.GetFocusedRow() != null)
            {
                ViewModelBase.DialogHelper.ViewOldSIName((this.oldSINames.GetFocusedRow() as OldSIName).ID);
            }
        }

        private void ContractsListGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Model.SelectedContract = contractsListGrid.GetFocusedRow() as Contract;
        }

        private void cbType_SelectedIndexChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            RefreshLicensiesTab();
        }

        private void RefreshLicensiesTab()
        {
            if (cbType.SelectedItem != null && (SIType)cbType.SelectedItem == SIType.СД)
            {
                dpSD.Visibility = System.Windows.Visibility.Visible;
                dpSD.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                dpUK.Visibility = System.Windows.Visibility.Collapsed;
                dpUK.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            }
            else
            {
                dpUK.Visibility = System.Windows.Visibility.Visible;
                dpUK.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                dpSD.Visibility = System.Windows.Visibility.Collapsed;
                dpSD.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            }
        }

        private void TabItem_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            RefreshLicensiesTab();
        }

        private void IdentifierGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (identifierGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            var identifier = identifierGrid.GetFocusedRow() as LegalEntityIdentifier;
            if (identifier != null && Model.CanExecuteEditIdentifier(identifier))
            {
                Model.EditIdentifier.Execute(identifier);
            }
        }

        private void BranchesListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (branchesListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (branchesListGrid.GetFocusedRow() != null)
            {
                App.DashboardManager.OpenNewTab(typeof(BranchView), ViewModelState.Edit, (this.branchesListGrid.GetFocusedRow() as Branch).ID, this.Model, "Филиал СИ");
            }
        }

        private void ContactsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (contactsGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (contactsGrid.GetFocusedRow() != null && Model.CanExecuteEditContact())
            {
                Model.EditContact.Execute(null);
            }
        }

    }
}
