﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Tools;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI.RopsAsvWizard;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.VR;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Views.LetterToNPFWizard;
using PFR_INVEST.Views.MonthTransferCreationWizard;
using PFR_INVEST.Views.PlanCorrectionWizard;
using PFR_INVEST.Views.PreferencesViews;
using PFR_INVEST.Views.TransferWizard;
using PFR_INVEST.DataObjects.ServiceItems;
using PFR_INVEST.Helpers;
using PFR_INVEST.Views.Analyze;
using PFR_INVEST.Views.Wizards.DepositCountWizard;
using PFR_INVEST.Views.Wizards.OpfrTransferWizard;
using PFR_INVEST.Views.Wizards.SpnDeposit;
using PFR_INVEST.Views.Wizards.TransferWizard.SIRopsAsvWizard;

namespace PFR_INVEST.Views
{
    public sealed partial class DashboardManager
    {
        private void PrepareModels(Type viewType, ViewModelState action, long recordId, object param, string caption, out UserControl newWindow, out ViewModelBase newViewModel)
        {
            newWindow = null;
            newViewModel = null;

            if (typeof(IModelBindable).IsAssignableFrom(viewType))
            {
                newWindow = (UserControl)Activator.CreateInstance(viewType);
                var prmed = newWindow as IModelBindableWithId;
                if (prmed != null)
                {
                    if (prmed.IsIdOnly)
                        newViewModel = (ViewModelBase) Activator.CreateInstance(prmed.ModelType, recordId);
                }
                if (newViewModel == null)
                {
                    var prmm = newWindow as IModelBindableWithParams;
                    if (prmm != null)
                    {
                        if (prmm.Params.Any())
                        {
                            var prms = prmm.Params;
                            for (var i = 0; i < prms.Length; i++)
                            {
                                if (ReferenceEquals(prms[i], "$id"))
                                    prms[i] = recordId;
                                if (ReferenceEquals(prms[i], "$state"))
                                    prms[i] = recordId > 0 ? ViewModelState.Edit : ViewModelState.Create;
                            }

                            newViewModel = (ViewModelBase) Activator.CreateInstance(prmm.ModelType, prms);
                        }
                    }
                    else newViewModel = (ViewModelBase) Activator.CreateInstance(((IModelBindable) newWindow).ModelType);
                }
                return;
            }

            #region Inbound data

            if (viewType == typeof(TransferSIWizardView))
            {
                newWindow = new TransferSIWizardView();
                newViewModel = new TransferSIWizardViewModel(recordId > 0) { ID = recordId };
                //Нужно, что бы не открывались дубли визардов
            }
            else if (viewType == typeof(OpfrListView))
            {
                newWindow = new OpfrListView();
                newViewModel = new OpfrListViewModel();
            }
            else if (viewType == typeof(OpfrRegisterView))
            {
                newWindow = new OpfrRegisterView();
                newViewModel = new OpfrRegisterViewModel(recordId > 0 ? (long?)recordId : null);
            }
            else if (viewType == typeof(OpfrTransferView))
            {
                newWindow = new OpfrTransferView();
                newViewModel = new OpfrTransferViewModel((long)param, 0, recordId > 0 ? (long?)recordId : null);
            }

            else if (viewType == typeof(TransferSI8YearWizardView))
            {
                newWindow = new TransferSI8YearWizardView();
                newViewModel = new TransferSI8YearWizardViewModel(recordId > 0) { ID = recordId };
                //Нужно, что бы не открывались дубли визардов
            }
            else if (viewType == typeof(TransferVRWizardView))
            {
                newWindow = new TransferVRWizardView();
                newViewModel = new TransferVRWizardViewModel(recordId > 0) { ID = recordId };
                //Нужно, что бы не открывались дубли визардов
            }
            else if (viewType == typeof(TransferVR13YearWizardView))
            {
                newWindow = new TransferVR13YearWizardView();
                newViewModel = new TransferVR13YearWizardViewModel(recordId > 0) { ID = recordId };
                //Нужно, что бы не открывались дубли визардов
            }
            else if (viewType == typeof(TransferVR8YearWizardView))
            {
                newWindow = new TransferVR8YearWizardView();
                newViewModel = new TransferVR8YearWizardViewModel(recordId > 0) { ID = recordId };
                //Нужно, что бы не открывались дубли визардов
            }

            else if (viewType == typeof(DeleteDocumentsListView))
            {
                newWindow = new DeleteDocumentsListView();
                newViewModel = new DeleteDocumentsListViewModel();
            }
            else if (viewType == typeof(OnesImportJournalListView))
            {
                newWindow = new OnesImportJournalListView();
                newViewModel = new OnesImportJournalListViewModel();
            }
            else if (viewType == typeof(OnesImportErrorsListView))
            {
                newWindow = new OnesImportErrorsListView();
                newViewModel = new OnesImportErrorsListViewModel();
            }
            else if (viewType == typeof(OnesExportJournalListView))
            {
                newWindow = new OnesExportJournalListView();
                newViewModel = new OnesExportJournalListViewModel();
            }
            else if (viewType == typeof(BankAgentsList))
            {
                newWindow = new BankAgentsList();
                newViewModel = new BankAgentsListViewModel();
            }
            else if (viewType == typeof(PortfolioStatusListViewModel))
            {
                newWindow = new PortfolioStatusListView();
                newViewModel = new PortfolioStatusListViewModel();
            }
            else if (viewType == typeof(BankAgentView))
            {
                newWindow = new BankAgentView();
                newViewModel = new BankAgentViewModel(recordId);
            }
            else if (viewType == typeof(SIPlanCorrectionWizardView))
            {
                newWindow = new SIPlanCorrectionWizardView();
                newViewModel = new SIPlanCorrectionWizardViewModel(recordId, 1);
            }
            else if (viewType == typeof(VRPlanCorrectionWizardView))
            {
                newWindow = new VRPlanCorrectionWizardView();
                newViewModel = new VRPlanCorrectionWizardViewModel(recordId, 3);
            }
            else if (viewType == typeof(SIMonthTransferCreationWizardView))
            {
                newWindow = new SIMonthTransferCreationWizardView();
                newViewModel = new SIMonthTransferCreationWizardModel(recordId);
            }
            else if (viewType == typeof(TransferSIRopsAsvWizardView))
            {
                newWindow = new TransferSIRopsAsvWizardView();
                newViewModel = new TransferSIRopsAsvWizardViewModel();
            }
            else if (viewType == typeof(VRMonthTransferCreationWizardView))
            {
                newWindow = new VRMonthTransferCreationWizardView();
                newViewModel = new VRMonthTransferCreationWizardModel(recordId);
            }
            else if (viewType == typeof(LetterToNPFWizardView))
            {
                newWindow = new LetterToNPFWizardView(Convert.ToBoolean(param));
                newViewModel = new LetterToNPFWizardViewModel(Convert.ToBoolean(param));
            }
            else if (viewType == typeof(RecalcUKPortfoliosView))
            {
                newWindow = new RecalcUKPortfoliosView();
                newViewModel = new RecalcUKPortfoliosViewModel();
            }
            else if (viewType == typeof(RecalcUKSIPortfoliosView))
            {
                newWindow = new RecalcUKSIPortfoliosView();
                newViewModel = new RecalcUKPortfoliosSIViewModel();
            }
            else if (viewType == typeof(RecalcUKVRPortfoliosView))
            {
                newWindow = new RecalcUKVRPortfoliosView();
                newViewModel = new RecalcUKPortfoliosVRViewModel();
            }

            else if (viewType == typeof(RecalcBuyOrSaleReportsNKDView))
            {
                newWindow = new RecalcBuyOrSaleReportsNKDView();
                newViewModel = new RecalcBuyOrSaleReportsNKDViewModel();
            }
            else if (viewType == typeof(MonthAccurateView))
            {
                newWindow = new MonthAccurateView();
                newViewModel = new MonthAccurateViewModel(action, recordId, param as string);
            }
            else if (viewType == typeof(QuarterAccurateView))
            {
                newWindow = new QuarterAccurateView();
                newViewModel = new QuarterAccurateViewModel(action, recordId, param as string);
            }
            else if (viewType == typeof(ServerSettingsView))
            {
                newWindow = new ServerSettingsView();
                newViewModel = new PreferencesViewModel();
            }
            else if (viewType == typeof(TemplatesListView))
            {
                newWindow = new TemplatesListView();
                newViewModel = new TemplatesListViewModel();
            }
            else if (viewType == typeof(ExchangeRateView))
            {
                newWindow = new ExchangeRateView();
                newViewModel = new ExchangeRateViewModel();
            }
            else if (viewType == typeof(ClientThemesView))
            {
                newWindow = new ClientThemesView();
                newViewModel = new ClientThemesViewModel();
            }
            else if (viewType == typeof(NPFView))
            {
                newWindow = new NPFView();
                newViewModel = new NPFViewModel(action, recordId);
            }
            else if (viewType == typeof(DKMonthListView))
            {
                newWindow = new DKMonthListView();
                newViewModel = new DKListViewModel(KDoc.Types.Month);
            }
            else if (viewType == typeof(DKYearListView))
            {
                newWindow = new DKYearListView();
                newViewModel = new DKListViewModel(KDoc.Types.Year);
            }
            else if (viewType == typeof(TreasurityView))
            {
                newWindow = new TreasurityView();
                newViewModel = new TreasurityViewModel(action, recordId, (Type)param);
            }
            else if (viewType == typeof(Treasurity2View))
            {
                newWindow = new Treasurity2View();
                newViewModel = new TreasurityViewModel(action, recordId, (Type)param);
            }
            else if (viewType == typeof(Treasurity3View))
            {
                newWindow = new Treasurity3View();
                newViewModel = new TreasurityViewModel(action, recordId, (Type)param);
            }
            else if (viewType == typeof(Treasurity4View))
            {
                newWindow = new Treasurity4View();
                newViewModel = new TreasurityViewModel(action, recordId, (Type)param);
            }

            else if (viewType == typeof(BankView))
            {
                newWindow = new BankView();
                newViewModel = new BankViewModel(recordId);
            }
            else if (viewType == typeof(OrderView))
            {
                newViewModel = new OrderViewModel(action, recordId);
                newWindow = new OrderView();
            }
            else if (viewType == typeof(OrderRelayingView))
            {
                newWindow = new OrderRelayingView();
                newViewModel = new OrderRelayingViewModel(action, recordId);
            }
            else if (viewType == typeof(BankAccountView))
            {
                newWindow = new BankAccountView();
                newViewModel = new BankAccountViewModel(recordId, action);
            }

            else if (viewType == typeof(NPFListView))
            {
                newWindow = new NPFListView();
                newViewModel = new NPFListViewModel();
            }
            else if (viewType == typeof(PFRBranchesListView))
            {
                newWindow = new PFRBranchesListView();
                newViewModel = new PFRBranchesListViewModel();
            }
            else if (viewType == typeof(RatesListView))
            {
                newWindow = new RatesListView();
                newViewModel = new RatesListViewModel();
            }
            else if (viewType == typeof(MissedRatesListView))
            {
                newWindow = new MissedRatesListView();
                newViewModel = new MissedRatesListViewModel();
            }
            else if (viewType == typeof(F20View))
            {
                newWindow = new F20View();
                newViewModel = new F20ViewModel(recordId);
            }
            else if (viewType == typeof(F22View))
            {
                newWindow = new F22View();
                newViewModel = new F22ViewModel(recordId);
            }
            else if (viewType == typeof(F24View))
            {
                newWindow = new F24View();
                newViewModel = new F24ViewModel(recordId);
            }
            else if (viewType == typeof(F25View))
            {
                newWindow = new F25View();
                newViewModel = new F25ViewModel(recordId);
            }

            else if (viewType == typeof(F26View))
            {
                newWindow = new F26View();
                newViewModel = new F26ViewModel(recordId);
            }
            else if (viewType == typeof(MarketCostListView))
            {
                newWindow = new MarketCostListView();
                newViewModel = new MarketCostListViewModel();
            }
            else if (viewType == typeof(MarketCostVRListView))
            {
                newWindow = new MarketCostVRListView();
                newViewModel = new MarketCostVRListViewModel();
            }
            else if (viewType == typeof(MarketCostSIListView))
            {
                newWindow = new MarketCostSIListView();
                newViewModel = new MarketCostSIListViewModel();
            }
            else if (viewType == typeof(MarketCostScopeListView))
            {
                newWindow = new MarketCostScopeListView();
                newViewModel = new MarketCostScopeListViewModel();
            }
            else if (viewType == typeof(F40ListView))
            {
                newWindow = new F40ListView();
                newViewModel = new F40ListViewModel();
            }
            else if (viewType == typeof(F40SIListView))
            {
                newWindow = new F40SIListView();
                newViewModel = new F40SIListViewModel();
            }
            else if (viewType == typeof(F40VRListView))
            {
                newWindow = new F40VRListView();
                newViewModel = new F40VRListViewModel();
            }
            else if (viewType == typeof(F50ListView))
            {
                newWindow = new F50ListView();
                newViewModel = new F50ListViewModel();
            }
            else if (viewType == typeof(F50VRListView))
            {
                newWindow = new F50VRListView();
                newViewModel = new F50VRListViewModel();
            }
            else if (viewType == typeof(F50SIListView))
            {
                newWindow = new F50SIListView();
                newViewModel = new F50SIListViewModel();
            }

            else if (viewType == typeof(OwnFundsListView))
            {
                newWindow = new OwnFundsListView();
                newViewModel = new OwnFundsListViewModel();
            }
            else if (viewType == typeof(SIDemandListView))
            {
                newWindow = new SIDemandListView();
                newViewModel = new F401402UKListViewModel(Document.Types.SI);
            }
            else if (viewType == typeof(VRDemandListView))
            {
                newWindow = new VRDemandListView();
                newViewModel = new F401402UKListViewModel(Document.Types.VR);
            }
            else if (viewType == typeof(DemandView))
            {
                newWindow = new DemandView();
                newViewModel = new F401402UKViewModel(recordId, true);
            }
            else if (viewType == typeof(SIF402ListView))
            {
                newWindow = new SIF402ListView();
                newViewModel = new SIF402ListViewModel();
            }
            else if (viewType == typeof(VRF402ListView))
            {
                newWindow = new VRF402ListView();
                newViewModel = new F401402UKListViewModel(Document.Types.VR);
            }
            else if (viewType == typeof(F402View))
            {
                newWindow = new F402View();
                newViewModel = new F401402UKViewModel(recordId, false);
            }
            else if (viewType == typeof(F80ListView))
            {
                newWindow = new F80ListView();
                newViewModel = new F80ListViewModel();
            }
            else if (viewType == typeof(F80SIListView))
            {
                newWindow = new F80SIListView();
                newViewModel = new F80SIListViewModel();
            }
            else if (viewType == typeof(F80VRListView))
            {
                newWindow = new F80VRListView();
                newViewModel = new F80VRListViewModel();
            }




            else if (viewType == typeof(F60ListView))
            {
                newWindow = new F60ListView();
                newViewModel = new F60ListViewModel();
            }
            else if (viewType == typeof(F60SIListView))
            {
                newWindow = new F60SIListView();
                newViewModel = new F60SIListViewModel();
            }
            else if (viewType == typeof(F60VRListView))
            {
                newWindow = new F60VRListView();
                newViewModel = new F60VRListViewModel();
            }

            else if (viewType == typeof(F70ListView))
            {
                newWindow = new F70ListView();
                newViewModel = new F70ListViewModel();
            }
            else if (viewType == typeof(F70SIListView))
            {
                newWindow = new F70SIListView();
                newViewModel = new F70SIListViewModel();
            }
            else if (viewType == typeof(F70VRListView))
            {
                newWindow = new F70VRListView();
                newViewModel = new F70VRListViewModel();
            }

            else if (viewType == typeof(YieldsListView))
            {
                newWindow = new YieldsListView();
                newViewModel = new YieldsListViewModel();
            }
            else if (viewType == typeof(YieldsSIListView))
            {
                newWindow = new YieldsSIListView();
                newViewModel = new YieldsSIListViewModel();
            }
            else if (viewType == typeof(YieldsVRListView))
            {
                newWindow = new YieldsVRListView();
                newViewModel = new YieldsVRListViewModel();
            }
            else if (viewType == typeof(PFRBranchView))
            {
                newWindow = new PFRBranchView();
                newViewModel = new PFRBranchViewModel(recordId, action);
            }
            else if (viewType == typeof(BanksList))
            {
                newWindow = new BanksList();
                newViewModel = new BanksListViewModel();
            }
            else if (viewType == typeof(BankAccountsList))
            {
                newWindow = new BankAccountsList();
                newViewModel = new BankAccountsListViewModel();
            }
            else if (viewType == typeof(RateView))
            {
                newWindow = new RateView();
                newViewModel = param == null ? new RateViewModel(recordId) : new RateViewModel((RatesListItem)param);
            }
            else if (viewType == typeof(F40View))
            {
                newWindow = new F40View();
                newViewModel = new F40ViewModel(recordId);
            }
            else if (viewType == typeof(F50View))
            {
                newWindow = new F50View();
                newViewModel = new F50ViewModel(recordId);
            }
            else if (viewType == typeof(ViolationView))
            {
                newWindow = new ViolationView();
                newViewModel = new ViolationViewModel(recordId);
            }
            else if (viewType == typeof(OwnFundsView))
            {
                newWindow = new OwnFundsView();
                newViewModel = new OwnFundsViewModel(recordId, action);
            }
            else if (viewType == typeof(F60View))
            {
                newWindow = new F60View();
                newViewModel = new F60ViewModel(recordId);
            }
            else if (viewType == typeof(F80View))
            {
                newWindow = new F80View();
                newViewModel = new F80ViewModel(recordId);
            }
            else if (viewType == typeof(F70View))
            {
                newWindow = new F70View();
                newViewModel = new F70ViewModel(recordId);
            }
            else if (viewType == typeof(YieldSIView))
            {
                newWindow = new YieldSIView();
                newViewModel = new YieldSIViewModel(action, recordId);
            }
            else if (viewType == typeof(YieldVRView))
            {
                newWindow = new YieldVRView();
                newViewModel = new YieldVRViewModel(action, recordId);
            }
            else if (viewType == typeof(ReportsView))
            {
                if (param is ReportFolderType)
                    newWindow = new ReportsView((ReportFolderType)param);
                else
                    newWindow = new ReportsView();
                newViewModel = new ReportsViewModel();
            }
            else if (viewType == typeof(CBReportManagerView))
            {
                bool resParam = param is bool && (bool)param;
                newWindow = new CBReportManagerView(resParam);
                newViewModel = new CBReportManagerViewModel(resParam);
            }
            else if (viewType == typeof(OpfrTransferWizardView))
            {
                newWindow = new OpfrTransferWizardView();
                newViewModel = new OpfrTransferWizardViewModel();
            }
            else if (viewType == typeof(DepositCountWizardView))
            {
                newWindow = new DepositCountWizardView();
                newViewModel = new DepositCountWizardViewModel();
            }
            else if (viewType == typeof(SpnDepositWizardView))
            {
                newWindow = new SpnDepositWizardView();
                newViewModel = new SpnDepositWizardViewModel(recordId);
            }


            else if (viewType == typeof(BOReportForm1View))
            {
                newWindow = new BOReportForm1View();
                if (recordId > 0)
                    newViewModel = new BOReportForm1ViewModel(recordId, param is bool && (bool)param);
                else if (param is int[])
                    newViewModel = new BOReportForm1ViewModel(((int[])param)[0], ((int[])param)[1]);
                else throw new Exception("Exception on BOReportForm1View gen call!");

            }
            else if (viewType == typeof(DeadZLListView))
            {
                newWindow = new DeadZLListView();
                newViewModel = new DeadZLListViewModel();
            }
            else if (viewType == typeof(DeadZLEditView))
            {
                newViewModel = new DeadZLEditViewModel();
                ((DeadZLEditViewModel)newViewModel).Load(recordId);
                newWindow = new DeadZLEditView();
            }
            else if (viewType == typeof(DeadZLAddView))
            {
                newViewModel = new DeadZLAddViewModel();
                newWindow = new DeadZLAddView();
            }
            else if (viewType == typeof(PFRAccountsListView))
            {
                newWindow = new PFRAccountsListView();
                newViewModel = new PFRAccountsListViewModel();
            }
            else if (viewType == typeof(PFRAccountView))
            {
                newWindow = new PFRAccountView();
                newViewModel = new PFRAccountViewModel(recordId);
            }
            else if (viewType == typeof(PortfolioView))
            {
                newWindow = new PortfolioView();
                newViewModel = new PortfolioViewModel(recordId, action);
            }
            else if (viewType == typeof(PortfoliosListView))
            {
                newWindow = new PortfoliosListView();
                newViewModel = new PortfoliosListViewModel(true);
            }
            else if (viewType == typeof(MarketPriceView))
            {
                newWindow = new MarketPriceView();
                if (recordId > 0)
                {
                    newViewModel = new MarketPriceViewModel();
                    ((MarketPriceViewModel)newViewModel).Load(recordId);
                }
                else
                {
                    var view = GetActiveView() as SecuritiesListView;
                    if (view != null)
                    {
                        long secID = view.GetSelectedSecurityID();
                        if (secID > 0)
                            newViewModel = new MarketPriceViewModel(secID);
                    }
                    else
                    {
                        var form = App.DashboardManager.GetActiveViewModel() as SecurityViewModel;
                        if (form != null && form.ID > 0)
                        {
                            newViewModel = new MarketPriceViewModel(form, 0);
                        }
                    }
                }
            }
            else if (viewType == typeof(MarketPricesListView))
            {
                newWindow = new MarketPricesListView();
                newViewModel = new MarketPricesListViewModel();
            }
            else if (viewType == typeof(GivenFinRegisterView))
            {
                newWindow = new GivenFinRegisterView();
                newViewModel = new GivenFinRegisterViewModel(recordId);
                //if (recordID > 0)
                //    ((GivenFinRegisterViewModel)newViewModel).Load(recordID);
            }
            else if (viewType == typeof(RegisterView))
            {
                newWindow = new RegisterView();
                newViewModel = new RegisterViewModel(recordId, action);
            }
            else if (viewType == typeof(RegistersListView))
            {
                newWindow = new RegistersListView();
                newViewModel = new RegistersListViewModel();
            }
            else if (viewType == typeof(RegistersArchiveListView))
            {
                newWindow = new RegistersArchiveListView();
                newViewModel = new RegistersArchiveListViewModel();
            }
            else if (viewType == typeof(SPNAllocationView))
            {
                newWindow = new SPNAllocationView();
                newViewModel = new SPNAllocationViewModel(recordId, action);
            }
            else if (viewType == typeof(AllocationNPFView))
            {
                newWindow = new AllocationNPFView();
                if (recordId > 0)
                    newViewModel = new AllocationNPFViewModel(recordId, action);
                else
                {
                    var model = App.DashboardManager.GetActiveViewModel() as SPNReturnViewModel;
                    if (model != null)
                    {
                        if (model.SelectedFR != null)
                        {
                            if (model.SelectedFR.ID == 0)
                                newViewModel = new AllocationNPFViewModel(model.SelectedFR.Finregister, model);
                        }
                        else
                        {
                            newViewModel = new AllocationNPFViewModel(model);
                        }
                        model.RegisterOpenedModel((AllocationNPFViewModel)newViewModel);
                    }
                }
            }
            else if (viewType == typeof(SPNReturnView))
            {
                newWindow = new SPNReturnView();
                newViewModel = new SPNReturnViewModel(recordId, action);
            }
            else if (viewType == typeof(NPFTempAllocationListView))
            {
                newWindow = new NPFTempAllocationListView();
                newViewModel = new NPFTempAllocationListViewModel();
            }
            else if (viewType == typeof(ZLRedistListView))
            {
                newWindow = new ZLRedistListView();
                newViewModel = new ZLRedistListViewModel();
            }
            else if (viewType == typeof(ZLRedistView))
            {
                newWindow = new ZLRedistView();
                newViewModel = new ZLRedistViewModel();
                if (recordId > 0)
                    ((ZLRedistViewModel)newViewModel).LoadZLRedist(recordId);
            }
            else if (viewType == typeof(TransferRequestView))
            {
                var prm = (object[])param;
                bool forOpfr = prm != null && (bool)prm[0];
                if (forOpfr)
                {
                    newWindow = new TransferRequestView(true);
                    newViewModel = new OpfrTransferRequestViewModel((long)prm[1]);
                }
                else
                {
                    newWindow = new TransferRequestView();
                    newViewModel = new PrintTransferRequestViewModel(recordId);
                }
            }
            else if (viewType == typeof(ERZLNotifyView))
            {
                newWindow = new ERZLNotifyView();
                newViewModel = new ERZLNotifyViewModel();
            }
            else if (viewType == typeof(SIContractView))
            {
                newWindow = new SIContractView();
                newViewModel = new SIContractViewModel(recordId, action);
            }
            else if (viewType == typeof(SIListView))
            {
                newWindow = new SIListView();
                newViewModel = new SIListViewModel(false);
            }
            else if (viewType == typeof(SIContactListView))
            {
                newWindow = new SIContactListView();
                newViewModel = new SIListViewModel(true);
            }
            else if (viewType == typeof(SIContractsListView))
            {
                newWindow = new SIContractsListView();
                newViewModel = new SIContractsListViewModel();
            }
            else if (viewType == typeof(VRContractsListView))
            {
                newWindow = new VRContractsListView();
                newViewModel = new VRContractsListViewModel();
            }
            else if (viewType == typeof(OrdersListView))
            {
                newWindow = new OrdersListView();
                newViewModel = new OrdersListViewModel();
            }
            else if (viewType == typeof(BuyReportsListView))
            {
                newWindow = new BuyReportsListView();
                newViewModel = new BuyReportsListViewModel();
            }
            else if (viewType == typeof(SaleReportsListView))
            {
                newWindow = new SaleReportsListView();
                newViewModel = new SaleReportsListViewModel();
            }
            else if (viewType == typeof(SIView))
            {
                newWindow = new SIView();
                newViewModel = new SIViewModel(recordId, action);
                /*if ((recordID > -1) && ((action == PFR_INVEST.VIEWMODEL_STATES.Edit) ||
					(action == PFR_INVEST.VIEWMODEL_STATES.Read)))
					((SIViewModel)newViewModel).LoadSI(recordID);*/
            }
            //else if (viewType == typeof(SIVRRegisterView))
            //{
            //    newWindow = new SIVRRegisterView();
            //    newViewModel = new SIVRRegisterViewModel(Document.Types.SI, recordID, action);
            //}

            else if (viewType == typeof(SIVRRegisterSIView))
            {
                newWindow = new SIVRRegisterSIView();
                newViewModel = new SIVRRegisterSIViewModel(recordId, action);
            }

            else if (viewType == typeof(SIVRRegisterVRView))
            {
                newWindow = new SIVRRegisterVRView();
                newViewModel = new SIVRRegisterVRViewModel(recordId, action);
            }

            else if (viewType == typeof(SupAgreementView))
            {
                newWindow = new SupAgreementView();
                newViewModel = new SupAgreementViewModel(recordId, action);
            }
            else if (viewType == typeof(PFRContactView))
            {
                newWindow = new PFRContactView();
                if (action == ViewModelState.Create)
                {
                    newViewModel = new PFRContactViewModel(0, action);
                    ((PFRContactViewModel)newViewModel).Load(recordId);
                }
                else newViewModel = new PFRContactViewModel(recordId, action);

            }
            else if (viewType == typeof(ContactView))
            {
                newWindow = new ContactView();
                newViewModel = new SIContactDlgViewModel(recordId, (long)param);
            }
            else if (viewType == typeof(OrderReportView))
            {
                newWindow = new OrderReportView();
                if (caption == OrderReportsHelper.BUYING_REPORT)
                    newViewModel = new BuyReportViewModel(recordId, action);
                else if (caption == OrderReportsHelper.SELLING_REPORT)
                    newViewModel = new SaleReportViewModel(recordId, action);
                else newViewModel = new RelayingReportViewModel(recordId, action);
            }
            else if (viewType == typeof(ReorganizationView))
            {
                newWindow = new ReorganizationView();
                newViewModel = new ReorganizationViewModel(recordId, (long)param, action);
            }
            else if (viewType == typeof(FastReportInputView))
            {
                newViewModel = new FastReportInputViewModel(recordId);
                newWindow = new FastReportInputView();
            }
            else if (viewType == typeof(FinRegisterView))
            {
                newWindow = new FinRegisterView();
                newViewModel = new FinRegisterViewModel(recordId, action);
            }
            else if (viewType == typeof(SecurityView))
            {
                newWindow = new SecurityView();
                newViewModel = new SecurityViewModel(recordId, action);
            }
            else if (viewType == typeof(SecuritiesListView))
            {
                newWindow = new SecuritiesListView();
                newViewModel = new SecuritiesListViewModel();
            }
            else if (viewType == typeof(PrecisePenaltyView))
            {
                newWindow = new PrecisePenaltyView();
                //var modelList = GetActiveViewModel() as DueListViewModel;
                var modelCard = GetActiveViewModel() as PenaltyViewModel;

                if (recordId > 0)
                {
                    newViewModel = new PrecisePenaltyViewModel(recordId);
                }
                else
                {
                    if (modelCard != null)
                    {
                        newViewModel = new PrecisePenaltyViewModel(modelCard);
                    }
                }
            }
            else if (viewType == typeof(PenaltyView))
            {
                newWindow = new PenaltyView();

                newViewModel = new PenaltyViewModel();

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((PenaltyViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(TransferView))
            {
                newWindow = new TransferView();
                newViewModel = new TransferViewModel(recordId, action);
            }

            else if (viewType == typeof(TransferSIView))
            {
                newWindow = new TransferSIView();
                newViewModel = new TransferSIViewModel(recordId, action);
            }

            else if (viewType == typeof(TransferVRView))
            {
                newWindow = new TransferVRView();
                newViewModel = new TransferVRViewModel(recordId, action);
            }
            else if (viewType == typeof(UKPaymentPlanView))
            {
                newWindow = new UKPaymentPlanView();
                newViewModel = new UKPaymentPlanViewModel(recordId, action);
            }
            ////Стало
            //else if (viewType == typeof(UKPaymentPlanView))
            //{
            //    newWindow = new UKPaymentPlanView();
            //    newViewModel = action == ViewModelState.Create ? new UKPaymentPlanViewModel(0, action) :
            //    new UKPaymentPlanViewModel(0, action);
            //}
            else if (viewType == typeof(BudgetView))
            {
                newWindow = new BudgetView();
                newViewModel = new BudgetViewModel();

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((BudgetViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(TransferSchilsView))
            {
                newWindow = new TransferSchilsView();
                newViewModel = new TransferSchilsViewModel();

                if (recordId > 0)
                {
                    if (action == ViewModelState.Edit || action == ViewModelState.Read)
                        ((TransferSchilsViewModel)newViewModel).Load(recordId);
                    else
                        ((TransferSchilsViewModel)newViewModel).LoadDirection(recordId);
                }
            }
            else if (viewType == typeof(ReturnView))
            {
                newWindow = new ReturnView();
                newViewModel = new ReturnViewModel();

                if (recordId > 0)
                {
                    if (action == ViewModelState.Edit || action == ViewModelState.Read)
                        ((ReturnViewModel)newViewModel).Load(recordId);
                    else
                        ((ReturnViewModel)newViewModel).LoadBudget(recordId);
                }
            }
            else if (viewType == typeof(DirectionView))
            {
                newWindow = new DirectionView();
                newViewModel = new DirectionViewModel();

                if (recordId > 0)
                {
                    if (action == ViewModelState.Edit || action == ViewModelState.Read)
                        ((DirectionViewModel)newViewModel).Load(recordId);
                    else
                        ((DirectionViewModel)newViewModel).LoadBudget(recordId);
                }
            }
            else if (viewType == typeof(BudgetCorrectionView))
            {
                newWindow = new BudgetCorrectionView();
                newViewModel = new BudgetCorrectionViewModel();

                if (recordId > 0)
                {
                    if (action == ViewModelState.Edit || action == ViewModelState.Read)
                        ((BudgetCorrectionViewModel)newViewModel).Load(recordId);
                    else
                        ((BudgetCorrectionViewModel)newViewModel).LoadBudget(recordId);
                }
            }
            else if (viewType == typeof(DueDeadView))
            {
                newWindow = new DueDeadView();
                newViewModel = new DueDeadViewModel((PortfolioIdentifier.PortfolioTypes)param);

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((DueDeadViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(DueView))
            {
                newWindow = new DueView();
                var prm = PortfolioIdentifier.PortfolioTypes.DSV;
                if (param is PortfolioIdentifier.PortfolioTypes)
                    prm = (PortfolioIdentifier.PortfolioTypes)param;

                newViewModel = new DueViewModel(recordId, prm);
            }
            else if (viewType == typeof(PrepaymentView))
            {
                newWindow = new PrepaymentView();
                newViewModel = new PrepaymentViewModel((PortfolioIdentifier.PortfolioTypes)param);

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((PrepaymentViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(PrepaymentAccurate))
            {
                newWindow = new PrepaymentAccurate();
                var viewModelBase = GetActiveViewModel();

                var model = (PrepaymentViewModel)param;
                if (recordId > 0)
                {
                    if (model == null)
                    {
                        var prepaymentAccurate = DataContainerFacade.GetByID<DopAps>(recordId);

                        model = new PrepaymentViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
                        if (prepaymentAccurate.ApsID.HasValue)
                            model.Load(prepaymentAccurate.ApsID.Value);
                    }

                    newViewModel = new PrepaymentAccurateViewModel(model);
                    ((PrepaymentAccurateViewModel)newViewModel).Load(recordId);

                }
                else
                {
                    newViewModel = new PrepaymentAccurateViewModel(model ?? (PrepaymentViewModel)viewModelBase);
                }
            }
            else if (viewType == typeof(DueDeadAccurateView))
            {
                newWindow = new DueDeadAccurateView();
                var viewModelBase = GetActiveViewModel();

                var model = (DueDeadViewModel)param;
                if (recordId > 0)
                {
                    if (model == null)
                    {
                        var prepaymentAccurate = DataContainerFacade.GetByID<DopAps>(recordId);

                        model = new DueDeadViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
                        if (prepaymentAccurate?.ApsID != null)
                            model.Load(prepaymentAccurate.ApsID.Value);
                    }
                    newViewModel = new DueDeadAccurateViewModel(model);
                    ((DueDeadAccurateViewModel)newViewModel).Load(recordId);

                }
                else
                {
                    newViewModel = new DueDeadAccurateViewModel(model ?? (DueDeadViewModel)viewModelBase);
                }
            }
            else if (viewType == typeof(DueExcessView))
            {
                newWindow = new DueExcessView();
                newViewModel = new DueExcessViewModel((PortfolioIdentifier.PortfolioTypes)param);

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((DueExcessViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(DueExcessAccurateView))
            {
                newWindow = new DueExcessAccurateView();
                var viewModelBase = GetActiveViewModel();

                var model = (DueExcessViewModel)param;
                if (recordId > 0)
                {
                    if (model == null)
                    {
                        var prepaymentAccurate = DataContainerFacade.GetByID<DopAps>(recordId);

                        model = new DueExcessViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
                        if (prepaymentAccurate?.ApsID != null)
                            model.Load(prepaymentAccurate.ApsID.Value);
                    }
                    newViewModel = new DueExcessAccurateViewModel(model);
                    ((DueExcessAccurateViewModel)newViewModel).Load(recordId);

                }
                else
                {
                    newViewModel = new DueExcessAccurateViewModel(model ?? (DueExcessViewModel)viewModelBase);
                }

            }
            else if (viewType == typeof(DueUndistributedView))
            {
                newWindow = new DueUndistributedView();
                newViewModel = new DueUndistributedViewModel((PortfolioIdentifier.PortfolioTypes)param);

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((DueUndistributedViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(DueUndistributedAccurateView))
            {
                newWindow = new DueUndistributedAccurateView();
                var viewModelBase = GetActiveViewModel();

                var model = (DueUndistributedViewModel)param;
                if (recordId > 0)
                {
                    if (model == null)
                    {

                        var prepaymentAccurate = DataContainerFacade.GetByID<DopAps>(recordId);

                        model = new DueUndistributedViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
                        if (prepaymentAccurate?.ApsID != null)
                            model.Load(prepaymentAccurate.ApsID.Value);
                    }

                    newViewModel = new DueUndistributedAccurateViewModel(model);
                    ((DueUndistributedAccurateViewModel)newViewModel).Load(recordId);

                }
                else
                {
                    newViewModel = new DueUndistributedAccurateViewModel(model ?? (DueUndistributedViewModel)viewModelBase);
                }
            }
            else if (viewType == typeof(EnrollmentPercents))
            {
                newWindow = new EnrollmentPercents();
                newViewModel = new EnrollmentPercentsViewModel();

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((EnrollmentPercentsViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(EnrollmentOtherView))
            {
                newWindow = new EnrollmentOtherView();
                newViewModel = new EnrollmentOtherViewModel();

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((EnrollmentOtherViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(WithdrawalView))
            {
                newWindow = new WithdrawalView();
                newViewModel = new WithdrawalViewModel();

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((WithdrawalViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(TransferToAccount))
            {
                newWindow = new TransferToAccount();
                newViewModel = new TransferToAccountViewModel();

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((TransferToAccountViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(CostsView))
            {
                newWindow = new CostsView();
                newViewModel = new CostsViewModel();

                if ((recordId > 0) && ((action == ViewModelState.Edit) ||
                    (action == ViewModelState.Read)))
                    ((CostsViewModel)newViewModel).Load(recordId);
            }
            else if (viewType == typeof(DueListView))
            {
                newWindow = new DueListView();
                newViewModel = new DueListViewModel(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV }, Exclude = true });
            }
            else if (viewType == typeof(DsvListView))
            {
                newWindow = new DsvListView();
                newViewModel = new DueListViewModel(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV } });
            }
            else if (viewType == typeof(AccountBalanceListView))
            {
                newWindow = new AccountBalanceListView();
                newViewModel = new AccountBalanceListViewModel();
            }
            //else if (viewType == typeof(TransfersListView))
            //{
            //    newWindow = new TransfersListView() { InitialSelectedRID = recordID };
            //    newViewModel = new TransfersListViewModel();
            //}
            else if (viewType == typeof(TransfersSIListView))
            {
                newWindow = new TransfersSIListView { InitialSelectedRid = recordId };
                newViewModel = new TransfersSIListViewModel();
            }
            else if (viewType == typeof(TransfersVRListView))
            {
                newWindow = new TransfersVRListView { InitialSelectedRid = recordId };
                newViewModel = new TransfersVRListViewModel();
            }


            else if (viewType == typeof(TransfersArchListView))
            {
                newWindow = new TransfersArchListView();
                newViewModel = new TransfersSIArchListViewModel();
            }

            else if (viewType == typeof(TransfersSIArchListView))
            {
                newWindow = new TransfersSIArchListView();
                newViewModel = new TransfersSIArchListViewModel();
            }
            else if (viewType == typeof(TransfersVRArchListView))
            {
                newWindow = new TransfersVRArchListView();
                newViewModel = new TransfersVRArchListViewModel();
            }


            else if (viewType == typeof(SchilsCostsListView))
            {
                newWindow = new SchilsCostsListView();
                newViewModel = new SchilsCostsListViewModel();
            }
            else if (viewType == typeof(ViolationCategoriesListView))
            {
                newWindow = new ViolationCategoriesListView();
                newViewModel = new ViolationCategoriesListViewModel();
            }
            else if (viewType == typeof(ViolationCategoryView))
            {
                newWindow = new ViolationCategoryView();
                newViewModel = new ViolationCategoryViewModel(recordId, action);
            }
            else if (viewType == typeof(CostsListView))
            {
                newWindow = new CostsListView();
                newViewModel = new CostsListViewModel();
            }
            else if (viewType == typeof(BalanceView))
            {
                newWindow = new BalanceView();
                newViewModel = new BalanceListViewModel(param);
            }
            else if (viewType == typeof(UKTransfersListView))
            {
                newWindow = new UKTransfersListView();
                newViewModel = new UKTransfersListViewModel(param);
            }
            else if (viewType == typeof(UKTransfersListArchiveView))
            {
                newWindow = new UKTransfersListArchiveView();
                newViewModel = new UKTransfersListArchiveViewModel();
            }

            else if (viewType == typeof(TransferToNPFListView))
            {
                newWindow = new TransferToNPFListView();
                newViewModel = new TransferFromOrToNPFListViewModel(true);
            }
            else if (viewType == typeof(TransferFromNPFListView))
            {
                newWindow = new TransferFromNPFListView();
                newViewModel = new TransferFromOrToNPFListViewModel(false);
            }
            else if (viewType == typeof(TransferFromOrToNPFArchiveListView))
            {
                newWindow = new TransferFromOrToNPFArchiveListView();
                newViewModel = new TransferFromOrToNPFArchiveListViewModel();
            }
            else if (viewType == typeof(TempAllocationListView))
            {
                newWindow = new TempAllocationListView();
                newViewModel = new TempAllocationListViewModel();
            }
            else if (viewType == typeof(IncomeSecurityListView))
            {
                newWindow = new IncomeSecurityListView();
                newViewModel = new IncomeSecurityListViewModel();
            }
            else if (viewType == typeof(IncomeSecurityView))
            {
                newWindow = new IncomeSecurityView();
                newViewModel = new IncomeSecurityViewModel(action, recordId);
            }
            //else if (viewType == typeof(InsuranceListView))
            //{
            //    newWindow = new InsuranceListView();
            //    newViewModel = new InsuranceListViewModel(false);
            //}
            //else if (viewType == typeof(InsuranceArchiveListView))
            //{
            //    newWindow = new InsuranceArchiveListView();
            //    newViewModel = new InsuranceListViewModel(true);
            //}
            else if (viewType == typeof(InsuranceVRView))
            {
                newWindow = new InsuranceVRView();
                newViewModel = new InsuranceVRViewModel(action, recordId);

            }
            else if (viewType == typeof(InsuranceSIView))
            {
                newWindow = new InsuranceSIView();
                newViewModel = new InsuranceSIViewModel(action, recordId);
            }
            else if (viewType == typeof(TDateListView))
            {
                newWindow = new TDateListView();
                newViewModel = new TDateListViewModel();
            }
            else if (viewType == typeof(BDateListView))
            {
                newWindow = new BDateListView();
                newViewModel = new BDateListViewModel();
            }
            else if (viewType == typeof(YieldOFZListView))
            {
                newWindow = new YieldOFZListView();
                newViewModel = new YieldOFZListViewModel();
            }
            else if (viewType == typeof(INDateListView))
            {
                newWindow = new INDateListView();
                newViewModel = new INDateListViewModel();
            }
            else if (viewType == typeof(ADUsersListView))
            {
                newWindow = new ADUsersListView();
                newViewModel = new ADUsersListViewModel();
            }
            else if (viewType == typeof(ADRolesListView))
            {
                newWindow = new ADRolesListView();
                newViewModel = new ADRolesListViewModel();
            }
            else if (viewType == typeof(UserStatisticChoose))
            {
                newWindow = new UserStatisticChoose();
                newViewModel = new UserStatisticChooseViewModel();
            }
            else if (viewType == typeof(PaymentView))
            {
                newWindow = new PaymentView();
                newViewModel = new PaymentViewModel(recordId, action);
            }
            else if (viewType == typeof(DepositView))
            {
                newWindow = new DepositView();
                newViewModel = new DepositViewModel(recordId, action);
            }
            else if (viewType == typeof(DepositsListView))
            {
                newWindow = new DepositsListView();
                newViewModel = new DepositsListViewModel(param);
            }
            else if (viewType == typeof(DepositsListByPortfolioView))
            {
                newWindow = new DepositsListByPortfolioView();
                newViewModel = new DepositsListByPortfolioViewModel();
            }

            else if (viewType == typeof(DepositsArchiveListView))
            {
                newWindow = new DepositsArchiveListView();
                newViewModel = new DepositsArchiveListViewModel(param);
            }
            else if (viewType == typeof(NetWealthsListView))
            {
                newWindow = new NetWealthsListView();
                newViewModel = new NetWealthsListViewModel();
            }
            else if (viewType == typeof(NetWealthsSIListView))
            {
                newWindow = new NetWealthsSIListView();
                newViewModel = new NetWealthsSIListViewModel();
            }
            else if (viewType == typeof(NetWealthsVRListView))
            {
                newWindow = new NetWealthsVRListView();
                newViewModel = new NetWealthsVRListViewModel();
            }

            else if (viewType == typeof(NetWealthsSumListView))
            {
                newWindow = new NetWealthsSumListView();
                newViewModel = new NetWealthsSumListViewModel(param);
            }
            else if (viewType == typeof(F10View))
            {
                newWindow = new F10View();
                newViewModel = new F10ViewModel(recordId);
            }
            else if (viewType == typeof(F12View))
            {
                newWindow = new F12View();
                newViewModel = new F12ViewModel(recordId);
            }
            else if (viewType == typeof(F14View))
            {
                newWindow = new F14View();
                newViewModel = new F14ViewModel(recordId);
            }
            else if (viewType == typeof(F15View))
            {
                newWindow = new F15View();
                newViewModel = new F15ViewModel(recordId);
            }
            else if (viewType == typeof(F16View))
            {
                newWindow = new F16View();
                newViewModel = new F16ViewModel(recordId);
            }
            else if (viewType == typeof(ZLMovementsListView))
            {
                newWindow = new ZLMovementsListView();
                newViewModel = new ZLMovementsListViewModel();
            }
            else if (viewType == typeof(ZLMovementsSIListView))
            {
                newWindow = new ZLMovementsSIListView();
                newViewModel = new ZLMovementsSIListViewModel();
            }

            else if (viewType == typeof(ZLMovementsVRListView))
            {
                newWindow = new ZLMovementsVRListView();
                newViewModel = new ZLMovementsVRListViewModel();
            }

            //else if (viewType == typeof(SPNMovementsListView))
            //{
            //    newWindow = new SPNMovementsListView();
            //    newViewModel = new SPNMovementsListViewModel();
            //}
            else if (viewType == typeof(SPNMovementsSIListView))
            {
                newWindow = new SPNMovementsSIListView();
                newViewModel = new SPNMovementsSIListViewModel();
            }
            else if (viewType == typeof(SPNMovementsVRListView))
            {
                newWindow = new SPNMovementsVRListView();
                newViewModel = new SPNMovementsVRListViewModel();
            }
            //else if (viewType == typeof(SPNMovementsUKListView))
            //{
            //    newWindow = new SPNMovementsUKListView();
            //    newViewModel = new SPNMovementsListViewModel();
            //}
            else if (viewType == typeof(SPNMovementsUKSIListView))
            {
                newWindow = new SPNMovementsUKSIListView();
                newViewModel = new SPNMovementsSIListViewModel();
            }
            else if (viewType == typeof(SPNMovementsUKVRListView))
            {
                newWindow = new SPNMovementsUKVRListView();
                newViewModel = new SPNMovementsVRListViewModel();
            }
            else if (viewType == typeof(ReqTransferView))
            {
                newWindow = new ReqTransferView();
                if (recordId > 0) //если Список перечислений уже сохранен в базе
                    newViewModel = new ReqTransferViewModel(recordId, action);
                else if (param != null) //если Список еще не сохранен в базе (смотри MainVindow.CreateOVSIReqTransfer_Executed)
                {
                    object[] arr = param as object[];
                    newViewModel = new ReqTransferInCacheViewModel(arr[0] as SITransfer, arr[1] as ReqTransfer, arr[2] as ITmpRepository<ReqTransfer>, action);
                }
            }
            else if (viewType == typeof(DocumentSIView))
            {
                newWindow = new DocumentSIView();
                newViewModel = new DocumentSIViewModel(recordId, action);
            }
            else if (viewType == typeof(DocumentNpfView))
            {
                newWindow = new DocumentNpfView();
                newViewModel = new DocumentNpfViewModel(recordId, action);
            }
            else if (viewType == typeof(WBISettingsView))
            {
                newWindow = new WBISettingsView();
                newViewModel = new PreferencesViewModel();
            }
            else if (viewType == typeof(DBFSettingsView))
            {
                newWindow = new DBFSettingsView();
                newViewModel = new PreferencesViewModel();
            }
          
            else if (viewType == typeof(ViolationsListView))
            {
                newWindow = new ViolationsListView();
                newViewModel = new ViolationsListViewModel();
            }
            else if (viewType == typeof(ViolationsSIListView))
            {
                newWindow = new ViolationsSIListView();
                newViewModel = new ViolationsSIListViewModel();
            }
            else if (viewType == typeof(ViolationsVRListView))
            {
                newWindow = new ViolationsVRListView();
                newViewModel = new ViolationsVRListViewModel();
            }
            else if (viewType == typeof(SIAssignPaymentsListView))
            {
                newWindow = new SIAssignPaymentsListView { InitialSelectedRID = recordId };
                newViewModel = new SIAssignPaymentsListViewModel();
            }
            else if (viewType == typeof(VRAssignPaymentsListView))
            {
                newWindow = new VRAssignPaymentsListView { InitialSelectedRID = recordId };
                newViewModel = new VRAssignPaymentsListViewModel();
                ((VRAssignPaymentsListViewModel)newViewModel).InitialSelectedRID = recordId;
            }
            else if (viewType == typeof(SIAssignPaymentsArchiveListView))
            {
                newWindow = new SIAssignPaymentsArchiveListView(recordId);
                newViewModel = new SIAssignPaymentsArchiveListViewModel();
            }
            else if (viewType == typeof(VRAssignPaymentsArchiveListView))
            {
                newWindow = new VRAssignPaymentsArchiveListView(recordId);
                newViewModel = new VRAssignPaymentsArchiveListViewModel();
            }
            else if (viewType == typeof(YearPlanView))
            {
                newWindow = new YearPlanView();
                newViewModel = new YearPlanViewModel(recordId, action);
            }
            else if (viewType == typeof(CorrespondenceListView))
            {
                newWindow = new CorrespondenceSIListView();
                newViewModel = new CorrespondenceSIListViewModel();
            }
            else if (viewType == typeof(CorrespondenceArchiveListView))
            {
                newWindow = new CorrespondenceSIArchiveListView();
                newViewModel = new CorrespondenceSIArchiveListViewModel();
            }
            else if (viewType == typeof(CorrespondenceControlListView))
            {
                newWindow = new CorrespondenceSIControlListView();
                newViewModel = new CorrespondenceSIControlListViewModel();
            }
            else if (viewType == typeof(YearPlanView))
            {
                newWindow = new YearPlanView();
                newViewModel = new YearPlanViewModel(recordId, action);
            }
            else if (viewType == typeof(LoadedODKLogSuccessListView))
            {
                newWindow = new LoadedODKLogSuccessListView();
                newViewModel = new LoadedODKLogListViewModel(true);
            }
            else if (viewType == typeof(LoadedODKLogSuccessSIListView))
            {
                newWindow = new LoadedODKLogSuccessSIListView();
                newViewModel = new LoadedODKLogListSIViewModel();
            }
            else if (viewType == typeof(LoadedODKLogSuccessVRListView))
            {
                newWindow = new LoadedODKLogSuccessVRListView();
                newViewModel = new LoadedODKLogListVRViewModel();
            }
            else if (viewType == typeof(LoadedODKLogErrorListView))
            {
                newWindow = new LoadedODKLogErrorListView();
                newViewModel = new LoadedODKLogListViewModel(false);
            }
            else if (viewType == typeof(LinkedDocumentView))
            {
                newWindow = new LinkedDocumentView();
                newViewModel = new LinkedDocumentSIViewModel(recordId, action);
            }
            else if (viewType == typeof(BranchView))
            {
                newWindow = new BranchView();
                newViewModel = new BranchViewModel(param as ViewModelCard, recordId);
            }
            else if (viewType == typeof(RatingView))
            {
                newWindow = new RatingView();
                newViewModel = new RatingViewModel(recordId, action);
            }
            else if (viewType == typeof(RatingsListView))
            {
                newWindow = new RatingsListView();
                newViewModel = new RatingsListViewModel();
            }
            else if (viewType == typeof(BoardView))
            {
                newWindow = new BoardView();
                newViewModel = new BoardViewModel(recordId, action);
            }
            else if (viewType == typeof(AgencyRatingView))
            {
                newWindow = new AgencyRatingView();
                newViewModel = new AgencyRatingViewModel(recordId, action);
            }
            else if (viewType == typeof(AgencyRatingsListView))
            {
                newWindow = new AgencyRatingsListView();
                newViewModel = new AgencyRatingsListViewModel();
            }
            else if (viewType == typeof(RatingAgenciesListView))
            {
                newWindow = new RatingAgenciesListView();
                newViewModel = new RatingAgenciesListViewModel();
            }

            else if (viewType == typeof(RatingAgencyView))
            {
                newWindow = new RatingAgencyView();
                newViewModel = new RatingAgencyViewModel(recordId);
            }
            else if (viewType == typeof(DepClaimSelectParamsListView))
            {
                newWindow = new DepClaimSelectParamsListView();
                newViewModel = new DepClaimSelectParamsListViewModel();
            }

            else if (viewType == typeof(DepClaimSelectParamsView))
            {
                newWindow = new DepClaimSelectParamsView();
                newViewModel = new DepClaimSelectParamsViewModel(recordId, action);
            }
            else if (viewType == typeof(DepClaimMaxListView))
            {
                newWindow = new DepClaimMaxListView();
                newViewModel = new DepClaimMaxListViewModel(DepClaimMaxListItem.Types.Max);
            }
            else if (viewType == typeof(DepClaimListView))
            {
                newWindow = new DepClaimListView();
                newViewModel = new DepClaimMaxListViewModel(DepClaimMaxListItem.Types.Common);
            }
            else if (viewType == typeof(DepClaim2ListView))
            {
                newWindow = new DepClaim2ListView();
                newViewModel = new DepClaim2ListViewModel();
            }
            else if (viewType == typeof(DepClaim2ConfirmListView))
            {
                newWindow = new DepClaim2ConfirmListView();
                newViewModel = new DepClaim2ConfirmListViewModel();
            }
            else if (viewType == typeof(DepClaim2OfferListView))
            {
                newWindow = new DepClaim2OfferListView();
                newViewModel = new DepClaim2OfferListViewModel();
            }
            else if (viewType == typeof(DepClaimOfferView))
            {
                newWindow = new DepClaimOfferView();
                newViewModel = new DepClaimOfferViewModel((long)recordId, action);
            }
            else if (viewType == typeof(DivisionListView))
            {
                newWindow = new DivisionListView();
                newViewModel = new DivisionListViewModel();
            }
            else if (viewType == typeof(DivisionView))
            {
                newWindow = new DivisionView();
                newViewModel = new DivisionViewModel(recordId, action);
            }
            else if (viewType == typeof(PensionNotificationListView))
            {
                newWindow = new PensionNotificationListView();
                newViewModel = new PensionNotificationListViewModel();
            }
            else if (viewType == typeof(PensionNotificationView))
            {
                newWindow = new PensionNotificationView();
                newViewModel = new PensionNotificationViewModel(recordId, action);
            }

            else if (viewType == typeof(PersonView))
            {
                newWindow = new PersonView();

                if (action == ViewModelState.Create)
                {
                    newViewModel = new PersonViewModel(0, action);
                    ((PersonViewModel)newViewModel).Load(recordId);
                }
                else newViewModel = new PersonViewModel(recordId, action);
            }
            else if (viewType == typeof(BankConclusionList))
            {
                newWindow = new BankConclusionList();
                newViewModel = new BankConclusionListViewModel();
            }
            else if (viewType == typeof(BankConclusionView))
            {
                newWindow = new BankConclusionView();
                newViewModel = new BankConclusionViewModel(recordId);
            }
            else if (viewType == typeof(NPFChiefView))
            {
                newWindow = new NPFChiefView();
                newViewModel = new NPFChiefViewModel(action, (NPFViewModel.LegalEntityChiefParamContainer)param);
            }
            else if (viewType == typeof(DepClaim2OfferAcceptedList))
            {
                newWindow = new DepClaim2OfferAcceptedList();
                newViewModel = new DepClaim2OfferAcceptedListViewModel((IEnumerable<DepClaimOffer>)param);
            }
            else if (viewType == typeof(DepositReturnListView))
            {
                newWindow = new DepositReturnListView();
                newViewModel = new DepositReturnListViewModel((IList<DepositReturnListItem>)param);
            }
            else if (viewType == typeof(PaymentHistoryView))
            {
                newWindow = new PaymentHistoryView();
                newViewModel = new PaymentHistoryViewModel(action, recordId);
            }
            else if (viewType == typeof(KBKListView))
            {
                newWindow = new KBKListView();
                newViewModel = new KBKListViewModel();
            }
            else if (viewType == typeof(KBKView))
            {
                newWindow = new KBKView();
                newViewModel = new KBKViewModel(recordId, action);
            }
            else if (viewType == typeof(CommonPPListView))
            {
                newWindow = new CommonPPListView();
                newViewModel = new CommonPPListViewModel((bool?) param ?? false, null);
            }
            else if (viewType == typeof(RejAppListView))
            {
                newWindow = new RejAppListView();
                newViewModel = new RejAppListViewModel();
            }
            else if (viewType == typeof(PaymentDetailListView))
            {
                newWindow = new PaymentDetailListView();
                newViewModel = new PaymentDetailListViewModel();
            }
            else if (viewType == typeof(PaymentDetailView))
            {
                newWindow = new PaymentDetailView();
                newViewModel = new PaymentDetailViewModel(recordId, action);
            }
            else if (viewType == typeof(CommonPPView))
            {
                newWindow = new CommonPPView();
                newViewModel = new CommonPPViewModel(recordId, action, (bool?) param ?? false);
            }
            else if (viewType == typeof(CommonPPSplitView))
            {
                newWindow = new CommonPPSplitView();
                newViewModel = new CommonPPSplitViewModel((DateTime)param);
            }
            else if (viewType == typeof(NPFCodeView))
            {
                newWindow = new NPFCodeView();
                newViewModel = new NPFCodeViewModel(recordId, action, (bool)param);
            }
            else if (viewType == typeof(NPFCodeListView))
            {
                newWindow = new NPFCodeListView();
                newViewModel = new NPFCodeListViewModel((bool)param);
            }
            else if (viewType == typeof(KIPErrorLogListView))
            {
                newWindow = new KIPErrorLogListView();
                newViewModel = new KIPErrorLogListViewModel();
            }
            else if (viewType == typeof(KIPRegisterListView))
            {
                newWindow = new KIPRegisterListView();
                newViewModel = new KIPRegisterListViewModel();
            }
            else if (viewType == typeof(ROPSSumListView))
            {
                newWindow = new ROPSSumListView();
                newViewModel = new ROPSSumListViewModel();
            }
            else if (viewType == typeof(ROPSSumView))
            {
                newWindow = new ROPSSumView();
                newViewModel = new ROPSSumViewModel(action, recordId);
            }
            else if (viewType == typeof(PaymentOrderListView))
            {
                newWindow = new PaymentOrderListView();
                newViewModel = new PaymentOrderListViewModel();
            }
            else if (viewType == typeof(PfrBranchReportInsuredPersonListView))
            {
                newWindow = new PfrBranchReportInsuredPersonListView();
                newViewModel = new PfrBranchReportInsuredPersonListViewModel();
            }
            else if (viewType == typeof(PfrBranchReportNpfNoticeListView))
            {
                newWindow = new PfrBranchReportNpfNoticeListView();
                newViewModel = new PfrBranchReportNpfNoticeListViewModel();
            }
            else if (viewType == typeof(PfrBranchReportAssigneePaymentListView))
            {
                newWindow = new PfrBranchReportAssigneePaymentListView();
                newViewModel = new PfrBranchReportAssigneePaymentListViewModel();
            }
            else if (viewType == typeof(PfrBranchReportCashExpendituresListView))
            {
                newWindow = new PfrBranchReportCashExpendituresListView();
                newViewModel = new PfrBranchReportCashExpendituresListViewModel();
            }
            else if (viewType == typeof(PfrBranchReportApplication741ListView))
            {
                newWindow = new PfrBranchReportApplication741ListView();
                newViewModel = new PfrBranchReportApplication741ListViewModel();
            }
            else if (viewType == typeof(PfrBranchReportDeliveryCostListView))
            {
                newWindow = new PfrBranchReportDeliveryCostListView();
                newViewModel = new PfrBranchReportDeliveryCostListViewModel();
            }
            else if (viewType == typeof(CBPaymentDetailListView))
            {
                newWindow = new CBPaymentDetailListView();
                newViewModel = new CBPaymentDetailListViewModel();
            }
            else if (viewType == typeof(AnalyzePensionplacementPivotView))
            {
                newWindow = new AnalyzePensionplacementPivotView();
                newViewModel = new AnalyzePensionplacementViewModel();
            }
            else if (viewType == typeof(AnalyzeIncomingstatementsPivotView))
            {
                newWindow = new AnalyzeIncomingstatementsPivotView();
                newViewModel = new AnalyzeIncomingstatementsPivotViewModel();
            }


            else if (viewType == typeof(AnalyzePensionPlacementReportListView))
            {
                newWindow = new AnalyzePensionPlacementReportListView();
                newViewModel = new AnalyzePensionPlacementReportListViewModel();
            }
            else if (viewType == typeof(AnalyzeIncomingstatementsListView))
            {
                newWindow = new AnalyzeIncomingstatementsListView();
                newViewModel = new AnalyzeIncomingstatementsListViewModel();
            }
            else if (viewType == typeof(AnalyzeInsuredpersonListView))
            {
                newWindow = new AnalyzeInsuredpersonListView();
                newViewModel = new AnalyzeInsuredpersonListViewModel();
            }
            else if (viewType == typeof(AnalyzePaymentyearrateListView))
            {
                newWindow = new AnalyzePaymentyearrateListView();
                newViewModel = new AnalyzePaymentyearrateListViewModel();
            }
            #endregion
        }
    }
}
