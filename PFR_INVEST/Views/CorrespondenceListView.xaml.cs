﻿using System.Windows.Input;
using PFR_INVEST.DataObjects.ListItems;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for CorrespondenceListView.xaml
    /// </summary>
    public abstract partial class CorrespondenceListView
    {
        protected CorrespondenceListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }
        
        protected abstract void OpenEdit(CorrespondenceListItemNew item);

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var currentItem  = tableView.DataControl.CurrentItem;
            if (currentItem == null) return;
            var item = currentItem as CorrespondenceListItemNew;
            OpenEdit(item);
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
	}
}
