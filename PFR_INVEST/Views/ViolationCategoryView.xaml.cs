﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ViolationCategoryView.xaml
    /// </summary>
    public partial class ViolationCategoryView : UserControl
    {
        public ViolationCategoryView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
