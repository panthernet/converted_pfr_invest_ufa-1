﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using System.Collections.Generic;
using DevExpress.Xpf.Grid;
using PFR_INVEST.DataObjects.ListItems;
using System;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for MarketCostScopeListView.xaml
	/// </summary>
	public partial class MarketCostScopeListView
	{
		public MarketCostScopeListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, grMarketCostScopeList);
        }

		#region events

		private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var item = grMarketCostScopeList.SelectedItem as MarketCostListItem;
			if (item == null)
				return;
			var selectedID = item.ID;
			switch (item.FormType)
			{
				case MarketCostIdentifier.Forms.F20:
					App.DashboardManager.OpenNewTab(typeof(F20View), ViewModelState.Read, selectedID, "Форма 20");
					break;
				case MarketCostIdentifier.Forms.F24:
					App.DashboardManager.OpenNewTab(typeof(F24View), ViewModelState.Read, selectedID, "Форма 24");
					break;
				case MarketCostIdentifier.Forms.F25:
					App.DashboardManager.OpenNewTab(typeof(F25View), ViewModelState.Read, selectedID, "Форма 25");
					break;
				case MarketCostIdentifier.Forms.F26:
					App.DashboardManager.OpenNewTab(typeof(F26View), ViewModelState.Read, selectedID, "Расчет рыночной стоимости активов, в которые инвестированы средства пенсионных накоплений (по учредителю управления)");
					break;
			}

		}

		#endregion

		protected void GridViewSort(object sender, CustomColumnSortEventArgs e)
		{
			try
			{
			    if (e.Column.FieldName != "ReportOnDateYear")
                    return;
			    var source = (grMarketCostScopeList.ItemsSource as List<MarketCostListItem>);
			    var dr1 = source?[e.ListSourceRowIndex1];
			    var dr2 = source?[e.ListSourceRowIndex2];
					
			    e.Result = Comparer<int?>.Default.Compare(dr1?.ReportOnDateYear, dr2?.ReportOnDateYear);
			    //Сортируем по дате для обхода бага грида со сбросом сортировки
			    if (e.Result == 0) 				
			        e.Result = Comparer<DateTime?>.Default.Compare(dr1?.ReportOnDate, dr2?.ReportOnDate);
			    e.Handled = true;
			}
			catch (Exception)
			{
				e.Handled = false;
			}
		}

		private void MarketCostScopeListViewShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			if (e.Column.FieldName != "ReportOnDate") return;

			var source = (grMarketCostScopeList.ItemsSource as List<MarketCostListItem>);

			foreach (var o in (List<object>) e.ComboBoxEdit.ItemsSource)
			{
				var item = o as CustomComboBoxItem;
			    var editvalue = item?.EditValue as DateTime?;
			    if (editvalue == null)
                    continue;
			    var si = source?.Find(x => x.ReportOnDate.HasValue && x.ReportOnDate.Value.Equals(editvalue.Value));
			    item.DisplayValue = si?.ReportOnDateDate;
			}
		}

		private void MarketCostScopeListViewDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
		    if (e.Column.FieldName != "ReportOnDate")
                return;
		    var item = e.Row as MarketCostListItem;
		    if (item != null)
		        e.DisplayText = item.ReportOnDateDate;
		}
	}
}
