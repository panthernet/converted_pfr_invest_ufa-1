﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for KBKListView.xaml
    /// </summary>
    public partial class NPFCodeListView
    {
        protected NPFCodeListViewModel Model => DataContext as NPFCodeListViewModel;

        public NPFCodeListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, grdNPFCodeList);
        }

        protected void tvNPFCodeList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (grdNPFCodeList.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (grdNPFCodeList.View.FocusedRowHandle >= 0)
                OpenItem((long)grdNPFCodeList.GetCellValue(grdNPFCodeList.View.FocusedRowHandle, "ID"));
        }

        protected virtual void OpenItem(long id)
        {
            App.DashboardManager.OpenNewTab(typeof (NPFCodeView), ViewModelState.Edit, id, Model.IsErrorCode,
                Model.IsErrorCode ? "Код ошибки" : "Код стыковки с договором");
        }
    }
}
