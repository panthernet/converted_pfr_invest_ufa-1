﻿using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for FinRegisterView.xaml
    /// </summary>
    public partial class FinRegisterView
    {
        public FinRegisterView()
        {
            InitializeComponent();
            Loaded += (sender, args) => UpdateTabVisibility();
            ModelInteractionHelper.SignUpForCustomAction(this, o => UpdateTabVisibility());
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
        }

        private void UpdateTabVisibility()
        {
            var dc = DataContext as FinRegisterViewModel;
            if (dc != null && dc.IsNotTransferred)
                tabView.HeaderLocation = HeaderLocation.Top;
        }

        private void gridControl_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Num")
                e.Value = e.ListSourceRowIndex + 1;
        }   
    }
}
