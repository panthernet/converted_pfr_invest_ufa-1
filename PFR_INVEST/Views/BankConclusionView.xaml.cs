﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for BankConclusionView.xaml
    /// </summary>
    public partial class BankConclusionView : UserControl
    {
        public BankConclusionView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
