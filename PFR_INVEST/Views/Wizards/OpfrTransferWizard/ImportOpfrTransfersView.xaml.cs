﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Wizards.OpfrTransferWizard
{
    public class ImportOpfrTransfersViewUserControl : GenericWizardStepEx<OpfrTransferWizardViewModel> { }

    /// <summary>
    /// Interaction logic for ImportOpfrTransfersView.xaml
    /// </summary>
    public partial class ImportOpfrTransfersView
    {
        public ImportOpfrTransfersView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, mainGrid, "OpfrTransferListItems");
        }
    }
}
