﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.DepositCountWizard
{
    public class ShowDepositsCreateStepViewUserControl : GenericWizardStepEx<DepositCountWizardViewModel> { }
    /// <summary>
    /// Interaction logic for ShowDepositsCreateStepView.xaml
    /// </summary>
    public partial class ShowDepositsCreateStepView : IWizardStepEx
    {
        public ShowDepositsCreateStepView()
        {
            InitializeComponent();
            FinishButtonText = "Разместить депозиты";
        }
    }
}
