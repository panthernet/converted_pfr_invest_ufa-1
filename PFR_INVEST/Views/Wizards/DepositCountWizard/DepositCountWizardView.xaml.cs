﻿using System;
using System.Collections.Generic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.DepositCountWizard
{
    /// <summary>
    /// Interaction logic for DepositCountWizardView.xaml
    /// </summary>
    public partial class DepositCountWizardView
    {
        private DepositCountWizardViewModel Model => DataContext as DepositCountWizardViewModel;

        public DepositCountWizardView()
        {
            InitializeComponent();
            InitWizardSteps();
        }

        private void InitWizardSteps()
        {
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SelectDepositCountActionStepView", typeof(SelectDepositCountActionStepView)));
            //второй для корректного отображени кнопки Далее, злобный хак
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("Fake", typeof(SelectDepositCountActionStepView)));
            wizard.btnFinish.Content = "Завершить";

            wizard.Cancel += (sender, args) =>
            {
                wizard.Close(false);
            };
            wizard.Complete += (sender, args) =>
            {
                if (!Model.FinalAction()) return;
                wizard.Close();
            };
        }
    }
}
