﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.SpnDeposit
{
    public class SpnDepositPrintDocumentsUserControl: GenericWizardStepEx<SpnDepositWizardViewModel> { }
    /// <summary>
    /// Interaction logic for SpnDepositPrintDocuments.xaml
    /// </summary>
    public partial class SpnDepositPrintDocuments
    {
        public SpnDepositPrintDocuments()
        {
            InitializeComponent();
            NextButtonText = "Экспортировать";
        }
    }
}
