﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.SpnDeposit
{
    public class SpnDepositCreateDepClaim2UserControl : GenericWizardStepEx<SpnDepositWizardViewModel> { }
    /// <summary>
    /// Interaction logic for SpnDepositCreateDepClaim2.xaml
    /// </summary>
    public partial class SpnDepositCreateDepClaim2
    {
        public SpnDepositCreateDepClaim2()
        {
            InitializeComponent();
            NextButtonText = "Сохранить";
        }
    }
}
