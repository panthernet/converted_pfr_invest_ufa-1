﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Views.TransferWizard;


namespace PFR_INVEST.Views.Wizards.TransferWizard.SIRopsAsvWizard
{
    public class TransferSIRopsAsvWizardView : TransferWizardView
    {
        public IWizard Wizard => ctlWizard;

        protected override void InitSteps()
        {
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("OperationType", typeof(OperationTypeRopsView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("SelectContracts", typeof(SelectContractsRopsAsv)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("InputTransferSum", typeof(InputTransferSumView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("FinalScreen", typeof(FinishScreenView)));
            ctlWizard.btnFinish.Content = "Готово";
        }

        


    }
}
