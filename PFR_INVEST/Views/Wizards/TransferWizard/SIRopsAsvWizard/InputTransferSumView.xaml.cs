﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Utils;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI.RopsAsvWizard;
using PFR_INVEST.BusinessLogic;


namespace PFR_INVEST.Views.Wizards.TransferWizard.SIRopsAsvWizard
{
    /// <summary>
    /// Interaction logic for InputTransferSumView.xaml
    /// </summary>
    public partial class InputTransferSumView : UserControl, IWizardStep
    {
        private const string SUM_ZERO_MSG = "Сумма всех элементов равна нулю. Вы уверены, что хотите продолжить?";
        public TransferSIRopsAsvWizardViewModel Model => DataContext as TransferSIRopsAsvWizardViewModel;

        public IWizard Wizard
        {
            get;
            set;
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

        public InputTransferSumView()
        {
            InitializeComponent();
        }
        private void grdContracts_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (grdContracts.IsValidRowHandle(grdContracts.View.FocusedRowHandle + 1))
                    grdContracts.View.MoveNextRow();
                else
                {
                    grdContracts.View.MoveFirstRow();
                    if (!grdContracts.GetGridView().VisibleColumns.Last().Equals(grdContracts.CurrentColumn))
                        grdContracts.View.MoveNextCell();
                    else
                        grdContracts.CurrentColumn = grdContracts.GetGridView().VisibleColumns.First();
                }

                if (grdContracts.CurrentColumn != null
                    && grdContracts.CurrentColumn.AllowEditing == DefaultBoolean.True)
                    grdContracts.View.ShowEditor();

                e.Handled = true;
            }
        }
        private void TableView_HiddenEditor(object sender, EditorEventArgs e)
        {
            UpdateCalculations();
        }

        private void UpdateCalculations()
        {
            decimal dSum = Model.SelectedContractsList.Sum(c => c.Sum);
            txtSumm.Text = dSum.ToString(CultureInfo.InvariantCulture);
            grdContracts.RefreshData();
        }

        protected void btnCalcSumm_Clicked(object sender, EventArgs e)
        {
            foreach (var c in Model.SelectedContractsList)
            {
                var scha = Model.GetSCHAByContract(c.ContractID, Model.StartDate.Value, Model.EndDate.Value);
                if (Model.IsShowRatio)
                    c.Sum = Math.Round((scha * Model.RatioASV.Value * Model.RatioEval.Value) / 100, 2);
                else
                    c.Sum = Math.Round((scha * Model.RatioROPS.Value) / 100, 2);
            }
            UpdateCalculations();
        }

        public bool OnMoveNext()
        {
            return CheckZeroSumOnMoveNext();
        }

        private void RefreshDataSource()
        {
            grdContracts.BeginDataUpdate();
            grdContracts.ItemsSource = null;
            grdContracts.ItemsSource = Model.SelectedContractsList;
            grdContracts.EndDataUpdate();
        }
        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            RefreshDataSource();
            //Wizard.SetDimensions(750, 400);
        }

        private bool CheckZeroSumOnMoveNext()
        {
            var aSelected = Model.SelectedContractsList;

            if (aSelected.Any(selectedItem => !selectedItem.Validate()))
            {
                ViewModelBase.DialogHelper.ShowError("Заполните корректно таблицу расчета сумм и коэффициентов.");
                return false;
            }

            var dSum = aSelected.Sum(rec => rec.Sum);

            if (dSum == 0)
                return DXMessageBox.Show(SUM_ZERO_MSG, "Внимание", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes;

            return true;

        }

    }
}
