﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.BranchReport;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.VR;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.TransferWizard
{
    public partial class FinishScreenView : IWizardStep
    {
        public FinishScreenView()
        {
            InitializeComponent();
        }

        public IWizard Wizard
        {
            get;
            set;
        }

        public TransferWizardViewModel Model => DataContext as TransferWizardViewModel;

        public bool OnMoveNext()
        {
            return true;
        }

        public bool OnMovePrev()
        {
            return true;
        }


        public void OnShow()
        {
            CreateObject();
            _created = true;
            Wizard.UpdateNavigationControlsState();
            //Wizard.SetDimensions(550, 120);
        }

        public WizardNavigationButtonState NextButtonState => _created ? WizardNavigationButtonState.Visible : WizardNavigationButtonState.Disabled;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => _created ? WizardNavigationButtonState.Hidden : WizardNavigationButtonState.Visible;

        private bool _created;

        private void OpenAssignPaymentsListView()
        {
            AssignPaymentsListViewModel vm = null;
            switch ((Document.Types)Model.ContractType)
            {
                case Document.Types.SI:
                    vm = App.DashboardManager.FindViewModel<SIAssignPaymentsListViewModel>();
                    break;
                case Document.Types.VR:
                    vm = App.DashboardManager.FindViewModel<VRAssignPaymentsListViewModel>();
                    break;
            }

            if (vm != null)
            {
                App.DashboardManager.RefreshListViewModels(new[] { vm.GetType() });
                vm.OnSelectGridRow(Model.RegisterID);
            }
            else
            {
                switch (Model.ContractType)
                {
                    case (int)Document.Types.VR:
                        App.DashboardManager.OpenNewTab(typeof(VRAssignPaymentsListView), ViewModelState.Read, Model.RegisterID, "Отзыв средств");
                        break;
                    case (int)Document.Types.SI:
                        App.DashboardManager.OpenNewTab(typeof(SIAssignPaymentsListView), ViewModelState.Read, Model.RegisterID, "Отзыв средств");
                        break;
                }
            }
        }

        private void OpenAssignPaymentsArchiveListView()
        {
            AssignPaymentsArchiveListViewModel vm = null;
            switch ((Document.Types)Model.ContractType)
            {
                case Document.Types.SI:
                    vm = App.DashboardManager.FindViewModel<SIAssignPaymentsArchiveListViewModel>();
                    break;
                case Document.Types.VR:
                    vm = App.DashboardManager.FindViewModel<VRAssignPaymentsArchiveListViewModel>();
                    break;
            }

            if (vm != null)
            {
                App.DashboardManager.RefreshListViewModels(new[] { vm.GetType() });
                vm.OnSelectGridRow(Model.RegisterID);
            }
            else
            {
                switch (Model.ContractType)
                {
                    case (int)Document.Types.VR:
                        App.DashboardManager.OpenNewTab(typeof(VRAssignPaymentsArchiveListView), ViewModelState.Read, Model.RegisterID, "Архив отзыва средств");
                        break;
                    case (int)Document.Types.SI:
                        App.DashboardManager.OpenNewTab(typeof(SIAssignPaymentsArchiveListView), ViewModelState.Read, Model.RegisterID, "Архив отзыва средств");
                        break;
                }
            }
        }

        private void OpenSITransfersListView()
        {
            switch (Model.ContractType)
            {
                case (int)Document.Types.VR:
                {
                    var vm = (TransfersVRListViewModel)App.DashboardManager.FindViewModel(typeof(TransfersVRListViewModel));

                    if (vm != null)
                    {
                        App.DashboardManager.RefreshListViewModels(new[] { typeof(TransfersVRListViewModel) });
                        vm.RaiseSelectGridRow(Model.RegisterID);
                    }
                    else
                    {
                        App.DashboardManager.OpenNewTab(typeof(TransfersVRListView), ViewModelState.Read, Model.RegisterID, "Перечисления ВР");
                    }

                }
                    break;
                case (int)Document.Types.SI:
                {

                    var vm = (TransfersSIListViewModel)App.DashboardManager.FindViewModel(typeof(TransfersSIListViewModel));

                    if (vm != null)
                    {
                        App.DashboardManager.RefreshListViewModels(new[] { typeof(TransfersSIListViewModel) });
                        vm.RaiseSelectGridRow(Model.RegisterID);
                    }
                    else
                    {
                        App.DashboardManager.OpenNewTab(typeof(TransfersSIListView), ViewModelState.Read, Model.RegisterID, "Перечисления СИ");
                    }
                }
                    break;
            }
        }

        public void OpenCreated()
        {
            if (Model.RegisterID <= 0) return;
            if (Model.GetType() == typeof(TransferVR13YearWizardViewModel))
            {
                var transfersYearReport = new PrintVRYearTransfers 
                {
                    VrYearTransferRegisterId = Model.RegisterID
                };
                transfersYearReport.Print();
                return;
            }

            if (!Model.CreateSITransfers)
            {
                var currentYear = DateTime.Today.Year;
                if (Convert.ToInt32(Model.SelectedYear.Name) < currentYear - 5)
                    OpenAssignPaymentsArchiveListView();
                else
                    OpenAssignPaymentsListView();
            }
            else
            {
                OpenSITransfersListView();
            }
        }

        private void CreateObject()
        {
            bool shouldRequest = true;

            while (shouldRequest)
            {
                try
                {
                    if(!Model.CreateRegisterEx())
                    {
                        ViewModelBase.DialogHelper.ShowError("Внутренняя ошибка при сохранении данных на сервере!\nПроизведен откат, данные не сохранены.");
                        const string exText = "EXCEPTION: На стороне сервиса при сохранении данных мастера перечислений! CreateRegisterEx()";
                        App.log.WriteLine(exText);
                        throw new Exception(exText);
                    }
                    shouldRequest = false;
                }
                catch (Exception ex)
                {
                    App.log.WriteException(ex);
                    var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    shouldRequest = res == MessageBoxResult.Yes;
                    if (!shouldRequest)
                        Wizard.DoCancel();
                }
            }
            if (Model.CreateSITransfers)
                txtNumber.EditValue = Model.RegisterID;
            else
                txtNumber.EditValue = Model.SelectedYear.Name;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model.CreateSITransfers) return;
            lblRegisterCompanyName.Content = "Год";
            groupBox.Header = "План на год";
        }
    }
}
