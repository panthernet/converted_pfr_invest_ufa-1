﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.TransferWizard
{
	/// <summary>
	/// Interaction logic for SelectManagementView.xaml
	/// </summary>
	public partial class SelectManagementView : IWizardStep
	{

		public const string EMPTY_SELECTION_MSG = "Ничего не выбрано";

		public TransferWizardViewModel Model => DataContext as TransferWizardViewModel;

	    public List<long> SelectedValues = new List<long>();

		public SelectManagementView()
		{
			InitializeComponent();
			Loaded += SelectManagementView_Loaded;
            ModelInteractionHelper.SignUpForCustomAction(this, a =>
            {
                SelectedValues.Clear();
                ((List<long>)a).ForEach(id =>
                {
                    if (!SelectedValues.Contains(id))
                        SelectedValues.Add(id);
                });
                var rList = GetDataRowHandles();
                rList.ForEach(h =>
                {
                    var id = Convert.ToInt64(grdCC.GetCellValue(h, "ContractID"));
                    if (SelectedValues.Contains(id))
                        grdCC.SetCellValue(h, "Selected", true);
                });
                if (SelectedValues.Any() && OnMoveNext())
                    Wizard.ShowNextStep();
            }, false);
        }

		void SelectManagementView_Loaded(object sender, RoutedEventArgs e)
		{
			grdCC.ItemsSource = Model.AvailableContractsList;
        }

		private void grdCC_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
		{
			if (e.Column.FieldName != "Selected") return;
			var sId = e.GetListSourceFieldValue("ContractID").ToString();
			long id;

			long.TryParse(sId, out id);

			if (e.IsGetData)
			{
				e.Value = GetIsSelected(id);
			}
			if (e.IsSetData)
			{
				SetIsSelected(id, (bool)e.Value);
			}
		}

		bool GetIsSelected(long id)
		{
			return SelectedValues.Contains(id);
		}

		void SetIsSelected(long id, bool value)
		{
			if (value == false)
				SelectedValues.Remove(id);
			else
				if (SelectedValues.Contains(id) == false)
					SelectedValues.Add(id);
		}

		public IWizard Wizard
		{
			get;
			set;
		}

		public bool OnMoveNext()
		{
			if (SelectedValues.Count == 0)
			{
				DXMessageBox.Show(EMPTY_SELECTION_MSG, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
				return false;
			}

			var selectedList = Model.AvailableContractsList.Where(rec => SelectedValues.Contains(rec.ContractID)).ToList();
			Model.SelectedContractsList = selectedList;
            if (Model.ImportList.Any())
                Model.ImportList.ForEach(item =>
                {
                    var name = item.UK.Trim().ToLower();
                    var cName = item.RegNum.Trim().ToLower();
                    var p = Model.SelectedContractsList.FirstOrDefault(a => a.UkFormalizedName.ToLower() == name && a.ContractName.ToLower() == cName);
                    if (p != null)
                    {
                        p.Sum = item.SumToUK ?? item.SumFromUKTotal ?? 0;
                        p.InvestIncome = item.InvestIncome ?? 0;
                        p.QuantityZL = item.ZLCount;
                      //  p.ContractOperateSum;
                    }
                });

            return true;
		}

		public bool OnMovePrev()
		{
			return true;
		}

		public void OnShow()
		{
			var shouldRequest = true;
			while (shouldRequest)
			{
				try
				{
					Model.EnsureAvailableList();
					if (!Model.AvailableContractsList.Any())
					{
						Wizard.ShowPreviousStep();

						var message = Model.AllContractsList.Any() ?
							"Невозможно создать реестр перечислений. На указанный период реестр по всем действующим договорам уже создан." :
						    $"Невозможно создать {(Model.SelectedTransferType.ShowMonth.HasValue ? "план на месяц" : "годовой план")}. На указанный период действующих договоров не обнаружено";

						DXMessageBox.Show(message, "Операция отменена.", MessageBoxButton.OK, MessageBoxImage.Warning);

						return;
					}

					shouldRequest = false;
					grdCC.ItemsSource = Model.AvailableContractsList;
				}
				catch (Exception ex)
				{
					App.log.WriteException(ex);
					var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
					shouldRequest = res == MessageBoxResult.Yes;
					if (!shouldRequest)
						Wizard.DoCancel();
				}
			}
			Wizard.SetFreeDimensions(750, 400, false);
		}

		public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;
	    public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;
	    public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

	    private void UpdateSelectionForAll(bool newIsSelected)
		{
			var list = GetDataRowHandles();
			for (var i = 0; i < list.Count; i++)
			{
				var rowHandle = grdCC.GetRowHandleByListIndex(i);
				grdCC.SetCellValue(rowHandle, "Selected", newIsSelected);
                SetIsSelected(Convert.ToInt32(grdCC.GetCellValue(rowHandle, "ContractID")), newIsSelected);
			}
		}

		private bool _mGridRowCheckBoxClicked;
		private void CheckBox_Click(object sender, RoutedEventArgs e)
		{
			_mGridRowCheckBoxClicked = true;
			if (SelectedValues.Count == 0)
				_headerCheckEdit.IsChecked = false;
			else if (SelectedValues.Count != Model.AvailableContractsList.Count)
				_headerCheckEdit.IsChecked = null;
			else
				_headerCheckEdit.IsChecked = true;
		}

		private void CheckEdit_Checked(object sender, RoutedEventArgs e)
		{
			if (!_mGridRowCheckBoxClicked)
				UpdateSelectionForAll(true);
			_mGridRowCheckBoxClicked = false;
		}

		private void CheckEdit_Indeterminate(object sender, RoutedEventArgs e)
		{
			if (!_mGridRowCheckBoxClicked)
				_headerCheckEdit.IsChecked = false;
			_mGridRowCheckBoxClicked = false;
		}

		private void CheckEdit_Unchecked(object sender, RoutedEventArgs e)
		{
			UpdateSelectionForAll(false);
			_mGridRowCheckBoxClicked = false;
		}

		private List<int> GetDataRowHandles()
		{
			var rowHandles = new List<int>();
			for (var i = 0; i < grdCC.VisibleRowCount; i++)
			{
				var rowHandle = grdCC.GetRowHandleByVisibleIndex(i);
				if (grdCC.IsGroupRowHandle(rowHandle))
				{
					if (!grdCC.IsGroupRowExpanded(rowHandle))
					{
						rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
					}
				}
				else
					rowHandles.Add(rowHandle);
			}
			return rowHandles;
		}

		private IEnumerable<int> GetDataRowHandlesInGroup(int groupRowHandle)
		{
			var rowHandles = new List<int>();
			for (var i = 0; i < grdCC.GetChildRowCount(groupRowHandle); i++)
			{
				var rowHandle = grdCC.GetChildRowHandle(groupRowHandle, i);
				if (grdCC.IsGroupRowHandle(rowHandle))
				{
					rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
				}
				else
					rowHandles.Add(rowHandle);
			}
			return rowHandles;
		}

		CheckBox _headerCheckEdit;
		private void CheckEdit_Loaded(object sender, RoutedEventArgs e)
		{
			_headerCheckEdit = sender as CheckBox;
		}
	}
}
