﻿using System;
using System.Collections.Generic;
using System.Windows;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.TransferWizard
{
    /// <summary>
    /// Interaction logic for TransferWizardView.xaml
    /// </summary>
    public partial class TransferWizardView
    {
        public Action CloseDelegate;
        public IWizard Wizard => ctlWizard;

        public TransferWizardView()
        {
            InitializeComponent();
            InitWizardSteps();
            ctlWizard.SizeChanged += ctlWizard_SizeChanged;
        }

        public void ctlWizard_SizeChanged(object sender, SizeChangedEventArgs e)
        {
           /* if (e.PreviousSize.Width == 0 && e.PreviousSize.Height == 0) return;
			var sz = new Size(ctlWizard.MinWidth + 10, ctlWizard.MinHeight + 40);
			var max = new Size(ctlWizard.MaxWidth + 10, ctlWizard.MaxHeight + 40);
			DashboardManager.ResizeActiveView(sz, max);	
             */  
        }

        public void InitWizardSteps()
        {
            InitSteps();

            Loaded += TransferWizardView_Loaded;
            Wizard.Cancel += Wizard_Cancel;
            Wizard.Complete += Wizard_Complete;
        }

        protected virtual void InitSteps()
        {
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("OperationType", typeof(OperationTypeCheckView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("SelectManagement", typeof(SelectManagementView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("AddSumm", typeof(AddSummView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("FinalScreen", typeof(FinishScreenView)));
        }
        private void Wizard_Complete(object sender, RoutedEventArgs e)
        {
            CloseDelegate?.Invoke();
            DashboardManager.CloseActiveView();
            var finishScreenView = ctlWizard.ActiveStep as FinishScreenView;
            finishScreenView?.OpenCreated();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.SizeChanged -= ctlWizard_SizeChanged;
            ctlWizard.Dispose();
        }

        private void Wizard_Cancel(object sender, RoutedEventArgs e)
        {
            CloseDelegate?.Invoke();
            DashboardManager.CloseActiveView();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.SizeChanged -= ctlWizard_SizeChanged;
            ctlWizard.Dispose();
        }

        private void TransferWizardView_Loaded(object sender, RoutedEventArgs e)
        {
            ctlWizard.ShowStep(0);
        }

    }
}
