﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Utils;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.VR;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.TransferWizard
{
    /// <summary>
    /// Interaction logic for AddSumm.xaml
    /// </summary>
    public partial class AddSummView : IWizardStep
    {

        private const string SUM_ZERO_MSG = "Сумма всех элементов равна нулю. Вы уверены, что хотите продолжить?";
        private const string SUM_ZERO_MSG2 = "Введенная общая сумма меньше либо равна нулю. Введите правильную сумму и повторите еще раз.";
        private const string CONTRACT_SUM_ZERO = "Сумма средств в управлении по одному или нескольким договорам меньше либо равна нулю. Расчет может быть неверным!";

        public enum CalculationType
        {
            BySum,
            ByContracts
        }

        public AddSummView()
        {
            InitializeComponent();
            Loaded += AddSummView_Loaded;
			txtSumm.EditValue = 0.0m;
        }

        void AddSummView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model.CreateSITransfers && !Model.ImportList.Any())
                rbBySum.IsChecked = true;
            else
                rbByContracts.IsChecked = true;
        }

        public CalculationType CurrentCalculationType => rbBySum.IsChecked == true ? CalculationType.BySum : CalculationType.ByContracts;

        public TransferWizardViewModel Model => DataContext as TransferWizardViewModel;


        public IWizard Wizard
        {
            get;
            set;
        }

        public bool OnMoveNext()
        {
            return CheckZeroSumOnMoveNext();
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            RefreshDataSource();
            UpdateUIByCalcType();
            //Wizard.SetDimensions(750, 400);
        }

        private bool CheckZeroSumOnMoveNext()
        {
            var aSelected = Model.SelectedContractsList;

            if (!IsModelPlan())
            {
                if (aSelected.Any(s => s.Sum <= 0))
                {
                    ViewModelBase.DialogHelper.ShowError("Сумма перечисления  должна  быть  больше нуля.");
                    return false;
                }
            }

            if (aSelected.Any(selectedItem => !selectedItem.Validate())) {
                ViewModelBase.DialogHelper.ShowError("Заполните корректно таблицу расчета сумм и коэффициентов.");
                return false;
            }


            var dSum = aSelected.Sum(rec => rec.Sum);

            //если превод из УК то нужно убедиться что количество переводимых ЗЛ не превышает
            //количество ЗЛ находящихся на балансе в УК
            if (Model.SelectedTransferDirection?.isFromUKToPFR != null && Model.SelectedTransferDirection.isFromUKToPFR.Value)
            {
                var data = new Dictionary<long, int>();
                aSelected.ForEach(a => data.Add(a.ContractID, a.QuantityZL));
                long zlCount;
                if (!Model.IsEnoughZL(data, out zlCount))
                {
                    string msg = $"На момент {DateTime.Today:dd.MM.yyyy} выбранный портфель содержит {zlCount} ЗЛ, что меньше, чем указанная сумма. Продолжить?";
                    return DXMessageBox.Show(msg, "Внимание", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes;
                }
            }


            if (dSum == 0)
                return DXMessageBox.Show(SUM_ZERO_MSG, "Внимание", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes;

            return true;

        }

        private void RefreshDataSource()
        {
            grdContracts.BeginDataUpdate();
            grdContracts.ItemsSource = null;
            grdContracts.ItemsSource = Model.SelectedContractsList;
            grdContracts.EndDataUpdate();
        }


        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

        private void CalcTypeChanged(object sender, RoutedEventArgs e)
        {
            UpdateUIByCalcType();
        }

        private void UpdateUIByCalcType()
        {
            txtSumm.IsEnabled = CurrentCalculationType != CalculationType.ByContracts;
            btnCalcSumm.Visibility = CurrentCalculationType == CalculationType.ByContracts ?
                Visibility.Hidden :
                Visibility.Visible;

            txtInvestProfit.IsEnabled = false;
            txtQuantityZL.IsEnabled = false;

            txtInvestProfit.Visibility =
                (Model.SelectedTransferDirection.isFromUKToPFR.Value && Model.CreateSITransfers) ?
                (Visibility.Visible) :
                Visibility.Collapsed;
            txtQuantityZL.Visibility = (Model.SelectedTransferType.ShowInsuredPersonsCount.Value) ?
                (Visibility.Visible) :
                Visibility.Collapsed;


            colSum.AllowEditing = Convert(CurrentCalculationType == CalculationType.ByContracts);

            colIncome.Visible = Model.SelectedTransferDirection.isFromUKToPFR.Value && Model.CreateSITransfers;
            colIncome.AllowEditing = Convert(colIncome.Visible);

            colQuantityZL.Visible = Model.SelectedTransferType.ShowInsuredPersonsCount.Value;
            colQuantityZL.AllowEditing = Convert(colQuantityZL.Visible);

            UpdateCalculations(false);
        }

        private static DefaultBoolean Convert(bool pB)
        {
            return pB ? DefaultBoolean.True : DefaultBoolean.False;
        }

        private void UpdateCalculations(bool pBDivide)
        {
            var aSelected = Model.SelectedContractsList;

            decimal dSum = 0;
            decimal dIncome = 0;
            decimal dZL = 0;

            //Просто суммирование по столбцам 
            foreach (var rec in aSelected)
            {
                if (Model.SelectedTransferDirection.isFromUKToPFR.Value == false)
                    rec.InvestIncome = 0;

                if (Model.SelectedTransferType.ShowInsuredPersonsCount.Value == false)
                    rec.QuantityZL = 0;

                dSum += rec.Sum;
                dIncome += rec.InvestIncome;
                dZL += rec.QuantityZL;
            }

            if (CurrentCalculationType == CalculationType.ByContracts)
                txtSumm.Text = dSum.ToString(CultureInfo.InvariantCulture);

            txtInvestProfit.Text = Model.SelectedTransferDirection.isFromUKToPFR.Value ? dIncome.ToString() : "";

            txtQuantityZL.Text = Model.SelectedTransferType.ShowInsuredPersonsCount.Value ? dZL.ToString() : "";


            //Расчет по весам
            if (CurrentCalculationType == CalculationType.BySum && pBDivide)
            {
                DivideSumByWeights();
            }

        }

        private void DivideSumByWeights()
        {

            decimal dSum;
			
            decimal.TryParse(txtSumm.Text.Replace('.', ','), NumberStyles.AllowDecimalPoint, CultureInfo.CurrentCulture, out dSum);

            if (dSum <= 0)
            {
                DXMessageBox.Show(SUM_ZERO_MSG2, "Внимание", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            bool validContracts = true;
            foreach (var rec in Model.SelectedContractsList)
            {
                if (rec.ContractOperateSum <= 0)
                {
                    validContracts = false;
                }
            }
            if (!validContracts)
            {
                DXMessageBox.Show(CONTRACT_SUM_ZERO, "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            Model.DivideSum(dSum);

            RefreshDataSource();
        }


        protected void btnCalcSumm_Clicked(object sender, EventArgs e)
        {
            UpdateCalculations(true);
        }

        private void grdContracts_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (grdContracts.IsValidRowHandle(grdContracts.View.FocusedRowHandle + 1))
                    grdContracts.View.MoveNextRow();
                else
                {
                    grdContracts.View.MoveFirstRow();
                    if (!grdContracts.GetGridView().VisibleColumns.Last().Equals(grdContracts.CurrentColumn))
                        grdContracts.View.MoveNextCell();
                    else
                        grdContracts.CurrentColumn = grdContracts.GetGridView().VisibleColumns.First();
                }

                if (grdContracts.CurrentColumn != null
                    && grdContracts.CurrentColumn.AllowEditing == DefaultBoolean.True)
                    grdContracts.View.ShowEditor();

                e.Handled = true;
            }
        }

        private void TableView_HiddenEditor(object sender, EditorEventArgs e)
        {
            UpdateCalculations(false);
        }

        private bool IsModelPlan()
        {
            return Model is TransferVR13YearWizardViewModel || Model is TransferVR8YearWizardViewModel
                   || Model is TransferSI8YearWizardViewModel;
        }

        private void ContentElement_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            
        }
    }
}
