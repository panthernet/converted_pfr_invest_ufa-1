﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.PlanCorrectionWizard
{
    /// <summary>
    /// Interaction logic for UKPlanSumsEditView.xaml
    /// </summary>
    public partial class UKPlanSumsEditView : IWizardStep
    {
        public UKPlanSumsEditView()
        {
            InitializeComponent();
        }

        public PlanCorrectionWizardViewModel<PlanContractRecord> Model => DataContext as PlanCorrectionWizardViewModel<PlanContractRecord>;

        public IWizard Wizard
        {
            get;
            set;
        }

        public bool OnMoveNext()
        {
            return true;
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            bool shouldRequest = true;

            while (shouldRequest)
            {
                try
                {
                    Model.EnsureAvailableList();
                    shouldRequest = false;
                }
                catch (Exception ex)
                {
                    App.log.WriteException(ex);
                    var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    shouldRequest = res == MessageBoxResult.Yes;
                    if (!shouldRequest)
                        Wizard.DoCancel();
                }
            }
            //Wizard.SetDimensions(450, 400);
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;
    }
}
