﻿using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.PlanCorrectionWizard
{
    /// <summary>
    /// Interaction logic for YearMonthSelectView.xaml
    /// </summary>
    public partial class OpTypeSelectView : IWizardStep
    {
        public OpTypeSelectView()
        {
            InitializeComponent();
        }

        public PlanCorrectionWizardViewModel<PlanContractRecord> Model => DataContext as PlanCorrectionWizardViewModel<PlanContractRecord>;

        public IWizard Wizard { get; set; }

        public bool OnMoveNext()
        {
            //Если есть выбор типа операции, проверяем на наличие годовых планов
            //без спец флага (т.к. предварительная проверка не производилась в DashboardManager)
            if (Model.SelectedOp != null)
            {
                //инициализируем данные, согласно выбранному типу операции
                Model.InitializeDates();
                //проверяем
                if (Model.IsModelContainCorrectData(false)) return true;
                DXMessageBox.Show("\"Годовые планы\" для выбранного типа операции отсутствуют.");
                return false;
            }
            return true;
        }

        public bool OnMovePrev()
        {
            return false;
        }

        public void OnShow()
        {
           // Wizard.SetDimensions(500, 110);
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;
    }
}
