﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.Views.Wizards
{
	/// <summary>
	/// Interaction logic for Wizard.xaml
	/// </summary>
	public partial class WizardControlEx : IWizard, IDisposable
	{
		private readonly Dictionary<string, IWizardStepEx> _steps;
	    private int _activeStepIndex;
	    private IWizardExModel _model;

	    private IWizardExModel Model
	    {
	        get { return _model; }
	        set { _model = value; if (Model == null)
                    throw new Exception("Модель визарда должна наследоваться от интерфейса IWizardExModel");
            }
	    }

        public WizardControlEx Me => this;

	    public int ActiveStepIndex
	    {
	        get { return _activeStepIndex; }
	        private set { _activeStepIndex = value;
	            if(Model != null) Model.Step = _activeStepIndex +1;
	        }
	    }

	    public List<KeyValuePair<string, Type>> WizardSteps { get; }
		public IWizardStepEx ActiveStep { get; private set; }

        #region Events
        public static readonly RoutedEvent CompleteEvent =
			EventManager.RegisterRoutedEvent("CompleteEvent",
			RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WizardControlEx));

		public event RoutedEventHandler Complete
		{
			add { AddHandler(CompleteEvent, value); }
			remove { RemoveHandler(CompleteEvent, value); }
		}

		protected void OnComplete()
		{
			RaiseEvent(new RoutedEventArgs(CompleteEvent));

		}

		public static readonly RoutedEvent CancelEvent =
			EventManager.RegisterRoutedEvent("CancelEvent",
			RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WizardControlEx));

		public event RoutedEventHandler Cancel
		{
			add { AddHandler(CancelEvent, value); }
			remove { RemoveHandler(CancelEvent, value); }
		}

		protected void OnCancel()
		{
			RaiseEvent(new RoutedEventArgs(CancelEvent));
		}

        #endregion

        public ICommand CancelCommand { get; set; }
        public ICommand NextCommand { get; set; }
        public ICommand PrevCommand { get; set; }

        public WizardControlEx()
		{
			InitializeComponent();
			_steps = new Dictionary<string, IWizardStepEx>();
			WizardSteps = new List<KeyValuePair<string, Type>>();

            //инициализация команд после контекста
            NextCommand = new DelegateCommand(o => ActiveStep?.CanExecuteNext() ?? false, o => { if(ActiveStep.OnMoveNext()) ShowNextStep(); });
            PrevCommand = new DelegateCommand(o => ActiveStep?.CanExecutePrev() ?? false, o => { if(ActiveStep.OnMovePrev()) ShowPreviousStep(); });
            CancelCommand = new DelegateCommand(o => ActiveStep?.CanExecuteCancel() ?? false, o =>
            {
                DoCancel();
            });

            DataContextChanged += (sender, e) =>
            {
                if (ReferenceEquals(DataContext, this)) return;
                Model = DataContext as IWizardExModel;
                btnNext.DataContext = btnCancel.DataContext = btnFinish.DataContext = btnPrev.DataContext = this;
            };

            Loaded += (sender, e) => 
                ShowStep(Model.Step == 0 ? 0 : (Model.Step - 1));
        }

        private void ShowStepContent()
		{
		    bool needInit = false;
			var activeStepInfo = WizardSteps[ActiveStepIndex];

			if (!_steps.ContainsKey(activeStepInfo.Key))
			{
				if (activeStepInfo.Value.GetInterface("IWizardStepEx") == null)
					throw new InvalidCastException("Wizard step control must implement IWizardStep interface");

				var step = (IWizardStepEx)Activator.CreateInstance(activeStepInfo.Value);
			    ((FrameworkElement) step).DataContext = Model;
				_steps.Add(activeStepInfo.Key, step);
			    needInit = true;
			}

			ActiveStep = _steps[activeStepInfo.Key];
            ActiveStep.Wizard = this;

			dckStepContent.Children.Clear();
			dckStepContent.Children.Add((UIElement)ActiveStep);

			//((FrameworkElement)ActiveStep).DataContext = Model;
            if(needInit)
                ActiveStep.OnInitialization();

            using (Loading.StartNew("Загрузка данных"))
            {
				ActiveStep.OnShow();
			}

			EnsureControlState();
		}

		public void EnsureControlState()
		{
			if (ActiveStepIndex == WizardSteps.Count - 1 && ActiveStep.NextButtonState != WizardNavigationButtonState.Hidden)
				btnFinish.Visibility = Visibility.Visible;
			else
				btnFinish.Visibility = Visibility.Collapsed;

			btnFinish.IsEnabled = ActiveStep.NextButtonState != WizardNavigationButtonState.Disabled;
			btnNext.Visibility = !btnFinish.IsVisible && ActiveStep.NextButtonState != WizardNavigationButtonState.Hidden ? Visibility.Visible : Visibility.Collapsed;
			btnNext.IsEnabled = ActiveStep.NextButtonState != WizardNavigationButtonState.Disabled;
			btnPrev.Visibility = ActiveStep.PrevButtonState != WizardNavigationButtonState.Hidden ? Visibility.Visible : Visibility.Collapsed;
			btnPrev.IsEnabled = ActiveStepIndex != 0 && ActiveStep.PrevButtonState != WizardNavigationButtonState.Hidden;
			btnCancel.Visibility = ActiveStep.CancelButtonState != WizardNavigationButtonState.Hidden ? Visibility.Visible : Visibility.Collapsed;
			btnCancel.IsEnabled = ActiveStep.CancelButtonState != WizardNavigationButtonState.Disabled;
            btnNext.Content = ActiveStep.NextButtonText;
            btnCancel.Content = ActiveStep.CancelButtonText;
            btnFinish.Content = ActiveStep.FinishButtonText;
            btnPrev.Content = ActiveStep.PrevButtonText;
        }


        public void UpdateNavigationControlsState()
		{
			EnsureControlState();
		}


		public void SetDimensions(int width, int height, int maxWidth = 0, int maxHeight = 0)
		{
			MinWidth = width;
			MinHeight = height;

			MaxHeight = Math.Max(height, maxHeight);
			MaxWidth = Math.Max(width, maxWidth);
		}

		public void SetFreeDimensions(int width, int height, bool setMin = true)
		{
            if (setMin)
            {
                MinWidth = width;
                MinHeight = height;
            }

            MaxWidth = double.PositiveInfinity;
			MaxHeight = double.PositiveInfinity;
			HorizontalAlignment = HorizontalAlignment.Stretch;
		}


		public void ShowNextStep()
		{
			if (ActiveStepIndex == WizardSteps.Count - 1)
			{
				OnComplete();
				return;
			}

			ActiveStepIndex++;
			ShowStepContent();
		}

		public void ShowStep(int stepIndex)
		{
			if (stepIndex >= WizardSteps.Count)
				throw new IndexOutOfRangeException();

			ActiveStepIndex = stepIndex;
            ShowStepContent();
		}

		public void ShowPreviousStep()
		{
			if (ActiveStepIndex > 0)
			{
				ActiveStepIndex--;
				ShowStepContent();
			}
		}

		public void DoCancel()
		{
			OnCancel();
		}

		public void Dispose()
		{
		    _steps?.Clear();
		    WizardSteps.Clear();
		}

	    public void Close(bool isOk = true)
	    {
            if(isOk)
	            Model.Step = 0;
            DashboardManager.CloseActiveView();
            if(Model.IsClosed)
                Dispose();
        }
	}
}
