﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace PFR_INVEST.Views.Wizards
{
	public interface IWizard
	{
	    void EnsureControlState();

        List<KeyValuePair<string, Type>> WizardSteps { get; }

        void ShowNextStep();

		void ShowStep(int stepIndex);

		void ShowPreviousStep();

		void DoCancel();

		void UpdateNavigationControlsState();

		void SetDimensions(int width, int height, int maxWidth = 0, int maxHeight = 0);

		event RoutedEventHandler Complete;
		event RoutedEventHandler Cancel;

        void SetFreeDimensions(int width, int height, bool setMin = true);
	}
}
