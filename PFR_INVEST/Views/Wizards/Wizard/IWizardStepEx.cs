﻿namespace PFR_INVEST.Views.Wizards
{
	public interface IWizardStepEx
    {
		IWizard Wizard { get; set; }
        /// <summary>
        /// Вызывается при успешном переходе на следующий шаг, до самого перехода. При возврате false мастер не переходит на след шаг.
        /// </summary>
        bool OnMoveNext();
        /// <summary>
        /// Вызывается при успешном возврате на предыдущий шаг, до самого перехода. При возврате false мастер не переходит на след шаг.
        /// </summary>
		bool OnMovePrev();
        /// <summary>
        /// Вызывается каждый раз при отображении шага
        /// </summary>
        void OnShow();
        /// <summary>
        /// Вызывается один раз при создании шага
        /// </summary>
        void OnInitialization();
        /// <summary>
        /// Вызывается для проверки доступности кнопки Отмена
        /// </summary>
	    bool CanExecuteCancel();
        /// <summary>
        /// Вызывается для проверки доступности кнопки Далее
        /// </summary>
        bool CanExecuteNext();
        /// <summary>
        /// Вызывается для проверки доступности кнопки Назад
        /// </summary>
        bool CanExecutePrev();

        WizardNavigationButtonState NextButtonState { get; }
        WizardNavigationButtonState PrevButtonState { get; }
        WizardNavigationButtonState CancelButtonState { get; }

        string NextButtonText { get; }
        string FinishButtonText { get; }
        string PrevButtonText { get; }
        string CancelButtonText { get; }

    }
}
