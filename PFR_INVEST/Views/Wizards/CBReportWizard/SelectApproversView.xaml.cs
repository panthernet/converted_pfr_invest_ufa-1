﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.CBReport;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Wizards.CBReportWizard
{
    /// <summary>
    /// Interaction logic for SelectApproversView.xaml
    /// </summary>
    public partial class SelectApproversView : IWizardStep
    { 

        private CBReportWizardWindowViewModel Model => DataContext as CBReportWizardWindowViewModel;
        public IWizard Wizard { get; set; }


        public SelectApproversView()
        {
            InitializeComponent();
            DataContextChanged += (sender, args) =>
                Model.InitializeSelectApprovers();
        }

        public bool OnMoveNext()
        {
            bool result = false;
            switch (Model.SelectedReportType)
            {
                case BOReportForm1.ReportTypeEnum.All:
                    result = Model.SelectedUser1 != null && Model.SelectedUser2NPF != null && Model.SelectedUser2UK != null && Model.SelectedUser3 != null;
                    break;
                case BOReportForm1.ReportTypeEnum.One:
                    result = Model.SelectedUser1 != null;
                    break;
                case BOReportForm1.ReportTypeEnum.Two:
                    result = Model.SelectedUser2NPF != null && Model.SelectedUser2UK != null;
                    break;
                case BOReportForm1.ReportTypeEnum.Three:
                    result = Model.SelectedUser3 != null;
                    break;
            }
            if(!result)
                ViewModelBase.DialogHelper.ShowWarning("Для продолжения необходимо указать всех исполнителей!");
            return result;
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            switch (Model.SelectedReportType)
            {
                case BOReportForm1.ReportTypeEnum.One:
                    app2NPF.Visibility = app2UK.Visibility = app3.Visibility = Visibility.Collapsed;
                    app1.Visibility = Visibility.Visible;
                    break;
                case BOReportForm1.ReportTypeEnum.Two:
                    app1.Visibility = app3.Visibility = Visibility.Collapsed;
                    app2NPF.Visibility = app2UK.Visibility = Visibility.Visible;
                    break;
                case BOReportForm1.ReportTypeEnum.Three:
                    app2NPF.Visibility = app2UK.Visibility = app1.Visibility = Visibility.Collapsed;
                    app3.Visibility = Visibility.Visible;
                    break;
                default:
                    app1.Visibility = app2NPF.Visibility = app2UK.Visibility = app3.Visibility = Visibility.Visible;
                    break;
            }
            Model.UpdateReportString();
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;
        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;
        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;
    }
}
