﻿using System;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.MonthTransferCreationWizard
{
	/// <summary>
	/// Interaction logic for UKFactSumsEditView.xaml
	/// </summary>
	public partial class UKFactSumsEditView : IWizardStep
	{
		public UKFactSumsEditView()
		{
			InitializeComponent();
			//txtSumm.EditValue = 0;
			view.CellValueChanged += view_CellValueChanged;
		}

		private void view_CellValueChanged(object sender, DevExpress.Xpf.Grid.CellValueChangedEventArgs e)
		{
			grdCC.UpdateTotalSummary();
			//txtSumm.EditValue = grdCC.GetTotalSummaryValue(grdCC.TotalSummary[0]);
		}

		public MonthTransferCreationWizardModel Model => DataContext as MonthTransferCreationWizardModel;

	    public IWizard Wizard
		{
			get;
			set;
		}

		public bool OnMoveNext()
		{
			if (Model.AvailableContractsList.Any(acl => acl.FactSum > 0))
			{
				CreateObject();
				Model.RegisterCreated = true;
				return true;
			}
			else
			{
				DXMessageBox.Show("Платежное поручение составлено неверно.\nСумма всех перечислений должна быть больше нуля.", "Операция отменена.", MessageBoxButton.OK, MessageBoxImage.Warning);
				return false;
			}
		}

		public bool OnMovePrev()
		{
			return true;
		}

		public void OnShow()
		{
			bool shouldRequest = true;

			while (shouldRequest)
			{
				try
				{
					//Model.EnsureAvailableList();

					Model.EnsureAvailableList();
					if (!Model.AvailableContractsList.Any())
					{
						Wizard.ShowPreviousStep();
					    DXMessageBox.Show(
					        Model.SelectedMonth != null
					            ? "Невозможно создать перечисление на месяц. На указанный период действующих договоров не обнаружено"
					            : "Невозможно создать годовой план. На указанный период действующих договоров не обнаружено", "Операция отменена.", MessageBoxButton.OK, MessageBoxImage.Warning);

					    return;
					}

					grdCC.RefreshData();
					shouldRequest = false;
				}
				catch (Exception ex)
				{
					App.log.WriteException(ex);
					var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
					shouldRequest = res == MessageBoxResult.Yes;
					if (!shouldRequest)
						Wizard.DoCancel();
				}
			}
			//Wizard.SetDimensions(700, 400);
		}

		public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

	    public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;

	    public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

	    private void CreateObject()
		{
			bool shouldRequest = true;

			while (shouldRequest)
			{
				try
				{
					Model.CreateRegister();
					shouldRequest = false;
				}
				catch (Exception ex)
				{
					App.log.WriteException(ex);
					var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
					shouldRequest = res == MessageBoxResult.Yes;
					if (!shouldRequest)
						Wizard.DoCancel();
				}
			}
		}
	}
}