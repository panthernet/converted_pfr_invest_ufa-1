﻿using System.Windows.Media;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.MonthTransferCreationWizard
{
    /// <summary>
    /// Interaction logic for FinishScreenView.xaml
    /// </summary>
    public partial class FinishScreenView : IWizardStep
    {
        private const string INFO_SUCCESS = "Создание перечислений прошло успешно.";

        public FinishScreenView()
        {
            InitializeComponent();
        }

        public IWizard Wizard
        {
            get;
            set;
        }

        public MonthTransferCreationWizardModel Model => DataContext as MonthTransferCreationWizardModel;

        public bool OnMoveNext()
        {
            return true;
        }

        public bool OnMovePrev()
        {
            return false;
        }

        public void OnShow()
        {
           // PrintRequests();
            txtInfo.Foreground = Brushes.Black;
            txtInfo.Content = INFO_SUCCESS;
            _printed = true;
            txtRegister.EditValue = Model.RegisterID;
            Wizard.UpdateNavigationControlsState();
            //Wizard.SetDimensions(500, 290);
        }

        public WizardNavigationButtonState NextButtonState => _printed ? WizardNavigationButtonState.Visible : WizardNavigationButtonState.Disabled;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Hidden;

        private void OpenSITransfersListView()
        {
            if (Model.Type == Document.Types.VR)
            {
                var vm = (TransfersVRListViewModel)App.DashboardManager.FindViewModel(typeof(TransfersVRListViewModel));
                if (vm != null)
                {
                    App.DashboardManager.RefreshListViewModels(new[] { typeof(TransfersVRListViewModel) });
                    vm.RaiseSelectGridRow(Model.RegisterID);
                }
                else
                {
                    App.DashboardManager.OpenNewTab(typeof(TransfersVRListView), ViewModelState.Read, Model.RegisterID, "Перечисления ВР");
                }
            }
            else if (Model.Type == Document.Types.SI)
            {
                var vm = (TransfersSIListViewModel)App.DashboardManager.FindViewModel(typeof(TransfersSIListViewModel));
                if (vm != null)
                {
                    App.DashboardManager.RefreshListViewModels(new[] { typeof(TransfersSIListViewModel) });
                    vm.RaiseSelectGridRow(Model.RegisterID);
                }
                else
                {
                    App.DashboardManager.OpenNewTab(typeof(TransfersSIListView), ViewModelState.Read, Model.RegisterID, "Перечисления СИ");
                }

            }
            else
            {
                var vm = (TransfersListViewModel)App.DashboardManager.FindViewModel(typeof(TransfersListViewModel));
                if (vm != null)
                {
                    App.DashboardManager.RefreshListViewModels(new[] { typeof(TransfersListViewModel) });
                    vm.RaiseSelectGridRow(Model.RegisterID);
                }
                else
                {
                    App.DashboardManager.OpenNewTab(typeof(TransfersListView), ViewModelState.Read, Model.RegisterID, "Перечисления");
                }
            }

        }

        private void RefreshAssignPaymentsListViews()
        {
            var viewModelTypesToRefresh = Model.GetRelatedAssignPaymentListViewModelTypes();
            App.DashboardManager.RefreshListViewModels(viewModelTypesToRefresh);
        }

        public void OpenCreated()
        {
            if (Model.RegisterID > 0)
            {
                OpenSITransfersListView();
                RefreshAssignPaymentsListViews();
            }
        }

        private bool _printed;

      /*  private void PrintRequests()
        {
            bool shouldRequest = true;

            while (shouldRequest)
            {
                try
                {
                    Loading.ShowWindow();
                    var res = Model.PrintRequests();
                    Loading.CloseWindow();
                    string message = string.Empty;
                    switch (res)
                    {
                        case MonthTransferCreationWizardModel.PrintResult.CannotCreateDirectory:
                            message = "Невозможно создать папку для выгрузки.\n" +
                                      "Возможно у вас нет прав для записи в выбранную папку.\n" +
                                      "Выбрать другую папку?";
                            break;
                        case MonthTransferCreationWizardModel.PrintResult.CannotCreateFile:
                            message = "Невозможно создать файл с требованием.\n" +
                                      "Возможно у вас нет прав для записи в выбранную папку.\n" +
                                      "Выбрать другую папку?";
                            break;
                        case MonthTransferCreationWizardModel.PrintResult.TemplateNotFound:
                            message = string.Format("Файл с шаблоном \"{0}\" для выгрузки не найден.\n" +
                                      "Загрузите файл шаблона и попробуйте еще раз.\n" +
                                      "Вернуться на предыдущую страницу мастера?", Model.errorItem);
                            break;
                        case MonthTransferCreationWizardModel.PrintResult.OK:
                        default:
                            break;
                    }

                    if (res != MonthTransferCreationWizardModel.PrintResult.OK)
                    {
                        Loading.CloseWindow();
                        txtInfo.Content = "Произошла ошибка при выгрузке требований.";
                        txtInfo.Foreground = Brushes.Red;
                        if (DXMessageBox.Show(message, "Ошибка при выгрузке требований", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            Wizard.ShowPreviousStep();
                    }

                    shouldRequest = false;
                }
                catch (Exception ex)
                {
                    App.log.WriteException(ex);
                    var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    shouldRequest = res == MessageBoxResult.Yes;
                    if (!shouldRequest)
                        Wizard.DoCancel();
                }

                var msg = string.Format("Требования за {0} {1} г. сформированы.", Model.SelectedMonth.Name.Trim(), Model.SelectedYear.Name.Trim());
                JournalLogger.LogChangeStateEvent(msg);
            }
        }*/
    }
}
