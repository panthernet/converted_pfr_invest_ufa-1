﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ArchiveDepositsListView.xaml
    /// </summary>
    public partial class DepositsArchiveListView
    {
        public DepositsArchiveListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, listGrid);
            listGrid.CustomColumnSort += (sender, e) =>
            {
                e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
                e.Handled = true;
            };
            tableView.ShowFilterPopup += tableView_ShowFilterPopup;
        }

        void tableView_ShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName == "Month")
            {
                var firstItem = (CustomComboBoxItem) ((List<object>) e.ComboBoxEdit.ItemsSource).First();
                var filterItems = new List<CustomComboBoxItem> {firstItem};
                ComponentHelper.GetMonthList().ForEach(a => filterItems.Add(new CustomComboBoxItem {DisplayValue = a, EditValue = a}));
                e.ComboBoxEdit.ItemsSource = filterItems;
            }
            else if (e.Column.FieldName == "Day")
            {
                e.ComboBoxEdit.ItemsSource = ((List<object>)e.ComboBoxEdit.ItemsSource).OrderBy(a =>
                {
                    DateTime dt;
                    return DateTime.TryParse(((CustomComboBoxItem) a).EditValue as string, out dt) ? dt.Date : DateTime.MinValue;
                }).ToList();
            }
        }

        private void ListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (listGrid.View.FocusedRowHandle >= 0)
                App.DashboardManager.OpenNewTab(typeof(DepositView), ViewModelState.Edit,
                    GetSelectedID(), "Депозит");
        }

        public long GetSelectedID()
        {
            if (listGrid.View.FocusedRowHandle <= -10000)
                return 0;
            if (listGrid.View.FocusedRowHandle < 0)
                return 0;
            try
            {
                return (long)listGrid.GetCellValue(listGrid.View.FocusedRowHandle, "ID");
            }
            catch
            {
                return 0;
            }
        }

        public long GetSelectedPortfolioID()
        {
            if (listGrid.View.FocusedRowHandle <= -10000)
                return 0;
            try
            {
                return (long)listGrid.GetCellValue(listGrid.View.FocusedRowHandle, "PortfolioID");
            }
            catch
            {
                return 0;
            }
        }
    }
}
