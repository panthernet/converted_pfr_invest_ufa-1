﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Views
{
    public class CorrespondenceVRArchiveListView : CorrespondenceArchiveListView
    {
        protected override void OpenItem(CorrespondenceListItemNew item)
        {
            if (item.IsAttach)
                App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentVRViewModel>(CorrespondenceListItem.s_AttachDoc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
            else
                App.DashboardManager.OpenNewTab<DocumentVRView, DocumentVRViewModel>(CorrespondenceListItem.s_Doc, ViewModelState.Edit, item.ID, ViewModelState.Edit);
        }
    }
}
