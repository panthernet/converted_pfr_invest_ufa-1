﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DemandListView.xaml
    /// </summary>
    public partial class DemandListView
    {
        public DemandListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, DemandGrid, "List");
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DemandGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (IsDemandSelected())
            {
                long lID = GetSelectedDemandID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(DemandView), ViewModelState.Edit, lID, "Требование на оплату");
            }
        }

        private bool IsDemandSelected()
        {
            return DemandGrid.View.FocusedRowData.Level >= DemandGrid.GetGroupedColumns().Count && DemandGrid.SelectedItem != null;
        }

        public long GetSelectedDemandID()
        {
            return Convert.ToInt64(DemandGrid.GetCellValue(DemandGrid.View.FocusedRowHandle, "ID"));
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
    }
}
