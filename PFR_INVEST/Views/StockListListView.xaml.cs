﻿using System;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ContragentsList.xaml
    /// </summary>
    public partial class StockListView: IModelBindable
    {
        public Type ModelType => typeof(StockListViewModel);

        public StockListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

            if (Grid.View.FocusedRowHandle >= 0)
            {
                var value = Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID");

                App.DashboardManager.OpenNewTab(typeof(StockView),
                                                ViewModelState.Edit,
                                                (long)
                                                value, "Биржа");
            }
        }

    }
}
