﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using DevExpress.Data;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for FinRegistersListView.xaml
    /// </summary>
    public partial class RegistersArchiveListView
    {
        public RegistersArchiveListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, registersListGrid);
            ModelInteractionHelper.SignUpForCustomAction(this, o =>
            {
                var isFr = (bool)((object[])o)[0];
                var id = (long)((object[])o)[1];
                SelectRow(id, isFr);
            });
        }

        private void SelectRow(long id, bool isFinregister = true)
        {
            try
            {
                var item = Model.List.FirstOrDefault(a => (isFinregister ? a.ID : a.RegisterID) == id);
                if (item == null) return;
                registersListGrid.ExpandFocusRow(registersListGrid.GetRowHandleByListIndex(Model.List.IndexOf(item)));
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex);
            }
        }

        public RegistersArchiveListViewModel Model => DataContext as RegistersArchiveListViewModel;

        private void grid_CustomSummaryExists(object sender, CustomSummaryExistEventArgs e)
        {
            e.Exists = e.GroupLevel == 2;
        }

        public long GetCurrFinRegID()
        {
            return Convert.ToInt64(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "ID"));
        }

        public bool CanCreateRequest()
        {
            if (!IsRegisterSelected()) return false;
            try
            {
                var regList = new List<RegistersListItem>(from RegistersListItem rli in Model.List
                    where rli.RegisterID == (long)registersListGrid.GetFocusedRowCellValue("RegisterID")
                    select rli);
                return regList.All(item => item != null && item.Count > 0);
            }
            catch
            {
                return false;
            }
        }

        public string GetCurrFinregRegisterKind()
        {
            return Convert.ToString(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "RegisterKind"));
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (registersListGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (IsRegisterSelected())
            {
                long lID = GetSelectedRegisterID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(RegisterView), ViewModelState.Read, lID, "Реестр");
            }
            else if (IsFinRegisterSelected())
            {
                long lID = GetSelectedID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(FinRegisterView), ViewModelState.Read, lID, "Финреестр");
            }
        }

        public long GetSelectedRegisterID()
        {
            return Convert.ToInt64(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "RegisterID"));
        }

        public long GetSelectedID()
        {
            return Convert.ToInt64(registersListGrid.GetCellValue(registersListGrid.View.FocusedRowHandle, "ID"));
        }

        public RegistersListItem SelectedFinRegister
        {
            get
            {
                var item = registersListGrid.GetFocusedRow() as RegistersListItem;
                return item?.RegisterKind != null && IsFinRegisterSelected() ? item : null;
            }
        }

        public bool IsRegisterSelected()
        {
            int lvl = registersListGrid.View.FocusedRowData.Level;
            if (lvl < registersListGrid.GetGroupedColumns().Count)
                return registersListGrid.GetGroupedColumns()[lvl].FieldName == "RegisterID";
            return false;
        }

        public bool IsFinRegisterSelected()
        {
            int lvl = registersListGrid.View.FocusedRowData.Level;
            return lvl == registersListGrid.GetGroupedColumns().Count;
        }

        private void registersListGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "{0} \tдокумент: {1}\r\n\t№ {2} от: {3}";
            if (e.Column.FieldName != "RegisterID") return;

            var currRow = (e.Row as RegistersListItem);

            if (currRow != null)
            {
                e.DisplayText = string.Format(dispText, 
                currRow.RegisterID > 0 ? currRow.RegisterID.ToString() : string.Empty,
                currRow.ApproveDocName ?? string.Empty,
                currRow.RegNum?.Trim() ?? string.Empty,
                currRow.RegDate?.ToShortDateString() ?? string.Empty);
            }
        }
    }
}
