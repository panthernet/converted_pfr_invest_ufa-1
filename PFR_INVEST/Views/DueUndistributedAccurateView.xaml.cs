﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DueUndistributedAccurateView.xaml
    /// </summary>
    public partial class DueUndistributedAccurateView : UserControl
    {
        public DueUndistributedAccurateView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
        }
    }
}
