﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using System.Windows;

namespace PFR_INVEST.Views
{
    public partial class ContractsGridView : UserControl
    {
        public static readonly DependencyProperty IsSIProperty =
            DependencyProperty.Register("IsSI", typeof(bool), typeof(ContractsGridView), new UIPropertyMetadata(false));

        private bool _mGridRowMouseDoubleClicked;

        public ContractsGridView()
        {
            InitializeComponent();
        }

        public bool IsSI
        {
            get { return (bool)GetValue(IsSIProperty); }
            set { SetValue(IsSIProperty, value); }
        }

        private void SIContractsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ItemsGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (IsContractSelected())
            {
                _mGridRowMouseDoubleClicked = true;
                long lID = GetSelectedContractID();
                if (lID > 0)
                {
                    App.DashboardManager.OpenNewTab(typeof(SIContractView), ViewModelState.Edit, lID, IsSI ? "Договор СИ" : "Договор ВР");
                }
            }
            else if (IsSupAgreementSelected())
            {
                long lID = GetSelectedSupAgreementID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(SupAgreementView), ViewModelState.Edit, lID, "Доп. соглашение");
            }
            else if (IsSISelected())
            {
                _mGridRowMouseDoubleClicked = true;
                long LegalEntityID = GetSelectedLegalEntityID();
                if (LegalEntityID > 0)
                    App.DashboardManager.OpenNewTab(typeof(SIView), ViewModelState.Edit, LegalEntityID, "Субъект");
            }
        }

        public bool IsContractSelected()
        {
            int lvl = ItemsGrid.View.FocusedRowData.Level;
            if (lvl < ItemsGrid.GetGroupedColumns().Count)
                return (ItemsGrid.GetGroupedColumns()[lvl].FieldName == "ContractPortfolioNumber");
            return false;
        }

        private bool IsSISelected()
        {
            int level = ItemsGrid.View.FocusedRowData.Level;
            return (ItemsGrid.GetGroupedColumns()[level].FieldName == "SIName");
        }

        public bool IsSupAgreementSelected()
        {
            return ItemsGrid.View.FocusedRowData.Level >= ItemsGrid.GetGroupedColumns().Count && ItemsGrid.SelectedItem != null;
        }

        public long GetSelectedContractID()
        {
            return Convert.ToInt64(ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "ContractID"));
        }

        public long GetSelectedLegalEntityID()
        {
            return Convert.ToInt64(ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "LegalEntityID"));
        }

        public long GetSelectedSupAgreementID()
        {
            return Convert.ToInt64(ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "ID"));
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }

        public string GetSelectedContractName()
        {
            if (IsContractSelected() && GetSelectedContractID() > 0)
            {
                return (string)ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "SIName");
            }
            return string.Empty;
        }

        public bool CanRestoreCO()
        {
            if (IsContractSelected() && GetSelectedContractID() > 0)
            {
                if (ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "DissolutionDate") != null && (DateTime)ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "DissolutionDate") != DateTime.MinValue)
                    return true;
            }
            return false;
        }

        public bool CanStopCO()
        {
            if (IsContractSelected() && GetSelectedContractID() > 0)
            {
                if (ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "DissolutionDate") == null || (DateTime)ItemsGrid.GetCellValue(ItemsGrid.View.FocusedRowHandle, "DissolutionDate") == DateTime.MinValue)
                    return true;
            }
            return false;
        }
    }
}
