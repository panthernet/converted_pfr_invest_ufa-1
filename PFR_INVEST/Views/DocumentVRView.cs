﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;

namespace PFR_INVEST.Views
{
    public class DocumentVRView : DocumentBaseView
    {
        protected override void OpenLinkedItem(LinkedDocumentItem item)
        {
            App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentVRViewModel>("Связанный документ", ViewModelState.Edit, item.ID, ViewModelState.Edit);
        }

        protected override void AddLinkedItem()
        {
            App.DashboardManager.OpenNewTab<LinkedDocumentView, LinkedDocumentVRViewModel>("Связанный документ", ViewModelState.Create, Model.ID, ViewModelState.Create);
        }
    }
}
