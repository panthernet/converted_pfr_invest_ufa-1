﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using BusinessLogic.ViewModelsList;

    public partial class TransfersListView
    {
        private bool _mGridRowMouseDoubleClicked;

        private TransfersListViewModel Model
        {
            get
            {
                _isModelSI = DataContext is TransfersSIListViewModel;
                return DataContext as TransfersListViewModel;
            }
        }

        private bool _isModelSI;
        public long InitialSelectedRid;

        public TransfersListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void Grid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "{0} \tдокумент: {1}     \r\n\t№ {2} от: {3}";

            if (!Grid.IsValidRowHandle(e.RowHandle))
                return;

            if (e.Column.FieldName != "RegisterID")
                return;

            var currRow = e.Row as TransferListItem;

            if (currRow != null)
            {
                e.DisplayText = string.Format(dispText,
                    currRow.RegisterID,
                    currRow.Kind,
                    currRow.RegisterNumber,
                    currRow.RegisterDate?.ToShortDateString() ?? string.Empty
                );
            }
            else
                e.DisplayText = string.Empty;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

            if (IsRegisterSelected())
            {
                _mGridRowMouseDoubleClicked = true;
                var id = GetSelectedRegisterID();
                if (id <= 0) return;
                if (_isModelSI)
                    App.DashboardManager.OpenNewTab(
                        typeof(SIVRRegisterSIView), ViewModelState.Edit, id, "Реестр перечислений СИ");
                else
                    App.DashboardManager.OpenNewTab(
                        typeof(SIVRRegisterVRView), ViewModelState.Edit, id, "Реестр перечислений ВР");
            }
            else if (IsTransferSelected())
            {
                _mGridRowMouseDoubleClicked = true;
                var id = GetSelectedTransferID();
                if (id <= 0)
                    return;
                if (_isModelSI)
                    App.DashboardManager.OpenNewTab(typeof(TransferSIView), ViewModelState.Edit, id, "Список перечислений СИ");
                else
                    App.DashboardManager.OpenNewTab(typeof(TransferVRView), ViewModelState.Edit, id, "Список перечислений ВР");
            }
            else if (IsReqTransferSelected())
            {
                _mGridRowMouseDoubleClicked = true;
                var id = GetSelectedReqTransferID();
                if (id > 0)
                    App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Edit, id, "Перечисление");
            }
        }

        public long GetNumberOfInitialSateTransfersInSelectedRegister()
        {
            if (!IsRegisterSelected())
                return 0;

            var regID = GetSelectedRegisterID();
            if (regID <= 0)
                return 0;

            var list = Model.List;

            return list.Count(tr => tr.RegisterID == regID && TransferStatusIdentifier.IsStatusInitialState(tr.Status));
        }

        public long GetNumberOfReceivedByUKTransfersInSelectedRegister()
        {
            var l = GetReceivedByUKTransfersInSelectedRegister();
            return l?.Count ?? 0;
        }

        public List<TransferListItem> GetTransfersInSelectedRegister()
        {
            if (!IsRegisterSelected())
                return null;

            var regID = GetSelectedRegisterID();
            if (regID <= 0)
                return null;

			var list = Model.List;

            return list.Where(tr => tr.RegisterID == regID).ToList();
        }

        public List<TransferListItem> GetReceivedByUKTransfersInSelectedRegister()
        {
            if (!IsRegisterSelected())
                return null;

            var regID = GetSelectedRegisterID();
            if (regID <= 0)
                return null;

			var list = Model.List;

            return list.Where(tr => tr.RegisterID == regID && TransferStatusIdentifier.IsStatusRequestReceivedByUK(tr.Status)).ToList();
        }

        public bool SelectedRegisterHasInitialSateTransfers()
        {
            return GetNumberOfInitialSateTransfersInSelectedRegister() > 0;
        }

        public bool SelectedRegisterHasReceivedByUKTransfers()
        {
            if (!IsRegisterSelected())
                return false;

            var regID = GetSelectedRegisterID();
            if (regID <= 0)
                return false;

			var list = Model.List;
            var regItems = list.Where(tr => tr.RegisterID == regID);
            return regItems.Any(tr => TransferStatusIdentifier.IsStatusRequestReceivedByUK(tr.Status));
        }

        public bool IsRegisterSelected()
        {
            var lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return Grid.GetGroupedColumns()[lvl].FieldName == "RegisterID";
            return false;
        }

        public bool IsTransferSelected()
        {
            var lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return Grid.GetGroupedColumns()[lvl].FieldName == "ContractNumber";
            return false;
        }

        public bool IsStatusSelected()
        {
            var lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return Grid.GetGroupedColumns()[lvl].FieldName == "Status";
            return false;
        }

        public bool IsReqTransferSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        public long GetSelectedTransferID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "TransferID"));
        }

        public long GetSelectedReqTransferID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ReqTransferID"));
        }

        public long GetSelectedRegisterID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "RegisterID"));
        }

        public string GetSelectedRegisterNumber()
        {
            return Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "RegisterNumber"));
        }

        public string GetSelectedStatus()
        {
            return Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "Status"));
        }

        public List<string> GetSelectedStatuses()
        {
            if (!IsRegisterSelected())
                return new List<string> {GetSelectedStatus()};
            var regId = GetSelectedRegisterID();
            var transferList = Model.List.Where(x => x.RegisterID == regId);
            return transferList.Select(x => x.Status).ToList();
        }

        public string GetSelectedDirection()
        {
            return Convert.ToString(Grid.GetCellValue(Grid.View.FocusedRowHandle, "Direction"));
        }

        public List<long> GetReqTransferIDBySelectedRegisterID(string status)
        {
			return Model.List.Where(tr =>
                tr.RegisterID == GetSelectedRegisterID() &&
                string.Equals(tr.Status, status, StringComparison.OrdinalIgnoreCase))
                .Select(tr => tr.ReqTransferID).ToList();
        }

        private void Model_OnSelectGridRowDelegate(long RID)
        {
            if (RID == 0 || Grid.VisibleRowCount == 0)
                return;

            try
            {
                var rowHandle = Grid.View.FocusedRowHandle + 1;

                while (Convert.ToDouble(Grid.GetCellValue(rowHandle, "RegisterID")) != RID &&
                    Grid.IsValidRowHandle(rowHandle))
                {
                    rowHandle++;
                }

                var handles = new List<int>();

                while (Grid.IsValidRowHandle(rowHandle))
                {
                    handles.Add(rowHandle);
                    rowHandle = Grid.GetParentRowHandle(rowHandle);
                }

                if (handles.Count > 2)
                {
                    Grid.ExpandGroupRow(handles[handles.Count - 1]);
                    Grid.ExpandGroupRow(handles[handles.Count - 2]);
                    Grid.ExpandGroupRow(handles[handles.Count - 3]);
                    Grid.View.FocusedRowHandle = handles[handles.Count - 3];
                }
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex);
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Model.OnSelectGridRow += Model_OnSelectGridRowDelegate;
            Model_OnSelectGridRowDelegate(InitialSelectedRid);
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }
    }

    public class ExecutionControlToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((TransferListItem.ExecutionControlType)value)
            {
                case TransferListItem.ExecutionControlType.ExecutionControlTypeExecuted:
                    return null;
                case TransferListItem.ExecutionControlType.ExecutionControlTypeNone:
                    return null;
                case TransferListItem.ExecutionControlType.ExecutionControlTypeNotExecuted:
                    return new BitmapImage(new Uri("/Images/redlight.png", UriKind.Relative));
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
