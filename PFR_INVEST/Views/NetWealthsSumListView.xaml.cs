﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using System.Collections.Generic;
using DevExpress.Xpf.Grid;
using System;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
	/// Interaction logic for NetWealthsSumListView.xaml
	/// </summary>
	public partial class NetWealthsSumListView
	{
		public NetWealthsSumListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

		private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
			var table = GetSelectedTable();
			var selectedID = GetSelectedID();

			switch (table)
			{
				case "EDO_ODKF010":
					App.DashboardManager.OpenNewTab(typeof(F10View),
						ViewModelState.Read, selectedID, "Сведения о стоимости чистых активов");
					break;
				case "EDO_ODKF012":
					App.DashboardManager.OpenNewTab(typeof(F12View), ViewModelState.Read, selectedID, "Расчет стоимости чистых активов, в которые инвестированы средства пенсионных накоплений");
					break;
				case "EDO_ODKF014":
					App.DashboardManager.OpenNewTab(typeof(F14View),
						ViewModelState.Read, selectedID, "Сведения о стоимости чистых активов");
					break;
				case "EDO_ODKF015":
					App.DashboardManager.OpenNewTab(typeof(F15View), ViewModelState.Read, selectedID, "Расчет стоимости чистых активов, в которые инвестированы средства пенсионных накоплений (по портфелю УК)");
					break;
				case "EDO_ODKF016":
					App.DashboardManager.OpenNewTab(typeof(F16View),
						ViewModelState.Read, selectedID, "Расчет стоимости чистых активов, в которые инвестированы средства пенсионных накоплений (по учредителю управления)");
					break;
			}
		}

		public long GetSelectedID()
		{
			if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
				return (long)Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID");
			return 0;
		}

		public string GetSelectedTable()
		{
			if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
				return (string)Grid.GetCellValue(Grid.View.FocusedRowHandle, "Table");
			return "";
		}

		protected void GridViewSort(object sender, CustomColumnSortEventArgs e)
		{
			try
			{
			    if (e.Column.FieldName != "ReportOnDateYear")
                    return;
			    e.Result = Comparer<int?>.Default.Compare((int)e.Value1, (int)e.Value2);
			    //Сортируем по дате для обхода бага грида со сбросом сортировки
			    if (e.Result == 0)
			    {
			        var source = (Grid.ItemsSource as List<NetWealthListItem>);
			        var dr1 = source?[e.ListSourceRowIndex1];
			        var dr2 = source?[e.ListSourceRowIndex2];
			        e.Result = Comparer<DateTime?>.Default.Compare(dr1?.ReportOnDate, dr2?.ReportOnDate);
			    }
			    e.Handled = true;
			}
			catch (Exception)
			{
				e.Handled = false;
			}
		}

		private void NetWealthsSumListViewShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			if (e.Column.FieldName != "ReportOnDate") return;
			var source = (Grid.ItemsSource as List<NetWealthListItem>);

			foreach (var o in (List<object>) e.ComboBoxEdit.ItemsSource)
			{
				var item = o as CustomComboBoxItem;
			    var editvalue = item?.EditValue as DateTime?;
			    if (editvalue == null)
                    continue;
			    var si = source?.Find(x => x.ReportOnDate.HasValue && x.ReportOnDate.Value.Equals(editvalue.Value));
			    item.DisplayValue = si?.ReportOnDateDate;
			}
		}

		private void NetWealthsSumListViewDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
		    if (e.Column.FieldName != "ReportOnDate")
                return;
		    var item = e.Row as NetWealthListItem;
		    if (item != null)
		        e.DisplayText = item.ReportOnDateDate;
		}
	}
}
