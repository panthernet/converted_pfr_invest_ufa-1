﻿using System.Windows.Controls;
using PFR_INVEST.BusinessLogic;
using System.Windows;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for IncomeSecurityView.xaml
    /// </summary>
    public partial class IncomeSecurityView : UserControl
    {
        public IncomeSecurityView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private IncomeSecurityViewModel Model => DataContext as IncomeSecurityViewModel;

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            Model?.GetCourceManuallyOrFromCBRSite();
        }
    }
}
