﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DevExpress.Xpf.Grid;
    using DataObjects;

    /// <summary>
    /// Interaction logic for DepclaimMaxListView.xaml
    /// </summary>
    public partial class DepClaim2ListView
    {
        public DepClaim2ListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            tableView.FocusedRowHandleChanged += tableView_FocusedRowHandleChanged;
        }

        private void tableView_FocusedRowHandleChanged(object sender, FocusedRowHandleChangedEventArgs e)
        {
            var row = tableView.FocusedRowData.Row as DepClaim2;
            if (row != null)
            {
                SelectedAuctionIDHelper.Value = row.AuctionID;
                SelectedAuctionAnyClaimHelper.Value = row;
            }
            else
            {
                SelectedAuctionIDHelper.Value = null;
                SelectedAuctionAnyClaimHelper.Value = null;
            }
        }
    }
}
