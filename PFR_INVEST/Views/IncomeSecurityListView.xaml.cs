﻿using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for IncomeSecurityListView.xaml
    /// </summary>
    public partial class IncomeSecurityListView
    {
        private bool _mGridRowMouseDoubleClicked;

        public IncomeSecurityListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, IncomeSecsListGrid);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        protected void IncomeSecListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (IncomeSecsListGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (!IsIncomeSecSelected()) return;
            var item = ((DataObjects.ListItems.IncomeSecurityListItem)(((RowData)(IncomeSecsListGrid.View.GetRowElementByMouseEventArgs(e).DataContext)).Row));

            long id = item.ID;
            _mGridRowMouseDoubleClicked = true;
            if (id > 0)
            {
                App.DashboardManager.OpenNewTab(typeof(IncomeSecurityView), ViewModelState.Edit, id, "Поступления по временному размещению");

                var modelIs = App.DashboardManager.GetActiveViewModel() as IncomeSecurityViewModel;
                if (modelIs != null)
                    modelIs.IsDataChanged = modelIs.IsDataChanged || modelIs.Rate == 0;
            }
            else
                if (item.PaymentHistoryID > 0)//PaymentHistoryView
                {
                    App.DashboardManager.OpenNewTab(typeof(PaymentHistoryView), ViewModelState.Edit, item.PaymentHistoryID.Value, "Поступления по временному размещению");

                    var modelIs = App.DashboardManager.GetActiveViewModel() as PaymentHistoryViewModel;
                    if (modelIs != null)
                        modelIs.IsDataChanged = modelIs.IsDataChanged || modelIs.Rate == 0;
                }
        }

        private bool IsIncomeSecSelected()
        {
            return IncomeSecsListGrid.View.FocusedRowData.Level >= IncomeSecsListGrid.GetGroupedColumns().Count && IncomeSecsListGrid.SelectedItem != null;
        }

        private void IncomeSecsListGrid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }

		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
	}
}
