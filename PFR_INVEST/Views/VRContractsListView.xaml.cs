﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class VRContractsListView : IContractsListView
    {
        public VRContractsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ContractsGrid.ItemsGrid);
        }

        public ContractsGridView GridControl => ContractsGrid;
    }
}
