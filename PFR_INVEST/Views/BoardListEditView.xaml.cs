﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for BoardListEditView.xaml
    /// </summary>
    public partial class BoardListEditView : UserControl
    {
        public BoardListViewModel VM => (BoardListViewModel)DataContext;
        public BoardListEditView(BoardListViewModel vm)
        {
            DataContext = vm;
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, boardsGrid);
            grid.MouseDoubleClick += (sender, e) =>
            {
                if (VM.Current == null) return;
                App.DashboardManager.OpenNewTab(typeof(BoardView), ViewModelState.Edit, VM.Current.ID, "Режим торгов");
            };

            //Unloaded += (a, b) => { VM.SaveCurrent(); };
            //grid.CellValueChanging += (sender, args) =>
            //{
            //    if (args.Value != null && args.Column.FieldName == "BoardId")
            //    {
            //        var row = ((PFR_INVEST.DataObjects.Board)args.Row);
            //        row.BoardId = args.Value.ToString();
            //        row.Validate();
            //        grid.ValidateEditor();
            //    } 
            //};
        }
    }
}
