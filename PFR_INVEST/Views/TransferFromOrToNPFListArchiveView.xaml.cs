﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for TransferFromOrToNPFArchiveListView.xaml
    /// </summary>
    public partial class TransferFromOrToNPFArchiveListView
    {
        public TransferFromOrToNPFArchiveListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, TransferToNPFGrid, "List");
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, TransferToNPFGrid);
        }

        private void TransferToNPFGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (TransferToNPFGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (!IsTransferSelected()) return;
            var id = GetAsgFinTrID();
            if (id > 0)
                App.DashboardManager.OpenNewTab(typeof(CommonPPView), ViewModelState.Read, id, "Данные платежного поручения");
        }

        protected bool IsTransferSelected()
        {
            return TransferToNPFGrid.View.FocusedRowData.Level >= TransferToNPFGrid.GetGroupedColumns().Count && TransferToNPFGrid.SelectedItem != null;
        }

        protected long GetAsgFinTrID()
        {
            return Convert.ToInt64(TransferToNPFGrid.GetCellValue(TransferToNPFGrid.View.FocusedRowHandle, "AsgFinTrID"));
        }

        private void ListGrid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }
    }
}
