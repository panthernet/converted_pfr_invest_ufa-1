﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for F402ListView.xaml
    /// </summary>
    public partial class F402ListView
    {
        public F402ListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, F401402Grid);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, F401402Grid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (F401402Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (IsF402Selected())
            {
                long lID = GetSelectedF402ID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(F402View), ViewModelState.Edit, lID, "Оплата услуг");
            }
        }

        public bool IsF402Selected()
        {
            return F401402Grid.View.FocusedRowData.Level >= F401402Grid.GetGroupedColumns().Count && F401402Grid.SelectedItem != null;
        }

        public long GetSelectedF402ID()
        {
            return Convert.ToInt64(F401402Grid.GetCellValue(F401402Grid.View.FocusedRowHandle, "ID"));
        }

        public string GetSelectedF402StatusName()
        {
            return Convert.ToString(F401402Grid.GetCellValue(F401402Grid.View.FocusedRowHandle, "StatusName"));
        }

        public F402Status? GetSelectedF402Status()
        {
            return (F402Status?)(long?)F401402Grid.GetCellValue(F401402Grid.View.FocusedRowHandle, "StatusId");
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void Grid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name.Contains("custommonth"))
            {
                var item = e.Row as F401402UKListItem;
                if (item != null)
                {
                    e.DisplayText = item.MonthWithSubitemsCount;
                }
            }
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }
    }
}
