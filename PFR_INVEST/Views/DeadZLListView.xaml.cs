﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DeadZLListView.xaml
    /// </summary>
    public partial class DeadZLListView
    {
        public DeadZLListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, deadZLGrid);
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (deadZLGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (deadZLGrid.View.FocusedRowHandle >= 0)
                App.DashboardManager.OpenNewTab(typeof(DeadZLEditView), ViewModelState.Edit,
                    (long)deadZLGrid.GetCellValue(deadZLGrid.View.FocusedRowHandle, "ID"), "Умершие ЗЛ");
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
	}
}