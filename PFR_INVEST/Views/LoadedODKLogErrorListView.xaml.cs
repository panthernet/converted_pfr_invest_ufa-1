﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class LoadedODKLogErrorListView
    {
        public LoadedODKLogErrorListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }
    }
}
