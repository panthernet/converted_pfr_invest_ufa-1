﻿namespace PFR_INVEST.Views
{
    using BusinessLogic;

    public class InsuranceVRListView : InsuranceListView
    {
        public InsuranceVRListView()
        {
            grdInsuranceList.Columns.GetColumnByName("c2").Header = "ГУК ВР";
        }

        protected override void OpenItem(long id)
        {
            App.DashboardManager.OpenNewTab(typeof(InsuranceVRView), ViewModelState.Edit, id, "Договор страхования ВР");
        }
    }
}
