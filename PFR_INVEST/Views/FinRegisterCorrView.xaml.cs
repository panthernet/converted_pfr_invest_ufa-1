﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PFR_INVEST.Core.Tools;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for FinRegisterCorrView.xaml
    /// </summary>
    public partial class FinRegisterCorrView : UserControl
    {
        public FinRegisterCorrView()
        {
            InitializeComponent();
            Loaded += ViewLoaded;
        }

        void ViewLoaded(object sender, RoutedEventArgs e)
        {
            new ControlsTabStoping(this.Content as DependencyObject);
        }
    }
}
