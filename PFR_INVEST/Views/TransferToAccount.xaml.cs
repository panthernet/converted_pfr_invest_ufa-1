﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for TransferToAccount.xaml
    /// </summary>
    public partial class TransferToAccount
    {
        public TransferToAccount()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private TransferToAccountViewModel Model => DataContext as TransferToAccountViewModel;

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            Model?.GetCourceManuallyOrFromCBRSite();
        }
    }
}
