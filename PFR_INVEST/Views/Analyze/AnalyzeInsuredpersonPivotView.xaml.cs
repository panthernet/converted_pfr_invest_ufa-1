﻿using System;
using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;


namespace PFR_INVEST.Views.Analyze
{

    /// <summary>
    /// Interaction logic for AnalyzeInsuredpersonPivotView.xaml
    /// </summary>
    public partial class AnalyzeInsuredpersonPivotView 
    {
        AnalyzeInsuredpersonPivotViewModel VM { get { return DataContext as AnalyzeInsuredpersonPivotViewModel; } }
        public AnalyzeInsuredpersonPivotView(AnalyzeInsuredpersonPivotViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
            Loaded += AnalyzeInsuredpersonPivotView_Loaded;
            pivot5.AllowExpandOnDoubleClick = false;
        }

        private void AnalyzeInsuredpersonPivotView_Loaded(object sender, RoutedEventArgs e)
        {
            if (VM != null)
                VM.OnDataRefreshing += VM_OnDataRefreshing;
            pivot5.Fields["SubjectTypeFond"].CollapseValue(SubjectSPN.FondType.НПФ);
            pivot5.Fields["SubjectTypeFond"].AllowExpand = false;
        }

        private void VM_OnDataRefreshing(object sender, EventArgs e)
        {
            pivot5.ReloadData();
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            AnalyzeReportHelper.SaveReport(pivot5, VM, (DataTemplate)Resources["PageHeader"], VM.FormTitle);
        }

    }
}
