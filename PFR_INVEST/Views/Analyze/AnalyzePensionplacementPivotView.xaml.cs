﻿using System;
using System.Windows;
using DevExpress.Xpf.PivotGrid;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;
using UserControl = System.Windows.Controls.UserControl;

namespace PFR_INVEST.Views.Analyze
{/// <summary>
 /// Interaction logic for AnalyzePensionplacementPivotView.xaml
 /// </summary>
    public partial class AnalyzePensionplacementPivotView : UserControl
    {
        AnalyzePensionplacementViewModel VM => DataContext as AnalyzePensionplacementViewModel;

        public AnalyzePensionplacementPivotView()
        {
            InitializeComponent();
            Loaded += AnalyzePensionplacementView_Loaded;
            pivot3.AllowExpandOnDoubleClick = false;
            pivot3.Fields["SubjectTypeFond"].AllowExpand = true;
            pivot3.Fields["SubjectSubgroup"].AllowExpand = true;
            pivot3.FieldValueExpanding += Pivot3_FieldValueExpanding;
        }

        private void Pivot3_FieldValueExpanding(object sender, PivotFieldValueCancelEventArgs e)
        {
            if (e.Field.FieldName == "SubjectSubgroup" && e.Value.ToString() != "ГУК")
            {
                e.Cancel = true;
                return;
            }
            if (e.Field.FieldName == "SubjectTypeFond" && e.Value.ToString() == "НПФ")
            {
                e.Cancel = true;
                
            }
        }

        private void AnalyzePensionplacementView_Loaded(object sender, RoutedEventArgs e)
        {
            if (VM != null)
                VM.OnDataRefreshing += VM_OnDataRefreshing;

           
        }

        

        private void VM_OnDataRefreshing(object sender, EventArgs e)
        {
            pivot3.ReloadData();
            pivot3.Fields["SubjectTypeFond"].CollapseValue(SubjectSPN.FondType.НПФ);
            pivot3.Fields["SubjectSubgroup"].CollapseValue("ЧУК");
            pivot3.Fields["SubjectSubgroup"].CollapseValue(string.Empty);
            

        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            AnalyzeReportHelper.SaveReport(pivot3, VM, (DataTemplate)Resources["PageHeader"], VM.FormTitle);
        }

    }

    
}
