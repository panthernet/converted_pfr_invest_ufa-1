﻿using System;
using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;
using UserControl = System.Windows.Controls.UserControl;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzePensionplacementPivotView.xaml
    /// </summary>
    public partial class AnalyzePensionfundtonpfPivotView : UserControl
    {
        AnalyzePensionfundtonpfPivotViewModel VM
        {
            get
            {
                return DataContext as AnalyzePensionfundtonpfPivotViewModel;
            }
        }

        public AnalyzePensionfundtonpfPivotView()
        {
            InitializeComponent();
            Loaded += AnalyzePensionfundtonpfPivotView_Loaded;
            pivot1.AllowExpandOnDoubleClick = false;
            
        }

        private void AnalyzePensionfundtonpfPivotView_Loaded(object sender, RoutedEventArgs e)
        {
            if (VM != null)
            {
                VM.OnDataRefreshing += VM_OnDataRefreshing;
                pivot1.Fields["SubjectTypeFond"].CollapseValue(SubjectSPN.FondType.НПФ);
                pivot1.Fields["SubjectTypeFond"].AllowExpand = false;
            }
        }
        private void VM_OnDataRefreshing(object sender, EventArgs e)
        {
            pivot1.ReloadData();
            pivot1.Fields["SubjectTypeFond"].CollapseValue(SubjectSPN.FondType.НПФ);
            pivot1.Fields["SubjectTypeFond"].AllowExpand = false;
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            AnalyzeReportHelper.SaveReport(pivot1, VM, (DataTemplate)Resources["PageHeader"], VM.FormTitleWithMeasure);
        }

       
    }
}
