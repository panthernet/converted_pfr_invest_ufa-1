﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzeReportEditBase.xaml
    /// </summary>
    public partial class AnalyzeReportEditBase : UserControl
    {
        private AnalyzeEditorDialogBase<YearReportBase> VM;
        private Type _listType;
        public AnalyzeReportEditBase(AnalyzeEditorDialogBase<YearReportBase> vm, Type listType)
        {
            DataContext = vm;
            Loaded += OnLoaded;
            VM.RequestClose += VM_RequestClose;
            InitializeComponent();
        }
        private void OnLoaded(object sender, EventArgs e)
        {
            if (VM.Years.Count > 0)
                return;
            Visibility = Visibility.Hidden;
            DXMessageBox.Show(@"Все отчеты на доступные периоды уже созданы!", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            VM_RequestClose(null, EventArgs.Empty);
        }
        protected  virtual void VM_RequestClose(object sender, EventArgs e)
        {
            var res = MessageBoxResult.OK;
            if (!string.IsNullOrEmpty(VM.ErrorMsg))
            {
                res = DXMessageBox.Show(VM.ErrorMsg, "Внимание",
                    VM.IsQuestion ? MessageBoxButton.YesNo : MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //if (res == MessageBoxResult.Yes)

            }

            if (res != MessageBoxResult.Yes || res != MessageBoxResult.OK)
                return;

            DashboardManager.CloseActiveView();
            var lvm = App.DashboardManager.FindViewModel(_listType);
            lvm?.Refresh();
        }
    }
}
