﻿using System;
using System.Windows;
using System.Windows.Forms;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.PivotGrid;
using DevExpress.Xpf.Printing;
using DevExpress.XtraPrinting;

namespace PFR_INVEST.Views.Analyze
{
    public class AnalyzeReportHelper
    {
        public static void SaveReport(PivotGridControl pivot, object dataContext, DataTemplate dTmpl, string header, int signCount = 7)
        {
            var link = new PrintableControlLink(pivot);
            link.PageHeaderData = dataContext;
            link.PageHeaderTemplate = dTmpl;//(DataTemplate)Resources["PageHeader"];
            link.CreateDocument(true);

            var fd = new SaveFileDialog();

            fd.Filter = "Excel (*.xlsx*)|*.xlsx*";
            fd.FileName = $"{header.Substring(0, signCount)}_{DateTime.Now.ToString("dd-MM-yy_H-mm-ss")}.xlsx".Replace(" ", "_");
            if (fd.ShowDialog() == DialogResult.OK)
            {
                var exportOptions = new XlsxExportOptions();
                exportOptions.TextExportMode = TextExportMode.Text;

                link.ExportToXlsx(fd.FileName, exportOptions);
                if (DXMessageBox.Show("Открыть сохраненный отчет?", "Действие",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    System.Diagnostics.Process.Start(fd.FileName);
            }
        }
    }
}
