﻿using DevExpress.Xpf.Grid;

namespace PFR_INVEST.Views
{
    public class SIAssignPaymentsListView : AssignPaymentsListView
    {
        public SIAssignPaymentsListView()
        {Grid.Columns.Add(new GridColumn()
            {
                Header = "Тип операции",
                FieldName = "OperationType",
                GroupIndex = 0
            });
        }
    }
}
