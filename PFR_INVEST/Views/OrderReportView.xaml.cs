﻿using System;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Editors;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Core.Tools;
using System.Windows.Data;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using System.Collections.Generic;

    using DevExpress.Xpf.Docking;

    /// <summary>
    /// Interaction logic for OrderReportView.xaml
    /// </summary>
    public partial class OrderReportView : UserControl
    {
        private OrderReportViewModelBase Model => DataContext as OrderReportViewModelBase;

        public OrderReportView()
        {
            InitializeComponent();
            Loaded += OrderReportView_Loaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void SetSaleReportControlsVisibility()
        {
            lblYield.Visibility = Visibility.Hidden;
            txtYield.Visibility = Visibility.Hidden;
            GroupBoxTotalAccount.Visibility = Visibility.Hidden;
            lblContragent.Content = "Покупатель";
            lblTransactionAmount.Content = "Сумма сделки по цене продажи:";
            lblTransactionAmountRub.Content = "Сумма сделки по цене продажи, руб:";
            lblTransactionAmountForPurchasePrice.Visibility = Visibility.Visible;
            lblTransactionAmountForPurchasePriceRub.Visibility = Visibility.Visible;
            txtTransactionAmountForPurchasePrice.Visibility = Visibility.Visible;
            txtTransactionAmountForPurchasePriceRub.Visibility = Visibility.Visible;
        }

        private void HideNotRoubleControls()
        {
            lblRate.Visibility = Visibility.Hidden;
            txtRate.Visibility = Visibility.Hidden;
            lblSumWithoutNKD.Visibility = Visibility.Hidden;
            txtSumWithoutNKD.Visibility = Visibility.Hidden;
            lblNKDTotal.Visibility = Visibility.Hidden;
            txtNKDTotal.Visibility = Visibility.Hidden;
            lblTransactionAmount.Visibility = Visibility.Hidden;
            txtTransactionAmount.Visibility = Visibility.Hidden;
            lblTransactionAmountForPurchasePrice.Visibility = Visibility.Hidden;
            txtTransactionAmountForPurchasePrice.Visibility = Visibility.Hidden;
            lblContragent.Visibility = Visibility.Hidden;
            txtContragent.Visibility = Visibility.Hidden;



            Grid.SetRow(lblSumNom, 4+1);
            Grid.SetRow(txtSumNom, 4 + 1);
            Grid.SetRow(lblSumWithoutNKDRub, 5 + 1);
            Grid.SetRow(txtSumWithoutNKDRub, 5 + 1);
            Grid.SetRow(lblNKDPerOneObl, 6 + 1);
            Grid.SetRow(txtNKDPerOneObl, 6 + 1);
            Grid.SetRow(lblNKDTotalRub, 7 + 1);
            Grid.SetRow(txtNKDTotalRub, 7 + 1);
            Grid.SetRow(lblTransactionAmountRub, 8 + 1);
            Grid.SetRow(txtTransactionAmountRub, 8 + 1);
            Grid.SetRow(lblTransactionAmountForPurchasePriceRub, 9 + 1);
            Grid.SetRow(txtTransactionAmountForPurchasePriceRub, 9 + 1);

            Grid.SetRow(lblYield, 9 + 1);
            Grid.SetRow(txtYield, 9 + 1);
        }


        private void HideNotRelayingControls()
        {
            GroupBoxRelaying.Visibility = Visibility.Visible;

            lblRate.Visibility = Visibility.Hidden;
            txtRate.Visibility = Visibility.Hidden;
            lblSumWithoutNKD.Visibility = Visibility.Hidden;
            txtSumWithoutNKD.Visibility = Visibility.Hidden;
            lblNKDTotal.Visibility = Visibility.Hidden;
            txtNKDTotal.Visibility = Visibility.Hidden;
            lblTransactionAmount.Visibility = Visibility.Hidden;
            txtTransactionAmount.Visibility = Visibility.Hidden;
            lblTransactionAmountForPurchasePrice.Visibility = Visibility.Hidden;
            txtTransactionAmountForPurchasePrice.Visibility = Visibility.Hidden;
            lblContragent.Visibility = Visibility.Hidden;
            txtContragent.Visibility = Visibility.Hidden;
            dpDEF.Visibility = Visibility.Hidden;
            lbDEPOdate.Visibility = Visibility.Hidden;
            //lblPayments.Visibility = Visibility.Hidden;
            //lblAvailbleCount.Visibility = Visibility.Hidden;
            Grid.SetRow(lblPayments, 7);
            Grid.SetRow(lblAvailbleCount, 8);
            lbCount.Content = "Общее количество, штук";
            lbDEPO.Content = "Счет";

            //1column
            Grid.SetRow(lbPrice, 6 + 1);
            Grid.SetRow(txtPrice, 6 + 1);
            Grid.SetRow(lblPayments, 7 + 1);
            Grid.SetRow(lblAvailbleCount, 8 + 1);
            Grid.SetRow(lbCount, 9 + 1);
            Grid.SetRow(txtCount, 9 + 1);

            //2 column
            Grid.SetRow(lblSumNom, 4 + 1);
            Grid.SetRow(txtSumNom, 4 + 1);
            Grid.SetRow(lblSumWithoutNKDRub, 5 + 1);
            Grid.SetRow(txtSumWithoutNKDRub, 5 + 1);
            Grid.SetRow(lblNKDPerOneObl, 6 + 1);
            Grid.SetRow(txtNKDPerOneObl, 6 + 1);
            Grid.SetRow(lblNKDTotalRub, 7 + 1);
            Grid.SetRow(txtNKDTotalRub, 7 + 1);
            Grid.SetRow(lblTransactionAmountRub, 8 + 1);
            Grid.SetRow(txtTransactionAmountRub, 8 + 1);
            Grid.SetRow(lblTransactionAmountForPurchasePriceRub, 9 + 1);
            Grid.SetRow(txtTransactionAmountForPurchasePriceRub, 9 + 1);

            txtNKDPerOneObl.IsReadOnly = true;

            var binding = new Binding("Count")
            {
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                ValidatesOnDataErrors = false
            };
            txtCount.SetBinding(BaseEdit.EditValueProperty, binding);
            txtCount.IsReadOnly = true;

            binding = new Binding("Price")
            {
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                ValidatesOnDataErrors = false
            };
            txtPrice.SetBinding(BaseEdit.EditValueProperty, binding);
            txtPrice.IsReadOnly = true;

            Grid.SetRow(GroupBoxRelaying, 11);
        }



        private void InvalidateNotificationLabelsPosition()
        {
            if (!Model.IsBuyOrder || Model.CurrencyIsRubles)
            {
                Grid.SetRow(lblPayments, 11 + 1);
                Grid.SetRow(lblAvailbleCount, 12 + 1);
            }
            if (Model.IsBuyOrder && Model.CurrencyIsRubles)
            {
                Grid.SetRow(GroupBoxTotalAccount,17 - 5 );
                Grid.SetRowSpan(GroupBoxTotalAccount,2);
            }else if (Model.IsBuyOrder && !Model.CurrencyIsRubles)
            {
                Grid.SetRow(GroupBoxTotalAccount, 17 - 3);
                Grid.SetColumn(GroupBoxTotalAccount, 2 - 1);
            }
        }

        void OrderReportView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null && !Model.IsEventsRegisteredOnLoad)
            {
                Model.IsEventsRegisteredOnLoad = true;
                Model.OnNeedEnterCourseManuallyOrFromCBRSite += Model_OnNeedEnterCourseManuallyOrFromCBRSite;
                Model.OnAvailableSaleSecurityCountChanged += Model_OnAvailableSaleSecurityCountChanged;
                Model.CloseBindingWindows += Model_CloseBindingWindows;
                Model.RequestClose += (s, ee) =>
                    {
                        if (Parent is LayoutPanel)
                            DashboardManager.Instance.DockManager.DockController.Close(Parent as LayoutPanel);
                    };

                if (DataContext is RelayingReportViewModel)
                {
                    SetSaleReportControlsVisibility();
                    HideNotRelayingControls();

                }
                else
                {
                    if (!Model.IsBuyOrder)
                        SetSaleReportControlsVisibility();

                    if (Model.CurrencyIsRubles)
                        HideNotRoubleControls();

                    InvalidateNotificationLabelsPosition();

                    if (!Model.IsRateRequested)
                        Model.GetCourceManuallyOrFromCBRSite();
                }
            }
        }

        protected void Model_OnNeedEnterCourseManuallyOrFromCBRSite(object Sender, EventArgs e)
        {
            Model.GetCourceManuallyOrFromCBRSite();
        }

        protected void Model_OnAvailableSaleSecurityCountChanged(object Sender, EventArgs e)
        {
            txtCount.GetBindingExpression(BaseEdit.EditValueProperty).UpdateTarget();
        }

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            Model.GetCourceManuallyOrFromCBRSite();
        }

        protected void Model_CloseBindingWindows(object Sender, EventArgs e)
        {

            List<ViewModelBase> lv = new List<ViewModelBase>();
            lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(SaleReportViewModel)));
            lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(BuyReportViewModel)));
            lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(RelayingReportViewModel)));
            foreach (var viewModelBase in lv)
            {
                (viewModelBase as OrderReportViewModelBase)?.CloseWindow();
            }
        }
    }
}
