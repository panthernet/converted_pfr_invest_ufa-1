﻿using System.Windows.Controls;
using PFR_INVEST.BusinessLogic;
using System;
using DevExpress.Xpf.Core;
using System.Windows;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DraftView.xaml
    /// </summary>
    public partial class RateView : UserControl
    {
        public RateView()
        {
            InitializeComponent();
            Loaded += RateView_Loaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        void RateView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Model != null && !Model.IsEventsRegisteredOnLoad)
            {
                Model.OnCantGetCourseFromCBRSite += Model_OnCantGetCourseFromCBRSite;
                Model.OnStartedGettingCourseFromCBRSite += Model_OnStartedGettingCourseFromCBRSite;
                Model.OnFinishedGettingCourseFromCBRSite += Model_OnFinishedGettingCourseFromCBRSite;
                Model.IsEventsRegisteredOnLoad = true;
            }
        }

        private void Model_OnStartedGettingCourseFromCBRSite(object sender, EventArgs e)
        {
            Loading.ShowWindow();
        }

        private void Model_OnFinishedGettingCourseFromCBRSite(object sender, EventArgs e)
        {
            var args = e as CurseEventArgs;
            Loading.CloseWindow();
            if(args.ShowSuccess)
                DXMessageBox.Show("Курс успешно получен с сайта ЦБРФ", PFR_INVEST.Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Model_OnCantGetCourseFromCBRSite(object sender, CantGetCourseFromCBRSiteEventArgs e)
        {
            DXMessageBox.Show(e.Message, PFR_INVEST.Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private RateViewModel Model => DataContext as RateViewModel;
    }
}
