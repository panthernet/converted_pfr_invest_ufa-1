﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for SchilsCostsListView.xaml
	/// </summary>
	public partial class SchilsCostsListView
	{
		private bool _mGridRowMouseDoubleClicked;
		public SchilsCostsListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

		private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var item = (Grid.SelectedItem as SchilsCostsListItem);

		    if (item == null) return;
		    switch (item.Operation3Type) 
		    {
		        case SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCreation:
		            App.DashboardManager.OpenNewTab(typeof(BudgetView), ViewModelState.Edit, item.BudgetID, "Бюджет");
		            break;
		        case SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCorrection:
		            App.DashboardManager.OpenNewTab(typeof(BudgetCorrectionView), ViewModelState.Edit, item.ID, "Уточнение бюджета");
		            break;
		        case SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction:
		            App.DashboardManager.OpenNewTab(typeof(DirectionView), ViewModelState.Edit, item.DirectionID, "Распоряжение");
		            break;
		        case SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Transfer:
		            App.DashboardManager.OpenNewTab(typeof(TransferSchilsView), ViewModelState.Edit, item.ID, "Перечисление");
		            break;
		        case SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Return:
		            App.DashboardManager.OpenNewTab(typeof(ReturnView), ViewModelState.Edit, item.ID, "Возврат");
		            break;		
		    }
		}

		public bool IsPortfolioSelected()
		{
			int lvl = Grid.View.FocusedRowData.Level;
			return lvl < Grid.GetGroupedColumns().Count &&
				   Grid.GetGroupedColumns()[lvl].FieldName == "Portfolio";
		}

		public bool IsBudgetSelected()
		{
			
			int lvl = Grid.View.FocusedRowData.Level;
			if (lvl < Grid.GetGroupedColumns().Count)
				return (Grid.GetGroupedColumns()[lvl].FieldName == "Operation1") &&
					   ((SchilsCostsListItem) Grid.View.FocusedRowData.Row).Operation1Type ==
					   SchilsCostsListItem.SchilsCostsListItemOpertion1Type.Budget;
            return Grid.View.FocusedRowData.Row != null && lvl >= Grid.GetGroupedColumns().Count &&
				   ((SchilsCostsListItem) Grid.View.FocusedRowData.Row).Operation2Type ==
				   SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCreation;
		}

		public long GetSelectedBudgetID()
		{
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "BudgetID"));
		}

		public bool IsBudgetCorrectionSelected()
		{
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null && Grid.View.FocusedRowData.Row != null &&
					   ((SchilsCostsListItem) Grid.View.FocusedRowData.Row).Operation2Type ==
					   SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCorrection;
		}

		public long GetSelectedBudgetCorrectionID()
		{
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
		}

		public bool IsDirectionSelected()
		{
		    if (Grid.View.FocusedRowData.Row == null) return false;
			int lvl = Grid.View.FocusedRowData.Level;
			if (lvl < Grid.GetGroupedColumns().Count)
				return (Grid.GetGroupedColumns()[lvl].FieldName == "Operation1") &&
					   ((SchilsCostsListItem) Grid.View.FocusedRowData.Row).Operation1Type ==
					   SchilsCostsListItem.SchilsCostsListItemOpertion1Type.Direction;

			return lvl >= Grid.GetGroupedColumns().Count &&
					  ((SchilsCostsListItem) Grid.View.FocusedRowData.Row).Operation2Type ==
					  SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction;
		}

		public long GetSelectedDirectionID()
		{
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "DirectionID"));
		}

		public bool IsReturnSelected()
		{
            return Grid.View.FocusedRowData.Row != null && Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null &&
					   ((SchilsCostsListItem) Grid.View.FocusedRowData.Row).Operation2Type ==
					   SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Return;
		}

		public long GetSelectedReturnID()
		{
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
		}

		public bool IsTransferSelected()
		{
            return Grid.View.FocusedRowData.Row != null && Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null &&
					   ((SchilsCostsListItem) Grid.View.FocusedRowData.Row).Operation2Type ==
					   SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Transfer;
		}

		public long GetSelectedTransferID()
		{
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
		}

		private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
		{
			e.Allow = !_mGridRowMouseDoubleClicked;
			_mGridRowMouseDoubleClicked = false;
		}

		private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
		{
			if ((e.Value2 ?? "").ToString() == "Возврат")
				e.Result = 1;
			else if ((e.Value1 ?? "").ToString() == "Создание")
				e.Result = -1;
			else if (e.Value1.ToString().StartsWith("Распоряжение") && e.Value2.ToString().StartsWith("Распоряжение"))
				e.Result = string.Compare(e.Value1.ToString(), e.Value2.ToString(), StringComparison.OrdinalIgnoreCase);

		}

	}
}
