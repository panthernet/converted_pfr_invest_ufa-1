﻿using System;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Interaction logic for OrderView.xaml
    /// </summary>
    public partial class OrderView : UserControl
    {
        private bool _isLoadedView;
        public OrderView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public OrderViewModel Model => DataContext as OrderViewModel;

        private void Model_OnNeedReloadOrderData(object sender, EventArgs e)
        {
            switch (Model.SecurityInOrderType)
            {
                case SecurityInOrderType.BuyCurrency:
                    buyCurrencyGrid.RefreshData();
                    break;
                case SecurityInOrderType.SaleCurrency:
                    saleCurrencyGrid.RefreshData();
                    break;
                case SecurityInOrderType.BuyRoublesOnAuction:
                    buyRoublesAuctionGrid.RefreshData();
                    break;
                case SecurityInOrderType.BuyRublesOnSecondMarket:
                    buyRoublesSecondMarketGrid.RefreshData();
                    break;
                case SecurityInOrderType.SaleRoubles:
                    saleRoublesGrid.RefreshData();
                    break;
                case SecurityInOrderType.Unknown:
                default:
                    break;
            }
        }

        void Model_OnNoncompetitiveRequestVisibilityChanged(object sender, EventArgs e)
        {
            txtNoncompetitiveRequest.GetBindingExpression(TextEdit.EditValueProperty).UpdateTarget();
            txtNoncompetitiveRequest.GetBindingExpression(TextEdit.VisibilityProperty).UpdateTarget();
            lblNoncompetitiveRequest.GetBindingExpression(Label.VisibilityProperty).UpdateTarget();
        }

        void Model_OnMissingSecurities(object sender, EventArgs e)
        {
            DXMessageBox.Show("Чтобы перевести поручение на исполнение необходимо добавить хотя бы одну ценную бумагу!",
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void orderView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null && !this._isLoadedView)
            {
                this._isLoadedView = true;
                Model.OnNeedReloadOrderData += Model_OnNeedReloadOrderData;
                Model.OnNonCompetitiveRequestVisibilityChanged += Model_OnNoncompetitiveRequestVisibilityChanged;
                Model.OnMissingSecurities += Model_OnMissingSecurities;

                Model.CloseBindingWindows += (s, ee) =>
                {
                    var lv = new List<ViewModelBase>();
                    lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(SaleReportViewModel)));
                    lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(BuyReportViewModel)));
                    lv.AddRange(App.DashboardManager.FindViewModelsList(typeof(RelayingReportViewModel)));
                    foreach (var viewModelBase in lv.OfType<OrderReportViewModelBase>())
                    {
                        viewModelBase.CloseWindow();
                    }
                };
            }
        }

        private void TableView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var tv = sender as TableView;
            if (tv?.FocusedRow == null)
                return;

            if (Model != null)
                Model.SelectedCbInOrderListItem = tv.FocusedRow as CbInOrderListItem;
        }
    }
}
