﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class UKTransfersListArchiveView
    {
        public UKTransfersListArchiveView()
        {
            InitializeComponent(); 
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, UKGrid);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, UKGrid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
        }

        private void UKGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (UKGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

			// Отключено http://jira.vs.it.ru/browse/DOKIPIV-193?focusedCommentId=229825&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-229825
			//if (IsTransferSelected())
			//    App.dashboardManager.OpenNewTab(typeof(EnterTransferPPDataView), ViewModelState.Read, this.GetSelectedTransfer(), "Данные платежного поручения");
        }

        private void UKGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (!UKGrid.IsValidRowHandle(e.RowHandle))
                return;

            if (!e.Column.FieldName.Equals("PfrBankAccount"))
                return;

            var item = e.Row as UKListItem;
            if (item == null)
                return;

            e.DisplayText = item.PfrBankAccount;
        }

        protected bool IsTransferSelected()
        {
            return UKGrid.View.FocusedRowData.Level >= UKGrid.GetGroupedColumns().Count && UKGrid.SelectedItem != null;
        }

        public UKListItem GetSelectedTransfer()
        {
            return UKGrid.CurrentItem as UKListItem;
        }
    }
}
