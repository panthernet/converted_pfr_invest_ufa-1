﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;
using PFR_INVEST.Tools;
using PFR_INVEST.Views.Dialogs;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for NPFView.xaml
    /// </summary>
    public partial class NPFView : UserControl
    {
        public NPFViewModel Model => DataContext as NPFViewModel;

        public NPFView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void OnEditAccountClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!Model.CanEditAccount)
                throw new InvalidOperationException(Model.EditAccountProhibitionReason);
            var bav = DataContainerFacade.GetByID<BankAccount>(Model.CurrentBankAccount.ID);

            if (bav == null)
                App.DashboardManager.OpenNewTab(typeof(BankAccountView), ViewModelState.Create, this.Model.ID, "Расчетный счет");
            else
                App.DashboardManager.OpenNewTab(typeof(BankAccountView), ViewModelState.Edit, this.Model.CurrentBankAccount.ID, "Расчетный счет");
        }

        private void AccountsTableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (AccountsListGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            long id = GetSelectedBankAccountID();
            if (id > 0)
                App.DashboardManager.OpenNewTab(typeof(BankAccountView), ViewModelState.Edit, id, "Расчетный счет");
        }

        private long GetSelectedBankAccountID()
        {
            return Convert.ToInt64(AccountsListGrid.GetCellValue(AccountsListGrid.View.FocusedRowHandle, "ID"));
        }

        private void GridControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (pausesList.View.GetRowElementByMouseEventArgs(e) == null || Model.EditAccessDenied) return;
            var gc = sender as GridControl;
            if ((gc?.GetFocusedRow() as NPFPause)?.ID > 0)
            {
                var dlg = new SuspendActivityDlg() { Owner = App.mainWindow };
                ThemesTool.SetCurrentTheme(dlg);

                var pause = DataContainerFacade.GetByID<NPFPause, long>((gc.GetFocusedRow() as NPFPause).ID);
                var Item = new SuspendActivityDlg.DataContentContainer()
                {
                    DateStart = pause.StartDate,
                    DateEnd = pause.EndDate,
                    DateDoc = pause.DocumentDate,
                    RegNum = pause.RegNum,
                };
                dlg.scaleGrid.DataContext = Item;

                if (dlg.ShowDialog().Value)
                {
                    pause.StartDate = (DateTime)dlg.SuspendDateStart.EditValue;
                    pause.EndDate = (DateTime)dlg.SuspendDateEnd.EditValue;
                    pause.RegNum = dlg.RegNum.Text;
                    pause.DocumentDate = (DateTime)dlg.DocDate.EditValue;
                    pause.ContragentID = Model.ContragentID;

                    DataContainerFacade.Save<NPFPause, long>(pause);

                    Model.RefreshNPFPauseList();
                    Model.RefreshConnectedLists();
                }
            }
        }

        private void ReorganizationsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ReorganizationsGrid.View.GetRowElementByMouseEventArgs(e) != null)
            {
                var reorganization = (Reorganization)ReorganizationsGrid.GetFocusedRow();
                OpenNpfEditForm(reorganization.SourceContragentID);
            }
        }

        private void ParentReorganizationsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ParentReorganizationsGrid.View.GetRowElementByMouseEventArgs(e) != null)
            {
                var reorganization = (Reorganization)ParentReorganizationsGrid.GetFocusedRow();
                OpenNpfEditForm(reorganization.ReceiverContragentID);
            }
        }

        private void OpenNpfEditForm(long contragentID)
        {
            var legalEntity = Model.GetLegalEntityByContragentID(contragentID);
            App.DashboardManager.OpenNewTab(typeof(NPFView), ViewModelState.Edit, legalEntity.ID, "Юридическое лицо");
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Model != null)
                Model.OnListIsEmpty += Model_OnListIsEmpty;
        }

        private void Model_OnListIsEmpty(object seder, EventArgs e)
        {
            MainTC.SelectedIndex = 0;
        }

        private void oldChiefGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (oldChiefGrid.View.GetRowElementByMouseEventArgs(e) != null)
            {
                LegalEntityChief x = oldChiefGrid.View.FocusedRow as LegalEntityChief;

                if (x != null)
                {
                    var caption = x.GetFullName();
                    long id = x.ID; //(long)oldChiefGrid.GetCellValue(oldChiefGrid.View.FocusedRowHandle, "ID");

                    //App.dashboardManager.OpenNewTab(typeof(NPFChiefView), ViewModelState.Edit, id,  x, fullName);
                    var p = new NPFViewModel.LegalEntityChiefParamContainer() { LegalEntityChief = x, Model = Model };

                    var state = Model.IsChiefChanged(x) ? ViewModelState.Read : ViewModelState.Edit;

                    if (state == ViewModelState.Read)
                        caption += " (только для чтения)";
                    App.DashboardManager.OpenNewTab(typeof(NPFChiefView), state, id, p, caption);
                }
            }
        }

        private void CouriersGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var x = CourierGrid.GetFocusedRow() as LegalEntityCourier;
            if (x == null)
                return;

            Model.EditCourier(x);
        }

        private void ContactsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var contact = ContactGrid.GetFocusedRow() as LegalEntityCourier;
            if (EditContactHelper.Command != null && EditContactHelper.Command.CanExecute(contact))
                EditContactHelper.Command.Execute(contact);
        }

        private void IdentifierGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var identifier = IdentifierGrid.GetFocusedRow() as LegalEntityIdentifier;
            if (EditIdentifierHelper.Command != null && EditIdentifierHelper.Command.CanExecute(identifier))
                EditIdentifierHelper.Command.Execute(identifier);
        }

        private void ReorganizationsGridContainer_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateReorganizationsGridContainerRows();
        }

        private void ParentReorganizationsGrid_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UpdateReorganizationsGridContainerRows();
        }

        private void UpdateReorganizationsGridContainerRows()
        {
            ReorganizationsGridContainer.RowDefinitions[1].Height = ParentReorganizationsGrid.IsVisible ? new GridLength(1, GridUnitType.Star) : GridLength.Auto;
            ReorganizationsGridContainer.RowDefinitions[3].Height = ReorganizationsGrid.IsVisible ? new GridLength(1, GridUnitType.Star) : GridLength.Auto;
        }

    }
}
