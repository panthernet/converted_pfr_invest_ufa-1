﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SecuritiesListView.xaml
    /// </summary>
    public partial class SecuritiesListView
    {
        public SecuritiesListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, SecuritiesGrid);
        }

        private void SecuritiesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (SecuritiesGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (SecuritiesGrid.View.FocusedRowHandle >= 0)
                App.DashboardManager.OpenNewTab(typeof(SecurityView), ViewModelState.Edit,
                    GetSelectedSecurityID(), "Ценная бумага");
        }

        public long GetSelectedSecurityID()
        {
            if (SecuritiesGrid.View.FocusedRowHandle <= -10000)
                return 0;
            if (SecuritiesGrid.View.FocusedRowHandle >= 0)
                return (long)SecuritiesGrid.GetCellValue(SecuritiesGrid.View.FocusedRowHandle, "ID");
            return 0;
        }
    }
}
