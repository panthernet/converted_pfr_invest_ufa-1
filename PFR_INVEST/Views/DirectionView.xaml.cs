﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DirectionView.xaml
    /// </summary>
    public partial class DirectionView : UserControl
    {
        public DirectionView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void GridControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (IsReturnSelected())
            {
                long id = GetSelectedID();
                if (id > 0)
                    App.DashboardManager.OpenNewTab(typeof(ReturnView), ViewModelState.Edit, id, "Возврат");
            }
            else if (IsTransferSelected())
            {
                long id = GetSelectedID();
                if (id > 0)
                    App.DashboardManager.OpenNewTab(typeof(TransferSchilsView), ViewModelState.Edit, id, "Перечисление");
            }
        }

        public bool IsReturnSelected()
        {
            return (Grid.SelectedItem as DirectionTransferListItem)?.OperationType ==
                   DirectionTransferListItem.DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeReturn;
        }

        public bool IsTransferSelected()
        {
            return (Grid.SelectedItem as DirectionTransferListItem)?.OperationType ==
                   DirectionTransferListItem.DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeTransfer;
        }

        public long GetSelectedID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }
    }
}
