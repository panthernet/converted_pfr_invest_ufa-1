﻿using System.Windows.Controls;
using System;
using DevExpress.Xpf.Editors;
using PFR_INVEST.Extensions;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for Treasurity3View.xaml
    /// </summary>
    public partial class Treasurity4View : UserControl
    {
		public Treasurity4View()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        #region Numeric field edit fix
        private bool _isManualChange;

		private void OnGotFocus(object sender, EventArgs e)
		{
			_isManualChange = true;
		}

		private void OnLostFocus(object sender, EventArgs e)
		{
			_isManualChange = false;
		}

		private void OnEditValueChanged(object sender, EditValueChangedEventArgs e)
		{
			if (_isManualChange)
			{
				Dispatcher.RemoveTextLeadingZero(sender, e);
			}
		}
		#endregion 
	}
}
