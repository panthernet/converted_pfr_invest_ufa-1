﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Interaction logic for RegisterView.xaml
    /// </summary>
    public partial class RegisterView : UserControl
    {
        private RegisterViewModel Model => DataContext as RegisterViewModel;

        public RegisterView()
        {
            InitializeComponent();
            Loaded += ViewLoaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void ViewLoaded(object sender, RoutedEventArgs e)
        {
            if (Model != null && !Model.IsEventsRegisteredCloseBindingWindows)
            {
                Model.CloseBindingWindows += (s, ea) =>
                {   
                    var lv = new List<ViewModelBase>();

                    var FRModels = App.DashboardManager.FindViewModelsList(typeof(FinRegisterViewModel));

                    if (ea.GetAttachedProperty("ids") != null && ea.GetAttachedProperty("ids") is List<long>)
                    {
                        var list = ea.GetAttachedProperty("ids") as List<long>;
                        lv = FRModels.Where(m => list.Contains(m.ID)).ToList();
                    }

                    foreach (var viewModelBase in lv)
                    {
                        (viewModelBase as FinRegisterViewModel)?.CloseWindow();
                    }

                    // закрытие форм, не сохраненных в БД
                    if (ea.GetAttachedProperty("pID") != null && ea.GetAttachedProperty("pID") is long)
                    {
                        var pID = (long)ea.GetAttachedProperty("pID");
                        lv = FRModels.Where(m => (m as FinRegisterViewModel).RegisterID == pID).ToList();
                    }

                    foreach (var viewModelBase in lv)
                    {
                        (viewModelBase as FinRegisterViewModel)?.CloseWindow();
                    }
                };
            }
        }

        private void grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (grid.View.GetRowElementByMouseEventArgs(e) == null) return;

			var mode = Model.IsReadOnly ? ViewModelState.Read : ViewModelState.Edit;

			if (Model is SPNAllocationViewModel || Model is SPNReturnViewModel)
			{
				// получение финреестра для конструктора выполняет dashboardManager
				if (Model.SelectedFR == null || Model.SelectedFR.ID == 0)
				{
					if (!Model.IsReadOnly)
						App.DashboardManager.OpenNewTab(typeof(AllocationNPFView), ViewModelState.Create, 0, "Финреестр");
				}
				else
				{
					App.DashboardManager.OpenNewTab(typeof(AllocationNPFView), mode, Model.SelectedFR.ID.Value, "Финреестр");
				}
				return;
			}

            long ID = Convert.ToInt64(grid.GetCellValue(this.grid.View.FocusedRowHandle, "ID"));
            if (ID > 0)
            {
				App.DashboardManager.OpenNewTab(typeof(FinRegisterView), mode, ID, "Финреестр");
            }
        }

        public Finregister SelectedFinregister
        {
            get
            {
                var row = gridView.FocusedRow as FinregisterWithCorrAndNPFListItem;
                return row?.Finregister;
            }
        }

        public FinregisterWithCorrAndNPFListItem SelectedFinregisterWithCorrAndNPF
        {
            get
            {
                var row = gridView.FocusedRow as FinregisterWithCorrAndNPFListItem;
                return row;
            }
        }

        private void gridView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
			// сбрасывает значение
            // gridView.PostEditor();
			var fr = SelectedFinregisterWithCorrAndNPF;
			if (fr?.Finregister != null)
			{
				switch (e.Column.FieldName)
				{
					case "Count": fr.Finregister.Count = fr.count = Convert.ToDecimal(e.Value); break;
					case "CFRCount": fr.Finregister.CFRCount = fr.cfrcount = Convert.ToDecimal(e.Value); break;
					case "ZLCount": fr.Finregister.ZLCount = fr.zlcount = Convert.ToInt64(e.Value); break;
				}

				fr.IsItemChanged = true;
				Model.IsDataChanged = true;
			}
		}

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Model.ERZL != null)
                App.DashboardManager.OpenNewTab(typeof(ZLRedistView), ViewModelState.Edit, this.Model.ERZL.ID, "Перераспределение ЗЛ");
        }
    }
}
