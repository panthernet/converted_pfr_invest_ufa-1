﻿using System.Linq;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for UserView.xaml
    /// </summary>
    public partial class ADUserView
    {
        private ADUserViewModel Model => DataContext as ADUserViewModel;

        public ADUserView()
        {
            InitializeComponent();
        }

        private void LVCurrentRoles_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var row = listGrid1.SelectedItems?.Cast<Role>().ToList();
            if (row == null) return;
            if (Model != null)
            {
                Model.SelectedAddedRole = row;
            }
        }

        private void LVAllRoles_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var row = listGrid2.SelectedItems?.Cast<Role>().ToList();
            if (row == null) return;
            if (Model != null)
            {
                Model.SelectedRemovedRole = row;
            }
        }
    }
}