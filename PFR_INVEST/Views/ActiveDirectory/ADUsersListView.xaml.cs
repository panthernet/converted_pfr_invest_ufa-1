﻿using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for UsersView.xaml
    /// </summary>
    public partial class ADUsersListView
    {
        public ADUsersListView()
        {
            InitializeComponent();
        }

        private void LVCurrentUsers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listView1.GetRowHandleByMouseEventArgs(e) == DataControlBase.InvalidRowHandle) return;
            ShowSelectedUser();
        }

        /// <summary>
        /// Показать свойства выбранного пользователя
        /// </summary>
        public void ShowSelectedUser()
        {
            var selectedItem = listGrid.SelectedItems == null || listGrid.SelectedItems.Count < 1 ? null : listGrid.SelectedItems[0];

            if (selectedItem is User) 
            {
                var roleForm = new ADUserView();
                var rm = new  ADUserViewModel(selectedItem as User);
                roleForm.DataContext = rm;
                App.DashboardManager.OpenNewTab("Пользователь: " + rm.User.Name, roleForm, ViewModelState.Edit, rm);
            }
            if (!(selectedItem is ADUserViewModel)) return;
            //  Перед отображением пользователя загрузим список ролей принадлежащих ему
            var roleForm2 = new ADUserView();
            var rm2 = selectedItem as ADUserViewModel;
            roleForm2.DataContext = rm2;
            rm2.Reload();

            App.DashboardManager.OpenNewTab("Пользователь: " + rm2.User.Name, roleForm2, ViewModelState.Create, rm2);
        }
    }
}