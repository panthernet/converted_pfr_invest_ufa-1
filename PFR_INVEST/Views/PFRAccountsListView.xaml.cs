﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PFRAccountsListView.xaml
    /// </summary>
    public partial class PFRAccountsListView
    {
        public PFRAccountsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, PFRAccountsGrid);
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (PFRAccountsGrid.View.GetRowElementByMouseEventArgs(e) != null)
            {
                App.DashboardManager.OpenNewTab(typeof(PFRAccountView), ViewModelState.Edit,
                        (long)PFRAccountsGrid.GetCellValue(PFRAccountsGrid.View.FocusedRowHandle, "ID"), "Счет ПФР");
            }
        }
    }
}
