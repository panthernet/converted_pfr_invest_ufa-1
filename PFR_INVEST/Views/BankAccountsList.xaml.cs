﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for BankAccountsList.xaml
    /// </summary>
    public partial class BankAccountsList
    {
        public BankAccountsList()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, accountsListGrid);

            accountsListGrid.FilterString = "IsNull([CloseDate])";
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (accountsListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (accountsListGrid.View.FocusedRowHandle >= 0)
                App.DashboardManager.OpenNewTab(typeof(BankAccountView), ViewModelState.Edit,
                    (long)accountsListGrid.GetCellValue(accountsListGrid.View.FocusedRowHandle, "ID"), "Расчетный счет");
        }
    }
}
