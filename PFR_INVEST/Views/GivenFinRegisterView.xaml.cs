﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for GivenFinRegisterView.xaml
    /// </summary>
    public partial class GivenFinRegisterView : UserControl
    {
        public GivenFinRegisterView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
