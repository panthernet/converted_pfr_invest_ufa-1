﻿using DevExpress.Xpf.Grid;

namespace PFR_INVEST.Views
{
    public class VRAssignPaymentsListView : AssignPaymentsListView
    {
        public VRAssignPaymentsListView()
        {
            Grid.Columns.Add(new GridColumn()
            {
                Header = "Тип операции",
                FieldName = "OperationType",
                GroupIndex = 0
            });
        }
    }
}
