﻿using System.Windows;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.Views.Login
{
    /// <summary>
    /// Interaction logic for ChangePasswordWindow.xaml
    /// </summary>
    public partial class ChangePasswordWindow
    {
        public DelegateCommand OkCommand { get; set; }

        public ChangePasswordWindow(bool isPwdChange)
        {
            InitializeComponent();
            DataContext = this;
            OkCommand = new DelegateCommand(o => !string.IsNullOrEmpty(txtPassword.Password) && (!isPwdChange || !string.IsNullOrEmpty(txtOldPassword.Password)), o => DialogResult = true);
            btnCancel.Click += (sender, args) => DialogResult = false;

            if (isPwdChange)
            {
                Title = "Смена пароля";
            }
            else
            {
                lblOldPassword.Visibility = Visibility.Collapsed;
                txtOldPassword.Visibility = Visibility.Collapsed;
                Title = "Установка пароля";
            }
        }
    }
}
