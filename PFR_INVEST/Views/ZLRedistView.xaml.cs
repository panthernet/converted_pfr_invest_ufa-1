﻿using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ZLRedistView.xaml
    /// </summary>
    public partial class ZLRedistView : UserControl
    {
        public ZLRedistView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void ERZLNotGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ERZLNotGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            //App.dashboardManager.OpenNewTab("Уведомление ЕРЗЛ", typeof(ERZLNotifyView), PFR_INVEST.VIEWMODEL_STATES.Edit, this.GetSelectedID());
            var vm = App.DashboardManager.GetActiveViewModel() as ZLRedistViewModel;
            if (vm == null || (vm.EditAccessDenied && !AP.Provider.CurrentUserSecurity.GetRoles().Any(a => a.ToString().ToLower().EndsWith("viewer")))) return;
            vm?.EditNotify.Execute(null);
        }
    }
}
