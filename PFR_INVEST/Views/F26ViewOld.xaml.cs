﻿using System;
using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    [Obsolete("Устаревший, использовать F26View", false)]
    public partial class F26ViewOld : UserControl
    {
        public F26ViewOld()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
