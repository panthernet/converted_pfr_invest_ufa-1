﻿using System;
using System.Threading;
using System.Windows;
using PFR_INVEST.Tools;

namespace PFR_INVEST
{
    /// <summary>
    /// Interaction logic for Loading2.xaml
    /// </summary>
    public partial class Loading
    {
        private static Loading _activeWindow;
        private static CancellationTokenSource _cancellationToken;
        public Loading()
        {
            InitializeComponent();
            ThemesTool.UpdateLoadingTextTheme(txtMessageBorder, txtMessage);
            ThemesTool.UpdateLoadingIndicator(null, PART_Logo);
            Loaded += (sender, args) => IsLoading = false;
        }

        private static bool IsLoading;

        public static void ShowWindow(string message = null)
        {
            if(IsLoading) return;
            IsLoading = true;
            try
            {
                if (_cancellationToken != null)
                {
                    if (message != null)
                        SetPublicMessage(message);
                    return;
                }

                _cancellationToken = new CancellationTokenSource();

                var thread = new Thread(DoWork);
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start(message);
                while (IsLoading)
                    Thread.Sleep(1);
            }
            finally
            {
                IsLoading = false;
            }
        }

        public static void CloseWindow()
        {
            _activeWindow?.Dispatcher?.Invoke(new Action(() => _activeWindow.Close()));
            _activeWindow = null;
            _cancellationToken = null;
        }

        private static void DoWork(object data)
        {
            try
            {
                var message = (string) data;
                _activeWindow = new Loading();
                _activeWindow?.Dispatcher?.Invoke(new Action(() =>
                {
                    _activeWindow.SetMessage(message);
                    _activeWindow.ShowDialog();
                }));
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

        public static void SetPublicMessage(string message)
        {
            _activeWindow?.Dispatcher?.Invoke(new Action(() =>
            {
                _activeWindow.SetMessage(message);
            }));
        }

        private void SetMessage(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                return;
            txtMessage.Content = message;
            txtMessageBorder.Visibility = string.IsNullOrWhiteSpace(message) ? Visibility.Collapsed : Visibility.Visible;
        }

        #region Session

        public class Session : IDisposable
        {
            public Session(string message)
            {
                ShowWindow(message);
            }

            public void Dispose()
            {
                CloseWindow();  
            }
        }

        public static Session StartNew(string message = "")
        {
            return new Session(message);
        }

        #endregion
    }
}
