﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Tools;

namespace PFR_INVEST
{
	/// <summary>
	/// Interaction logic for Loading.xaml
	/// </summary>
	public partial class Loading3 : Window
	{
	    private static readonly ManualResetEvent RequestTermination = new ManualResetEvent(false);

		/// <summary>
		/// Врапер для использования индикатора загрузки в конструкциях using
		/// </summary>
		public class Session : IDisposable
		{
			public Session(string message)
			{
				ShowWindow(true, message);
			}

			public void Dispose()
			{
                CloseWindow();  
			}
		}

	    public static bool IsActiveLoading { get; private set; }

	    private static Loading3 ActiveWindow { get; set; }

		private static Thread WinThread { get; set; }

		private static Timer SecureTimer { get; set; }


	    public static Session StartNew(string message = "")
		{
			return new Session(message);
		}

		/// <summary>
		/// Start shutdown timer when critical phase passed successfuly / 10sec default
		/// </summary>
		public static void StartShutdownTimer()
		{
		    ActiveWindow?.Dispatcher.Invoke(new Action(() =>
		    {
		        SecureTimer = new Timer(Tcb, null, 10000, 0);
		    }), null);
		}

		private static void Tcb(object stateInfo)
		{
			CloseWindow();
			SecureTimer.Dispose();
			SecureTimer = null;
		}

		/// <summary>
		/// Показывает индикатор загружености с сообщением
		/// </summary>
		/// <param name="message">Собщение</param>
		public static void ShowWindow(string message) 
		{
			ShowWindow(true, message);
		}

		public static void ShowWindow(bool indeterminate, string message)
		{
            //если заблокирован, не создаем новый
		    if (IsBlocked())
		    {
                //но можем обновить сообщение!
                SetPublicMessage(message);
                return;
		    }
			if (WinThread != null) try { System.Windows.Threading.Dispatcher.FromThread(WinThread)?.InvokeShutdown(); }
				catch { }
			if (AppSettings.LoadingDisabled) return;
			WinThread = new Thread(DoWork);
			IsActiveLoading = true;
			WinThread.SetApartmentState(ApartmentState.STA);
			WinThread.Start(new object[] { indeterminate, message });
            while(!IsBlocked()) //ждем пока не создастся окно индикатора в новом потоке
            {
                Thread.Sleep(5);
            }
		}

		public static void ShowWindow(bool indeterminate)
		{
			ShowWindow(indeterminate, null);
		}

		public static void SetPublicMessage(string message)
		{
		    ActiveWindow?.Dispatcher.Invoke(new Action(() =>
		    {
		        ActiveWindow.SetMessage(message);
		    }), null);
		    Thread.Sleep(300);
		}

		public void SetMessage(string message)
		{
			if (string.IsNullOrWhiteSpace(message))
				return;
			txtMessage.Content = message;
			txtMessageBorder.Visibility = string.IsNullOrWhiteSpace(message) ? Visibility.Collapsed : Visibility.Visible;
		}

		/// <summary>
		/// Скрывает индикатор загружености
		/// </summary>
		public static void CloseWindow()
		{
			if (ActiveWindow != null && WinThread != null && WinThread.IsAlive)
			{
                ResetBlock();
                ActiveWindow.Dispatcher.Invoke(new Action(() => {ActiveWindow.Close(); }));
            }
            ActiveWindow = null;
		    if (WinThread != null)
		    {
		        System.Windows.Threading.Dispatcher.FromThread(WinThread)?.InvokeShutdown();
		        WinThread = null;
		    }
		    IsActiveLoading = false;
		}

		private static void DoWork(object param)
		{
		    try
		    {
                if(IsBlocked() || !IsActiveLoading) return;

		        if (!(param is object[])) return;

		        var isIndeterminate = (bool) ((object[]) param)[0];
		        var message = ((object[]) param)[1] as string;

		        ActiveWindow = new Loading3(isIndeterminate);
                Block(); //блокируем поток после создания окна
                ActiveWindow.Dispatcher.Invoke(new Action(() =>
		        {
		            ActiveWindow.SetMessage(message);
		            ActiveWindow.ShowDialog();
                }));
		    }
		    catch (Exception)
		    {

		    }
            finally
		    {
		        ResetBlock(); //разблокируем поток перед завершением
		        WinThread = null;
		    }
		}

        private static void Block()
        {
            RequestTermination?.Set();
        }

        private static void ResetBlock()
        {
            RequestTermination?.Reset();
        }

        private static bool IsBlocked()
        {
            return RequestTermination != null && RequestTermination.WaitOne(0);
        }

		public Loading3()
		{
			InitializeComponent();
		    ThemesTool.UpdateLoadingTextTheme(txtMessageBorder, txtMessage);
			//ActiveWindow = this;
			Loaded += Loading_Loaded;
			Closed += Loading_Closed;
		}

		public Loading3(bool isIndeterminate)
			: this()
		{
			progressIndicator.IsIndeterminate = isIndeterminate;
		}

		void Loading_Closed(object sender, EventArgs e)
		{
			Loaded -= Loading_Loaded;
			Closed -= Loading_Closed;
			progressIndicator.StopIndicator();
			progressIndicator = null;
		}

		void Loading_Loaded(object sender, RoutedEventArgs e)
		{
			progressIndicator.StartIndicator();
		}

		private void Window_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			//отключаем все нажатия мыши
			e.Handled = true;
		}

		private void Window_PreviewKeyDown(object sender, KeyboardEventArgs e)
		{
			//отключение нажатий клавиатуры
			e.Handled = true;
		}
	}
}
