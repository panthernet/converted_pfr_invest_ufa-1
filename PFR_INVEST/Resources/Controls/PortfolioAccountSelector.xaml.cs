﻿using System.Windows;
using System.Windows.Controls;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Tools;
using PFR_INVEST.Views.Dialogs;

namespace PFR_INVEST.Resources.Controls
{
    /// <summary>
    /// Interaction logic for PortfolioAccountSelector.xaml
    /// </summary>
    public partial class PortfolioAccountSelector : UserControl
    {
        public PortfolioAccountSelector()
        {
            InitializeComponent();
        }

        public PortfolioFullListItem SelectedPortfolioAccount
        {
            get { return (PortfolioFullListItem)GetValue(SelectedPortfolioAccountProperty); }
            set { SetValue(SelectedPortfolioAccountProperty, value); }
        }

        public static readonly DependencyProperty SelectedPortfolioAccountProperty =
        DependencyProperty.Register("SelectedPortfolioAccount", typeof(PortfolioFullListItem), typeof(PortfolioAccountSelector));

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new SelectPFRAccountByPortfolioDlg();
            var dlgVM = new PortfoliosListViewModel();
            dlg.DataContext = dlgVM;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);
            if (dlg.ShowDialog() == true)
                this.SelectedPortfolioAccount = dlgVM.SelectedItem;
        }
    }
}
