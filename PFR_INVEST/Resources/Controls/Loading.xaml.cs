﻿using System.Windows;
using System.Windows.Controls;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Resources.Controls
{
    /// <summary>
    /// Interaction logic for Loading.xaml
    /// </summary>
    public partial class Loading : UserControl
    {
        public Loading()
        {
            InitializeComponent();
            ThemesTool.UpdateLoadingTextTheme(txtMessageBorder, txtMessage);
        }

        private void Loading_Loaded(object sender, RoutedEventArgs e)
        {
            progressIndicator.StartIndicator();
        }

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(Loading), new UIPropertyMetadata(string.Empty, OnMessageChanged));

        private static void OnMessageChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var ctl = (sender as Loading).txtMessage;
            var text = (string)e.NewValue;
            ctl.Content = text;
            (sender as Loading).txtMessageBorder.Visibility = string.IsNullOrWhiteSpace(text) ? Visibility.Collapsed : Visibility.Visible;
        }
    }
}
