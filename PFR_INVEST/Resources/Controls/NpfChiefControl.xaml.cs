﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.Controls
{
    /// <summary>
    /// Interaction logic for NpfChiefControl.xaml
    /// </summary>
    public partial class NpfChiefControl : UserControl
    {
        public static readonly DependencyProperty RetireModeProperty =
            DependencyProperty.Register("RetireMode", typeof(PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode), typeof(NpfChiefControl), new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustRetireMode)));


        public PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode RetireMode
        {
            get
            {
                return (PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode)GetValue(RetireModeProperty);
            }

            set
            {
                SetValue(RetireModeProperty, value);
            }
        }

        private static void AdjustRetireMode(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            (source as NpfChiefControl).SetRetireMode((PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode)e.NewValue);
        }

        public void SetRetireMode(PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode mode)
        {
            switch (mode)
            {
                case PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode.CanRetire:
                    btnRetire.Visibility = System.Windows.Visibility.Visible;
                    phRetireDate.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode.Retired:
                    btnRetire.Visibility = System.Windows.Visibility.Collapsed;
                    phRetireDate.Visibility = System.Windows.Visibility.Visible;
                    foreach (var control in new [] { pnlHeadLastName, pnlHeadFirstName, pnlHeadPatronymicName})
                        control.Visibility = Visibility.Collapsed;
                    break;
                case PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode.NoRetire:
                    btnRetire.Visibility = System.Windows.Visibility.Collapsed;
                    phRetireDate.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                default: throw new NotSupportedException();
            }
        }

        public NpfChiefControl()
        {
            InitializeComponent();
            SetRetireMode(PFR_INVEST.BusinessLogic.NPFViewModel.EnRetireMode.CanRetire);
        }



        //public ICommand RetireHead { get; private set; }

        //private void InitCommands()
        //{
        //    RetireHead = new DelegateCommand(o => CanExecuteRetireHead(),
        //       o => ExecuteRetireHead());
        //}
    }
}
