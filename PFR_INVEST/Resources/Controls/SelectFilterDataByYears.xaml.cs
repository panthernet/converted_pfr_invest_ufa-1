﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PFR_INVEST.Resources.Controls
{
    /// <summary>
    /// Interaction logic for Loading.xaml
    /// </summary>
    public partial class SelectFilterDataByYears : UserControl
    {
        public SelectFilterDataByYears()
        {
            InitializeComponent();
        }


        public static readonly DependencyProperty ItemsSourceProperty =
           DependencyProperty.Register("ItemsSource",
           typeof(Object),
           typeof(SelectFilterDataByYears),
           new PropertyMetadata(default(object)));
        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }


        public static readonly DependencyProperty EditValueProperty =
           DependencyProperty.Register("EditValue",
           typeof(object),
           typeof(SelectFilterDataByYears),
            new PropertyMetadata(default(object)));
        public object EditValue
        {
            get { return (object)GetValue(EditValueProperty); }
            set { SetValue(EditValueProperty, value); }
        }


        public static readonly DependencyProperty DisplayMemberProperty =
           DependencyProperty.Register("DisplayMember",
           typeof(String),
           typeof(SelectFilterDataByYears),
           new UIPropertyMetadata(string.Empty, new PropertyChangedCallback(DisplayMemberChanged)));
        public string DisplayMember
        {
            get { return (string)GetValue(DisplayMemberProperty); }
            set { SetValue(DisplayMemberProperty, value); }
        }
        private static void DisplayMemberChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = (sender as SelectFilterDataByYears).filterYear;
            control.DisplayMember = (string)e.NewValue;
        }


        public static readonly DependencyProperty TitleFilterProperty =
           DependencyProperty.Register("TitleFilter",
           typeof(String),
           typeof(SelectFilterDataByYears),
           new UIPropertyMetadata(string.Empty, new PropertyChangedCallback(TitleFilterChanged)));
        public string TitleFilter
        {
            get { return (string)GetValue(TitleFilterProperty); }
            set { SetValue(TitleFilterProperty, value); }
        }
        private static void TitleFilterChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = (sender as SelectFilterDataByYears).titleFilter;
            control.Content = (string)e.NewValue;
        }


        //private static void ItemsSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        //{
        //    var control = (sender as FilterLoading).filterYear;
        //    control.ItemsSource = (Dictionary<string, int>)e.NewValue;
        //}

        //private static void EditValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        //{
        //    var control = (sender as FilterLoading).filterYear;
        //    control.EditValue = (KeyValuePair<string, int>)e.NewValue;

        //}






    }
}
