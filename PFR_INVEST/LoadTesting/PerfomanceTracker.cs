using System;

namespace PFR_INVEST.LoadTesting
{	
	public class PerfomanceTracker
	{
		private DateTime m_dtLastCheckPoint;

		public PerfomanceTracker()
		{
			m_dtLastCheckPoint = DateTime.Now;
		}

		public void Clear()
		{
			m_dtLastCheckPoint = DateTime.Now;
		}

		public long GetMSeconds()
		{	
			long lRes = DateTime.Now.Ticks - m_dtLastCheckPoint.Ticks;			
			m_dtLastCheckPoint = DateTime.Now;
			return lRes / TimeSpan.TicksPerMillisecond; 
		}

		public long GetSeconds()
		{	
			long lRes = DateTime.Now.Ticks - m_dtLastCheckPoint.Ticks;			
			m_dtLastCheckPoint = DateTime.Now;
			return lRes / TimeSpan.TicksPerSecond; 
		}

		/// <summary>
		/// Without clearing track time
		/// </summary>
		/// <returns></returns>
		public long PeekMSeconds()
		{
			long lRes = DateTime.Now.Ticks - m_dtLastCheckPoint.Ticks;						
			return lRes / TimeSpan.TicksPerMillisecond; 
		}
	}
}
