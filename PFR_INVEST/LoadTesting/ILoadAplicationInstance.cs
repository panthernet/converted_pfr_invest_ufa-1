﻿using System;

namespace PFR_INVEST.LoadTesting
{
    public interface ILoadAplicationInstance
    {
        int InstanceID { get; }
        string ScenarioReport { get; }
        void RunScenario(ILoadApplicationController controller, Type scenarioType, int ID, int CycleCount, DateTime? LaunchTime, bool delay, int DelayMaxNumber);
        void Stop();
    }
}
    