﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class SPNMovementsListScenario : ScenarioBase
	{
		public override string ScenarioName
		{
			get { return "Работа с СИ - Перечисления средств - Журнал движения СПН"; }
		}

		public override void Run()
		{
			base.Login();
			var rnd = new Random();
			base.RaiseStepChanged(0.1, "Загрузка модели списка");
			var model = new SPNMovementsSIListViewModel();

			var list = model.List;
			if (list.Count == 0)
			{
				base.RiseEmptySequence();
				return;
			}
			var item = list.RandomItem(rnd);

			base.RaiseStepChanged(0.3, "Список перечислений");

			var recordID = item.TransferID;
			var card2 = new TransferViewModel(recordID, ViewModelState.Edit);


			base.RaiseStepChanged(0.6, "Перечисление");

			recordID = item.ReqTransferID;
			var card3 = new ReqTransferViewModel(recordID, ViewModelState.Edit);

			base.RaiseScenarioFinished();
		}
	}
}
