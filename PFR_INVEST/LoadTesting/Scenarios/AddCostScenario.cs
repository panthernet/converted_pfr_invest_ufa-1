﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddCostScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Временное размещение - Добавить расходы"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка");
            var modelList = new CostsListViewModel();

            base.RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new CostsViewModel() { State = ViewModelState.Create };

            base.RaiseStepChanged(0.5, "Заполнение формы");
            model.Account = (new PortfoliosListViewModel()).PortfolioList.RandomItem(rnd);
            model.SelectedBank = model.BanksList.RandomItem(rnd);
            model.Summ = 0.01M * rnd.Next(10000);
            model.Cost.RegNum = "Тестовой " + rnd.Next(1000);
            model.Cost.PaymentNumber = "Тестовой №" + rnd.Next(1000);
            model.DatePP = DateTime.Now.AddDays(rnd.Next(14) + 1);
            model.Cost.Date = DateTime.Now;
            model.Cost.Kind = "Вознаграждение за сделки по ЦБ|Комиссионное вознаграждение по торгам|Комиссионное вознаграждение за депозитарные услуги".Split('|').ToList().RandomItem(rnd);
            //model.SelectedCurrency = model.CurrencyList.RandomItem(rnd);
			var temp = model.DiDate;

            base.RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            base.RaiseFinishedPercentChanged(1);
        }
    }
}
