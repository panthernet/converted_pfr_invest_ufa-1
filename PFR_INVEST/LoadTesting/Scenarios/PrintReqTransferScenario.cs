﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.ViewModelsList;
namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class PrintReqTransferScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисления средств - Формирование заявки на перечисление "; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();
            base.RaiseFinishedPercentChanged(0.1);
            base.RaiseStepChanged("Загрузка модели списка");
            var model = new TransfersSIListViewModel();

            var list = model.List.Where(t => TransferDirectionIdentifier.IsFromUKToPFR(t.Direction)
                                                    && TransferStatusIdentifier.IsStatusInitialState(t.Status)).ToList();
            var item = list.RandomItem(rnd);

            base.RaiseStepChanged(0.2, "Реестр перечислений");
            var recordID = item.RegisterID;
            var card = new SIVRRegisterViewModel(Document.Types.SI,recordID, ViewModelState.Edit);


            var register = card.TransferRegister;
            if (register == null)
                return;

            base.RaiseStepChanged(0.5, "Создание заявки");

            PrintReqTransfers printReqTransfers = new PrintReqTransfers(register, false, register.RegisterNumber, DateTime.Today);
            printReqTransfers.Print();
            base.RaiseFinishedPercentChanged(1);
        }
    }
}
