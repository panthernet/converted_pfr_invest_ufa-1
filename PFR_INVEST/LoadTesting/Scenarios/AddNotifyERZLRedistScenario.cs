﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddNotifyERZLRedistScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с НПФ - Ув. о перераспр. ЗЛ"; }
        }

        public override void Run()
        {

            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new ZLRedistListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.2));
            RaiseStepChanged(new StepChangedEventArgs("Выбор случайной записи"));
            var id = lmodel.ZLRedistList[rnd.Next(0, lmodel.ZLRedistList.Count - 1)].ID;
            var zlmodel = new ZLRedistViewModel(); zlmodel.LoadZLRedist(id);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
            var model = new ERZLNotifyViewModel() { IsDataChanged = true };

            // фильтруем список НПФ, чтобы нельзя было добавить уведомление на одного и того же НПФ дважды
            if (model.NPFList != null && model.NPFList.Count > 0)
                foreach (var notifyLI in zlmodel.NotifiesList)
                {
                    var tid = notifyLI.ContragentID;
                    model.NPFList = model.NPFList.Where(item => item.ContragentID != tid).ToList();
                }

            model.Init(zlmodel.ID, ViewModelState.Create);



            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            model.SelectedNPF = model.NPFList[rnd.Next(0, model.NPFList.Count - 1)];
            model.NotifyNum = "123";
            model.NPF2PFRCount = 20;
            if (model.ContragentsList_NPF2OTHER != null)
            {
                for (int i = 0; i < 5; i++)
                {
                    var index = rnd.Next(0, model.ContragentsList_NPF2OTHER.Count - 1);
                    model.ContragentsList_NPF2OTHER[index].Count = rnd.Next(1, 50);
                }
                model.RecalculateNPF2OTHER();
            }
            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
