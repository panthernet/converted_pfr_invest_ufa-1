﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddSIDocumentScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Корреспонденция - Добавить документ"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            RaiseStepChanged(0.3, "Загрузка модели формы");
            var model = new DocumentSIViewModel(0, ViewModelState.Create) { State = ViewModelState.Create };

            RaiseStepChanged(0.5, "Заполнение формы");
            model.IncomingNumber = "Нагрузочное " + rnd.Next(1000).ToString();
            model.Comment = "Создано в нагрузочном тестировании";
            model.SelectedLE = model.LEList.RandomItem(rnd);
            model.IncomingDate = DateTime.Now.AddDays(rnd.Next(10) - 5);
            model.OutgoingDate = model.IncomingDate.Value.AddDays(rnd.Next(30));
            model.SelectedDocumentClass = model.DocumentClassList.RandomItem(rnd);

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseFinishedPercentChanged(1.0);
        }
    }
}
