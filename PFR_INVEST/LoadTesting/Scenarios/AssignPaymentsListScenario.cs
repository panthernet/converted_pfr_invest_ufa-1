﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    class AssignPaymentsListScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Отзыв средств - Отзыв средств"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();
            base.RaiseFinishedPercentChanged(0.1);
            base.RaiseStepChanged("Загрузка модели списка");
            var model = new SIAssignPaymentsListViewModel();

            var list = model.APTransfers;
            var item = list.First();

            //base.RaiseStepChanged(0.2, "Реестр перечислений");
            //var recordID = item.RegisterID;
            //var card1 = new SIVRRegisterViewModel(recordID, VIEWMODEL_STATES.Edit);

            //base.RaiseStepChanged(0.3, "Список перечислений");
            //recordID = item.TransferID;
            //var card2 = new OVSITransferViewModel(recordID, VIEWMODEL_STATES.Edit);

            //base.RaiseStepChanged(0.6, "Перечисление");
            //recordID = item.ReqTransferID;
            //var card3 = new OVSIRequestTransferViewModel(recordID, VIEWMODEL_STATES.Edit);
        }
    }
}
