﻿using System;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddTreasurityDocScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Страховые взносы - Добавить документ казначейства"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка СПН");
			var modelList = new DueListViewModel(new DataObjects.ServiceItems.PortfolioTypeFilter() { Types = new DataObjects.Portfolio.Types[] { Portfolio.Types.SPN } });

            base.RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new TreasurityViewModel(ViewModelState.Create, 0,null);

            base.RaiseStepChanged(0.5, "Заполнение формы");

            model.SelectedPeriodID = (long)KDoc.Types.Month;
            model.CanExecuteSave();//Костыть
            model.Regnum = "Тестовой " + rnd.Next(100);
            model.Summ1 = 0.01M * rnd.Next(10000);
            model.Penni1 = 0.01M * rnd.Next(10000);
            model.Penni1Proc = 0.01M * rnd.Next(10000);
            model.Pay1 = 0.01M * rnd.Next(10000);
            model.Other1 = 0.01M * rnd.Next(10000);
            model.Proc1 = 0.01M * rnd.Next(10000);

            model.Summ2 = 0.01M * rnd.Next(10000);
            model.Penni2 = 0.01M * rnd.Next(10000);
            model.Penni2Proc = 0.01M * rnd.Next(10000);
            model.Pay2 = 0.01M * rnd.Next(10000);
            model.Other2 = 0.01M * rnd.Next(10000);
            model.Proc2 = 0.01M * rnd.Next(10000);

            model.Summ3 = 0.01M * rnd.Next(10000);
            model.Penni3 = 0.01M * rnd.Next(10000);
            model.Penni3Proc = 0.01M * rnd.Next(10000);
            model.Pay3 = 0.01M * rnd.Next(10000);
            model.Other3 = 0.01M * rnd.Next(10000);
            model.Proc3 = 0.01M * rnd.Next(10000);

            model.Summ4 = 0.01M * rnd.Next(10000);
            model.Penni4 = 0.01M * rnd.Next(10000);
            model.Penni4Proc = 0.01M * rnd.Next(10000);
            model.Pay4 = 0.01M * rnd.Next(10000);
            model.Other4 = 0.01M * rnd.Next(10000);
            model.Proc4 = 0.01M * rnd.Next(10000);

            model.Summ5 = 0.01M * rnd.Next(10000);
            model.Penni5 = 0.01M * rnd.Next(10000);
            model.Pay5 = 0.01M * rnd.Next(10000);
            model.Other5 = 0.01M * rnd.Next(10000);
            model.Proc5 = 0.01M * rnd.Next(10000);
            //model.DSV = 0.01M * rnd.Next(10000);

            model.SelectedPortfolioID = model.Portfolios.RandomItem(rnd).ID;         

            base.RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            base.RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);
            base.RaiseFinishedPercentChanged(1);

        }
    }
}
