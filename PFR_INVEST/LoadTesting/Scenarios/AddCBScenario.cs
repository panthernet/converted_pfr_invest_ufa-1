﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddCBScenario : ScenarioBase
    {
        //        public const string ScenarioName = "Добавление НПФ";
        public override string ScenarioName
        {
            get { return "Справочники - Добавление ценной бумаги"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new SecuritiesListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
            var model = new SecurityViewModel(0, ViewModelState.Create) { IsDataChanged = true };

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            //model.SelectedSecKind = model.SecurityKindList[rnd.Next(0, model.SecurityKindList.Count - 1)];
            model.CB.Name = "Test CB";
            model.CB.Issue = "Test issue";
            model.CB.SecurityId = "Test security ID";
            model.CB.RegNum = "Test reg num";
            model.CB.ISIN = "Test ISIN";
            model.CB.RepaymentDate = DateTime.Now;
            model.CB.LocationDate = DateTime.Now;
            model.CB.IssueVolume = 10;
            model.CB.PlacedVolume = 10;
            model.CB.NomValue = 9;
            model.CB.PaymentPeriod = 2;
            model.SelectedCurrency = model.CurrencyList[rnd.Next(0, model.CurrencyList.Count - 1)];

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Проверка на сохранение не пройдена!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
