﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    using PFR_INVEST.DataObjects;
	using PFR_INVEST.BusinessLogic.ViewModelsList;

    public class TransferListScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисления средств - Перечисления"; }
        }

        public override void Run()
        {
            try
            {
                Login();
                var rnd = new Random();
                base.RaiseFinishedPercentChanged(0.1);
                base.RaiseStepChanged("Загрузка модели списка");
                var model = new TransfersSIListViewModel();

                var list = model.List;
                var item = list.First();

                base.RaiseStepChanged(0.2, "Реестр перечислений");
                var recordID = item.RegisterID;
                var card1 = new SIVRRegisterViewModel(Document.Types.SI, recordID, ViewModelState.Edit);

                base.RaiseStepChanged(0.3, "Список перечислений");
                recordID = item.TransferID;
                var card2 = new TransferViewModel(recordID, ViewModelState.Edit);

                base.RaiseStepChanged(0.6, "Перечисление");
                recordID = item.ReqTransferID;
                var card3 = new ReqTransferViewModel(recordID, ViewModelState.Edit);
            }
            catch (Exception ex) { RaiseException(ex); }
            finally { try { RaiseScenarioFinished(new EventArgs()); } catch { } }
        }
    }
}
