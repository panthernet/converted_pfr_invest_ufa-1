﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class TestScenario:ScenarioBase
	{
		public override string ScenarioName
		{
			get { return "Тестовой - Метрика"; }
		}

		public override void Run()
		{		
			
			List<ScenarioBase> scenarios = new List<ScenarioBase>();
			
			scenarios.Add(new AddDueScenario());
			scenarios.Add(new AddDuePrecise());
			scenarios.Add(new AddPrepaymentScenario());
			scenarios.Add(new AddPrepaymentPreciseScenario());
			scenarios.Add(new AddDueDeadScenario());
			scenarios.Add(new AddDueDeadPreciseScenario());
			scenarios.Add(new AddDueExcessScenario());
			scenarios.Add(new AddDueExcessPreciseScenario());
			scenarios.Add(new AddDueUndistributedScenario());
			scenarios.Add(new AddDueUndistributedPreciseScenario());
			scenarios.Add(new AddTreasurityDocScenario());
			
			scenarios.Add(new AddEnrollmentPercentsScenario());
			scenarios.Add(new AddEnrollmentOtherScenario());
			scenarios.Add(new AddWidthdrawScenario());
			scenarios.Add(new AddTransferScenario());
			scenarios.Add(new CostsListScenario());
			scenarios.Add(new AddCostScenario());
			scenarios.Add(new SchilsCostsListScenario());
			scenarios.Add(new AddBudgetCorrectionScenario());
			scenarios.Add(new AddDirectionScenario());
			scenarios.Add(new AddTransferSchilsScenario());
			scenarios.Add(new AddReturnScenario());
			scenarios.Add(new BalanceListScenario());
			scenarios.Add(new TempAllocationListScenario());
			scenarios.Add(new TransferToNPFListScenario());
			scenarios.Add(new UKTransferListScenario());
			scenarios.Add(new AddDeadERZLScenario());
			scenarios.Add(new AddERZLNotifyScenario());
			scenarios.Add(new AddFinregisterScenario());
			scenarios.Add(new AddNotifyERZLRedistScenario());
			scenarios.Add(new AddRegisterScenario());
			scenarios.Add(new OpenRegisterArchiveScenario());
			scenarios.Add(new AddInsuranceScenario());
			scenarios.Add(new AddOVSIRequestTransferScenario());
			scenarios.Add(new AddOVSITransferScenario());
			scenarios.Add(new AddSIDocumentScenario());
			scenarios.Add(new ArchiveSIDocumentsListScenario());
			scenarios.Add(new ArchiveTransferListScenario());
			scenarios.Add(new AssignPaymentsListScenario());
			scenarios.Add(new ControlSIDocumentsListScenario());
			scenarios.Add(new InsuranceArchiveListScenario());
			scenarios.Add(new InsuranceListScenario());
			scenarios.Add(new PlanCorrectionWizardScenario());
			scenarios.Add(new PrintReqTransferScenario());
			scenarios.Add(new SIDocumentsListScenario());
			scenarios.Add(new SPNMovementsListScenario());
			scenarios.Add(new TransferListScenario());
			scenarios.Add(new TransferWizardScenario());
			scenarios.Add(new ZLMovementsListScenario());
			scenarios.Add(new PrintRequestScenario());
			scenarios.Add(new AddBankScenario());
			scenarios.Add(new AddCBPaymentScenario());
			scenarios.Add(new AddCBScenario());
			scenarios.Add(new AddContractScenario());
			scenarios.Add(new AddDopAgreementScenario());
			scenarios.Add(new AddNPFScenario());
			scenarios.Add(new AddPFRBankAccountScenario());
			scenarios.Add(new AddSIScenario());

			File.Delete("d:\\PFR_LOG.txt");
			foreach (var sc in scenarios)
			{
				Stopwatch sw = Stopwatch.StartNew();
				string Error = "";
				try
				{
					sc.Run();
				}
				catch (Exception ex)
				{
					Error = ex.Message;
				}
				finally
				{
					sw.Stop();
				}
				string message = string.Format("{0}|{1}|{2}\r\n", sc.ScenarioName, sw.Elapsed.Seconds, Error);
				base.RaiseStepChanged(message);
				File.AppendAllText("d:\\PFR_LOG.txt", message);

			}
		}
	}
}
