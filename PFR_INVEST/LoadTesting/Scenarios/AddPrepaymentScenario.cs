﻿using System;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddPrepaymentScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Страховые взносы - Добавить авансовый платеж"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка СПН");
			var modelList = new DueListViewModel(new DataObjects.ServiceItems.PortfolioTypeFilter() { Types = new DataObjects.Portfolio.Types[] { Portfolio.Types.SPN } });

            base.RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new PrepaymentViewModel(PortfolioIdentifier.PortfolioTypes.SPN) { State = ViewModelState.Create };


            base.RaiseStepChanged(0.5, "Заполнение формы");
            model.SrcAccount = (new PortfoliosListViewModel()).PortfolioList.RandomItem(rnd);
            model.DstAccount = (new PortfoliosListViewModel()).PortfolioList.RandomItem(rnd);

            model.DocDate = DateTime.Now;
            model.RegNum= "Тестовой " + rnd.Next(1000).ToString();
            model.Summ = 0.01M * rnd.Next(10000);

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);

            base.RaiseFinishedPercentChanged(1);
        }
    }
}
