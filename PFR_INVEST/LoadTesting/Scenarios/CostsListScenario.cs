﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class CostsListScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Временное размещение - Расходы по сделкам ЦБ"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка");
            var modelList = new CostsListViewModel();
            var list = modelList.List;

            var count = rnd.Next(5) + 5;
            base.RaiseFinishedPercentChanged(0.2);
            for (int i = 0; i < count; i++)
            {
                var item = list.RandomItem(rnd);
                if (item == null)
                    continue;
                base.RaiseStepChanged("Открытие карточки " + (i + 1).ToString());
                var recordID = item.ID;
                var card = new CostsViewModel();
                card.State = ViewModelState.Edit;
                card.Load(recordID);
            }


            base.RaiseFinishedPercentChanged(1);
        }
    }
}
