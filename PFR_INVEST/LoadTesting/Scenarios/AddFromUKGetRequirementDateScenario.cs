﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddFromUKGetRequirementDateScenario : ScenarioBase
    {

        public override string ScenarioName
        {
            get { return "Расбота с СИ - Перечисление средств - Перечисление из УК - Ввести дату вручения требования"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();
            base.RaiseStepChanged(0.1, "Загрузка модели списка");
            var modelList = new TransfersSIListViewModel();

            var list = modelList.List.Where(t => TransferDirectionIdentifier.IsFromUKToPFR(t.Direction)
                                                        && TransferStatusIdentifier.IsStatusSPNTransfered(t.Status)).ToList();
            var item = list.Skip(rnd.Next(list.Count - 1)).First();

            RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new ReqTransferViewModel(item.ReqTransferID, ViewModelState.Edit) { State = ViewModelState.Edit };

            RaiseStepChanged(0.3, "Заполнение формы");
            model.RequestDeliveryDate = DateTime.Now;
            model.SetSPNTransferDate = DateTime.Now.AddDays(1);
            model.TransferStatus = TransferStatusIdentifier.sDemandReceivedUK;

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);


            base.RaiseScenarioFinished();
        }
    }
}
