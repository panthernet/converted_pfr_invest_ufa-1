﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddContractScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Справочники - Добавление Контракта"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new SIContractsListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.2));
            RaiseStepChanged(new StepChangedEventArgs("Выбор случайного СИ"));
            var lelist = new SIListViewModel(false);
            var leid = lelist.SIList[rnd.Next(0, lelist.SIList.Count - 1)].LegalEntityID;

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
            var model = new SIContractViewModel(leid, ViewModelState.Create) { IsDataChanged = true };

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            model.SelectedContractName = model.ContractNamesList[rnd.Next(0, model.ContractNamesList.Count - 1)];
            model.ContractDate = DateTime.Now;
            model.ContractNumber = "12345678901234567890";
            model.Duration = 10;

            model.SelectedDocumentType = model.DocumentTypesList[rnd.Next(0, model.DocumentTypesList.Count - 1)];
            model.DocumentNumber = "1234567890";
            model.DocumentDate = DateTime.Now;

            model.BankAccountLegalEntityName = "Test bank LE";
            model.BankAccountBankName = "Test bank name";
            model.BankAccountAccountNumber = "12345678901234567890";
            model.BankAccountBankLocation = "Test bank location";
            model.BankAccountBIK = "123456789";
            model.BankAccountINN = "0123456789";
            model.BankAccountKPP = "123456789";
            model.BankAccountCorrespondentAccountNumber = "12345678901234567890";

            model.ContrInvestGCBRFMIN = 10;
            model.ContrInvestGCBRFMAX = 90;
            model.ContrInvestGCBSMIN = 10;
            model.ContrInvestGCBSMAX = 90;
            model.ContrInvestOBLMIN = 10;
            model.ContrInvestOBLMAX = 90;
            model.ContrInvestIPMIN = 10;
            model.ContrInvestIPMAX = 90;
            model.ContrInvestSRMIN = 10;
            model.ContrInvestSRMAX = 90;
            model.ContrInvestDEPMIN = 10;
            model.ContrInvestDEPMAX = 90;
            model.ContrInvestSRVALMIN = 10;
            model.ContrInvestSRVALMAX = 90;

            model.RewardCostAmountReward = 80;
            model.RewardCostMaxCost = 90;
            model.RewardCostMaxPaySD = 20;


            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Проверка на сохранение не пройдена!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
