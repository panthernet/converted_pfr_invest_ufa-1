﻿using System;
using PFR_INVEST.BusinessLogic.Journal;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ViewUserActionsScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Настройки - Просмотр действий пользователя"; }
        }

        public override void Run()
        {
            try
            {
                Login();
                var rnd = new Random();

                //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
                //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
                //RaiseScenarioFinished(new EventArgs());

                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.2));
                RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
                var model = new UserStatisticChooseViewModel();

                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
                RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
                //model.Filter.UserID = "testuser1";
                model.Filter.ObjectName = model.JournalObjectList[rnd.Next(0, model.JournalObjectList.Count - 1)];


                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.8));
                RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
                model.RefreshEntryList();

                //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
                //model.ExecuteDeleteTest(DocOperation.Archive);

                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
            }
            catch (Exception ex) { RaiseException(ex); }//MessageBox.Show(ex.Message + " " + ex.GetType()); }
            finally { try { RaiseScenarioFinished(new EventArgs()); } catch { } }

        }
    }
}
