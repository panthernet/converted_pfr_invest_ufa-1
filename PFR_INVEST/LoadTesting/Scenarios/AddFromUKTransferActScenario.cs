﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddFromUKTransferActScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Расбота с СИ - Перечисление средств - Перечисление из УК - Ввести дату Акта передачи"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();
            base.RaiseStepChanged(0.1, "Загрузка модели списка");
            var modelList = new TransfersSIListViewModel();

            var list = modelList.List.Where(t => TransferDirectionIdentifier.IsFromUKToPFR(t.Direction)
                                                        && TransferStatusIdentifier.IsStatusRequestReceivedByUK(t.Status)).ToList();
            var item = list.Skip(rnd.Next(list.Count - 1)).First();

            RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new ReqTransferViewModel(item.ReqTransferID, ViewModelState.Edit);

            RaiseStepChanged(0.3, "Заполнение формы");
            model.TransferActDate = DateTime.Now;
            model.TransferActNumber = model.Transfer.ID.ToString();
            model.TransferStatus = TransferStatusIdentifier.sActSigned;

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);


            base.RaiseScenarioFinished();
        }
    }
}
