﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ComplexSIScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Комплексный - Работа с СИ"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            var scenarios = new List<ScenarioBase>();
            scenarios.Add(new AddInsuranceScenario());
            scenarios.Add(new AddOVSIRequestTransferScenario());
            scenarios.Add(new AddOVSITransferScenario());
            scenarios.Add(new AddSIDocumentScenario());
            scenarios.Add(new ArchiveSIDocumentsListScenario());
            scenarios.Add(new ArchiveTransferListScenario());
            scenarios.Add(new AssignPaymentsListScenario());
            scenarios.Add(new ControlSIDocumentsListScenario());
            scenarios.Add(new InsuranceArchiveListScenario());
            scenarios.Add(new InsuranceListScenario());
            scenarios.Add(new PlanCorrectionWizardScenario());
            scenarios.Add(new PrintReqTransferScenario());
            scenarios.Add(new SIDocumentsListScenario());
            scenarios.Add(new SPNMovementsListScenario());
            scenarios.Add(new TransferListScenario());
            scenarios.Add(new TransferWizardScenario());
            scenarios.Add(new ZLMovementsListScenario());
            scenarios.Add(new PrintRequestScenario());

            RunInnerScenarioCycle(scenarios);
        }
    }
}
