﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class BalanceListScenario : ScenarioBase
	{
		public override string ScenarioName
		{
			get { return "Бэк-офис - Сальдо - Сальдо"; }
		}

		public override void Run()
		{
			base.Login();
			var rnd = new Random();

			base.RaiseStepChanged("Загрузка модели списка");
			var modelList = new BalanceListViewModel(null);

			base.RaiseFinishedPercentChanged(1);
		}
	}
}
