﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddDuePrecise : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Страховые взносы - Добавить уточнение страховых взносов"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged("Загрузка модели списка СПН");
			var modelList = new DueListViewModel(new DataObjects.ServiceItems.PortfolioTypeFilter() { Types = new DataObjects.Portfolio.Types[] { Portfolio.Types.SPN } });

            var list = modelList.DueList.Where(d => d.Operation == DueDocKindIdentifier.Due).ToList();
            var item = list.RandomItem(rnd);

            base.RaiseStepChanged(0.2, "Загрузка модели формы");
            var modelCard = new DueViewModel(item.ID, PortfolioIdentifier.PortfolioTypes.SPN);
            if (modelCard.Account == null)
            {
                base.RiseEmptySequence();
                return;
            }
            var model = new MonthAccurateViewModel(ViewModelState.Create, modelCard.ID);

            base.RaiseStepChanged(0.5, "Заполнение формы");
            model.DocDate = DateTime.Now;
            model.RegNum = "Тестовой " + rnd.Next(1000);
            model.MSumm = 0.01M * rnd.Next(10000) - 50M;
            var temp = model.ChSumm; // дёрганье для вычисления

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);
            base.RaiseFinishedPercentChanged(1);
        }
    }
}
