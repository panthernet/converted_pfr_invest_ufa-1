﻿using DevExpress.Xpf.Docking.Base;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.NavBar;

namespace PFR_INVEST.Helpers.Localizers
{
    public class DLLocalizer : DockingLocalizer
    {
        public override string GetLocalizedString(DockingStringId id)
        {
            switch (id)
            {
                case DockingStringId.ControlButtonMinimize: return "Свернуть";
                case DockingStringId.ControlButtonMaximize: return "Развернуть";
                case DockingStringId.ControlButtonRestore: return "Восстановить";
                case DockingStringId.ControlButtonClose: return "Закрыть";
                case DockingStringId.ControlButtonAutoHide: return "Автоскрытие";
                case DockingStringId.MenuItemFloat: return "Открепить";
                case DockingStringId.MenuItemHide: return "Закрыть";
                case DockingStringId.MenuItemAutoHide: return "Автоскрытие";
                case DockingStringId.MenuItemDock: return "Докировать";
                case DockingStringId.MenuItemLeft: return "Слева";
                case DockingStringId.MenuItemRight: return "Справа";

                default: return base.GetLocalizedString(id);
            }
        }
    }

    public class GCLocalizer : GridControlLocalizer
    {
        public override string GetLocalizedString(GridControlStringId id)
        {
            switch (id)
            {
                case GridControlStringId.MenuGroupPanelFullExpand: return "Полностью раскрыть";
                case GridControlStringId.MenuGroupPanelFullCollapse: return "Полностью свернуть";
                case GridControlStringId.MenuGroupPanelClearGrouping: return "Очистить группирование";
                case GridControlStringId.GridGroupPanelText: return "Перетащите сюда заголовок колонки для формирования группы по этой колонке";
                case GridControlStringId.MenuColumnSortAscending: return "Сортировать по возрастающей";
                case GridControlStringId.MenuColumnSortDescending: return "Сортировать по убывающей";
                case GridControlStringId.MenuColumnClearSorting: return "Очистить сортировку";
                case GridControlStringId.MenuColumnGroup: return "Группировать по колонке";
                case GridControlStringId.MenuColumnHideGroupPanel: return "Убрать группировку по колонкам";
                case GridControlStringId.MenuColumnShowColumnChooser: return "Выбрать колонки...";
                case GridControlStringId.MenuColumnBestFit: return "Авторазмер";
                case GridControlStringId.MenuColumnBestFitColumns: return "Авторазмер (все колонки)";
                case GridControlStringId.MenuColumnFilterEditor: return "Редактор фильтров...";
                case GridControlStringId.ColumnChooserCaption: return "Выбор колонок";
                case GridControlStringId.FilterEditorTitle: return "Редактор фильтров";
                case GridControlStringId.PopupFilterBlanks: return "(Пустые)";
                case GridControlStringId.PopupFilterNonBlanks: return "(Со значением)";
                case GridControlStringId.PopupFilterAll: return "(Все)";
                case GridControlStringId.MenuColumnShowGroupPanel: return "Показать группировку по колонкам";
                case GridControlStringId.ColumnChooserDragText: return "Перетащите колонку сюда";
                case GridControlStringId.MenuColumnHideColumnChooser: return "Скрыть выбор колонок";

                default: return base.GetLocalizedString(id);
            }
        }
    }

    public class NBLocalizer : NavBarLocalizer
    {
        public override string GetLocalizedString(NavBarStringId id)
        {
            switch (id)
            {

                default: return base.GetLocalizedString(id);
            }
        }
    }

    public class EDLocalizer : EditorLocalizer
    {
        public override string GetLocalizedString(EditorStringId id)
        {
            switch (id)
            {
                case EditorStringId.FilterPanelClearFilter: return "Очистить фильтр";
                case EditorStringId.FilterPanelEditFilter: return "Редактировать фильтр";
                case EditorStringId.FilterPanelEnableFilter: return "Включить фильтр";
                case EditorStringId.FilterPanelDisableFilter: return "Выключить фильтр";
                case EditorStringId.OK: return "ОК";
                case EditorStringId.Cancel: return "Отмена";
                case EditorStringId.Clear: return "Сброс";
                case EditorStringId.Apply: return "Принять";
                case EditorStringId.FilterToolTipNodeAction: return "Действия";
                case EditorStringId.FilterToolTipNodeAdd: return "Добавить новое условие в группу";
                case EditorStringId.FilterGroupAnd: return "И";
                case EditorStringId.FilterGroupOr: return "ИЛИ";
                case EditorStringId.FilterClauseEquals: return "РАВНО";
                case EditorStringId.FilterToolTipKeysRemove: return "(Используйте кнопку Удалить или Вычесть)";
                case EditorStringId.FilterToolTipNodeRemove: return "Удалить условие";
                case EditorStringId.FilterToolTipKeysAdd: return "(Используйте кнопку Вставить или Добавить)";
                default: return base.GetLocalizedString(id);
            }
        }
    }
}
