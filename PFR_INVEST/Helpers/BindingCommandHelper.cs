﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PFR_INVEST.Helpers
{
    /// <summary>
    /// Хелпер для связи кода вьюхи с моделью
    /// В коде вызываем Command 
    /// В разметке биндим Command на нужное свойство модели
    /// </summary>
    public class BindingCommandHelper:Control
    {
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(BindingCommandHelper), new UIPropertyMetadata(null));        
    }
}
