﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DevExpress.Xpf.PivotGrid.Printing;
using DevExpress.Xpf.Printing;

namespace PFR_INVEST.Helpers
{
    /// <summary>
    /// Используется для настроек (перенос строк в ячейке) экспорта PivotGrid в аналитическом блоке в XAML разметке
    /// </summary>
    public class CustomTextExporter : TextExporter, ITextExportSettings
    {
        public HorizontalAlignment TextAlignment { get; set; }
        TextWrapping ITextExportSettings.TextWrapping
        {
            get { return TextWrapping.Wrap; }
        }

        HorizontalAlignment ITextExportSettings.HorizontalAlignment
        {
            get { return TextAlignment; }
        }
    }

}
