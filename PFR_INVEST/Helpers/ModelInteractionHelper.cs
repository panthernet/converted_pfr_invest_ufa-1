﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Data;
using DevExpress.Xpf.Docking;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Dialogs;

namespace PFR_INVEST.Helpers
{
    /// <summary>
    /// Контейнер методов для упрощения взаимодействий модели и представления
    /// </summary>
    public static class ModelInteractionHelper
    {
        /// <summary>
        /// Реализует необходимые методы на стороне формы для работы с моделями IPagedLoadListModel
        /// </summary>
        /// <param name="ctrl">Контрол, использующий модель IPagedLoadListModel</param>
        public static void SubscribeForPageLoadModel(UserControl ctrl)
        {
            ctrl.Loaded += (sender, args) =>
            {
                var model = ctrl.DataContext as IPagedLoadListModel;
                if (model == null) return;
                model.PropertyChanged += RaiseMessageOnFault;
            };

            ctrl.Unloaded += (sender, args) =>
            {
                var model = ctrl.DataContext as IPagedLoadListModel;
                if (model == null) return;
                model.PropertyChanged -= RaiseMessageOnFault;
                model.IsClosed = true;
            };
        }

        private static void RaiseMessageOnFault(object sender, PropertyChangedEventArgs e)
        {
            var model = sender as IPagedLoadListModel;
            if (e.PropertyName.Contains("IsFaulted") && model.IsFaulted)
                ViewModelBase.DialogHelper.ShowError("Произошла ошибка при загрузке страницы данных");
        }

        /// <summary>
        /// Принудительно выводит диалоговое окно поверх всех остальных. исключает баг с закидыванием окна в бэкграунд
        /// </summary>
        /// <param name="window">Окно</param>
        public static void ForceDialogWindowToForeground(Window window)
        {
            window.Loaded += (sender, args) =>
            {
                window.Activate();
                window.Topmost = true;  // important
                window.Topmost = false; // important
                window.Focus();         // important
            };
        }

        private static readonly List<GridRefreshHelper> GrhList = new List<GridRefreshHelper>();

        /// <summary>
        /// Подписывает форму на авто корректировку таб стопов при загрузке
        /// </summary>
        /// <param name="ctrl">Контрол</param>
        /// <param name="obj">Контент контрола (обысно свойство Content)</param>
        public static void SubscribeForControlTabStops(UserControl ctrl, object obj)
        {
            ctrl.Loaded += (sender, args) => ControlsTabStoping.Apply(obj as DependencyObject);
        }

        public static void SubscribeForControlTabStops(Window wnd, object obj)
        {
            wnd.Loaded += (sender, args) => ControlsTabStoping.Apply(obj as DependencyObject);
        }

        /// <summary>
        /// Добавляет поддержку GridRefreshHelper для грида контрола
        /// </summary>
        /// <param name="ctrl">Контрол</param>
        /// <param name="grid">Грид</param>
        public static void SubscribeForGridRefreshHelper(UserControl ctrl, GridControl grid)
        {
            //очищаем старые (на всякий случай, если хелпер используется не в док панели)
            GrhList.Where(a => a.Grid == null || a.Parent == null).ToList().ForEach(a =>
            {
                a.OnClose();
                GrhList.Remove(a);
            });
            //добавляем новый
            GrhList.Add(new GridRefreshHelper(ctrl, grid));
        }

        /// <summary>
        /// Отписка от GridRefreshHelper и очистка ресурсов (вызывается автоматически при закрытии ПАНЕЛИ)
        /// </summary>
        /// <param name="ctrl"></param>
        public static void UnsubscribeForGridRefreshHelper(UserControl ctrl)
        {
            //очищаем старые
            var item = GrhList.FirstOrDefault(a => ReferenceEquals(a.Parent, ctrl));
            if (item == null) return;
            item.OnClose();
            GrhList.Remove(item);
        }

        /// <summary>
        /// Подписывает окно на автоматическое закрытие по событию модели IRequestCloseViewModel. Указывается в конструкторе View.
        /// </summary>
        /// <param name="wnd">Окно (View)</param>
        /// <param name="method">Метод для выполенения по событию (опционально)</param>
        /// <param name="condition">Доп. условие для проверки на валидность закрытия формы (опционально)</param>
        public static void SignUpForCloseRequest(Window wnd, Action<object> method = null, Func<bool, bool> condition = null)
        {            
            if (wnd == null) return;
            EventHandler a = delegate(object sndr, EventArgs e)
            {
                if (condition != null && !condition(sndr is bool && !(bool)sndr)) return;
                if (sndr is bool) wnd.DialogResult =(bool) sndr;
                //else wnd.DialogResult = false;
                method?.Invoke(sndr);
            };

            wnd.DataContextChanged += (sender, args) =>
            {
                var model = args.NewValue as IRequestCloseViewModel;
                if (model != null)
                    model.RequestClose += a;
                model = args.OldValue as IRequestCloseViewModel;
                if (model != null && args.NewValue == null)
                    model.RequestClose -= a;
            };
            wnd.Unloaded += delegate
            {
                var model = wnd.DataContext as IRequestCloseViewModel;
                if (model != null)
                    model.RequestClose -= a;
            };
        }

        /// <summary>
        /// Подписка на закрытие UI окна
        /// </summary>
        /// <param name="ctrl">Контрол VIEW</param>
        /// <param name="precise">Точное закрытие по поиску в иерархии окон, иначе текущее выьранное окно</param>
        public static void SignUpForCloseRequest(Control ctrl, bool precise = false)
        {
            if (ctrl == null) return;
            EventHandler a = delegate
            {
                if (!precise)
                    DashboardManager.CloseActiveView();
                else
                {
                    var panel = ctrl.Parent as LayoutPanel;
                    if (panel != null)
                        DashboardManager.Instance.DockManager.DockController.Close(panel);
                }
            };

            ctrl.DataContextChanged += (sender, args) =>
            {
                var model = args.NewValue as IRequestCloseViewModel;
                if (model != null)
                    model.RequestClose += a;
                model = args.OldValue as IRequestCloseViewModel;
                if (model != null && args.NewValue == null)
                    model.RequestClose -= a;
            };


        }

        /// <summary>
        /// Подписывает окно на кастомное действие по событию модели IRequestCustomAction. Указывается в конструкторе View.
        /// </summary>
        /// <param name="ctrl">Контрол (View)</param>
        /// <param name="method">Метод для выполенения по событию</param>
        /// <param name="unsub">Отписываться от событий при смене контекста</param>
        public static void SignUpForCustomAction(UserControl ctrl, Action<object> method, bool unsub = true)
        {
            if (ctrl == null) return;
            EventHandler a = delegate(object o, EventArgs e) { method(o); };

            ctrl.DataContextChanged += (sender, args) =>
            {
                var action = args.NewValue as IRequestCustomAction;
                if (action != null)
                    action.RequestCustomAction += a;
                if (unsub)
                {
                    action = args.OldValue as IRequestCustomAction;
                    if (action != null && args.NewValue == null)
                        action.RequestCustomAction -= a;
                }
            };
            if (unsub)
            {
                ctrl.Unloaded += (sender, args) =>
                {
                    var model = ctrl.DataContext as IRequestCustomAction;
                    if (model != null)
                        model.RequestCustomAction -= a;
                };
            }
        }

        /// <summary>
        /// Подписывает окно на обновление грида при смене значения указанного свойства контекста
        /// </summary>
        /// <param name="ctrl">Контрол</param>
        /// <param name="grid">Грид</param>
        /// <param name="propName">Наименование свойств контекста</param>
        public static void SubscribeForGridRefreshData(Control ctrl, GridControl grid, string propName)
        {
            if (ctrl == null) return;
            PropertyChangedEventHandler action = delegate(object o, PropertyChangedEventArgs e)
            {
                if (e.PropertyName == propName)
                    grid.RefreshData();
            };
            ctrl.DataContextChanged += (sender, e) =>
            {
                var value = e.NewValue as ViewModelBase;
                if (value != null)
                    value.PropertyChanged += action;
                else
                {
                    value = e.OldValue as ViewModelBase;
                    if (value != null) value.PropertyChanged -= action;
                }
            };
            ctrl.Unloaded += (sender, e) =>
            {
                var value = ctrl.DataContext as ViewModelBase;
                if (value != null)
                    value.PropertyChanged -= action;
            };
        }

        /// <summary>
        /// Подписка на обработку вызова по отображению/скрытию окна загрузки. sender события должен содержать сообщение для отображения
        /// </summary>
        /// <param name="ctrl">Контрол</param>
        public static void SubscribeForLoadingIndicator(Control ctrl)
        {
            ctrl.DataContextChanged += (sender, e) =>
            {
                var value = e.NewValue as IRequestLoadingIndicator;
                if (value != null)
                    value.RequestLoadingIndicator += ShowLoadingIndicator;
                var oValue = e.OldValue as IRequestLoadingIndicator;
                if (oValue != null && e.NewValue == null)
                    oValue.RequestLoadingIndicator -= ShowLoadingIndicator;
            };
            ctrl.Unloaded += (sender, e) =>
            {
                var value = ctrl.DataContext as IRequestLoadingIndicator;
                if(value != null)
                    value.RequestLoadingIndicator -= ShowLoadingIndicator;
            };
        }

        private static void ShowLoadingIndicator(object sender, EventArgs e)
        {
            if (sender != null)
                Loading.ShowWindow(sender.ToString());
            else Loading.CloseWindow();
        }

        public static void SubscribeForTopmostRequest(Window wnd)
        {
            wnd.DataContextChanged += (sender, e) =>
            {
                var value = e.NewValue as IRequestTopmost;
                if (value != null)
                    value.RequestTopMost += (s, a) => App.mainWindow.Topmost = s != null;
            };
        }
        
        public static void SignUpForDevExpressCustomColumnSort(GridControl grid, string fieldName, DevExpressColumnSortType sortType)
        {
            grid.CustomColumnSort += (sender, e) =>
            {
                if (e.Column.FieldName == fieldName)
                {
                    switch (sortType)
                    {
                        case DevExpressColumnSortType.Int:
                            var i1 = e.Value1 == null ? 0 : Convert.ToInt32(e.Value1);
                            var i2 = e.Value2 == null ? 0 : Convert.ToInt32(e.Value2);
                            e.Result = e.SortOrder == ColumnSortOrder.Descending ? i2.CompareTo(i1) : i1.CompareTo(i2);
                            break;
                        case DevExpressColumnSortType.Decimal:
                            var d1 = e.Value1 == null ? 0 : Convert.ToInt32(e.Value1);
                            var d2 = e.Value2 == null ? 0 : Convert.ToInt32(e.Value2);
                            e.Result = e.SortOrder == ColumnSortOrder.Descending ? d2.CompareTo(d1) : d1.CompareTo(d2);
                            break;
                        case DevExpressColumnSortType.Month:
                            //month sorting
                            var v1 = e.Value1?.ToString() ?? string.Empty;
                            var v2 = e.Value2?.ToString() ?? string.Empty;
                            int index1 = DateUtil.GetMonthIndex(v1.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(),
                                e.SortOrder == ColumnSortOrder.Descending);
                            int index2 = DateUtil.GetMonthIndex(v2.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(),
                                e.SortOrder == ColumnSortOrder.Descending);
                            e.Result = e.SortOrder == ColumnSortOrder.Descending ? index2.CompareTo(index1) : index1.CompareTo(index2);
                            break;
                    }
                }
            };
        }

        private static string GetFirstWord(string monthRecord)
        {
            if (monthRecord == null) return string.Empty;
            var m = Regex.Match(monthRecord, "^\\w+");
            return m.Success ? m.Value : string.Empty;
        }

        public static void SignUpForDevExpressCustomColumnPopupSort(GridControl grid, string fieldName, DevExpressColumnSortType sortType)
        {
            grid.View.ShowFilterPopup += (sender, e) =>
            {
                if (sortType == DevExpressColumnSortType.Month)
                {
                    //month sorting
                    var source = e.ComboBoxEdit.ItemsSource as List<object>;
                    var sortedItems = new List<CustomComboBoxItem>();
                    var commonItems = new List<CustomComboBoxItem>();
                    var trailingItems = new List<CustomComboBoxItem>();
                    foreach (var o in source)
                    {
                        var item = o as CustomComboBoxItem;
                        var displayvalue = item?.DisplayValue as string;
                        if (displayvalue != null)
                        {
                            if (Regex.IsMatch(displayvalue, "^\\(.*\\)$"))
                            {
                                commonItems.Add(item);
                            }
                            else if (DateUtil.GetMonthIndex(GetFirstWord(displayvalue)) == -1)
                            {
                                if (!trailingItems.Exists(x => displayvalue.Equals(x.DisplayValue as string)))
                                    trailingItems.Add(item);
                            }
                            else if (!sortedItems.Exists(x => displayvalue.Equals(x.DisplayValue as string)))
                            {
                                sortedItems.Add(item);
                            }
                        }
                    }

                    sortedItems.Sort((x, y) => DateUtil.GetMonthIndex(GetFirstWord(x.DisplayValue as string), e.Column.SortOrder == ColumnSortOrder.Descending) - DateUtil.GetMonthIndex(GetFirstWord(y.DisplayValue as string), e.Column.SortOrder == ColumnSortOrder.Descending));
                    commonItems.AddRange(sortedItems);
                    commonItems.AddRange(trailingItems);
                    e.ComboBoxEdit.ItemsSource = commonItems;
                    return;
                }
                var list = ((List<object>) e.ComboBoxEdit.ItemsSource).OrderBy(a =>
                    {
                        var aItem = a as CustomComboBoxItem;
                        if (aItem == null) return 0;
                        var value = aItem.EditValue;
                        switch (sortType)
                        {
                            case DevExpressColumnSortType.Int:
                                int iResult;
                                return !int.TryParse(value.ToString(), out iResult) ? 0 : iResult;
                            case DevExpressColumnSortType.Decimal:
                                decimal dResult;
                                return !decimal.TryParse(value.ToString(), out dResult) ? 0 : dResult;
                            default:
                                throw new Exception("SignUpForDevExpressCustomColumnPopupSort - Type not supported!");
                        }
                    })
                    .GroupBy(a => (a as CustomComboBoxItem)?.EditValue?.ToString())
                    .Select(group => group.First());
                e.ComboBoxEdit.ItemsSource = list;
            };
        }
    }
}
