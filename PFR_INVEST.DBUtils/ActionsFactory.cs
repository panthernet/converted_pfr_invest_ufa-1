﻿namespace PFR_INVEST.DBUtils
{

    public static class ActionsFactory
    {
        public static IDBUtil Create(string action, ExecutionContext executionContext)
        {
            IDBUtil idbu = null;

            switch (action)
            {
                case "RESETIDENTITYSTARTVALUE":
                    idbu = new ResetIdentityStartValue(executionContext);
                    break;
            }

            return idbu;
        }
    }
}
