﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.IO;
using System.Text;
using PFR_INVEST.Common.Logger;

namespace PFR_INVEST.DBUtils
{
    class Program
    {

        static void Main(string[] args)
        {

            string schemeName = ConfigurationManager.AppSettings.Get("SchemeName");
            string dsn = ConfigurationManager.AppSettings.Get("DSN");
            string databaseName = ConfigurationManager.AppSettings.Get("DatabaseName");
            string hostName = ConfigurationManager.AppSettings.Get("HostName");
            string port = ConfigurationManager.AppSettings.Get("Port");
            string userName = ConfigurationManager.AppSettings.Get("UserName");
            string password = ConfigurationManager.AppSettings.Get("Password");
            List<string> executeActions = ConfigurationManager.AppSettings.Get("ExecuteActions").ToUpper().Split(';').ToList();

            string fileLog = Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, "Log", DateTime.Now.Date.ToString("dd-MM-yyyy") + ".txt");

            string connectionString = string.Empty;
            if (String.IsNullOrEmpty(dsn))
            {
                connectionString =
                    string.Format(
                        "Driver=IBM DB2 ODBC DRIVER;Database={0};Hostname={1};Protocol=TCPIP;Port={2};Uid={3};Pwd={4};",
                        databaseName, hostName, port, userName, password);
            }
            else
            {


                connectionString = string.Format("DSN={0}", dsn);
            }

            var severity = ConfigurationManager.AppSettings.Get("LogSeverity");
            LogSeverity severityValue;
            if (string.IsNullOrEmpty(severity))
                severityValue = LogSeverity.Error;
            else
                severityValue = (LogSeverity)Enum.Parse(typeof(LogSeverity), severity, true);

            Logger.Instance.Severity = severityValue;
            Logger.Instance.Attach(new ObserverLogToFile(fileLog));
            WriteLog(string.Empty, LogSeverity.Warning);
            WriteLog("Утилита исправления ошибок базы данных запущена.", LogSeverity.Info);
            WriteLog("Будут выполнены следующие действия: ", LogSeverity.Info);
            
            foreach (string action in executeActions)
            {

                WriteLog(string.Format("Выполняется - {0}", action), LogSeverity.Info);
                WriteLog("Инициализация обработчика для " + action, LogSeverity.Info);

                IDBUtil executeAction = ActionsFactory.Create(action, new ExecutionContext(connectionString, schemeName));
                if (executeAction == null)
                {
                    WriteLog(string.Format("Не валидный key=\"ExecuteActions\" value=\"{0}\"", action), LogSeverity.Error);
                    continue;
                }
                WriteLog(string.Format("Обработчик для {0} создан", action), LogSeverity.Info);
                WriteLog(string.Format("Выполняется {0} ", action), LogSeverity.Info);

                if (executeAction.Execute())
                {
                    WriteLog(string.Format("{0} выполнился успешно ", action), LogSeverity.Info);
                }
                else
                {
                    WriteLog(string.Format("Во время выполнения {0} возникли ошибки, для получения детальной информации смотри лог файл {1}", action, fileLog), LogSeverity.Warning);
                }
                WriteLog(string.Format("Операция {0} завершена.", action), LogSeverity.Info);
            }

            WriteLog("Утилита исправления ошибок базы данных остановлена.", LogSeverity.Info);

        }



        static void WriteLog(string message, LogSeverity logSeverity)
        {
            switch (logSeverity)
            {
                case LogSeverity.Debug:
                    Logger.Instance.Debug(message);
                    break;
                case LogSeverity.Error:
                    Logger.Instance.Error(message);
                    break;
                case LogSeverity.Fatal:
                    Logger.Instance.Fatal(message);
                    break;
                case LogSeverity.Info:
                    Logger.Instance.Info(message);
                    break;
                case LogSeverity.Warning:
                    Logger.Instance.Warning(message);
                    break;
                default:
                    Logger.Instance.Info(message);
                    break;
            }

            Console.WriteLine(message);
        }
    }
}
