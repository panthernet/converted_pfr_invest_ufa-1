﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Integration.VIO.JSON
{
    public enum EventType
    {
        None = 0,
        Notification = 1,
        Reply = 2,
        Status = 3
    }
}
