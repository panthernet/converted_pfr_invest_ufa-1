﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace PFR_INVEST.Integration.VIO.JSON
{
    /// <summary>
    /// Основной класс, представляющий сообщение из очереди MQ (ВИО)
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Message
    {
        /// <summary>
        /// Тип сообщения
        /// Формат Enum
        /// </summary>
        public EventType EventType
        {
            get
            {
                switch (EventTypeText.ToLower())
                {
                    case "notification":
                        return EventType.Notification;

                    case "reply":
                        return EventType.Reply;

                    case "status":
                        return EventType.Status;
                }

                return EventType.None; 
            }
            set
            {
                switch (value)
                {
                    case EventType.Notification:
                        EventTypeText = "notification";
                        break;

                    case EventType.Reply:
                        EventTypeText = "reply";
                        break;

                    case EventType.Status:
                        EventTypeText = "status";
                        break;
                }
            }
        }

        /// <summary>
        /// Тип передаваемого сообщения.
        /// Может принимать значения:
        ///     notification    - Для квитанции о направлении входящего документа на обработку
        ///     reply           - Для квитанции об обработке входящего документа
        ///     status          - Для квитанци об обработке входящего документа
        /// </summary>
        [JsonProperty("event-type")]
        public string EventTypeText { get; set; }

        /// <summary>
        /// Идентификатор обращения/процесса
        /// 
        /// Notification:
        /// Формируется отправителем сообщения. Может не быть уникальным. 
        /// Будет содержать одинаковое значение для документов из одного пакета.
        /// Формат идентификатора: [A-Fa-f0-9]{32}
        /// 
        /// Reply:
        /// Идентификатор процесса, запущенного на ВИО. 
        /// Может не быть уникальным. 
        /// Будет содержать одинаковое значение для документов из одного пакета.
        /// 
        /// Status:
        /// Идентификатор обращения
        /// Формируется отправителем сообщения.
        /// Формат идентификатора: [A-Fa-f0-9]{32}
        /// </summary>
        [JsonProperty("request-id")]
        public string RequestId { get; set; }

        /// <summary>
        /// Уникальный идентификатор сообщения.
        /// Формат идентификатора: [A-Fa-f0-9]{32}
        /// </summary>
        [JsonProperty("message-id")]
        public string MessageId { get; set; }

        /// <summary>
        /// Уникальный идентификатор входящего документа, присвоенный в едином хранилище документов (ЕХД) ВИО
        /// </summary>
        [JsonProperty("document-id")]
        public long DocumentId { get; set; }

        /// <summary>
        /// Уникальный идентификатор родительского документа, присвоенный в едином хранилище документов (ЕХД) ВИО
        /// </summary>
        [JsonProperty("parent-document-id")]
        public long ParentDocumentId { get; set; }

        /// <summary>
        /// Используется для указания уникального идентификатора технологического процесса, в рамках которого происходит обмен сообщениями.
        /// Возможные значения:
        ///     urn:process:1:1.0
        ///     urn:process:2:1.0
        ///     urn:process:3:1.0
        ///     urn:process:4:1.0
        ///     urn:process:5:1.0
        /// </summary>
        [JsonProperty("process")]
        public string Process { get; set; }

        /// <summary>
        /// Номер типа входящего документа.
        /// Список значений приведен в приложении В.4
        /// </summary>
        [JsonProperty("type")]
        public long Type { get; set; }

        /// <summary>
        /// Адрес отправителя.
        /// Формат: urn:region:[код_региона]:[код_компонента]
        /// Перечень кодов регионов/ТО ПФР приведены в приложении В.1
        /// Перечень кодов подсистем\компонентов приведены в приложении В.2
        /// </summary>
        [JsonProperty("from")]
        public string From { get; set; }

        /// <summary>
        /// Адрес получателя.
        /// Формат: urn:region:[код_региона]:[код_компонента]
        /// Перечень кодов регионов/ТО ПФР приведены в приложении В.1
        /// Перечень кодов подсистем\компонентов приведены в приложении В.2 
        /// </summary>
        [JsonProperty("to")]
        public string To { get; set; }

        /// <summary>
        /// Блок транзитных технологических данных. 
        /// Указывается информация, которую необходимо получить в неизмененном виде в ответе на запрос.
        /// </summary>
        [JsonProperty("peer-block")]
        public IList<PeerBlock> PeerBlock { get; set; }

        /// <summary>
        /// Статус ответа
        /// Формат Enum
        /// </summary>
        public MessageStatus Status
        {
            get
            {
                switch ($"{StatusText}".ToLower())
                {
                    case "ok":
                        return MessageStatus.Ok;

                    case "failed":
                        return MessageStatus.Failed;
                }

                return MessageStatus.Ok;
            }
            set
            {
                switch (value)
                {
                    case MessageStatus.Ok:
                        StatusText = "ok";
                        break;

                    case MessageStatus.Failed:
                        StatusText = "failed";
                        break;
                }
            }
        }

        /// <summary>
        /// Статус обработки передаваемого сообщения (только для REPLY).
        ///     ok      - Для квитанции о направлении входящего документа на обработку
        ///     failed  - Для квитанции об обработке входящего документа
        /// </summary>
        [JsonProperty("status")]
        public string StatusText { get; set; }


        /// <summary>
        /// Список ошибок.
        /// Присутствует в сообщениях со статусом failed.
        /// </summary>
        [JsonProperty("errors")]
        public IList<Error> Errors { get; set; }
    }
}
