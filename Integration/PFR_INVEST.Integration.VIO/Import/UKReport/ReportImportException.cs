﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Integration.VIO.Import.UKReport
{
    public class ReportImportException : Exception
    {
        public ReportImportException(string message) : base(message)
        {

        }
    }
}
