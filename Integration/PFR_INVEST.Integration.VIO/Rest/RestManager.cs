﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using NHibernate.Linq;
using PFR_INVEST.Integration.VIO.BL;
using RestSharp;

namespace PFR_INVEST.Integration.VIO.Rest
{
    /// <summary>
    /// Класс для работы с REST-API ЕХД ВИО
    /// </summary>
    public class RestManager
    {
        private VIOSettings Settings { get; }

        public RestManager(VIOSettings settings)
        {
            Settings = settings;
        }
        
        /// <summary>
        /// Получает контент запрашиваемого документа из ЕХД
        /// Получает первый XML-файл из папки
        /// </summary>
        /// <returns></returns>
        public string GetDocumentBody(long documentId)
        {
            var client = new RestClient($"http://{Settings.MQConnection.EHDHost}:{Settings.MQConnection.EHDPort}/{Settings.MQConnection.EHDBaseUri}");
            // /{Settings.MQConnection.EHDBaseUri}"
            // client.Authenticator = new HttpBasicAuthenticator(username, password);
            
            var request = new RestRequest("contents/1/stream?documentId={documentId}&mimeType=application%2Fxml", Method.GET);
            request.AddParameter("documentId", documentId.ToString());
            
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string

            var doc = XDocument.Parse(content);
            var docContent = doc.Root.Value;
            
            //// Убираем обрамление кавычками
            //if (content.Length > 1 && content[0] == '\"')
            //{
            //    content = content.Substring(1, content.Length - 2);
            //}

            return docContent;
        }
    }
}
