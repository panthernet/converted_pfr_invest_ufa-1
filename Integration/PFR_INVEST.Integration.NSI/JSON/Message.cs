﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Integration.NSI.JSON
{
	public class Message
	{
		public class DictionaryData 
		{
			/// <summary>
			/// Блок описателя справочника
			/// </summary>
			public Dictionary dictionary { get; set; }

			/// <summary>
			/// Блоки описателей атрибутов терминов справочника
			/// </summary>
			public Attribute[] terms { get; set; }

			/// <summary>
			/// Блоки записей справочника (терминов)
			/// </summary>
			public DictionaryRecord[] records { get; set; }

		}

		/// <summary>
		/// Результат выполнения пакета операций. 0 - успешно, отрицательное - ошибка
		/// </summary>
		public long? resultCode { get; set; }

		/// <summary>
		/// Сообщение об ошибке. В случае успешного завершения - пустая строка
		/// </summary>
		public string errorCode { get; set; }

		/// <summary>
		/// Код об ошибки. В случае успешного завершения - пустая строка
		/// </summary>
		public string errorMessage { get; set; }

		/// <summary>
		/// Тип сообщения. Cls 101
		/// </summary>
		public long? type { get; set; }

		/// <summary>
		/// Подтип сообщения, он же тип операции, Cls 101
		/// </summary>
		public long? subType { get; set; }

		/// <summary>
		/// Идентификатор заявки
		/// </summary>
		public long? requestId { get; set; }

		/// <summary>
		/// Идентификатор подсистемы, Cls 301
		/// </summary>
		public long? senderType { get; set; }

		/// <summary>
		/// Идентификатор справочника, согласно выделенному диапазону
		/// </summary>
		public long? dictionaryId { get; set; }

		/// <summary>
		/// Идентификатор пользователя из компонента СИУУЗ подсистемы ПОИБ, 
		/// </summary>
		public string userId { get; set; }

		/// <summary>
		/// Фамилия, имя и отчество пользователя из компонента СИУУЗ подсистемы ПОИБ, 
		/// </summary>
		public string userFIO { get; set; }

		/// <summary>
		/// Формализованное обоснование изменений
		/// </summary>
		public string reason { get; set; }

		/// <summary>
		/// Идентификатор региона, Cls 302
		/// </summary>
		public long? region { get; set; }

		/// <summary>
		/// Произвольный комментарий к изменениям
		/// </summary>
		public string dsc { get; set; }

		/// <summary>
		/// Номер выводимой страницы. Применяется при запросах описателей словарей, атрибутов и записей справочника (терминов)
		/// </summary>
		public int? page { get; set; }

		/// <summary>
		/// Размер выводимой страницы. Применяется при запросах описателей словарей, атрибутов и записей справочника (терминов)
		/// </summary>
		public int? pageSize { get; set; }


		/// <summary>
		/// Блоки данных  по запрошенным объектам, в соответствии с type, subType 
		/// </summary>
		public DictionaryData dictionaryData { get; set; }


        //С ВЕРСИИ 1.32

        /// <summary>
        /// Контрольная сумма
        /// </summary>
        public long? crc { get; set; }
        /// <summary>
        /// Название закрепленного ответственного за ведение словаря подразделения
        /// </summary>
        public string ownerDept { get; set; }
        /// <summary>
        /// Фамилия, имя и отчество пользователя, ответственного за ведение словаря
        /// </summary>
        public string ownerUser { get; set; }

    }
}
