﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Integration.NSI.JSON;
using Newtonsoft.Json;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.Integration.NSI.BL;

namespace PFR_INVEST.Integration.NSI
{
	public class MessageProcessor
	{
		public NSISettings Settings { get; }

		public MessageProcessor()
		{
            Settings = ServerEnvironment.LoadServerConfig();
		}

		public void ParseMessage(string message)
		{
			var msgID = Guid.NewGuid();
		    Logs.Messages.InfoFormat($"Новое сообщение номер: {msgID}");
			Logs.Messages.Info(message);
		    Logs.MainLog.InfoFormat($"Получено сообщение номер: {msgID}");
			Message msg;
			//Парсим сообщение
			try
			{
				msg = JsonConvert.DeserializeObject<Message>(message);
			    if (Settings.DecodeMessages)
			        DecodeMessage(msg);
			}
			catch (Exception ex)
			{
				Logs.MainLog.Error("Ошибка парсинга JSON сообщения", ex);
				return;
			}
			try
			{
				var dict = Settings.Dictionaries.FirstOrDefault(d => d.NSIDictionaryID == msg.dictionaryId);

				if (dict == null)
				{
					//Словать в сообщении не найден в настройках - пропускаем
				    Logs.MainLog.InfoFormat($"Словарь NSIID = {msg.dictionaryId} не найден в конфигурации, сообщение проигнорировано");
					return;
				}

				//Словарь найден, начинаем обработку сообщения 
			    Logs.MainLog.InfoFormat($"Таблица словаря '{dict.DBTable}'");

			    var updater = new DBUpdater(dict, Settings, msg);
                //ошибка на этапе создания апдейтера? пропускаем сообщение
                if(updater.DBHelper == null)
                    return;
			    foreach (var record in msg.dictionaryData.records)
				{
				    Logs.MainLog.InfoFormat($"Обрабатываем запись НСИ с ID = {record.number}");
					ActionResult result = null;
					if (msg.subType == Settings.MessageSubTypes.Create)
						result = updater.Create(record);
					else if (msg.subType == Settings.MessageSubTypes.Update)
						result = updater.Update(record);
					else if (msg.subType == Settings.MessageSubTypes.Enable)
						result = updater.Enable(record);
					else if (msg.subType == Settings.MessageSubTypes.Disable)
						result = updater.Disable(record);

					if (result == null)
					{
						//подтип сообщения неизвестен, обработка не производилась
					    Logs.MainLog.InfoFormat($"Неизвестный подтип сообщения '{msg.subType}', сообщение проигнорировано");
					}else
				    if (result.IsSuccess)
				    {
				        Logs.MainLog.InfoFormat("Операция выполнена успешно!");
				    }
				    else
				    {
				        Logs.MainLog.ErrorFormat($"Операция прервана: {result.GetExceptionOrMessage()}");
				    }
				}
			}
			catch (Exception ex) 
			{
				Logs.MainLog.Error("Ошибка обработки сообщения", ex);
			}
		}

	    private void DecodeMessage(Message msg)
	    {
	        msg.errorCode = ConvertFromUtf(msg.errorCode);
	        msg.errorMessage = ConvertFromUtf(msg.errorMessage);
	        msg.userFIO = ConvertFromUtf(msg.userFIO);
	        msg.userId = ConvertFromUtf(msg.userId);
	        msg.reason = ConvertFromUtf(msg.reason);
	        msg.dsc = ConvertFromUtf(msg.dsc);
	        if (msg.dictionaryData != null)
	        {
	            msg.dictionaryData.records.ForEach(a =>
	            {
	                a.terms?.ForEach(term =>
	                {
	                    term.term = ConvertFromUtf(term.term);
	                    term.termType = ConvertFromUtf(term.termType);
	                    term.termName = ConvertFromUtf(term.termName);
	                });
	            });
	            if (msg.dictionaryData.dictionary != null)
	            {
	                msg.dictionaryData.dictionary.name = ConvertFromUtf(msg.dictionaryData.dictionary.name);
	                msg.dictionaryData.dictionary.dsc = ConvertFromUtf(msg.dictionaryData.dictionary.dsc);
	            }
	            msg.dictionaryData.terms?.ForEach(a =>
                {
                    a.dscName = ConvertFromUtf(a.dscName);
                    a.name = ConvertFromUtf(a.name);
                    a.mask = ConvertFromUtf(a.mask);
                    a.dsc = ConvertFromUtf(a.dsc);
                });
	        }
	    }

	    private string ConvertFromUtf(string value)
	    {
	        if (string.IsNullOrEmpty(value)) return null;
	        if (Settings.DecodeMode == 1)
	        {
	            return value.Replace("�", "");
	        }
	        var b = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, value.ToByteArray());
	        var res = b.ToStringFromBytes();
	        return res;
	    }

      
        public static List<string> PushDebugData(NSISettings cfg)
	    {
	        try
	        {
                return ServerEnvironment.LoadDebugFlesNSI();
	        }
	        catch (Exception ex)
	        {
                Logs.MainLog.Error("Ошибка загрузки отладочной информации NSI", ex);
                return new List<string>();
	        }
	    }
	}
}
