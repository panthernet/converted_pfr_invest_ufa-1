﻿namespace PFR_INVEST.Integration.NSI.BL
{
    internal enum OpType
    {
        Create,
        Update,
        Delete
    }
}
