﻿using log4net;

namespace PFR_INVEST.Integration.NSI.BL
{
	public static class Logs
	{
		internal static readonly ILog MainLog = LogManager.GetLogger("Integration.NSI.MainLog");
		internal static readonly ILog Messages = LogManager.GetLogger("Integration.NSI.Messages");
 
	}
}
