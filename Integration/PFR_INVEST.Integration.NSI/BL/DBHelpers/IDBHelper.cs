﻿using System;
using NHibernate;
using PFR_INVEST.Integration.NSI.JSON;

namespace PFR_INVEST.Integration.NSI.BL.DBHelpers
{
	public interface IDBHelper
	{
		string Dictionary { get; }		
		Type Type { get; }
        string DBTable { get; }

		long Create(ISession session, DictionaryRecord record, DBUpdater updater);
		void Update(ISession session, long id, DictionaryRecord record, DBUpdater updater);
		bool Enable(ISession session, long id);
		bool Disable(ISession session, long id);

        bool HasStatus { get; }
	}
}
