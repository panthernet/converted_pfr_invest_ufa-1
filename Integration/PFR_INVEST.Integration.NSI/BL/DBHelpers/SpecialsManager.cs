﻿using System;
using NHibernate;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Integration.NSI.BL.DBHelpers
{
    internal static class SpecialsManager
    {
        internal static void ProcessSpecial(ISession session, string specialName, object obj, string value, OpType opType)
        {
            Logs.MainLog.ErrorFormat($@"Отработка функции {specialName}");

            switch (specialName)
            {
                case "ContractLegalEntityName":
                    SpecialContractLegalEntityName(session, (Contract)obj, value, opType);
                    break;
                case "PfrBranchRegionName":
                    SpecialPfrBranchRegionName(session, (PFRBranch) obj, value);
                    break;
            }
        }

        private static void SpecialPfrBranchRegionName(ISession session, PFRBranch branch, string value)
        {
            var id = session.CreateQuery(@"select r.ID from FEDO r where lower(trim(r.Name))=:value")
                .SetParameter("value", value.ToLower().Trim())
                .SetMaxResults(1)
                .UniqueResult<long>();
            branch.FEDO_ID = id > 0 ? id : (branch.FEDO_ID == 0 ? 9 : branch.FEDO_ID);
            if(id == 0)
                Logs.MainLog.Info($@"Для ОПФР {branch.Name} указан регион, не найденный в БД ПТК: {value}");
        }

        private static void SpecialContractLegalEntityName(ISession session, Contract c, string value, OpType opType)
        {
            var leId = session.CreateQuery(@"select le.ID from LegalEntity le 
                                                        join le.Contragent c 
                                                        where trim(lower(le.FormalizedName))=:name and c.StatusID<>-1")
                .SetParameter("name", value?.Trim().ToLower())
                .SetMaxResults(1)
                .UniqueResult<long>();
            switch (opType)
            {
                case OpType.Create:

                    if(leId == 0)
                        throw new Exception($@"Не удалось найти УК/НПФ по формализованному наименованию {value}! Непривязанный контракт недопустим!");
                    c.LegalEntityID = leId;
                    break;
                case OpType.Update:
                    if (c.LegalEntityID != leId)
                    {
                        Logs.MainLog.Info($@"Контракт {c.ContractNumber}({c.ID}) перепривязан к УК с ID = {leId}");
                        c.LegalEntityID = leId;
                    }
                    break;
            }
        }


        public static void ProcessDirectMapping(object item, NSISettings.Mapping term)
        {

        }
    }
}
