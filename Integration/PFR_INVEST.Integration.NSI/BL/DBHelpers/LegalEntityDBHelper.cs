﻿using System;
using System.Linq;
using NHibernate;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Integration.NSI.JSON;

namespace PFR_INVEST.Integration.NSI.BL.DBHelpers
{
	internal class LegalEntityDBHelper : IDBHelper
	{
	    public bool HasStatus => true;
        public const string SPECIAL_NPFIDENTIFIER = "NPFIdentifier";
		public string Dictionary => _legalEntityType == ContragentIdentifier.NPF ? "NPF" : "UK";
	    public Type Type => typeof(LegalEntity);
	    public string DBTable { get; }
	    private readonly string _legalEntityType;

	    public LegalEntityDBHelper(string type, string dbTable)
	    {
	        _legalEntityType = type;
	        DBTable = dbTable;
	    }
				
		private LegalEntityHib CreateNew(ISession session)
		{
			//Контрагент
			var ca = new ContragentHib
			{
				StatusID = 1,
                TypeName = _legalEntityType
			};
			session.SaveOrUpdate(ca);
			session.Flush();
		    ca.ID = (long)session.GetIdentifier(ca);
			
			//НПФ
		    var le = new LegalEntityHib
		    {
		        ContragentID = ca.ID,
		        FormalizedName = "",
		        Contragent = ca,
		    };
		    session.SaveOrUpdate(le);
			session.Flush();

			//Счёт
			/*var ba = new BankAccount
			{ 
				LegalEntityID = le.ID,
				AccountNumber = string.Empty,
				INN = string.Empty,
				KPP = string.Empty,
				LegalEntityName = string.Empty,
				FormalizedName = string.Empty,
				BankName = string.Empty,
				BankLocation = string.Empty
			};
			session.SaveOrUpdate(ba);
			session.Flush();

			//Лицензия
			var lic = new License {LegalEntityID = le.ID};
			session.SaveOrUpdate(lic);
			session.Flush();
            */
			//Виртуальные счета
		    if (Dictionary == "NPF")
		    {
		        var rubAcc = new Account
		        {
		            Name = AccountIdentifier.NPF_PERSONAL_ACCOUNT,
		            ContragentID = ca.ID,
		            AccountTypeID = AccountIdentifier.MeasureIDs.Roubles
		        };
		        session.SaveOrUpdate(rubAcc);

		        var zlAcc = new Account
		        {
		            Name = AccountIdentifier.NPF_PERSONAL_ACCOUNT,
		            ContragentID = ca.ID,
		            AccountTypeID = AccountIdentifier.MeasureIDs.ZL
		        };
		        session.SaveOrUpdate(zlAcc);

		        var deadZLAcc = new Account
		        {
		            Name = AccountIdentifier.NPF_PERSONAL_ACCOUNT,
		            ContragentID = ca.ID,
		            AccountTypeID = AccountIdentifier.MeasureIDs.DeadZL
		        };
		        session.SaveOrUpdate(deadZLAcc);
		        session.Flush();
		    }
		    return le;
		}

	
		public long Create(ISession session, DictionaryRecord record, DBUpdater updater)
		{
			var le = CreateNew(session);

		    if (le == null) return -1;
		    updater.UpdateFields(le, record, OpType.Create);
		    le.Contragent.Name = le.FormalizedName;
		    session.SaveOrUpdate(le);
		    session.Flush();
		    var id = (long)session.GetIdentifier(le);

		    if (_legalEntityType == ContragentIdentifier.NPF)
		        UpdateIdentifierRecord(session, id, record, updater);
		    else UpdateContractRecord(session, id, record);
		    return id;
		}

	    public void Update(ISession session, long id, DictionaryRecord record, DBUpdater updater)
		{
			var le = session.Get<LegalEntityHib>(id);
		    if (le == null) return;
		    var oldNpf = le.Copy();

		    updater.UpdateFields(le, record, OpType.Update);
		    if (oldNpf.FormalizedName != le.FormalizedName
		        || oldNpf.ShortName != le.ShortName
		        || oldNpf.FullName != le.FullName)
		    {
		        var oldName = new OldNPFName
		        {
		            FullName = oldNpf.FullName,
		            ShortName = oldNpf.ShortName,
		            FormalizedName = oldNpf.FormalizedName,
		            LegalEntityID = id,
		            Date = DateTime.Now
		        };
		        le.Contragent.Name = $"{le.FormalizedName} ({oldNpf.FormalizedName})";

		        session.SaveOrUpdate(oldName);
		    }

		    session.SaveOrUpdate(le);
		    session.Flush();
            if(_legalEntityType == ContragentIdentifier.NPF)
		        UpdateIdentifierRecord(session, id, record, updater);
            else UpdateContractRecord(session, id, record);
        }

		public bool Enable(ISession session, long id)
		{
			var le = session.Get<LegalEntityHib>(id);
			if (le != null)
			{
				le.Contragent.StatusID = 1;
                if(_legalEntityType == ContragentIdentifier.UK)
			        session.CreateQuery(@"update Contract c set c.Status = 0 where c.LegalEntityID =:id")
			            .SetParameter("id", id).ExecuteUpdate();
                session.SaveOrUpdate(le);
			}
            return true;
		}

		public bool Disable(ISession session, long id)
		{
			var le = session.Get<LegalEntityHib>(id);
			if (le != null) 
			{
				le.Contragent.StatusID = -1;
                if(_legalEntityType == ContragentIdentifier.UK)
                    session.CreateQuery(@"update Contract c set c.Status = -1 where c.LegalEntityID =:id")
			            .SetParameter("id", id).ExecuteUpdate();
                session.SaveOrUpdate(le);
            }
		    return true;
		}


	    private static void UpdateIdentifierRecord(ISession session, long id, DictionaryRecord record, DBUpdater updater)
	    {
            //ищем маппинг поля идентификатора НПФ в конфиге НСИ)
	        var termname = updater.GetMappingTermForSpecialName(SPECIAL_NPFIDENTIFIER);
            if(string.IsNullOrEmpty(termname)) return;

            //ищем упоминание поля в сообщении
            var term = record.terms.FirstOrDefault(a => a.termName == termname);
	        if (term == null) return;

            //ищем уже добавленные идентификаторы НПФ
            var list = session.CreateQuery("from LegalEntityIdentifier li where li.Identifier=:id")
	            .SetParameter("id", term.term)
	            .List<LegalEntityIdentifier>()
	            .ToList();
            //если уже имеется один
	        if (list.Any())
	        {
                //если привязан к другому НПФ/УК
	            if (list.First().LegalEntityID != id)
	                Logs.MainLog.ErrorFormat("Идентификатор НПФ {0} уже существует и назначен другому НПФ/УК! Запись идентификатора не будет создана", term.term);
	        }
	        else
	        {
                //создаем новый идентификатор
	            session.SaveOrUpdate(new LegalEntityIdentifier { StatusID = 1, LegalEntityID = id, Identifier = term.term, SetDate = DateTime.Today});
                Logs.MainLog.InfoFormat("Идентификатор НПФ {0} успешно добавлен и назначен", term.term);
            }
	    }

        private static void UpdateContractRecord(ISession session, long id, DictionaryRecord record)
        {
            var cNumber = record.terms.FirstOrDefault(a => a.termName == "NPFUK_ContractNumber");
            var cDate = record.terms.FirstOrDefault(a => a.termName == "NPFUK_ContractDate");
            var cEndDate = record.terms.FirstOrDefault(a => a.termName == "NPFUK_ContractDateEnd");
            Logs.MainLog.InfoFormat($"Ищем договор {cNumber?.term} из записи УК по номеру и дате договора...");
            if (cNumber != null && cDate != null && !string.IsNullOrEmpty(cNumber.term) && !string.IsNullOrEmpty(cDate.term))
            {
                var cNumberValue = cNumber.term;
                var cDateValue = DateTime.Parse(cDate.term);
                var cEndDateValue = string.IsNullOrEmpty(cEndDate.term) ? null : (DateTime?)DateTime.Parse(cEndDate.term);
                var c = session.CreateQuery("from Contract c where c.ContractNumber = :num and c.ContractDate=:date")
                    .SetParameter("num", cNumberValue)
                    .SetParameter("date", cDateValue)
                    .UniqueResult<Contract>();

                if (c == null)
                {
                    c = new Contract {ContractDate = cDateValue, ContractNumber = cNumberValue, ContractEndDate = cEndDateValue, LegalEntityID = id};
                    Logs.MainLog.InfoFormat($"Создаем новый договор: {c.ContractNumber} от {c.ContractDate?.ToShortDateString()}");
                }
                else
                {
                    c.ContractEndDate = cEndDateValue;
                    Logs.MainLog.InfoFormat($"Обновляем дату окончания договора: {c.ContractNumber} от {c.ContractDate?.ToShortDateString()}");
                }
                session.SaveOrUpdate(c);
                session.Flush();

            }
            else Logs.MainLog.InfoFormat("Договор не указан или данных недостаточно для поиска.");

        }

    }
}
