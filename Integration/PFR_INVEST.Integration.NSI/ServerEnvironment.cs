﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Integration.NSI.BL;
using System.IO;
using System.Linq;
using PFR_INVEST.Core.ListExtensions;

namespace PFR_INVEST.Integration.NSI
{
	public static class ServerEnvironment
	{
		public static string ConfigFilePath => @"Integration\nsi\NSI.Settings.xml";

	    public static string DebugFilePath => @"Integration\nsi\NSI.Debug.xml";

	    private const string NSI_DEBUG_NPF_FOLDER = @"Integration\nsi\DebugData";

		public static NSISettings LoadServerConfig()
		{
			var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigFilePath);
			//var file = System.Web.HttpContext.Current.Server.MapPath(ServerEnvironment.ConfigFilePath);
			var settings = NSISettings.Load(file);
			return settings;
		}

	    public static List<string> LoadDebugFlesNSI()
	    {
            var fContent = new List<string>();
            Directory.GetFiles( Path.Combine(AppDomain.CurrentDomain.BaseDirectory, NSI_DEBUG_NPF_FOLDER), "*.json", SearchOption.TopDirectoryOnly)
                .OrderBy(a=> a)
                .ForEach(a=> fContent.Add(File.ReadAllText(a)));
	        return fContent;
	    }
	}
}
