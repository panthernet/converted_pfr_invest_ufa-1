﻿using System;
using System.IO;
using db2connector.Contract;

namespace PFR_INVEST.Integration.KIPClient
{
	public static class ServerEnvironment
	{
		public static string ConfigFilePath => @"Integration\kip\KIP.Settings.xml";
        
		public static KIPSettings LoadServerConfig()
		{
			var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigFilePath);
			//var file = System.Web.HttpContext.Current.Server.MapPath(ServerEnvironment.ConfigFilePath);
			var settings = KIPSettings.Load(file);
			return settings;
		}
	}
}
