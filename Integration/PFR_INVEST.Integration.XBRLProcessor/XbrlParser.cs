﻿using System;
using System.IO;
using JeffFerguson.Gepsio;

namespace PFR_INVEST.Integration.XBRLProcessor
{
    public class XbrlParser
    {
        private XbrlDocument _doc;

        public XbrlParser(string path)
        {
            if(string.IsNullOrEmpty(path))
                throw new ArgumentNullException();
            if(!File.Exists(path))
                throw new FileNotFoundException();
            _doc = new XbrlDocument();
            try
            {
                _doc.Load(path);
            }
            catch (Exception ex)
            {
                throw new Exception($"Ошибка загрузки XBRL файла {path}\n{ex.InnerException?.Message ?? ex.Message}");
            }
        }

        public object ParseF060()
        {
            return null;
        }
    }
}
