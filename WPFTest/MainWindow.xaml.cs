﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Xaml;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Docking;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views;
using PFR_INVEST;
using PFR_INVEST.Auth.ClientData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Views.Interface;

namespace WPFTest
{
    public partial class MainWindow : DXWindow
    {
        public BindingList<Type> TypesList { get; set; }
        public BindingList<string> ThemesList { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            DashboardManager.LoadSizes();
            but_test.Click += but_test_Click;

            DataContext = this;
            TypesList = new BindingList<Type>();
            foreach (var item in Assembly.GetAssembly(typeof(YieldView)).GetTypes())
            {
                if (item.IsClass && item.Namespace == "PFR_INVEST.Views" && item.BaseType == typeof(UserControl))
                    TypesList.Add(item);
            }
            foreach (var item in Assembly.GetAssembly(typeof(YieldSIView)).GetTypes())
            {
                if (item.IsClass && item.Namespace == "PFR_INVEST.Views" && item.BaseType == typeof(UserControl))
                    TypesList.Add(item);
            }
            foreach (var item in Assembly.GetAssembly(typeof(YieldVRView)).GetTypes())
            {
                if (item.IsClass && item.Namespace == "PFR_INVEST.Views" && item.BaseType == typeof(UserControl))
                    TypesList.Add(item);
            }




            TypesList = new BindingList<Type>(TypesList.OrderBy(a => a.Name).ToList());

            if (Environment.GetCommandLineArgs().Count() > 1)
                cbox.SelectedIndex = TypesList.IndexOf(TypesList.First(t => t.Name == Environment.GetCommandLineArgs()[1]));
            else
                cbox.SelectedIndex = 0;

            AP.Provider = new AuthProvider("") {CurrentUserSecurity = new UserSecurity()};
            AP.Provider.CurrentUserSecurity.AddRole(DOKIP_ROLE_TYPE.Administrator);

            ThemesList = new BindingList<string>() { "DeepBlue", "Office2007Blue", "Office2007Black", "Office2007Silver" };
            ThemeManager.ApplicationThemeName = "Office2007Blue";
            themes.SelectedIndex = 0;
            themes.SelectedIndexChanged += themes_SelectedIndexChanged;
            
        }

        private void CreateClient()
        {
            WCFClient.UseAutoRepair = false;
            WCFClient.ClientCredentials = new System.ServiceModel.Description.ClientCredentials();

            WCFClient.ClientCredentials.Windows.ClientCredential.Domain = AppSettings.DebugLoginDomain;
            WCFClient.ClientCredentials.Windows.ClientCredential.UserName = AppSettings.DebugLoginName;
            WCFClient.ClientCredentials.Windows.ClientCredential.Password = AppSettings.DebugLoginPassword;

            WCFClient.CreateClientByCredentials();
            WCFClient.UseAutoRepair = true;
        }

        void themes_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            if (themes.SelectedItem == null) return;
            ThemeManager.ApplicationThemeName = themes.SelectedItem.ToString();
        }

        void but_test_Click(object sender, RoutedEventArgs e)
        {
            if (cbox.SelectedItem == null) return;
            CreateWindow(cbox.SelectedItem as Type);
        }


        public void CreateWindow(Type newWindow)
        {
            var panel = (LayoutPanel)null;
            try
            {

                var instance = (UserControl)Assembly.GetAssembly(newWindow).CreateInstance(newWindow.FullName);

                instance.PreviewMouseWheel += instance_PreviewMouseWheel;
                var sz = DashboardManager.GetSizeStorageItem(instance.GetType()) ?? DashboardManager.AddSizeStorageItem(instance.GetType());
                if (newWindow.Name.Contains("List"))
                    instance.DataContext = new GeneralListViewModel();
                else instance.DataContext = new GeneralCardViewModel();

                if (!(instance is IAutoParentPanelSize))
                {
                    panel = dockManager.DockController.AddPanel(new Point(20, 60), sz.Size);
                    panel.Content = instance;
                    panel.MinHeight = sz.MinSize.Height;
                    panel.MinWidth = sz.MinSize.Width;
                }
                else
                {
                    panel = dockManager.DockController.AddPanel(new Point(20, 60), Size.Empty);
                    panel.Content = instance;
                }
                //panel.MinSize = new Size(sz.MinSize.Width, sz.MinSize.Height);
                panel.Caption = instance.GetType().Name;
                dockManager.Activate(panel);
                dockManager.BringToFront(panel);
            }
            catch (Exception ex)
            {
                dockManager.DockController.RemovePanel(panel);
                DXMessageBox.Show(ex.Message, "Exception");
            }
        }

        void instance_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl))
            {
                var vm = GetActiveViewModel() as ViewModelCard;
                if (vm != null)
                    vm.Scale(1.0f + (double)e.Delta / 3000.0f);
            }
        }

        public ViewModelBase GetActiveViewModel()
        {
            var activePanel = dockManager.ActiveDockItem as LayoutPanel;
            if (activePanel != null)
            {
                var window = activePanel.Content as UserControl;
                if (window != null)
                    return window.DataContext as ViewModelBase;

                return null;
            }
            return null;
        }

        public void LoadMainXaml()
        {
            const string xamlpath = "../../../PFR_INVEST/Views/MainWindow/MainWindow.xaml";
            //DXWindow window = null;
            using (var fs = new FileStream(xamlpath, FileMode.Open, FileAccess.Read))
            {
                // Get the root element, which we know is a Window
                var set = new XamlObjectWriterSettings(); set.RootObjectInstance = new DevExpress.Xpf.Ribbon.DXRibbonWindow();
                var reader = new XamlXmlReader(fs);
                //window = (DXWindow)reader.
            }

            //var bm =  window.GetElementByName("barManager") as BarManager;
            //bm.Items
        }
    }

    #region BaseCardClasses
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class GeneralCardViewModel : ViewModelCard
    {
        public override string this[string columnName]
        {
            get
            {
                return null;
            }
        }

        public override bool CanExecuteSave()
        {
            return true;
        }

        protected override void ExecuteSave()
        {
        }
    }
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class GeneralListViewModel : ViewModelList
    {
        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object obj)
        {
        }
    }
    #endregion
}


