﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Proxy;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.BusinessLogic
{
	public class BankCalculator
	{
	    public static string[] FitchRatings { get; } = { "AAA", "AA+", "AA", "AA-", "A+", "A", "A-", "BBB+", "BBB", "BBB-", "BB+", "BB", "BB-", "B+", "B", "B-", "CCC", "CC", "C", "RD", "D", "Нет рейтинга", "Отозван" };

	    public static string[] SPRatings => FitchRatings;
	    public static string[] MoodyRatings { get; } = { "Aaa", "Aa1", "Aa2", "Aa3", "A1", "A2", "A3", "Baa1", "Baa2", "Baa3", "Ba1", "Ba2", "Ba3", "B1", "B2", "B3", "Caa1", "Caa2", "Caa3", "Ca", "C", "Нет рейтинга", "Отозван" };


	    public Dictionary<string, DBField> Fields { get; private set; }



		private decimal _r;
		private decimal _l4m;

		public decimal R
		{
			get
			{
				if (_r == 0)
					CalculateR();

				return _r;
			}
		}

		public decimal Limit4MoneyOld
		{
			get
			{
				if (_l4m == 0)
					CalculateL4M();

				return _l4m;
			}
		}

		public BankCalculator(Dictionary<string, DBField> fields)
		{
			Fields = fields;
		}

		private void CalculateR()
		{
			bool isGreater_BBBPlus = Array.IndexOf(FitchRatings, Fields["LEGALENTITY.FITCH"].Value.ToString()) < Array.IndexOf(FitchRatings, "BBB+") || Array.IndexOf(FitchRatings, Fields["LEGALENTITY.STANDARD"].Value.ToString()) < Array.IndexOf(FitchRatings, "BBB+");
			bool isGreater_Baa1 = Array.IndexOf(MoodyRatings, Fields["LEGALENTITY.MOODY"].Value.ToString()) < Array.IndexOf(MoodyRatings, "Baa1");
			if ((isGreater_BBBPlus || isGreater_Baa1) && (decimal)Fields["LEGALENTITY.MONEY"].Value >= 90000000000)
			{
				_r = 1;
				return;
			}


			bool isGreater_BBB = Array.IndexOf(FitchRatings, Fields["LEGALENTITY.FITCH"].Value.ToString()) < Array.IndexOf(FitchRatings, "BBB") || Array.IndexOf(FitchRatings, Fields["LEGALENTITY.STANDARD"].Value.ToString()) < Array.IndexOf(FitchRatings, "BBB");
			bool isGreater_Baa2 = Array.IndexOf(MoodyRatings, Fields["LEGALENTITY.MOODY"].Value.ToString()) < Array.IndexOf(MoodyRatings, "Baa2");
			if (isGreater_BBB || isGreater_Baa2)
			{
				_r = 0.75M;
				return;
			}


			bool isEq_BBBMinus_or_BBPlus = (Array.IndexOf(FitchRatings, Fields["LEGALENTITY.FITCH"].Value.ToString()) == Array.IndexOf(FitchRatings, "BBB-") || Array.IndexOf(FitchRatings, Fields["LEGALENTITY.FITCH"].Value.ToString()) == Array.IndexOf(FitchRatings, "BB+")) ||
										   (Array.IndexOf(FitchRatings, Fields["LEGALENTITY.STANDARD"].Value.ToString()) == Array.IndexOf(FitchRatings, "BBB-") || Array.IndexOf(FitchRatings, Fields["LEGALENTITY.STANDARD"].Value.ToString()) == Array.IndexOf(FitchRatings, "BB+"));
			bool isEq_Baa3_or_Ba1 = Array.IndexOf(MoodyRatings, Fields["LEGALENTITY.MOODY"].Value.ToString()) == Array.IndexOf(MoodyRatings, "Baa3") || Array.IndexOf(MoodyRatings, Fields["LEGALENTITY.MOODY"].Value.ToString()) == Array.IndexOf(MoodyRatings, "Ba1");
			if (isEq_BBBMinus_or_BBPlus || isEq_Baa3_or_Ba1)
			{
				_r = 0.5M;
				return;
			}


			bool isEq_BB_or_BBMinus = (Array.IndexOf(FitchRatings, Fields["LEGALENTITY.FITCH"].Value.ToString()) == Array.IndexOf(FitchRatings, "BB") || Array.IndexOf(FitchRatings, Fields["LEGALENTITY.FITCH"].Value.ToString()) == Array.IndexOf(FitchRatings, "BB-")) ||
									  (Array.IndexOf(FitchRatings, Fields["LEGALENTITY.STANDARD"].Value.ToString()) == Array.IndexOf(FitchRatings, "BB") || Array.IndexOf(FitchRatings, Fields["LEGALENTITY.STANDARD"].Value.ToString()) == Array.IndexOf(FitchRatings, "BB-"));

			bool isEq_Ba2_or_Ba3 = Array.IndexOf(MoodyRatings, Fields["LEGALENTITY.MOODY"].Value.ToString()) == Array.IndexOf(MoodyRatings, "Ba2") || Array.IndexOf(MoodyRatings, Fields["LEGALENTITY.MOODY"].Value.ToString()) == Array.IndexOf(MoodyRatings, "Ba3");
			if (isEq_BB_or_BBMinus || isEq_Ba2_or_Ba3)
			{
				_r = 0.25M;
				return;
			}
		}

		private void CalculateL4M()
		{
			decimal m = (decimal)Fields["LEGALENTITY.MONEY"].Value / 1000000.0M;
			_l4m = Math.Round(R * m);
		}

		private static bool IsRating(IList<string> values, string rating, string compareRating)
		{
			if (string.IsNullOrWhiteSpace(rating))
				rating = values.LastOrDefault() ?? string.Empty;
			rating = rating.TrimEnd();
			return values.IndexOf(rating) <= values.IndexOf(compareRating);
		}

		[Obsolete("Старая формула. Использовать новую - CalculateRating")]
		public static decimal CalculateR(LegalEntity bank)
		{
		    if ((IsRating(FitchRatings, bank.Fitch, "BBB+")
					|| IsRating(SPRatings, bank.Standard, "BBB+")
					|| IsRating(MoodyRatings, bank.Moody, "Baa1"))
				&& (bank.Money >= 90000000000))
				return 1;
		    if (IsRating(FitchRatings, bank.Fitch, "BBB")
		        || IsRating(SPRatings, bank.Standard, "BBB")
		        || IsRating(MoodyRatings, bank.Moody, "Baa2"))
		        return 0.75M;
		    if (IsRating(FitchRatings, bank.Fitch, "BB+")
		        || IsRating(SPRatings, bank.Standard, "BB+")
		        || IsRating(MoodyRatings, bank.Moody, "Ba1"))
		        return 0.5M;
		    if (IsRating(FitchRatings, bank.Fitch, "BB-")
		        || IsRating(SPRatings, bank.Standard, "BB-")
		        || IsRating(MoodyRatings, bank.Moody, "Ba3"))
		        return 0.25M;
		    return 0;
		}

	    public static decimal CalculateRating(LegalEntity bank, IList<MultiplierRating> ratings)
		{
			var m = ratings.Max(r => r.Multiplier);
			return m ?? 0;
		}


		/// <summary>
		/// Коэффициент (приказ Мин.Фина №24н от 17.02.2015)
		/// </summary>
		/// <param name="bank"></param>
		/// <returns></returns>
		public static decimal LimitMult(LegalEntity bank)
		{
			//Логика вынесена для доступности для сервера
			return BankHelper.LimitMult(bank);			
		}

		/// <summary>
		/// Лимит на средства, млн. руб. 
		/// </summary>        
		public static decimal Limit4Money(LegalEntity bank)
		{
			//Логика вынесена для доступности для сервера
			return BankHelper.Limit4Money(bank);		
		}

		/// <summary>
		/// Лимит на средства, млн. руб. 
		/// </summary>        
		public static decimal Limit4Money(LegalEntity bank, IList<MultiplierRating> ratings)
		{
			decimal m = (bank.Money ?? 0m) / 1000000.0m;
			if (m < 0M)
				m = 0M;
			decimal r = CalculateRating(bank, ratings);
			decimal res = r * m;
			return Math.Round(res, 0);
		}

		public static decimal Limit4Requests(LegalEntity bank)
		{
			return Math.Round(Limit4Money(bank) - calculateD(bank) + calculateV(bank));
		}

		public static decimal calculateD(LegalEntity bank)
		{
			//d=(d1+d2+d3)/1000000 (в млн руб)"
			//d1 - сумма СПН, размещенных в банке на начало предшествующего рабочего дня
			//d2 - сумма СПН, подлежащиз размещению в предшествующий рабочий день
			//d3 - сумма СПН, подлежащих размещению в депозиты в день проведения Отбора заявок

			return 0;
		}

		public static decimal calculateV(LegalEntity bank)
		{
			//v=(v1+v2+v3) /1000000
			//v1 - сумма СПН, подлежащих вохврату из банка на начало предшествующего Отбору заявок рабочего дня
			//v2 - сумма СПН, подлежащих вохврату из банка в день Отбора заявок 
			//v3 - сумма СПН, подлежащих вохврату из банка в день перечисления средств по итогам аукциона

			return 0;
		}

        /// <summary>
        /// Проверка, является ли банк активным, в т.ч. с точчки зрения собственных средств
        /// </summary>
        /// <param name="bank">Банк</param>
		public static bool IsActive(LegalEntity bank)
		{
            //!!!!
            //ПРИ СМЕНЕ ЛОГИКИ НУЖНО УЧЕСТЬ BankListItem.ToLeForActiveCheck()
            //!!!!
			return (bank.PFRAGRSTAT ?? string.Empty).Trim().Equals("Заключено", StringComparison.OrdinalIgnoreCase)
				&& (Limit4Money(bank) > 0 || Limit4Requests(bank) > 0);
		}

        /// <summary>
        /// Возвращает статус банка, активен или нет
        /// </summary>
        /// <param name="bank">Банк</param>
	    public static bool IsActiveStatus(LegalEntity bank)
	    {
	        return (bank.PFRAGRSTAT ?? string.Empty).Trim().Equals("Заключено", StringComparison.OrdinalIgnoreCase);
	    }

        public static bool IsActiveStatus(string pfragrstat)
        {
            return (pfragrstat ?? string.Empty).Trim().Equals("Заключено", StringComparison.OrdinalIgnoreCase);
        }

    }
}
