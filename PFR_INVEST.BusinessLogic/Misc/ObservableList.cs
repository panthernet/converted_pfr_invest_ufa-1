﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.ObjectModel;

namespace PFR_INVEST.BusinessLogic.Misc
{
    public class ObservableList<T> : ObservableCollection<T>
    {
        public ObservableList()
            : base()
        {
        }

        public ObservableList(IEnumerable<T> collection)
            : base(collection)
        {
            foreach (var item in this)
                Subscribe(item);
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnCollectionChanged(e);

            if(e.NewItems != null)
                foreach (var item in e.NewItems) Subscribe((T)item);
            if (e.OldItems != null)
                foreach (var item in e.OldItems) Unsubscribe((T)item);
        }

        private void Subscribe(T item)
        {
            if (item is INotifyPropertyChanged)
                ((INotifyPropertyChanged)item).PropertyChanged += new PropertyChangedEventHandler(ObservableList_PropertyChanged);
        }
        private void Unsubscribe(T item)
        {
            if (item is INotifyPropertyChanged)
                ((INotifyPropertyChanged)item).PropertyChanged -= new PropertyChangedEventHandler(ObservableList_PropertyChanged);
        }

        void ObservableList_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (ItemChanged != null)
                ItemChanged(sender, e);
        }

        public event EventHandler<PropertyChangedEventArgs> ItemChanged;
    }
}
