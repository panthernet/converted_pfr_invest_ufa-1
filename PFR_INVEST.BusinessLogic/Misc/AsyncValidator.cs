﻿using System;

namespace PFR_INVEST.BusinessLogic.Misc
{
    /// <summary>
    /// "Асинхронный" валидатор для поля модели
    /// Использование
    /// 1. В сеттере поля: this.SomeValidator.Reset();
    /// 2. В методе this[]: return this.SomeValidator.Error;
    /// 3. В методе Validate модели:    if (!this.SomeValidator.ValidateIfNot()) return this.SomeValidator.Error;
    /// 4. В onValidChanged валидатора не забываем вызвать OnPropertyChanged("SomeField");
    /// 5. Для окончательной проверки в методе BeforeExecuteSaveCheck: return this.LicenseValidator.Validate();
    /// </summary>
    public class AsyncValidator
    {
        public Func<string> ValidateFunc { get; }
        public Action OnValidChangedAction { get; }
        /// <summary>
        /// null - идёт проверка и валидность неизвестна
        /// </summary>
        public bool? IsValid { get; private set; }
        public string Error { get; private set; }

        public AsyncValidator(Func<string> validateFunc, Action onValidChanged)
        {
            Reset();
            ValidateFunc = validateFunc ?? (() => null);
            OnValidChangedAction = onValidChanged ?? (() => { });
        }

        public void Reset()
        {
            Error = null;
            IsValid = null;
        }

        public bool Validate()
        {
            IsValid = null;
            var oldError = Error;
            Error = ValidateFunc();
            IsValid = string.IsNullOrWhiteSpace(Error);
            if (oldError != Error)
                OnValidChangedAction();


            return IsValid.Value;
        }

        /// <summary>
        /// Запускает валидацию только, если она уже была проверена раньше 
        /// </summary>
        /// <returns></returns>
        public bool ValidateIfNot()
        {
            return IsValid ?? Validate();
        }
    }
}
