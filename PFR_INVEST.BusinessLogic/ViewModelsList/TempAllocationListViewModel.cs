﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public sealed class TempAllocationListViewModel : PagedLoadListModelBase<TempAllocationListItem>, ISettingOpenForm
    {

        public TempAllocationListViewModel()
        {
            DataObjectTypeForJournal = typeof (object);
            DataObjectDescForJournal = "Комплексный список";
        }

        public OpenSettingsProvider SettingsProvider { get; set; }

        private long _countSellPaper, _countBuyPaper, _countAllocateDeposit, _countReturnDeposit, _countBalanceExtra;
        private const long COUNT_BALANCE_EXTRA_RETURN = 0;

        protected override void BeforeListRefresh()
        {
            base.BeforeListRefresh();
            if (SettingsProvider == null)
                SettingsProvider = new OpenSettingsProvider("TempAllocationListViewModel", "Бэк-офис - Временное размещение - Временное размещение активов", (s) => ExecuteRefreshList(null));
        }
                

        protected override long GetListCount()
        {
            _countSellPaper = BLServiceSystem.Client.GetCountBuyOrSaleReportsListFilteredHib(true, null, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            _countBuyPaper = BLServiceSystem.Client.GetCountBuyOrSaleReportsListFilteredHib(false, null, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            _countAllocateDeposit = BLServiceSystem.Client.GetCountTempAllocateDepositListFilteredHib(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            _countReturnDeposit = BLServiceSystem.Client.GetCountTempAllocateReturnDepositListFilteredHib(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            _countBalanceExtra = BLServiceSystem.Client.GetCountTempAllocateBalanceExtraListFilteredHib(true, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);//т.к. данные из таблицы удваиваются Allocate + Return
           
            return _countSellPaper + _countBuyPaper + _countAllocateDeposit + _countReturnDeposit + _countBalanceExtra + COUNT_BALANCE_EXTRA_RETURN;


        }

        protected override List<TempAllocationListItem> GetListPart(long index)
        {
            if (index < _countSellPaper)//Sell paper
                return BLServiceSystem.Client.GetTempAllocationBuyOrSaleListFilteredByPageHib(true, null, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            if (index < _countSellPaper + _countBuyPaper) //Buy Paper
                return BLServiceSystem.Client.GetTempAllocationBuyOrSaleListFilteredByPageHib(false, null, (int)(index - (_countSellPaper)), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            if (index < _countSellPaper + _countBuyPaper + _countAllocateDeposit) //Allocate Deposit
                return BLServiceSystem.Client.GetTempAllocateDepositListFilteredByPageHib((int)(index - (_countSellPaper + _countBuyPaper)), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            if (index < _countSellPaper + _countBuyPaper + _countAllocateDeposit + _countReturnDeposit) //Return Deposit
                return BLServiceSystem.Client.GetTempAllocateReturnDepositListFilteredHib((int)(index - (_countSellPaper + _countBuyPaper + _countAllocateDeposit)), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            index -= _countSellPaper + _countBuyPaper + _countAllocateDeposit + _countReturnDeposit;
            return BLServiceSystem.Client.GetTempAllocateBalanceExtraListFilteredHib(true, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }
    }
}
