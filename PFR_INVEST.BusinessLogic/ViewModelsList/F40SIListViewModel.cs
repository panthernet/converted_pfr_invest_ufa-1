﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F40SIListViewModel : F40ListViewModel
    {
         const string GRID_TITLE = "Отчеты по сделкам";
         const string LOG_PREFIX = "Логирование грида 'Отчеты по сделкам'";

         public F40SIListViewModel()
             : base()
         {
             //ViewModelBase.Logger.WriteLine(string.Format(LOG_PREFIX));
             //ViewModelBase.Logger.WriteLine(string.Format("{0}: начало загрузки данных для грида : {1}", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
             this.OnDataRefreshed += new EventHandler(F40SIListViewModel_OnDataRefreshed);
         }

         void F40SIListViewModel_OnDataRefreshed(object sender, EventArgs e)
         {
             //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
             //ViewModelBase.Logger.WriteLine(string.Format("{0}: Грид загружен: {1}", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
         }


         //protected override void UpdatePart(List<F040DetailsListItem> part)
         //{
         //    this.CalculateDocNumbers(part);
         //}

         protected override long GetListCount()
         {
             long recordsCount = BLServiceSystem.Client.GetF040DetailsCount((int)Document.Types.SI); 
             //return BLServiceSystem.Client.GetF040DetailsCount((int)Document.Types.SI); 
             Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
             //ViewModelBase.Logger.WriteLine(string.Format("{0}: общее количество записей: {1}", LOG_PREFIX, recordsCount));
             return recordsCount;

         }


         protected override List<F040DetailsListItem> GetListPart(long index)
         {
             //return BLServiceSystem.Client.GetF040DetailsByPage((int)Document.Types.SI, (int)index).ToList();
             Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
             //ViewModelBase.Logger.WriteLine(string.Format("{0}: обращение к wcf сервису для получения порции данных, время: {1}.", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
             //List<F040DetailsListItemLite> records = BLServiceSystem.Client.GetF040DetailsByPageLite((int)Document.Types.SI, (int)index).ToList();
             //List<F040DetailsListItem> records2 = records.ConvertAll(x => new F040DetailsListItem(x));

             List<F040DetailsListItem> records2 = BLServiceSystem.Client.GetF040DetailsByPage((int)Document.Types.SI, (int)index).ToList();
             
             //ViewModelBase.Logger.WriteLine(string.Format("{0}: получена порция данных из {1} записей, время: {2}.", LOG_PREFIX, records == null ? 0 : records.Count, DateTime.Now.ToString("HH:mm:ss:ffff")));
             return records2;

         }

        //protected override void ExecuteRefreshList(object param)
        //{
        //    CalculateDocNumbers(BLServiceSystem.Client.GetF040ListByTypeContract((int)Document.Types.SI));
        //}
    }
}
