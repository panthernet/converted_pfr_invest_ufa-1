﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public sealed class IncomeSecurityListViewModel : ViewModelListObservable<IncomeSecurityListItem>, IUpdateListenerModel
	{

        public IncomeSecurityListViewModel()
        {
            DataObjectTypeForJournal = typeof(Incomesec);
        }

		protected override void ExecuteRefreshList(object param)
		{
			List.Fill(BLServiceSystem.Client.GetIncomeSecurityList());
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(Incomesec) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
		    if (type != typeof (Incomesec)) return;
		    var item = BLServiceSystem.Client.GetIncomeSecurityList(id).FirstOrDefault();
		    var old = List.FirstOrDefault(r => r.ID == id);

		    UpdateRecord(old, item);
		}
	}
}
