﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using System.Reflection;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class DeleteDocumentsListViewModel : ViewModelList
    {
        private IList<DeleteDocumentEntry> _mDocs;
        public IList<DeleteDocumentEntry> Docs
        {
            get
            {
                return _mDocs;
            }
            set
            {
                _mDocs = value;
                OnPropertyChanged("Docs");
            }
        }

        public override Type DataObjectTypeForJournal => typeof (DeleteDocumentEntry);

        public event EventHandler RestoreError;

        protected void OnRestoreError(string text)
        {
            if (RestoreError != null)
                RestoreError(text, null);
        }

        #region RestoreDocument command
        private CommandBinding _restoreDocumentBinding;
        public CommandBinding RestoreDocumentBinding => _restoreDocumentBinding ?? (_restoreDocumentBinding = new CommandBinding(RestoreDocument, RestoreDocument_Executed, RestoreDocument_CanExecute));

        private RoutedUICommand _restoreDocument;
        public RoutedUICommand RestoreDocument => _restoreDocument ?? (_restoreDocument = new RoutedUICommand("Restore document", "RestoreDocument", GetType()));

        public void RestoreDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var param = (object[])e.Parameter;
            if (param == null) return;
            var otype = (string)param[0];
            var id = (long?)param[1];
            var delID = (long?)param[2];

            if(!delID.HasValue) return;

           // var item = Docs.FirstOrDefault(a => a.ID == delID);
           // var data = item?.Data;

            switch (BLServiceSystem.Client.RestoreDocument(delID.Value))
            {
                case WebServiceDataError.DocumentDeleteError:
                case WebServiceDataError.GeneralException:
                    OnRestoreError("При восстановлении записи возникла непредвиденная ошибка!");
                    break;
                case WebServiceDataError.DocumentDeleteEntryNotFound:
                    OnRestoreError("Запись для восстановления не найдена!");
                    break;
                case WebServiceDataError.UnknownUndeleteType:
                    OnRestoreError("Обработчик для восстановления данного типа записей отсутствует!");
                    break;
                default:
                    RefreshModelLists(otype, id ?? 0);
                    SpecialUpdates(otype, id ?? 0);
                    break;
            }
            JournalLogger.LogTypeEvent(this, JournalEventType.RESTORE_DATA, "Восcтановление", $"Тип обьекта {otype}, Id {id}");
            RefreshList.Execute(null);
        }

        private void SpecialUpdates(string otype, long id)
        {
            if(otype == null || id == 0) return;
            switch (otype.ToLower())
            {
                case "contract":
                    var c = DataContainerFacade.GetByID<Contract>(id);
                    var contragent = c.GetLegalEntity().GetContragent();
                    if (contragent != null && (contragent.TypeName == "УК" || contragent.TypeName == "ГУК"))
                    {
                        ContragentHelper.UpdateContragentStatus(contragent, c.GetLegalEntity().GetContracts().ToList());
                        DataContainerFacade.Save(contragent);
                        ViewModelManager.RefreshViewModels(typeof(SIListViewModel));
                    }

                    break;
                default:
                    return;
            }
        }

        private void RefreshModelLists(string otype, long id)
        {
            try
            {
                var ass = Assembly.GetAssembly(typeof(BaseDataObject));
                var type = ass.GetType(otype, false);
                if (type != null)
                {
                    //работаем с новым типом
                    var attr = type.GetCustomAttributes(typeof(UseRestoreAttribute), false).Select(a => (UseRestoreAttribute)a).FirstOrDefault();
                    if (attr != null)
                        ViewModelManager.UpdateDataInAllModels(type, id);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Undelete - refresh list");
                DialogHelper.ShowAlert("Ошибка обновления данных в открытых списках. Обновление не произведено.");
            }
        }

        public void RestoreDocument_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion

        protected override void ExecuteRefreshList(object param)
        {
            var allDocs = new List<DeleteDocumentEntry>(BLServiceSystem.Client.GetDeletedDocumentsList());

            var typeForDeleting = new [] { "ViolationCategoryViewModel", "ReorganizationViewModel" }.ToList();

            Docs = new List<DeleteDocumentEntry>( allDocs.Where(d => typeForDeleting.All(t => d.DocumentType != t)));
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
