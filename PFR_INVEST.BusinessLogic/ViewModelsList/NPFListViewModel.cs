﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class NPFListItem
    {
#if WEBCLIENT
        /// <summary>
        /// Обяаательное поле идентификатор для веб-клиента
        /// </summary>
        public long ID => LegalEntity?.ID ?? 0;
#endif

        public LegalEntity LegalEntity { get; private set; }
        public Contragent Contragent { get; private set; }
        public Status Status { get; private set; }
        public IList<LegalEntityChief> Chiefs { get; private set; }

        public long? ParentContragentID { get; private set; }

        public NPFListItem(LegalEntity le)
        {
            if (le == null) return;
            LegalEntity = le;
            Contragent = le.GetContragent();
            Status = Contragent.GetStatus();
            Chiefs = le.GetChiefs();
            var lastChief = Chiefs.LastOrDefault(x => x.RetireDate == null);

            if (lastChief != null)
            {
                LegalEntity.HeadFullName = string.Format("{0} {1} {2}", lastChief.LastName, lastChief.FirstName, lastChief.PatronymicName).Trim();
                LegalEntity.HeadPosition = lastChief.Position;
            }

            ParentContragentID = Contragent.GetReorganizations()
                .Cast<Reorganization>()
                .Where(r => r.ReceiverContragentID != Contragent.ID)
                .Select(r => r.ReceiverContragentID)
                .FirstOrDefault();
        }

        public override string ToString()
        {
            return string.Format(LegalEntity.FormalizedName);
        }
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class NPFListViewModel : PagedLoadListModelBase<NPFListItem>
    {

        public NPFListViewModel()
        {
            DataObjectTypeForJournal = typeof (LegalEntity);
        }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountNPFListLEReorgHib();
        }

        protected override List<NPFListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetNPFListLEReorgByPageHib((int)index).ToList().ConvertAll(le => new NPFListItem(le));
        }

        #region Old

        //private IList<NPFListItem> m_NPFList;
        //      public IList<NPFListItem> NPFList
        //      {
        //          get
        //          {
        //              RaiseDataRefreshing();
        //              return m_NPFList;
        //          }
        //          set
        //          {
        //              m_NPFList = value;
        //              OnPropertyChanged("NPFList");
        //          }
        //      }

        //      protected override void ExecuteRefreshList(object param)
        //      {
        //          NPFList = BLServiceSystem.Client.GetNPFListLEReorgHib().ToList().ConvertAll(le => new NPFListItem(le));
        //      }

        //      protected override bool CanExecuteRefreshList()
        //      {
        //          return true;
        //      }
        #endregion
    }
}
