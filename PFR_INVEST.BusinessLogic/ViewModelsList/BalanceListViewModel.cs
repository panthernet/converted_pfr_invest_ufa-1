﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Proxy.DataContracts;

namespace PFR_INVEST.BusinessLogic
{
    internal class Db2BalanceItemsComparer : IComparer<DB2BalanceListItem>
    {

        int IComparer<DB2BalanceListItem>.Compare(DB2BalanceListItem x, DB2BalanceListItem y)
        {
            if (x == y)
                return 0;
            return x.DATE > y.DATE ? 1 : -1;
        }
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class BalanceListViewModel : PagedLoadListModelBase<DB2BalanceListItem>, ISettingOpenForm//, IUpdateListenerModel
    {
        public BalanceListViewModel(object param)
            : base(param)
        {
            ID = -1;
        }

        protected override void BeforeListRefresh()
        {
            if (SettingsProvider != null) return;
            if (!(RefreshParameter is List<string>) && RefreshParameter != null) return;
            var formName = "";
            var comment = "";
            if (RefreshParameter == null)
            {
                formName = "Расчет строки 030";
                comment = "Расчет строки 030";
            } 
            else
            {
                var list = ((List<string>)RefreshParameter);
                formName = list.FirstOrDefault();
                comment = list.Skip(1).FirstOrDefault();
            }
            OnDataRefreshed += (sender, e) => RefreshList(TmpList);
            SettingsProvider = new OpenSettingsProvider(formName, comment, s => ExecuteRefreshList(null)) {IsPeriodVisible = true};
        }

		//private long countAccoperation, countApps, countAddSpn,
		//            countDoApps, countDopSpnM, countKDoc,
		//            countOrdReport, countCost, countIncomeSec,
		//            countAsgFinTr, countReqTransfer, countTransfer,
		//            countReturn, counDeposit, countBalanceExtra;

        private readonly List<SaldoParts> _targetParts = new List<SaldoParts>
        {
            SaldoParts.Accoperation,
            SaldoParts.Apps,
            SaldoParts.AddSpn,
            SaldoParts.DoApps,
            SaldoParts.DopSpn_M,
            //SaldoParts.K_Doc,
            SaldoParts.OrdReport,
            SaldoParts.Cost,
            SaldoParts.IncomeSec,
            SaldoParts.Asg_Fin_Tr,
            SaldoParts.Req_Transfer,
            SaldoParts.Transfer,
            SaldoParts.Return,
            SaldoParts.Deposit,
            SaldoParts.BalanceExtra,
        };
        private readonly Dictionary<SaldoParts, long> _countParts = new Dictionary<SaldoParts, long>();
        private readonly Dictionary<SaldoParts, long> _loadedIndex = new Dictionary<SaldoParts, long>();

        protected override long GetListCount()
        {
            if (SettingsProvider == null) return -1;

            foreach (var part in _targetParts)
            {
                _countParts[part] = BLServiceSystem.Client.GetCountBalanceList(new List<SaldoParts> { part }, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
                _loadedIndex[part] = 0;
            }

            return _countParts.Sum(x => x.Value);
            
        }


        protected override List<DB2BalanceListItem> GetListPart(long index)
        {
            var nextPart = _targetParts.FirstOrDefault(x => _countParts[x] - _loadedIndex[x] > 0);
            if((int)nextPart == 0) return new List<DB2BalanceListItem>();
            var part = BLServiceSystem.Client.GetBalanceListByPage(
                    new List<SaldoParts> { nextPart },
                    (int)(_loadedIndex[nextPart]), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            _loadedIndex[nextPart] += part.Count;
            return part;
        }


        protected void RefreshList(List<DB2BalanceListItem> list)
        {
            //SetBusy(true);
            var templist = new List<BalanceListItem>();
            const string sDelimiter = "_";
            var reOrderList = new Dictionary<string, List<DB2BalanceListItem>>();
            foreach (var p in list)
            {
                var key = string.Concat(p.PORTFOLIO, sDelimiter, p.ACCOUNT, sDelimiter, p.DATE.Year.ToString("D2"), p.DATE.Month.ToString("D2"));
                if (!reOrderList.ContainsKey(key))
                    reOrderList[key] = new List<DB2BalanceListItem>();

                reOrderList[key].Add(p);
            }

            IEnumerable<KeyValuePair<string, List<DB2BalanceListItem>>> sortedOrderList = reOrderList.OrderBy(pair => pair.Key);

            var dictStart = new Dictionary<string, decimal>();


            foreach (var branch in sortedOrderList)
            {

                var items = branch.Value;

                decimal start = 0;

                var key = string.Concat(items[0].PORTFOLIO, sDelimiter, items[0].ACCOUNT);
                if (!dictStart.ContainsKey(key))
                    dictStart.Add(key, start);

                start = dictStart[key];

				//Db2BalanceItemsComparer DB2Comparer = new Db2BalanceItemsComparer();
				items = items.OrderBy(b => b.DATE).ThenBy(b => b.ID).ToList();

                foreach (var x in items.Select(it => new BalanceListItem(it)))
                {
                    templist.Add(x);
                    var minus = x.Minus;
                    x.Start = start;
                    start = start + x.Plus + (minus > 0 ? -minus : minus);
                    x.Finish = start;
                }

                dictStart[key] = start;
            }

            GridList = templist;
        }


        public OpenSettingsProvider SettingsProvider { get; set; }

        private List<BalanceListItem> _gridlist = new List<BalanceListItem>();
        public List<BalanceListItem> GridList
        {
            get
            {
                RaiseDataRefreshing();
                return _gridlist;
            }
            set
            {
                _gridlist = value;
                OnPropertyChanged("GridList");
            }
        }


        // грид обновляется только целиком нажатием кнопки: http://jira.vs.it.ru/browse/DOKIPIV-125?focusedCommentId=222876&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-222876
        //Type[] IUpdateListenerModel.UpdateListenTypes
        //{
        //    get
        //    {
        //        return new Type[] { typeof(AccOperation), typeof(Aps), typeof(AddSPN), typeof(DopAps), typeof(DopSPN), typeof(OrdReport), typeof(Cost), typeof(Incomesec)
        //    , typeof(AsgFinTr), typeof(ReqTransfer), typeof(Transfer), typeof(Return), typeof(Deposit), typeof(BalanceExtra)};
        //    }
        //}

        //void IUpdateListenerModel.OnDataUpdate(Type type, long id)
        //{
        //    List<SaldoParts> saldoParts = new List<SaldoParts>();
        //    if (type == typeof(AccOperation))
        //        saldoParts.Add(SaldoParts.Accoperation);
        //    else if (type == typeof(Aps))
        //        saldoParts.Add(SaldoParts.Apps);
        //    else if (type == typeof(AddSPN))
        //        saldoParts.Add(SaldoParts.AddSpn);
        //    else if (type == typeof(DopAps))
        //        saldoParts.Add(SaldoParts.DoApps);
        //    else if (type == typeof(DopSPN))
        //        saldoParts.Add(SaldoParts.DopSpn_M);
        //    else if (type == typeof(OrdReport))
        //        saldoParts.Add(SaldoParts.OrdReport);
        //    else if (type == typeof(Cost))
        //        saldoParts.Add(SaldoParts.Cost);
        //    else if (type == typeof(Incomesec))
        //        saldoParts.Add(SaldoParts.IncomeSec);
        //    else if (type == typeof(AsgFinTr))
        //    {
        //        // новые ПП одинаковы дял УК и НПФ
        //        saldoParts.Add(SaldoParts.Asg_Fin_Tr);
        //        saldoParts.Add(SaldoParts.Req_Transfer);
        //    }
        //    else if (type == typeof(ReqTransfer))
        //        saldoParts.Add(SaldoParts.Req_Transfer);
        //    else if (type == typeof(Transfer))
        //        saldoParts.Add(SaldoParts.Transfer);
        //    else if (type == typeof(Return))
        //        saldoParts.Add(SaldoParts.Return);
        //    else if (type == typeof(Deposit))
        //        saldoParts.Add(SaldoParts.Deposit);
        //    else
        //        saldoParts.Add(SaldoParts.BalanceExtra);

        //    var data = BLServiceSystem.Client.GetBalanceListByID(saldoParts.ToList(), id, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);


        //    var ids = saldoParts.Select(p => (int)p).ToArray();
        //    var toRemove = List.Where(x => x.ID == id && ids.Contains(x.Ord)).ToList();
        //    toRemove.ForEach(x => List.Remove(x));

        //    data.ForEach(x => List.Add(new BalanceListItem(x)));//, PFRAccountsList.FirstOrDefault(a => a.AccountNumber == x.ACCOUNT))));

        //    RefreshList(t_List);
        //    OnPropertyChanged("List");
        //}
	}
}
