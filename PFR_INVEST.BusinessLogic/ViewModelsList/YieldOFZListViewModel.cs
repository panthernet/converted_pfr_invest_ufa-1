﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class YieldOFZListViewModel : ViewModelListObservable<BDate>
    {

       // private List<BDate> m_BDateList_Cache;
        // public RangeObservableCollection<BDate> BDateList = new RangeObservableCollection<BDate>();

        //private DateTime? _selectedDate;

        //public DateTime? SelectedDate
        //{
        //    get { return _selectedDate; }
        //    set
        //    {

        //        if (SetPropertyValue(nameof(SelectedDate), ref _selectedDate, value))
        //        {
        //            if (SelectedDate.HasValue)
        //                List.Fill(m_BDateList_Cache.Where(d => d.TradeDate == SelectedDate.Value.Date));
        //            else
        //                List.Clear();

        //            RaiseDataRefreshing();
        //        }

        //    }
        //}
        public YieldOFZListViewModel()
        {
            DataObjectTypeForJournal = typeof(BDate);
           // SelectedDate = DateTime.Now;
            ExecuteRefreshList(null);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object param)
        {
            List.Fill(DataContainerFacade.GetList<BDate>().Where(d => d.BoardIdText.Equals("TQOB", StringComparison.InvariantCultureIgnoreCase)
            && d.SecurityIdText.StartsWith("SU", StringComparison.InvariantCultureIgnoreCase)));

            //if (SelectedDate.HasValue)
            //    List.Fill(m_BDateList_Cache.Where(d => d.TradeDate == SelectedDate.Value.Date));
            //else List.Clear();
            //RaiseDataRefreshing();
        }
    }
}
