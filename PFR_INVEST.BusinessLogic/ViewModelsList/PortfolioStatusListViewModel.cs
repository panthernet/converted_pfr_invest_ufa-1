﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PortfolioStatusListViewModel : ViewModelList
    {
        private List<PortfolioStatusListItem> m_PortfoliosList;
        public List<PortfolioStatusListItem> PortfoliosList
        {
            get
            {
                RaiseDataRefreshing();
                return m_PortfoliosList;
            }
            set
            {
                m_PortfoliosList = value;
                OnPropertyChanged("PortfoliosList");
            }
        }

        public PortfolioStatusListViewModel()
        {
            DataObjectTypeForJournal = typeof(Portfolio);
            DataObjectDescForJournal = "Составная таблица c информацией по портфелю";
        }

        protected override void ExecuteRefreshList(object param)
        {
            PortfoliosList = BLServiceSystem.Client.GetPortfolioStatusListHib();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
