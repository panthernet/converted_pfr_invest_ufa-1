﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class StockListViewModel : ViewModelListObservable<Stock>, IUpdateListenerModel
    {
        private Stock _selectedStock;

        public Stock SelectedStock
        {
            get { return _selectedStock; }
            set { _selectedStock = value; OnPropertyChanged(nameof(SelectedStock)); }
        }

        public override Type DataObjectTypeForJournal => typeof(Stock);

        protected override void ExecuteRefreshList(object param)
        {
            List.Fill(DataContainerFacade.GetList<Stock>());
        }

        public Type[] UpdateListenTypes => new[] {typeof(Stock)};

        public void OnDataUpdate(Type type, long id)
        {
            var oldItem = List.FirstOrDefault(r => r.ID == id);
            var newItem = DataContainerFacade.GetByID<Stock>(id);
            UpdateRecord(oldItem, newItem);

        }
    }
}
