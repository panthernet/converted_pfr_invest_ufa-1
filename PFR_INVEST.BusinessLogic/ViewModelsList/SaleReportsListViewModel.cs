﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class SaleReportsListViewModel : ViewModelList
    {
        private List<BuyOrSaleReportsListItem> m_ReportsList;
        public List<BuyOrSaleReportsListItem> ReportsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_ReportsList;
            }
            set
            {
                m_ReportsList = value;
                OnPropertyChanged("ReportsList");
            }
        }

        public SaleReportsListViewModel()
        {
            DataObjectTypeForJournal = typeof(OrdReport);
        }

        protected override void ExecuteRefreshList(object param)
        {
            ReportsList = BLServiceSystem.Client.GetBuyOrSaleReportsListHib(true, null);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
