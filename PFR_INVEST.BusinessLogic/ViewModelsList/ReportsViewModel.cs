﻿using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    //Модель заглушка для отчетов
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class ReportsViewModel : ViewModelList
    {

        public ReportsViewModel()
        {
            DataObjectTypeForJournal = typeof(object);
        }

        protected override void ExecuteRefreshList(object param)
        {

        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
