﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class CorrespondenceVRArchiveListViewModel : CorrespondenceVRBaseListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            base.ExecuteRefreshList(Document.Statuses.Executed);
        }
    }
}
