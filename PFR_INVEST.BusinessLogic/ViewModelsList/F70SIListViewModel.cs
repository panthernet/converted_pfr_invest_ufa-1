﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F70SIListViewModel : F70ListViewModel
    {

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountF70ListByTypeContractHib((int)Document.Types.SI);
        }

        protected override List<F070DetailsListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetF70ListByTypeContractPageHib((int)Document.Types.SI, (int)index); ;
        }


        //protected override void ExecuteRefreshList(object param)
        //{
        //    this.CalculateDocNumbers(BLServiceSystem.Client.GetF70ListByTypeContractHib((int)Document.Types.SI));
        //}
    }
}
