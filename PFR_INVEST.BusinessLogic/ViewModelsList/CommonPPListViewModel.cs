﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public sealed class CommonPPListViewModel : PagedLoadObservableListModelBase<PPListItem>, ISettingOpenForm, IUpdateListenerModel
	{
		public List<Element> DirectionsList { get; private set; }
		public List<Element> LinksList { get; private set; }
		public List<Element> SectionsList { get; private set; }
	    private bool _isOpfr;

	    public bool IsOPFR => _isOpfr;

		public CommonPPListViewModel(bool isOpfr = false, object param = null)
			: base(param)
		{
		    _isOpfr = isOpfr;
			ID = -1;
		    DataObjectTypeForJournal = typeof (AsgFinTr);
			DirectionsList = BLServiceSystem.Client.GetElementByType(Element.Types.PPDirection);
			LinksList = BLServiceSystem.Client.GetElementByType(Element.Types.PPLink);
			SectionsList = BLServiceSystem.Client.GetElementByType(Element.Types.PPSection);
		}


		protected override void BeforeListRefresh()
		{
			if (SettingsProvider == null)
			{
			    SettingsProvider = new OpenSettingsProvider("CommonPPListViewModel", "Бэк-офис - Перечисления - Платежные поручения", (s) => ExecuteRefreshList(null)) {IsPeriodVisible = true};
			}
        }

		protected override long GetListCount()
		{
			return BLServiceSystem.Client.GetCommonPPCount(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, _isOpfr);
		}


		protected override List<PPListItem> GetListPart(long index)
		{
			var part = BLServiceSystem.Client.GetCommonPPByPage(index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, _isOpfr);

			return part;
		}


		public OpenSettingsProvider SettingsProvider { get; set; }


		public Type[] UpdateListenTypes => new[] { typeof(AsgFinTr) };

	    public void OnDataUpdate(Type type, long id)
		{			
			var item = BLServiceSystem.Client.GetCommonPPByID(id, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
			var old = List.FirstOrDefault(l => l.ID == id);
			
			UpdateRecord(old, item);
		}
	}
}
