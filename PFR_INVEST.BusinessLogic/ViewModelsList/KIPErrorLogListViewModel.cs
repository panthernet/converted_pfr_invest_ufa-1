﻿using System;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [ModelEntity(Type = typeof(KipLog), JournalDescription = "Журнал ошибок импорта/экспорта ПТК КИП")]
	public class KIPErrorLogListViewModel : ViewModelList<KipLog>
	{

		private DateTime? dateFrom = DateTime.Now.AddDays(-7);
		public DateTime? DateFrom
		{
			get { return dateFrom; }
			set
			{
				if (dateFrom != value)
				{
					dateFrom = value;
					OnPropertyChanged("DateFrom");
				}
			}
		}

		private DateTime? _dateTo = DateTime.Now;
		public DateTime? DateTo
		{
			get { return _dateTo; }
			set
			{
				if (_dateTo != value)
				{
					_dateTo = value;
					OnPropertyChanged("DateTo");
				}
			}
		}



		public bool UserIsAdministrator => AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator);

	    protected override void ExecuteRefreshList(object param)
		{
			List = BLServiceSystem.Client.GetKIPLogList(DateFrom ?? DateTime.MinValue, DateTo ?? DateTime.MaxValue);

		}

	}
}
