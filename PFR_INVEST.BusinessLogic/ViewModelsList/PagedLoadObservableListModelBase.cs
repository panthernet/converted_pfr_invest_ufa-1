﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.BusinessLogic
{
	public abstract class PagedLoadObservableListModelBase<T> : PagedLoadListModelBase<T>
	{
		private ObservableCollectionEx<T> _mList;
		public new ObservableCollectionEx<T> List
		{
			get { return _mList; }
			private set
			{
				_mList = value;
				OnPropertyChanged(nameof(List));
			}
		}

	    protected PagedLoadObservableListModelBase(object param = null)
			: base(param){}

		protected override void ListLoadedInternal(List<T> list)
		{
			List = new ObservableCollectionEx<T>(list);
		}

		/// <summary>
		/// Заменяет старую запись в гриде на новую без передёргивания грида
		/// </summary>
		/// <param name="oldItem">Старая запись из грида</param>
		/// <param name="newItem">Новая запись для грида</param>
		protected void UpdateRecord(T oldItem, T newItem)
		{
			if (newItem != null
				&& (!(newItem is IMarkedAsDeleted) || !(newItem as IMarkedAsDeleted).IsDeleted()))
				List.Add(newItem);
			if (oldItem != null)
				List.Remove(oldItem);
		}

		/// <summary>
		/// Заменяет старые записи в гриде на новые без передёргивания грида
		/// </summary>
		/// <param name="oldItems">Старая запись из грида</param>
		/// <param name="newItems">Новая запись для грида</param>
		protected void UpdateRecord(IEnumerable<T> oldItems, IEnumerable<T> newItems)
		{
		    _mList.SupressNotification = true;
            OnPropertyChanged("ListRefreshing");
            if (oldItems != null)
                foreach (var r in oldItems)
                    List.Remove(r);

            if (newItems != null)
			    foreach (var r in newItems)
			    {
			        var reg = r as IMarkedAsDeleted;
			        if (reg == null || !reg.IsDeleted())
			            List.Add(r);
			    }
            _mList.SupressNotification = false;
            OnPropertyChanged(nameof(List));
            OnPropertyChanged("ListRefreshed");//Добавлено оповещение об окончании обновления записей для возобновления фокуса выбранного элемента в сгруппированных гридах на основе этого класса.
                                              

        }

        public class ObservableCollectionEx<T> : ObservableCollection<T>
        {
            private bool _notificationSupressed;
            private bool _supressNotification;
            public bool SupressNotification
            {
                get
                {
                    return _supressNotification;
                }
                set
                {
                    _supressNotification = value;
                    if (!_supressNotification && _notificationSupressed)
                    {
                        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                        _notificationSupressed = false;
                    }
                }
            }

            public ObservableCollectionEx()
            {
                
            }

            public ObservableCollectionEx(List<T> list)
                : base(list)
            {
                
            }

            protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            {
                if (SupressNotification)
                {
                    _notificationSupressed = true;
                    return;
                }
                base.OnCollectionChanged(e);
            }
        }
    }
}
