﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
	public abstract class SPNMovementsListViewModel : ViewModelList<TransferListItem>
    {
		public abstract Document.Types Type { get; }

        protected SPNMovementsListViewModel()
        {
            DataObjectTypeForJournal = typeof(Transfer);
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = BLServiceSystem.Client.GetTransfersListForSPNMoveList(Type);
        }       
    }
}
