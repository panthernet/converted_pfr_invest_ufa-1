﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F80SIListViewModel : F80ListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            this.CalculateDocNumbers(BLServiceSystem.Client.GetF80ListByContractTypeHib((int)Document.Types.SI));
        }
    }
}
