﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public class UKTransfersListViewModel : PagedLoadObservableListModelBase<UKListItem>, IUpdateListenerModel
	{
		public UKTransfersListViewModel(object ppEntered)
			: base(ppEntered)
		{
            DataObjectTypeForJournal = typeof(ReqTransfer);
		}

		private bool _ppEntered;		

		protected override void BeforeListRefresh()
		{
			_ppEntered = (bool)RefreshParameter;
			
		}


		protected override long GetListCount()
		{
			return BLServiceSystem.Client.GetCountUKTransfersList(_ppEntered);
		}

		protected override List<UKListItem> GetListPart(long index)
		{
			return BLServiceSystem.Client.GetUKTransfersListHibByPage(_ppEntered, (int)index).ConvertAll(
					 tr => new UKListItem(tr));
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(ReqTransfer) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
		    if (type != typeof (ReqTransfer)) return;
		    var data = BLServiceSystem.Client.GetUKTransfersListHibByID(_ppEntered, id).ConvertAll(
		        tr => new UKListItem(tr));

		    var old = List.Where(r => r.ID == id).ToList();

		    UpdateRecord(old, data);
		}
	}
}
