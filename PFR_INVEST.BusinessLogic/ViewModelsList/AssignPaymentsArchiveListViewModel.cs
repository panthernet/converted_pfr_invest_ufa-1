﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class AssignPaymentsArchiveListViewModel : ViewModelList
    {
        public delegate void SelectGridRowDelegate(long RID);

        private List<TransferListItem> _APTransfers = new List<TransferListItem>();

        public event SelectGridRowDelegate OnSelectGridRowDelegate;

        public List<TransferListItem> APArchiveTransfers
        {
            get
            {
                RaiseDataRefreshing();
                return _APTransfers;
            }
            set
            {
                _APTransfers = value;
                OnPropertyChanged("APArchiveTransfers");
            }
        }

        public void OnSelectGridRow(long RID)
        {
            if (OnSelectGridRowDelegate != null)
            {
                OnSelectGridRowDelegate(RID);
            }
        }

        protected override void ExecuteRefreshList(object param)
        {
            APArchiveTransfers = GetTransfers();
        }

        protected abstract List<TransferListItem> GetTransfers();

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
