﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class AnalyzeInsuredpersonListViewModel : ViewModelListObservable<AnalyzeInsuredpersonReport>
    {
        public virtual string FormTitle
            =>
                "Форма 4. Количество застрахованных лиц в системе обязательного пенсионного страхования, нарастающим итогом"
            ;

        public bool IsArchiveMode { get; set; }
        public int? Year { get; set; }
        public int? Quark { get; set; }

        protected override void ExecuteRefreshList(object param)
        {
            var data = DataContainerFacade.GetClient().GetInsuredpersonReportsByYearKvartal(Year, Quark, IsArchiveMode);
            data.ForEach(l => l.QuarkDisplay = QuarkDisplayHelper.GetQuarkDisplayValue(l.Kvartal));
            List.Fill(data);
        }
    }
}