﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
	public class MarketPricesListViewModel : ViewModelListObservable<MarketPrice>, IUpdateListenerModel
	{

        public MarketPricesListViewModel()
        {
            DataObjectTypeForJournal = typeof(MarketPrice);
        }

		protected override void ExecuteRefreshList(object param)
		{
			var allPrices = DataContainerFacade.GetList<MarketPrice>();
			List.Fill(allPrices.FindAll(p => p.StatusID != -1).ToList());
		}

		protected override bool CanExecuteRefreshList()
		{
			return true;
		}

		public Type[] UpdateListenTypes => new[] { typeof(MarketPrice) };

	    public void OnDataUpdate(Type type, long id)
		{
		    if (type != typeof (MarketPrice)) return;
		    var old = List.FirstOrDefault(m => m.ID == id);
		    var item = DataContainerFacade.GetByID<MarketPrice>(id);
		    base.UpdateRecord(old, item);
		}
	}
}
