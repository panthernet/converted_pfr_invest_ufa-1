﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class RejAppListViewModel : ViewModelList
    {
        public bool IsBusy { get; set; }
        public string BusyMessage => "Загружаем данные...";

        private List<RejectApplication> _rejAppList;
        public List<RejectApplication> RejAppList
        {
            get
            {
                return _rejAppList;
            }
            set
            {
                _rejAppList = value;
                OnPropertyChanged("RejAppList");
            }
        }

        public RejAppListViewModel()
        {
            DataObjectTypeForJournal = typeof(RejectApplication);
        }

        protected override void ExecuteRefreshList(object param)
        {
            IsBusy = true;
            RejAppList = BLServiceSystem.Client.GetRejectApplications();//  DataContainerFacade.GetList<RejectApplication>();
            IsBusy = false;
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
