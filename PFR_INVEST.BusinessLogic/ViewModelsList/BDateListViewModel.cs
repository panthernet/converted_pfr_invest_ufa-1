﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BDateListViewModel : ViewModelList
    {
        private List<BDate> m_BDateList;
        public List<BDate> BDateList
        {
            get
            {
                RaiseDataRefreshing();
                return m_BDateList;
            }
            set
            {
                m_BDateList = value;
                OnPropertyChanged("BDateList");
            }
        }

        public BDateListViewModel()
        {
            DataObjectTypeForJournal = typeof(BDate);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object param)
        {
            BDateList = DataContainerFacade.GetList<BDate>();
        }
    }
}
