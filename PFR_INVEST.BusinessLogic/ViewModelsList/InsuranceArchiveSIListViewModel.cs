﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class InsuranceArchiveSIListViewModel : InsuranceListViewModel
    {
        private const bool ARCHIVE = true;

        protected override void ExecuteRefreshList(object param)
        {
            InsuranceList = BLServiceSystem.Client.GetInsuranceListByType(ARCHIVE, (int)Document.Types.SI);
        }



        public InsuranceArchiveSIListViewModel()
            : base(ARCHIVE)
        {
            DataObjectTypeForJournal = typeof(InsuranceDoc);           
        }

        

    }
}
