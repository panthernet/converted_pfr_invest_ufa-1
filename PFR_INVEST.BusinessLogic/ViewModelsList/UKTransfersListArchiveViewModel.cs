﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public sealed class UKTransfersListArchiveViewModel : UKTransfersListViewModel, ISettingOpenForm, IUpdateListenerModel
    {
        public OpenSettingsProvider SettingsProvider { get; set; }

        public UKTransfersListArchiveViewModel()
            : base(true)
        {
            SettingsProvider = new OpenSettingsProvider("UKTransfersListArchiveViewModel", "Бэк-офис - Перечисления - Архив перечислений УК", (s) => ExecuteRefreshList(null));
            ExecuteRefreshList(null);
        }
               
        protected override long GetListCount()
        {
            if (SettingsProvider == null) return -1;
            return BLServiceSystem.Client.GetCountUKTransfersListArchiveFiltered(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override List<UKListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetUKTransfersListHibByPageArchiveFiltered(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, (int)index).ConvertAll(
                    tr => new UKListItem(tr));
        }

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(ReqTransfer)};

        void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
		    if (type != typeof (ReqTransfer)) return;
		    var data = BLServiceSystem.Client.GetUKTransfersListHibByPageArchiveFiltered(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd, 0, id).ConvertAll(
		        tr => new UKListItem(tr));
		    var old = List.Where(r => r.ID == id).ToList();

		    UpdateRecord(old, data);
		}
    }
}
