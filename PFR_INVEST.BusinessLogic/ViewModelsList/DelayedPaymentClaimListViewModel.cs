﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class DelayedPaymentClaimListViewModel : ViewModelList<DelayedPaymentClaim>
    {
        public DelayedPaymentClaimListViewModel()
        {
            DataObjectTypeForJournal = typeof(DelayedPaymentClaim);
        }

        protected override void ExecuteRefreshList(object param)
        {
            this.List = DataContainerFacade.GetList<DelayedPaymentClaim>();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
