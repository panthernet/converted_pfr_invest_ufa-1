﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class DepClaim2OfferListViewModel : ViewModelListObservable<DepClaimOfferListItem>, IDepClaimOfferProviderEx, IUpdateListenerModel
    {

        /*private List<DepClaimOfferListItem> _list;
        public List<DepClaimOfferListItem> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }*/
        public delegate void SelectGridRowDelegate(long rID);
        public event SelectGridRowDelegate OnSelectGridRowDelegate;

        public void OnSelectGridRow(long rID)
        {
            if (OnSelectGridRowDelegate != null)
            {
                OnSelectGridRowDelegate(rID);
            }
        }

        public DepClaimOfferListItem SelectedItem { get; set; }

        public DepClaimSelectParams SelectedAuction { get; set; }


        public long? SelectedAuctionID { get; set; }
        public long? SelectedOfferID { get; set; }


        public DepClaim2OfferListViewModel()
        {
            DataObjectTypeForJournal = typeof (DepClaimOffer);
        }

        public override bool CanExecuteDelete()
        {
            return false;
        }

        protected override void ExecuteRefreshList(object param)
        {
            List.Fill(BLServiceSystem.Client.GetDepClaimOfferList());
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }




        long? IDepClaimAuctionProvider.AuctionID => SelectedAuctionID;

        long? IDepClaimOfferProvider.OfferID => SelectedOfferID;

        DepClaimOffer IDepClaimOfferProviderEx.Offer => SelectedItem != null ? SelectedItem.Offer : null;

        public DepClaimSelectParams.Statuses? AuctionStatus => SelectedAuction == null ? (DepClaimSelectParams.Statuses?)null : SelectedAuction.AuctionStatus;

        public Type[] UpdateListenTypes => new[] {typeof (DepClaimOffer)};

        public void OnDataUpdate(Type type, long id)
        {
            if(type != typeof(DepClaimOffer)) return;

            var old = List.FirstOrDefault(a => a.Offer.ID == id);
            var li = BLServiceSystem.Client.GetDepClaimOfferById(id);
            if (li == null) return;
            base.UpdateRecord(old, li);
            OnSelectGridRow(id);
        }
    }
}
