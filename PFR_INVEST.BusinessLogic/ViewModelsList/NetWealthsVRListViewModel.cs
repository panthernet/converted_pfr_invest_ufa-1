﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class NetWealthsVRListViewModel : NetWealthsListViewModel
    {
        public NetWealthsVRListViewModel() : base() { }

        protected override Document.Types DocumentType => Document.Types.VR;

        protected override void BeforeListRefresh()
		{
			this.dict = new Dictionary<string, List<NetWealthListItem>>();
			if (this.SettingsProvider == null)
				this.SettingsProvider = new OpenSettingsProvider("Чистые активы (СЧА) ВР", "Работа с ВР - Отчеты ГУК ВР - Чистые активы (СЧА)", (s) => this.ExecuteRefreshList(null));
			if (!IsOnDataRefreshed)
			{
				OnDataRefreshed += new EventHandler((object sender, EventArgs e) => this.UpdateDocNumbers(this.TmpList));
			}
		}  
    }
}
