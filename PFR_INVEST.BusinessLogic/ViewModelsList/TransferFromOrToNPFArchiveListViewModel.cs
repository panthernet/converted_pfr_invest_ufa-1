﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class TransferFromOrToNPFArchiveListViewModel : PagedLoadListModelBase<TransferFromOrToNPFListItem>, ISettingOpenForm
    {
        public OpenSettingsProvider SettingsProvider { get; set; }
       
        public TransferFromOrToNPFArchiveListViewModel()
        {
            DataObjectTypeForJournal = typeof (AsgFinTr);
        }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountTransferToNPFArchiveListFiltered(this.SettingsProvider.Filter.PeriodStart, this.SettingsProvider.Filter.PeriodEnd);
        }

        protected override List<TransferFromOrToNPFListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetTransferToNPFArchiveListFilteredByPage((int)index, this.SettingsProvider.Filter.PeriodStart, this.SettingsProvider.Filter.PeriodEnd);
        }

        protected override void BeforeListRefresh()
        {
            base.BeforeListRefresh();
            if(this.SettingsProvider == null)
                this.SettingsProvider = new OpenSettingsProvider("TransferFromOrToNPFArchiveListViewModel", "Бэк-офис - Перечисления - Передача средств в НПФ Архив", (s) => this.ExecuteRefreshList(null));
        }

    }
}
