﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
	public abstract class TransfersListViewModel : ViewModelList<TransferListItem>
    {
		public abstract Document.Types Type { get; }
			
        public delegate void SelectGridRowDelegate(long rid);

        public event SelectGridRowDelegate OnSelectGridRow;

        public void RaiseSelectGridRow(long rid)
        {
            OnSelectGridRow?.Invoke(rid);
        }

        public OpenSettingsProvider SettingsProvider { get; set; }

		//protected override void BeforeListRefresh()
		//{
		//    if (this.SettingsProvider == null)
		//        this.SettingsProvider = new Misc.OpenSettingsProvider("TransfersListViewModel", "Работа с СИ - Перечисление средств - Перечисления", (s) => this.ExecuteRefreshList(null));
		//}

        protected TransfersListViewModel()
        {
            DataObjectTypeForJournal = typeof(Transfer);
        }

		protected override void ExecuteRefreshList(object param)
		{
			if (this.SettingsProvider == null)
				 this.SettingsProvider = new OpenSettingsProvider("TransfersListViewModel", "Работа с СИ - Перечисление средств - Перечисления", (s) => this.ExecuteRefreshList(null));
			if (this.SettingsProvider != null && this.SettingsProvider.IsPeriodChecked)
			{
				List = BLServiceSystem.Client.GetTransfersListBYContractType(this.Type, false, false, false, 0, this.SettingsProvider.PeriodStart, this.SettingsProvider.PeriodEnd);
			}
			else
			{
				List = BLServiceSystem.Client.GetTransfersListBYContractType(this.Type, false, false, false);
			}
		}
	}
}
