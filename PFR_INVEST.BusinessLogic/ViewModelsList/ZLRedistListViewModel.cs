﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    public class ZLRedistListViewModel : ViewModelList
    {
        private List<ERZLListItem> m_ZLRedistList;
        public List<ERZLListItem> ZLRedistList
        {
            get
            {
                RaiseDataRefreshing();
                return m_ZLRedistList;
            }
            set
            {
                m_ZLRedistList = value;
                OnPropertyChanged("ZLRedistList");
            }
        }

        public ZLRedistListViewModel()
        {
            DataObjectTypeForJournal = typeof(ERZL);
        }

        protected override void ExecuteRefreshList(object param)
        {
            ZLRedistList = new List<ERZLListItem>(BLServiceSystem.Client.GetERZLListHib());
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
