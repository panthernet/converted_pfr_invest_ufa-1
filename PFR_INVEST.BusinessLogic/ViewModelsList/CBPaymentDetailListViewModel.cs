﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class CBPaymentDetailListViewModel: ViewModelList<CBPaymentDetailListItem>
    {   
        public CBPaymentDetailListItem FocusedPaymentDetail { get; set; }

        public DelegateCommand<object> AddPaymentDetail { get; private set; }
        public DelegateCommand<CBPaymentDetailListItem> DeletePaymentDetail { get; private set; }
        public DelegateCommand<CBPaymentDetailListItem> EditPaymentDetail { get; private set; }

        public CBPaymentDetailListViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            AddPaymentDetail = new DelegateCommand<object>(o => CanExecuteAddPaymentDetail(), ExecuteAddPaymentDetail);
            DeletePaymentDetail = new DelegateCommand<CBPaymentDetailListItem>(CanExecuteDeletePaymentDetail, ExecuteDeletePaymentDetail);
            EditPaymentDetail = new DelegateCommand<CBPaymentDetailListItem>(CanExecuteEditPaymentDetail, ExecuteEditPaymentDetail);
        }

        private bool CanExecuteAddPaymentDetail()
        {
            return EditAccessAllowed;
        }

        private void ExecuteAddPaymentDetail(object o)
        {
            var item = new CBPaymentDetail();
            if (DialogHelper.EditCBPaymentDetail(item, true))
            {
                ExecuteRefreshList(null);
            }
        }

        private bool CanExecuteDeletePaymentDetail(CBPaymentDetailListItem item)
        {
            return List.Contains(item);
        }

        private void ExecuteDeletePaymentDetail(CBPaymentDetailListItem item)
        {
            var msg = string.Format("Удалить назначение платежа \"{0}\"", item.ReportPaymentDetail);
            if (DialogHelper.ShowConfirmation(msg))
            {
                DataContainerFacade.Delete<CBPaymentDetail>(item.CBPaymentDetail);
                ExecuteRefreshList(null);
            }
        }

        private bool CanExecuteEditPaymentDetail(CBPaymentDetailListItem item)
        {
            return List.Contains(item);
        }

        private void ExecuteEditPaymentDetail(CBPaymentDetailListItem item)
        {
            if (DialogHelper.EditCBPaymentDetail(item.CBPaymentDetail, false))
            {
                ExecuteRefreshList(null);
            }
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = BLServiceSystem.Client.GetCBPaymentDetailList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
