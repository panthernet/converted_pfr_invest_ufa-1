﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PFRAccountsListViewModel : ViewModelList
    {
        private List<PFRAccountsListItem> m_PFRAccountsList;
        public List<PFRAccountsListItem> PFRAccountsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_PFRAccountsList;
            }
            set
            {
                m_PFRAccountsList = value;
                OnPropertyChanged("PFRAccountsList");
            }
        }

        public PFRAccountsListViewModel()
        {
            DataObjectTypeForJournal = typeof(PfrBankAccount);
        }


        protected override void ExecuteRefreshList(object param)
        {
            PFRAccountsList = BLServiceSystem.Client.GetPFRAccountsList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
