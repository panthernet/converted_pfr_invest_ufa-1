﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    public class RatingsListViewModel : ViewModelList
    {
        public RatingsListViewModel()
            : base(typeof(AgencyRatingViewModel))
        {
            DataObjectTypeForJournal = typeof(Rating);
            RegisterRatingCommands();
        }

        private IList<Rating> m_RatingsList;

        #region Commands
        public DelegateCommand<Rating> DeleteRatingCommand { get; private set; }
        public DelegateCommand<Rating> EditRatingCommand { get; private set; }
        public DelegateCommand<object> AddRatingCommand { get; private set; }

        public void RegisterRatingCommands()
        {
            this.DeleteRatingCommand = new DelegateCommand<Rating>(r => this.RatingsList.Count > 0 && r != null,
                r =>
                {
                    if (IsRatingInUse(r))
                    {
                        DialogHelper.ShowError("Перед удалением рейтинга необходимо удалить все связанные коэффициенты");
                        return;
                    }

                    if (!DialogHelper.ShowExclamation(string.Format("Удалить рейтинг '{0}' ?", r.RatingName), "Удаление рейтинга", true)) return;
                    DataContainerFacade.Delete(r);
                    RatingsList = BLServiceSystem.Client.GetRatingsListHib();
                    ViewModelManager.RefreshViewModels(typeof(AgencyRatingViewModel));
                });
            this.EditRatingCommand = new DelegateCommand<Rating>(r => this.RatingsList.Count > 0 && r != null,
                r =>
                {
                    if (!DialogHelper.EditRating(r, false)) return;
                    RatingsList = BLServiceSystem.Client.GetRatingsListHib();
                    ViewModelManager.RefreshViewModels(typeof(AgencyRatingViewModel));
                });
            this.AddRatingCommand = new DelegateCommand<object>(o => true,
                o =>
                {
                    var h = new Rating();
                    if (!DialogHelper.EditRating(h, true)) return;
                    RatingsList = BLServiceSystem.Client.GetRatingsListHib();
                    ViewModelManager.RefreshViewModels(typeof(AgencyRatingViewModel));
                });
        }

        private static bool IsRatingInUse(Rating rating)
        {
            var multipliersLinked = DataContainerFacade.GetListByProperty<MultiplierRating>("RatingID", rating.ID);
            return multipliersLinked.Count > 0;
        }
        #endregion Commands

        public IList<Rating> RatingsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_RatingsList;
            }
            set
            {
                m_RatingsList = value;
                OnPropertyChanged("RatingsList");
            }
        }

        protected override void ExecuteRefreshList(object param)
        {
            RatingsList = BLServiceSystem.Client.GetRatingsListHib();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }


        public void EditSelectedRating(long id)
        {
            var rating = DataContainerFacade.GetByID<Rating>(id);
            if (DialogHelper.EditRating(rating, false))
            {
                ExecuteRefreshList(null);
            }
        }
    }
}
