﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F80VRListViewModel : F80ListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            this.CalculateDocNumbers(BLServiceSystem.Client.GetF80ListByContractTypeHib((int)Document.Types.VR));
        }
    }
}
