﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class NetWealthsSumListViewModel : PagedLoadListModelBase<NetWealthListItem>
    {
        public OpenSettingsProvider SettingsProvider { get; set; }

        protected long F010ListCount { get; set; }
        protected long F012ListCount { get; set; }
        protected long F014ListCount { get; set; }
        protected long F016ListCount { get; set; }

        public NetWealthsSumListViewModel(object param) : base(param) 
        {
            DataObjectTypeForJournal = typeof(object);
            DataObjectDescForJournal = "Составной список. Использует EDO_ODKF010, EDO_ODKF012, EDO_ODKF016";
        }

        protected override long GetListCount()
        {
            if (SettingsProvider == null) return -1;
            //F015 отключено, так как не содержит совокупных
            F010ListCount = BLServiceSystem.Client.GetCountNetWealthsSumF010(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            F012ListCount = BLServiceSystem.Client.GetCountNetWealthsSumF012(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);

            //F014 отключено так как полностью скопирована в 016 при миграции
            //F014ListCount = BLServiceSystem.Client.GetCountNetWealthsF014(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            F016ListCount = BLServiceSystem.Client.GetCountNetWealthsF016(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            return F010ListCount + F012ListCount + F014ListCount + F016ListCount;

        }

        protected override void BeforeListRefresh()
        {
            if (this.SettingsProvider != null) return;
            if (!(this.RefreshParameter is List<string>)) return;
            var list = ((List<string>) this.RefreshParameter);
            var formName = list.FirstOrDefault();
            var comment = list.Skip(1).FirstOrDefault();
            this.SettingsProvider = new OpenSettingsProvider(formName, comment, (s) => this.ExecuteRefreshList(null));
        }

        protected override List<NetWealthListItem> AfterListLoaded(List<NetWealthListItem> list)
        {
            //Округление с 4 до 2
            list.ForEach(i => { i.NetWealthSum = Math.Round(i.NetWealthSum ?? 0, 2, MidpointRounding.AwayFromZero); });
            return base.AfterListLoaded(list);
        }

        protected override List<NetWealthListItem> GetListPart(long index)
        {

            if (index < F010ListCount)
            {
                return BLServiceSystem.Client.GetNetWealthsSumF010ListByOnlyPage((int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList();
            }
            else if (index < F010ListCount + F012ListCount)
            {
                return BLServiceSystem.Client.GetNetWealthsSumF012ListByPage((int)(index - F010ListCount), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList();
            }
            else if (index < F010ListCount + F012ListCount + F014ListCount)
            {
                return
                    BLServiceSystem.Client.GetNetWealthsF014ListByPage((int)(index - (F010ListCount + F012ListCount)), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd)
                        .ToList();
            }
            else //if(index < F010ListCount + F012ListCount + F014ListCount + F016ListCount)
            {
                var f = BLServiceSystem.Client.GetNetWealthsF016ListByPage((int)(index - (F010ListCount + F012ListCount + F014ListCount)), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList();
                return BLServiceSystem.Client.GetNetWealthsF016ListByPage((int)(index - (F010ListCount + F012ListCount + F014ListCount)), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList();

            }


        }


        #region Old
        //private List<NetWealthListItem> m_List;
        //public List<NetWealthListItem> List
        //{
        //    get
        //    {
        //        return m_List;
        //    }
        //    set
        //    {
        //        m_List = value;
        //        OnPropertyChanged("List");
        //    }
        //}

        //private bool CanExecuteRefresh()
        //{
        //    return true;
        //}

        //protected override void ExecuteRefreshList(object param)
        //{
        //    //http://jira.vs.it.ru/browse/PFRPTK-1204?focusedCommentId=181770&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-181770
        //    //В общем в гриде должны показываться данные из всех 4-х таблиц (EDO_ODKF010, EDO_ODKF012, EDO_ODKF014, EDO_ODKF016) 

        //    List<NetWealthListItem> list = new List<NetWealthListItem>();

        // list.AddRange(BLServiceSystem.Client.GetNetWealthsF010List());
        //    list.AddRange(BLServiceSystem.Client.GetNetWealthsF012List());
        //    list.AddRange(BLServiceSystem.Client.GetNetWealthsF014List());
        //    list.AddRange(BLServiceSystem.Client.GetNetWealthsF016List());


        //    //list.AddRange(BLServiceSystem.Client.GetNetWealthsF010ListNullContract());
        //    //list.AddRange(BLServiceSystem.Client.GetNetWealthsF016List());
        //    //list.AddRange(BLServiceSystem.Client.GetNetWealthsF012List());
        //    //list.AddRange(BLServiceSystem.Client.GetNetWealthsF014List()); использовать GetNetWealthsF016List
        //    List = list;
        //}

        //protected override bool CanExecuteRefreshList()
        //{
        //    return true;
        //}
        #endregion

    }
}
