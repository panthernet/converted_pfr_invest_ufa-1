﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    public class AgencyRatingsListViewModel : ViewModelList
    {
        private IList<AgencyRatingListItem> m_AgencyRatingsList;
        public IList<AgencyRatingListItem> AgencyRatingsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_AgencyRatingsList;
            }
            set
            {
                m_AgencyRatingsList = value;
                OnPropertyChanged("AgencyRatingsList");
            }
        }

        public AgencyRatingsListViewModel()
        {
            DataObjectTypeForJournal = typeof (MultiplierRating);
        }

        protected override void ExecuteRefreshList(object param)
        {
            AgencyRatingsList = BLServiceSystem.Client.GetAgencyRatingsListHib();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

    }
}
