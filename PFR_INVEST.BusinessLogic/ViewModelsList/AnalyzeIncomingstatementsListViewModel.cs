﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    public class AnalyzeIncomingstatementsListViewModel : ViewModelListObservable<AnalyzeIncomingstatementsReport>
    {
        public virtual string FormTitle
            =>
                "Форма 6. Принятые заявления застрахованных лиц по формированию средств пенсионных накоплений с разбивкой по способу подачи"
            ;

        public bool IsArchiveMode { get; set; }
        public int? Year { get; set; }
        public int? Quark { get; set; }

        protected override void ExecuteRefreshList(object param)
        {
            var data = DataContainerFacade.GetClient().GetIncomingReportsByYearKvartal(Year, Quark, IsArchiveMode);
            data.ForEach(l => l.QuarkDisplay = QuarkDisplayHelper.GetQuarkDisplayValue(l.Kvartal));
            List.Fill(data);
        }
    }
}