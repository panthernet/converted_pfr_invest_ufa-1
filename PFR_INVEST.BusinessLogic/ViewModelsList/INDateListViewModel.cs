﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class INDateListViewModel : ViewModelList
    {
        private List<INDate> m_INDateList;
        public List<INDate> INDateList
        {
            get
            {
                RaiseDataRefreshing();
                return m_INDateList;
            }
            set
            {
                m_INDateList = value;
                OnPropertyChanged("INDateList");
            }
        }

        public INDateListViewModel()
        {
            DataObjectTypeForJournal = typeof(INDate);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object param)
        {
            INDateList = DataContainerFacade.GetList<INDate>();
        }
    }
}
