﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class DivisionListViewModel : ViewModelList
    {
        private List<Division> _list;
        public List<Division> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        private Division _selectedDivision;

        public Division SelectedDivision
        {
            get { return _selectedDivision; }
            set
            {
                _selectedDivision = value;
                OnPropertyChanged("SelectedDivision");
            }
        }

        public DivisionListViewModel()
        {
            DataObjectTypeForJournal = typeof(Division);
        }


        protected override void ExecuteRefreshList(object param)
        {
            List = DataContainerFacade.GetList<Division>().ToList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
