﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    //[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    //[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public abstract class InsuranceListViewModel : ViewModelList
    {
        private List<InsuranceListItem> _mInsuranceList;
        public List<InsuranceListItem> InsuranceList
        {
            get
            {
                RaiseDataRefreshing();
                return _mInsuranceList;
            }
            set
            {
                _mInsuranceList = value;
                OnPropertyChanged("InsuranceList");
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object param)
        {
            InsuranceList = BLServiceSystem.Client.GetInsuranceList((bool)param);
            
        }


        protected InsuranceListViewModel(bool archive)
            : base(archive)
        {
            
        }

       
    }
}
