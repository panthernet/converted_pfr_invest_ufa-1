﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class NPFCodeListViewModel : ViewModelList
    {
        private List<NPFErrorCode> _list;
        public List<NPFErrorCode> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        private NPFErrorCode _selectedNPFCode;

        public NPFErrorCode SelectedNPFCode
        {
            get { return _selectedNPFCode; }
            set
            {
                _selectedNPFCode = value;
                OnPropertyChanged("SelectedNPFCode");
            }
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = DataContainerFacade.GetListByPropertyConditions<NPFErrorCode>(
                new List<ListPropertyCondition>(){
                ListPropertyCondition.Equal("IsErrorCode", this.IsErrorCode ? 1 : 0),
                ListPropertyCondition.NotEqual("StatusID", (long)-1),
                }).ToList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        public bool IsErrorCode { get; private set; }

        public NPFCodeListViewModel(bool isErrorCode)
        {
            DataObjectTypeForJournal = typeof (NPFErrorCode);
            this.IsErrorCode = isErrorCode;
            ExecuteRefreshList(null);
        }

        public string CodeColumnName => this.IsErrorCode ? "Код ошибки" : "Код стыковки с договором";
    }
}
