﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_directory_editor)]
    public class PfrBranchReportCashExpendituresListViewModel : ViewModelList
    {
        private List<PfrBranchReportCashExpendituresListItem> _items;
        public List<PfrBranchReportCashExpendituresListItem> Items
        {
            get
            {
                RaiseDataRefreshing();
                return _items;
            }
            set
            {
                _items = value;
                OnPropertyChanged("Items");
            }
        }

        public PfrBranchReportCashExpendituresListViewModel()
        {
            DataObjectTypeForJournal = typeof(PfrBranchReportCashExpenditures);
        }

        protected override void ExecuteRefreshList(object param)
        {
            Items = BLServiceSystem.Client.GetPfrReportCashExpendituresList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
