﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class OnesImportJournalListViewModel : PagedLoadObservableListModelBase<OnesFileImport>, ISettingOpenForm
    {
        public override Type DataObjectTypeForJournal => typeof(OnesFileImport);
        public OpenSettingsProvider SettingsProvider { get; set; }
        public bool IsEventsRegisteredOnDataRefreshed;
        public List<string> DocKinds { get; set; }
        public string SelectedDocKind { get; set; }

        protected override long GetListCount()
        {
            UpdateDocKinds();
            return DataContainerFacade.GetListByPropertyConditionsCount<OnesFileImport>(GetPropList());
        }

        private List<ListPropertyCondition> GetPropList()
        {
            var cList = new List<ListPropertyCondition>
            {
                ListPropertyCondition.GreaterEqThan("LoadDate", SettingsProvider.Filter.PeriodStart),
                ListPropertyCondition.LessEqThan("LoadDate", SettingsProvider.Filter.PeriodEnd),
            };
            if (SelectedDocKind != DocKinds.First())
                cList.Add(ListPropertyCondition.Equal("DocKind", SelectedDocKind));
            return cList;
        }

        private void UpdateDocKinds()
        {
            var kind = SelectedDocKind;
            DocKinds = BLServiceSystem.Client.GetOnesKindsList();
            DocKinds.Insert(0, "(Все)");
            if (kind != null && DocKinds.Contains(kind))
                SelectedDocKind = kind;
            else SelectedDocKind = DocKinds.FirstOrDefault();
            OnPropertyChanged("SelectedDocKind");
            OnPropertyChanged("DocKinds");            
        }

        protected override List<OnesFileImport> GetListPart(long index)
        {
            return DataContainerFacade.GetListPageConditions<OnesFileImport>((int)index, GetPropList());
        }

        protected override void BeforeListRefresh()
        {
            if (!IsEventsRegisteredOnDataRefreshed)
                IsEventsRegisteredOnDataRefreshed = true;

            if (SettingsProvider == null)
            {
                SettingsProvider = new OpenSettingsProvider("OnesImportJournalListViewModel", "Импорт 1С - Загруженные файлы", s => ExecuteRefreshList(null))
                {
                    PeriodEnd = DateTime.Today,
                    PeriodStart = DateTime.Today//.AddYears(-1).Date
                };
            }
        }
    }
}
