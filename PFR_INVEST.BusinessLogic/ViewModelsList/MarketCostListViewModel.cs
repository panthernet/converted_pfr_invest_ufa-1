﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class MarketCostListViewModel : PagedLoadListModelBase<MarketCostListItem>
    {
        protected long F020ListCount { get; set; }
        protected long F025ListCount { get; set; }

		protected virtual Document.Types DocType => Document.Types.All;

        public OpenSettingsProvider SettingsProvider { get; set; }

        public MarketCostListViewModel()
        {
            DataObjectTypeForJournal = typeof (object);
            DataObjectDescForJournal = "Составной список EDO_ODKF020 и EDO_ODKF025";
        }

		protected override long GetListCount()
		{
			F020ListCount = BLServiceSystem.Client.GetCountMarketCostF020Filtered((int)DocType, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
			F025ListCount = BLServiceSystem.Client.GetCountMarketCostF025Filtered((int)DocType, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
			return F020ListCount + F025ListCount;

		}

		protected override List<MarketCostListItem> GetListPart(long index)
		{
			return index < F020ListCount ?
				BLServiceSystem.Client.GetMarketCostF020ListFilteredByPage((int)DocType, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList() :
				BLServiceSystem.Client.GetMarketCostF025ListFilteredByPage((int)DocType, (int)(index - F020ListCount), SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd).ToList();

		}

		protected override void BeforeListRefresh()
		{
			OnDataRefreshed += (sender, e) => this.UpdateDocNumbers(this.TmpList);
			if (this.SettingsProvider == null)
				this.SettingsProvider = new OpenSettingsProvider("MarketCostSIListViewModel", "Работа с СИ - Отчеты УК - Рыночная стоимость активов (РСА)", (s) => this.ExecuteRefreshList(null));

		}

	//    protected override long GetListCount()
	//    {
	//        F020ListCount = BLServiceSystem.Client.GetCountMarketCostF020((int)DocType);
	//        F025ListCount = BLServiceSystem.Client.GetCountMarketCostF025((int)DocType);
	//        return F020ListCount + F025ListCount;

	//    }

	//    protected override List<MarketCostListItem> GetListPart(long index)
	//    {
	//        return index < F020ListCount ?
	//BLServiceSystem.Client.GetMarketCostF020ListByPage((int)DocType, (int)index).ToList() :
	//BLServiceSystem.Client.GetMarketCostF025ListByPage((int)DocType, (int)(index - F020ListCount)).ToList();

	//    }

        protected void UpdateDocNumbers(List<MarketCostListItem> newList)
        {
            const string sFormat = "{0}-{1}";
            var dict = new Dictionary<string, List<MarketCostListItem>>();
            newList.ForEach(i =>
            {
                string key = i.ReportOnDate.HasValue ? string.Format(sFormat, i.ReportOnDate.Value.Year.ToString(), i.ReportOnDate.Value.Date.ToShortDateString()) : string.Format(sFormat, string.Empty, string.Empty);
                if (!dict.ContainsKey(key))
                    dict[key] = new List<MarketCostListItem>();
                dict[key].Add(i);
            });
            foreach (var l in dict)
            {
                int cnt = l.Value.Count;
                l.Value.ForEach(i =>
                {
                    i.lDocumentNumbers = cnt;
                });
            }
            this.List = newList;
            //OnPropertyChanged("List");
        }

        //protected void CalculateDocNumbers(List<MarketCostListItem> newList)
        //{
        //    UpdateDocNumbers(newList);
        //    this.List = newList;
        //}

		//protected override void BeforeListRefresh()
		//{

		//    OnDataRefreshed += new EventHandler((object sender, EventArgs e) => this.UpdateDocNumbers(this.t_List));

		//}


       
    }
}
