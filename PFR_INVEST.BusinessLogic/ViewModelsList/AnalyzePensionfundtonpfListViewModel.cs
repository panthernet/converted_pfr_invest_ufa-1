﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class AnalyzePensionfundtonpfListViewModel : ViewModelListObservable<AnalyzePensionfundtonpfReport>
    {
        public virtual string FormTitle
            =>
                "Форма 2. Средства пенсионных накоплений, переданные в управляющие компании и НПФ без учета возврата в ПФР"
            ;

        public virtual bool WithSubtract => false;

        public bool IsArchiveMode { get; set; }
        public int? Year { get; set; }
        public int? Quark { get; set; }

        protected override void ExecuteRefreshList(object param)
        {
            var data = DataContainerFacade.GetClient().GetPensionfundReportsByYearKvartal(Year, Quark, 0, IsArchiveMode);
            data.ForEach(l => l.QuarkDisplay = QuarkDisplayHelper.GetQuarkDisplayValue(l.Kvartal));
            List.Fill(data);
        }
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class AnalyzePensionfundtonpfWithReturnListViewModel : AnalyzePensionfundtonpfListViewModel
    {
        public override string FormTitle
            =>
                "Форма 3. Средства пенсионных накоплений, переданные в управляющие компании и НПФ за вычетом возврата в ПФР"
            ;

        public override bool WithSubtract => true;

        protected override void ExecuteRefreshList(object param)
        {
            var data = DataContainerFacade.GetClient().GetPensionfundReportsByYearKvartal(Year, Quark, 1, IsArchiveMode);
            data.ForEach(l => l.QuarkDisplay = QuarkDisplayHelper.GetQuarkDisplayValue(l.Kvartal));
            List.Fill(data);
        }
    }
}