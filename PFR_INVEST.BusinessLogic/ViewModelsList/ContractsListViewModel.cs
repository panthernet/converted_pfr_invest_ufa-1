﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class ContractsListViewModel : ViewModelList
    {
        private List<SIContractListItem> m_SIContractsList;

        protected ContractsListViewModel()
        {
        }

        public List<SIContractListItem> SIContractsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_SIContractsList;
            }
            set
            {
                m_SIContractsList = value;
                OnPropertyChanged("SIContractsList");
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        protected override void ExecuteRefreshList(object param)
        {
            this.SIContractsList = GetContractItems();
            //var allContractList = GetContractItems();
            //var allContracts = DataContainerFacade.GetList<Contract>();
            //var contracts = allContracts.FindAll(p => p.Status == -1);
            //foreach (var contract in contracts)
            //{
            //    allContractList.RemoveAll(p => p.ContractID == contract.ID);    
            //}
            //SIContractsList = allContractList;
        }

        protected abstract List<SIContractListItem> GetContractItems();
    }
}
