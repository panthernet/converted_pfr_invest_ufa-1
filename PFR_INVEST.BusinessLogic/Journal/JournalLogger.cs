﻿using PFR_INVEST.DataObjects.Journal;
using System;

namespace PFR_INVEST.BusinessLogic.Journal
{
    public class JournalLogger
    {
        private static JournalLogger _instance;
        public static JournalLogger Instance => _instance ?? (_instance = new JournalLogger());

        private static string GetHeader(ViewModelBase model)
        {
            var viewModelCard = model as ViewModelCard;
            if (viewModelCard != null && !string.IsNullOrEmpty(viewModelCard.GetDocumentHeader()))
                return viewModelCard.GetDocumentHeader();
            return model.Header;
        }

        private static string GetAdditionalInfo(ViewModelBase model)
        {
            var m = model as ViewModelCard;
            return m != null ? m.DocumentAdditionalInfo : null;
        }

        public static void LogModelEvent(ViewModelBase model, JournalEventType type)
        {
            LogModelEvent(model, type, string.Empty);
        }

        public static void LogTypeEvent(ViewModelBase model, JournalEventType type, string text, string additionalInfo = null)
        {
            if (model == null) return;
            long documentId = model.ID;
            LogEvent(text, type, documentId, model.GetType().Name, additionalInfo);
        }

        /// <summary>
        /// Логирование действий пользователей с моделями
        /// </summary>
        /// <param name="model">Модель</param>
        /// <param name="type">Тип события</param>
        /// <param name="additionalText">Текст</param>
        /// <param name="additionalInfo">Дополнительная текстовая информация</param>
        public static void LogModelEvent(ViewModelBase model, JournalEventType type, string additionalText, string additionalInfo = null)
        {
            if (model == null) return;
            var message = string.Format("{0} {1}", GetHeader(model), additionalText);
            var documentId = model.ID;
            additionalInfo = additionalInfo ?? GetAdditionalInfo(model);
            var entityType = model.GetModelEntityType();
            var entityDoc = model.GetModelEntityJComment();
            LogEvent(message, type, documentId, model.GetType().Name, additionalInfo, entityType, entityDoc);           
        }

        /// <summary>
        /// Логирование действий пользователей
        /// </summary>
        /// <param name="objectName">Сообщение для графы Наименование объекта</param>
        /// <param name="type">Тип события</param>
        /// <param name="documentId">ID документа</param>
        /// <param name="entityTypeName">Наименование типа используемых данных в модели</param>
        /// <param name="additionalInfo">Дополнительная текстовая информация</param>
        /// <param name="entityType">Тип объекта данных (DataObject) карточки, для получения доп. инфомации на стороне сервиса</param>
        /// <param name="entityDesc"></param>
        public static void LogEvent(string objectName, JournalEventType type, long? documentId, string entityTypeName, string additionalInfo = null, Type entityType = null, string entityDesc = null)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(a =>
                    {
                        try
                        {
                            if (!String.IsNullOrEmpty(additionalInfo))
                                additionalInfo = additionalInfo.Length > 500 ? additionalInfo.Substring(0, 476) + "..." : additionalInfo;
                            var isEntityLogged = entityType != null && entityType != typeof (object);
                            if (isEntityLogged || !string.IsNullOrEmpty(entityDesc))
                            {
                                BLServiceSystem.Client.SaveDetailedJornalLog(type, objectName, documentId, entityTypeName, additionalInfo, isEntityLogged ? entityType.Name : null, entityDesc);
                                return;
                            }
                            if (String.IsNullOrEmpty(additionalInfo))
                                BLServiceSystem.Client.SaveJornalLog(type, objectName, documentId, entityTypeName);
                            else
                                BLServiceSystem.Client.SaveJornalLogWithAdditionalInfo(type, objectName, documentId, entityTypeName, additionalInfo.Length > 500 ? additionalInfo.Substring(0, 476) + "..." : additionalInfo);
                        }
                        catch {

                        }
                    }
                    );
        }
        private static void LogEvent(string objectName, JournalEventType type)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(a =>
            {
                try
                {
                    BLServiceSystem.Client.SaveJornalLog(type, objectName, 0, string.Empty);
                }
                catch { }
            });
        }

        /// <summary>
        /// Логирование. Авторизация в системе
        /// </summary>
        public static void LogAuthorizationEvent(bool success = true)
        {
            LogEvent("Авторизация в системе", JournalEventType.AUTHORIZATION, null, string.Empty, success ? "УСПЕШНО" : "НЕ УСПЕШНО");
        }

        /// <summary>
        /// Логирование. Выход из системы
        /// </summary>
        public static void LogLogoutEvent()
        {
            LogEvent("Выход из системы вручную", JournalEventType.LOGOUT);
        }

        public static void LogOpenEvent(ViewModelCard model)
        {
            if (model == null) return;
            if (model.State != ViewModelState.Create)
                LogModelEvent(model, JournalEventType.SELECT);
        }

        public static void LogDeleteEvent(ViewModelCard model)
        {
            if (model == null) return;
            if (model.State != ViewModelState.Create)
                LogModelEvent(model, JournalEventType.DELETE);
        }

        public static void LogArchiveEvent(ViewModelCard model)
        {
            if (model == null) return;
            if (model.State != ViewModelState.Create)
                LogModelEvent(model, JournalEventType.ARCHIVE);
        }

        public static void LogRollbackEvent(ViewModelCard model)
        {
            LogRollbackEvent(model, string.Empty);
        }

        public static void LogRollbackEvent(ViewModelCard model, string text)
        {
            if (model == null) return;
            if (model.State != ViewModelState.Create)
                LogModelEvent(model, JournalEventType.ROLLBACK, text);
        }

        public static void LogSaveEvent(ViewModelCard model)
        {
            if (model == null) return;
            LogModelEvent(model, model.State == ViewModelState.Create ? JournalEventType.INSERT : JournalEventType.UPDATE);
        }

        public static void LogChangeStateEvent(ViewModelCard model, string msg)
        {
            if (model == null || model.State == ViewModelState.Create)
                return;

            var header = GetHeader(model);
            var fullMsg = string.IsNullOrEmpty(header) ? msg : header + ", " + msg;
            LogEvent(fullMsg, JournalEventType.CHANGE_STATE, model.ID, model.GetType().Name, null, model.GetModelEntityType());
        }

        /*public static void LogChangeStateEvent(string msg)
        {
            LogEvent(msg, JournalEventType.CHANGE_STATE, null, null);
        }*/
        public static void LogChangeStateEvent(Type oType, string msg, long? docId = null, string addInfo = null)
        {
            LogEvent(msg, JournalEventType.CHANGE_STATE, docId, oType.Name, addInfo, oType);
        }

        /// <summary>
        /// Логирует событие для оъекта выбранного типа. Можно использовать для логирования сторонних сущностей в карточке, использующей другой тип сущности.
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="eventType">Тип события</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="objectName">Наименование объекта (сообщение для соотв. колонки в действиях пользователя)</param>
        public static void LogSingleEvent<T>(JournalEventType eventType, long id, string objectName)
        {
            LogEvent(objectName, eventType,id, typeof(T).Name, null, typeof(T));
        }
    }
}
