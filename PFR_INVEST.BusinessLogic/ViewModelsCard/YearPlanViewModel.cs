﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class YearPlanViewModel : ViewModelCard
    {
        public SIRegister Register { get; set; }
        public SPNOperation Operation { get; set; }
        public Element OperationType { get; set; }
        public Year Year { get; set; }

        public string YearPlanNumber
        {
            get {
                return Register.RegisterNumber != null ? Register.RegisterNumber.Trim() : null;
            }
            set
            {
                Register.RegisterNumber = value;
                OnPropertyChanged("YearPlanNumber");
            }
        }

        private bool _isOperationContentVisibile;
        public bool IsOperationContentVisibile { get {return _isOperationContentVisibile; } set
        {
            _isOperationContentVisibile = value; OnPropertyChanged("IsOperationContentVisibile");
        } }

        #region RegisterKind
        readonly List<string> docTypes = new List<string> 
        { 
            "Постановление правления ПФР", 
            "Распоряжение правления ПФР" 
        };
        public List<string> DocTypes => docTypes;

        public string SelectedDocType
        {
            get
            {
                return Register.RegisterKind;
            }
            set
            {
                Register.RegisterKind = value;
                OnPropertyChanged("SelectedDocType");
            }
        }
        #endregion

        #region Constants
        public string OperationName => Operation != null ? Operation.Name : "(Отсутствует)";

        public string OperationTypeName => OperationType == null ? "(Тип операции отсутствует)": (Operation != null ? Operation.Name : OperationType.Name);

        public string YearName => Year.Name;

        public DateTime? RegisterDate => Register.RegisterDate;

        public string Comment
        {
            get
            {
                return Register.Comment;
            }
            set
            {
                Register.Comment = value;
                OnPropertyChanged("Comment");
            }
        }
        #endregion

        #region Вычисляемые поля
        private decimal m_UKTotalSumm;
        public decimal UKTotalSumm
        {
            get
            {
                return m_UKTotalSumm;
            }
            set
            {
                m_UKTotalSumm = value;
                OnPropertyChanged("UKTotalSumm");
            }
        }

        private decimal m_TotalZLSumm;
        public decimal TotalZLSumm
        {
            get
            {
                return m_TotalZLSumm;
            }
            set
            {
                m_TotalZLSumm = value;
                OnPropertyChanged("TotalZLSumm");
            }
        }

        private decimal m_TotalInvestDohod;
        public decimal TotalInvestDohod
        {
            get
            {
                return m_TotalInvestDohod;
            }
            set
            {
                m_TotalInvestDohod = value;
                OnPropertyChanged("TotalInvestDohod");
            }
        }

        private decimal remainder;
        public decimal Remainder
        {
            get
            {
                return remainder;
            }
            set
            {
                remainder = value;
                OnPropertyChanged("Remainder");
            }
        }
        #endregion

        #region Список планов
        private List<YearPlanListItem> m_UKPaymentPlans;
        public List<YearPlanListItem> UKPaymentPlans
        {
            get
            {
                return m_UKPaymentPlans;
            }
            set
            {
                m_UKPaymentPlans = value;
                OnPropertyChanged("UKPaymentPlans");
            }
        }
        #endregion

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;

            switch (State)
            {
                case ViewModelState.Read:
                case ViewModelState.Create:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<SIRegister, long>(Register);
                    break;
                default:
                    break;
            }
        }

        public YearPlanViewModel(long id, ViewModelState action)
            : base(typeof(SIAssignPaymentsArchiveListViewModel), typeof(VRAssignPaymentsArchiveListViewModel), typeof(SIAssignPaymentsListViewModel), typeof(VRAssignPaymentsListViewModel))
        {
            DataObjectTypeForJournal = typeof(SIRegister);
            State = action;
            ID = id;

            Register = DataContainerFacade.GetByID<SIRegister, long>(ID);
            Operation = Register.GetOperation();
            OperationType = Register.GetOperationType();
            Year = Register.GetCompanyYear();

            RefreshUKPaymentsPlans();
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {

            if(!BLServiceSystem.Client.DeleteYearPlan(ID, false))
            {
                DialogHelper.ShowWarning("Удалить \"Годовой план\" невозможно, так как были выполнены выплаты.",
                    "Операция отменена");
                return;
            }
 
            base.ExecuteDelete(DocOperation.Archive);
        }

        public void RefreshUKPaymentsPlans()
        {
            bool alreadyChanged = IsDataChanged;

            //в этом запросе Сумма, ЗЛ и инвест доход вычисляются правильно, по перечислениям
            UKPaymentPlans = BLServiceSystem.Client.GetUKPaymentPlansForYearPlan(ID);

            UKTotalSumm = (from item in UKPaymentPlans
                           select item.Sum).Sum();

            TotalZLSumm =  (from item in UKPaymentPlans
                           select item.InsuredPersonsCount).Sum();

            TotalInvestDohod = (from item in UKPaymentPlans
                                select item.InvesmentIncome).Sum();

            if (UKPaymentPlans.Any())
                IsOperationContentVisibile = false;
            IsDataChanged = alreadyChanged;
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        private bool Validate()
        {
            return ValidateFields();
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "YearPlanNumber":
                        return YearPlanNumber.ValidateMaxLength(50);
                    case "Comment":
                        return Comment.ValidateMaxLength(1024);
                }
                return null;
            }
        }
    }
}
