﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class LinkedDocumentNpfViewModel:LinkedDocumentBaseViewModel
    {
        public LinkedDocumentNpfViewModel(long id) : this(id, ViewModelState.Edit) { }

        public LinkedDocumentNpfViewModel(long id, ViewModelState action)
            :base(id, action)
        { }

        protected override IList<Element> GetStoragePlacesList()
        {
            // фильтрацию по признаку Visible лучше производить позднее, так как иначе потребуется дополнительный запрос к БД в случае выставленного невидимого пункта
            return BLServiceSystem.Client.GetElementByType(Element.Types.NpfDocumentStoragePlaces).ToList();
        }

		public override bool HasExecutorList => false;

        protected override IList<AttachClassific> GetAttachClassificList()
        {
            return DataContainerFacade.GetList<AttachClassific>();
        }

        protected override Document.Types Type => Document.Types.Npf;

        protected override Type ParentModelType => typeof(DocumentNpfViewModel);
    }
}
