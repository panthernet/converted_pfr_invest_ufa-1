﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F80ViewModel : ViewModelCard
    {
        private IList<F80Deal> infoList = new List<F80Deal>();
        public IList<F80Deal> InfoList
        {
            get { return infoList; }
            set { infoList = value; OnPropertyChanged("InfoList"); }
        }

        private EdoOdkF080 _Document;
        public EdoOdkF080 Document
        {
            get { return _Document; }
            set
            {
                _Document = value;
                OnPropertyChanged("Document");
            }
        }

        private Contract _Contract;
        public Contract Contract
        {
            get { return _Contract; }
            set
            {
                _Contract = value;
                OnPropertyChanged("Contract");
            }
        }

        private LegalEntity _LegalEntity;
        public LegalEntity LegalEntity
        {
            get { return _LegalEntity; }
            set
            {
                _LegalEntity = value;
                OnPropertyChanged("LegalEntity");
            }
        }

        public F80ViewModel(long id)
            : base(typeof(F80ListViewModel))
        {
            DataObjectTypeForJournal = typeof(EdoOdkF080);
            ID = id;
            Document = DataContainerFacade.GetByID<EdoOdkF080, long>(id);
            Contract = Document.GetContract();
            LegalEntity = Contract.GetLegalEntity();
            InfoList = Document.GetDeals();
        }

        public override bool CanExecuteSave() { return false; }

        protected override void ExecuteSave() { }

        public void RefreshLists() { }

        public override string this[string columnName] => "";

        private string Validate()
        {
            return "";
        }
    }
}
