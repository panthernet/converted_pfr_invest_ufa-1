﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class SPNReturnViewModel : RegisterViewModel
    {
		public override bool IsPortfolioVisible => true;

        public override bool IsSumCFRVisible => true;
        public override bool IsSumZLVisible => true;
        public override bool IsCompanyVisible => false;

        public override bool IsCompanyRequired => false;
        public override bool IsERZLVisible => false;
        public override bool IsPayAssVisible => false;

        #region GridColumnSettings
        public override bool IsCFRColumnVisible => true;
        public override bool IsZLColumnEditable => true;
        public override bool IsCountColumnEditable => true;
        public override bool IsCFRCountColumnEditable => true;

        #endregion

		public List<AllocationNPFViewModel> CachedFRModel { get; set; }

		#region Kind
		private static List<string> kinds = new List<string>
		{
			RegisterIdentifier.TempReturn
		};

		public override List<string> Kinds => kinds;

        public override bool IsKindEditable => false;

        #endregion

        public override bool CanExecuteSave()
        {
            //хак для непонятного бага, когда IsDataChanged false при создании
            if (State == ViewModelState.Create && !IsDataChanged)
                IsDataChanged = true;
            return base.CanExecuteSave();
        }

        public SPNReturnViewModel(long recordID, ViewModelState action)
            : base(recordID, action)
        {
			RefreshPortfoliosList();
			Contents = PFRToNPFContents;

			if (action == ViewModelState.Create)
			{
				if (PortfoliosList.Any())
				{
					SelectedPortfolio = PortfoliosList.First();
				}
				CachedFRModel = new List<AllocationNPFViewModel>();
			}
			else
			{
				//var mR = this.register.GetMainRegister();
				
				RefreshFinregWithNPFList();
				if (finregWithNPFList.Any(x => !RegisterIdentifier.FinregisterStatuses.IsCreated(x.Finregister.Status)))
				{
					State = ViewModelState.Read;
				}

				SelectedPortfolio = PortfoliosList.FirstOrDefault(x => x.ID == register.PortfolioID);
			}

			/*if (!ConnectedListViewModels.Contains(typeof(NPFTempAllocationListViewModel)))
			{
				var t = ConnectedListViewModels.ToList();
				t.Add(typeof(NPFTempAllocationListViewModel));
				ConnectedListViewModels = t;
			}*/
        }

		public void RefreshPortfoliosList()
		{
			PortfoliosList = BLServiceSystem.Client.GetSPNAllocatedPortfolios();
			if (State == ViewModelState.Edit && register?.PortfolioID != null && PortfoliosList.All(x => x.ID != register.PortfolioID.Value))
			{
				var p = DataContainerFacade.GetByID<Portfolio>(register.PortfolioID);
				PortfoliosList.Add(p);
			}
		}

		#region Portfolio
		public override bool IsPortfolioEnabled => true;

        public override Portfolio SelectedPortfolio
		{
			get
			{
				return selectedPortfolio;
			}

			set
			{
				selectedPortfolio = value;
				if (selectedPortfolio != null)
					register.PortfolioID = selectedPortfolio.ID;

				OnPropertyChanged("SelectedPortfolio");
			

				//RefreshFinregWithNPFList();
			}
		}

		protected override void AfterPropertyChanged(string propertyName)
		{
			switch (propertyName) 
			{
				case "Content":
				case "SelectedPortfolio":
					if (State == ViewModelState.Create)
					{
						LoadAvailableSPN();
						CloseCachedFinregisters();
					}
					break;
			}
		}

		private void CloseCachedFinregisters()
		{
			if (CachedFRModel != null)
			{
				CachedFRModel.ForEach(x => x.CloseWindow());
				CachedFRModel.Clear();
			}
		}

		public void RegisterOpenedModel(AllocationNPFViewModel frModel)
		{
			if (CachedFRModel != null)
			{
				CachedFRModel.Add(frModel);
			}
		}

		private void LoadAvailableSPN()
		{
			if (State != ViewModelState.Create) return;

			if (SelectedPortfolio != null && Content != null)
			{
				UnsubscribeItems();
				var avList = GetAvailableNPFList();
				FinregWithNPFList = avList.Select(x => new FinregisterWithCorrAndNPFListItem
				{
					CFRCount = x.CFRCount,
					CorrCount = null,
					Count = x.Count,
					Finregister = x,
					ID = 0,
					IsItemChanged = true,
					NPFName = x.NPFName,
					NPFStatus = x.NPFStatus,
					ZLCount = x.ZLCount
				}).ToList();
				SubscribeItems();
				RecalcSums();
				OnPropertyChanged("FinregWithNPFList");
			}
		}
		#endregion

		protected override void LoadFinregFromDB()
		{
			if (register != null)
			{
				var mRec = register.GetMainRegister(); //DataContainerFacade.GetListByProperty<Register>("ReturnID", this.register.ID).FirstOrDefault();
				finregWithNPFList = BLServiceSystem.Client.GetFinregisterWithCorrAndNPFList(mRec.ID);
			}
		}

		protected override void AddMainRecord()
		{
			if (register != null)
			{
				// создание основного реестра для извлечения
				var mRec = register.GetMainRegister(); //DataContainerFacade.GetListByProperty<Register>("ReturnID", this.register.ID).FirstOrDefault();

				if (mRec == null)
				{
					mRec = new Register();
					register.CopyTo(mRec);
					mRec.ID = 0;
				}
				else
				{
					var id = mRec.ID;
					register.CopyTo(mRec);
					mRec.ID = id;
				}
				register.ExtensionData.Remove("MainRegister");

				mRec.Kind = RegisterIdentifier.PFRtoNPF;
				mRec.ReturnID = register.ID;
				mRec.ID = DataContainerFacade.Save(mRec);
				finregWithNPFList.ForEach(x => x.Finregister.RegisterID = mRec.ID);
			}
		}

		protected override void SaveReturnFinregisters()
		{
			if (finregWithNPFList != null && finregWithNPFList.Any())
			{
				/*foreach (var fr in finregWithNPFList)
				{
					if (fr.Finregister is FinregisterListItem)
					{
						var item = new Finregister();
						fr.Finregister.CopyTo(item);
						fr.Finregister.ID = DataContainerFacade.Save(item);
					}
					else
					{
						fr.Finregister.ID = DataContainerFacade.Save(fr.Finregister);
					}
				}*/
                var result = BLServiceSystem.Client.SaveReturnFinregisters(finregWithNPFList);
			    for (int i = 0; i < result.Count; i++)
			        finregWithNPFList[i].Finregister.ID = result[i];
				CachedFRModel.ForEach(x => x.CloseWindow());
			}
		}

		public void RaiseFinregisterChanged()
		{
			RecalcSums();
			OnPropertyChanged("FinregWithNPFList");
		}

		public override void ExecuteDeleteFR()
		{
			if (State == ViewModelState.Create)
			{
				finregWithNPFList.Remove(SelectedFR);
				//OnPropertyChanged("FinregWithNPFList");
				RaiseFinregisterChanged();
			}
			else
			{
				base.ExecuteDeleteFR();
			}
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			if (ID > 0)
			{
				var mRec = register.GetMainRegister(); //DataContainerFacade.GetListByProperty<Register>("ReturnID", this.register.ID).FirstOrDefault();
				var frNPFList = BLServiceSystem.Client.GetFinregisterWithCorrAndNPFList(mRec.ID);
				if (frNPFList.Any(x => !RegisterIdentifier.FinregisterStatuses.IsCreated(x.Finregister.Status)))
				{
					State = ViewModelState.Read;
					DialogHelper.ShowAlert("Не все финреестры находятся в начальном статусе");
					return false;
				}
			}

		    if (SelectedPortfolio == null)
		    {
                DialogHelper.ShowError("Сохранение реестра с типом операции 'Изъятие СПН из временного размещение' невозможно с пустым значением в поле 'Портфель'. \nВыберите портфель и повторите сохранение.");
		        return false;
		    }

			var avNPFList = GetAvailableNPFList(false);

			if ((avNPFList == null || avNPFList.Count == 0) && finregWithNPFList != null && finregWithNPFList.Count > 0)
			{
				DialogHelper.ShowAlert("Нет средств, доступных для изъятия в выбранном портфеле");
				return false;
			}

			foreach (var fr in finregWithNPFList)
			{
				var gr = avNPFList.FirstOrDefault(x => x.CrAccID == fr.Finregister.CrAccID && x.DbtAccID == fr.Finregister.DbtAccID);
				if (gr == null)
				{
					DialogHelper.ShowAlert("Не удается добавить или отредактировать НПФ \"{0}\". Возможно, для изъятия недостаточно средств.", fr.NPFName);
					return false;
				}

				if (gr.Count < fr.Count)
				{
					DialogHelper.ShowAlert("Недостаточно средств находится в размещении НПФ \"{0}\" по выбранному портфелю.", gr.NPFName);
					return false;
				}

				if (gr.ZLCount < fr.ZLCount)
				{
					DialogHelper.ShowAlert("Недостаточное количество ЗЛ находится в размещении НПФ \"{0}\" по выбранному портфелю.", gr.NPFName);
					return false;
				}
			}

			return base.BeforeExecuteSaveCheck();
		}
	}
}
