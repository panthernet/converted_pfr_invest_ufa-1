﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class DepClaimOfferViewModel : ViewModelCard, IRefreshableCardViewModel, IDepClaimOfferProviderEx, IUpdateRaisingModel
    {
        private OfferPfrBankAccountSum _activeOfferPfrBankAccountSum;
        private ObservableCollection<OfferPfrBankAccountSum> _offerPfrBankAccountSumList;
        private string _messageBySummary;

        private const string MessageBySumSuccess = "";
        //"Итоговая сумма совпадает с суммой депозита по договору банковского депозита";
        private const string MessageBySumWarning =
            "Итоговая сумма не совпадает с суммой депозита по договору банковского депозита";

        public DepClaimOffer Offer { get; private set; }
        public DepClaim2 DepClaim { get; private set; }
        public DepClaimSelectParams Auction { get; private set; }
        public LegalEntity Bank { get; private set; }
        public DepClaimOffer.Statuses OriginalStatus { get; private set; }

        public DepClaimOfferViewModel(long depClaimOfferID, ViewModelState state)
            : base(typeof(DepClaimSelectParamsListViewModel),
                    typeof(DepClaimSelectParamsViewModel),
                    typeof(DepClaimSelectParamsViewModel))
        {
            DataObjectTypeForJournal = typeof(DepClaimOffer);
            State = state;
            ID = depClaimOfferID;

            Load();
            InitialOfferNumber = OfferNumber;

            AddSumForPortfolio = new DelegateCommand(ShowAddSumForPortfolioDialog);
            RemoveSumForPortfolio = new DelegateCommand(ShowRemoveSumForPortfolioDialog);
            CheckSumByPortfolioAndAmount();
        }

        protected override void PostOpenInternal()
        {
            base.PostOpenInternal();

            UpdateState();
        }

        private void UpdateState()
        {
			//Если уже есть депозит по оферте, редактирование не доступно
            if (State == ViewModelState.Edit
				&& (Offer.IsDepositsGenerated || Auction.AuctionStatus == DepClaimSelectParams.Statuses.DepositSettled))
            {
                State = ViewModelState.Read;

                OnPropertyChanged("RemoveSumForPortfolioEnabled");
                OnPropertyChanged("AddSumForPortfolioEnabled");
                OnPropertyChanged("IsReadOnly");
                OnPropertyChanged("IsEditable");
            }
        }

        private void Load()
        {
            Offer = DataContainerFacade.GetByID<DepClaimOffer>(ID);
            OriginalStatus = Offer.Status;
            DepClaim = DataContainerFacade.GetByID<DepClaim2>(Offer.DepClaimID);
            Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(Offer.AuctionID);
            Bank = DataContainerFacade.GetByID<LegalEntity>(Offer.BankID);

            AllowedStatuses = new ObservableList<DepClaimOffer.Statuses>();
            UpdateAllowedStatuses();
            OfferPfrBankAccountSumList = new ObservableCollection<OfferPfrBankAccountSum>(BLServiceSystem.Client.GetOfferPfrBankAccountSumForOffer(ID));
        }

        private void ShowRemoveSumForPortfolioDialog(object o)
        {
            if (DialogHelper.ShowConfirmation("Вы уверены?"))
            {
                JournalLogger.LogModelEvent(this, JournalEventType.DELETE, "Удалить сумму по портфелю");
                ActiveOfferPfrBankAccountSum.StatusID = -1;
                OnPropertyChanged("OfferPfrBankAccountSumList");
                CheckSumByPortfolioAndAmount();
            }
        }

        private void CheckSumByPortfolioAndAmount()
        {
            MessageBySummary = OfferPfrBankAccountSumList.Sum(item => item.Sum) == Amount ?
            MessageBySumSuccess : MessageBySumWarning;
        }

        private void ShowAddSumForPortfolioDialog(object o)
        {
            OfferPfrBankAccountSum result;
            if (DialogHelper.ShowAddSumForPortfolio(Offer.ID, Amount - OfferPfrBankAccountSumList.Sum(item => item.Sum), out result))
            {
                _offerPfrBankAccountSumList.Add(result);
                OnPropertyChanged("OfferPfrBankAccountSumList");
                CheckSumByPortfolioAndAmount();
            }
        }

        public void EditSum(OfferPfrBankAccountSum x)
        {
            if (DialogHelper.ShowEditSumForPortfolio(x))
            {
                OnPropertyChanged("OfferPfrBankAccountSumList");
                CheckSumByPortfolioAndAmount();
            }
        }

        private void UpdateAllowedStatuses()
        {
            AllowedStatuses.Clear();

            AllowedStatuses.Add(Offer.Status);
            switch (Offer.Status)
            {
                case DepClaimOffer.Statuses.NotSigned:
                    AllowedStatuses.Add(DepClaimOffer.Statuses.Signed);
                    break;
                case DepClaimOffer.Statuses.Signed:
                    AllowedStatuses.Add(DepClaimOffer.Statuses.Accepted);
                    AllowedStatuses.Add(DepClaimOffer.Statuses.NotAccepted);
                    break;
				case DepClaimOffer.Statuses.Accepted:
					AllowedStatuses.Add(DepClaimOffer.Statuses.Signed);
					AllowedStatuses.Add(DepClaimOffer.Statuses.NotAccepted);
					break;
				case DepClaimOffer.Statuses.NotAccepted:
					AllowedStatuses.Add(DepClaimOffer.Statuses.Signed);
					AllowedStatuses.Add(DepClaimOffer.Statuses.Accepted);
					break;
            }

            OnPropertyChanged("AllowedStatuses");
        }

        public ObservableCollection<OfferPfrBankAccountSum> OfferPfrBankAccountSumList
        {
            get
            {
                return new ObservableCollection<OfferPfrBankAccountSum>(_offerPfrBankAccountSumList.Where(item => item.StatusID > -1));
            }
            set
            {
                _offerPfrBankAccountSumList = value;
                OnPropertyChanged("OfferPfrBankAccountSumList");
            }
        }

        public OfferPfrBankAccountSum ActiveOfferPfrBankAccountSum
        {
            get { return _activeOfferPfrBankAccountSum; }
            set
            {
                _activeOfferPfrBankAccountSum = value;
                OnPropertyChanged("RemoveSumForPortfolioEnabled");
            }
        }

        public DelegateCommand AddSumForPortfolio { get; private set; }

        public DelegateCommand RemoveSumForPortfolio { get; private set; }

        public bool RemoveSumForPortfolioEnabled => IsEditable && ActiveOfferPfrBankAccountSum != null;

        public bool AddSumForPortfolioEnabled => IsEditable;


        #region Fields

        /// <summary>
        /// Дата и время проведения отбора заявок кредитных организаций на заключение договоров банковского депозита
        /// </summary>       
        public DateTime AuctionDate => Auction.SelectDate;

        /// <summary>
        /// Дата и время проведения отбора заявок кредитных организаций на заключение договоров банковского депозита
        /// </summary>       
        public TimeSpan AuctionStartTime => Auction.DepClaimAcceptStart;

        /// <summary>
        /// Дата и время проведения отбора заявок кредитных организаций на заключение договоров банковского депозита
        /// </summary>        
        public TimeSpan AuctionEndTime => Auction.DepClaimAcceptEnd;

        /// <summary>
        /// Срок (дата и время) действия оферты на подписание договора банковского депозита
        /// </summary>        
        public DateTime OfferDate => Auction.SelectDate;

        /// <summary>
        /// Срок (дата и время) действия оферты на подписание договора банковского депозита
        /// </summary>
        public TimeSpan OfferStartTime => Auction.OfferReceiveStart;

        /// <summary>
        /// Срок (дата и время) действия оферты на подписание договора банковского депозита
        /// </summary>
        public TimeSpan OfferEndTime => Auction.OfferReceiveEnd;

        /// <summary>
        /// Наименование отправителя оферты на заключение договора банковского депозита
        /// </summary>        
        public string SenderName => "ПФР";

        /// <summary>
        /// Наименование кредитной организации, являющейся получателем оферты и отправителем акцепта  
        /// </summary>
        public string BankName => Bank.FormalizedNameFull;

        /// <summary>
        /// Полное наименование кредитной организации, являющейся получателем оферты и отправителем акцепта  
        /// </summary>
        public string FullBankName => Bank.FullName;

        /// <summary>
        /// Краткое наименование кредитной организации, являющейся получателем оферты и отправителем акцепта  
        /// </summary>
        public string ShortBankName => Bank.ShortName;

        /// <summary>
        /// № соглашения с ПФР
        /// </summary>
        public string PFRAGRBUM => Bank.PFRAGR;

        /// <summary>
        /// Дата соглашения с ПФР
        /// </summary>
        public DateTime? PFRAGRDATE => Bank.PFRAGRDATE;

        /// <summary>
        /// Реквизиты соглашения, в рамках которого заключается договор банковского депозита
        /// </summary>        
        public string PFRAGR
        {
            get
            {
                if (Bank.PFRAGRDATE == null)
                {
                    return Bank.PFRAGR;
                }
                return string.Format("{0} {1}", Bank.PFRAGR, ((DateTime)Bank.PFRAGRDATE).ToString("dd.MM.yyyy"));
            }
        }

        /// <summary>
        /// Банковский идентификационный код кредитной организации, являющейся получателем оферты и отправителем акцепта 
        /// </summary>
        public string BankBIK => Bank.BIK;

        /// <summary>
        /// Корреспондентский счет кредитной организации, являющейся получателем оферты и отправителем акцепта 
        /// </summary>
        public string BankCorrAcc => (Bank.CorrAcc + " " + Bank.CorrNote).Trim();

        /// <summary>
        /// Дата подписания договора банковского депозита
        /// </summary>
        public DateTime AuctionDate2 => Auction.SelectDate;


        private long InitialOfferNumber { get; set; }
        /// <summary>
        /// Номер договора банковского депозита 
        /// </summary>
        public long OfferNumber 
        { 
            get { return Offer.Number; } 
            set
            {
                if (Offer.Number != value)
                {
                    Offer.Number = value;
                    OnPropertyChanged("OfferNumber");
                    OnPropertyChanged("IsOfferNumberEditable");
                    OnPropertyChanged("IsOfferNumberReadonly");
                }
            }
        }

        public bool IsOfferNumberEditable => OriginalStatus == DepClaimOffer.Statuses.NotSigned;

        public bool IsOfferNumberReadonly => !IsOfferNumberEditable;

        /// <summary>
        /// Сумма депозита по договору банковского депозита, рублей                        
        /// </summary>
        public decimal Amount => DepClaim.Amount;

        /// <summary>
        /// Процентная ставка по договору банковского депозита, % 
        /// </summary>
        public decimal Rate => DepClaim.Rate;

        /// <summary>
        /// Сумма процентов, подлежащих уплате по договору банковского депозита, рублей    
        /// </summary>
        public decimal Payment => DepClaim.Payment;

        /// <summary>
        /// Валюта платежа                    
        /// </summary>
        public string Currency => "Российский рубль";

        /// <summary>
        /// Дата перечисления Пенсионным фондом Российской Федерации на корреспондентский счет кредитной организации суммы депозита по договору банковского  депозита
        /// </summary>
        public DateTime SettleDate => DepClaim.SettleDate;

        /// <summary>
        /// Дата возврата кредитной организацией суммы депозита и начисленных процентов по договору банковского депозита
        /// </summary>
        public DateTime ReturnDate => DepClaim.ReturnDate;

        /// <summary>
        /// Сообщение о итоговое сумме по портфелю
        /// </summary>
        public string MessageBySummary
        {
            get { return _messageBySummary; }
            set
            {
                _messageBySummary = value;
                OnPropertyChanged("MessageBySummary");
            }
        }

        public DepClaimOffer.Statuses SelectedStatus
        {
            get { return Offer.Status; }
            set
            {
                if (AllowedStatuses.Contains(value))
                {
                    Offer.Status = value;
                    IsDataChanged = true;
                }
            }
        }

        public ObservableList<DepClaimOffer.Statuses> AllowedStatuses { get; private set; }

        #endregion

        protected override bool BeforeExecuteSaveCheck()
        {
			var dbOffer = DataContainerFacade.GetByID<DepClaimOffer>(Offer.ID);

			if (dbOffer.IsDepositsGenerated)
			{
				DialogHelper.ShowAlert("Нельзя сохранить изменения в оферте, так как по ней уже был создан депозит.");
				return false;
			}

			var dbAuction = DataContainerFacade.GetByID<DepClaimSelectParams>(Auction.ID);

			if (dbAuction.AuctionStatus == DepClaimSelectParams.Statuses.DepositSettled)
			{
				DialogHelper.ShowAlert("Нельзя сохранить изменения в оферте, так как по аукциону уже были созданы депозиты.");
				return false;
			}

			
			
			if (BLServiceSystem.Client.IsOfferNumberUsed(Auction.SelectDate.Year, ID, OfferNumber))
            {
                DialogHelper.ShowAlert(string.Format("Номер '{0}' уже используется для одной из оферт.", OfferNumber));
                return false;
            }

            if (OriginalStatus == Offer.Status)
                return true;

            var oldS = OriginalStatus.GetDescription();
            var newS = Offer.Status.GetDescription();
            var msg = string.Format(@"Статус оферты был изменён с ""{0}"" на ""{1}"". Вы подтверждаете изменение статуса?", oldS, newS);

            return DialogHelper.ShowConfirmation(msg);
        }

        private bool _isSaveSuccess;

        protected override void ExecuteSave()
        {
            _isSaveSuccess = false;

            switch (Offer.Status)
            {
                case DepClaimOffer.Statuses.NotAccepted:
                    DepClaim.Status = DepClaim2.Statuses.ConfirmNotAccepted;
                    break;
                case DepClaimOffer.Statuses.Accepted:
                    if (OfferPfrBankAccountSumList.Count == 0)
                    {
                        DialogHelper.ShowAlert("Необходимо выбрать портфель");
                        return;
                    }
                    if (OfferPfrBankAccountSumList.Sum(item => item.Sum) != Amount)
                    {
                        DialogHelper.ShowAlert("Итоговая сумма не совпадает с суммой депозита по договору банковского депозита");
                        return;
                    }
                    break;
            }

            DataContainerFacade.Save(DepClaim);
            DataContainerFacade.Save(Offer);
            SaveOfferPfrBankAccountSumList();
            //Обновление статуса аукциона если все оферты семенили статус
            BLServiceSystem.Client.UpdateAuctionStatus(Offer.AuctionID);

            UpdateAllowedStatuses();
            OriginalStatus = Offer.Status;

            _isSaveSuccess = true;
        }

        protected override void OnCardSaved()
        {
            base.OnCardSaved();

            if (_isSaveSuccess)
            {
                UpdateState();
                OnPropertyChanged("IsOfferNumberEditable");
                OnPropertyChanged("IsOfferNumberReadonly");
                IsDataChanged = false;
            }
        }

        private void SaveOfferPfrBankAccountSumList()
        {

            foreach (var x in _offerPfrBankAccountSumList)
            {
                if (x.StatusID == -1)//Deleted
                {
                    if (x.ID != 0)//Don't need to delete new item
                        DataContainerFacade.Delete(x);
                    continue;
                }
                if (x.ID == 0)//New
                {
                    x.ID = DataContainerFacade.Save(x);
                    continue;
                }
                if (x.IsDirty)//Edited
                {
                    x.ID = DataContainerFacade.Save(x);
                }
            }


            OfferPfrBankAccountSumList = new ObservableCollection<OfferPfrBankAccountSum>(
                BLServiceSystem.Client.GetOfferPfrBankAccountSumForOffer(Offer.ID));
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && String.IsNullOrWhiteSpace(Validate());
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName.ToLower())
                {
                    case "offernumber": return (OfferNumber > 9999 && InitialOfferNumber != OfferNumber) || OfferNumber < 1 ? "Допустимо только значение из диапазона 1 - 9999" : null;
                    default: return null;
                }
            }
        }

        private string Validate()
        {
            const string fieldNames = "OfferNumber";

            return fieldNames.Split("|".ToCharArray()).Any(fieldName => !String.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
        }

        void IRefreshableViewModel.RefreshModel()
        {
            Load();
        }

        long? IRefreshableCardViewModel.CardID => ID;

        public long? OfferID => ID;

        public DepClaimSelectParams.Statuses? AuctionStatus => Auction != null ? Auction.AuctionStatus : (DepClaimSelectParams.Statuses?)null;

        public long? AuctionID => Auction != null ? Auction.ID : (long?)null;

        public bool IsReadOnly => State == ViewModelState.Read;

        public bool IsEditable => !IsReadOnly;

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new[] { new KeyValuePair<Type, long>(typeof(DepClaimOffer), ID) };
        }
    }
}
