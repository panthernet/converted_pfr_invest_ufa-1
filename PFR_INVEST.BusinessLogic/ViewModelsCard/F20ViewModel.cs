﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F20ViewModel : ViewModelCard
    {
        private EdoOdkF020 odk;
        public EdoOdkF020 Odk
        {
            get { return odk; }
            set
            {
                odk = value;
                OnPropertyChanged("Odk");
            }
        }

        private Contract contract;
        public Contract Contract
        {
            get { return contract; }
            set
            {
                contract = value;
                OnPropertyChanged("Contract");
            }
        }

        private LegalEntity legalEntity;
        public LegalEntity LegalEntity
        {
            get { return legalEntity; }
            set
            {
                legalEntity = value;
                OnPropertyChanged("LegalEntity");
            }
        }

        public F20ViewModel(long id)
        {
            DataObjectTypeForJournal = typeof(EdoOdkF020);
            if (id > 0)
            {
                ID = id;
                Odk = DataContainerFacade.GetByID<EdoOdkF020, long>(id);
                Contract = Odk.GetContract();
                LegalEntity = Contract.GetLegalEntity();
                RefreshF20List();
            }
        }

        private List<F20ListItem> _F20List = new List<F20ListItem>();
        public List<F20ListItem> F20List
        {
            get { return _F20List; }
            set
            {
                _F20List = value;
                OnPropertyChanged("F20List");
            }
        }

		public string PortfolioName => Odk.Portfolio ?? Contract.PortfolioName;

        public override bool CanExecuteSave() { return false; }

        protected override void ExecuteSave() { }

        public override string this[string columnName] => string.Empty;


        public void RefreshF20List()
        {
            List<F20ListItem> list = new List<F20ListItem>();

            list.Add(new F20ListItem("1. Денежные средства на счетах в кредитных организациях (010):", Odk.Funds1, Odk.Funds2, Odk.Funds3));
            list.Add(new F20ListItem("1.1. в том числе в иностранной валюте (011):", Odk.Fundscur1, Odk.Fundscur2, Odk.Fundscur3));
            list.Add(new F20ListItem("2. Депозиты в рублях в кредитных организациях (020):", Odk.Deprur1, Odk.Deprur2, Odk.Deprur3));
            list.Add(new F20ListItem("3. Государственные ценные бумаги Российской Федерации (030):", Odk.Gcbrf1, Odk.Gcbrf2, Odk.Gcbrf3));
            list.Add(new F20ListItem("3.1. в том числе обязательства по которым выражены в иностранной валюте (031):", Odk.Gcbrfincur1, Odk.Gcbrfincur2, Odk.Gcbrfincur3));
            list.Add(new F20ListItem("4. Государственные ценные бумаги субъектов Российской Федерации (040):", Odk.Gcbsub1, Odk.Gcbsub2, Odk.Gcbsub3));
            list.Add(new F20ListItem("4.1. в том числе обязательства по которым выражены в иностранной валюте (041):", Odk.Gcbsubcur1, Odk.Gcbsubcur2, Odk.Gcbsubcur3));
            list.Add(new F20ListItem("5. Облигации муниципальных образований (050):", Odk.Stockmo1, Odk.Stockmo2, Odk.Stockmo3));
            list.Add(new F20ListItem("6. Облигации российских хозяйственных обществ (060):", Odk.Stockmoho1, Odk.Stockmoho2, Odk.Stockmoho3));
            list.Add(new F20ListItem("6.1. в том числе обязательства по которым выражены в иностранной валюте (061):", Odk.Stockmohoincur1, Odk.Stockmohoincur2, Odk.Stockmohoincur3));
            list.Add(new F20ListItem("7. Акции российских эмитентов, созданных в форме открытых акционерных обществ (070):", Odk.Akciire1, Odk.Akciire2, Odk.Akciire3));
            list.Add(new F20ListItem("8. Паи (акции, доли) индексных инвестиционных фондов, размещающих средства в государственные ценные бумаги иностранных государств, облигации и акции иных иностранных эмитентов (080):", Odk.Pai1, Odk.Pai2, Odk.Pai3));
            list.Add(new F20ListItem("9. Ипотечные ценные бумаги, выпущенные в соответствии с законодательством Российской Федерации об ипотечных ценных бумаг (090):", Odk.Cbipoteka1, Odk.Cbipoteka2, Odk.Cbipoteka3));
            list.Add(new F20ListItem("9.1. в том числе обеспеченные государственной гарантией Российской Федерации (091):", Odk.Cbipotekagrf1, Odk.Cbipotekagrf2, Odk.Cbipotekagrf3));
            list.Add(new F20ListItem("10. Дебиторская задолженность (100):", Odk.DebDolgItogo1, Odk.DebDolgItogo2, Odk.DebDolgItogo3));
            list.Add(new F20ListItem("10.1. в том числе средства, переданные профессиональным участникам рынка ценных бумаг (101):", Odk.Profi1, Odk.Profi2, Odk.Profi3));
            list.Add(new F20ListItem("10.2. в том числе дебиторская задолженность по процентному (купонному) доходу по ценным бумагам (102):", Odk.Procdolg1, Odk.Procdolg2, Odk.Procdolg3));
            list.Add(new F20ListItem("10.3. в том числе прочая дебиторская задолженность (103):", Odk.Otherdolg1, Odk.Otherdolg2, Odk.Otherdolg3));
            list.Add(new F20ListItem("11. Прочие активы (110):", Odk.Otheractiv1, Odk.Otheractiv2, Odk.Otheractiv3));
            list.Add(new F20ListItem("ИТОГО рыночная стоимость инвестиционного портфеля: (строки 010 + 020 + 030 + 040 + 050 + 060 + 070 + 080 + 090 + 100 + 110) (120):", Odk.Sumactiv1, null/*Odk.Sumactiv2*/, Odk.Sumactiv3));

            F20List = list;

        }

    }
}
