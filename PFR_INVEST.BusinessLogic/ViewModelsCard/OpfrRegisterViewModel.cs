﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    [ModelEntity(Type = typeof(OpfrRegister))]
    public class OpfrRegisterViewModel: ViewModelCard, IUpdateRaisingModel, IUpdateListenerModel
    {
        private readonly OpfrRegister _register;
        private bool _isNotNew;
        private OpfrTransferListItem _transfer;
        private string _portfolioName;

        public List<Month> MonthList { get; set; }
        public List<Year> YearList { get; set; }
        public List<Element> DocKindList { get; set; }
        public List<Element> RegTypeList { get; set; }

        public string PortfolioName => _portfolioName;

        public ICommand DeleteTransferCommand { get; set; }
        public ICommand SelectPortfolioCommand { get; set; }

        public BindingList<OpfrTransferListItem> TransfersList { get; set; }

        public OpfrTransferListItem Transfer
        {
            get { return _transfer; }
            set { _transfer = value; OnPropertyChangedNoData("Transfer");}
        }

        public bool IsNotNew
        {
            get { return _isNotNew; }
            set { _isNotNew = value; OnPropertyChangedNoData("IsNotNew"); }
        }

        public long YearID
        {
            get { return _register.YearID; }
            set { _register.YearID = value; OnPropertyChanged("YearID"); }
        }

        public long MonthID
        {
            get { return _register.MonthID; }
            set { _register.MonthID = value; OnPropertyChanged("MonthID"); }
        }

        public long DocKindID
        {
            get { return _register.KindID; }
            set { _register.KindID = value; OnPropertyChanged("DocKindID"); }
        }

        public long RegTypeID
        {
            get { return _register.RegTypeID; }
            set { _register.RegTypeID = value; OnPropertyChanged("RegTypeID"); }
        }

        public DateTime? Date
        {
            get { return _register.RegDate; }
            set { _register.RegDate = value; OnPropertyChanged("Date"); }
        }

        public string RegNum
        {
            get { return _register.RegNum; }
            set { _register.RegNum = value; OnPropertyChanged("RegNum"); }
        }

        public string Comment
        {
            get { return _register.Comment; }
            set { _register.Comment = value; OnPropertyChanged("Comment"); }
        }

        public long? PortfolioID
        {
            get { return _register.ImportPortfolioID; }
            set { _register.ImportPortfolioID = value; OnPropertyChanged("PortfolioID"); }
        }

        public OpfrRegisterViewModel(long? id = null)
        {
            MonthList = DataContainerFacade.GetList<Month>().OrderBy(a => a.ID).ToList();
            YearList = DataContainerFacade.GetList<Year>().OrderBy(a => a.ID).ToList();
            DocKindList = DataContainerFacade.GetListByProperty<Element>("Key", (long)Element.Types.OpfrDirection, true).OrderBy(a => a.Name).ToList();
            RegTypeList = DataContainerFacade.GetListByProperty<Element>("Key", (long)Element.Types.OpfrRegType, true).OrderBy(a => a.Name).ToList();

            DeleteTransferCommand = new DelegateCommand(a=> Transfer != null && EditAccessAllowed, DeleteTransferExecute);
            SelectPortfolioCommand = new DelegateCommand(a=> EditAccessAllowed, SelectPfExecute);

            _register = id == null ? new OpfrRegister() : DataContainerFacade.GetByID<OpfrRegister>(id);
            ID = _register.ID;
            if (id == null)
            {
                Date = DateTime.Now.Date;
                RegTypeID = (long)Element.SpecialDictionaryItems.OpfrRegTypeRasp;
                DocKindID = (long)Element.SpecialDictionaryItems.OpfrDirectionFromPfr;
                MonthID = Date.Value.Month;
                YearID = Date.Value.Year - 2000;
                IsNotNew = false;
                IsDataChanged = true;
                TransfersList = new BindingList<OpfrTransferListItem>();
            }
            else
            {
                IsNotNew = true;
                if (_register.ImportPortfolioID.HasValue)
                {
                    var pf = DataContainerFacade.GetByID<Portfolio>(_register.ImportPortfolioID.Value);
                    if(pf != null)
                        _portfolioName = pf.Name;
                } 
                TransfersList = new BindingList<OpfrTransferListItem>(BLServiceSystem.Client.GetOpfrTransferListItems(_register.ID).ToList());
            }
        }

        private void SelectPfExecute(object obj)
        {
            var pf = DialogHelper.SelectPortfolio(true, Portfolio.Types.DSV, Portfolio.Types.ROPS, Portfolio.Types.SPN,
                Portfolio.Types.NoType);
            if (pf == null)
            {
                _portfolioName = null;
                PortfolioID = null;
            }
            else
            {
                _portfolioName = pf.PFName;
                PortfolioID = pf.PortfolioID;
            }
            OnPropertyChanged("PortfolioName");
        }

        private void DeleteTransferExecute(object obj)
        {
            if(!OpfrTransferViewModel.CanDeleteTransfer(Transfer))
            {
                DialogHelper.ShowAlert("Невозможно удалить перечисление, т.к. оно находится в статусе отличном от начального!");
                return;
            }

            if (DialogHelper.ShowConfirmation("Удалить выбранное перечисление?") != true) return;

            var tId = Transfer.ID;
            DataContainerFacade.Delete<OpfrTransfer>(Transfer.ID);
            TransfersList.Remove(Transfer);
            ViewModelManager.UpdateDataInAllModels(GetUpdatedTransfersList(tId));
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "YearID":
                        return _register.YearID == 0 ? "Значение не задано!" : null;
                    case "DocKindID":
                        return _register.KindID == 0 ? "Значение не задано!" : null;
                    case "MonthID":
                        return _register.MonthID == 0 ? "Значение не задано!" : null;
                    case "RegTypeID":
                        return _register.RegTypeID == 0 ? "Значение не задано!" : null;
                    case "Comment":
                        return _register.Comment.ValidateMaxLength(2500);
                    case "RegNum":
                        return _register.RegNum.ValidateMaxLength(50);
                }
                return null;
            }
        }

        private string Validate()
        {
            const string values = "YearID|DocKindID|MonthID|RegTypeID|Comment|RegNum";
            if (_register == null) return "Ошибка";
            return values.Split('|').Any(fld => !string.IsNullOrEmpty(this[fld])) ? "Неверный формат данных" : null;
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Create:
                case ViewModelState.Edit:
                    ID = _register.ID = DataContainerFacade.Save(_register);
                    //TODO пока пропускаем работу с портфелем до апдейта постановки
                    if (TransfersList.Count > 0 && false)
                    {
                        BLServiceSystem.Client.UpdateOpfrTransfersPortfolio(TransfersList.Select(a => a.ID).ToArray(),
                            _register.ImportPortfolioID);
                    }
                    IsNotNew = true;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && (State == ViewModelState.Create || IsDataChanged);
        }

        public override bool CanExecuteDelete()
        {
            return TransfersList.All(OpfrTransferViewModel.CanDeleteTransfer);
        }

        protected override void ExecuteDelete(int delType = 1)
        {
           // if(!DialogHelper.ShowConfirmation("Удалить распоряжение вместе с перечислениями?")) return;
            TransfersList.ForEach(a=> DataContainerFacade.Delete<OpfrTransfer>(a.ID));
            DataContainerFacade.Delete(_register);
            base.ExecuteDelete(DocOperation.Archive);
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(OpfrRegister), _register.ID) };
        }

        private IList<KeyValuePair<Type, long>> GetUpdatedTransfersList(long id)
        {
            return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(OpfrTransfer), id) };
        }

        public Type[] UpdateListenTypes => new[] {typeof(OpfrTransfer)};

        public void OnDataUpdate(Type type, long id)
        {
            //пропускаем, если сущность еще не сохранена
            if(_register.ID == 0 || State == ViewModelState.Create) return; 
            //обновляем весь список разом
            TransfersList = new BindingList<OpfrTransferListItem>(BLServiceSystem.Client.GetOpfrTransferListItems(_register.ID).ToList());
            OnPropertyChangedNoData("TransfersList");
        }
    }
}
