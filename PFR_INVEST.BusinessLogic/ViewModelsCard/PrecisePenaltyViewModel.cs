﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class PrecisePenaltyViewModel : ViewModelCard, IUpdateRaisingModel
    {
        #region Fields
        private readonly PenaltyViewModel _dueVM;

        private DopSPN _mPenaltyAcc;
        public DopSPN PenaltyAcc
        {
            get { return _mPenaltyAcc; }
            set
            {
                if (_mPenaltyAcc == value) return;
                _mPenaltyAcc = value;
                OnPropertyChanged("PenaltyAcc");
            }
        }


        public decimal MSumm
        {
            get { return PenaltyAcc.MSumm ?? 0; }
            set
            {
                PenaltyAcc.MSumm = value;
                OnPropertyChanged("ChSumm");
                OnPropertyChanged("MSumm");
            }
        }


        private IList<Month> _monthsList = new List<Month>();
        public IList<Month> MonthsList
        {
            get { return _monthsList; }
            set
            {
                _monthsList = value;
                OnPropertyChanged("MonthsList");
            }
        }


        public long? SelectedMonthID
        {
            get { return PenaltyAcc.PeriodID; }
            set
            {
                PenaltyAcc.PeriodID = value;
                OnPropertyChanged("SelectedMonthID");
            }
        }

        #region ChSumm
        public decimal ChSumm
        {
            get
            {
                if (_dueVM?.Penalty.Summ != null && PenaltyAcc.MSumm.HasValue)
                {
                    if (_dueVM.DopSPNs.Count <= 1) return PenaltyAcc.ChSumm ?? 0;

                    var lastAccSumm = _dueVM.DopSPNs.Where(d => d.ID != PenaltyAcc.ID && d.Date <= PenaltyAcc.Date)
                        .OrderByDescending(d => d.Date)
                        .Select(d => d.MSumm).FirstOrDefault();
                    //Корректируем сумму любого кроме первого уточнения
                    if (lastAccSumm.HasValue)
                        PenaltyAcc.ChSumm = MSumm - lastAccSumm.Value;

                    return PenaltyAcc.ChSumm ?? 0;
                }
                return 0;
            }
            set
            {
                PenaltyAcc.ChSumm = value;
                OnPropertyChanged("ChSumm");
            }
        }
        #endregion

        #region Account
        public ICommand SelectAccount { get; private set; }

        private PortfolioFullListItem _account;
        public PortfolioFullListItem Account
        {
            get { return _account; }
            set
            {
                _account = value;
                if (value != null)
                {
                    _account = value;
                    PenaltyAcc.PortfolioID = value.Portfolio.ID;
                    PenaltyAcc.LegalEntityID = value.BankLE.ID;

                    OnPropertyChanged("AccountNumber");
                    OnPropertyChanged("Bank");
                    OnPropertyChanged("PFName");
                }

                OnPropertyChanged("Account");
            }
        }
        #endregion

        public bool ReadAccessOnly => IsReadOnly;

        public bool PeriodEnabled => !IsReadOnly;

        #endregion

        #region Execute implementation
        private void ExecuteSelectAccount()
        {
            var account = DialogHelper.SelectPortfolio();
            if (account != null)
                Account = account;
        }


        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    PenaltyAcc.AddSpnID = _dueVM.Penalty.ID;
                    ID = PenaltyAcc.ID = DataContainerFacade.Save(PenaltyAcc);
                    break;
            }
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(PenaltyAcc);
            base.ExecuteDelete(delType);
        }

        public override bool CanExecuteSave()
        {
			return false;//Пенни могут появлятся в системе только через ДК, и никак иначе
            //return String.IsNullOrEmpty(Validate()) && this.IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
			return false; //Пенни могут появлятся в системе только через ДК, и никак иначе
            //return State == ViewModelState.Edit;
        }

        #endregion

        #region Validation

        private void RegisterValidation()
        {
            PenaltyAcc.PropertyChanged += (sender, e) => { IsDataChanged = true; };
            PenaltyAcc.OnValidate += Field_Validate;
        }

        private void Field_Validate(object sender, BaseDataObject.ValidateEventArgs e)
        {
            DopSPN item = (sender as DopSPN);
            if (item != null)
            {
                string error = "Не заполнены все обязательные поля!";
                switch (e.PropertyName)
                {
                    case "OperationDate": e.Error = item.OperationDate.HasValue ? null : error; break;
                    case "Date": e.Error = item.Date.HasValue ? null : error; break;
                    case "RegNum": e.Error = string.IsNullOrWhiteSpace(item.RegNum) ? error : null; break;
                    case "MSumm": e.Error = item.MSumm.HasValue ? null : error; break;
                    case "ChSumm": e.Error = item.ChSumm.HasValue ? null : error; break;

                }
            }
        }

        public override string this[string columnName]
        {
            get
            {
                BaseDataObject.ValidateEventArgs e = new BaseDataObject.ValidateEventArgs(columnName);
                Field_Validate(PenaltyAcc, e);
                if (!string.IsNullOrEmpty(e.Error))
                    return e.Error;
                return null;
            }
        }

        private string Validate()
        {
            const string errorMessage = "Не заполнены все обязательные поля!";
            const string validaetFields = "OperationDate|Date|RegNum|MSumm|ChSumm";
            if (Account == null)
                return errorMessage;

            foreach (string key in validaetFields.Split('|'))
            {
                BaseDataObject.ValidateEventArgs e = new BaseDataObject.ValidateEventArgs(key);
                Field_Validate(PenaltyAcc, e);
                if (!string.IsNullOrEmpty(e.Error))
                {
                    return e.Error;
                }
            }
            return null;
        }
        #endregion

        public PrecisePenaltyViewModel(PenaltyViewModel _dueVM)
            : this()
        {
            if (_dueVM == null)
                throw new ArgumentNullException("PenaltyViewModel");
            this._dueVM = _dueVM;
            PenaltyAcc = new DopSPN {OperationDate = DateTime.Now, Date = DateTime.Now, DocKind = 2};

            Account = this._dueVM.Account;
            RegisterValidation();
            ID = PenaltyAcc.ID;
			
        }

        public PrecisePenaltyViewModel(long dopSPNID)
            : this()
        {
            ID = dopSPNID;
            PenaltyAcc = DataContainerFacade.GetByID<DopSPN>(dopSPNID);
            _dueVM = new PenaltyViewModel();
            if (PenaltyAcc != null)
                _dueVM.Load(PenaltyAcc.AddSpnID ?? 0);


            //Берём локальный счёт из уточнения
            if (PenaltyAcc.PortfolioID.HasValue && PenaltyAcc.QuarterPfrBankAccountID.HasValue)
                Account = BLServiceSystem.Client.GetPortfolioFull(PenaltyAcc.QuarterPfrBankAccountID.Value, PenaltyAcc.PortfolioID.Value);
            //Если нет локального - то из поручения
            if(Account == null)
                Account = _dueVM.Account;

            RegisterValidation();
			
        }

		public override bool IsReadOnly => true;

        private PrecisePenaltyViewModel()
            //: base(typeof(DueListViewModel))
        {
            DataObjectTypeForJournal = typeof(DopSPN);
            MonthsList = DataContainerFacade.GetList<Month>();
           SelectAccount = new DelegateCommand(o=>! ReadAccessOnly,o => ExecuteSelectAccount());

            ReInitMaxMinDatesAndLastSum();
        }

        private void ReInitMaxMinDatesAndLastSum()
        {

        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (DopSPN), PenaltyAcc.ID)};
            return list;
        }
    }
}
