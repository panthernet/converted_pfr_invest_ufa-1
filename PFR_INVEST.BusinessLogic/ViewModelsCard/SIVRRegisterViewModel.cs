﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.SIReport;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class SIVRRegisterViewModel : ViewModelCard, IUpdateListenerModel
    {
        public SIRegister TransferRegister { get; set; }

        #region DocTypes

        public List<string> DocTypes { get; set; }

        public string SelectedDocType
        {
            get
            {
                return TransferRegister.RegisterKind;
            }
            set
            {
                TransferRegister.RegisterKind = value;
                OnPropertyChanged("SelectedDocType");
            }
        }

        #endregion

        public bool IsRegisterNotImported => !SiRegisterTransferData.IMPORT_FLAG.Equals(TransferRegister.Comment);

        public bool IsOperationsEnabled => TransferRegister.OperationID != (long)SIRegister.Operations.DeadZL
                                           && TransferRegister.OperationID != (long)SIRegister.Operations.WithdrawNCTP
                                           && TransferRegister.OperationID != (long)SIRegister.Operations.WithdrawSPV
                                           && IsRegisterNotImported
                                           && !IsReadOnly;


        #region Directions

        private List<SPNDirection> _directions;

        public List<SPNDirection> Directions
        {
            get
            {
                return _directions;
            }
            set
            {
                _directions = value;
                if (value != null) SelectedDirection = value.FirstOrDefault();
                OnPropertyChanged("Directions");
            }
        }

        private SPNDirection _mSelectedDirection;

        public SPNDirection SelectedDirection
        {
            get
            {
                return _mSelectedDirection;
            }
            set
            {
                if (_mSelectedDirection == value)
                    return;

                _mSelectedDirection = value;
                if (value != null)
                {
                    TransferRegister.DirectionID = value.ID;
                    var saveOp = SelectedOperation;
                    if (value.isFromUKToPFR != null) Operations = value.isFromUKToPFR.Value ? _uKtoPFROperations : _pfRtoUKOperations;

                    if (saveOp != null)
                        SelectedOperation = Operations.FirstOrDefault(o => o.ID == saveOp.ID);
                }
                OnPropertyChanged("SelectedDirection");
            }
        }

        #endregion

        #region Operations

        private readonly List<SPNOperation> _uKtoPFROperations;

        private readonly List<SPNOperation> _pfRtoUKOperations;

        private List<SPNOperation> _mOperations;

        public List<SPNOperation> Operations
        {
            get
            {
                return _mOperations;
            }
            set
            {
                _mOperations = value;
                //if (value != null) SelectedOperation = Operations.First();
                OnPropertyChanged("Operations");
            }
        }

        private SPNOperation _mSelectedOperation;

        public SPNOperation SelectedOperation
        {
            get
            {
                return _mSelectedOperation;
            }
            set
            {
                if (_mSelectedOperation == value)
                    return;

                _mSelectedOperation = value;
                if (value != null) TransferRegister.OperationID = value.ID;
                UpdatePaymentAssignment();
                OnPropertyChanged("SelectedOperation");
            }
        }

        #endregion

        #region YearsList

        public List<Year> YearsList { get; set; }

        private Year _mSelectedYear;

        public Year SelectedYear
        {
            get
            {
                return _mSelectedYear;
            }

            set
            {
                _mSelectedYear = value;
                if (value != null) TransferRegister.CompanyYearID = value.ID;
                OnPropertyChanged("SelectedYear");
            }
        }

        #endregion

        #region MonthsList

        public List<Month> MonthsList { get; set; }

        private Month _mSelectedMonth;

        public Month SelectedMonth
        {
            get
            {
                return _mSelectedMonth;
            }
            set
            {
                _mSelectedMonth = value;
                if (value != null && value.ID > 0) TransferRegister.CompanyMonthID = value.ID;
                else TransferRegister.CompanyMonthID = null;
                OnPropertyChanged("SelectedMonth");
            }
        }

        #endregion

        #region RegisterNumber

        public string RegisterNumber
        {
            get
            {
                if (TransferRegister.RegisterNumber != null) return TransferRegister.RegisterNumber.Trim();
                return null;
            }
            set
            {
                TransferRegister.RegisterNumber = value;
                OnPropertyChanged("RegisterNumber");
            }
        }

        public DateTime? RegisterDate
        {
            get
            {
                return TransferRegister.RegisterDate;
            }
            set
            {
                if (TransferRegister.RegisterDate == value)
                    return;

                TransferRegister.RegisterDate = value;
                OnPropertyChanged("RegisterDate");


            }
        }

        #endregion

        #region Comment

        public string Comment
        {
            get
            {
                return TransferRegister.Comment;
            }
            set
            {
                TransferRegister.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        #endregion

        #region UKTotalSumm

        private Decimal _ukTotalSumm;

        public Decimal UKTotalSumm
        {
            get
            {
                _ukTotalSumm = RequestsGrid.Sum(tr => tr.Sum);
                return _ukTotalSumm;
            }
            set
            {

                _ukTotalSumm = value;
                OnPropertyChanged("UKTotalSumm");
            }
        }

        #endregion

        #region TotalInvestDohod

        private Decimal _totalInvestDohod;

        public Decimal TotalInvestDohod
        {
            get
            {
                return _totalInvestDohod;
            }
            set
            {
                _totalInvestDohod = value;
                OnPropertyChanged("TotalInvestDohod");
            }
        }

        #endregion

        #region TotalZLSumm

        private Decimal _totalZLSumm;

        public Decimal TotalZLSumm
        {
            get
            {
                return _totalZLSumm;
            }
            set
            {
                _totalZLSumm = value;
                OnPropertyChanged("TotalZLSumm");
            }
        }

        #endregion

        #region RequestsGrid

        private bool _mRegisterHasInitialSateTransfers;

        public bool RegisterHasInitialSateTransfers => _mRegisterHasInitialSateTransfers;

        private List<ModelTransferItem> _requestsGrid = new List<ModelTransferItem>();

        public List<ModelTransferItem> RequestsGrid
        {
            get
            {
                return _requestsGrid;
            }
            set
            {
                _requestsGrid = value;
                OnPropertyChanged("RequestsGrid");
                _mRegisterHasInitialSateTransfers =
                    RequestsGrid.Count(tr => TransferStatusIdentifier.IsStatusInitialState(tr.TransferStatus))
                    > 0;
                OnPropertyChanged("UKTotalSumm");
            }
        }

        #endregion
        #region Pay Assignment
        /// <summary>
        /// Назначение платежа
        /// </summary>
        public string PayAssignment
        {
            get
            {
                return TransferRegister.PayAssignment;
            }
            set
            {
                if (TransferRegister.PayAssignment == value)
                    return;

                TransferRegister.PayAssignment = value;
                OnPropertyChanged("PayAssignment");
            }
        }

        /// <summary>
        /// Видимость назначения платежа (override в дочерних моделях)
        /// </summary>
        public virtual bool IsPayAssVisible => true;

        private static List<string> _PFRToNPFContents;
        protected static List<string> PFRToNPFContents
        {
            get
            {
                if (_PFRToNPFContents == null)
                {
                    var contentList = BLServiceSystem.Client.GetElementByType(Element.Types.RegisterPFRtoNPF);
                    _PFRToNPFContents = contentList.Where(x => x.Visible).Select(e => e.Name).ToList();
                }
                return _PFRToNPFContents;
            }
        }

        private static List<string> _NPFToPFRContents;
        protected static List<string> NPFToPFRContents
        {
            get
            {
                if (_NPFToPFRContents == null)
                {
                    var contentList = BLServiceSystem.Client.GetElementByType(Element.Types.RegisterNPFtoPFR);
                    _NPFToPFRContents = contentList.Where(x => x.Visible).Select(e => e.Name).ToList();
                }
                return _NPFToPFRContents;
            }
        }
        #endregion
        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {
            var list = BLServiceSystem.Client.GetSITransfersByRegister(TransferRegister.ID);

            if (!IsAllowRemove(list) || !DialogHelper.ConfirmSIRegisterDeleting())
            {
                Deleting = false;
                return;
            }

            //оптимизация http://jira.vs.it.ru/browse/DOKIPIV-473
            BLServiceSystem.Client.DeleteSIVRRegister(list);

            base.ExecuteDelete();
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    ID = DataContainerFacade.Save<SIRegister, long>(TransferRegister);
                    break;
                case ViewModelState.Create:
                //Реестры создаются только через мастер
                default:
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && Validate();
        }

        private Document.Types _contractType;
        public SIVRRegisterViewModel(Document.Types contractType, long id, ViewModelState action)
            : base(typeof(TransfersSIListViewModel), typeof(TransfersSIArchListViewModel), typeof(SPNMovementsSIListViewModel), typeof(ZLMovementsSIListViewModel), typeof(TransfersVRListViewModel), typeof(TransfersVRArchListViewModel), typeof(SPNMovementsVRListViewModel), typeof(ZLMovementsVRListViewModel))
        {
            DataObjectTypeForJournal = typeof(SIRegister);
            DocTypes = new List<string>(new[] { "Постановление правления ПФР", "Распоряжение правления ПФР" });
            OnPropertyChanged("DocTypes");
            YearsList = DataContainerFacade.GetList<Year>();
            OnPropertyChanged("YearsList");
            MonthsList = DataContainerFacade.GetList<Month>();
            //MonthsList.Insert(0, new Month() { Name = TransferRegisterCompanyMonthIdentifier.NoMonth });
            OnPropertyChanged("MonthsList");


            _directions = DataContainerFacade.GetList<SPNDirection>();

            _contractType = contractType;

            if (contractType == Document.Types.SI)
            {
                _directions = _directions.Where(trd => trd.ID == 1 || trd.ID == 2).ToList();
                _uKtoPFROperations = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(1);
                _pfRtoUKOperations = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(2);

            }
            else if (contractType == Document.Types.VR)
            {
                _directions = _directions.Where(trd => trd.ID == 3 || trd.ID == 4).ToList();
                _uKtoPFROperations = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(3);
                _pfRtoUKOperations = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(4);
            }
            //http://jira.vs.it.ru/browse/DOKIPIV-370?focusedCommentId=241765&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-241765
            _uKtoPFROperations = _uKtoPFROperations.Where(o => o.ID != (long)SIRegister.Operations.WithdrawNCTP
                                                            && o.ID != (long)SIRegister.Operations.WithdrawSPV
                                                            && o.ID != (long)SIRegister.Operations.DeadZL)
                                                            .OrderBy(o => o.Name).ToList();
            _pfRtoUKOperations = _pfRtoUKOperations.Where(o => o.ID != (long)SIRegister.Operations.WithdrawNCTP
                                                            && o.ID != (long)SIRegister.Operations.WithdrawSPV
                                                            && o.ID != (long)SIRegister.Operations.DeadZL).OrderBy(o => o.Name).ToList();




            //UKtoPFROperations = BLServiceSystem.Client.GetSPNOperationsUKtoPFRListHib();
            //PFRtoUKOperations = BLServiceSystem.Client.GetSPNOperationsPFRtoUKListHib();


            if (id <= 0)
            {
                //Create
                TransferRegister = new SIRegister { RegisterDate = DateTime.Today };
                RegisterDate = DateTime.Now;
                SelectedDirection = Directions.First();
                SelectedDocType = DocTypes.First();
                SelectedYear = YearsList.First(y => y.Name == DateTime.Now.Year.ToString());
                SelectedMonth = MonthsList.First(m => m.ID == DateTime.Now.Month);
                IsDataChanged = true;


            }
            else
            {
                ID = id;

                //Edit
                TransferRegister = DataContainerFacade.GetByID<SIRegister, long>(id);

                if (TransferRegister.DirectionID != null && TransferRegister.DirectionID.Value > 0)
                {
                    _mSelectedDirection = Directions.FirstOrDefault(dir => dir.ID == TransferRegister.DirectionID);
                }
                else SelectedDirection = Directions.FirstOrDefault();

                if (SelectedDirection == null) return;
                _mOperations = SelectedDirection.isFromUKToPFR.Value ? _uKtoPFROperations : _pfRtoUKOperations;

                //если ВР и выплата правореемникам
                if (contractType == Document.Types.VR && TransferRegister.OperationID == 8)
                {
                    Operations.Add(
                        new List<SPNOperation>(BLServiceSystem.Client.GetSPNOperationsByTypeListHib(1)).FirstOrDefault(
                            op => string.Equals(op.Name, "Выплаты правопреемникам", StringComparison.OrdinalIgnoreCase)));
                }

                //if (contractType == Document.Types.SI && TransferRegister.OperationID != 8)
                //{
                //    Operations = Operations.Where(op => op.ID != 8).ToList();
                //}



                if (TransferRegister.OperationID != null && TransferRegister.OperationID.Value > 0)
                {
                    var opSel = Operations.FirstOrDefault(op => op.ID == TransferRegister.OperationID);
                    //Добавляем для отображения
                    if (opSel == null)
                    {
                        opSel = DataContainerFacade.GetByID<SPNOperation>(TransferRegister.OperationID.Value);
                        Operations.Add(opSel);
                    }
                    _mSelectedOperation = opSel;

                }
                else SelectedOperation = null; //Operations.FirstOrDefault();

                if (TransferRegister.CompanyYearID != null && TransferRegister.CompanyYearID.Value > 0)
                {
                    _mSelectedYear = YearsList.FirstOrDefault(y => y.ID == TransferRegister.CompanyYearID);
                }
                else SelectedYear = YearsList.FirstOrDefault();

                if (TransferRegister.CompanyMonthID != null && TransferRegister.CompanyMonthID.Value > 0)
                {
                    SelectedMonth = MonthsList.First(m => m.ID == TransferRegister.CompanyMonthID);
                }
                RefreshTransferLists();

                TotalInvestDohod = BLServiceSystem.Client.GetTotalInvestDohodForSIRegister(id);
                UpdateTotalZL();

                //Если назначение платежа не заполнено ранее то пытаемся подобрать по выбранному направлению и содержанию.
                if (string.IsNullOrEmpty(PayAssignment))
                    UpdatePaymentAssignment();
            }
        }

        private void UpdateTotalZL()
        {
            TotalZLSumm = BLServiceSystem.Client.GetTotalZLSummForSIRegister(InternalID);
        }

        public void RefreshTransferLists()
        {
            bool alreadyChanged = IsDataChanged;

            RequestsGrid =
                BLServiceSystem.Client.GetTransfersForSIRegisterHib(ID)
                               .ConvertAll(tr => new ModelTransferItem(tr));

            IsDataChanged = alreadyChanged;
        }

        public bool Validate()
        {
            const string fields = "RegisterNumber";
            return fields.Split('|').All(f => string.IsNullOrWhiteSpace(this[f]));
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "RegisterNumber": return RegisterNumber.ValidateRequired() ?? RegisterNumber.ValidateMaxLength(50);

                    default: return null;
                }
            }
        }

        private bool IsAllowRemove(List<SITransfer> transferList)
        {
            if (RequestsGrid.Any())
            {
                if (RequestsGrid.Any(p => p.GetTransfer().IsSpnTransferredOver()))
                {
                    DialogHelper.ShowWarning("Удалить реестр невозможно, так как были выполнены перечисления СПН.\nУдалите перечисления и повторите операцию.",
                                               "Операция отменена");
                    return false;
                }
                //return true;
            }

            if (transferList.Count > 0 && BLServiceSystem.Client.IsUKPlansHasReqTransfers(transferList.Select(a => a.ID).ToList()))
            {
                DialogHelper.ShowWarning("Удалить реестр плана невозможно, так как на него существуют перечисления.\nУдалите перечисления и повторите операцию.",
    "Операция отменена");
                return false;
            }
            /* foreach (var t in transferList)
             {
                 //var pL = DataContainerFacade.GetListByProperty<SIUKPlan>("TransferListID", siTransfer.ID).ToList();
                 var pL = new List<ListPropertyCondition> 
                 { 
                     ListPropertyCondition.Equal("TransferListID", t.ID), 
                     ListPropertyCondition.NotEqual("StatusID", (long)-1),
                     ListPropertyCondition.IsNotNull("TransferID")
                 };
                 if (DataContainerFacade.GetListByPropertyConditionsCount<SIUKPlan>(pL) > 0)
                 {
                     DialogHelper.ShowWarning("Удалить реестр плана невозможно, так как на него существуют перечисления.\nУдалите перечисления и повторите операцию.",
                         "Операция отменена");
                     return false;
                 }
             }*/
            return true;
        }

        public class ModelTransferItem
        {
            private readonly ReqTransfer _transfer;

            public ModelTransferItem(ReqTransfer transfer)
            {
                _transfer = transfer;
            }

            public ReqTransfer GetTransfer()
            {
                return _transfer;
            }

            public long ID => _transfer.GetTransferList().ID;

            public string UKFormalizedName => _transfer.GetTransferList().GetContract().GetLegalEntity().FormalizedNameFull;

            public string ContractNumber => _transfer.GetTransferList().GetContract().ContractNumber;

            public decimal Sum => _transfer.Sum.GetValueOrDefault();

            public long InsuredPersonsCount => _transfer.ZLMoveCount ?? 0;

            public decimal InvestmentIncome => _transfer.InvestmentIncome.GetValueOrDefault();

            public string TransferStatus => _transfer.TransferStatus;
        }

        public Type[] UpdateListenTypes => new[] { typeof(ReqTransfer) };

        public void OnDataUpdate(Type type, long id)
        {
            if (type == typeof(ReqTransfer))
            {
                RefreshTransferLists();
                UpdateTotalZL();
            }
        }

        #region Назначение платежа 22.09.2016

        protected string UpdatePaymentAssignment()
        {
            if (SelectedDirection == null || SelectedOperation == null)
                return PayAssignment = string.Empty; 

            var part = _contractType == Document.Types.SI ? (long)Element.SpecialDictionaryItems.AppPartSI : (long)Element.SpecialDictionaryItems.AppPartVR;
            var payAss = BLServiceSystem.Client.GetPayAssForReport(part, Element.SiDirectionToCommonDirection(SelectedDirection), SelectedOperation.ID);
            return PayAssignment = payAss == null ? PayAssignment : PaymentAssignmentHelper.ReplaceTagDataForNPF(payAss.Name, RegisterDate);

        }

        #endregion
    }
}
