﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.XMLModel;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class XMLImportViewModel : ViewModelCardDialog
    {
        //public string FilePath { get; set; }
        private string _filePath;

        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value; OnPropertyChanged(nameof(FilePath));
                if (ImportData.CanExecute(null))
                    ImportData.Execute(null);
            }
        }

        public int ImporterDataCount => _importedData?.Count ?? 0;
        public bool IsValid => ImportData.CanExecute(null) && string.IsNullOrEmpty(Error) && _importedData.Any();

        private RangeObservableCollection<BDate> _importedData = new RangeObservableCollection<BDate>();

        private List<Security> _securities;
        private List<Board> _boards;
        List<string> _brokenBoards = new List<string>();
        List<string> _brokenSecurities = new List<string>();
        public DelegateCommand ImportData { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conectedList">Тип класса списка</param>
        public XMLImportViewModel(Type conectedList) : base(conectedList)
        {
            _boards = new List<Board>(DataContainerFacade.GetList<Board>().Where(s => !string.IsNullOrEmpty(s.BoardId)));
            _securities = new List<Security>(DataContainerFacade.GetList<Security>().Where(s => !string.IsNullOrEmpty(s.SecurityId)));
            ImportData = new DelegateCommand(x => !string.IsNullOrEmpty(FilePath), x => parseXMLData());
        }

        void parseXMLData()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BondXmlRow));

            XmlDocument xd = new XmlDocument();
            xd.Load(FilePath);
            _importedData.Clear();
            _brokenBoards.Clear();
            _brokenSecurities.Clear();
            //Десериализация и конвертирование в BData
            foreach (var node in xd.SelectNodes("document/data/rows/row[@BOARDID='TQOB' and starts-with(@SECID,'SU') and starts-with(@SHORTNAME,'ОФЗ')]"))
            {
                XmlNode xnode = ((XmlNode)node);
                var row = (BondXmlRow)serializer.Deserialize(new StringReader(xnode.OuterXml));
                _importedData.Add(convertRowToBDate(row));
            }
            checkHasBrokenData();

            if (!string.IsNullOrEmpty(Error))
                DialogHelper.ShowError(Error);

            OnPropertyChanged(nameof(IsValid));
        }



        void checkHasBrokenData()
        {
            //Проверка отсутствия не найденных данных в БД, формирование ошибки
            if (!_brokenBoards.Any() && !_brokenSecurities.Any())
            { Error = string.Empty; return; }

            var sb = new StringBuilder("В базе данных не найдены записи для:\n\r");

            if (_brokenBoards.Any())
            {
                sb.AppendLine(" - Режимы торгов: ");
                int count = 0;
                foreach (var b in _brokenBoards.Distinct())
                {
                    if (count > 4)
                    {
                        sb.AppendLine();
                        count = 0;
                    }
                    sb.Append($"\"{b}\" ");
                    count++;
                }
                sb.AppendLine("");
            }
            if (_brokenSecurities.Any())
            {
                sb.AppendLine(" - Ценные бумаги: ");
                int count = 0;
                foreach (var b in _brokenSecurities.Distinct())
                {
                    if (count > 4)
                    {
                        sb.AppendLine();
                        count = 0;
                    }
                    sb.Append($"\"{b}\" ");
                    count++;
                }
                sb.AppendLine("");
            }
            sb.AppendLine("Добавьте в справочные таблицы необходимые записи и повторите импорт");
            Error = sb.ToString();
            //return Error;
            //DialogHelper.ShowError(Error);


        }

        public override string this[string columnName] => columnName == "FilePath" ? FilePath.ValidateNonEmpty() : string.Empty;

        protected override void ExecuteSave()
        {
            foreach (var b in _importedData)
            {
                DataContainerFacade.Save(b);
            }
            
        }

        public override bool IsDataChanged
        {
            get { return _importedData.Any(); }
            set {; }
        }

        public override bool CanExecuteSave()
        {
            return _importedData.Any(); // ValidateFields();
        }



        private BDate convertRowToBDate(BondXmlRow row)
        {
            long? sec_id =
                _securities.FirstOrDefault(
                    s => s.SecurityId.Equals(row.SECID, StringComparison.InvariantCultureIgnoreCase))?.ID;

            long? board_id =
                _boards.FirstOrDefault(b => b.BoardId.Equals(row.BOARDID, StringComparison.InvariantCultureIgnoreCase))?.ID;

            if (!sec_id.HasValue)
                _brokenSecurities.Add(row.SECID);
            if (!board_id.HasValue)
                _brokenBoards.Add(row.BOARDID);

            var bdate = row.ConvertToBDate();
            bdate.BoardID = board_id;
            bdate.SecurityID = sec_id;
            return bdate;
        }

    }
}
