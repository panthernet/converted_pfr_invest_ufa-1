﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Annotations;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using License = PFR_INVEST.DataObjects.License;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
	public class NPFViewModel : ViewModelCard, IUpdateListenerModel, IValidatableViewModel
	{
		public EventHandler OnListIsEmpty;
		protected void RaiseListIsEmpty()
		{
		    OnListIsEmpty?.Invoke(this, new EventArgs());
		}

		public LegalEntity LegalEntity { get; private set; }
		public LegalEntityChief LegalEntityChief { get; private set; }
		public List<LegalEntityChief> ChangedLegalEntityChiefList { get; private set; }

#if WEBCLIENT
	    [Display(Name = "№ соглашения с ПФР")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string PFRAGR { get { return LegalEntity?.PFRAGR; } set { LegalEntity.PFRAGR = value; } }

        [Display(Name = "Дата соглашения с ПФР")]
        public DateTime? PFRAGRDATE { get { return LegalEntity?.PFRAGRDATE; } set { LegalEntity.PFRAGRDATE = value; } }
#endif

        private Status _status;
		public Status Status
		{
			get { return _status; }
			private set
			{
				if (value == null)
					throw new Exception("Статус НПФ невозможно сбросить в NULL");

				if (_status == null)
				{
					var msg = "Статус НПФ: " + value.Name;
					JournalLogger.LogChangeStateEvent(this, msg);
				}
				else
				{
					if (_status.Equals(value))
						return;

					var msg = "Статус НПФ: " + _status.Name + " -> " + value.Name;
					JournalLogger.LogChangeStateEvent(this, msg);
				}

				_status = value;
				OnPropertyChanged("NPFStatus");
				OnPropertyChanged("IsGarantAllowed");
			}
		}

		public Contragent Contragent { get; private set; }
		public License License { get; private set; }
		public BankAccount CurrentBankAccount { get; private set; }

		public long ContragentID
		{
		    get { return Contragent.ID; }
#if WEBCLIENT
            set { Contragent.ID = value; }
#endif
        }

	    //используеться для отключения полей, после сохранения объектов
		public bool IsCardSaved => State != ViewModelState.Create || EditAccessDenied;

	    #region Основные сведения
		private string _mOriginalFormalizedName;

	    [Display(Name = "Полное официальное наименование")]
	    [Required]
	    [StringLength(128, MinimumLength = 10, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string FullName
		{
			get
			{
				return LegalEntity.FullName;
			}
			set
			{
				LegalEntity.FullName = value;
				OnPropertyChanged("FullName");
			}
		}

		private string _mOriginalFullName;

	    [Display(Name = "Краткое официальное наименование")]
	    [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string ShortName
		{
			get
			{
				return LegalEntity.ShortName;
			}
			set
			{
				LegalEntity.ShortName = value;
				OnPropertyChanged("ShortName");
			}
		}

		private string _mOriginalShortName;
	    [Display(Name = "Формализованное наименование")]
	    [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string FormalizedName
		{
			get
			{
				return LegalEntity.FormalizedName;
			}
			set
			{
				LegalEntity.FormalizedName = value;
				OnPropertyChanged("FormalizedName");
			}
		}

	    [Display(Name = "Номер государственной регистрации")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string RegistrationNumber
		{
			get {
			    return LegalEntity.RegistrationNum?.Trim();
			}
		    set
			{
				LegalEntity.RegistrationNum = value;
				OnPropertyChanged("RegistrationNumber");
			}
		}

	    [Display(Name = "Дата регистрации")]
        public DateTime? RegistrationDate
		{
			get
			{
				return LegalEntity.RegistrationDate;
			}
			set
			{
				LegalEntity.RegistrationDate = value;
				OnPropertyChanged("RegistrationDate");
			}
		}

	    [Display(Name = "Орган, осуществивший регистрацию")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Registrator
		{
			get
			{
				return LegalEntity.Registrator;
			}
			set
			{
				LegalEntity.Registrator = value;
				OnPropertyChanged("Registrator");
			}
		}

        [Display(Name = "Статус")]
		public string NPFStatus => Status == null ? string.Empty : Status.Name;

        #endregion

        #region Адрес
	    [Display(Name = "Юридический адрес")]
        [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string LegalAddress
		{
			get
			{
				return LegalEntity.LegalAddress;
			}
			set
			{
				LegalEntity.LegalAddress = value;
				OnPropertyChanged("LegalAddress");
			}
		}

	    [Display(Name = "Почтовый адрес")]
	    [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string PostalAddress
		{
			get
			{
				return LegalEntity.PostAddress;
			}
			set
			{
				LegalEntity.PostAddress = value;
				OnPropertyChanged("PostalAddress");
			}
		}

	    [Display(Name = "Фактический адрес")]
	    [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string RealAddress
		{
			get
			{
				return LegalEntity.StreetAddress;
			}
			set
			{
				LegalEntity.StreetAddress = value;
				OnPropertyChanged("RealAddress");
			}
		}

	    [Display(Name = "Телефон")]
	    [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Phone
		{
			get
			{
				return LegalEntity.Phone;
			}
			set
			{
				LegalEntity.Phone = value;
				OnPropertyChanged("Phone");
			}
		}

	    [Display(Name = "Факс")]
	    [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Fax
		{
			get
			{
				return LegalEntity.Fax;
			}
			set
			{
				LegalEntity.Fax = value;
				OnPropertyChanged("Fax");
			}
		}

	    [Display(Name = "Адрес Эл. почты")]
	    [Required]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [EmailAnnotation]
        public string EMail
		{
			get
			{
				return LegalEntity.EAddress;
			}
			set
			{
				LegalEntity.EAddress = value;
				OnPropertyChanged("EMail");
			}
		}

	    [Display(Name = "Сайт в сети Интернет")]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Site
		{
			get
			{
				return LegalEntity.Site;
			}
			set
			{
				LegalEntity.Site = value;
				OnPropertyChanged("Site");
			}
		}
        #endregion

        #region Лицензии
	    [Display(Name = "Номер лицензии")]
	    [Required]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
		public string LicNumber
		{
			get {
			    return License.Number?.Trim();
			}
		    set
			{
				License.Number = value;
				LicenseValidator.Reset();
				OnPropertyChanged("LicNumber");
			}
		}

        /// <summary>
        /// Разрешаем редактирование даты лицензии если она не введена
        /// </summary>
        public bool IsLicenseDateEditDisabled => LicRegDate != null;

	    [Display(Name = "Дата выдачи лицензии")]
	    [DataType(DataType.Date)]
	    [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? LicRegDate
		{
			get
			{
				return License.RegistrationDate;
			}
			set
			{
				License.RegistrationDate = value;
                OnPropertyChanged("LicRegDate");
                OnPropertyChanged("IsLicenseDateEditEnabled");
			}
		}

	    [Display(Name = "Орган, выдавший лицензию")]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string LicRegistrator
		{
			get
			{
				return License.Registrator;
			}
			set
			{
				License.Registrator = value;
				OnPropertyChanged("LicRegistrator");
			}
		}


	    [Display(Name = "Дата аннулирования лицензии")]
	    [DataType(DataType.Date)]
	    [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? LicCloseDate
	    {
	        get { return License.CloseDate; }
	        set { License.CloseDate = value; }
	    }

	    [Display(Name = "Дата ликвидации НПФ")]
	    [DataType(DataType.Date)]
	    [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? CloseDate
		{
			get
			{
				return LegalEntity.CloseDate;
			}
			set
			{
				LegalEntity.CloseDate = value;
				RefreshStatus();
				OnPropertyChanged("CloseDate");
			}
		}
        #endregion

        #region Руководство
	    [DisplayName("Имя")]
	    [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string HeadFirstName
		{
			get
			{
				return LegalEntityChief.FirstName;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.FirstName = x;
				LegalEntity.HeadFullName = GetFullName();
				OnPropertyChanged("HeadFirstName");
			}
		}

		private static string TrimNull(string value)
		{
		    return value == null ? null : value.Trim();
		}

        [DisplayName("Фамилия")]
        [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [Required]
	    public string HeadLastName
		{
			get
			{
				return LegalEntityChief.LastName;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.LastName = x;
				LegalEntity.HeadFullName = GetFullName();
				OnPropertyChanged("HeadLastName");
			}
		}

	    [DisplayName("Отчество")]
	    [StringLength(100, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string HeadPatronymicName
		{
			get
			{
				return LegalEntityChief.PatronymicName;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.PatronymicName = x;
				LegalEntity.HeadFullName = GetFullName();
				OnPropertyChanged("HeadPatronymicName");
			}
		}

		private string GetFullName()
		{
		    return string.IsNullOrEmpty(LegalEntityChief.PatronymicName) ? $"{LegalEntityChief.LastName} {LegalEntityChief.FirstName}" : $"{LegalEntityChief.LastName} {LegalEntityChief.FirstName} {LegalEntityChief.PatronymicName}";
		}

	    [DisplayName("Должность руководителя")]
	    [StringLength(128, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [Required]
        public string HeadPosition
		{
			get
			{
				return LegalEntityChief.Position;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntity.HeadPosition = x;
				LegalEntityChief.Position = x;
				OnPropertyChanged("HeadPosition");
			}
		}

	    [DisplayName("Действует на основании")]
	    [StringLength(300, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string HeadAccordingTo
		{
			get
			{
				return LegalEntityChief.AccordingTo;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.AccordingTo = x;
				OnPropertyChanged("HeadAccordingTo");
			}
		}

	    [DisplayName("Телефон рабочий")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
       // [Mask("+7 (999) 999 99 99")]
        public string HeadWorkPhone
		{
			get
			{
				return LegalEntityChief.WorkPhone;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.WorkPhone = x;
				OnPropertyChanged("HeadWorkPhone");
			}
		}

	    [DisplayName("Доб.")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "n")]
        public string HeadWorkPhoneExtension
		{
			get
			{
				return LegalEntityChief.WorkPhoneExtension;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.WorkPhoneExtension = x;
				OnPropertyChanged("HeadWorkPhoneExtension");
			}
		}

	    [DisplayName("Телефон мобильный")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string HeadMobilePhone
		{
			get
			{
				return LegalEntityChief.MobilePhone;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.MobilePhone = x;
				OnPropertyChanged("HeadMobilePhone");
			}
		}

	    [DisplayName("Электронная почта")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [EmailAnnotation]
        public string HeadEmail
		{
			get
			{
				return LegalEntityChief.Email;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntityChief.Email = x;
				OnPropertyChanged("HeadEmail");
			}
		}

	    [Display(Name = "Дата вступления в должность")]
	    [DataType(DataType.Date)]
	    [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? HeadInaugurationDate
		{
			get
			{
				return LegalEntityChief.InaugurationDate;
			}
			set
			{
				LegalEntityChief.InaugurationDate = value;
				OnPropertyChanged("HeadInaugurationDate");
				OnPropertyChanged("HeadRetireDate");
			}
		}

	    [Display(Name = "Дата прекращения полномочий")]
	    [DataType(DataType.Date)]
	    [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? HeadRetireDate
		{
			get
			{
				return LegalEntityChief.RetireDate;
			}
			set
			{
				LegalEntityChief.RetireDate = value;
				OnPropertyChanged("HeadInaugurationDate");
				OnPropertyChanged("HeadRetireDate");
			}
		}

        [DisplayName("Комментарий")]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Comment
		{
			get
			{
				return LegalEntityChief.Comment;
			}
			set
			{
				var x = TrimNull(value);
				LegalEntity.Comment = x;
				LegalEntityChief.Comment = x;
				OnPropertyChanged("Comment");
			}
		}
		#endregion

		#region Банковские реквизиты
		private bool _IsGarantAllowed = true;
		public bool IsGarantAllowed => StatusIdentifier.IsNPFStatusActivityCarries(NPFStatus) && _IsGarantAllowed;

	    private bool _isGaranted;
		public bool IsGaranted
		{
			get { return _isGaranted; }
			set
			{
				if (_isGaranted != value)
				{
					_isGaranted = value;
					OnPropertyChanged("IsGaranted");
					GarantACBID = value ? (long)LegalEntity.GarantACB.Included : (long)LegalEntity.GarantACB.NotIncluded;
				}
			}
		}

		public long? GarantACBID
		{
			get { return LegalEntity.GarantACBID; }
			set
			{
				if (LegalEntity.GarantACBID != value)
				{
					LegalEntity.GarantACBID = value;
					OnPropertyChanged("GarantACBID");
					OnPropertyChanged("FundNum");
					OnPropertyChanged("FundDate");
					//IsGaranted = GarantACBID.HasValue && GarantACBID.Value == (long)LegalEntity.GarantACB.Included;
				}
			}
		}

        [DisplayName("Номер по реестру фондов-участников")]
        [StringLength(10, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
		public long? FundNum
		{
			get { return LegalEntity.FundNum; }
			set { if (LegalEntity.FundNum != value) { LegalEntity.FundNum = value; OnPropertyChanged("FundNum"); }}
		}

        [DisplayName("Дата внесения в реестр фондов-участников")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? FundDate
		{
			get { return LegalEntity.FundDate; }
			set { if (LegalEntity.FundDate != value) { LegalEntity.FundDate = value; OnPropertyChanged("FundDate"); }}
		}

        [DisplayName("Номер уведомления")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
		public string NotifNum
		{
			get { return LegalEntity.NotifNum; }
			set { if (LegalEntity.NotifNum != value) { LegalEntity.NotifNum = value; OnPropertyChanged("NotifNum"); } }
		}

	    [DisplayName("Дата уведомления")]
	    [DataType(DataType.Date)]
	    [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? NotifDate
		{
			get { return LegalEntity.NotifDate; }
			set { if (LegalEntity.NotifDate != value) { LegalEntity.NotifDate = value; OnPropertyChanged("NotifDate"); }}
		}

        #endregion

        #region Банковские реквизиты
	    [DisplayName("ИНН НПФ")]
	    [StringLength(10, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string INN
		{
			get {
			    return LegalEntity.INN?.Trim();
			}
		    set
			{
				LegalEntity.INN = value;
				OnPropertyChanged("INN");
			}
		}

	    [DisplayName("КПП НПФ")]
	    [StringLength(9, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string OKPP
		{
			get {
			    return LegalEntity.OKPP != null ? LegalEntity.OKPP.Trim() : null;
			}
		    set
			{
				LegalEntity.OKPP = value;
				OnPropertyChanged("OKPP");
			}
		}

	    [DisplayName("Наименование получателя платежа (сокращенное)")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [Required]
        public string CurrBankAccLegalEntityName
		{
			get
			{
				return CurrentBankAccount.LegalEntityName;
			}
			set
			{
				CurrentBankAccount.LegalEntityName = value;
				OnPropertyChanged("CurrBankAccLegalEntityName");
			}
		}

	    [DisplayName("Номер счёта получателя платежа")]
	    [StringLength(20, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string CurrBankAccAccountNumber
		{
			get
			{
				return CurrentBankAccount.AccountNumber;
			}
			set
			{
				CurrentBankAccount.AccountNumber = value;
				OnPropertyChanged("CurrBankAccAccountNumber");
			}
		}

	    [DisplayName("Банк получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string CurrBankAccBankName
		{
			get
			{
				return CurrentBankAccount.BankName;
			}
			set
			{
				CurrentBankAccount.BankName = value;
				OnPropertyChanged("CurrBankAccBankName");
			}
		}

	    [DisplayName("Местонахождение банка получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string CurrBankAccBankLocation
		{
			get
			{
				return CurrentBankAccount.BankLocation;
			}
			set
			{
				CurrentBankAccount.BankLocation = value;
				OnPropertyChanged("CurrBankAccBankLocation");
			}
		}

	    [DisplayName("Корреспондентский счет банка получателя платежа")]
	    [StringLength(22, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string CurrBankAccCorrespondentAccountNumber
		{
			get
			{
				return IsCardSaved ? CurrentBankAccount.CorrespondentAccountFull : CurrentBankAccount.CorrespondentAccountNumber;
			}
			set
			{
				CurrentBankAccount.CorrespondentAccountNumber = value;
				OnPropertyChanged("CurrBankAccCorrespondentAccountNumber");
			}
		}

		public string Mask => IsCardSaved ? null : @"\d{20}";

	    public string MaskType => IsCardSaved ? null : "RegEx";

	    [DisplayName("БИК получателя платежа")]
	    [StringLength(9, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string CurrBankAccBIK
		{
			get {
			    return CurrentBankAccount.BIK;
			}
		    set
			{
				CurrentBankAccount.BIK = value;
				OnPropertyChanged("CurrBankAccBIK");
			}
		}

	    [DisplayName("ИНН получателя платежа")]
	    [StringLength(10, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string CurrBankAccINN
		{
			get {
			    return CurrentBankAccount.INN?.Trim();
			}
		    set
			{
				CurrentBankAccount.INN = value;
				OnPropertyChanged("CurrBankAccINN");
			}
		}

	    [DisplayName("КПП получателя платежа")]
	    [StringLength(9, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string CurrBankAccKPP
		{
			get {
			    return CurrentBankAccount.KPP?.Trim();
			}
		    set
			{
				CurrentBankAccount.KPP = value;
				OnPropertyChanged("CurrBankAccKPP");
			}
		}
		#endregion


		#region Списки
		private List<OldNPFName> _mOldNamesList;
		public List<OldNPFName> OldNamesList
		{
			get
			{
				return _mOldNamesList;
			}
			set
			{
				_mOldNamesList = value;
				OnPropertyChanged("OldNamesList");
			}
		}

		private List<NPFPause> _mNPFPauseList;
		public List<NPFPause> NPFPauseList
		{
			get
			{
				return _mNPFPauseList;
			}
			set
			{
				_mNPFPauseList = value;
				OnPropertyChanged("NPFPauseList");
				OnPropertyChanged("HasPauses");
			}
		}

		public Visibility HasPauses => NPFPauseList != null && NPFPauseList.Count > 0 ? Visibility.Visible : Visibility.Collapsed;

        private IList<BankAccount> _mOldBankAccountsList;
		public IList<BankAccount> OldBankAccountsList
		{
			get
			{
				return _mOldBankAccountsList;
			}
			set
			{
				_mOldBankAccountsList = value;
				OnPropertyChanged("OldBankAccountsList");
			}
		}

		public Branch SelectedBranch
		{
			get;
			set;
		}
		private List<Branch> _mBranchesList;
		public List<Branch> BranchesList
		{
			get
			{
				return _mBranchesList;
			}
			set
			{
				_mBranchesList = value;
				OnPropertyChanged("BranchesList");
				OnPropertyChanged("HasBranches");
			}
		}

		public Visibility HasBranches => BranchesList != null && BranchesList.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
        public Reorganization SelectedReorg { get; set; }

		private List<Reorganization> _mReorganizationsList;
		public List<Reorganization> ReorganizationsList
		{
			get
			{
				return _mReorganizationsList;
			}
			set
			{
				_mReorganizationsList = value;
				OnPropertyChanged("ReorganizationsList");
				OnPropertyChanged("HasReorganizations");
				OnPropertyChanged("HasAnyReorganizations");
			}
		}

		public Visibility HasReorganizations => ReorganizationsList != null && ReorganizationsList.Count > 0 ? Visibility.Visible : Visibility.Collapsed;

	    private List<Reorganization> _mParentReorganizationsList;
		public List<Reorganization> ParentReorganizationsList
		{
			get
			{
				return _mParentReorganizationsList;
			}
			set
			{
				_mParentReorganizationsList = value;
				OnPropertyChanged("ParentReorganizationsList");
				OnPropertyChanged("HasParentReorganizations");
				OnPropertyChanged("HasAnyReorganizations");
			}
		}

		public Visibility HasParentReorganizations => ParentReorganizationsList != null && ParentReorganizationsList.Count > 0 ? Visibility.Visible : Visibility.Collapsed;

	    public Visibility HasAnyReorganizations => HasReorganizations == Visibility.Visible || HasParentReorganizations == Visibility.Visible ? Visibility.Visible : Visibility.Collapsed;

        private ObservableList<LegalEntityChief> _oldChiefList;
		public ObservableList<LegalEntityChief> OldChiefList
		{
			get
			{
				return _oldChiefList;
			}
			set
			{
				_oldChiefList = value;
				OnPropertyChanged("OldChiefList");
			}
		}

		private List<LegalEntityCourier> _courierList;
		public List<LegalEntityCourier> CourierList
		{
			get
			{
				return _courierList;
			}
			set
			{
				_courierList = value;
				OnPropertyChanged("CourierList");
			}
		}

		private List<LegalEntityCourier> _courierListDeleted;
		public List<LegalEntityCourier> CourierListDeleted
		{
			get
			{
				return _courierListDeleted;
			}
			set
			{
				_courierListDeleted = value;
				OnPropertyChanged("CourierListDeleted");
			}
		}

		private List<LegalEntityCourier> _courierListChanged;
		public List<LegalEntityCourier> CourierListChanged
		{
			get
			{
				return _courierListChanged;
			}
			set
			{
				_courierListChanged = value;
				OnPropertyChanged("CourierListChanged");
			}
		}

		public ObservableList<LegalEntityCourier> ContactList { get; private set; }
		private IList<LegalEntityIdentifier> _allIdentifierList;
		public IList<LegalEntityIdentifier> AllIdentifierList
		{
			get
			{
				return _allIdentifierList;
			}
			set
			{
				_allIdentifierList = value;
				OnPropertyChanged("AllIdentifierList");
			}
		}


		private List<LegalEntityIdentifier> _identifierList;
		public List<LegalEntityIdentifier> IdentifierList
		{
			get
			{
				return _identifierList;
			}
			set
			{
				_identifierList = value;
				OnPropertyChanged("IdentifierList");
			}
		}

		private List<LegalEntityIdentifier> _identifierListDeleted;
		public List<LegalEntityIdentifier> IdentifierListDeleted
		{
			get
			{
				return _identifierListDeleted;
			}
			set
			{
				_identifierListDeleted = value;
				OnPropertyChanged("IdentifierListDeleted");
			}
		}

		private List<LegalEntityIdentifier> _identifierListChanged;
		public List<LegalEntityIdentifier> IdentifierListChanged
		{
			get
			{
				return _identifierListChanged;
			}
			set
			{
				_identifierListChanged = value;
				OnPropertyChanged("IdentifierListChanged");
			}
		}

		#endregion

		#region Обновление списков
		public void RefreshOldNPFNamesList()
		{
			try
			{
				OldNamesList = BLServiceSystem.Client.GetOldNPFNamesSortedByDate(ID);
			}
			catch (Exception e)
			{
				HandleException(e);
			}
		}

		public void RefreshReorganizationsList()
		{
			try
			{
				var oldIsDataChanged = IsDataChanged;

				ReorganizationsList = DataContainerFacade.GetListByProperty<Reorganization>("ReceiverContragentID", ContragentID).ToList();
				if (ReorganizationsList.Count == 0)
					RaiseListIsEmpty();

				ParentReorganizationsList = DataContainerFacade.GetListByProperty<Reorganization>("SourceContragentID", ContragentID).ToList();
				if (ParentReorganizationsList.Count == 0)
					RaiseListIsEmpty();

				RefreshStatus();
				IsDataChanged = oldIsDataChanged;
			}
			catch (Exception e)
			{
				HandleException(e);
			}
		}

		public LegalEntity GetLegalEntityByContragentID(long contragentID)
		{
			return DataContainerFacade.GetListByProperty<LegalEntity>("ContragentID", contragentID).FirstOrDefault();
		}

		public void RefreshNPFPauseList()
		{
			try
			{
				var alreadyChanged = IsDataChanged;
				NPFPauseList = DataContainerFacade.GetListByProperty<NPFPause>("ContragentID", ContragentID).ToList();
				RefreshStatus();
				//RefreshConnectedViewModels();
				IsDataChanged = alreadyChanged;
			}
			catch (Exception e)
			{
				HandleException(e);
			}
		}

		public void RefreshBankAccountsList()
		{
			try
			{
				var alreadyChanged = IsDataChanged;
				//Теоритически BA должен быть, но с нашим импортом все возможно
				DataContainerFacade.ResetExtensionData(LegalEntity, "BankAccounts");
				CurrentBankAccount = LegalEntity.GetBankAccount() ?? new BankAccount();
				OnPropertyChanged("CurrBankAccLegalEntityName");
				OnPropertyChanged("CurrBankAccAccountNumber");
				OnPropertyChanged("CurrBankAccBankName");
				OnPropertyChanged("CurrBankAccBankLocation");
				OnPropertyChanged("CurrBankAccCorrespondentAccountNumber");
				OnPropertyChanged("CurrBankAccBIK");
				OnPropertyChanged("CurrBankAccINN");
				OnPropertyChanged("CurrBankAccKPP");
				OldBankAccountsList = BLServiceSystem.Client.GetOldBankAccounts(LegalEntity.ID);
				//OldBankAccountsList.ForEach(
				//    ba =>
				//        ba.CorrespondentAccountNumber =
				//            string.Join(" ", ba.CorrespondentAccountNumber, ba.CorrespondentAccountNote));
				IsDataChanged = alreadyChanged;
			}
			catch (Exception e)
			{
				HandleException(e);
			}
		}

		public void RefreshBranchesList()
		{
			try
			{
				var alreadyChanged = IsDataChanged;
				SelectedBranch = null;
				BranchesList = DataContainerFacade.GetListByProperty<Branch>("LegalEntityID", LegalEntity.ID).ToList();
				if (BranchesList.Count > 0)
					SelectedBranch = BranchesList.First();
				else
					RaiseListIsEmpty();
				IsDataChanged = alreadyChanged;
			}
			catch (Exception e)
			{
				HandleException(e);
			}
		}

		public void RefreshStatus()
		{
			//Сюда надо вынести всю работу со статусом!

			if (Contragent.StatusID == -1)
				return;

			Contragent.StatusID = StatusIdentifier.Identifier.Active.ToLong();

			if (CloseDate <= DateTime.Now.Date)
			{
                Contragent.StatusID = StatusIdentifier.Identifier.Eliminated.ToLong();
				Status = DataContainerFacade.GetByID<Status, long>(Contragent.StatusID);
				IsGaranted = false;
				return;
			}

			if (LicCloseDate <= DateTime.Now.Date)
			{
                Contragent.StatusID = StatusIdentifier.Identifier.LicenseRevoked.ToLong();
				Status = DataContainerFacade.GetByID<Status, long>(Contragent.StatusID);
				IsGaranted = false;
				return;
			}

			if (ParentReorganizationsList != null)
			{
			    if (ParentReorganizationsList.Any(item => item.ReorganizationTypeID == ReorganizationIdentifier.TypeJoin 
			                                              || item.ReorganizationTypeID == ReorganizationIdentifier.TypeMerge 
			                                              || item.ReorganizationTypeID == ReorganizationIdentifier.TypeTransformation))
			    {
                    Contragent.StatusID = StatusIdentifier.Identifier.Reorganized.ToLong();
			        Status = DataContainerFacade.GetByID<Status, long>(Contragent.StatusID);
			        DataContainerFacade.Save<Contragent, long>(Contragent);
			        IsGaranted = false;
			        return;
			    }
			    //если отсутствуют реорганизации, меняем статус обратно
                if (Contragent.StatusID == StatusIdentifier.Identifier.Reorganized.ToLong())
				{
                    Contragent.StatusID = StatusIdentifier.Identifier.Active.ToLong();
					Status = DataContainerFacade.GetByID<Status, long>(Contragent.StatusID);
				}
			}

		    if (NPFPauseList != null)
			{
				foreach (var pause in NPFPauseList)
				{
					if (pause.StartDate > DateTime.Now.Date || pause.EndDate < DateTime.Now.Date)
                        Contragent.StatusID = StatusIdentifier.Identifier.Active.ToLong();
					else
                        Contragent.StatusID = StatusIdentifier.Identifier.Suspended.ToLong(); break;
				}
			}

			Status = DataContainerFacade.GetByID<Status, long>(Contragent.StatusID);
            IsGaranted = IsGaranted && Contragent.StatusID == StatusIdentifier.Identifier.Active.ToLong();

			DataContainerFacade.Save<Contragent, long>(Contragent);
		}
		#endregion

		#region Commands
		public ICommand AddBranch { get; private set; }
		public ICommand EditBranch { get; private set; }
		public ICommand DeleteBranch { get; private set; }
		public ICommand AnnulLicense { get; private set; }
		public ICommand CancelAnnulLicense { get; private set; }
		public ICommand DeleteReorg { get; private set; }
		public ICommand AnnulCloseDate { get; private set; }
		public ICommand EditAccount { get; private set; }
		public ICommand RetireChief { get; private set; }
		public ICommand AddCourier { get; private set; }
		public ICommand DeleteCourier { get; private set; }
		public ICommand ShowDeletedCourierList { get; private set; }

		public DelegateCommand<object> AddContact { get; private set; }
		public DelegateCommand<LegalEntityCourier> DeleteContact { get; private set; }
		public DelegateCommand<LegalEntityCourier> EditContact { get; private set; }

		public DelegateCommand<object> AddIdentifier { get; private set; }
		public DelegateCommand<LegalEntityIdentifier> DeleteIdentifier { get; private set; }
		public DelegateCommand<LegalEntityIdentifier> EditIdentifier { get; private set; }

		private void InitCommands()
		{
			AddBranch = new DelegateCommand(o => CanExecuteAddBranch(), o => ExecuteAddBranch());
			EditBranch = new DelegateCommand(o => CanExecuteEditBranch(), o => ExecuteEditBranch());
			DeleteBranch = new DelegateCommand(o => CanExecuteDeleteBranch(), o => ExecuteDeleteBranch());

			AnnulLicense = new DelegateCommand(o => CanExecuteAnnulLicence(), o => ExecuteAnnulLicence());
			CancelAnnulLicense = new DelegateCommand(o => CanExecuteCancelAnnulLicence(), o => ExecuteCancelAnnulLicence());

			DeleteReorg = new DelegateCommand(o => CanExecuteDeleteReorg(), o => ExecuteDeleteReorg());

			AnnulCloseDate = new DelegateCommand(o => CanAnnulCloseDate(), o => ExecuteAnnulCloseDate());

			RetireChief = new DelegateCommand(o => CanExecuteRetireChief(), o => ExecuteRetireChief());

			AddCourier = new DelegateCommand(o => CanExecuteAddCourier(), o => ExecuteAddCourier());
			DeleteCourier = new DelegateCommand(o => CanExecuteDeleteCourier(), o => ExecuteDeleteCourier());
			ShowDeletedCourierList = new DelegateCommand(o => CanExecuteShowDeletedCourierList(), o => ExecuteShowDeletedCourierList());

			AddContact = new DelegateCommand<object>(o => CanExecuteAddContact(), ExecuteAddContact);
			DeleteContact = new DelegateCommand<LegalEntityCourier>(CanExecuteDeleteContact, ExecuteDeleteContact);
			EditContact = new DelegateCommand<LegalEntityCourier>(CanExecuteEditContact, ExecuteEditContact);

			AddIdentifier = new DelegateCommand<object>(o => CanExecuteAddIdentifier(), ExecuteAddIdentifier);
			DeleteIdentifier = new DelegateCommand<LegalEntityIdentifier>(o => CanExecuteDeleteIdentifier(), o => ExecuteDeleteIdentifier());
			EditIdentifier = new DelegateCommand<LegalEntityIdentifier>(CanExecuteEditIdentifier, ExecuteEditIdentifier);


		}
        #endregion

#if WEBCLIENT


	    public static List<BankAccount> WebGetOldBankAccounts(long leId)
	    {
	        return BLServiceSystem.Client.GetOldBankAccounts(leId).ToList();
        }

        public List<LegalEntityCourier> WebContactsList => ContactList?.ToList();

        public static void WebDeleteIdentifier(long id)
	    {
	        DataContainerFacade.Delete<LegalEntityIdentifier>(id);
	    }

	    public static void WebDeleteCourier(long id)
	    {
	        DataContainerFacade.Delete<LegalEntityCourier>(id);
	    }

        public static void WebDeleteContact(long id)
	    {
	        DataContainerFacade.Delete<LegalEntityCourier>(id);
	    }

        [DisplayName("В реестр фондов-участников внесен")]
	    public bool? IsGarantedNullable
	    {
	        get { return _isGaranted; }
            set { IsGaranted = value ?? false; }
	    }

        public bool WebDoChiefRetire { get; set; }

	    public bool HasPausesBool => HasPauses == Visibility.Visible;
	    public bool HasBranchesBool => HasBranches == Visibility.Visible;
	    public bool HasAnyReorganizationsBool => HasAnyReorganizations == Visibility.Visible;
	    public bool HasParentReorganizationsBool => HasParentReorganizations == Visibility.Visible;
	    public bool HasReorganizationsBool => HasReorganizations == Visibility.Visible;

#endif

        public NPFViewModel(ViewModelState action, long id)
			: base(typeof(NPFListViewModel))
		{
			DataObjectTypeForJournal = typeof(LegalEntity);
			InitCommands();
			State = action;
			ContactList = new ObservableList<LegalEntityCourier>();
			ContactList.CollectionChanged += (sender, e) => { IsDataChanged = true; };
			ContactList.ItemChanged += (sender, e) => { IsDataChanged = true; };

			IdentifierList = new List<LegalEntityIdentifier>();
			AllIdentifierList = new List<LegalEntityIdentifier>();


			if (State == ViewModelState.Create)
			{
				LegalEntity = new LegalEntity();
				LoadLegalEntityChief(LegalEntity);

				Contragent = new Contragent
				{
					StatusID = 1,
					TypeName = ContragentIdentifier.NPF
				};
				CurrentBankAccount = new BankAccount();
				Status = DataContainerFacade.GetByID<Status>(Contragent.StatusID);
				//IsGarantAllowed = StatusIdentifier.IsNPFStatusActivityCarries(NPFStatus);
				GarantACBID = (long)LegalEntity.GarantACB.NotIncluded;
				License = new License();
				RetireMode = EnRetireMode.CanRetire;
				LoadCourierAndContactAndIdentifierList(LegalEntity);
			}
			else
			{
				ID = id;

				LegalEntity = DataContainerFacade.GetByID<LegalEntity>(ID);
				LoadLegalEntityChief(LegalEntity);


				Contragent = DataContainerFacade.GetByID<Contragent>(LegalEntity.ContragentID);
				Status = DataContainerFacade.GetByID<Status>(Contragent.StatusID);
				
				//if (IsGarantAllowed)
				//    IsGaranted = GarantACBID == (long)LegalEntity.GarantACB.Included;

				IsGaranted = GarantACBID == (long)LegalEntity.GarantACB.Included;

				License = BLServiceSystem.Client.GetCurrentLicense(ID) ?? new License();
			    LoadCourierAndContactAndIdentifierList(LegalEntity);

				_mOriginalFullName = LegalEntity.FullName;
				_mOriginalShortName = LegalEntity.ShortName;
				_mOriginalFormalizedName = LegalEntity.FormalizedName;


				RefreshBankAccountsList();
				RefreshOldNPFNamesList();
				RefreshReorganizationsList();
				RefreshNPFPauseList();
				RefreshBranchesList();

			}

			LicenseValidator = new AsyncValidator(() =>
			{
				var result = BLServiceSystem.Client.CheckLicenseUnique(LegalEntity.ID, License.Number, Contragent.TypeName);
				return result.IsSuccess ? null : result.ErrorMessage;
			},
			() => { OnPropertyChanged("LicNumber"); });

			LegalEntity.PropertyChanged += (sender, e) => { IsDataChanged = true; };
			IsDataChanged = false;
		}

		private void LoadCourierAndContactAndIdentifierList(LegalEntity legalEntity)
		{
			CourierListChanged = new List<LegalEntityCourier>();
			IdentifierListChanged = new List<LegalEntityIdentifier>();

			if (legalEntity == null || legalEntity.ID == 0)
			{
				CourierList = new List<LegalEntityCourier>();
				CourierListDeleted = new List<LegalEntityCourier>();
				IdentifierList = new List<LegalEntityIdentifier>();
				IdentifierListDeleted = new List<LegalEntityIdentifier>();

				return;
			}

		    var couriers = GetCouriersList(LegalEntity.ID);
            CourierList = couriers.Where(x => !x.IsDeleted).ToList();
			CourierListDeleted = couriers.Where(x => x.IsDeleted).ToList();
			GetContactsList(LegalEntity.ID).ForEach(a=> ContactList.Add(a));
			AllIdentifierList = BLServiceSystem.Client.GetExistingIdentifiers();
		    var list = GetIdentifiersList(LegalEntity.ID);
		    IdentifierList = list.Where(x => !x.IsDeleted).ToList();
		    IdentifierListDeleted = list.Where(x => x.IsDeleted).ToList();
		}

	    public static List<LegalEntityCourier> GetCouriersList(long leid, bool notDeleted = false)
	    {
	        if (leid == 0) return new List<LegalEntityCourier>();
            var xL = DataContainerFacade.GetListByProperty<LegalEntityCourier>("LegalEntityID", leid);

	        var res =  xL.Where(x => x.Type == LegalEntityCourier.Types.Courier).ToList();
	        return notDeleted ? res.Where(x => !x.IsDeleted).ToList() : res;
	    }

	    public static List<LegalEntityCourier> GetContactsList(long leid)
	    {
	        if(leid == 0)  return new List<LegalEntityCourier>();
	        var xL = DataContainerFacade.GetListByProperty<LegalEntityCourier>("LegalEntityID", leid);

	        var contacts = xL.Where(x => x.Type == LegalEntityCourier.Types.Contact).ToList();
	        return contacts.Where(x => !x.IsDeleted).ToList();
	    }


        public static List<LegalEntityIdentifier> GetIdentifiersList(long leid, bool notDeleted = false)
	    {
            if(leid == 0)  return new List<LegalEntityIdentifier>();
	        var xxL = DataContainerFacade.GetListByProperty<LegalEntityIdentifier>("LegalEntityID", leid).OrderByDescending(x => x.SetDate);
	        var identifiers = xxL.ToList();
	        identifiers.ForEach(x => x.AdditionInfo = Guid.NewGuid());
	        return notDeleted ? identifiers.Where(x => !x.IsDeleted).ToList() : identifiers;
	    }


        private void LoadLegalEntityChief(LegalEntity legalEntity)
		{
			ChangedLegalEntityChiefList = new List<LegalEntityChief>();

			if (legalEntity == null || legalEntity.ID == 0)
			{
				LegalEntityChief = new LegalEntityChief();
				OldChiefList = new ObservableList<LegalEntityChief>();
			}
			else
			{
				var xL = DataContainerFacade.GetListByProperty<LegalEntityChief>("LegalEntityID", legalEntity.ID);

				LegalEntityChief = xL.LastOrDefault(x => x.RetireDate == null);
				OldChiefList = new ObservableList<LegalEntityChief>(xL.Where(x => x.RetireDate != null));


				if (LegalEntityChief == null)// LegalEntityChief record doesn't exists, create and fill from LegalEntity
				{
					LegalEntityChief = new LegalEntityChief
					{
						LegalEntityID = legalEntity.ID,
						Position = legalEntity.HeadPosition,
						Comment = legalEntity.Comment
					};
					ApplyFullName(LegalEntityChief, legalEntity.HeadFullName);
				}

				if (OldChiefList == null)
					OldChiefList = new ObservableList<LegalEntityChief>();
			}

			RetireMode = EnRetireMode.CanRetire;
		}


		private void ApplyFullName(LegalEntityChief res, string fullName)
		{
			string fName;
			string lName;
			string pName;

			ApplyFullName(fullName, out fName, out lName, out pName);

			res.FirstName = fName;
			res.LastName = lName;
			res.PatronymicName = pName;
		}

		private void ApplyFullName(string fullName, out string fName, out string lName, out string pName)
		{
			fName = null;
			lName = null;
			pName = null;

			if (string.IsNullOrEmpty(fullName))
				return;

			var sL = fullName.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			if (sL.Length == 0)
				return;

			lName = sL[0];

			if (sL.Length == 1)
				return;

			fName = sL[1];

			if (sL.Length == 2)
				return;

			pName = string.Join(" ", sL.Skip(2).ToArray());
		}

		public void RefreshConnectedLists()
		{
			RefreshListViewModels(new List<Type> { typeof(NPFListViewModel) });
		}

		#region Identifiers

		public LegalEntityIdentifier FocusedIdentifier { get; set; }

		private bool CanExecuteAddIdentifier()
		{
			return EditAccessAllowed;
		}

		private void ExecuteAddIdentifier(object o)
		{
			var x = new LegalEntityIdentifier();

			var idenList = JoinIdentifiers();
			var res = DialogHelper.EditNpfIdentifier(x, true, idenList);
			if (res)
			{
				IdentifierList.Add(x);
				if (!IdentifierListChanged.Contains(x))
					IdentifierListChanged.Add(x);

				IdentifierList = IdentifierList.ToList();//Refresh display table
				OnPropertyChanged("IdentifierList");
				//AllIdentifierList = DataContainerFacade.GetList<LegalEntityIdentifier>();
				AllIdentifierList = BLServiceSystem.Client.GetExistingIdentifiers();
				OnPropertyChanged("AllIdentifierList");
			}

		}

		private List<LegalEntityIdentifier> JoinIdentifiers()
		{
		    var all = AllIdentifierList.ToList();
			all.RemoveAll(x => IdentifierList.Exists(i => x.ID == i.ID));
			all.RemoveAll(x => IdentifierListDeleted.Exists(i => x.ID == i.ID));
			all.RemoveAll(x => IdentifierListChanged.Exists(i => x.ID == i.ID));

			var idList = IdentifierList.Union(IdentifierListChanged).Union(IdentifierListDeleted).Union(all).ToList();
			idList.Where(x => x.AdditionInfo == Guid.Empty).ForEach(x => x.AdditionInfo = Guid.NewGuid());

			return idList;
		}

		private bool CanExecuteDeleteIdentifier()
		{
			return EditAccessAllowed && FocusedIdentifier != null;
		}

	    public static bool CanDeleteIdentifier(string name, long id = 0)
	    {
	        if (id > 0)
	            name = DataContainerFacade.GetByID<LegalEntityIdentifier>(id)?.Identifier;

	        //проверка на наличие идентификатора в БД
	        var count = DataContainerFacade.GetListByPropertyConditionsCount<RejectApplication>(new List<ListPropertyCondition>
	        {
	            ListPropertyCondition.Equal("SelectionIdentifier",name)
	        }) + DataContainerFacade.GetListByPropertyConditionsCount<RejectApplication>(new List<ListPropertyCondition>
	        {
	            ListPropertyCondition.Equal("FromIdentifier",name)
	        });

	        return count == 0;
	    }

		private void ExecuteDeleteIdentifier()
		{
			if (FocusedIdentifier == null)
				return;

			var x = FocusedIdentifier;


			if (!CanDeleteIdentifier(x.Identifier))
			{
				DialogHelper.ShowError("Выбранный идентификатор невозможно удалить, т.к. он используется в системе!");
				return;
			}

			IdentifierList.Remove(x);

			x.MarkDeleted();
			//пропускаем запоминание идентификатора, который еще не был сохранен
			if (x.ID != 0)
			{
				IdentifierListDeleted.Add(x);
				if (!IdentifierListChanged.Contains(x))
					IdentifierListChanged.Add(x);
			}
			else
			{
				if (IdentifierListChanged.Contains(x))
					IdentifierListChanged.Remove(x);
			}
			IdentifierList = IdentifierList.ToList();
			OnPropertyChanged("IdentifierList");
			//AllIdentifierList = DataContainerFacade.GetList<LegalEntityIdentifier>();
			AllIdentifierList = BLServiceSystem.Client.GetExistingIdentifiers();
			OnPropertyChanged("AllIdentifierList");

		}

		private bool CanExecuteEditIdentifier(LegalEntityIdentifier identifier)
		{
			return IdentifierList.Contains(identifier);
		}

		private void ExecuteEditIdentifier(LegalEntityIdentifier x)
		{
			if (x == null)
				return;

			var idenList = JoinIdentifiers();
			var res = DialogHelper.EditNpfIdentifier(x, false, idenList);
			if (res)
			{
				if (!IdentifierListChanged.Contains(x))
					IdentifierListChanged.Add(x);

				IdentifierList = IdentifierList.ToList();//Refresh display table
				OnPropertyChanged("IdentifierList");
				//AllIdentifierList = DataContainerFacade.GetList<LegalEntityIdentifier>();
				AllIdentifierList = BLServiceSystem.Client.GetExistingIdentifiers();
				OnPropertyChanged("AllIdentifierList");

			}
		}






		#endregion

		#region Contacts

		public LegalEntityCourier FocusedContact { get; set; }

		private bool CanExecuteAddContact()
		{
			return EditAccessAllowed;
		}

		private void ExecuteAddContact(object o)
		{
			var contact = new LegalEntityCourier { Type = LegalEntityCourier.Types.Contact };
			if (DialogHelper.EditNpfContact(contact, true))
			{
				ContactList.Add(contact);
			}

		}

		private bool CanExecuteDeleteContact(LegalEntityCourier contact)
		{
			return ContactList.Contains(contact);

		}

		private void ExecuteDeleteContact(LegalEntityCourier contact)
		{
			var msg = string.Format("Удалить контакт \"{0} {1}\"", contact.LastName.Trim(), contact.FirstName.Trim());
			if (DialogHelper.ShowConfirmation(msg))
			{
				ContactList.Remove(contact);
			}
		}

		private bool CanExecuteEditContact(LegalEntityCourier contact)
		{
			return ContactList.Contains(contact);
		}

		private void ExecuteEditContact(LegalEntityCourier contact)
		{
			if (DialogHelper.EditNpfContact(contact, false))
			{

			}

		}

		#endregion

		#region Execute Implementations
		private void CreateNewAccountsForContragent()
		{
			var rubAcc = new Account
			{
				Name = AccountIdentifier.NPF_PERSONAL_ACCOUNT,
				ContragentID = ContragentID,
				AccountTypeID = AccountIdentifier.MeasureIDs.Roubles
			};
			DataContainerFacade.Save(rubAcc);

			var zlAcc = new Account
			{
				Name = AccountIdentifier.NPF_PERSONAL_ACCOUNT,
				ContragentID = ContragentID,
				AccountTypeID = AccountIdentifier.MeasureIDs.ZL
			};
			DataContainerFacade.Save(zlAcc);

			var deadZLAcc = new Account
			{
				Name = AccountIdentifier.NPF_PERSONAL_ACCOUNT,
				ContragentID = ContragentID,
				AccountTypeID = AccountIdentifier.MeasureIDs.DeadZL
			};
			DataContainerFacade.Save(deadZLAcc);
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
					if (_mOriginalFormalizedName != LegalEntity.FormalizedName ||
						_mOriginalFullName != LegalEntity.FullName ||
						_mOriginalShortName != LegalEntity.ShortName)
					{
#if !WEBCLIENT
                        //запрашиваем подтверждение изменения названия
                        if (DialogHelper.ConfirmNPFNameChanged())
						{
#endif
							var oldName = new OldNPFName
							{
								FullName = _mOriginalFullName,
								ShortName = _mOriginalShortName,
								FormalizedName = _mOriginalFormalizedName,
								LegalEntityID = ID,
								Date = DateTime.Now
							};
							Contragent.Name = string.Format("{0} ({1})", FormalizedName, _mOriginalFormalizedName);
							oldName.ID = DataContainerFacade.Save<OldNPFName, long>(oldName);
							RefreshOldNPFNamesList();
#if !WEBCLIENT
						}
						else //отменяем изменения
						{
							LegalEntity.FormalizedName = _mOriginalFormalizedName;
							LegalEntity.FullName = _mOriginalFullName;
							LegalEntity.ShortName = _mOriginalShortName;
							OnPropertyChanged("FullName");
							OnPropertyChanged("ShortName");
							OnPropertyChanged("FormalizedName");
						}
#endif
					}
                    DataContainerFacade.Save(LegalEntity);
					SaveChief();
					SaveCourierList();
					SaveContactList();
					SaveIdentifierList();

					DataContainerFacade.Save(Contragent);
			        if (License != null)
			        {
                        if(License.ID == 0)
                            License.LegalEntityID = CurrentBankAccount.LegalEntityID = LegalEntity.ID = ID;
                        License.ID = DataContainerFacade.Save(License);
			        }
			        //DataContainerFacade.Save<License>(License);
					OnPropertyChanged("IsCardSaved");
					IsDataChanged = false;
					break;
				case ViewModelState.Create:
					Contragent.Name = FormalizedName;
					LegalEntity.ContragentID = Contragent.ID = DataContainerFacade.Save(Contragent);
					ID = DataContainerFacade.Save(LegalEntity);

				    Contragent.LegalEntityID = ID;
				    DataContainerFacade.Save(Contragent);

					License.LegalEntityID = CurrentBankAccount.LegalEntityID = LegalEntity.ID = ID;
					CurrentBankAccount.ID = DataContainerFacade.Save(CurrentBankAccount);
					License.ID = DataContainerFacade.Save(License);

					SaveChief();
					SaveCourierList();
					SaveContactList();
					SaveIdentifierList();

					CreateNewAccountsForContragent();
					OnPropertyChanged("IsCardSaved");
					IsDataChanged = false;
					break;
			}
		}

		private void SaveCourierList()
		{
			foreach (var x in CourierListChanged)
			{
				x.LegalEntityID = ID;
				var cid = DataContainerFacade.Save(x);
				x.ID = cid;

				if (x.LetterOfAttorneyListChanged != null)
					foreach (var y in x.LetterOfAttorneyListChanged)
					{
						y.LegalEntityCourierID = cid;
						y.ID = DataContainerFacade.Save(y);
					}

				if (x.LetterOfAttorneyListDeleted != null)
					x.LetterOfAttorneyListDeleted.ForEach(l => DataContainerFacade.Delete<LegalEntityCourierLetterOfAttorney>(l.ID));
			}
		}

		private void SaveContactList()
		{
			BLServiceSystem.Client.SaveNPFContacts(ID, ContactList);
		}

		private void SaveIdentifierList()
		{
			foreach (var x in IdentifierListChanged)
			{
				x.LegalEntityID = ID;
				if (x.StatusID == -1)
				{
					BLServiceSystem.Client.SaveDeleteEntryLog(x.GetType().Name, "Идентификатор НПФ - " + x.Identifier, x.ID, 1);
				}
				var cid = DataContainerFacade.Save(x);
				x.ID = cid;
			}
		}

		private void SaveChief()
		{
			LegalEntityChief.LegalEntityID = ID;
			LegalEntityChief.ID = DataContainerFacade.Save(LegalEntityChief);
			if (ChangedLegalEntityChiefList != null)
				foreach (var x in ChangedLegalEntityChiefList)
				{
					x.LegalEntityID = ID;
					x.ID = DataContainerFacade.Save(x);
				}
		    if (ChangedLegalEntityChiefList != null) ChangedLegalEntityChiefList.Clear();
		}



		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create && Contragent.StatusID != -1;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			Contragent.StatusID = -1;
			DataContainerFacade.Save<Contragent, long>(Contragent);
			base.ExecuteDelete(delType, Contragent.ID);
		}

		protected override void OnCardSaved()
		{
			base.OnCardSaved();
			_mOriginalFormalizedName = LegalEntity.FormalizedName;
			_mOriginalFullName = LegalEntity.FullName;
			_mOriginalShortName = LegalEntity.ShortName;
		}

		public override bool CanExecuteSave()
		{
			return string.IsNullOrEmpty(Validate()) && IsDataChanged && !EditAccessDenied;
		}

		private void ExecuteAddBranch()
		{
			try
			{
				DialogHelper.AddBranchNPF(ID);
			}
			catch
			{
				RaiseGetDataError();
			}
		}

		private bool CanExecuteAddBranch()
		{
			return (ID > 0 && !EditAccessDenied);
		}

		private void ExecuteEditBranch()
		{
			if (SelectedBranch == null)
				return;

			try
			{
				DialogHelper.EditBranchNPF(ID);
			}
			catch
			{
				RaiseGetDataError();
			}
		}

		private bool CanExecuteEditBranch()
		{
			return (SelectedBranch != null && !EditAccessDenied);
		}

		private void ExecuteDeleteBranch()
		{
			if (SelectedBranch != null)
			{
				DataContainerFacade.Delete(SelectedBranch);
				JournalLogger.LogModelEvent(this, JournalEventType.DELETE, "Филиал", "Удаление филиала " + SelectedBranch.Name);
				RefreshBranchesList();
			}
		}

		public bool CanExecuteDeleteBranch()
		{
			return (SelectedBranch != null && !EditAccessDenied);
		}
#endregion

#region Reorganization delete
		public bool CanExecuteDeleteReorg()
		{
			return SelectedReorg != null && !EditAccessDenied;
		}

		private void ExecuteDeleteReorg()
		{
			if (SelectedReorg != null && !EditAccessDenied)
			{
				if (GetViewModelsList != null)
				{
					var list = GetViewModelsList(typeof(ReorganizationViewModel));
					if (list != null && list.Cast<ReorganizationViewModel>().Where(r => r.ID == SelectedReorg.ID).ToList().Count > 0)
					{
						DialogHelper.ShowAlert("Вы пытаетесь удалить запись, для которой сейчас открыта форма. Перед удалением следует закрыть форму.");
						return;
					}
				}
				var m = new ReorganizationViewModel(SelectedReorg.ID, -1, ViewModelState.Read);
				m.ExecuteDelete();
			}
			RefreshReorganizationsList();
		}
#endregion

		private AsyncValidator LicenseValidator { get; set; }

		protected override bool BeforeExecuteSaveCheck()
		{
			if (!LicenseValidator.Validate())
			{
				DialogHelper.ShowError(LicenseValidator.Error);
				return false;
			}

			AllIdentifierList = BLServiceSystem.Client.GetExistingIdentifiers();

			var match = AllIdentifierList.FirstOrDefault(x => x.LegalEntityID != LegalEntity.ID && IdentifierList.Any(y => y.Identifier == x.Identifier));
			if (match != null)
			{
				DialogHelper.ShowAlert("Идентификатор \"{0}\" уже присутствует в системе.", match.Identifier);
				return false;
			}

			return base.BeforeExecuteSaveCheck();
		}

		public string Validate()
		{
			//registrator|site|registrationnum|
			const string fieldNames = "fullName|shortname|formalizedName|legalAddress|realaddress|postaladdress|phone|fax|email|licnumber|licregdate|licregistrator|headfirstname|headlastname|headpatronymicname|headposition|headaccordingto|headinaugurationdate|headretiredate|comment|currbankacclegalentityname|currbankaccbanklocation|currbankaccbankname|currbankaccaccountnumber|currbankaccbik|currbankaccinn|currbankacckpp|currbankacccorrespondentaccountnumber";
			if (fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])))
			    return "Неверный формат данных";

			if (IsGaranted)
			{
				if (!FundNum.HasValue || FundNum.Value <= 0)
					return "Неверный формат данных";
				if (!FundDate.HasValue || FundDate.Value == DateTime.MinValue)
					return "Неверный формат данных";
			}


			return !LicenseValidator.ValidateIfNot() ? LicenseValidator.Error : null;
		}

		private string ValidateChief()
		{
			const string fieldNames = "headfirstname|headlastname|headpatronymicname|headposition|headaccordingto|headinaugurationdate|headretiredate|comment";
			return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
		}

		public bool IsValidChief => ValidateChief() == null;

	    public override string this[string columnName]
		{
			get
			{
				const string errorMessage = "Неверный формат данных";
				switch (columnName.ToLower())
				{
					//основные данные
					case "fullname": return IsInvalidLength(LegalEntity.FullName, 128) ? errorMessage : null;
					case "shortname": return IsInvalidLength(LegalEntity.ShortName, 128) ? errorMessage : null;
					case "formalizedname": return IsInvalidLength(LegalEntity.FormalizedName, 128) ? errorMessage : null;
					//адрес
					case "legaladdress": return IsInvalidLength(LegalAddress, 128) ? errorMessage : null;
					case "realaddress": return IsInvalidLength(RealAddress, 128) ? errorMessage : null;
					case "postaladdress": return IsInvalidLength(PostalAddress, 128) ? errorMessage : null;
					case "phone": return IsInvalidLength(Phone, 128) ? errorMessage : null;
					case "fax": return IsInvalidLength(Fax, 128) ? errorMessage : null;
					case "email": return IsInvalidLength(EMail, 128) || !EMail.Trim().Split(',', ';').All(mail => DataTools.IsEmail(mail.Trim())) ? errorMessage : null;
					//Лицензии
					case "licnumber":
						return IsInvalidLength(LicNumber, 50) ? errorMessage : LicenseValidator.Error;
					case "licregdate": return LicRegDate == null || LicRegDate == DateTime.MinValue ? errorMessage : null;
					case "licregistrator": return IsInvalidLength(LicRegistrator, 128) ? errorMessage : null;

					//case "registrationnum": return IsInvalidLength(RegistrationNumber, 50) ? errorMessage : null;
					//case "registrator": return IsInvalidLength(Registrator, 512) ? errorMessage : null;
					//case "site": return IsInvalidLength(Site, 128) ? errorMessage : null;

					//Руководство
					case "headfirstname": return IsInvalidLength(HeadFirstName, 100) ? errorMessage : null;
					case "headlastname": return IsInvalidLength(HeadLastName, 100) ? errorMessage : null;
					case "headpatronymicname":
						if (HeadPatronymicName != null && HeadPatronymicName.Length > 100)
							return errorMessage;
						break;
					case "headposition": return IsInvalidLength(HeadPosition, 128) ? errorMessage : null;
					case "headaccordingto": return IsInvalidLength(HeadAccordingTo, 300) ? errorMessage : null;
					case "headinaugurationdate":
						if (HeadInaugurationDate == null || HeadInaugurationDate == DateTime.MinValue)
							return errorMessage;
						if (HeadRetireDate != null && HeadRetireDate < HeadInaugurationDate)
							return errorMessage;

						if (HeadInaugurationDate < GetMaxHeadDate())
							return "Дата вступления в должность не может быть раньше даты прекращения полномочий предыдущего руководства";
						break;
					case "headretiredate":
						if (HeadInaugurationDate != null && HeadRetireDate != null && HeadRetireDate < HeadInaugurationDate)
							return errorMessage;
						break;
					case "comment":
						if (Comment != null && Comment.Length > 512)
							return errorMessage;
						break;

					//Банковские реквизиты
					case "inn": return !(INN != null && INN.Length == 10 && DataTools.IsNumericString(INN)) ? errorMessage : null;
					case "okpp": return !(OKPP != null && OKPP.Length == 9 && DataTools.IsNumericString(OKPP)) ? errorMessage : null;

					case "currbankacclegalentityname": return IsInvalidLength(CurrBankAccLegalEntityName, 512) ? errorMessage : null;
					case "currbankaccbanklocation": return IsInvalidLength(CurrBankAccBankLocation, 512) ? errorMessage : null;
					case "currbankaccbankname": return IsInvalidLength(CurrBankAccBankName, 512) ? errorMessage : null;
					case "currbankaccaccountnumber":
						return !(CurrBankAccAccountNumber != null && CurrBankAccAccountNumber.Trim().Length == 20 && DataTools.IsNumericString(CurrBankAccAccountNumber.Trim())) ? errorMessage : null;
					case "currbankaccbik":
						return !(CurrBankAccBIK != null && CurrBankAccBIK.Trim().Length == 9 && DataTools.IsNumericString(CurrBankAccBIK.Trim())) ? errorMessage : null;
					case "currbankaccinn":
						return CurrBankAccINN == null || (CurrBankAccINN != null && !(CurrBankAccINN.Length == 10 && DataTools.IsNumericString(CurrBankAccINN))) ? errorMessage : null;
					case "currbankacckpp":
						return CurrBankAccKPP == null || (CurrBankAccKPP != null && !(CurrBankAccKPP.Length == 9 && DataTools.IsNumericString(CurrBankAccKPP))) ? errorMessage : null;
					case "currbankacccorrespondentaccountnumber":
						return IsCardSaved ? null :
							!(CurrBankAccCorrespondentAccountNumber != null && CurrBankAccCorrespondentAccountNumber.Trim().Length == 20 && DataTools.IsNumericString(CurrBankAccCorrespondentAccountNumber.Trim())) ? errorMessage : null;

					case "fundnum":
						return (IsGaranted && (!FundNum.HasValue || FundNum.Value <= 0)) ? errorMessage : null;
					case "funddate":
						return (IsGaranted && (!FundDate.HasValue || FundDate.Value == DateTime.MinValue)) ? errorMessage : null;
					default: return null;
				}
				return null;
			}
		}

		private DateTime? GetMaxHeadDate()
		{
			var d = DateTime.MinValue;
			foreach (var x in OldChiefList)
			{
				if (x.InaugurationDate.HasValue && d < x.InaugurationDate)
					d = x.InaugurationDate.Value;

				if (x.RetireDate.HasValue && d < x.RetireDate.Value)
					d = x.RetireDate.Value;
			}
			return d;
		}

		private static bool IsInvalidLength(string value, int maxLength)
		{
			return string.IsNullOrEmpty(value) || value.Length > maxLength;
		}

		/*private string CheckActivity(DateTime start, DateTime end)
		{
			var now = DateTime.Now.Date;
			return start > now || end < now
				? StatusIdentifier.S_ACTIVE
				: StatusIdentifier.S_SUSPENDED;
		}*/

#region 3rd code
		public bool SuspendActivity()
		{
			if (DialogHelper.SuspendActivity(ContragentID))
			{
				var isDataChanged = IsDataChanged;
				if (LegalEntity != null)
				{
					GarantACBID = (long)LegalEntity.GarantACB.NotIncluded;
					DataContainerFacade.Save<LegalEntity, long>(LegalEntity);
				}
				RefreshNPFPauseList();
				RefreshStatus();
				IsDataChanged = isDataChanged;
			    return true;
			}
		    return false;
		}

		public bool CanExecuteAnnulLicence()
		{
			return (StatusIdentifier.IsNPFStatusActivityCarries(NPFStatus) || StatusIdentifier.IsNPFStatusActivitySuspended(NPFStatus)) && !EditAccessDenied;
		}

		public void ExecuteAnnulLicence()
		{
			var res = DialogHelper.AnnulLicence();
			if (res.HasValue)
			{
				License.CloseDate = res.Value;
				IsDataChanged = true;
				RefreshStatus();
				OnPropertyChanged("LicCloseDate");
			}
		}

		public bool CanExecuteCancelAnnulLicence()
		{
			return LicCloseDate != null && !EditAccessDenied;
		}

		public void ExecuteCancelAnnulLicence()
		{
			License.CloseDate = null;
			IsDataChanged = true;
			OnPropertyChanged("LicCloseDate");

			RefreshStatus();
		}

		public bool CanAnnulCloseDate()
		{
			return CloseDate != null && !EditAccessDenied;
		}

		public void ExecuteAnnulCloseDate()
		{
			CloseDate = null;
		}

		public bool CanEditAccount => State != ViewModelState.Create && !EditAccessDenied;

	    public string EditAccountProhibitionReason => "Невозможно редактировать расчетный счет в режиме создания";

	    public bool CanExecuteRetireChief()
		{
			return State != ViewModelState.Create && !EditAccessDenied && RetireMode == EnRetireMode.CanRetire && IsValidChief;
		}

		public void ExecuteRetireChief()
		{
			var res = DialogHelper.RetireChief(HeadInaugurationDate);
			if (res.HasValue)
			{
				DoRetireChief(res.Value);
			}
		}

		public void DoRetireChief(DateTime retireDate)
		{
			HeadRetireDate = retireDate;

			var old = LegalEntityChief;


			LegalEntityChief = new LegalEntityChief { LegalEntityID = old.LegalEntityID };
			IsDataChanged = true;
			LegalEntity.HeadFullName = null;
			LegalEntity.HeadPosition = null;
			LegalEntity.Comment = null;

			//OldLegalEntityChiefList.Add(LegalEntityChief);
			ChangedLegalEntityChiefList.Add(old);
			OldChiefList.Add(old);
			//OldChiefList = new ObservableList<DataObjects.LegalEntityChief>(OldChiefList);//Need to refresh list

			RetireMode = EnRetireMode.CanRetire;

			RefreshStatus();

			OnPropertyChanged("HeadFirstName");
			OnPropertyChanged("HeadLastName");
			OnPropertyChanged("HeadPatronymicName");

			OnPropertyChanged("HeadPosition");
			OnPropertyChanged("HeadAccordingTo");

			OnPropertyChanged("HeadWorkPhone");
			OnPropertyChanged("HeadWorkPhoneExtension");
			OnPropertyChanged("HeadMobilePhone");
			OnPropertyChanged("HeadEmail");

			OnPropertyChanged("HeadInaugurationDate");
			OnPropertyChanged("HeadRetireDate");
			OnPropertyChanged("Comment");

			OnPropertyChanged("OldChiefList");
		}


		public enum EnRetireMode
		{
			CanRetire = 0,
			Retired = 1,
			NoRetire = 2
		}

		private EnRetireMode _retireMode;
		public EnRetireMode RetireMode
		{
			get
			{
				return _retireMode;
			}
			set
			{
				_retireMode = value;
				OnPropertyChanged("RetireMode");
			}
		}

		public void DeleteFromDataBase()
		{
			long licenseId = -1, bankAccountId = -1;
			if (License != null)
			{
				licenseId = License.ID;
				License = null;
			}
			if (CurrentBankAccount != null)
			{
				bankAccountId = CurrentBankAccount.ID;
				CurrentBankAccount = null;
			}
			ExecuteSave();
			if (licenseId > 0)
				DataContainerFacade.Delete<License>(licenseId);
			if (bankAccountId > 0)
				DataContainerFacade.Delete<BankAccount>(bankAccountId);

			DataContainerFacade.Delete<LegalEntity>(ID);
		}
#endregion

#region Execute Courier

		private bool CanExecuteAddCourier()
		{
			return EditAccessAllowed;
		}

		private void ExecuteAddCourier()
		{
			var x = new LegalEntityCourier { Type = LegalEntityCourier.Types.Courier };
			var res = DialogHelper.EditNpfCourier(x, true);
			if (res)
			{
				CourierList.Add(x);
				if (!CourierListChanged.Contains(x))
					CourierListChanged.Add(x);

				CourierList = CourierList.ToList();//Refresh display table
				OnPropertyChanged("CourierList");
			}
		}

		private bool CanExecuteDeleteCourier()
		{
			//If have selected item in grid
			return EditAccessAllowed && FocusedCourier != null;
		}

		private void ExecuteDeleteCourier()
		{
			if (FocusedCourier == null)
				return;

			var x = FocusedCourier;

			CourierList.Remove(x);

			x.MarkDeleted();

			CourierListDeleted.Add(x);
			if (!CourierListChanged.Contains(x))
				CourierListChanged.Add(x);

			CourierList = CourierList.ToList();
			OnPropertyChanged("CourierList");
		}


		private bool CanExecuteShowDeletedCourierList()
		{
			return CourierListDeleted.Count > 0;
		}

		private void ExecuteShowDeletedCourierList()
		{
			DialogHelper.ShowNpfCourierDeletedList(CourierListDeleted);
		}

		public void EditCourier(LegalEntityCourier x)
		{
			if (x == null)
				return;

			var res = DialogHelper.EditNpfCourier(x, false);
			if (res)
			{
				if (!CourierListChanged.Contains(x))
					CourierListChanged.Add(x);

				CourierList = CourierList.ToList();//Refresh display table
				OnPropertyChanged("CourierList");
			}
		}

	    public LegalEntityCourier FocusedCourier { get; set; }

#endregion


		/// <summary>
		/// Determines is model data have ever been saved to database
		/// </summary>
		/// <returns></returns>
		public bool IsInDatabase()
		{
			return LegalEntity != null && LegalEntity.ID != 0;
		}

		public class LegalEntityChiefParamContainer
		{
			public LegalEntityChief LegalEntityChief { get; set; }
			public NPFViewModel Model { get; set; }
		}


		internal bool IsValidChiefValue(LegalEntityChief newValue, LegalEntityChief currentValue)
		{
		    return HeadInaugurationDate == null || newValue.RetireDate == null || newValue.RetireDate.Value <= HeadInaugurationDate.Value;
		}

	    public bool IsChiefChanged(LegalEntityChief x)
		{
			return ChangedLegalEntityChiefList.Contains(x);
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(LegalEntity) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long iId)
		{
			if (type == typeof(LegalEntity) && iId == LegalEntity.ID) 
			{
				//Пока реагируем только на обновление счетов
				RefreshBankAccountsList();
			}
		}

	}


}