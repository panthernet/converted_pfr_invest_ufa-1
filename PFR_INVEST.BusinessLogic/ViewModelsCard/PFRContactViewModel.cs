﻿using System;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class PFRContactViewModel : ViewModelCard
    {
        private PFRContact _contact = new PFRContact();
        public PFRContact Contact
        {
            get { return _contact; }
            set { _contact = value; OnPropertyChanged("Contact"); }
        }

        public string FIO
        {
            get { return _contact.FIO; }
            set { _contact.FIO = value; OnPropertyChanged("FIO"); }
        }
        public string Post
        {
            get { return _contact.Post; }
            set { _contact.Post = value; OnPropertyChanged("Post"); }
        }
        public string Phone
        {
            get { return _contact.Phone; }
            set { _contact.Phone = value; OnPropertyChanged("Phone"); }
        }
        public string Fax
        {
            get { return Contact.Fax; }
            set { Contact.Fax = value; OnPropertyChanged("Fax"); }
        }
        public string Email
        {
            get { return _contact.Email; }
            set { _contact.Email = value; OnPropertyChanged("Email"); }
        }


        private long _branchID;

        public PFRContactViewModel(long id, ViewModelState action)
            : base(typeof(PFRBranchesListViewModel))
        {
            DataObjectTypeForJournal = typeof(PFRContact);
            ID = id;
            if (action == ViewModelState.Create)
            {
                //this.Fields = BLServiceSystem.Client.GetPFRContactStructure();
                //this.Fields["PFRB_ID"].Value = id;
                Contact.ID = id;
            }
            else
            {
                //this.Fields = BLServiceSystem.Client.GetPFRContact(id);
                Contact = DataContainerFacade.GetByID<PFRContact>(id);
            }
        }

        public void Load(long branchid)
        {
            _branchID = branchid;
        }

        #region Execute implementation
        public override bool CanExecuteSave()
        {
            return String.IsNullOrEmpty(Validate()) && IsDataChanged;
        }


        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                    case ViewModelState.Create:
                        if (State == ViewModelState.Create)
                            Contact.PFRB_ID = _branchID;
                        ID = Contact.ID = DataContainerFacade.Save(Contact);
                        if (GetViewModel != null)
                        {
                            var vm = GetViewModelsList(typeof(PFRBranchViewModel)).OfType<PFRBranchViewModel>().FirstOrDefault(a=> a.ID == _branchID);
                            if (vm != null)
                                vm.RefreshLists(true);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public override bool CanExecuteDelete()
        {
            return Contact.ID > 0 && DeleteAccessAllowed;
        }

        protected override void ExecuteDelete(int delType)
        {
            if (Contact.ID > 0)
            {
                DataContainerFacade.Delete(Contact);
                if (GetViewModel != null)
                {
                    var vm = GetViewModel(typeof(PFRBranchViewModel)) as PFRBranchViewModel;
                    if (vm != null)
                        vm.RefreshLists(true);
                }
            }
            base.ExecuteDelete(DocOperation.Archive);
        }

        public void ExecuteDelete()
        {
            ExecuteDelete(DocOperation.Archive);
        }


        #endregion

        private static readonly string[] contactPropertyNamesToValidate = { "FIO", "POST", "PHONE", "FAX", "EMAIL" };

        public override string this[string propertyName]
        {
            get
            {
                switch (propertyName.ToLower())
                {
                    case "fio":
                        return DataTools.ValidateNotNullOrWhitespace(Contact.FIO) ?? DataTools.ValidateLength(Contact.FIO, 512);
                    case "post":
                        return DataTools.ValidateNotNullOrWhitespace(Contact.Post) ?? DataTools.ValidateLength(Contact.Post, 512);
                    case "phone":
                        return DataTools.ValidateNotNullOrWhitespace(Contact.Phone) ?? DataTools.ValidateLength(Contact.Phone, 32);
                    case "fax":
                        return DataTools.ValidateNotNullOrWhitespace(Contact.Fax) ?? DataTools.ValidateLength(Contact.Fax, 32);
                    case "email":
                        return DataTools.ValidateNotNullOrWhitespace(Contact.Email) ?? DataTools.ValidateLength(Contact.Email, 128) ?? DataTools.ValidateEmail(Contact.Email);
                    default:
                        return null;
                }
            }
        }

        private string Validate()
        {
            return contactPropertyNamesToValidate.Select(x => this[x]).FirstOrDefault(x => x != null);
        }

        public override string GetDocumentHeader()
        {
            return "Контакт ПФР";
        }
    }
}
