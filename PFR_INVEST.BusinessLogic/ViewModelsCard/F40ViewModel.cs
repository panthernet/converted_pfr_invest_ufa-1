﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F40ViewModel : ViewModelCard
    {
        /// <summary>
        /// Поле хранения данных для старой ODBC системы
        /// </summary>
         public Dictionary<string, DBField> Fields { get; set; }

        #region Fields

        private IList<F040Detail> infoList = new List<F040Detail>();
        public IList<F040Detail> InfoList
        {
            get { return infoList; }
            set { infoList = value; OnPropertyChanged("InfoList"); }
        }

        private IList<F040Rrz> rrzList = new List<F040Rrz>();
        public IList<F040Rrz> RRZList
        {
            get { return rrzList; }
            set { rrzList = value; OnPropertyChanged("RRZList"); }
        }

        public DateTime? RegDate
        {
            get { return DBFieldHelper.GetDate("EDO_ODKF040.REGDATE", Fields); }
            set
            {
                DBFieldHelper.SetValue("EDO_ODKF040.REGDATE", value, Fields);
                OnPropertyChanged("RegDate");
            }
        }

        public DateTime? WriteDate
        {
            get
            {
                return DBFieldHelper.GetDate("EDO_ODKF040.WRITEDATE", Fields);
            }
            set
            {
                DBFieldHelper.SetValue("EDO_ODKF040.WRITEDATE", value, Fields);
                OnPropertyChanged("WriteDate");
            }
        }

        public string ReportDate
        {
            get
            {
                string Year = DBFieldHelper.GetValue("YEARS.NAME", Fields) as string;
                string Month = DBFieldHelper.GetValue("MONTHS.NAME", Fields) as string;
                return string.Format("{0} {1}", Month, Year);
            }
            set { }
        }

        public string Portfolio
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Fields["EDO_ODKF040.PORTFOLIO"].Value as string))
                    return Fields["EDO_ODKF040.PORTFOLIO"].Value as string;
                return Fields["CONTRACT.PORTFNAME"].Value as string;
            }
        }

        #endregion

        public F40ViewModel(long id)
        {
            //DataObjectTypeForJournal = typeof(EdoOdkF020);
            ID = id;
            Fields = BLServiceSystem.Client.GetF40(id);
            InfoList = BLServiceSystem.Client.GetF040Details(id);
            RRZList = BLServiceSystem.Client.GetF040Rrzs(id);
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        protected override void ExecuteSave()
        {
        }

        public override string this[string columnName] => "";
    }
}
