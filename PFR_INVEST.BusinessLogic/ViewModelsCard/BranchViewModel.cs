﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class BranchViewModel : ViewModelCard, IUpdateRaisingModel
    {

        public override string GetDocumentHeader()
        {
            return "Филиал";
        }

        #region Fields

        protected readonly Branch Branch;

        [Display(Name = "Наименование")]
        [Required]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchName
        {
            get 
            { 
                return Branch?.Name;
            }
            set 
            { 
                if (Branch != null)
                    Branch.Name = value;
                OnPropertyChanged("BranchName"); 
            }
        }

        [Display(Name = "Фактический адрес")]
        [Required]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchAddress
        {
            get { return Branch?.Address; }
            set 
            { 
                if (Branch != null)
                    Branch.Address = value;
                OnPropertyChanged("BranchAddress"); 
            }
        }

        [Display(Name = "Почтовый адрес")]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchPosAddress
        {
            get 
            {
                return Branch?.PostAddress;
            }
            set 
            { 
                if (Branch != null)
                    Branch.PostAddress = value;
                OnPropertyChanged("BranchPosAddress"); 
            }
        }

        [Display(Name = "Телефон")]
        [StringLength(20, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchPhone
        {
            get 
            {
                return Branch?.Phone;
            }
            set 
            { 
                if (Branch != null)
                    Branch.Phone = value;
                OnPropertyChanged("BranchPhone"); 
            }
        }
        #endregion

        #region Execute Implementations

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType = 1)
        {
            DataContainerFacade.Delete(Branch);
            base.ExecuteDelete(delType);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;

                case ViewModelState.Create:
                case ViewModelState.Edit:
                    if ((_parentVm is SIViewModel) || (_parentVm is NPFViewModel))
                    {
                        var siVm = _parentVm as SIViewModel;
                        if (siVm != null)
                            Branch.LegalEntityID = siVm.LegalEntity.ID;

                        if (_parentVm is NPFViewModel)
                            Branch.LegalEntityID = _parentVm.ID;

                        Branch.ID = ID = DataContainerFacade.Save(Branch);

                        if (siVm != null)
                        {
                            var newList = ((SIViewModel) _parentVm).BranchesList.Select(ent => ent.ID == InternalID ? Branch : ent).ToList();
                            ((SIViewModel) _parentVm).BranchesList = newList;
                        }

                        if (_parentVm is NPFViewModel)
                            ((NPFViewModel) _parentVm).RefreshBranchesList();
                    }
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }
        #endregion

        private readonly ViewModelCard _parentVm;

        public BranchViewModel(ViewModelCard vm)
        {
            Branch = new Branch();
            DataObjectTypeForJournal = typeof(Branch);    

            _parentVm = vm;
        }

        public BranchViewModel(ViewModelCard vm, long branchID)
        {
            Branch = DataContainerFacade.GetByID<Branch>(branchID) ?? new Branch();
            ID = branchID;
            _parentVm = vm;
        }

        private const string NOT_NULL_FIELDS = "branchname|branchaddress|branchphone";
        private const string S_ERROR = "Неверный формат данных";

        public override string this[string columnName]
        {
            get
            {
                if (Branch == null)
                    return S_ERROR;

                switch (columnName.ToLower())
                {
                    case "branchname":
                        return !string.IsNullOrWhiteSpace(Branch.Name) ? Branch.Name.ValidateMaxLength(512) : S_ERROR;
                    case "branchaddress":
                        return !string.IsNullOrWhiteSpace(Branch.Address) ? Branch.Address.ValidateMaxLength(512) : S_ERROR;
                    case "branchphone":
                        return string.IsNullOrWhiteSpace(Branch.Phone) || (!string.IsNullOrEmpty(Branch.Phone) && Branch.Phone.Length <= 20) ? null : "Длина строки больше 20 символов";
                    case "branchposaddress":
                        return Branch.PostAddress.ValidateMaxLength(512);
                    default:
                        return null;
                }
            }
        }

        protected string Validate()
        {
            if (Branch == null)
                return S_ERROR;

            return NOT_NULL_FIELDS.Split('|').Any(fld => !string.IsNullOrEmpty(this[fld])) ? S_ERROR : null;
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Branch), ID) };

        }
    }
}