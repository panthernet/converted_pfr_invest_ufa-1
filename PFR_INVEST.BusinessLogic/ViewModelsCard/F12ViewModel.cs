﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [Obsolete("Устаревший, использовать F15ViewModel", false)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class F12ViewModel : ViewModelCard
    {
        #region Fields

        private List<NetWealthsInnerListItem> netWealthsList;
        public List<NetWealthsInnerListItem> NetWealthsList
        {
            get
            {
                return netWealthsList;
            }

            set
            {
                netWealthsList = value;
                OnPropertyChanged("NetWealthsList");
            }
        }

        private EdoOdkF012 odk;
        public EdoOdkF012 Odk
        {
            get { return odk; }
            set
            {
                odk = value;
                OnPropertyChanged("Odk");
            }
        }

        private Contract contract;
        public Contract Contract
        {
            get { return contract; }
            set
            {
                contract = value;
                OnPropertyChanged("Contract");
            }
        }

        private LegalEntity legalEntity;
        public LegalEntity LegalEntity
        {
            get { return legalEntity; }
            set
            {
                legalEntity = value;
                OnPropertyChanged("LegalEntity");
            }
        }

        #endregion

        #region Execute implementation

        public override bool CanExecuteSave() { return false; }

        protected override void ExecuteSave() { }
        #endregion

        #region Validation
        public override string this[string columnName] => "";

        #endregion

        public F12ViewModel(long id)
            : base(typeof(NetWealthsListViewModel))
        {
            DataObjectTypeForJournal = typeof(EdoOdkF012);
            if (id > 0)
            {
                ID = id;
                Odk = DataContainerFacade.GetByID<EdoOdkF012, long>(id);
                Contract = Odk.GetContract();
                LegalEntity = Contract.GetLegalEntity();
                RefreshNetWealthsList();
            }
        }

        private void RefreshNetWealthsList()
        {
            List<NetWealthsInnerListItem> tmpList = new List<NetWealthsInnerListItem>();

            tmpList.Add(new NetWealthsInnerListItem("1. Активы, денежные средства (010):", Odk.A010, null));
            tmpList.Add(new NetWealthsInnerListItem("2. Активы, депозиты (020):", Odk.A020, null));
            tmpList.Add(new NetWealthsInnerListItem("3. Ценные бумаги (030):", Odk.A030, null));
            tmpList.Add(new NetWealthsInnerListItem("3.1. Государственные ценные бумаги (031):", Odk.A031, null));
            tmpList.Add(new NetWealthsInnerListItem("3.2. Государственные ценные бумаги субъектов РФ (032):", Odk.A032, null));
            tmpList.Add(new NetWealthsInnerListItem("3.3. Облигации муниципальных образований (033):", Odk.A033, null));
            tmpList.Add(new NetWealthsInnerListItem("3.4. Облигации российских хозяйсвтенных обществ (034):", Odk.A034, null));
            tmpList.Add(new NetWealthsInnerListItem("3.5. Акции российских эмитентов в форме (035):", Odk.A035, null));
            tmpList.Add(new NetWealthsInnerListItem("3.6. Паи индексных инвестиционных фондов (036):", Odk.A036, null));
            tmpList.Add(new NetWealthsInnerListItem("3.7. Ипотечные ценные бумаги (037):", Odk.A037, null));
            tmpList.Add(new NetWealthsInnerListItem("4. Дебиторская задолженность (040):", Odk.A40, null));
            tmpList.Add(new NetWealthsInnerListItem("4.1. Средства предоставленные профессиональным участникам (041):", Odk.A41, null));
            tmpList.Add(new NetWealthsInnerListItem("4.2. Дебиторская задолженность по процентному доходу (042):", Odk.A42, null));
            tmpList.Add(new NetWealthsInnerListItem("4.3. Прочая дебиторская задолженность (043):", Odk.A43, null));
            tmpList.Add(new NetWealthsInnerListItem("5. Прочие активы (050):", Odk.A50, null));
            tmpList.Add(new NetWealthsInnerListItem("6. Итого ИМУЩЕСТВА (060):", Odk.A60, null));
            tmpList.Add(new NetWealthsInnerListItem("7. Кредиторская задолженность (070):", Odk.A70, null));
            tmpList.Add(new NetWealthsInnerListItem("7.1. Обязательства перед проф. участниками (071):", Odk.A071, null));
            tmpList.Add(new NetWealthsInnerListItem("7.2. Кредиторская задолженность по возмещению расходов (072):", Odk.A072, null));
            tmpList.Add(new NetWealthsInnerListItem("7.3. Прочая кредиторская задолженность (073):", Odk.A073, null));
            tmpList.Add(new NetWealthsInnerListItem("8. Итого сумма обязательств (080):", Odk.A080, null));
            tmpList.Add(new NetWealthsInnerListItem("9. Итого стоимость чистых активов (090):", Odk.A090, null));

            NetWealthsList = tmpList;
        }
    }
}
