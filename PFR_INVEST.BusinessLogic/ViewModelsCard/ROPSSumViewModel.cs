﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class ROPSSumViewModel : ViewModelCard, IUpdateRaisingModel
	{
		public ROPSSum ROPS { get; private set; }

		public DateTime DirectionDate
		{
			get { return ROPS.DirectionDate; }
			set
			{
				if (ROPS.DirectionDate != value) { ROPS.DirectionDate = value; OnPropertyChanged("DirectionDate"); }
			}
		}

		public string DirectionNum
		{
			get { return ROPS.DirectionNum; }
			set
			{
				if (ROPS.DirectionNum != value) { ROPS.DirectionNum = value; OnPropertyChanged("DirectionNum"); }
			}
		}

		public DateTime Date
		{
			get { return ROPS.Date; }
			set
			{
				if (ROPS.Date != value) { ROPS.Date = value; OnPropertyChanged("Date"); }
			}
		}

		public decimal Sum
		{
			get { return ROPS.Sum; }
			set
			{
				if (ROPS.Sum != value) { ROPS.Sum = value; OnPropertyChanged("Sum"); }
			}
		}

		public ROPSSumViewModel(ViewModelState action, long id)
		{
			if (action == ViewModelState.Create)
			{
				ROPS = new ROPSSum { DirectionDate = DateTime.Now, Date = DateTime.Now };
			}
			else
				Load(id);
						
		}

		public void Load(long lId)
		{
			ID = lId;
			ROPS = DataContainerFacade.GetByID<ROPSSum>(lId);
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			var exists = DataContainerFacade.GetListByProperty<ROPSSum>("Date", Date).Where(r => r.ID != ROPS.ID && !r.IsDeleted()).FirstOrDefault();
			if (exists != null)
			{
				DialogHelper.ShowError(string.Format("На дату {0} уже существует распоряжения РОПС.", ROPS.Date.ToShortDateString()));
				return false;
			}
		    return true;
		}

		public override bool CanExecuteSave()
		{
			return IsDataChanged && ValidateFields();
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Create:
				case ViewModelState.Edit:
					ID = ROPS.ID = DataContainerFacade.Save(ROPS);

					break;
			}
		}

		public override bool CanExecuteDelete()
		{
			return ROPS.ID > 0;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			DataContainerFacade.Delete(ROPS);
			base.ExecuteDelete(delType);
			
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "DirectionDate": return DirectionDate.ValidateRequired();
					case "DirectionNum": return DirectionNum.ValidateNonEmpty() ?? DirectionNum.ValidateMaxLength(25);
					case "Date": return Date.ValidateRequired();
					case "Sum": return Sum.ValidateRequired() ?? Sum.ValidateNonNegative() ?? Sum.ValidateMaxFormat();
					default: return null;
				}
			}
		}



		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(ROPSSum), ROPS.ID) };
		}


	}
}
