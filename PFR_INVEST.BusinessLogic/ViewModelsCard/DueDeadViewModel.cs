﻿//using System.Windows.Input;
//using PFR_INVEST.Core.BusinessLogic;
//using PFR_INVEST.DataObjects.ListItems;
//using PFR_INVEST.Proxy;

using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class DueDeadViewModel : ApsModelBase
	{
		/// <summary>
		/// Конструктор
		/// </summary>
        public DueDeadViewModel(PortfolioIdentifier.PortfolioTypes p_Type)
			: base(//typeof(DueListViewModel), 
            p_Type)
		{
            APS.KindID = 2; // потому что страховые взносы умерших
		}

	}
}
