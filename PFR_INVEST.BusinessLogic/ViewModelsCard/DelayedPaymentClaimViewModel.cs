﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class DelayedPaymentClaimViewModel : ViewModelCard
    {
        public DelayedPaymentClaim Claim { get; private set; }
        public IList<Element> FZList { get; private set; }
        public IList<SimpleObjectValue> NpfList { get; private set; }

        public bool IsReadOnly => State == ViewModelState.Read;

        public DelayedPaymentClaimViewModel() : this(0, ViewModelState.Create) { }

        public DelayedPaymentClaimViewModel(long id, ViewModelState action)
            : base(typeof(DelayedPaymentClaimListViewModel))
        {
            DataObjectTypeForJournal = typeof(DelayedPaymentClaim);
            ID = id;
            State = action;
            NpfList = BLServiceSystem.Client.GetActiveLegalEntityByType(Contragent.Types.Npf);

            if (action == ViewModelState.Create)
            {
                Claim = new DelayedPaymentClaim();
                Claim.UFODate = DateTime.Now;
            }
            else
            {
                Claim = DataContainerFacade.GetByID<DelayedPaymentClaim>(id);
            }

            FZList = BLServiceSystem.Client.GetElementByType(Element.Types.FZ).Where(x=>x.Visible || x.ID == Claim.FZID).ToList();

            //Из за бага DevExpress 13 высталяем значение не null
            //if (!this.Claim.ZLCount.HasValue)
            //    this.Claim.ZLCount = 0;

            Claim.PropertyChanged += (sender, e) => 
            { 
                IsDataChanged = true;
                OnPropertyChanged("NpfInn");
            };
        }


        public string NpfInn 
        {
            get 
            {
                if (Claim.NpfID.HasValue)
                {
                    var npf = NpfList.FirstOrDefault(n => n.ID == Claim.NpfID);
                    if (npf != null && npf.ExtensionData.ContainsKey("INN")) 
                    {
                        return npf.ExtensionData["INN"] as string;
                    }
                }
                
                return string.Empty;
            }
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && DeleteAccessAllowed;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(Claim);
            base.ExecuteDelete(DocOperation.Delete);
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && Claim.Validate();
        }


        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Edit:
                        DataContainerFacade.Save(Claim);
                        break;
                    case ViewModelState.Create:
                        ID = Claim.ID = DataContainerFacade.Save(Claim);
                        break;
                    case ViewModelState.Read:
                    default:
                        break;
                }
            }
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    default: return string.Empty;
                }
            }
        }







    }
}
