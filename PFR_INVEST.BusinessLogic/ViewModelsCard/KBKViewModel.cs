﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class KBKViewModel : ViewModelCard, IUpdateRaisingModel
	{
        #region Fields

	    private string _oldNumber;

        public IList<KBKHistory> KBKHistoryList { get; set; }

		public KBK KBK
		{
			get;
			private set;
		}

		private DateTime changeDate = DateTime.Now;
		public DateTime ChangeDate
		{
			get { return changeDate; }
			set { if (changeDate != value) { changeDate = value; OnPropertyChanged("ChangeDate"); } }
		}

		//для отслеживания изменения названий
		private string m_oldCode;
		private string m_oldName;

		#endregion

		#region Execute Implementations

		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create && !IsBlockedByData;
		}

		protected override void ExecuteDelete(int delType)
		{
			DataContainerFacade.Delete(KBK);
			base.ExecuteDelete(DocOperation.Archive);

		}

	    public override bool BeforeExecuteDeleteCheck()
	    {
	        if (!GetRefreshedBlockInfo()) return true;
	        DialogHelper.ShowError("Выбранный КБК используется в записях! Удаление невозможно");
	        return false;
	    }

	    protected override bool BeforeExecuteSaveCheck()
		{
		    if (State == ViewModelState.Edit && KBK.Code != _oldNumber && GetRefreshedBlockInfo())
		    {
		        KBK.Code = _oldNumber;
		        DialogHelper.ShowError("Невозможно сохранить КБК, т.к. был изменен его код и КБК уже используется в записях!\r\nРедактирование кода КБК будет заблокировано и будет возвращено предыдущее значение кода.");
		        return false;
		    }

			var existList = DataContainerFacade.GetListByProperty<KBK>("Code", KBK.Code)
							.Where(x => x.ID != KBK.ID).ToList();
		    if (!existList.Any()) return true;
		    DialogHelper.ShowError(existList.First().StatusID == -1 ? 
		        "Указанный КБК ранее уже был добавлен в систему, но сейчас находится в статусе \"Удален\".\r\nНеобходимо обратиться к Администратору для восстановления КБК! Повторно КБК с таким же кодом создать нельзя!" 
		        : "Указанный КБК уже используется в системе. Введите другой КБК");
		    return false;
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			{
				switch (State)
				{
					case ViewModelState.Read:
						break;
					case ViewModelState.Edit:
						DataContainerFacade.Save(KBK);
						if (!m_oldCode.Equals(KBK.Code) || !m_oldName.Equals(KBK.Name))
						{
							var nameChange = new KBKHistory
							{
								Date = ChangeDate,
								Code = m_oldCode,
								KBKId = KBK.ID,
								Name = m_oldName
							};
							DataContainerFacade.Save(nameChange);
							RefreshLists();
						}
						m_oldName = KBK.Name;
						m_oldCode = KBK.Code;
						RaiseKBKUpdated();
						break;
					case ViewModelState.Create:
						ID = KBK.ID = DataContainerFacade.Save(KBK);
						State = ViewModelState.Edit;
						m_oldName = KBK.Name;
						m_oldCode = KBK.Code;
						OnPropertyChanged("IsEdit");
						RaiseKBKCreated();
						break;
					default:
						break;
				}
			    _oldNumber = KBK.Code;

			}
		}

		public override bool CanExecuteSave()
		{
			return string.IsNullOrEmpty(Validate()) &&
				IsDataChanged;
		}

		#endregion

		public void ItemPropertyChangedHandler(string propertyName)
		{
			OnPropertyChanged(propertyName);
		}

		public void RefreshLists()
		{
			try
			{
				KBKHistoryList = DataContainerFacade.GetListByProperty<KBKHistory>("KBKId", KBK.ID);
				OnPropertyChanged("KBKHistoryList");
			}
			catch (Exception e)
			{
				if (DoNotThrowExceptionOnError == false)
					throw;

				Logger.WriteException(e);
				if (IsInitialized)
					RaiseGetDataError();
				else
					throw new ViewModelInitializeException();
			}
		}

		public KBKViewModel(long id, ViewModelState action)
			: base(typeof(KBKListViewModel))
		{
            DataObjectTypeForJournal = typeof(KBK);
            State = action;

			if (action == ViewModelState.Create)
			{
				KBK = new KBK();
				ChangeDate = DateTime.Now;
				KBKHistoryList = new List<KBKHistory>();
				ID = -1;
			}
			else
			{
				KBK = DataContainerFacade.GetByID<KBK>(id);
			    _oldNumber = KBK.Code;
                ID = id;

				m_oldCode = KBK.Code;
				m_oldName = KBK.Name;

				ChangeDate = DateTime.Now;
			    IsBlockedByData = GetRefreshedBlockInfo();

                RefreshLists();
			}
			Register();
		}

	    public bool IsBlockedByData
	    {
	        get { return _isBlockedByData; }
	        set { _isBlockedByData = value; OnPropertyChanged(nameof(IsBlockedByData)); }
	    }

	    private bool GetRefreshedBlockInfo()
	    {
            IsBlockedByData = DataContainerFacade.GetListByPropertyCount<AsgFinTr>("KBKID", ID, true) != 0 ||
                            DataContainerFacade.GetListByPropertyCount<ReqTransfer>("KBKID", ID, true) != 0 ||
                            DataContainerFacade.GetListByPropertyCount<PaymentDetail>("KBKID", ID, true) != 0;
	        return IsBlockedByData;
	    }


	    private string Validate()
		{
			var data = "changedate";
			foreach (var key in data.ToLower().Split('|'))
			{
				var res = this[key];
				if (!string.IsNullOrEmpty(res))
					return res;
			}
			var kbkdata = "code|name|comment";
			foreach (var key in kbkdata.ToLower().Split('|'))
			{
				var e = new BaseDataObject.ValidateEventArgs(key);
				KBK_Validate(KBK, e);
				if (!string.IsNullOrEmpty(e.Error))
					return e.Error;
			}
			return string.Empty;
		}

		#region Validation
		public override string this[string key]
		{
			get
			{
				try
				{
					switch (key.ToLower())
					{
						//case "name": return KBK.Name == null || KBK.Name.Length == 0 || KBK.Name.Length > 200 ? DataTools.DATAERROR_NEED_DATA : null;
						//case "code": return KBK.Code == null || KBK.Code.Length != 20 ? DataTools.DATAERROR_NEED_DATA : null;

						case "changedate": return ChangeDate == null || ChangeDate == DateTime.MinValue ? DataTools.DATAERROR_NEED_DATA : null;

						default: return null;
					}
				}
				catch { return DataTools.DATAERROR_NEED_DATA; }
			}
		}
		#endregion

		public bool IsEdit => State == ViewModelState.Edit;

	    private void Register()
		{
			KBK.PropertyChanged += (sender, e) => { IsDataChanged = true; };
			KBK.OnValidate += KBK_Validate;
		}

        private const string TOO_LARGE_VALUE = "Введено слишком длинное значение";

		private void KBK_Validate(object sender, BaseDataObject.ValidateEventArgs e)
		{
			var kbk = sender as KBK;
			if (kbk != null)
			{
				switch (e.PropertyName.ToLower())
				{
                    case "name": e.Error = string.IsNullOrWhiteSpace(KBK.Name) ? DataTools.DATAERROR_NEED_DATA :
                        KBK.Name.Length > 200 ? TOO_LARGE_VALUE : null;
						break;
					case "comment": e.Error = KBK.Comment != null && KBK.Comment.Length > 200 ? TOO_LARGE_VALUE : null;
						break;
                    case "code": e.Error = string.IsNullOrEmpty(KBK.Code) ? DataTools.DATAERROR_NEED_DATA : DataTools.ValidateLengthExact(KBK.Code, 20);
						break;
                    case "maskedcode": e.Error = KBK.MaskedCode == null ? DataTools.DATAERROR_NEED_DATA :
                        !Regex.IsMatch(KBK.MaskedCode, "\\d{3} \\d \\d{2} \\d{5} \\d{2} \\d{4} \\d{3}") ? DataTools.DATAERROR_INVALID_FORMAT : null;
						break;
				}
			}
		}

		public EventHandler OnKBKCreated;
		protected void RaiseKBKCreated()
		{
		    OnKBKCreated?.Invoke(this, null);
		}

		public EventHandler OnKBKUpdated;
	    private bool _isBlockedByData;

	    protected void RaiseKBKUpdated()
	    {
	        OnKBKUpdated?.Invoke(this, null);
	    }

		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(KBK), KBK.ID) };
		}
	}
}
