﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class DocumentVRViewModel : DocumentBaseViewModel
    {
        public DocumentVRViewModel()
            : this(0, ViewModelState.Create)
        {
        }

        public DocumentVRViewModel(long id)
            : this(id, ViewModelState.Edit)
        {
        }

        public DocumentVRViewModel(long id, ViewModelState state)
            : base(id, state)
        {
        }

        public override Document.Types Type => Document.Types.VR;

        protected override IList<string> GetAdditionalExecutionInfo()
        {
            return BLServiceSystem.Client.GetElementByType(Element.Types.VRDocumentAdditionalExecutionInfo).Where(x => x.Visible).Select(e => e.Name).ToList();
        }

        protected override IList<string> GetOriginalStoragePlaces()
        {
            return BLServiceSystem.Client.GetElementByType(Element.Types.VRDocumentStoragePlaces).Where(x=>x.Visible).Select(e => e.Name).ToList();
        }

        protected override IList<LegalEntity> GetLegalEntityList()
        {
            return BLServiceSystem.Client.GetActiveUKListHib();
        }

        protected override IList<DocumentClass> GetDocumentClassList()
        {
            return DataContainerFacade.GetList<DocumentClass>();
        }

		public override bool HasExecutorList => false;

        //protected override IList<Person> GetPersonList()
		//{
		//    return new List<Person>();
		//}

        protected override IList<AdditionalDocumentInfo> GetAdditionalInfoList()
        {
            return new List<AdditionalDocumentInfo>();
            //return DataContainerFacade.GetList<AdditionalDocumentInfo>();
        }

        public override bool HasOutgoingNumber => true;
    }
}