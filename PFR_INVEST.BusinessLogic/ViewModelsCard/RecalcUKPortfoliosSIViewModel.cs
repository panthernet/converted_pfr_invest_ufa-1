﻿using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class RecalcUKPortfoliosSIViewModel : RecalcUKPortfoliosViewModel
    {
        public override void LoadContracts()
        {
            m_contracts = DataContainerFacade.GetList<Contract>().Where(c => c.TypeID == (int)Document.Types.SI).ToList();

            ContractsCount = m_contracts.Count;
            CurrentContractIndex = 0;
            ErrorsCount = 0;
            Canceled = false;
        }
    }
}
