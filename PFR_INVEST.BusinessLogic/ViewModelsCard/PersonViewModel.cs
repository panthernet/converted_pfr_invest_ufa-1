﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class PersonViewModel : ViewModelCard, IUpdateRaisingModel
	{
		private Person _person = new Person();
		public Person Person
		{
			get { return _person; }
			set { _person = value; OnPropertyChanged("Person"); }
		}

		public string FirstName
		{
			get { return Person.FirstName; }
			set { Person.FirstName = value; OnPropertyChanged("FirstName"); }
		}

		public string LastName
		{
			get { return Person.LastName; }
			set { Person.LastName = value; OnPropertyChanged("LastName"); }
		}

		public string MiddleName
		{
			get { return Person.MiddleName; }
			set { Person.MiddleName = value; OnPropertyChanged("MiddleName"); }
		}

		public string Position
		{
			get { return Person.Position; }
			set { Person.Position = value; OnPropertyChanged("Position"); }
		}
		public string Phone
		{
			get { return Person.Phone; }
			set { Person.Phone = value; OnPropertyChanged("Phone"); }
		}
		public string Fax
		{
			get { return Person.Fax; }
			set { Person.Fax = value; OnPropertyChanged("Fax"); }
		}
		public string Email
		{
			get { return Person.Email; }
			set { Person.Email = value; OnPropertyChanged("Email"); }
		}


		public bool IsDisabled
		{
			get { return Person.IsDisabled; }
			set { Person.IsDisabled = value; OnPropertyChanged("IsDisabled"); }
		}



		private long _divisionId;

		public PersonViewModel(long id, ViewModelState action)
			: base(typeof(DivisionListViewModel))
		{
			DataObjectTypeForJournal = typeof(Person);
			if (action == ViewModelState.Create)
			{
				//this.Fields = BLServiceSystem.Client.GetPersonStructure();
				//this.Fields["PFRB_ID"].Value = id;
				Person.ID = id;
			}
			else
			{
				//this.Fields = BLServiceSystem.Client.GetPerson(id);
				Person = DataContainerFacade.GetByID<Person>(id);
			}
			ID = Person.ID;
		}

		public void Load(long divisionId)
		{
			_divisionId = divisionId;
		}

		#region Execute implementation
		public override bool CanExecuteSave()
		{
			return String.IsNullOrEmpty(Validate()) && IsDataChanged;
		}


		protected override void ExecuteSave()
		{
			if (CanExecuteSave())
			{
				switch (State)
				{
					case ViewModelState.Read:
						break;
					case ViewModelState.Edit:
					case ViewModelState.Create:
						if (State == ViewModelState.Create)
							Person.DivisionId = _divisionId;
						ID = Person.ID = DataContainerFacade.Save(Person);
						if (GetViewModel != null)
						{
							var vm = GetViewModel(typeof(DivisionViewModel)) as DivisionViewModel;
							if (vm != null)
								vm.RefreshLists(true);
						}
						break;
				}
			}
		}

		public override bool CanExecuteDelete()
		{
			return Person.ID > 0 && DeleteAccessAllowed;
		}

		public override bool BeforeExecuteDeleteCheck()
		{
			var val = ValidatePersonUsage(Person.ID);
			if (!string.IsNullOrWhiteSpace(val))
			{
				DialogHelper.ShowAlert(val);
				return false;
			}

			return base.BeforeExecuteDeleteCheck();
		}

		protected override void ExecuteDelete(int delType)
		{
			if (Person.ID > 0)
			{
				DataContainerFacade.Delete(Person);
				if (GetViewModel != null)
				{
					var vm = GetViewModel(typeof(DivisionViewModel)) as DivisionViewModel;
					if (vm != null)
						vm.RefreshLists(true);
				}
			}
			base.ExecuteDelete(DocOperation.Delete);
		}

		public static string ValidatePersonUsage(long personID)
		{
			if (personID > 0)
			{
				var docUs = DataContainerFacade.GetListByPropertyConditions<Document>(new List<ListPropertyCondition> 
				{ 
					ListPropertyCondition.Equal("ExecutorID", personID), 
					ListPropertyCondition.NotEqual("StatusID", (long)-1) 
				});
				if (docUs.Any())
				{
					return "Удаление невозможно, так как в системе присутствует документ, в котором сотрудник отмечен как исполнитель";
				}

				var attUs = DataContainerFacade.GetListByPropertyConditions<Attach>(new List<ListPropertyCondition> 
				{ 
					ListPropertyCondition.Equal("ExecutorID", personID), 
					ListPropertyCondition.NotEqual("StatusID", (long)-1) 
				});
				if (attUs.Any())
				{
					return "Удаление невозможно, так как в системе присутствует вложение, в котором сотрудник отмечен как исполнитель";
				}

				var ordUs = DataContainerFacade.GetListByPropertyConditions<CbOrder>(new List<ListPropertyCondition>
				{
					ListPropertyCondition.Equal("FIO_ID", personID)
				});
				if (ordUs.Any())
				{
					return "Удаление невозможно, так как в системе присутствует поручение, в котором сотрудник отмечен как уполномоченное лицо";
				}
			}

			return null;
		}

		public void ExecuteDelete()
		{
			ExecuteDelete(DocOperation.Delete);
		}


		#endregion

		private const string ValidatedFields = "FirstName|LastName|Position|PHONE|email|fax|middlename";

		public override string this[string columnName]
		{
			get
			{
				string res = string.Empty;
				switch (columnName.ToLower())
				{
					case "firstname": res = string.IsNullOrWhiteSpace(Person.FirstName) ? DataTools.DATAERROR_NEED_DATA : null; break;
					case "fax": res = !string.IsNullOrEmpty(Person.Fax) && string.IsNullOrWhiteSpace(Person.Fax) ? DataTools.DATAERROR_INVALID_FORMAT : null; break;
					case "middlename": res = !string.IsNullOrEmpty(Person.MiddleName) && string.IsNullOrWhiteSpace(Person.MiddleName) ? DataTools.DATAERROR_INVALID_FORMAT : null; break;
					case "lastname": res = string.IsNullOrWhiteSpace(Person.LastName) ? DataTools.DATAERROR_NEED_DATA : null; break;
					case "phone": res = string.IsNullOrWhiteSpace(Person.Phone) ? DataTools.DATAERROR_NEED_DATA : null; break;
					case "position": res = string.IsNullOrWhiteSpace(Person.Position) ? DataTools.DATAERROR_NEED_DATA : null; break;
					case "email":
						res = !string.IsNullOrEmpty(Person.Email) && (!DataTools.IsEmail(Person.Email.Trim()) || string.IsNullOrWhiteSpace(Person.Email)) ? DataTools.DATAERROR_INVALID_FORMAT : null;
						break;
				}
				return res;
			}
		}

		private string Validate()
		{
			foreach (string key in ValidatedFields.Split('|'))
			{
				string errorMessage = this[key];
				if (!string.IsNullOrEmpty(this[key]))
					return errorMessage;
			}
			return null;
		}

		public override string GetDocumentHeader()
		{
			return "Сотрудник";
		}

		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Person), Person.ID) };
		}
	}
}
