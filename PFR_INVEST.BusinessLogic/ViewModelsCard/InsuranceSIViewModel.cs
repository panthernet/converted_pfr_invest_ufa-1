﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class InsuranceSIViewModel :InsuranceViewModel
    {

        public InsuranceSIViewModel(ViewModelState action, long id = 0)
            : base(action,Document.Types.SI, id )
        {
            
        }


       
    }
}
