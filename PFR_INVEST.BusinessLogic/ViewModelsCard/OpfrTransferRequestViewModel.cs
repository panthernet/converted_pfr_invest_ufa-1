﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public class OpfrTransferRequestViewModel : ViewModelCardDialog, IRequestCloseViewModel, IUpdateRaisingModel, IRequestLoadingIndicator
	{

        public DelegateCommand OkCommand { get; set; }

        public event EventHandler RequestClose;
        public event EventHandler RequestLoadingIndicator;

	    protected void OnRequestClose()
	    {
	        RequestClose?.Invoke(this, null);
	    }

        protected void OnRequestLoadingIndicator(object data = null)
        {
            RequestLoadingIndicator?.Invoke(data, null);
        }

        public long RegisterID { get; }
        private readonly OpfrRegister _register;

        public DateTime? TrancheDate
		{
			get
			{
				return _register?.TrancheDate;
			}

			set
			{
				if (_register == null) return;
				_register.TrancheDate = value;
				OnPropertyChanged("TrancheDate");
				OnPropertyChanged("IsValid");
			}
		}

        public string TrancheNumber
        {
            get
            {
                return _register?.TrancheNum;
            }

            set
            {
                if (_register == null) return;
                _register.TrancheNum = value;
                OnPropertyChanged("TrancheNumber");
                OnPropertyChanged("IsValid");
            }
        }

		public OpfrTransferRequestViewModel(long regID)
            : base(true)
		{
			RegisterID = regID;

			if (RegisterID > 0)
				_register = DataContainerFacade.GetByID<OpfrRegister, long>(RegisterID);

			TrancheDate = DateTime.Now;

            OkCommand = new DelegateCommand(o => IsValid, o =>
            {
                OnRequestLoadingIndicator("Создание заявки...");
                ExecuteCreateRequest();
                OnRequestClose();
                OnRequestLoadingIndicator();
                //т.к. не испoльзyем стандартные метoды Save / Delete
                ViewModelManager.UpdateDataInAllModels(GetUpdatedList());
            });
		}

		public void ExecuteCreateRequest()
		{
            BLServiceSystem.Client.OpfrRegisterUpdateRequest(_register);
		}


		public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToLower())
				{
                    case "tranchedate": return TrancheDate == null ? "Необходимо указать дату заявки" : null;
                    case "tranchenumber": return string.IsNullOrWhiteSpace(TrancheNumber) ? "Необходимо указать номер заявки" : TrancheNumber.ValidateMaxLength(50);
					default: return null;
				}
			}
		}

		public bool IsValid => string.IsNullOrWhiteSpace(Validate());

        private string Validate()
		{
			const string fieldNames =
				"tranchedate|tranchenumber";

			return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
		}

	    public IList<KeyValuePair<Type, long>> GetUpdatedList()
	    {
            return new List<KeyValuePair<Type, long>>{ 
				new KeyValuePair<Type, long>(typeof(OpfrRegister), RegisterID)
			};
	    }

	}
}
