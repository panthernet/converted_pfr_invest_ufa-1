﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	public class AccModelBase : ViewModelCard, IUpdateRaisingModel
	{
		#region Fields

		public bool ReadAccessOnly => IsReadOnly;

	    public bool EnabledState => !ReadAccessOnly;


	    protected AccOperation acc;

		public string Comment
		{
			get { return acc.Comment; }
			set { acc.Comment = value; OnPropertyChanged("Comment"); }
		}

		public virtual object Content
		{
			get { return acc.Content; }
			set { acc.Content = (string)value; OnPropertyChanged("Content"); }
		}

		public DateTime OperationDate
		{
			get
			{
				return acc == null ? DateTime.Now : acc.OperationDate == null ? DateTime.Now : acc.OperationDate.Value.Date;
			}
			set
			{
				acc.OperationDate = value;
				OnPropertyChanged("OperationDate");
			}
		}

		public DateTime NewDate
		{
			get
			{
				return acc == null ? DateTime.Now : acc.NewDate == null ? DateTime.Now : acc.NewDate.Value.Date;
			}
			set
			{
				acc.NewDate = value;
				SelectedCurs = GetCursForSelectedCurrency();
				OnPropertyChanged("NewDate");
				OnPropertyChanged("IsSelectCourceButtonEnabled");
			}
		}

		public decimal Rate
		{
			get
			{
				if (SelectedCurrency == null)
					return 0;

				if (CurrencyHelper.IsRUB(SelectedCurrency))
					return 1;

				if (SelectedCurs == null)
					return 0;

				return SelectedCurs.Value ?? 0;
			}
		}

		public decimal? Summ
		{
			get
			{
				//return acc.Summ.HasValue ? acc.Summ.Value : 0;
				return acc.Summ;
			}

			set
			{
				acc.Summ = value == 0 ? null : value;
				OnPropertyChanged("Rate");
				OnPropertyChanged("Summ");
				OnPropertyChanged("Total");
			}
		}

		public decimal Total
		{
			get
			{
				if (acc.Summ == null)
					return 0;

				var total = Rate == 0 ? Math.Round(acc.Summ.Value, 4) : Rate * acc.Summ.Value;

				//Округляем результат до 2 знаков
				return Math.Round(total, 2, MidpointRounding.AwayFromZero);
			}
		}

		private Curs selCurs;
		public Curs SelectedCurs
		{
			get { return selCurs; }
			set
			{
				if (selCurs != value)
				{
					selCurs = value;
				}
				OnPropertyChanged("Rate");
				OnPropertyChanged("Total");
				OnPropertyChanged("IsSelectCourceButtonEnabled");
			}
		}

		private Currency selCurrency;
		public Currency SelectedCurrency
		{
			get { return selCurrency; }
			set
			{
				if (value != null)
				{
					acc.CurrencyID = value.ID;
					selCurrency = value;
					SelectedCurs = GetCursForSelectedCurrency();
					OnPropertyChanged("SelectedCurrency");
					OnPropertyChanged("Summ");
					OnPropertyChanged("IsSelectCourceButtonEnabled");
				}
			}
		}

		public bool IsSelectCourceButtonEnabled => (SelectedCurs == null || SelectedCurs.Date == null || !SelectedCurs.Date.Value.Equals(NewDate)) && Rate == 0 && !IsReadOnly;

	    protected PortfolioFullListItem account;
		/// <summary>
		/// Портфель (счёт)
		/// </summary>
		public virtual PortfolioFullListItem Account
		{
			get
			{
				return account;
			}

			set
			{
				account = value;

				if (value != null)
				{
					//Если тип операции списание - обновляем счёт-источник
					if (acc.KindID == 4)
					{
						acc.SourcePortfolioID = value.Portfolio.ID;
						acc.SourcePFRBankAccountID = value.PfrBankAccount.ID;
					}
					else
					{
						acc.PortfolioID = value.Portfolio.ID;
						acc.PFRBankAccountID = value.PfrBankAccount.ID;
					}

					SelectedCurrency = value.PfrBankAccount.GetCurrency();
				}

				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("Bank");
				OnPropertyChanged("PFName");
				OnPropertyChanged("Account");
			}
		}

		#endregion

		#region Execute

		public override bool CanExecuteDelete()
		{
			return State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType)
		{
			DataContainerFacade.Delete(acc);

			base.ExecuteDelete(DocOperation.Delete);
		}

		protected virtual void ExecuteSelectAccount()
		{
			var selected = DialogHelper.SelectPortfolio();

			if (selected != null)
			{
				Account = selected;
			}
		}

		public override bool CanExecuteSave()
		{
			return string.IsNullOrEmpty(Validate()) && IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
					DataContainerFacade.Save<AccOperation, long>(acc);
					break;
				case ViewModelState.Create:
					ID = DataContainerFacade.Save<AccOperation, long>(acc);
					acc.ID = ID;
					break;
			}
		}
		#endregion

		#region Validation
		public override string this[string columnName]
		{
			get
			{
				const string errorMessage = "Неверное значение";
				const string errorMaxValue = "Недопустимо большое значение";
				switch (columnName.ToUpper())
				{
					case "OPERATIONDATE": return (acc.OperationDate == null) || (((DateTime)acc.OperationDate).Year < 1900) ? errorMessage : null;
					case "NEWDATE": return (acc.NewDate == null) || (((DateTime)acc.NewDate).Year < 1900) ? errorMessage : null;
					case "SUMM": return Summ.ValidateRequired() ?? Summ.ValidateMaxFormat();
					case "RATE": return Rate == 0 ? errorMessage : null;
					case "COMMENT": return Comment.ValidateMaxLength(511);//  != null && this.Comment.Length > 511 ? errorMaxValue : null;
					case "CONTENT": return Content is string && ((string) Content).Length > 255 ? errorMaxValue : null;
					default: return null;
				}
			}
		}

		protected virtual string Validate()
		{
			const string errorMessage = "Неверный формат данных";

			if (Account == null)
				return errorMessage;

			var fields = "OPERATIONDATE|NEWDATE|SUMM|RATE|COMMENT|CONTENT";

			foreach (var fieldName in fields.Split('|'))
			{
				var err = this[fieldName];
				if (!string.IsNullOrEmpty(err))
				{
					return err;
				}
			}

			return (acc == null || acc.CurrencyID == null || acc.Summ == null  || Rate == 0) || string.IsNullOrEmpty(acc.Content)
				? errorMessage : null;
		}
		#endregion

		#region Commands
		public ICommand SelectAccount { get; private set; }
		#endregion

		Curs GetCursForSelectedCurrency()
		{
			if (SelectedCurrency == null)
				return null;
			Curs custom = null;
			if (acc.CursID.HasValue)
			{
				custom = DataContainerFacade.GetByID<Curs>(acc.CursID.Value);
				if (custom != null && custom.CurrencyID != SelectedCurrency.ID)
					custom = null;
			}

			return custom ?? BLServiceSystem.Client.GetOnDateCurrentCursesHib(acc.NewDate ?? DateTime.Now).FirstOrDefault(c => c.CurrencyID == SelectedCurrency.ID && c.Date.Value.Equals(NewDate));
		}

		public void GetCourceManuallyOrFromCBRSite()
		{
			if (SelectedCurrency == null)
				return;

			var requestedRate = DialogHelper.GetCourceManuallyOrFromCBRSite(SelectedCurrency.ID, NewDate);
		    if (!requestedRate.HasValue) return;
		    var rate = new Curs
		    {
		        Value = requestedRate.Value,
		        Date = NewDate,
		        CurrencyID = SelectedCurrency.ID
		    };
		    rate.ID = DataContainerFacade.Save<Curs, long>(rate);
		    SelectedCurs = rate;
		}

		/// <summary>
		/// Загрузка данных об неразнесенных страховых взносах
		/// </summary>
		/// <param name="lId">ID неразнесенных страховых взносов в базе</param>
		public virtual void Load(long lId)
		{
			try
			{
				ID = lId;
				acc = DataContainerFacade.GetByID<AccOperation, long>(lId);


				if (acc.KindID == 4)
				{
					//Поле счёта списания должно заполнятся из Source;
					if (acc.SourcePFRBankAccountID != null && acc.SourcePortfolioID != null)
					{
						Account = BLServiceSystem.Client.GetPortfolioFull((long)acc.SourcePFRBankAccountID, (long)acc.SourcePortfolioID);
					}
				}
				else
				{

					if (acc.PFRBankAccountID != null && acc.PortfolioID != null)
					{
						Account = BLServiceSystem.Client.GetPortfolioFull((long)acc.PFRBankAccountID, (long)acc.PortfolioID);
					}
				}

			}
			catch (Exception e)
			{
				if (DoNotThrowExceptionOnError == false)
					throw;

				Logger.WriteException(e);
				throw new ViewModelInitializeException();
			}
		}

		public AccModelBase(params Type[] t)
			: base(t)
		{
		    DataObjectTypeForJournal = typeof (AccOperation);
			SelectAccount = new DelegateCommand(o => !IsReadOnly, o => ExecuteSelectAccount());
		    acc = new AccOperation {OperationDate = DateTime.Now.Date};
		    acc.NewDate = acc.OperationDate;

			ID = acc.ID;
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>>
			{ 
				new KeyValuePair<Type, long>(typeof(AccOperation), acc.ID)
			};
		}
	}
}
