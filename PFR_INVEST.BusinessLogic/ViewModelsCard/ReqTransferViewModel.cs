﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class ReqTransferViewModel : ViewModelCard, IUpdateRaisingModel, IRefreshableCardViewModel
    {
        public ReqTransfer Transfer { get; set; }
        public SITransfer TransferList { get; set; }
        public Contract Contract { get; set; }
        public LegalEntity LegalEntity { get; set; }
        public BankAccount BankAccount { get; set; }
        public SIRegister TransferRegister { get; set; }
        public SPNOperation Operation { get; set; }
        public SPNDirection Direction { get; set; }
		public List<AsgFinTr> CommonPPList { get; private set; }
		public List<PaymentDetail> PaymentDetailsList { get; private set; }

        public Document.Types DocumentType
        {
            get
            {
                if (Contract == null)
                    return Document.Types.Other;
                return (Document.Types)Contract.TypeID;
            }
        }

        public DateTime? TransferDate
        {
            get
            {
                return Transfer.Date;
            }
            set
            {
                Transfer.Date = value;
                OnPropertyChanged("TransferDate");
            }
        }

        public string PaymentOrderNumber
        {
            get
            {
                return Transfer.PaymentOrderNumber;
            }
            set
            {
                Transfer.PaymentOrderNumber = value;
                OnPropertyChanged("PaymentOrderNumber");
            }
        }

        public DateTime? PaymentOrderDate
        {
            get
            {
                return Transfer.PaymentOrderDate;
            }
            set
            {
                Transfer.PaymentOrderDate = value;
                OnPropertyChanged("PaymentOrderDate");
            }
        }

        public decimal? TransferInvestmentIncome
        {
            get
            {
                return Transfer.InvestmentIncome;
            }
            set
            {
                Transfer.InvestmentIncome = value;
                OnPropertyChanged("TransferInvestmentIncome");
            }
        }

        public decimal? TransferSum
        {
            get
            {
                return Transfer.Sum ?? 0;
            }
            set
            {
                Transfer.Sum = value;
                OnPropertyChanged("TransferSum");
            }
        }

        public long? TransferZL
        {
            get
            {
                return Transfer.ZLMoveCount ?? 0;
            }
            set
            {
                Transfer.ZLMoveCount = value;
                OnPropertyChanged("TransferZL");
            }
        }

        public string RegisterInfo
        {
            get
            {
                if (Operation != null)
                    return string.Format("{0}    \r\nРеестр {1}",
                        Operation.Name,
                        TransferRegister.RegisterNumber);
                return string.Format("Реестр {0}",
                    TransferRegister.RegisterNumber);
            }
        }

        public string TransferStatus
        {
            get
            {
                return Transfer.TransferStatus;
            }
            set
            {
                Transfer.TransferStatus = value;
                OnPropertyChanged("TransferStatus");
            }
        }

        public DateTime? TransferActDate
        {
            get
            {
                return Transfer.TransferActDate;
            }
            set
            {
                Transfer.TransferActDate = value;
                OnPropertyChanged("TransferActDate");
            }
        }

        public string TransferActNumber
        {
            get
            {
                return Transfer.TransferActNumber;
            }
            set
            {
                Transfer.TransferActNumber = value;
                OnPropertyChanged("TransferActNumber");
            }
        }

        public string RequestNumber
        {
            get
            {
                return Transfer.RequestNumber;
            }
            set
            {
                Transfer.RequestNumber = value;
                OnPropertyChanged("RequestNumber");
            }
        }

        public DateTime? RequestFormingDate
        {
            get
            {
                return Transfer.RequestFormingDate;
            }
            set
            {
                Transfer.RequestFormingDate = value;
                OnPropertyChanged("RequestFormingDate");
            }
        }

        public DateTime? RequestCreationDate
        {
            get
            {
                return Transfer.RequestCreationDate;
            }
            set
            {
                Transfer.RequestCreationDate = value;
                OnPropertyChanged("RequestCreationDate");
            }
        }

        public DateTime? RequestDeliveryDate
        {
            get
            {
                return Transfer.RequestDeliveryDate;
            }
            set
            {
                Transfer.RequestDeliveryDate = value;
                OnPropertyChanged("RequestDeliveryDate");
            }
        }

        public DateTime? SetSPNTransferDate
        {
            get
            {
                return Transfer.SetSPNTransferDate;
            }
            set
            {
                Transfer.SetSPNTransferDate = value;
                OnPropertyChanged("SetSPNTransferDate");
            }
        }

        public bool IsTransferFromPFRToUK => Direction != null && Direction.isFromUKToPFR != null && !Direction.isFromUKToPFR.Value;

        public Visibility ShowControlsFromPFRToUK => IsTransferFromPFRToUK ? Visibility.Visible : Visibility.Collapsed;

        public Visibility ShowInvestment => IsTransferFromUKToPFR ? Visibility.Visible : Visibility.Collapsed;

        public bool IsTransferFromUKToPFR => Direction != null && Direction.isFromUKToPFR != null && Direction.isFromUKToPFR.Value;

        public Visibility ShowControlsFromUKToPFR => IsTransferFromUKToPFR ? Visibility.Visible : Visibility.Collapsed;

        public bool IsTransferSaved => ID > 0;

        const string EXECUTION_CONTROL_EXECUTED = "Исполнено";
        const string EXECUTION_CONTROL_NOT_EXECUTED = "Не исполнено";
        public string ExecutionControl
        {
            get
            {
                //Велижанин: так что если дата списания средств со счета > установленная дата перечисления то карсный шарик
                //Красный шарик = не исполнено
                //а если платежка не заполнена еще - то ничего не делаем

                if (!Transfer.SetSPNTransferDate.HasValue || !Transfer.SPNDebitDate.HasValue)
                    return null;

                return (Transfer.SPNDebitDate.Value.Date > Transfer.SetSPNTransferDate.Value.Date)
                    ? EXECUTION_CONTROL_NOT_EXECUTED
                    : EXECUTION_CONTROL_EXECUTED;
            }
        }

        private const string SPN_DEBIT_DATE_FORMAT = "Дата списания СПН {0}:";
        public string SPNDebitDateLabel => IsTransferFromUKToPFR ? string.Format(SPN_DEBIT_DATE_FORMAT, DocumentType == Document.Types.VR ? "со счета ГУК" : "со счета УК") : string.Format(SPN_DEBIT_DATE_FORMAT, "со счета ПФР");

        #region KBK
        public KBK SelectedKBK
        {
            get
            {
                return KBKList.First(x => x.ID == (Transfer.KBKID ?? 0));
            }
            set
            {
                if (value.ID != 0)
                {
                    Transfer.KBKID = value.ID;
                }
                else
                {
                    Transfer.KBKID = null;
                }
            }
        }

        public List<KBK> KBKList { get; set; }
        #endregion

        public override bool CanExecuteDelete()
        {
            return State == ViewModelState.Edit;
        }

        public override bool BeforeExecuteDeleteCheck()
        {
            return IsAllowRemove();
        }

        protected override void ExecuteDelete(int delType)
        {
            BLServiceSystem.Client.DeleteReqTransfer(Transfer.ID, Transfer.TransferListID, Contract.ID, TransferStatusIdentifier.IsStatusActSigned(Transfer.TransferStatus));
           /* var plans = BLServiceSystem.Client.GetSIUKPlansByReqID(Transfer.ID);

            foreach (var item in plans)
            {
                // DataContainerFacade.Delete<SIUKPlan>(item);
                item.UnsetTransferId(Transfer.ID);
                var rq = item.GetReqTransfer() ?? item.GetReqTransfer2();
                item.FactSum = rq == null ? 0 : rq.Sum;
                DataContainerFacade.Save<SIUKPlan, long>(item);
            }

            if (TransferStatusIdentifier.IsStatusActSigned(Transfer.TransferStatus))
            {
                BLServiceSystem.Client.UpdateTransferByActSigned(
                     Contract,
                     Transfer,
                     IsTransferFromPFRToUK,
                     TransferStatusIdentifier.ActionTransfer.DeleteTransfer);
            }

            var reqs = DataContainerFacade.GetListByProperty<AsgFinTr>("ReqTransferID", Transfer.ID).ToList();
            reqs.ForEach2(a => a.ClearLink());
            DataContainerFacade.BatchSave<AsgFinTr, long>(reqs);
            
            DataContainerFacade.Delete(Transfer);
            */
            if (TransferStatusIdentifier.IsStatusActSigned(Transfer.TransferStatus))
            {
                ViewModelManager.RefreshViewModels(typeof(SPNMovementsSIListViewModel), typeof(SPNMovementsVRListViewModel));
            }
            base.ExecuteDelete(DocOperation.Archive);
        }


        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                        BLServiceSystem.Client.SaveReqTransfer(Transfer);
                        OnPropertyChanged("IsTransferSaved");
                        break;
                    case ViewModelState.Create:
                        Transfer.ID = ID = BLServiceSystem.Client.SaveReqTransfer(Transfer);
                        ViewModelManager.RefreshCardViewModel(typeof(TransferVRViewModel), Transfer.ID);
                        ViewModelManager.RefreshCardViewModel(typeof(TransferSIViewModel), Transfer.ID);
                        OnPropertyChanged("IsTransferSaved");
                        break;
                    default:
                        break;
                }
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

		private ReqTransferViewModel()
			: base(typeof(TransfersSIListViewModel), 
								typeof(TransfersSIArchListViewModel), 
								typeof(TransfersVRListViewModel), 
								typeof(TransfersVRArchListViewModel),
                                typeof(SIAssignPaymentsArchiveListViewModel), 
								typeof(VRAssignPaymentsArchiveListViewModel), 
								typeof(SIAssignPaymentsListViewModel), 
								typeof(VRAssignPaymentsListViewModel),
                                typeof(ZLMovementsSIListViewModel),
								typeof(ZLMovementsVRListViewModel),
								typeof(SPNMovementsSIListViewModel),
								typeof(SPNMovementsVRListViewModel),
								typeof(UKTransfersListViewModel)) 
		{
            DataObjectTypeForJournal = typeof(ReqTransfer);	
		}

        public ReqTransferViewModel(long id, ViewModelState state)
            : this()
        {
            State = state;

            LoadCardData(id);

            RefreshConnectedCardsViewModels = RefreshParentViewModel;
        }

        public ReqTransferViewModel(SITransfer siTransfer, ReqTransfer reqTransfer, ViewModelState state)
            : this()
        {
            State = state;

            Transfer = reqTransfer ?? new ReqTransfer { TransferStatus = TransferStatusIdentifier.sBeginState };
            TransferList = siTransfer;
            LoadCardData();
            RefreshConnectedCardsViewModels = RefreshParentViewModel;
        }

        public void LoadCardData(long id)
        {		
			if (State == ViewModelState.Create)
            {
                Transfer = new ReqTransfer
                {
                    TransferListID = id,
                    TransferStatus = TransferStatusIdentifier.sBeginState
                };
                TransferList = DataContainerFacade.GetByID<SITransfer, long>(id);
            }
            else
            {
                ID = id;
                Transfer = DataContainerFacade.GetByID<ReqTransfer, long>(ID);
                TransferList = Transfer.GetTransferList();				
            }
            LoadCardData();
        }

        public void LoadCardData()
        {
			PaymentDetailsList = DataContainerFacade.GetList<PaymentDetail>(false);

			CommonPPList = Transfer.GetCommonPPList();
			OnPropertyChanged("CommonPPList");
            Contract = TransferList.GetContract();
            LegalEntity = Contract.GetLegalEntity();
            BankAccount = Contract.GetBankAccount();
            TransferRegister = TransferList.GetTransferRegister();
            Operation = TransferRegister.GetOperation();
            Direction = TransferRegister.GetDirection();
            KBKList = DataContainerFacade.GetList<KBK>();           

			// если есть ПП, то выводим КБК из ПП
			// фикс http://jira.vs.it.ru/browse/DOKIPIV-185, так как ПП на одно перечисление по http://jira.vs.it.ru/browse/DOKIPIV-126 может быть несколько
			var kbkAsg = CommonPPList.OrderBy(pp=>pp.SPNDate).LastOrDefault();
			var currentKBK = kbkAsg != null ? kbkAsg.KBKID : Transfer.KBKID;
			Transfer.KBKID = currentKBK;

			if (KBKList.All(k => k.ID != currentKBK))
			{
				var kbk = DataContainerFacade.GetByID<KBK>(currentKBK);
				if (kbk != null)
					KBKList.Insert(0, kbk);
			}


            KBKList.Add(new KBK { ID = 0, MaskedCode = KBK.EMPTY, Name = KBK.EMPTY });
            OnPropertyChanged("TransferStatus");
        }

        private void RefreshParentViewModel()
        {
            if (GetViewModel != null)
            {
                TransferViewModel vm1 = GetViewModel(typeof(TransferViewModel)) as TransferViewModel;
                if (vm1 != null)
                    vm1.RefreshTransfers();

                UKPaymentPlanViewModel vm2 = GetViewModel(typeof(UKPaymentPlanViewModel)) as UKPaymentPlanViewModel;
                if (vm2 != null)
                    vm2.RefreshTransfers();

                SIVRRegisterViewModel vm3 = GetViewModel(typeof(SIVRRegisterViewModel)) as SIVRRegisterViewModel;
                if (vm3 != null)
                    vm3.RefreshTransferLists();

                YearPlanViewModel vm4 = GetViewModel(typeof(YearPlanViewModel)) as YearPlanViewModel;
                if (vm4 != null)
                    vm4.RefreshUKPaymentsPlans();
            }
        }

        private bool Validate()
        {
            return Transfer.Sum > 0;
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "TransferSum": return (Transfer.Sum > 0) ? null : "Значение должно быть больше нуля";
					case "RequestNumber": return RequestNumber.ValidateMaxLength(50);
                    default: return "";
                }

            }
        }

        private bool IsAllowRemove()
        {
            return true;
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (ReqTransfer), Transfer.ID)};
            return list;
        }

        public void RefreshModel()
        {
            if(Transfer == null || Transfer.ID == 0) return;
            LoadCardData(Transfer.ID);
            IsDataChanged = false;
        }

        public long? CardID => ID;
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class ReqTransferInCacheViewModel : ReqTransferViewModel
    {

        //ссылка на интерфейс для работы с временными Перечеслениями,
        //до сохранения в базу

        private readonly ITmpRepository<ReqTransfer> _tmpRepository;

        /// <summary>
        /// Создает модель которая редактирует Запрос, без сохранения его в базу
        /// </summary>
        /// <param name="tmpRepository"></param>
        /// <param name="state"></param>
        /// <param name="siTransfer"></param>
        /// <param name="reqTransfer"></param>
        public ReqTransferInCacheViewModel(SITransfer siTransfer, ReqTransfer reqTransfer, ITmpRepository<ReqTransfer> tmpRepository, ViewModelState state)
            : base(siTransfer, reqTransfer, state)
        {
            _tmpRepository = tmpRepository;
        }


        protected override void ExecuteDelete(int delType)
        {
            _tmpRepository.Delete(Transfer);
        }


        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                _tmpRepository.Save(Transfer);
            }
        }

    }

}
