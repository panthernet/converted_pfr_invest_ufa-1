﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class GivenFinRegisterViewModel : ViewModelCard
    {

        private Finregister finregister;

        public string FinNum
        {
            get {
                return finregister != null ? finregister.FinNum : "";
            }

            set
            {
                if (finregister != null)
                {
                    finregister.FinNum = value;
                    OnPropertyChanged("FinNum");
                }
            }
        }

        public DateTime? FinDate
        {
            get {
                return finregister != null ? finregister.FinDate : null;
            }

            set
            {
                if (finregister != null)
                {
                    finregister.FinDate = value;
                    OnPropertyChanged("FinDate");
                }
            }
        }

        public string FinMoveType
        {
            get {
                return finregister != null ? finregister.FinMoveType : "";
            }

            set
            {
                if (finregister != null)
                {
                    finregister.FinMoveType = value;
                    OnPropertyChanged("FinMoveType");
                }
            }
        }

        public List<string> FinMoveTypes => RegisterIdentifier.FinregisterMoveTypes.ToList;

        public GivenFinRegisterViewModel(long fr_id)
            : base(typeof(RegistersListViewModel))
        {
            DataObjectTypeForJournal = typeof(Finregister);
            ID = fr_id;
            finregister = DataContainerFacade.GetByID<Finregister, long>(fr_id);
            finregister.FinDate = DateTime.Now;
        }

        #region Execute implementations

        public override bool CanExecuteSave()
        {
            return String.IsNullOrEmpty(Validate());
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    finregister.Status = RegisterIdentifier.FinregisterStatuses.Transferred;
                    DataContainerFacade.Save<Finregister, long>(finregister);
                    break;
                case ViewModelState.Create:
                    break;
            }
        }
        #endregion

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                switch (columnName.ToLower())
                {
                    case "finnum": return string.IsNullOrEmpty(FinNum) ? errorMessage : null;
                    case "finmovetype": return string.IsNullOrEmpty(FinMoveType) ? errorMessage : null;
                    default: return null;
                }
            }
        }

        private string Validate()
        {
            const string fieldNames =
                "FinNum|FinDate|FinMoveType";

            foreach (string fieldName in fieldNames.Split("|".ToCharArray()))
            {
                if (!String.IsNullOrEmpty(this[fieldName]))
                {
                    return "Неверный формат данных";
                }
            }

            return null;
        }
    }
}
