﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.User)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class PenaltyViewModel : ViewModelCard, IUpdateRaisingModel
	{

		#region Fields
		private PortfolioIdentifier.PortfolioTypes type = PortfolioIdentifier.PortfolioTypes.SPN;
		public PortfolioIdentifier.PortfolioTypes Type
		{
			get { return type; }
			set
			{
				type = value;
				OnPropertyChanged("Type");
			}
		}

		private PortfolioFullListItem _account;
		public PortfolioFullListItem Account
		{
			get { return _account; }
			set
			{
				_account = value;
				if (value != null)
				{
					Penalty.PortfolioID = value.Portfolio.ID;
					Penalty.LegalEntityID = value.BankLE.ID;
				}

				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("Bank");
				OnPropertyChanged("PFName");
				OnPropertyChanged("Account");
			}
		}

		private IList<Month> _monthsList = new List<Month>();
		public IList<Month> MonthsList
		{
			get { return _monthsList; }
			set
			{
				_monthsList = value;
				OnPropertyChanged("MonthsList");
			}
		}

		private IList<DopSPN> _dopSpNs = new List<DopSPN>();
		public IList<DopSPN> DopSPNs
		{
			get
			{
				return _dopSpNs;
			}
			set
			{
				_dopSpNs = value;
				OnPropertyChanged("DopSPNs");
			}
		}
		#endregion

		#region Execute implementation


		private void ExecuteSelectAccount()
		{
			PortfolioFullListItem selected;
			switch (Type) 
			{
				case PortfolioIdentifier.PortfolioTypes.SPN:
				case PortfolioIdentifier.PortfolioTypes.NOT_DSV:
					selected = DialogHelper.SelectPortfolio(true, true, Portfolio.Types.DSV);
					break;
				case PortfolioIdentifier.PortfolioTypes.DSV:
				case PortfolioIdentifier.PortfolioTypes.NOT_SPN:
					selected = DialogHelper.SelectPortfolio(true, true, Portfolio.Types.SPN);
					break;
				default:
					selected = DialogHelper.SelectPortfolio(true);
					break;

			}			
			if (selected != null)
			{
				Account = selected;
			}
		}

        //Пенни могут появлятся в системе только через ДК, и никак иначе
        public override bool IsReadOnly => true;

        public override bool CanExecuteDelete()
		{
			return false;
		}

		public override bool CanExecuteSave()
		{
			return false; //Пенни могут появлятся в системе только через ДК, и никак иначе
			//return String.IsNullOrEmpty(Validate()) && this.IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
					ID = Penalty.ID = DataContainerFacade.Save(Penalty);
					break;

			}
		}
		#endregion

		#region Validation

		private void RegisterValidation()
		{
			Penalty.PropertyChanged += (sender, e) => { IsDataChanged = true; };
			Penalty.OnValidate += Field_Validate;
		}

		private static void Field_Validate(object sender, BaseDataObject.ValidateEventArgs e)
		{
			var item = (sender as AddSPN);
			if (item != null)
			{
			    const string error = "Не заполнены все обязательные поля!";
			    switch (e.PropertyName)
				{
					case "OperationDate": e.Error = item.OperationDate.HasValue ? null : error; break;
					case "NewDate": e.Error = item.NewDate.HasValue ? null : error; break;
					case "RegNum": e.Error = string.IsNullOrWhiteSpace(item.RegNum) ? error : null; break;
					case "Summ": e.Error = item.Summ.HasValue ? null : error; break;
				}
			}
		}


		public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToUpper())
				{
					default: return null;
				}
			}
		}

		private string Validate()
		{
			const string errorMessage = "Неверный формат данных";
			const string validaetFields = "OperationDate|NewDate|RegNum|Summ";

			if (Account == null)
				return errorMessage;

			if (!Penalty.MonthID.HasValue)
				return errorMessage;

			foreach (var key in validaetFields.Split('|'))
			{
				var e = new BaseDataObject.ValidateEventArgs(key);
				Field_Validate(Penalty, e);
				if (!string.IsNullOrEmpty(e.Error))
				{
					return e.Error;
				}
			}
			return null;
		}
		#endregion

		public ICommand SelectAccount { get; private set; }

		public ICommand AddDopSPN { get; private set; }


		public bool PeriodEnabled => State == ViewModelState.Create;

	    public bool ReadAccessOnly => IsReadOnly;

	    private AddSPN _mPenalty;
		public AddSPN Penalty
		{
			get { return _mPenalty; }
			set
			{
				if (_mPenalty != value)
				{
					_mPenalty = value;
					OnPropertyChanged("Penalty");
				}
			}
		}


		public void Load(long lId)
		{
			try
			{
				ID = lId;
				Penalty = DataContainerFacade.GetByID<AddSPN>(lId);
				if (Penalty.PortfolioID.HasValue && Penalty.PFRBankAccountID.HasValue)
					Account = BLServiceSystem.Client.GetPortfolioFull(Penalty.PFRBankAccountID.Value, Penalty.PortfolioID.Value);

				DopSPNs = Penalty.GetDopSPNList();
				RegisterValidation();
			}
			catch (Exception e)
			{
				if (DoNotThrowExceptionOnError == false)
					throw;

				Logger.WriteException(e);
				throw new ViewModelInitializeException();
			}
		}

		public PenaltyViewModel()
		//: base(typeof(DueListViewModel))
		{
			DataObjectTypeForJournal = typeof(AddSPN);
			SelectAccount = new DelegateCommand(o => false, o => ExecuteSelectAccount());//Пенни могут появлятся в системе только через ДК, и никак иначе
			//this.AddDopSPN = new Commands.DelegateCommand(o => this.ExecuteAddDopSPN());

			Penalty = new AddSPN();
			MonthsList = DataContainerFacade.GetList<Month>();
			Penalty.Kind = 2;
			Penalty.OperationDate = DateTime.Now;
			Penalty.NewDate = DateTime.Now;
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(AddSPN), Penalty.ID) };
			return list;
		}
	}
}
