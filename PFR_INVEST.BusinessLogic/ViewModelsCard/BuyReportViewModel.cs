﻿using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
	public class BuyReportViewModel : OrderReportViewModelBase
	{
		protected override void RefreshMaxSaleOrBuySecurityCount()
		{
			if (NeedCheckBuyOrSaleReportCount)
			{
				long? availableByOrder = OrderReportsHelper.GetAvailableBuyOrSaleSecurityCountByOrder(m_CbInOrderList, m_AlreadyAddedReports, SelectedSecurity, Report);
				if (availableByOrder.HasValue)
				{
					m_MaxSaleOrBuySecurityCount = availableByOrder.Value;
					AvailableCountString = string.Format(AVAILABLE_COUNT_STRING_FORMAT, "покупки", m_MaxSaleOrBuySecurityCount);
					RaiseAvailableSaleSecurityCountChanged();
				}
				else
				{
					m_MaxSaleOrBuySecurityCount = 0;
					AvailableCountString = string.Format(AVAILABLE_COUNT_STRING_FORMAT, "покупки", AVAILABLE_COUNT_INVALID);
				}
			}
			else
			{
				m_MaxSaleOrBuySecurityCount = 0;
				AvailableCountString = null;
			}
		}

		protected override void RecalcRubSums()
		{
			TransactionAmountRub = TransactionAmount * Rate;
			OnPropertyChanged("NKDTotalRub");
			SumWithoutNKDRub = SumWithoutNKD * Rate;
		}

		protected override void RecalcTransactionAmountForPurchasePrice()
		{
		}

		#region Yield
		public decimal Yield
		{
			get
			{
				return Report != null ? Report.Yield ?? 0 : 0;
			}

			set
			{
				if (Report != null)
				{
					Report.Yield = value;
					OnPropertyChanged("Yield");
				}
			}
		}
		#endregion



		#region SelectedPortfolioBankAccount

		private string sourceAccount;
		public string SourceAccount => sourceAccount;

	    private string sourceBank;
		public string SourceBank => sourceBank;

	    private string sourcePortfolio;
		public string SourcePortfolio => sourcePortfolio;

	    private PortfolioFullListItem selectedPortfolioBankAccount;
		public PortfolioFullListItem SelectedPortfolioBankAccount
		{
			get { return selectedPortfolioBankAccount; }
			set
			{
				selectedPortfolioBankAccount = value;

				if (value != null)
				{
					sourceAccount = value.AccountNumber;
					sourceBank = value.Bank;
					sourcePortfolio = value.PFName;


					OnPropertyChanged("SelectedPortfolioBankAccount");
					OnPropertyChanged("SourceAccount");
					OnPropertyChanged("SourceBank");
					OnPropertyChanged("SourcePortfolio");

				}

			}
		}




		public ICommand SelectedPortfolioBankAccountCommand { get; set; }

		private void SelectedPortfolioBankAccountAction(object o)
		{
			PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink param = null;

			if (SelectedPortfolioBankAccount != null && SelectedPortfolioBankAccount.PortfolioPFRBankAccount != null)
			{
				param = new PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink
				{
				  PfrBankAccountID = SelectedPortfolioBankAccount.PortfolioPFRBankAccount.PFRBankAccountID.Value,
				  PortfolioID = SelectedPortfolioBankAccount.PortfolioPFRBankAccount.PortfolioID.Value
			  };
			}

			PortfolioIdentifier.PortfolioPBAParams p = new PortfolioIdentifier.PortfolioPBAParams();

			if (param != null)
			{
				p.HiddenPortfolioAccountLinks = new[] { param };
			}




			var portfolioAccount = DialogHelper.SelectPortfolio(p);

			if (portfolioAccount != null)
			{
				SelectedPortfolioBankAccount = portfolioAccount;
				Report.TotalAccountPfBaccID = SelectedPortfolioBankAccount.PortfolioPFRBankAccount.ID;
			}

		}
		#endregion



		public BuyReportViewModel(long lOrderID)
			: this(lOrderID, ViewModelState.Create)
		{
		}

		public BuyReportViewModel(long lID, ViewModelState action)
			: base(lID, action, new[] { typeof(BuyReportsListViewModel), typeof(SaleReportsListViewModel), typeof(PortfolioStatusListViewModel) })
		{
			SelectedPortfolioBankAccountCommand = new DelegateCommand(o => !isReadOnly, SelectedPortfolioBankAccountAction);

			if (Report != null && Report.TotalAccountPfBaccID.HasValue)
			{
				SelectedPortfolioBankAccount = BLServiceSystem.Client.GetPortfolioFullByPfBaccID(Report.TotalAccountPfBaccID.Value);
			}
			else if (Order != null)
			{
				if (Order.TradeAccountID.HasValue && Order.TradeAccountID.Value > 0)
				{
					SelectedPortfolioBankAccount = BLServiceSystem.Client.GetPortfolioFull(Order.TradeAccountID.Value, Order.PortfolioID.Value);

				}
				else if (Order.CurrentAccountID.HasValue && Order.CurrentAccountID.Value > 0)
				{
					SelectedPortfolioBankAccount = BLServiceSystem.Client.GetPortfolioFull(Order.CurrentAccountID.Value, Order.PortfolioID.Value);

				}

			}

		}

		#region Execute implementation
		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}
		#endregion

		#region Validation
		private const string INVALID_DATE_ORDER = "Не указана дата сделки";
		private const string INVALID_DATE_DEPO = "Не указана дата операции по счету ДЕПО";
		private const string INVALID_SELECTED_CONTRAGENT = "Не указан продавец";
		private const string INVALID_CURS = "Неверно указан курс";

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "DateOrder": return DateOrder == null ? INVALID_DATE_ORDER : null;
					case "DateDEPO": return CurrencyIsRubles
										? null
										: DateDEPO == null ? INVALID_DATE_DEPO : null;
					case "Yield": return OrderReportsHelper.CheckYield(Yield);
					case "Price": return OrderReportsHelper.CheckPrice(Price);
					case "Count":
						return OrderReportsHelper.CheckCount(Count) ?? OrderReportsHelper.CheckCountSecurity(SumNom, 17) ?? OrderReportsHelper.CheckNkdTotal(Count, NKDPerOneObl, 15);
					case "NKDPerOneObl":
						return OrderReportsHelper.CheckNkdTotal(Count, NKDPerOneObl, 15);

					case "SelectedContragent": return (!CurrencyIsRubles && string.IsNullOrEmpty(SelectedContragent))
													? INVALID_SELECTED_CONTRAGENT
													: null;
					case "Rate": return (!CurrencyIsRubles && Rate <= 0)
													? INVALID_CURS
													: null;
					default: return null;
				}
			}
		}

		public override bool Validate()
		{
			return string.IsNullOrEmpty(this["DateOrder"]) &&
				   string.IsNullOrEmpty(this["DateDEPO"]) &&
				   string.IsNullOrEmpty(this["Price"]) &&
				   string.IsNullOrEmpty(this["SelectedContragent"]) &&
				   string.IsNullOrEmpty(this["Count"]) &&
				   string.IsNullOrEmpty(this["NKDPerOneObl"]) &&
				   string.IsNullOrEmpty(this["Yield"]) &&
				   string.IsNullOrEmpty(this["Rate"]);
		}
		#endregion
	}
}
