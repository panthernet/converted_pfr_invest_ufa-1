﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class SaleReportViewModel : OrderReportViewModelBase
    {
        protected override void RefreshMaxSaleOrBuySecurityCount()
        {
            if (NeedCheckBuyOrSaleReportCount)
            {if (CurrencyIsRubles)
                {
                    //Проверка рублевых продаж по количесву штук в портфеле и согласно поручению

                    //Доступно для продажи согласно поручению
                    long? availableByOrder = OrderReportsHelper.GetAvailableBuyOrSaleSecurityCountByOrder(m_CbInOrderList, m_AlreadyAddedReports, SelectedSecurity, Report);
                    if (availableByOrder.HasValue)
                    {
                        //Доступно для продажи по портфелю (сколько всего таких ЦБ куплено в этот портфель)
                        long availableByPortfolio = OrderReportsHelper.GetAvailableSaleSecurityCountByPortfolio(Report, BuyReports, SaleReports, CurrencyIsRubles);
                        //Итого доступно - минимум между доступным по портфелю или поручению
                        m_MaxSaleOrBuySecurityCount = Math.Min(availableByOrder.Value, availableByPortfolio);
                        AvailableCountString = string.Format(AVAILABLE_COUNT_STRING_FORMAT, "продажи", m_MaxSaleOrBuySecurityCount);
                    }
                    else
                    {
                        m_MaxSaleOrBuySecurityCount = 0;
                        AvailableCountString = string.Format(AVAILABLE_COUNT_STRING_FORMAT, "продажи", AVAILABLE_COUNT_INVALID);
                    }
                }
                else
                {
                    //Проверка валютных продаж - только по количеству штук в портфеле

                    //Доступно для продажи по портфелю (сколько всего таких ЦБ куплено в этот портфель)
                    m_MaxSaleOrBuySecurityCount = OrderReportsHelper.GetAvailableSaleSecurityCountByPortfolio(Report, BuyReports, SaleReports, CurrencyIsRubles);
                    AvailableCountString = string.Format(AVAILABLE_COUNT_STRING_FORMAT, "продажи", m_MaxSaleOrBuySecurityCount);
                }

                RaiseAvailableSaleSecurityCountChanged();
            }
            else
            {
                m_MaxSaleOrBuySecurityCount = 0;
                AvailableCountString = null;
            }
        }

        protected override void RecalcRubSums()
        {
            TransactionAmountRub = TransactionAmount * Rate;
            OnPropertyChanged("TransactionAmountForPurchasePriceRub");
            OnPropertyChanged("NKDTotalRub");
            SumWithoutNKDRub = SumWithoutNKD * Rate;
        }

        Dictionary<long, List<OrdReport>> BuyReportsDic = new Dictionary<long, List<OrdReport>>();
        protected List<OrdReport> BuyReports
        {
            get
            {
                if (!BuyReportsDic.ContainsKey(SelectedSecurity.ID))
                    BuyReportsDic[SelectedSecurity.ID] = BLServiceSystem.Client.GetOrdReportsBySecurityPortfolioAndType(SelectedSecurity.ID, Order.PortfolioID.Value, OrderTypeIdentifier.BUYING);
                return BuyReportsDic[SelectedSecurity.ID];
            }
        }

        Dictionary<long, List<OrdReport>> SaleReportsDic = new Dictionary<long, List<OrdReport>>();
        protected List<OrdReport> SaleReports
        {
            get
            {
                if (!SaleReportsDic.ContainsKey(SelectedSecurity.ID))
                    SaleReportsDic[SelectedSecurity.ID] = BLServiceSystem.Client.GetOrdReportsBySecurityPortfolioAndType(SelectedSecurity.ID, Order.PortfolioID.Value, OrderTypeIdentifier.SELLING);
                return SaleReportsDic[SelectedSecurity.ID];
            }
        }

        #region TransactionAmountForPurchasePrice
        protected override void RecalcTransactionAmountForPurchasePrice()
        {
            if (string.IsNullOrEmpty(this["Count"]))
            {
                if (SelectedSecurity != null && Order.PortfolioID.HasValue)
                    CalculatedTransactionAmountForPurchasePrice = OrderReportsHelper.CalcTransactionAmountForPurchasePrice(Report, SelectedSecurity, BuyReports, SaleReports);
                else
                    CalculatedTransactionAmountForPurchasePrice = 0;
            }
            else
                CalculatedTransactionAmountForPurchasePrice = 0;
            OnPropertyChanged("TransactionAmountForPurchasePrice");
            OnPropertyChanged("TransactionAmountForPurchasePriceRub");
        }

        private decimal CalculatedTransactionAmountForPurchasePrice { get; set; }


        public decimal TransactionAmountForPurchasePrice
        {
            get 
            { 
                return Report.SumBuy ?? CalculatedTransactionAmountForPurchasePrice; 
            }
            set
            {
                if (Report.SumBuy != value)
                {
                    Report.SumBuy = value;
                    OnPropertyChanged("TransactionAmountForPurchasePrice");
                    OnPropertyChanged("TransactionAmountForPurchasePriceRub");
                }
            }
        }


        public decimal TransactionAmountForPurchasePriceRub
        {
            get { return Report.SumBuyRub ?? (CalculatedTransactionAmountForPurchasePrice * Rate); }
            set
            {
                if (Report.SumBuyRub != value)
                {
                    Report.SumBuyRub = value;
                    OnPropertyChanged("TransactionAmountForPurchasePriceRub");
                }
            }
        }
        #endregion

        public SaleReportViewModel(long lOrderID)
            : this(lOrderID, ViewModelState.Create)
        {
        }

        public SaleReportViewModel(long lID, ViewModelState action)
            : base(lID, action, new[] { typeof(SaleReportsListViewModel), typeof(BuyReportsListViewModel), typeof(PortfolioStatusListViewModel) })
        {
        }

        #region Execute implementation
        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }
        #endregion

        #region Validation
        private const string INVALID_DATE_ORDER = "Не указана дата сделки";
        private const string INVALID_DATE_DEPO = "Не указана дата операции по счету ДЕПО";
        private const string INVALID_SELECTED_CONTRAGENT = "Не указан покупатель";
        private const string INVALID_CURS = "Неверно указан курс";

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "DateOrder": return DateOrder == null ? INVALID_DATE_ORDER : null;
                    case "DateDEPO": return CurrencyIsRubles
                                            ? null
                                            : DateDEPO == null ? INVALID_DATE_DEPO : null;
                    case "Price": return OrderReportsHelper.CheckPrice(Price);
                    case "Count":
                        return OrderReportsHelper.CheckCount(Count) ?? OrderReportsHelper.CheckCountSecurity(SumNom,17) ?? OrderReportsHelper.CheckNkdTotal(Count, NKDPerOneObl, 15 );
                    case "NKDPerOneObl":
                        return OrderReportsHelper.CheckNkdTotal(Count, NKDPerOneObl, 15);

                    case "SelectedContragent": return (!CurrencyIsRubles && string.IsNullOrEmpty(SelectedContragent))
                                                    ? INVALID_SELECTED_CONTRAGENT
                                                    : null;
                    case "Rate": return (!CurrencyIsRubles && Rate <= 0)
                                                ? INVALID_CURS
                                                : null;
                    default: return null;
                }
            }
        }

        public override bool Validate()
        {
            return string.IsNullOrEmpty(this["DateOrder"]) &&
                   string.IsNullOrEmpty(this["DateDEPO"]) &&
                   string.IsNullOrEmpty(this["Price"]) &&
                   string.IsNullOrEmpty(this["SelectedContragent"]) &&
                   string.IsNullOrEmpty(this["Count"]) &&
                   string.IsNullOrEmpty(this["NKDPerOneObl"]) &&
                   string.IsNullOrEmpty(this["Rate"]);
        }
        #endregion
    }
}
