﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class PFRAccountViewModel : ViewModelCard
    {
        private PFRAccountsListViewModel _pfrAccountsListViewModel = new PFRAccountsListViewModel();
        

        private List<LegalEntity> _legalEntitiesList = new List<LegalEntity>();

        private List<AccBankType> _typesList = new List<AccBankType>();
        private List<Currency> _currencyList = new List<Currency>();

        public List<LegalEntity> LegalEntitiesList
        {
            get { return _legalEntitiesList; }
            set { _legalEntitiesList = value; OnPropertyChanged("LegalEntitiesList"); }
        }

        public List<AccBankType> TypesList
        {
            get { return _typesList; }
            set { _typesList = value; OnPropertyChanged("TypesList"); }
        }

        public List<Currency> CurrencyList
        {
            get { return _currencyList; }
            set { _currencyList = value; OnPropertyChanged("CurrencyList"); }
        }

        public LegalEntity SelectedLegalEntity
        {
            get { return _legalEntitiesList.FirstOrDefault(le => BA.LegalEntityBankID != null && le.ID == BA.LegalEntityBankID); }
            set
            {
                if (value != null) BA.LegalEntityBankID = value.ID;
                OnPropertyChanged("SelectedLegalEntity");
            }
        }

        public AccBankType SelectedType
        {
            get { return _typesList.FirstOrDefault(type => BA.AccountTypeID != null && type.ID == BA.AccountTypeID); }
            set
            {
                BA.AccountTypeID = value.ID;
                OnPropertyChanged("SelectedType");
				OnPropertyChanged("IsDEPO");
            }
        }

    	public bool IsDEPO => SelectedType != null && SelectedType.Name == "ДЕПО";

        public string SelectedStatus
        {
            get
            {
                return BA.Status.Trim();
            }

            set
            {
                BA.Status = value.Trim();
                OnPropertyChanged("SelectedStatus");
            }
        }

        public Currency SelectedCurrency
        {
            get { return _currencyList.FirstOrDefault(curr => BA.CurrencyID != null && curr.ID == BA.CurrencyID); }
            set
            {
                if (value == null) return;
                BA.CurrencyID = value.ID;
                OnPropertyChanged("SelectedCurrency");
            }
        }

        public string BAAccountNumber
        {
            get { return BA.AccountNumber; }
            set { BA.AccountNumber = value; OnPropertyChanged("BAAccountNumber"); }
        }

        public string BAStatus
        {
            get { return BA.Status; }
            set { BA.Status = value; OnPropertyChanged("BAStatus"); }
        }

        private void Load(long id)
        {
            try
            {
                BA = DataContainerFacade.GetByID<PfrBankAccount>(id);

                OnPropertyChanged("SelectedType");
                OnPropertyChanged("SelectedCurrency");
                OnPropertyChanged("SelectedLegalEntity");
                ID = id;
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public void RefreshLists()
        {
            try
            {
                var cards = BLServiceSystem.Client.GetBankAgentListForDeposit().Where(a => a.ShortName.Trim() != "ММВБ").ToList();

                if (cards.Count > 0)
                    LegalEntitiesList = new List<LegalEntity>(cards);

                TypesList = BLServiceSystem.Client.GetAccBankTypes();
                CurrencyList = BLServiceSystem.Client.GetCurrencyListHib();

            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        #region Properies

        private PfrBankAccount _ba;
        public PfrBankAccount BA 
        { 
            get { return _ba; } 
            set { _ba = value; OnPropertyChanged("BA"); } 
        }

        public DateTime? OpenDate
        {
            get { return BA.OpenDate; }
            set
            {
                if (BA.OpenDate == value) return;
                BA.OpenDate = value;
                OnPropertyChanged("OpenDate");
            }
        }

        public DateTime? CloseDate
        {
            get { return BA.CloseDate; }
            set
            {
                if (BA.CloseDate == value) return;
                BA.CloseDate = value;
                OnPropertyChanged("CloseDate");
            }
        }

        #endregion Properies

        #region Execute Implementations


        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && BA.StatusID != -1;
        }

        public void ExecuteDelete()
        {
            ExecuteDelete(DocOperation.Archive);
        }

        public override bool BeforeExecuteDeleteCheck()
        {
            if (!BLServiceSystem.Client.IsUseAccountInBalanceList(BA.ID)) return true;
            DialogHelper.ShowError("Выбранный счет удалить нельзя, т.к. он используется в Бэк-офис/Счета/Счета !!!");
            return false;
        }

        protected override void ExecuteDelete(int delType)
        {
            BA.StatusID = -1;
            DataContainerFacade.Save<PfrBankAccount, long>(BA);
            base.ExecuteDelete(DocOperation.Archive);
        }


        protected override bool BeforeExecuteSaveCheck()
        {
            _pfrAccountsListViewModel = new PFRAccountsListViewModel();
            if (!BAAccountNumberAlreadyExists()) return base.BeforeExecuteSaveCheck();
            DialogHelper.ShowError("Этот номер счета уже существует");
            OnPropertyChanged("BAAccountNumber");

            return base.BeforeExecuteSaveCheck();
        }

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                        DataContainerFacade.Save<PfrBankAccount, long>(BA);
                        break;
                    case ViewModelState.Create:
                        BA.StatusID = 0;
                        ID = BA.ID = DataContainerFacade.Save<PfrBankAccount, long>(BA);
                        break;
                }
            }
        }

        public override bool CanExecuteSave()
        {
            return String.IsNullOrEmpty(Validate()) && IsDataChanged;
        }
        #endregion

		private const string ACCOUNT_NUMBER_INVALID_VALUE = "Номер счета заполнен неверно";
		private const string REQUIRED_FIELD_WARNING = "Поле обязательно для заполнения";

		private string AccountNumberStorageMask => @"\d{20}";

        public string AccountNumberPresentationMask => @"\d{1} \d{2} \d{2} \d{3} \d{1} \d{4} \d{3} \d{4}";

        public override string this[string columnName]
        {
            get
            {
                switch (columnName.ToUpper())
                {
                    case "SELECTEDTYPE":
                    case "TYPE_ID":
                        if (_ba.AccountTypeID == null || _ba.AccountTypeID < 1)
                            return DataTools.DATAERROR_NOT_SELECTED;
                        return null;

                    case "BAACCOUNTNUMBER":
						if (string.IsNullOrEmpty(BAAccountNumber))
							return DataTools.DATAERROR_NEED_DATA;

						// PFR00002026 Счета ДЕПО тоже нужно проверять на наличие дубликатов
						// if (_ba.AccountTypeID != 4)
						if (BAAccountNumberAlreadyExists())
							return "Этот номер счета уже существует";

						if (IsDEPO)
							return null;

						var regex = new Regex(AccountNumberStorageMask);
						return regex.IsMatch(BAAccountNumber) 
							? null
							: ACCOUNT_NUMBER_INVALID_VALUE;
						
                    case "STATUS":
                        return string.IsNullOrEmpty(_ba.Status) ? DataTools.DATAERROR_NOT_SELECTED : null;
                    case "SELECTEDCURRENCY": return SelectedCurrency == null ? DataTools.DATAERROR_NOT_SELECTED : null;
                    case "SELECTEDLEGALENTITY":
                    case "LE_BANK_ID": return (_ba.LegalEntityBankID == null || _ba.LegalEntityBankID < 1) ? DataTools.DATAERROR_NOT_SELECTED : null;
                    case "OPENDATE": return _ba.OpenDate > _ba.CloseDate ? string.Empty : null;
                    case "CLOSEDATE": return _ba.CloseDate < _ba.OpenDate ? string.Empty : null;
                    default: return null;
                }
            }
        }

        private bool BAAccountNumberAlreadyExists()
        {
            var pfrAccounts = _pfrAccountsListViewModel.PFRAccountsList;
        	return pfrAccounts.Any(account => account.AccountNumber == BAAccountNumber && account.ID != BA.ID);
        }

        private string Validate()
        {
            const string fields = "BAAccountNumber|STATUS|SELECTEDCURRENCY|SELECTEDTYPE|SELECTEDLEGALENTITY|OPENDATE|CLOSEDATE";
            return fields.Split('|').Select(f => this[f]).FirstOrDefault(res => !string.IsNullOrEmpty(res));
        }

        public PFRAccountViewModel(long recordId)
            : base(typeof(PFRAccountsListViewModel))
        {
            DataObjectTypeForJournal = typeof(PfrBankAccount);
            //this.Fields = BLServiceSystem.Client.GetPFRAccountStructure();
            if (BA == null)
                BA = new PfrBankAccount();
            BA.Status = "Активный";
            OnPropertyChanged("BA");
            //this.SelectedCurrency = CurrencyList[0];
            BA.PropertyChanged += (sender, e) =>
            {
                IsDataChanged = true;
                OnPropertyChanged("BA");
            };
            RefreshLists();

            SelectedCurrency = CurrencyList.Find(a => CurrencyIdentifier.IsRUB(a.Name));

            if (recordId > 0) Load(recordId);

            IsDataChanged = false;

        }
    }
}
