﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class BudgetCorrectionViewModel : ViewModelCard
    {
        public BudgetCorrection Correction { get; private set; }

        private List<BudgetListItem> _mBudgetsList;
        public List<BudgetListItem> BudgetsList
        {
            get
            {
                return _mBudgetsList;
            }
            set
            {
                _mBudgetsList = value;
                OnPropertyChanged("BudgetsList");
            }
        }

        private BudgetListItem _mSelectedBudget;
        public BudgetListItem SelectedBudget
        {
            get
            {
                return _mSelectedBudget;
            }
            set
            {
                _mSelectedBudget = value;
                if (value != null)
                {
                    Correction.BudgetID = value.ID;
                }
                OnPropertyChanged("SelectedBudget");
            }
        }

        public DateTime? Date
        {
            get
            {
                return Correction.Date;
            }
            set
            {
                Correction.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public DateTime? DIDate
        {
            get
            {
                return Correction.DIDate;
            }
            set
            {
                Correction.DIDate = value;
                OnPropertyChanged("DIDate");
            }
        }

        public string Foundation
        {
            get
            {
                return Correction.Foundation;
            }
            set
            {
                Correction.Foundation = value;
                OnPropertyChanged("Foundation");
            }
        }

        public decimal? Sum
        {
            get
            {
                return Correction.Sum;
            }
            set
            {
                Correction.Sum = value;
                OnPropertyChanged("Sum");
            }
        }

        public string Comment
        {
            get
            {
                return Correction.Comment;
            }
            set
            {
                Correction.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(Correction);
            base.ExecuteDelete(DocOperation.Delete);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<BudgetCorrection, long>(Correction);
                    break;
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save<BudgetCorrection, long>(Correction);
                    Correction.ID = ID;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        private const string INVALID_DATE = "Не указана дата";
        private const string INVALID_SUM = "Неверно указана сумма";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Date": return Date != null ? null : INVALID_DATE;
                    case "Sum": return Sum.HasValue ? null : INVALID_SUM;
                    case "Foundation": return Foundation.ValidateMaxLength(512);
                    case "Comment": return Comment.ValidateMaxLength(512);
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return ValidateFields();
        }

        public void LoadBudget(long lID)
        {
            SelectedBudget = BudgetsList.Find(b => b.ID == lID);
        }

        public void Load(long lID)
        {
            try
            {
                ID = lID;

                Correction = DataContainerFacade.GetByID<BudgetCorrection, long>(lID);

                if (Correction.BudgetID.HasValue)
                    LoadBudget(Correction.BudgetID.Value);
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public BudgetCorrectionViewModel()
            : base(typeof(SchilsCostsListViewModel))
        {
            DataObjectTypeForJournal = typeof(BudgetCorrection);    

            try
            {
                BudgetsList = BLServiceSystem.Client.GetBudgetsListHib().ConvertAll(b => new BudgetListItem(b));
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }

            Correction = new BudgetCorrection();

            SelectedBudget = BudgetsList.FirstOrDefault();

            Date = DateTime.Now;

            RefreshConnectedCardsViewModels = RefreshParentViewModel;
        }

        private static void RefreshParentViewModel()
        {
            if (GetViewModel == null) return;
            var vm = GetViewModel(typeof(BudgetViewModel)) as BudgetViewModel;
            if (vm != null)
                vm.RefreshCorrections();
        }
    }
}