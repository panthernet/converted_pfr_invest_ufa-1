﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class PensionNotificationViewModel : ViewModelCard, IRequestCloseViewModel
	{
		private PensionNotification pensionNotification;

        public long? PensionNotificationID
		{
			get
			{
			    if (pensionNotification != null)
					return pensionNotification.ID;
			    return null;
			}
		}

		private List<LegalEntity> npfList = new List<LegalEntity>();

		public List<LegalEntity> NPFList
		{
			get { return npfList; }
			set
			{
				npfList = value;
				OnPropertyChanged("NPFList");
			}
		}

		private LegalEntity selectedNPF;
		public LegalEntity SelectedNPF
		{
			get { return selectedNPF; }
			set
			{
				selectedNPF = value;
                pensionNotification.SenderID = selectedNPF.ID;
				OnPropertyChanged("SelectedNPF");
			}
		}

        private List<Element> pensionTypeList = new List<Element>();
        public List<Element> PensionTypeList
		{
            get { return pensionTypeList; }
			set
			{
                pensionTypeList = value;
                OnPropertyChanged("PensionTypeList");
			}
		}

        private Element selectedPensionType;
        public Element SelectedPensionType
        {
            get { return selectedPensionType; }
            set
            {
                selectedPensionType = value;
                pensionNotification.NotifTypeID = selectedPensionType.ID;
                OnPropertyChanged("SelectedPensionType");
            }
        }

        public DateTime? DeliveryDate
        {
            get {
                return pensionNotification != null ? pensionNotification.DeliveryDate : null;
            }

            set
            {
                if (pensionNotification == null) return;
                pensionNotification.DeliveryDate = value;
                OnPropertyChanged("DeliveryDate");
            }
        }

        public string NotifNum
		{
			get {
			    return pensionNotification != null ? pensionNotification.NotifNum : string.Empty;
			}

            set
			{
			    if (pensionNotification == null) return;
			    pensionNotification.NotifNum = value;
			    OnPropertyChanged("NotifNum");
			}
		}

        public DateTime? NotifDate
		{
			get {
			    return pensionNotification != null ? pensionNotification.NotifDate : null;
			}

            set
			{
			    if (pensionNotification == null) return;
			    pensionNotification.NotifDate = value;
			    OnPropertyChanged("NotifDate");
			}
		}
		
        public string RegisterNum
		{
			get {
			    return pensionNotification != null ? pensionNotification.RegisterNum : string.Empty;
			}

            set
			{
			    if (pensionNotification == null) return;
			    pensionNotification.RegisterNum = value;
			    OnPropertyChanged("RegisterNum");
			}
		}

        public DateTime? RegisterDate
		{
			get {
			    return pensionNotification != null ? pensionNotification.RegisterDate : null;
			}

            set
			{
			    if (pensionNotification == null) return;
			    pensionNotification.RegisterDate = value;
			    OnPropertyChanged("RegisterDate");
			}
		}

		#region Execute Implementations

		public override bool CanExecuteDelete()
		{
			return State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType)
		{
			var e = pensionNotification.ExtensionData;
			pensionNotification.ExtensionData = null;
            DataContainerFacade.Delete(pensionNotification);
			pensionNotification.ExtensionData = e;
			base.ExecuteDelete(DocOperation.Archive);
		}

		public override bool CanExecuteSave()
		{
			return String.IsNullOrEmpty(Validate()) && IsDataChanged;
		}

        protected override void ExecuteSave()
        {
            if (selectedNPF != null && pensionNotification != null)
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:

                        var ee = pensionNotification.ExtensionData;
                        pensionNotification.ExtensionData = null;

                        ID = DataContainerFacade.Save<PensionNotification, long>(pensionNotification);
                        pensionNotification.ExtensionData = ee;

                        break;
                    case ViewModelState.Create:

                        var ce = pensionNotification.ExtensionData;
                        pensionNotification.ExtensionData = null;
                        ID = pensionNotification.ID = DataContainerFacade.Save<PensionNotification, long>(pensionNotification);
                        pensionNotification.ExtensionData = ce;

                        break;
                    default:
                        break;
                }
            }
        }
		
		#endregion

		public bool isReadOnly => State == ViewModelState.Read;

	    public bool isEnabled => !isReadOnly;

	    public void RefreshConnectedCards()
		{
		}

        public PensionNotificationViewModel() : this(0, ViewModelState.Create) { }

        public PensionNotificationViewModel(long id, ViewModelState action)
            : base(new[] { typeof(PensionNotificationListViewModel) })
		{
            DataObjectTypeForJournal = typeof(PensionNotification);
            State = action;

			RefreshConnectedCardsViewModels = RefreshConnectedCards;

            NPFList = BLServiceSystem.Client.GetNPFListLEHib(new StatusFilter()).ToList();
            PensionTypeList = BLServiceSystem.Client.GetElementByType(Element.Types.PensionNotificationType).Where(x => x.Visible).ToList();

			if (action == ViewModelState.Create)
			{
				pensionNotification = new PensionNotification();

                SelectedNPF = NPFList.FirstOrDefault();
                SelectedPensionType = PensionTypeList.FirstOrDefault();

                DeliveryDate = NotifDate = DateTime.Now;

			}
			else
			{
				ID = id;

				pensionNotification = DataContainerFacade.GetByID<PensionNotification, long>(id);

				if (npfList != null && pensionNotification != null)
				{

                    selectedNPF = npfList.FirstOrDefault(npf => npf.ID == pensionNotification.SenderID);
                    SelectedPensionType = PensionTypeList.FirstOrDefault(x => x.ID == pensionNotification.NotifTypeID.Value);
				}
			}
			IsDataChanged = false;
		}

		public override string this[string columnName]
		{
			get
			{
                switch (columnName.ToLower())
                {
                    case "notifnum":
                        if (string.IsNullOrEmpty(NotifNum))
                            return "Поле обязательное для заполнения";
                        if (NotifNum.Trim().Length > 100)
                            return "Максимальная длина 100 символов";
                        break;
                    case "selectednpf":
                        if (SelectedNPF == null)
                            return "Выберите отправителя";
                        break;
                    case "selectedpensiontype":
                        if (SelectedPensionType == null)
                            return "Выберите тип уведомления";
                        break;
                    case "deliverydate":
                        if (DeliveryDate == null || !DeliveryDate.HasValue || DeliveryDate.Value == DateTime.MinValue)
                            return "Заполните дату поступления";
                        break;
                    case "notifdate":
                        if (NotifDate == null || !NotifDate.HasValue || NotifDate.Value == DateTime.MinValue)
                            return "Заполните дату уведомления";
                        break;

                    default: return null;
                }

                return null;
			}
		}

		private string Validate()
		{
			const string fieldNames =
                "selectednpf|notifnum|selectedpensiontype|deliverydate|notifdate";

			foreach (string fieldName in fieldNames.Split("|".ToCharArray()))
			{
				if (!String.IsNullOrEmpty(this[fieldName]))
				{
					return "Неверный формат данных";
				}
			}

			return null;
		}

		public event EventHandler RequestClose;

		public void CloseWindow()
		{
			if (RequestClose != null)
			{
                // необходимо заблокировать запрос о сохранении при закрытии окна
                Deleting = true;
				RequestClose(this, EventArgs.Empty);
			}
		}
	}
}
