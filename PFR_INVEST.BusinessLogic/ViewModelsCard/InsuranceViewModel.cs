﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    //[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    //[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    //[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public abstract class InsuranceViewModel : ViewModelCard
    {
        public InsuranceDoc InsuranceDoc { get; private set; }

        #region Contract
        List<Contract> _mContracts;
        public List<Contract> Contracts
        {
            get
            {
                return _mContracts;
            }
            set
            {
                _mContracts = value;
                //if (value != null)
                //{
                //    SelectedContract = m_Contracts.FirstOrDefault();
                //}
                //else
                //    SelectedContract = null;
                OnPropertyChanged("Contracts");
            }
        }

        private Contract _mSelectedContract;
        public Contract SelectedContract
        {
            get
            {
                return _mSelectedContract;
            }
            set
            {
                _mSelectedContract = value;
                if (value != null)
                {
                    InsuranceDoc.ContractID = _mSelectedContract.ID;
                }
                else
                    InsuranceDoc.ContractID = null;
                OnPropertyChanged("SelectedContract");
            }
        }
        #endregion

        #region UK
        private List<LegalEntity> _mUKList;
        public List<LegalEntity> UKList
        {
            get
            {
                return _mUKList;
            }
            set
            {
                _mUKList = value;
                OnPropertyChanged("UKList");
            }
        }

        private LegalEntity _mSelectedUK;
        public LegalEntity SelectedUK
        {
            get
            {
                return _mSelectedUK;
            }

            set
            {
                _mSelectedUK = value;
                Contracts = _mSelectedUK != null ? _mSelectedUK.GetContracts().Where(c => c.TypeID == _conrtactType && c.Status != -1).ToList() : null;
                OnPropertyChanged("SelectedUK");
            }
        }
        #endregion

        public decimal Sum
        {
            get
            {
                return InsuranceDoc.Sum ?? 0;
            }
            set
            {
                InsuranceDoc.Sum = value;
                OnPropertyChanged("Sum");
            }
        }

        public string Insurance
        {
            get
            {
                return InsuranceDoc.Insurance;
            }
            set
            {
                InsuranceDoc.Insurance = value;
                OnPropertyChanged("Insurance");
            }
        }

        public string ReqNum
        {
            get {
                return InsuranceDoc.ReqNum != null ? InsuranceDoc.ReqNum.Trim() : null;
            }
            set
            {
                InsuranceDoc.ReqNum = value;
                OnPropertyChanged("ReqNum");
            }
        }

        public DateTime? ReqDate
        {
            get
            {
                return InsuranceDoc.ReqDate;
            }
            set
            {
                InsuranceDoc.ReqDate = value;
                OnPropertyChanged("ReqDate");
            }
        }

        public DateTime? CloseDate
        {
            get
            {
                return InsuranceDoc.CloseDate;
            }
            set
            {
                InsuranceDoc.CloseDate = value;
                OnPropertyChanged("CloseDate");
            }
        }

        public string DocSum
        {
            get
            {
                return InsuranceDoc.DocSum;
            }
            set
            {
                InsuranceDoc.DocSum = value;
                OnPropertyChanged("DocSum");
            }
        }

        public DateTime? ChangeDate
        {
            get
            {
                return InsuranceDoc.ChangeDate;
            }
            set
            {
                InsuranceDoc.ChangeDate = value;
                OnPropertyChanged("ChangeDate");
            }
        }

        private readonly int _conrtactType;

        protected InsuranceViewModel(ViewModelState action,Document.Types conrtactType, long id = 0)
            : base(typeof(InsuranceListViewModel), typeof(InsuranceSIListViewModel), typeof(InsuranceVRListViewModel), typeof(InsuranceArchiveVRListViewModel), typeof(InsuranceArchiveSIListViewModel))
        {
            DataObjectTypeForJournal = typeof(InsuranceDoc);

            _conrtactType = (int)conrtactType;
            // используется метод выборки с привязкой к перечислениям, так как страхуется определенная сумма
            UKList = BLServiceSystem.Client.GetActiveUKListTransferHib(_conrtactType); 

            State = action;

            if (State == ViewModelState.Create)
            {
                InsuranceDoc = new InsuranceDoc {TypeID = (int) conrtactType};
                ReqDate = DateTime.Today;
                ChangeDate = DateTime.Today;
                DocSum = "Дополнительное соглашение";
                CloseDate = DateTime.Today;
            }
            else
            {
                ID = id;
                InsuranceDoc = DataContainerFacade.GetByID<InsuranceDoc>(ID);
                var c = DataContainerFacade.GetByID<Contract>(InsuranceDoc.ContractID);
                //если текущий УК документа отсутствует в списке, добавляем его
                if (UKList.Find(a => a.ID == c.LegalEntityID) == null)
                    UKList.Add(c.GetLegalEntity());

                SelectedUK = UKList.FirstOrDefault(uk => uk.ID == c.LegalEntityID);
                SelectedContract = SelectedUK.GetContracts().FirstOrDefault(con => con.ID == InsuranceDoc.ContractID);
            }
        }


        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType = 1)
        {
            DataContainerFacade.Delete(InsuranceDoc);
            base.ExecuteDelete(DocOperation.Delete);
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Create:
                case ViewModelState.Edit:
                    ID = InsuranceDoc.ID = DataContainerFacade.Save(InsuranceDoc);
                    break;
            }
        }


        private bool Validate()
        {
            return string.IsNullOrEmpty(this["Sum"]) &&
                   string.IsNullOrEmpty(this["SelectedContract"]) &&
                   string.IsNullOrEmpty(this["DocSum"]) &&
                   string.IsNullOrEmpty(this["Insurance"]) &&
                   string.IsNullOrEmpty(this["ReqNum"]);
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Sum": return Sum <= 0 || Sum > (decimal)999999999999999.99 ? "Неверно заполнена сумма" : null;
                    case "SelectedContract": return SelectedContract == null ? "Не выбран договор" : null;
                    case "DocSum": return string.IsNullOrWhiteSpace(DocSum) ? "Не заполнено поле" : null;
                    case "Insurance": return string.IsNullOrWhiteSpace(Insurance) ? "Не заполнено поле" : null;
                    case "ReqNum": return string.IsNullOrWhiteSpace(ReqNum) ? "Не заполнено поле" : null;
                    default: return null;
                }
            }
        }
    }
}
