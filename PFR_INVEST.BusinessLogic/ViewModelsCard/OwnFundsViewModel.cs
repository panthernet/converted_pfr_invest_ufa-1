﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
	public class OwnFundsViewModel : ViewModelCard, INotifyPropertyChanged
	{
		private OwnedFounds _ownedFounds;

		private List<Year> m_YearsList;
		public List<Year> YearsList
		{
			get { return m_YearsList; }
			set
			{
				m_YearsList = value;
				OnPropertyChanged("YearsList");
			}
		}

		private Year _mSelectedYear;
		public Year SelectedYear
		{
			get
			{
				return _mSelectedYear;
			}
			set
			{
				if (value != null)
				{
					_ownedFounds.YearID = value.ID;
					_mSelectedYear = value;
					OnPropertyChanged("SelectedYear");
					RefreshUKList();
				}
			}
		}

		private int[] _mQuartersList;
		public int[] QuartersList
		{
			get
			{
				return _mQuartersList;
			}
			set
			{
				_mQuartersList = value;
				OnPropertyChanged("QuartersList");
			}
		}

		public int? SelectedQuartal
		{
			get
			{
				return _ownedFounds.Quartal;
			}
			set
			{
				if (value != null)
				{
					_ownedFounds.Quartal = value;
					OnPropertyChanged("SelectedQuartal");
					RefreshUKList();
				}
			}
		}

		private void RefreshOnEdit()
		{
			if (State == ViewModelState.Edit && SelectedUK != null)
				RefreshHistoryList();
		}

		private readonly List<LegalEntity> _ukFullList;
		private void RefreshUKList(long? activeByID = null)
		{
			if (!activeByID.HasValue && (SelectedYear == null || SelectedQuartal == null))
				return;

			var ofl = DataContainerFacade.GetList<OwnedFounds>();
		    var busyUkList = ofl.Where(l => l.YearID == SelectedYear.ID && l.Quartal == SelectedQuartal).Select(a => a.LegalEntityID).Distinct().ToList();
		    if (activeByID.HasValue)
		    {
		        UKList = _ukActiveList.Where(a => !busyUkList.Contains(a.ID)).ToList();
                _mSelectedUK = _ukFullList.FirstOrDefault(uk => uk.ID == _ownedFounds.LegalEntityID);
                if(_mSelectedUK != null && !UKList.Contains(_mSelectedUK))
                    UKList.Add(_mSelectedUK);
		    }
		    else
		    {
                UKList = _ukActiveList.Where(a => !busyUkList.Contains(a.ID)).ToList();
                SelectedUK = UKList.FirstOrDefault();
            }

		}

		private List<LegalEntity> _mUKList;
		public List<LegalEntity> UKList
		{
			get
			{
				return _mUKList;
			}
			set
			{
				_mUKList = value;
				OnPropertyChanged("UKList");
			}
		}

		//private LegalEntity s_UK;
		private LegalEntity _mSelectedUK;
		public LegalEntity SelectedUK
		{
			get
			{
				return _mSelectedUK;
			}
			set
			{
				if (value != null)
				{
					_mSelectedUK = value;
					_ownedFounds.LegalEntityID = _mSelectedUK.ID;
					RefreshOnEdit();
					OnPropertyChanged("SelectedUK");
					//s_UK = m_SelectedUK;
				}
			}
		}

		public decimal Capital
		{
			get
			{
				return _ownedFounds.Capital ?? 0;
			}
			set
			{
				_ownedFounds.Capital = value;
				OnPropertyChanged("Capital");
			}
		}

		public decimal CountPay
		{
			get
			{
				return _ownedFounds.CountPay ?? 0;
			}
			set
			{
				_ownedFounds.CountPay = value;
				OnPropertyChanged("CountPay");
			}
		}

		public decimal CountDogRez
		{
			get
			{
				return _ownedFounds.CountDogRez ?? 0;
			}
			set
			{
				_ownedFounds.CountDogRez = value;
				OnPropertyChanged("CountDogRez");
			}
		}

		public decimal CountDogAccum
		{
			get
			{
				return _ownedFounds.CountDogAccum ?? 0;
			}
			set
			{
				_ownedFounds.CountDogAccum = value;
				OnPropertyChanged("CountDogAccum");
			}
		}

		public decimal CostPay
		{
			get
			{
				return _ownedFounds.CostPay ?? 0;
			}
			set
			{
				_ownedFounds.CostPay = value;
				OnPropertyChanged("CostPay");
			}
		}

		public decimal CostRez
		{
			get
			{
				return _ownedFounds.CostRez ?? 0;
			}
			set
			{
				_ownedFounds.CostRez = value;
				OnPropertyChanged("CostRez");
			}
		}

		public decimal CostInvestNPF
		{
			get
			{
				return _ownedFounds.CostInvestNPF ?? 0;
			}
			set
			{
				_ownedFounds.CostInvestNPF = value;
				OnPropertyChanged("CostInvestNPF");
			}
		}

		public decimal CostInvestPFR
		{
			get
			{
				return _ownedFounds.CostInvestPFR ?? 0;
			}
			set
			{
				_ownedFounds.CostInvestPFR = value;
				OnPropertyChanged("CostInvestPFR");
			}
		}

		List<OwnedFounds> _mHistoryList;
	    private readonly List<LegalEntity> _ukActiveList;

	    public List<OwnedFounds> HistoryList
		{
			get
			{
				return _mHistoryList;
			}
			set
			{
				_mHistoryList = value;
				OnPropertyChanged("HistoryList");
			}
		}

		void RefreshHistoryList()
		{
			bool wasChanged = IsDataChanged;
			HistoryList = BLServiceSystem.Client.GetOwnedFoundsHistoryList(SelectedYear.ID, SelectedQuartal.Value, SelectedUK.ID, ID);
			IsDataChanged = wasChanged;
		}

		protected override void OnCardSaved()
		{
			base.OnCardSaved();
			RefreshHistoryList();
		}

		public OwnFundsViewModel(long id, ViewModelState action)
			: base(typeof(OwnFundsListViewModel))
		{
            DataObjectTypeForJournal = typeof(OwnedFounds);
            State = action;
			YearsList = DataContainerFacade.GetList<Year>();
			QuartersList = new[] { 1, 2, 3, 4 };

		    _ukFullList = BLServiceSystem.Client.GetUKListHib();
		    _ukActiveList = BLServiceSystem.Client.GetActiveUKListHib();

            if (State == ViewModelState.Create)
			{
			    _ownedFounds = new OwnedFounds {Date = DateTime.Now.Date};
			    SelectedYear = YearsList.FirstOrDefault();
				SelectedQuartal = QuartersList.FirstOrDefault();
                RefreshUKList();
                //SelectedUK = UKList.FirstOrDefault();
			}
			else
			{
				ID = id;
				_ownedFounds = DataContainerFacade.GetByID<OwnedFounds>(ID);
                _mSelectedYear = YearsList.FirstOrDefault(year => year.ID == _ownedFounds.YearID);
                RefreshUKList(_ownedFounds.LegalEntityID);
				RefreshHistoryList();
			}
		}

		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create;
		}

		protected override void ExecuteDelete(int delType)
		{
			OwnedFounds of = DataContainerFacade.GetByID<OwnedFounds>(ID); //получаем "свежие" данные из базы
			BLServiceSystem.Client.DeleteOwnedFounds(of.YearID.Value, of.Quartal.Value, of.LegalEntityID.Value);
			base.ExecuteDelete(DocOperation.Delete);
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Create:
					ID = DataContainerFacade.Save(_ownedFounds);
					_ownedFounds.ID = ID;
					break;
				case ViewModelState.Edit:
					if (DialogHelper.ConfirmHistoryChanged())
					{
						_ownedFounds = new OwnedFounds
						{
							Capital = _ownedFounds.Capital,
							CountPay = _ownedFounds.CountPay,
							CountDogRez = _ownedFounds.CountDogRez,
							CountDogAccum = _ownedFounds.CountDogAccum,
							CostPay = _ownedFounds.CostPay,
							CostRez = _ownedFounds.CostRez,
							CostInvestNPF = _ownedFounds.CostInvestNPF,
							CostInvestPFR = _ownedFounds.CostInvestPFR,
							Quartal = _ownedFounds.Quartal,
							YearID = _ownedFounds.YearID,
							LegalEntityID = _ownedFounds.LegalEntityID,
							Date = DateTime.Now.Date//OwnedFounds.Date
						};
					}
					ID = DataContainerFacade.Save(_ownedFounds);
					_ownedFounds.ID = ID;
					break;
			}
		}


		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "SelectedYear": return SelectedYear.ValidateRequired();
					case "SelectedQuartal": return SelectedQuartal.ValidateRequired();
					case "SelectedUK": return SelectedUK.ValidateRequired();
					case "Capital": return Capital.ValidateRequired() ?? Capital.ValidateNonNegative();
					case "CountPay": return CountPay.ValidateNonNegative();
					case "CountDogRez": return CountDogRez.ValidateNonNegative();
					case "CountDogAccum": return CountDogAccum.ValidateNonNegative();
					case "CostPay": return CostPay.ValidateNonNegative();
					case "CostRez": return CostRez.ValidateNonNegative();
					case "CostInvestNPF": return CostInvestNPF.ValidateNonNegative();
					case "CostInvestPFR": return CostInvestPFR.ValidateNonNegative();
				}
				return null;
			}
		}

		private bool Validate()
		{
			return ValidateFields();
		}
	}
}