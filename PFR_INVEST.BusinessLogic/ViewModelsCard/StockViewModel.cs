﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    public class StockViewModel : ViewModelCard, IUpdateRaisingModel, ICloseViewModelAfterSave
    {

        #region Fields

        private readonly Stock _stock;
        private readonly string _originalName;
        private readonly string _originalFullName;
        public string Name
        {
            get 
            { 
                return _stock?.Name;
            }
            set 
            { 
                if (_stock != null)
                    _stock.Name = value;
                OnPropertyChanged("Name"); 
            }
        }

        public string FullName
        {
            get { return _stock?.FullName; }
            set 
            { 
                if (_stock != null)
                    _stock.FullName = value;
                OnPropertyChanged("FullName"); 
            }
        }

        private DateTime? _prevUpdateDate;

        public DateTime? UpdateDate
        {
            get 
            {
                return _stock?.UpdateDate;
            }
            set 
            { 
                if (_stock != null)
                    _stock.UpdateDate = value;
                OnPropertyChanged("UpdateDate"); 
            }
        }

        public override Type DataObjectTypeForJournal => typeof(Branch);

        public ObservableCollection<StockHistory> NamesList { get; set; }

        #endregion

        #region Execute Implementations

        public override bool CanExecuteDelete()
        {
            return false;
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Create:
                case ViewModelState.Edit:
                    if ((!string.IsNullOrEmpty(_originalName) && _originalName != _stock.Name) || (!string.IsNullOrEmpty(_originalFullName) && _originalFullName != _stock.FullName) )
                    {
                        //update history
                        var sh = new StockHistory
                        {
                            StockID = _stock.ID,
                            Name = _originalName,
                            FullName = _originalFullName,
                            UpdateDate = _prevUpdateDate
                        };
                        DataContainerFacade.Save(sh);
                    }
                    //_stock.UpdateDate = DateTime.Today;
                    ID = DataContainerFacade.Save(_stock);
                    IsDataChanged = false;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && (_stock.Name != _originalName || _stock.FullName != _originalFullName || _stock.UpdateDate != _prevUpdateDate) && _stock.UpdateDate.HasValue;
        }
        #endregion


        public StockViewModel(): this(0) { }
        public StockViewModel(long id = 0)
        {
            _stock = id == 0 ? new Stock() : DataContainerFacade.GetByID<Stock>(id);
            _prevUpdateDate = UpdateDate;
            ID = _stock.ID;
            _originalName = _stock.Name;
            _originalFullName = _stock.FullName;
            if (ID > 0)
            {
                NamesList = new ObservableCollection<StockHistory>(DataContainerFacade.GetListByProperty<StockHistory>("StockID", _stock.ID));
            }
        }

        private const string NOT_NULL_FIELDS = "name|fullname";
        private const string S_ERROR = "Неверный формат данных";

        public override string this[string columnName]
        {
            get
            {
                if (_stock == null)
                    return S_ERROR;

                switch (columnName.ToLower())
                {
                    case "name":
                        return !string.IsNullOrWhiteSpace(_stock.Name) ? _stock.Name.ValidateMaxLength(100) : S_ERROR;
                    case "fullname":
                        return !string.IsNullOrWhiteSpace(_stock.FullName) ? _stock.FullName.ValidateMaxLength(512) : S_ERROR;
                    case "updatedate":
                        return !UpdateDate.HasValue ? S_ERROR : null;
                    default:
                        return null;
                }
            }
        }

        private string Validate()
        {
            if (_stock == null)
                return S_ERROR;

            return NOT_NULL_FIELDS.Split('|').Any(fld => !string.IsNullOrEmpty(this[fld])) ? S_ERROR : null;
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Stock), ID) };
        }
    }
}