﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class BOReportForm1ViewModel: ViewModelCard, IRequestCustomAction
    {
        public BOReportForm1 Report { get; set; }
        public BOReportForm1Sum SelectedSum { get; set; }

        //public string Text19 { get { return  Report == null ? "" : string.Format(@"по инвестиционному портфелю {0}а {1} года", Report.QuartalText, Report.Year); } }

        public List<Portfolio> PortfolioList { get; set; }

        private Portfolio _selectedPortfolio;

        public Portfolio SelectedPortfolio
        {
            get { return _selectedPortfolio; }
            set
            {
                _selectedPortfolio = value;
                if (Report != null)
                    Report.PortfolioID = value?.ID;
                OnPropertyChanged("SelectedPortfolio");
            }
        }

        public ICommand AddSum { get; set; }
        public ICommand DelSum { get; set; }
        public event EventHandler RequestCustomAction;

        private readonly List<BOReportForm1Sum> _toRemove = new List<BOReportForm1Sum>();

        private void OnRequestCustomAction()
        {
            RequestCustomAction?.Invoke(null, null);
        }

        public BOReportForm1ViewModel()
        {
            PortfolioList = DataContainerFacade.GetList<Portfolio>().OrderBy(a => a.Name).ToList();
            SelectedPortfolio = PortfolioList.FirstOrDefault();

            AddSum = new DelegateCommand(a => Report != null && EditAccessAllowed, a =>
            {
                var result = DialogHelper.BOReportForm1AddSum(Report.ID);
                if (result == null) return;
                Report.SumList.Add(result);
                result.PropertyChanged += (sender, args) => IsDataChanged = true;
                result.OnValidate += Data_Validate;
                IsDataChanged = true;
                OnRequestCustomAction();
            });

            DelSum = new DelegateCommand(a => Report != null && SelectedSum != null && EditAccessAllowed, a =>
            {
                if (!DialogHelper.ShowConfirmation("Удалить выбранную запись?")) return;

                if (SelectedSum.ID != 0)
                {
                    _toRemove.Add(SelectedSum);
                    IsDataChanged = true;
                }
                Report.SumList.Remove(SelectedSum);
                OnRequestCustomAction();
            });
        }

        private void BindReportActivities()
        {
            if (Report != null)
            {
                Report.PropertyChanged += (sender, args) => IsDataChanged = true;
                Report.Data.PropertyChanged += (sender, args) => IsDataChanged = true;
                Report.Data.OnValidate += Data_Validate;

                Report.SumList.ForEach(a =>
                {
                    a.PropertyChanged += (sender, args) => IsDataChanged = true;
                    a.OnValidate += Data_Validate;
                });
            }
        }

        private long _oldReportId;

        public BOReportForm1ViewModel(long reportId, bool update): this()
        {
            Report = BLServiceSystem.Client.GetCBReport(reportId);
            if (update)
            {
                UpdateReport(true);
                //ExecuteSave();
            }
            BindReportActivities();
            TmpSolution();
            SelectedPortfolio = Report.PortfolioID.HasValue ? PortfolioList.FirstOrDefault(a => a.ID == Report.PortfolioID) : PortfolioList.FirstOrDefault();
        }

        public BOReportForm1ViewModel(int quartal, int year):this()
        {
            var dt = DateTime.Today;
            Report = new BOReportForm1
            {
                Year = year,
                Quartal = quartal,
                CreateDate = dt,
                Date = new DateTime(quartal == 4 ? (year+1) : year, DateTools.GetLastMonthOfQuarterForBO(quartal), 1),
                UpdateDate = dt,
                Status = 1,
                OKUD = BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.One)
            };
            UpdateReport();
            BindReportActivities();
            TmpSolution();
            //OnPropertyChanged("Text19"); 
        }

        private void TmpSolution()
        {
            Report.Data.Quartal3 = Report.Data.Quartal3 ?? 0;
            Report.Data.Quartal4 = Report.Data.Quartal4 ?? 0;
            Report.Data.Total4 = Report.Data.Total4 ?? 0;
            Report.Data.Total3 = Report.Data.Total3 ?? 0;
        }

        private void UpdateReport(bool createNew = false)
        {
            if (createNew)
            {
                //делаем неактивным и сохраняем старый отчет
                //Report.Status = 0;
               // Report.UpdateDate = DateTime.Today;
                //DataContainerFacade.Save(Report);
                _oldReportId = Report.ID;
                //делаем новый отчет на базе старого
                Report.ID = 0;
                Report.Data = new BOReportForm1Data();
                Report.SumList = new List<BOReportForm1Sum>();
                Report.Status = 1;
                //Report.CreateDate = DateTime.Today;
                Report.UpdateDate = DateTime.Today;
                IsDataChanged = true;
            }

            //заполняем отчет данными
            Report.Data.Total2 = Math.Round(BLServiceSystem.Client.GetKDocsSumForBOReport(Report.Year, Report.Quartal) / 1000000M);
            Report.Data.Quartal2 = Math.Round(BLServiceSystem.Client.GetKDocsSumForBOReportQuarter(Report.Year, Report.Quartal) / 1000000M);
            var x = BLServiceSystem.Client.GetDSVForBOReport(Report.Year, Report.Quartal, false, false);
            Report.Data.Total3 = Math.Round(BLServiceSystem.Client.GetDSVForBOReport(Report.Year, Report.Quartal, false, true) / 1000000M);
            Report.Data.Quartal3 = Math.Round(BLServiceSystem.Client.GetDSVForBOReport(Report.Year, Report.Quartal, true, true) / 1000000M);
            Report.Data.Total4 = Math.Round(BLServiceSystem.Client.GetDSVForBOReport(Report.Year, Report.Quartal, false, false) / 1000000M);
            Report.Data.Quartal4 = Math.Round(BLServiceSystem.Client.GetDSVForBOReport(Report.Year, Report.Quartal, true, false) / 1000000M);

            Report.Data.Total5 = Math.Round(BLServiceSystem.Client.GetTempAllocSumForBOReport(Report.Year, Report.Quartal, false) / 1000000M);
            Report.Data.Quartal5 = Math.Round(BLServiceSystem.Client.GetTempAllocSumForBOReport(Report.Year, Report.Quartal, true) / 1000000M);

            Report.Data.Total8 = Math.Round(BLServiceSystem.Client.GetOrdReportForBOReport(Report.Year, Report.Quartal, false) / 1000000M);
            Report.Data.Quartal8 = Math.Round(BLServiceSystem.Client.GetOrdReportForBOReport(Report.Year, Report.Quartal, true) / 1000000M);

            Report.Data.Total10 = Math.Round(BLServiceSystem.Client.GetDepPercForBOReport(Report.Year, Report.Quartal, false) / 1000000M);
            Report.Data.Quartal10 = Math.Round(BLServiceSystem.Client.GetDepPercForBOReport(Report.Year, Report.Quartal, true) / 1000000M);

            Report.Data.Total13 = 0;
            Report.Data.Quartal13 = 0;

            Report.Data.Total14 = Math.Round(BLServiceSystem.Client.GetCostsForBOReport(Report.Year, Report.Quartal, false, CostIdentifier.s_TradingCommission) / 1000000M);
            Report.Data.Quartal14 = Math.Round(BLServiceSystem.Client.GetCostsForBOReport(Report.Year, Report.Quartal, true, CostIdentifier.s_TradingCommission) / 1000000M);
            Report.Data.Total15 = Math.Round(BLServiceSystem.Client.GetCostsForBOReport(Report.Year, Report.Quartal, false, CostIdentifier.s_DepositServicesCommission) / 1000000M);
            Report.Data.Quartal15 = Math.Round(BLServiceSystem.Client.GetCostsForBOReport(Report.Year, Report.Quartal, true, CostIdentifier.s_DepositServicesCommission) / 1000000M);


        }

        public override string this[string columnName] => string.Empty;

        protected override bool BeforeExecuteSaveCheck()
        {
            if (Report.ID != 0)
            {
                var report = DataContainerFacade.GetByID<BOReportForm1>(Report.ID);
                if (report.Status == 0)
                {
                    DialogHelper.ShowError("Сохраняемый отчет помечен в Бд как устаревший! Сохранение невозможно.");
                    return false;
                }
            }

            return Report.ID!=0 || _oldReportId != 0 || DialogHelper.BOReportForm1CheckForAvailability(Report.Quartal, Report.Year);
        }

        protected override void ExecuteSave()
        {
            //if (State != ViewModelState.Create && State != ViewModelState.Edit) return;
            bool isNew = Report.ID == 0;

            if (_oldReportId != 0)
            {
                var rep = DataContainerFacade.GetByID<BOReportForm1>(_oldReportId);
                rep.Status = 0;
                rep.UpdateDate = DateTime.Today;
                DataContainerFacade.Save(rep);
                _oldReportId = 0;
            }

            var idList = BLServiceSystem.Client.SaveReportForm1(Report);
            if (_toRemove.Count > 0)
            {
                DataContainerFacade.BatchDelete(_toRemove);
                _toRemove.Clear();
            }
            //Report = BLServiceSystem.Client.GetCBReport(rId);
            Report.ID = Report.Data.ReportID = idList[0];
            idList.RemoveAt(0);
            Report.Data.ID = idList[0];
            idList.RemoveAt(0);
            Report.SumList.ForEach(a =>
            {
                a.ReportID = Report.ID;
                a.ID = idList[0];
                idList.RemoveAt(0);
            });
           // SelectedSum = Report.SumList.FirstOrDefault();

            BindReportActivities();
            DialogHelper.RefreshCBReportManagerList(isNew ? 0 : Report.ID);
            IsDataChanged = false;
        }

        public override bool CanExecuteSave()
        {
            return IsDataChanged && string.IsNullOrEmpty(Validate());
        }

        private string Validate()
        {
            if (Report == null) return "Отчет не создан";
            const string dataFields = "Total9|Quartal9|Total11|Quartal11|Total16|Quartal16|Total18|Quartal19|Total|Quartal8";
            const string dataFieldsSum = "Sum";

            foreach (var e in dataFields.Split('|').Select(key => new BaseDataObject.ValidateEventArgs(key)))
            {
                Data_Validate(Report.Data, e);
                if (!string.IsNullOrEmpty(e.Error))
                    return e.Error;           
            }

            foreach (var e in dataFieldsSum.Split('|').Select(key => new BaseDataObject.ValidateEventArgs(key)))
            {
                foreach (var a in Report.SumList)
                {
                    Data_Validate(a, e);
                    if (!string.IsNullOrEmpty(e.Error))
                        return e.Error;
                }
            }
            return null;
        }

        private static void Data_Validate(object sender, BaseDataObject.ValidateEventArgs e)
        {
            var data = (sender as BOReportForm1Data);
            var sum = (sender as BOReportForm1Sum);
            //if (data == null) return;
            const string error = "Поле обязательное для заполнения";
            switch (e.PropertyName)
            {
                case "Total9": e.Error = !data.Total9.HasValue ? error : data.Total9.Value.ValidateMaxFormat();
                    break;
                case "Quartal9": e.Error = !data.Quartal9.HasValue ? error : data.Quartal9.Value.ValidateMaxFormat();
                    break;
                case "Total11": e.Error = !data.Total11.HasValue ? error : data.Total11.Value.ValidateMaxFormat();
                    break;
                case "Quartal11": e.Error = !data.Quartal11.HasValue ? error : data.Quartal11.Value.ValidateMaxFormat();
                    break;
                case "Total16": e.Error = !data.Total16.HasValue ? error : data.Total16.Value.ValidateMaxFormat();
                    break;
                case "Quartal16": e.Error = !data.Quartal16.HasValue ? error : data.Quartal16.Value.ValidateMaxFormat();
                    break;
                case "Total18": e.Error = !data.Total18.HasValue ? error : data.Total18.Value.ValidateMaxFormat();
                    break;
                case "Quartal19": e.Error = !data.Quartal19.HasValue ? error : data.Quartal19.Value.ValidateMaxFormat();
                    break;

                case "Quartal1": e.Error = data.Quartal1.ValidateMaxFormat();
                    break;
                case "Total1": e.Error = data.Total1.ValidateMaxFormat();
                    break;
                case "Quartal2": e.Error = !data.Quartal2.HasValue ? null : data.Quartal2.Value.ValidateMaxFormat();
                    break;
                case "Total2": e.Error = !data.Total2.HasValue ? null : data.Total2.Value.ValidateMaxFormat();
                    break;
                case "Quartal3": e.Error = !data.Quartal3.HasValue ? null : data.Quartal3.Value.ValidateMaxFormat();
                    break;
                case "Total3": e.Error = !data.Total3.HasValue ? null : data.Total3.Value.ValidateMaxFormat();
                    break;
                case "Quartal4": e.Error = !data.Quartal4.HasValue ? null : data.Quartal4.Value.ValidateMaxFormat();
                    break;
                case "Total4": e.Error = !data.Total4.HasValue ? null : data.Total4.Value.ValidateMaxFormat();
                    break;
                case "Quartal5": e.Error = !data.Quartal5.HasValue ? null : data.Quartal5.Value.ValidateMaxFormat();
                    break;
                case "Total5": e.Error = !data.Total5.HasValue ? null : data.Total5.Value.ValidateMaxFormat();
                    break;
                case "Quartal6": e.Error = data.Quartal6.ValidateMaxFormat();
                    break;
                case "Total6": e.Error = data.Total6.ValidateMaxFormat();
                    break;
                case "Quartal7": e.Error = data.Quartal7.ValidateMaxFormat();
                    break;
                case "Total7": e.Error = data.Total7.ValidateMaxFormat();
                    break;
                case "Quartal8": e.Error = !data.Quartal8.HasValue ? null : data.Quartal8.Value.ValidateMaxFormat();
                    break;
                case "Total8": e.Error = !data.Total8.HasValue ? null : data.Total8.Value.ValidateMaxFormat();
                    break;
                case "Quartal10": e.Error = !data.Quartal10.HasValue ? null : data.Quartal10.Value.ValidateMaxFormat();
                    break;
                case "Total10": e.Error = !data.Total10.HasValue ? null : data.Total10.Value.ValidateMaxFormat();
                    break;
                case "Quartal12": e.Error = data.Quartal12.ValidateMaxFormat();
                    break;
                case "Total12": e.Error = data.Total12.ValidateMaxFormat();
                    break;
                case "Quartal13": e.Error = !data.Quartal13.HasValue ? null : data.Quartal13.Value.ValidateMaxFormat();
                    break;
                case "Total13": e.Error = !data.Total13.HasValue ? null : data.Total13.Value.ValidateMaxFormat();
                    break;
                case "Quartal14": e.Error = !data.Quartal14.HasValue ? null : data.Quartal14.Value.ValidateMaxFormat();
                    break;
                case "Total14": e.Error = !data.Total14.HasValue ? null : data.Total14.Value.ValidateMaxFormat();
                    break;
                case "Quartal15": e.Error = !data.Quartal15.HasValue ? null : data.Quartal15.Value.ValidateMaxFormat();
                    break;
                case "Total15": e.Error = !data.Total15.HasValue ? null : data.Total15.Value.ValidateMaxFormat();
                    break;
                case "Quartal17": e.Error = data.Quartal17.ValidateMaxFormat();
                    break;
                case "Total17": e.Error = data.Total17.ValidateMaxFormat();
                    break;

                case "Sum": e.Error = sum.Sum.ValidateNonZero() ?? sum.Sum.ValidateMaxFormat();
                    break;

            }
        }

    }
}
