﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class DueUndistributedViewModel : ApsModelBase
	{
        public DueUndistributedViewModel(PortfolioIdentifier.PortfolioTypes p_Type)
			: base(p_Type)
		{
			APS.KindID = 4; // потому что страховые взносы умерших
		}
	}
}
