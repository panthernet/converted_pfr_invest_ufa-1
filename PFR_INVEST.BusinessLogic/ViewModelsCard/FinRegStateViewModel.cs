﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
	public sealed class FinRegStateViewModel : ViewModelCardDialog, IRequestCloseViewModel, IUpdateListenerModel, IUpdateRaisingModel
	{
		public event EventHandler RequestClose;


	    private Finregister Finregister;

	    private Register Register;

        /// <summary>
        /// Является ли текущий финреестр дочерним от финреестра в статусе Не передан
        /// </summary>
        public bool IsChildFinregister => Finregister != null && Finregister.ParentFinregisterID != null;

	    #region FinregisterID
		public long? FinregisterID => Finregister != null ? (long?)Finregister.ID : null;

	    #endregion

		#region RegisterID
		public long? RegisterID => Finregister != null ? Finregister.RegisterID : null;

	    #endregion

		#region RegisterKind
		public string RegisterKind => Register != null ? Register.Kind : null;

	    #endregion

		#region NPF

		private List<LegalEntity> _npfList = new List<LegalEntity>();

		//private List<LegalEntity> npfList
		//{
		//    get 
		//    { 
		//        return _npfList; 
		//    }
		//    set
		//    {
		//        _npfList = value;
		//    }
		//}

		public List<LegalEntity> NPFList
		{
			get { return _npfList; }
			set
			{
				_npfList = value;
				OnPropertyChanged("NPFList");
			}
		}

		private LegalEntity _selectedNPF;
		public LegalEntity SelectedNPF
		{
			get { return _selectedNPF; }
			set
			{
				_selectedNPF = value;
				OnPropertyChanged("SelectedNPF");
			}
		}
		#endregion

		#region Content
		public string Content
		{
			get
			{
				return Register != null ? Register.Content : "";
			}

			set { }
		}
		#endregion

		#region Count
		public string CountLabelContent => RegisterIdentifier.IsFromNPF(Register.Kind) ? "Сумма СПН:" : "Сумма СПН*:";

	    public decimal? Count
		{
			get {
			    return Finregister != null ? Finregister.Count : null;
			}

		    set
			{
				Finregister.Count = value ?? 0;
				OnPropertyChanged("Count");
			}
		}


		/// <summary>
		/// Если финрееcтр доступен в БО и п/п не залочены
		/// </summary>
		public bool IsAvailableInBoNotComplete
		{
			get
			{
				//передача
				if (!string.IsNullOrEmpty(RegisterKind) && RegisterIdentifier.IsToNPF(Register.Kind))
					return State == ViewModelState.Edit && Register.TrancheDate != null && (!DraftSum.HasValue || DraftSum != Count);
				return State == ViewModelState.Edit && Status != null && RegisterIdentifier.FinregisterStatuses.IsIssued(Status) && (!DraftSum.HasValue || DraftSum != Count);
			}
		}

		/// <summary>
		/// Если финреестр вернулся из БО c заполненными п/п
		/// </summary>
		public bool IsReturnedFromBackOffice => draftsList.Count > 0 && !IsAvailableInBoNotComplete;

	    #endregion

		#region CFRCount
		public bool IsCFRCountVisible => false;

	    public decimal? CFRCount
		{
			get
			{
				return Finregister != null ? Finregister.CFRCount : null;
			}

			set
			{
				if (Finregister == null) return;
                Finregister.CFRCount = Convert.ToDecimal(value);
				//corr.CurrentSum = Convertion.ToDecimal(value);
				OnPropertyChanged("CFRCount");
			}
		}
		#endregion

		#region Status
		public string Status
		{
			get
			{
				return Finregister != null ? Finregister.Status : string.Empty;
			}

			set
			{
				if (Finregister == null)
					return;

				if (value == null)
					throw new Exception("Статус финреестра невозможно сбросить в NULL");

				if (Finregister.Status == null)
				{
					var msg = "Статус финреестра: " + value;
					JournalLogger.LogChangeStateEvent(this, msg);
				}
				else
				{
					if (Finregister.Status.Equals(value))
						return;

					var msg = "Статус финреестра: " + Finregister.Status + " -> " + value;
					JournalLogger.LogChangeStateEvent(this, msg);
				}

				Finregister.Status = value;
				OnPropertyChanged("IsCountReadOnly");
				OnPropertyChanged("Status");
			}
		}
		#endregion

		#region FinStatuses

		private List<string> finStatuses;
		public List<string> FinStatuses
		{
			get
			{
				return finStatuses;
			}

			set
			{
				finStatuses = value;
				OnPropertyChanged("FinStatuses");
			}
		}

		#endregion

		#region TrancheDate
		public DateTime? TrancheDate
		{
			get { return Register != null && RegisterIdentifier.IsToNPF(Register.Kind.ToLower()) ? Register.TrancheDate : null; }
			set
			{
				if (Register == null || !RegisterIdentifier.IsToNPF(Register.Kind.ToLower())) return;
				Register.TrancheDate = value;
				OnPropertyChanged("TrancheDate");
			}
		}

		public Visibility TrancheDateVisibility => Register != null && RegisterIdentifier.IsToNPF(Register.Kind.ToLower()) ? Visibility.Visible : Visibility.Collapsed;

	    #endregion

		#region RegNum

		private string _oldRegNum;
		public string RegNum
		{
			get { return Finregister.RegNum; }
			set
			{
				Finregister.RegNum = value;
				OnPropertyChanged("RegNum");
				OnPropertyChanged("Date");
			}
		}
		#endregion

		#region Date

	    public DateTime? Date
		{
			get { return Finregister.Date; }
			set
			{
				Finregister.Date = value;
				OnPropertyChanged("RegNum");
				OnPropertyChanged("Date");
			}
		}
		#endregion

		#region ZLCount
		public long? ZLCount
		{
			get
			{
				return Finregister != null ? Finregister.ZLCount : null;
			}

			set
			{
				if (Finregister == null) return;
                Finregister.ZLCount = Convert.ToInt64(value);
				OnPropertyChanged("ZLCount");
			}
		}

		#endregion

		#region FinMoveType
		public string FinMoveType
		{
			get
			{
				return Finregister != null ? Finregister.FinMoveType : "";
			}

			set
			{
				if (Finregister == null) return;
				Finregister.FinMoveType = value;
				OnPropertyChanged("FinMoveType");
			}
		}
		#endregion

		#region FinMoveTypes

		private List<string> finMoveTypes;
		public List<string> FinMoveTypes
		{
			get
			{
				return finMoveTypes;
			}
			set
			{
				finMoveTypes = value;
				OnPropertyChanged("FinMoveTypes");
			}
		}

		#endregion

		#region FinDate
		public DateTime? FinDate
		{
			get
			{
				return Finregister != null ? Finregister.FinDate : null;
			}

			set
			{
				if (Finregister == null) return;
				Finregister.FinDate = value;
				OnPropertyChanged("FinDate");
			}
		}
		#endregion

		#region FinNum
		public string FinNum
		{
			get { return Finregister != null && Finregister.FinNum != null ? Finregister.FinNum : ""; }

			set
			{
				if (Finregister == null) return;
				Finregister.FinNum = value;
				OnPropertyChanged("FinNum");
			}
		}
		#endregion
	

		#region DraftsList
		private List<AsgFinTr> draftsList = new List<AsgFinTr>();
		public List<AsgFinTr> DraftsList
		{
			get
			{
				return draftsList;
			}

			set
			{
				draftsList = value;
				OnPropertyChanged("DraftsList");
			}
		}

		public Visibility DraftsListVisibility => draftsList != null && draftsList.Count > 0 ? Visibility.Visible : Visibility.Collapsed;

	    #endregion

		#region DraftSum
		public decimal? DraftSum
		{
			get
			{
				if (draftsList != null && draftsList.Count > 0)
				{
					decimal? sum = null;
					foreach (AsgFinTr dr in draftsList)
					{
						if (sum == null && dr.DraftAmount != null)
							sum = 0;
						sum += dr.DraftAmount;
					}
					return sum;
				}
				return null;
			}
		}
		#endregion

		#region Comment

		public string Comment
		{
			get { return Finregister != null && Finregister.Comment != null ? Finregister.Comment : string.Empty; }

			set
			{
				if (Finregister == null) return;
				Finregister.Comment = value;
				OnPropertyChanged("Comment");
			}
		}
		#endregion


		
		public FinRegStateViewModel(long recordID = 0)
		{
			DataObjectTypeForJournal = typeof(object);
            State = ViewModelState.Edit;
            RefreshConnectedCardsViewModels = RefreshConnectedCards;
            FinMoveTypes = RegisterIdentifier.FinregisterMoveTypes.ToList;

            if (recordID == 0)
		    {
		        Finregister = new Finregister();
		    }
		    else
		    {
		        ID = recordID;
		        ReloadFRFromDB();
		    }
		    IsDataChanged = false;
		}


		public void ReloadFRFromDB()
		{
			Finregister = DataContainerFacade.GetByID<Finregister, long>(ID);
		    _oldRegNum = RegNum;

			Register = Finregister.GetRegister();

            if(string.IsNullOrEmpty(RegNum))
                RegNum = Register.RegNum;
            if(Date == null)
                Date = Register.RegDate;

			RefreshDraftsList();

			Status = FinStatuses.FirstOrDefault(x => x.Equals(Status, StringComparison.InvariantCultureIgnoreCase)) ?? FinStatuses.First(RegisterIdentifier.FinregisterStatuses.IsCreated); ;
			FinMoveType = FinMoveTypes.FirstOrDefault(x => x.Equals(FinMoveType, StringComparison.InvariantCultureIgnoreCase));
		}

		#region Refresh

		public void RefreshConnectedCards()
		{
			if (GetViewModel != null)
			{
				var rVM = GetViewModel(typeof(RegisterViewModel)) as RegisterViewModel;
				if (rVM != null)
					rVM.LoadRegister();
				var frVM = GetViewModel(typeof(FinRegisterViewModel)) as FinRegisterViewModel;
				if (frVM != null)
					frVM.RefreshStatus();
			}
		}


		public void RefreshGiveFields()
		{
			bool isdatachanged = IsDataChanged;
			OnPropertyChanged("FinDate");
			OnPropertyChanged("FinNum");
			OnPropertyChanged("FinMoveType");
			IsDataChanged = isdatachanged;
		}

		public void RefreshDraftsList()
		{
			bool isdatachanged = IsDataChanged;
			if (Finregister.ExtensionData != null)
				Finregister.ExtensionData.Remove("DraftsList");
			DraftsList = new List<AsgFinTr>(Finregister.GetDraftsList().Cast<AsgFinTr>());
			OnPropertyChanged("DraftsListVisibility");
			IsDataChanged = isdatachanged;
		}
		#endregion

		#region Validation
		public override string this[string columnName]
		{
			get
			{
				const string errorMessage = "Неверный формат данных";
				switch (columnName.ToLower())
				{				
					case "finnum": return FinNum != null && FinNum.Length > 49 ? errorMessage : null;
					case "findate": return FinDate == null ? errorMessage : null;
					case "finmovetype": return string.IsNullOrEmpty(FinMoveType) ? errorMessage : null;
					default: return null;
				}
			}
		}

	    private string Validate()
		{
			var fieldNames =
				"selectednpf|count|zlcount|regnum|date|finnum|cfrcount|comment";

			return fieldNames.Split("|".ToCharArray()).Any(fieldName => !String.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
		}
		#endregion


		public void CloseWindow()
		{
			if (RequestClose != null)
			{
				// необходимо заблокировать запрос о сохранении при закрытии окна
				Deleting = true;
				RequestClose(this, EventArgs.Empty);
			}
		}

		#region IUpdateListenerModel implementation
		public Type[] UpdateListenTypes => new[] { typeof(RegistersListViewModel), typeof(RegistersArchiveListViewModel) };

	    public void OnDataUpdate(Type type, long id)
		{
			if (type != typeof(Finregister) || id <= 0) return;
			ID = id;
			ReloadFRFromDB();
		}
		#endregion

		#region IUpdateRaisingModel implementation
		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Finregister), ID) };

            if(Finregister.ParentFinregisterID != null)
            {
                list.Add(new KeyValuePair<Type, long>(typeof(Finregister), (long)Finregister.ParentFinregisterID));
            }
			return list;
		}

		private KeyValuePair<Type, long> GetDeletedFinregisterForUpdate(long id)
		{
			return new KeyValuePair<Type, long>(typeof(Finregister), id);
		}

		#endregion
	}
}
