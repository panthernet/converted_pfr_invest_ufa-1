﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ServiceItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OUFV_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class SIContractViewModel : ViewModelCard
    {
        private Contract _contract;
        private readonly BankAccount _bankAccount;
        private readonly LegalEntity _legalEntity;

        private ContrInvest _contrInvest;
        private RewardCost _rewardCost;

        public delegate void NeedRefreshSupAgreementsListDelegate();
        public event NeedRefreshSupAgreementsListDelegate OnNeedRefreshSupAgreementsList;
        protected void NeedRefreshSupAgreementsList()
        {
            if (OnNeedRefreshSupAgreementsList != null)
                OnNeedRefreshSupAgreementsList();
        }
        public delegate void CloseDeleteSupAgreementWindow(long id);
        public event CloseDeleteSupAgreementWindow OnCloseDeleteSupAgreementWindow;
        protected void CloseSupAgreementWindow(long id)
        {
            if (OnCloseDeleteSupAgreementWindow != null)
                OnCloseDeleteSupAgreementWindow(id);
        }

        private readonly string[] _mContrInvestProperties;

        public bool IsContractSaved => ID > 0 && !EditAccessDenied;

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && _contract.Status != -1;
        }

        protected override void ExecuteDelete(int delType)
        {
            DeleteEntity(_contract);
            UpdateContragent(_legalEntity.ID, _legalEntity.GetContragent());
        }

        #region Full & Formalized Name
        public string FullName => _legalEntity == null ? null : _legalEntity.FullName;

        public string FormalizedName => _legalEntity == null ? null : _legalEntity.FormalizedName;

        #endregion

        #region OperateSum, PortfolioName, DissolutionDate, DissolutionBase
        public decimal OperateSum
        {
            get
            {
                return _contract == null ? 0 : _contract.OperateSum ?? 0;
            }
            set
            {
                if (_contract == null) return;
                _contract.OperateSum = value;
                OnPropertyChanged("OperateSum");
            }
        }

        public string PortfolioName
        {
            get
            {
                return _contract == null ? null : _contract.PortfolioName;
            }
            set
            {
                _contract.PortfolioName = value;
                OnPropertyChanged("PortfolioName");
            }
        }

        public DateTime? DissolutionDate
        {
            get
            {
                return _contract == null ? null : _contract.DissolutionDate;
            }
            set
            {
                _contract.DissolutionDate = value;
                OnPropertyChanged("DissolutionDate");
            }
        }

        public string DissolutionBase
        {
            get
            {

                return _contract == null ? null : _contract.DissolutionBase;
            }
            set
            {
                _contract.DissolutionBase = value;
                OnPropertyChanged("DissolutionBase");
            }
        }
        #endregion

        #region Selected Contract Name
        private List<ContractName> _mContractNamesList;
        public List<ContractName> ContractNamesList
        {
            get
            {
                return _mContractNamesList;
            }
            set
            {
                _mContractNamesList = value;
                OnPropertyChanged("ContractNamesList");
            }
        }

        private ContractName _mSelectedContractName;
        public ContractName SelectedContractName
        {
            get
            {
                return _mSelectedContractName;
            }
            set
            {
                _mSelectedContractName = value;
                if (value != null)
                {
                    _contract.ContractNameID = value.ID;
                    _contract.TypeID = ContractHelper.GetContractTypeID(value.ID);
                }
                OnPropertyChanged("SelectedContractName");
            }
        }
        #endregion

        #region Contract Date & Contract Number
        public DateTime? ContractDate
        {
            get
            {
                return _contract == null ? null : _contract.ContractDate;
            }
            set
            {
                _contract.ContractDate = value;
                RecalcContractEndDate();
                OnPropertyChanged("ContractDate");
            }
        }

        public string ContractNumber
        {
            get
            {
                return _contract == null ? null : _contract.ContractNumber;
            }
            set
            {
                _contract.ContractNumber = value;
                OnPropertyChanged("ContractNumber");
            }
        }
        #endregion

        #region Contract End Dates & Duration
        public DateTime? ContractEndDate
        {
            get
            {
                return _contract == null ? null : _contract.ContractEndDate;
            }
            set
            {
                _contract.ContractEndDate = value;
                OnPropertyChanged("ContractEndDate");
                OnPropertyChanged("ContractSupAgreementEndDate");
            }
        }

        public DateTime? ContractSupAgreementEndDate
        {
            get
            {
                if (_contract == null)
                    return null;

                if (_contract.ContractSupAgreementEndDate.HasValue)
                    return _contract.ContractSupAgreementEndDate;

                UpdateContractSupAgreementEndDate();
                return _contract.ContractSupAgreementEndDate ?? ContractEndDate;
            }
            set
            {
                if (_contract.ContractSupAgreementEndDate== value) return;
                _contract.ContractSupAgreementEndDate = value;
                OnPropertyChanged("ContractSupAgreementEndDate");
            }
        }

        private void UpdateContractSupAgreementEndDate()
        {
            _contract.ContractSupAgreementEndDate = ContractHelper.GetContractSupAgrEndDate(_contract.GetSupAgreements());
        }

        private int? _mDuration;
        public int? Duration
        {
            get
            {
                return _mDuration;
            }
            set
            {
                _mDuration = value;
                RecalcContractEndDate();
                OnPropertyChanged("Duration");
            }
        }
        #endregion

        #region Selected Document Type
        private List<DocType> _mDocumentTypesList;
        public List<DocType> DocumentTypesList
        {
            get
            {
                return _mDocumentTypesList;
            }
            set
            {
                _mDocumentTypesList = value;
                OnPropertyChanged("DocumentTypesList");
            }
        }

        private DocType _mSelectedDocumentType;
        public DocType SelectedDocumentType
        {
            get
            {
                return _mSelectedDocumentType;
            }
            set
            {
                _mSelectedDocumentType = value;
                if (value != null)
                    _contract.DocumentTypeID = value.ID;
                OnPropertyChanged("SelectedDocumentType");
            }
        }
        #endregion

        #region Document Number & Date
        public string DocumentNumber
        {
            get
            {
                return _contract == null ? null : _contract.DocumentNumber;
            }
            set
            {
                _contract.DocumentNumber = value;
                OnPropertyChanged("DocumentNumber");
            }
        }

        public DateTime? DocumentDate
        {
            get
            {
                return _contract == null ? null : _contract.DocumentDate;
            }
            set
            {
                _contract.DocumentDate = value;
                OnPropertyChanged("DocumentDate");
            }
        }

        public string OtherInfo
        {
            get
            {
                return _contract == null ? null : _contract.OtherInfo;
            }
            set
            {
                _contract.OtherInfo = value;
                OnPropertyChanged("OtherInfo");
            }
        }
        #endregion

        #region BankAccount
        public string BankAccountLegalEntityName
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.LegalEntityName) ? null : _bankAccount.LegalEntityName;
            }
            set
            {
                _bankAccount.LegalEntityName = value;
                OnPropertyChanged("BankAccountLegalEntityName");
            }
        }

        public string BankAccountAccountNumber
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.AccountNumber) ? null : _bankAccount.AccountNumber;
            }
            set
            {
                _bankAccount.AccountNumber = value;
                OnPropertyChanged("BankAccountAccountNumber");
            }
        }

        public string BankAccountBankName
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.BankName) ? null : _bankAccount.BankName;
            }
            set
            {
                _bankAccount.BankName = value;
                OnPropertyChanged("BankAccountBankName");
            }
        }

		public string BankAccountCorrespondentAccountNote
		{
			get
			{
				return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.CorrespondentAccountNote) ? null : _bankAccount.CorrespondentAccountNote;
			}
			set
			{
				_bankAccount.CorrespondentAccountNote = value;
				OnPropertyChanged("BankAccountCorrespondentAccountNote");
			}
		}

        public string BankAccountBankLocation
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.BankLocation) ? null : _bankAccount.BankLocation;
            }
            set
            {
                _bankAccount.BankLocation = value;
                OnPropertyChanged("BankAccountBankLocation");
            }
        }

        public string BankAccountCorrespondentAccountNumber
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.CorrespondentAccountNumber) ? null : _bankAccount.CorrespondentAccountNumber;
            }
            set
            {
                _bankAccount.CorrespondentAccountNumber = value;
                OnPropertyChanged("BankAccountCorrespondentAccountNumber");
            }
        }

        public string BankAccountBIK
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.BIK) ? null : _bankAccount.BIK.Trim();
            }
            set
            {
                _bankAccount.BIK = value;
                OnPropertyChanged("BankAccountBIK");
            }
        }

        public string BankAccountINN
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.INN) ? null : _bankAccount.INN.Trim();
            }
            set
            {
                _bankAccount.INN = value;
                OnPropertyChanged("BankAccountINN");
            }
        }

        public string BankAccountKPP
        {
            get
            {
                return _bankAccount == null ? null : string.IsNullOrEmpty(_bankAccount.KPP) ? null : _bankAccount.KPP.Trim();
            }
            set
            {
                _bankAccount.KPP = value;
                OnPropertyChanged("BankAccountKPP");
            }
        }
        #endregion

        #region ContrInvest Lists
        private List<ContrInvestOBL> _mContrInvestOblList;
        public List<ContrInvestOBL> ContrInvestOBLList
        {
            get
            {
                return _mContrInvestOblList;
            }
            set
            {
                _mContrInvestOblList = value;
                OnPropertyChanged("ContrInvestOBLList");
            }
        }

        private List<ContrInvestAKC> _mContrInvestAkcList;
        public List<ContrInvestAKC> ContrInvestAKCList
        {
            get
            {
                return _mContrInvestAkcList;
            }
            set
            {
                _mContrInvestAkcList = value;
                OnPropertyChanged("ContrInvestAKCList");
            }
        }
        private List<ContrInvestPAY> _mContrInvestPayList;
        public List<ContrInvestPAY> ContrInvestPAYList
        {
            get
            {
                return _mContrInvestPayList;
            }
            set
            {
                _mContrInvestPayList = value;
                OnPropertyChanged("ContrInvestPAYList");
            }
        }
        #endregion

        #region Ценные бумаги
        public long ContrInvestGCBRFMIN
        {
            get
            {
                return _contrInvest.GCBRFMIN ?? 0;
            }
            set
            {
                _contrInvest.GCBRFMIN = value;
                OnPropertyChanged("ContrInvestGCBRFMIN");
            }
        }

        public long ContrInvestGCBRFMAX
        {
            get
            {
                return _contrInvest.GCBRFMAX ?? 0;
            }
            set
            {
                _contrInvest.GCBRFMAX = value;
                OnPropertyChanged("ContrInvestGCBRFMAX");
            }
        }

        public long ContrInvestGCBSMIN
        {
            get
            {
                return _contrInvest.GCBSMIN ?? 0;
            }
            set
            {
                _contrInvest.GCBSMIN = value;
                OnPropertyChanged("ContrInvestGCBSMIN");
            }
        }

        public long ContrInvestGCBSMAX
        {
            get
            {
                return _contrInvest.GCBSMAX ?? 0;
            }
            set
            {
                _contrInvest.GCBSMAX = value;
                OnPropertyChanged("ContrInvestGCBSMAX");
            }
        }
        #endregion

        #region Облигации
        public long ContrInvestOBLMIN
        {
            get
            {
                return _contrInvest.OBLMIN ?? 0;
            }
            set
            {
                _contrInvest.OBLMIN = value;
                OnPropertyChanged("ContrInvestOBLMIN");
            }
        }

        public long ContrInvestOBLMAX
        {
            get
            {
                return _contrInvest.OBLMAX ?? 0;
            }
            set
            {
                _contrInvest.OBLMAX = value;
                OnPropertyChanged("ContrInvestOBLMAX");
            }
        }
        #endregion

        #region Ипотечные ценные бумаги
        public long ContrInvestIPMIN
        {
            get
            {
                return _contrInvest.IPMIN ?? 0;
            }
            set
            {
                _contrInvest.IPMIN = value;
                OnPropertyChanged("ContrInvestIPMIN");
            }
        }

        public long ContrInvestIPMAX
        {
            get
            {
                return _contrInvest.IPMAX ?? 0;
            }
            set
            {
                _contrInvest.IPMAX = value;
                OnPropertyChanged("ContrInvestIPMAX");
            }
        }
        #endregion

        #region Средства
        public long ContrInvestSRMIN
        {
            get
            {
                return _contrInvest.SRMIN ?? 0;
            }
            set
            {
                _contrInvest.SRMIN = value;
                OnPropertyChanged("ContrInvestSRMIN");
            }
        }

        public long ContrInvestSRMAX
        {
            get
            {
                return _contrInvest.SRMAX ?? 0;
            }
            set
            {
                _contrInvest.SRMAX = value;
                OnPropertyChanged("ContrInvestSRMAX");
            }
        }

        public long ContrInvestDEPMIN
        {
            get
            {
                return _contrInvest.DEPMIN ?? 0;
            }
            set
            {
                _contrInvest.DEPMIN = value;
                OnPropertyChanged("ContrInvestDEPMIN");
            }
        }

        public long ContrInvestDEPMAX
        {
            get
            {
                return _contrInvest.DEPMAX ?? 0;
            }
            set
            {
                _contrInvest.DEPMAX = value;
                OnPropertyChanged("ContrInvestDEPMAX");
            }
        }

        public long ContrInvestSRVALMIN
        {
            get
            {
                return _contrInvest.SRVALMIN ?? 0;
            }
            set
            {
                _contrInvest.SRVALMIN = value;
                OnPropertyChanged("ContrInvestSRVALMIN");
            }
        }

        public long ContrInvestSRVALMAX
        {
            get
            {
                return _contrInvest.SRVALMAX ?? 0;
            }
            set
            {
                _contrInvest.SRVALMAX = value;
                OnPropertyChanged("ContrInvestSRVALMAX");
            }
        }
        #endregion

        #region Reward Cost
        public decimal RewardCostMaxCost
        {
            get
            {
                return _rewardCost.MaxCost ?? 0;
            }
            set
            {
                _rewardCost.MaxCost = value;
                OnPropertyChanged("RewardCostMaxCost");
            }
        }

        public decimal RewardCostMaxPaySD
        {
            get
            {
                return _rewardCost.MaxPaySD ?? 0;
            }
            set
            {
                _rewardCost.MaxPaySD = value;
                OnPropertyChanged("RewardCostMaxPaySD");
            }
        }

        public decimal RewardCostAmountReward
        {
            get
            {
                return _rewardCost.AmountReward ?? 0;
            }
            set
            {
                _rewardCost.AmountReward = value;
                OnPropertyChanged("RewardCostAmountReward");
            }
        }
        public DateTime? RewardCostChDate
        {
            get
            {
                return _rewardCost.ChDate;
            }
            set
            {
                _rewardCost.ChDate = value;
                OnPropertyChanged("RewardCostChDate");
            }
        }

        public string RewardCostEndCh
        {
            get
            {
                return _rewardCost.EndCh;
            }
            set
            {
                _rewardCost.EndCh = value;
                OnPropertyChanged("RewardCostEndCh");
            }
        }
        #endregion

        #region SupAgreements
        private List<SupAgreement> _mSupAgreementsList;
        public List<SupAgreement> SupAgreementsList
        {
            get
            {
                return _mSupAgreementsList;
            }
            set
            {
                _mSupAgreementsList = value;
                NeedRefreshSupAgreementsList();
            }
        }
        #endregion

        public Document.Types ContractType => (Document.Types)_contract.TypeID;

        private void RecalcContractEndDate()
        {
            if (ContractDate.HasValue && Duration.HasValue)
                ContractEndDate = ContractDate.Value.AddYears(Duration.Value).AddDays(1);
        }

        private bool _mContrInvestCreated;
        private void CreateContrInvest(bool fixLists = false)
        {
            if (!fixLists)
                _contrInvest = new ContrInvest();

            ContrInvestOBLList = new List<ContrInvestOBL>();
            ContrInvestAKCList = new List<ContrInvestAKC>();
            ContrInvestPAYList = new List<ContrInvestPAY>();
            for (int i = 0; i < 4; i++)
            {
                string year = (2004 + i).ToString();
                ContrInvestOBLList.Add(new ContrInvestOBL
                {
                    Year = year,
                    Min = 0,
                    Max = 0
                });
                ContrInvestAKCList.Add(new ContrInvestAKC
                {
                    Year = year,
                    Min = 0,
                    Max = 0
                });
                ContrInvestPAYList.Add(new ContrInvestPAY
                {
                    Year = year,
                    Min = 0,
                    Max = 0
                });
            }
            SubscribeToContrInvestListItemEvents();
            _mContrInvestCreated = true;
        }

        private void SubscribeToContrInvestListItemEvents()
        {
            for (int i = 0; i < 4; i++)
            {
                ContrInvestOBLList[i].PropertyChanged += ListItemPropertyChanged;
                ContrInvestAKCList[i].PropertyChanged += ListItemPropertyChanged;
                ContrInvestPAYList[i].PropertyChanged += ListItemPropertyChanged;
            }
        }

        void ListItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        private bool _mRewardCostCreated;
        private void CreateRewardCost()
        {
            _rewardCost = new RewardCost();

            _mRewardCostCreated = true;
        }

        public void ExecuteStopCO()
        {
            bool wasChanged = IsDataChanged;
            _contract = DataContainerFacade.GetByID<Contract, long>(InternalID);
            UpdateContragent(_legalEntity.ID, _legalEntity.GetContragent());
            OnPropertyChanged("DissolutionDate");
            OnPropertyChanged("DissolutionBase");
            IsDataChanged = wasChanged;

            JournalLogger.LogChangeStateEvent(this, "Договор расторгнут");
        }

        public void ExecuteResumeCO()
        {
            bool wasChanged = IsDataChanged;
            _contract = DataContainerFacade.GetByID<Contract, long>(InternalID);
            UpdateContragent(_legalEntity.ID, _legalEntity.GetContragent());
            OnPropertyChanged("DissolutionDate");
            OnPropertyChanged("DissolutionBase");
            IsDataChanged = wasChanged;

            JournalLogger.LogChangeStateEvent(this, "Договор заключен");
        }

        public static void ExecuteResumeCOExternal(long contractId)
        {
            var contract = DataContainerFacade.GetByID<Contract, long>(contractId);
            var le = contract.GetLegalEntity();
            UpdateContragent(le.ID, le.GetContragent());
        }

        private static void UpdateContragent(long leId, Contragent contragent)
        {
            if(contragent == null || string.Equals(contragent.TypeName, "НПФ", StringComparison.OrdinalIgnoreCase) )
                return;

            var contracts =
                DataContainerFacade.GetListByPropertyConditions<Contract>(
                    new List<ListPropertyCondition>
                    {
                        ListPropertyCondition.Equal("LegalEntityID", leId),
                        ListPropertyCondition.NotEqual("Status", -1)
                    });

            if (contracts == null) return;
            ContragentHelper.UpdateContragentStatus(contragent, contracts.ToList());

            DataContainerFacade.Save(contragent);
        }

        private static void OnRefreshCall()
        {
            if (GetViewModel == null) return;
            var vm = GetViewModel(typeof(SIViewModel)) as SIViewModel;
            if (vm != null)
                vm.RefreshContractsList();
        }

        private readonly bool _notSd;
        public bool NotSD => _notSd;

        public SIContractViewModel(long id, ViewModelState action)
            : base(typeof(SIListViewModel), typeof(SIContractsListViewModel), typeof(VRContractsListViewModel), typeof(InsuranceArchiveSIListViewModel), typeof(InsuranceSIListViewModel))
        {

            DataObjectTypeForJournal = typeof(Contract);
            RefreshConnectedCardsViewModels += OnRefreshCall;

            _deletesupagr = new DelegateCommand(o => CanExecuteDeleteSupAgreement(),
                o => ExecuteDeleteSupAgreement());

            _mContrInvestProperties = (from prop in GetType().GetProperties()
                                       where prop.Name.StartsWith("ContrInvest")
                                       where prop.Name.EndsWith("MIN") || prop.Name.EndsWith("MAX")
                                       select prop.Name).ToArray();
            DocumentTypesList = DataContainerFacade.GetList<DocType>();
            _notSd = true;
            if (action == ViewModelState.Create)
            {
                _legalEntity = DataContainerFacade.GetByID<LegalEntity, long>(id);
                _contract = new Contract
                {
                    LegalEntityID = _legalEntity.ID,
                    ContractDate = DateTime.Now
                };

                //SelectedContractName = ContractNamesList.First();
                SelectedDocumentType = DocumentTypesList.First();

                _bankAccount = new BankAccount();

                CreateContrInvest();

                CreateRewardCost();

                if (_legalEntity.GetContragent().TypeName == "СД")
                    _notSd = false;
            }
            else
            {
                try
                {
                    ID = id;
                    _contract = DataContainerFacade.GetByID<Contract, long>(ID);
                    _bankAccount = _contract.GetBankAccount() ?? new BankAccount(); //для НСИ
                    _legalEntity = _contract.GetLegalEntity();

                    if (_legalEntity.GetContragent().TypeName == "СД")
                        _notSd = false;

                    if (ContractDate.HasValue && ContractEndDate.HasValue)
                        Duration = ContractEndDate.Value.AddDays(-1).Year - ContractDate.Value.Year;


                    SelectedDocumentType = DocumentTypesList.FirstOrDefault(
                        documentType => documentType.ID == _contract.DocumentTypeID
                    ) ?? DocumentTypesList.FirstOrDefault(); //для НСИ

                    var contrInvests = new List<ContrInvest>(_contract.GetContrInvests().Cast<ContrInvest>());
                    if (contrInvests.Count > 0)
                    {
                        _contrInvest = contrInvests[0];
                        ContrInvestOBLList = _contrInvest.GetContrInvestOBLList().Cast<ContrInvestOBL>().ToList();
                        ContrInvestAKCList = _contrInvest.GetContrInvestAKCList().Cast<ContrInvestAKC>().ToList();
                        ContrInvestPAYList = _contrInvest.GetContrInvestPAYList().Cast<ContrInvestPAY>().ToList();
                        if (ContrInvestOBLList.Count == 0 && ContrInvestAKCList.Count == 0 && ContrInvestPAYList.Count == 0) CreateContrInvest(true);
                        else SubscribeToContrInvestListItemEvents();
                    }
                    else
                    {
                        CreateContrInvest();
                        _contrInvest.ContractID = ID;
                    }

                    var rewardCostList = new List<RewardCost>(_contract.GetRewardCosts().Cast<RewardCost>());
                    if (rewardCostList.Count > 0)
                    {
                        _rewardCost = rewardCostList[0];
                    }
                    else
                    {
                        CreateRewardCost();
                        _rewardCost.ContractID = ID;
                    }

                    RefreshSupAgreements();
                }
                catch (Exception e)
                {
                    if (DoNotThrowExceptionOnError == false)
                        throw;

                    Logger.WriteException(e);
                    throw new ViewModelInitializeException();
                }
            }

            var names = DataContainerFacade.GetList<ContractName>();
            if (!NotSD) names = names.FindAll(a => !a.Name.Contains("Договор доверительного"));
            ContractNamesList = names;

            SelectedContractName = action == ViewModelState.Edit ? ContractNamesList.FirstOrDefault(c => c.ID == _contract.ContractNameID) : ContractNamesList.First();
        }

        #region Delete sup agreement
        public SupAgreement SelectedSupAgreement { get; set; }

        private readonly ICommand _deletesupagr;
        public ICommand DeleteSupAgreement => _deletesupagr;

        private void ExecuteDeleteSupAgreement()
        {
            if (SelectedSupAgreement == null) return;
            if (!DialogHelper.ShowQuestion("Удалить выбранное соглашение?", string.Format("Удаление дополнительного соглашения {0}", SelectedSupAgreement.Number))) return;

            DataContainerFacade.Delete(SelectedSupAgreement);
            CloseSupAgreementWindow(SelectedSupAgreement.ID);
            SelectedSupAgreement = null;
            JournalLogger.LogDeleteEvent(this);
            RefreshSupAgreements();
            UpdateContractSupAgreementEndDate();
            DataContainerFacade.Save(_contract);
        }

        public bool CanExecuteDeleteSupAgreement()
        {
            return SelectedSupAgreement != null && CheckDeleteAccess(typeof(SupAgreementViewModel));
        }
        #endregion

        public void RefreshSupAgreements()
        {
            if (_contract.ExtensionData != null)
                _contract.ExtensionData.Remove("SupAgreements");
            SupAgreementsList = _contract.GetSupAgreements().ToList();
            _contract.ContractSupAgreementEndDate = null;
            var isDataChanged = IsDataChanged;
            OnPropertyChanged("ContractSupAgreementEndDate");
            if (!isDataChanged) IsDataChanged = false;}

        #region Execute Implementations

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

       /* private void SaveNewContrInvest(long pContractID)
        {
            _contrInvest.ContractID = pContractID;
            long contrInvestID = DataContainerFacade.Save<ContrInvest, long>(_contrInvest);
            _contrInvest.ID = contrInvestID;
            foreach (ContrInvestOBL obl in ContrInvestOBLList)
            {
                obl.ContrInvestID = contrInvestID;
                DataContainerFacade.Save<ContrInvestOBL, long>(obl);
            }
            foreach (ContrInvestAKC akc in ContrInvestAKCList)
            {
                akc.ContrInvestID = contrInvestID;
                DataContainerFacade.Save<ContrInvestAKC, long>(akc);
            }
            foreach (ContrInvestPAY pay in ContrInvestPAYList)
            {
                pay.ContrInvestID = contrInvestID;
                DataContainerFacade.Save<ContrInvestPAY, long>(pay);
            }
        }*/

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                SaveSIContractItem data;
                SaveSIContractResultItem result;
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                        data = new SaveSIContractItem
                        {
                            BankAccount = NotSD ? _bankAccount : null,
                            Contract = _contract,
                            RewardCost = _rewardCost,
                            ContrInvest = _contrInvest,
                            ContrInvestOBLList = ContrInvestOBLList ?? new List<ContrInvestOBL>(),
                            ContrInvestAKCList = ContrInvestAKCList ?? new List<ContrInvestAKC>(),
                            ContrInvestPAYList = ContrInvestPAYList ?? new List<ContrInvestPAY>()
                        };

                        //if (_mRewardCostCreated)
                        //    _rewardCost.ContractID = ID;

                        result = BLServiceSystem.Client.SaveSIContract(data, false);
                        if (_mContrInvestCreated)
                        {
                            _contrInvest.ContractID = result.ContractId;
                            _contrInvest.ID = result.ContrInvestId;
                        }
                        if (_mRewardCostCreated)
                            _rewardCost.ID = result.RewardCostId;
                        #region OLD
                        /*                        if (NotSD)
                            DataContainerFacade.Save<BankAccount, long>(_bankAccount);
                        DataContainerFacade.Save<Contract, long>(_contract);


                        if (_mContrInvestCreated)
                            SaveNewContrInvest(ID);
                        else
                        {
                            DataContainerFacade.Save<ContrInvest, long>(_contrInvest);
                            for (int i = 0; i < 4; i++)
                            {
                                DataContainerFacade.Save<ContrInvestOBL, long>(ContrInvestOBLList[i]);
                                DataContainerFacade.Save<ContrInvestPAY, long>(ContrInvestPAYList[i]);
                                DataContainerFacade.Save<ContrInvestAKC, long>(ContrInvestAKCList[i]);
                            }
                        }

                        if (_mRewardCostCreated)
                            _rewardCost.ContractID = ID;
                        DataContainerFacade.Save<RewardCost, long>(_rewardCost);*/
                        #endregion
                        break;
                    case ViewModelState.Create:
                        //gen data
                        data = new SaveSIContractItem
                        {
                            BankAccount = NotSD ? _bankAccount : null,
                            Contract = _contract,
                            RewardCost = _rewardCost,
                            ContrInvest = _contrInvest,
                            ContrInvestOBLList = ContrInvestOBLList ?? new List<ContrInvestOBL>(),
                            ContrInvestAKCList = ContrInvestAKCList ?? new List<ContrInvestAKC>(),
                            ContrInvestPAYList = ContrInvestPAYList ?? new List<ContrInvestPAY>()
                        };
#region OLD
                        /*if (NotSD)
                            _contract.BankAccountID = DataContainerFacade.Save<BankAccount, long>(_bankAccount);
                        ID = DataContainerFacade.Save<Contract, long>(_contract);
                        _contract.ID = ID;
                        SaveNewContrInvest(ID);
                        UpdateContragent();

                        _rewardCost.ContractID = ID;
                        _rewardCost.ID = DataContainerFacade.Save<RewardCost, long>(_rewardCost);
                        */
#endregion
                        //parse result
                        result = BLServiceSystem.Client.SaveSIContract(data, true);
                        if (NotSD) _contract.BankAccountID = result.BankAccountId;
                        _contrInvest.ContractID = _rewardCost.ContractID = ID = _contract.ID = result.ContractId;
                        _contrInvest.ID = result.ContrInvestId;
                        _rewardCost.ID = result.RewardCostId;

                        UpdateContragent(_legalEntity.ID, _legalEntity.GetContragent());
                        if (!_contract.OperateSum.HasValue)
                            _contract.OperateSum = 0;

                        OnPropertyChanged("IsContractSaved");
                        break;
                }
            }
        }
        #endregion

        #region Validation
        private bool Validate()
        {
            foreach (string prop in _mContrInvestProperties)
            {
                if (!string.IsNullOrEmpty(this[prop]))
                    return false;
            }

            bool res =
                string.IsNullOrEmpty(this["SelectedContractName"]) &&
                string.IsNullOrEmpty(this["ContractDate"]) &&
                string.IsNullOrEmpty(this["ContractNumber"]) &&
                string.IsNullOrEmpty(this["Duration"]) &&
				string.IsNullOrEmpty(this["PortfolioName"]) &&

                string.IsNullOrEmpty(this["SelectedDocumentType"]) &&
                string.IsNullOrEmpty(this["DocumentNumber"]) &&
                string.IsNullOrEmpty(this["DocumentDate"]) &&

                string.IsNullOrEmpty(this["RewardCostMaxCost"]) &&
                string.IsNullOrEmpty(this["RewardCostMaxPaySD"]) &&
                string.IsNullOrEmpty(this["RewardCostAmountReward"]);

            bool res2 = string.IsNullOrEmpty(this["BankAccountLegalEntityName"]) &&
                string.IsNullOrEmpty(this["BankAccountAccountNumber"]) &&
                string.IsNullOrEmpty(this["BankAccountBankName"]) &&
                string.IsNullOrEmpty(this["BankAccountBankLocation"]) &&
                string.IsNullOrEmpty(this["BankAccountCorrespondentAccountNumber"]) &&
                string.IsNullOrEmpty(this["BankAccountBIK"]) &&
                string.IsNullOrEmpty(this["BankAccountINN"]) &&
                string.IsNullOrEmpty(this["BankAccountKPP"]);

            return !NotSD ? res : (res && res2);
        }

        private const string INVALID_SELECTED_CONTRACT_NAME = "Не выбрано название договора";
        private const string INVALID_CONTRACT_DATE = "Не введена дата регистрации договора";
        private const string INVALID_CONTRACT_NUMBER = "Не введен номер договора";
        private const string INVALID_CONTRACT_DURATION = "Неверно введен срок действия договора";

        private const string INVALID_SELECTED_DOCUMENT_TYPE = "Неверно выбран тип документа";
        private const string INVALID_DOCUMENT_NUMBER = "Не введен номер документа";
        private const string INVALID_DOCUMENT_DATE = "Не введена дата документа";

        private const string INVALID_BA_LE_NAME = "Не введено наименование получателя платежа";
        private const string INVALID_BA_ACCOUNT_NUMBER = "Неверно введен номер рассчетного счета получателя платежа";
        private const string INVALID_BA_BANK_NAME = "Не введен банк получателя платежа";
        private const string INVALID_BA_BANK_LOCATION = "Не введено местонахождение банка получателя платежа";
        private const string INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER = "Неверно введен корреспондентский счет банка получателя платежа";
        private const string INVALID_BA_BIK = "Неверно введен БИК банка получателя платежа";
        private const string INVALID_BA_INN = "Неверно введен ИНН банка получателя платежа";
        private const string INVALID_BA_KPP = "Неверно введен КПП банка получателя платежа";

        private const string INVALID_PERCENT_VALUE = "Значение должно быть в диапазоне от 0 до 100";
        public override string this[string columnName]
        {
            get
            {
                if (columnName.StartsWith("ContrInvest") && (columnName.EndsWith("MIN") || columnName.EndsWith("MAX")))
                {
                    var property = GetType().GetProperty(columnName);
                    if (property == null)
                        return null;

                    long? value = (long?)property.GetValue(this, null);
                    return value == null || value < 0 || value > 100 ? INVALID_PERCENT_VALUE : null;
                }

                switch (columnName)
                {
                    case "SelectedContractName":
                        return SelectedContractName == null ? INVALID_SELECTED_CONTRACT_NAME : null;
                    case "ContractDate":
                        return ContractDate == null ? INVALID_CONTRACT_DATE : null;
                    case "ContractNumber":
                        return string.IsNullOrEmpty(ContractNumber) ? INVALID_CONTRACT_NUMBER : ContractNumber.ValidateMaxLength(32);
                    case "PortfolioName":
                        return PortfolioName.ValidateMaxLength(256);
                    case "OtherInfo":
                        return OtherInfo.ValidateMaxLength(512);
                    case "RewardCostEndCh":
                        return RewardCostEndCh.ValidateMaxLength(128);
                    case "Duration":
                        return Duration == null || Duration.Value < 1 ? INVALID_CONTRACT_DURATION : null;

                    case "SelectedDocumentType":
                        return SelectedDocumentType == null ? INVALID_SELECTED_DOCUMENT_TYPE : null;
                    case "DocumentNumber":
                        return string.IsNullOrEmpty(DocumentNumber) ? INVALID_DOCUMENT_NUMBER : DocumentNumber.ValidateMaxLength(32);
                    case "DocumentDate":
                        return DocumentDate == null ? INVALID_DOCUMENT_DATE : null;

                    case "BankAccountLegalEntityName":
                        return string.IsNullOrEmpty(BankAccountLegalEntityName) ? INVALID_BA_LE_NAME : BankAccountLegalEntityName.ValidateMaxLength(512);
                    case "BankAccountAccountNumber":
                        return BankAccountAccountNumber == null || BankAccountAccountNumber.Length != 20 ? INVALID_BA_ACCOUNT_NUMBER : null;
                    case "BankAccountBankName":
                        return string.IsNullOrEmpty(BankAccountBankName) ? INVALID_BA_BANK_NAME : BankAccountBankName.ValidateMaxLength(512);
                    case "BankAccountBankLocation":
                        return string.IsNullOrEmpty(BankAccountBankLocation) ? INVALID_BA_BANK_LOCATION : BankAccountBankLocation.ValidateMaxLength(512);
                    case "BankAccountCorrespondentAccountNumber":
                        return BankAccountCorrespondentAccountNumber == null || BankAccountCorrespondentAccountNumber.Length != 20 ? INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER : null;
                    case "BankAccountBIK":
                        return BankAccountBIK == null || BankAccountBIK.Length != 9 ? INVALID_BA_BIK : null;
                    case "BankAccountINN":
                        return BankAccountINN == null || BankAccountINN.Length != 10 ? INVALID_BA_INN : null;
                    case "BankAccountKPP":
                        return BankAccountKPP == null || BankAccountKPP.Length != 9 ? INVALID_BA_KPP : null;

                    case "RewardCostMaxCost":
                        return RewardCostMaxCost < 0 || RewardCostMaxCost > 100 ? INVALID_PERCENT_VALUE : null;
                    case "RewardCostMaxPaySD":
                        return RewardCostMaxPaySD < 0 || RewardCostMaxPaySD > 100 ? INVALID_PERCENT_VALUE : null;
                    case "RewardCostAmountReward":
                        return RewardCostAmountReward < 0 || RewardCostAmountReward > 100 ? INVALID_PERCENT_VALUE : null;
                    default:
                        return null;
                }
            }
        }
        #endregion
    }
}
