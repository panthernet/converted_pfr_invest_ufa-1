﻿using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class DueUndistributedAccurateViewModel : ApsAccurateModelBase
    {
        public DueUndistributedAccurateViewModel(DueUndistributedViewModel parentVM)
            : base(parentVM)
        {           
            Dopaps.DocKind = 4; // потому что страховые взносы умерших
        }
    }
}
