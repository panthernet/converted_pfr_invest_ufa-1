﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class QuarterAccurateViewModel : ViewModelCard, IUpdateRaisingModel, IUpdateListenerModel, IRequestCloseViewModel
	{
		public DopSPN DopSPN { get; private set; }
		public Month Month { get; private set; }
		public IList<AddSPN> AddSPNList { get; private set; }
		public IList<DopSPN> DopSPNList { get; private set; }

		public string DocKind { get; private set; }

		private decimal m_LastSum;
		//public decimal MSumm
		//{
		//	get
		//	{
		//		return DopSPN.MSumm ?? 0;
		//	}
		//	set
		//	{
		//		DopSPN.MSumm = value;
		//		//RefreshChSum();
		//		OnPropertyChanged("MSumm");
		//	}
		//}

		public decimal ChSumm
		{
			get
			{
				return DopSPN.ChSumm ?? 0;
			}

			set
			{
				if (DopSPN.ChSumm.GetValueOrDefault() != value)
				{
					DopSPN.ChSumm = value;
					OnPropertyChanged("ChSumm");
				}
			}
		}

		public DateTime? OperationDate
		{
			get
			{
				return DopSPN.OperationDate;
			}
			set
			{
				DopSPN.OperationDate = value;
				OnPropertyChanged("OperationDate");
			}
		}

		public DateTime? DocDate
		{
			get
			{
				return DopSPN.Date;
			}
			set
			{
				DopSPN.Date = value;
				OnPropertyChanged("DocDate");
			}
		}

		public string RegNum
		{
			get
			{
				return DopSPN.RegNum;
			}
			set
			{
				DopSPN.RegNum = value;
				OnPropertyChanged("RegNum");
			}
		}

		/// <summary>
		/// Отображается в поле как месяца квартального уточнения
		/// </summary>
		public string QuarterName
		{
			get
			{
				switch (DopSPN.Quarter)
				{
					case 1: return "3 месяца";
					case 2: return "6 месяцев";
					case 3: return "9 месяцев";
					case 4: return "12 месяцев";
					default: return null;
				}
			}
		}

		/// <summary>
		/// Это режим ДСВ?
		/// </summary>
		public bool IsDSV => DocKind == DueDocKindIdentifier.DueDSVAccurateYear;

	    public string MSummLabel => IsDSV ? "Отклонения, итого (руб.):" : "Отклонения (руб.):";

	    /// <summary>
		/// Режим чтения для поля "Отклонения, итого"
		/// </summary>
		public bool IsMSummTotalReadOnly => IsDSV || IsMSummReadOnly;

	    public GridLength MSummPartHeight => IsDSV ? new GridLength(38) : new GridLength(0);

	    /// <summary>
		/// Отображается в поле как год квартального уточнения
		/// </summary>
		public long? QuarterYear => DopSPN.QuarterYear + 2000;

	    public string Comment
		{
			get
			{
				return DopSPN.Comment;
			}
			set
			{
				DopSPN.Comment = value;
				OnPropertyChanged("Comment");
			}
		}

		public bool IsMSummReadOnly => ReadAccessOnly;

	    public string DocDateError { get; private set; }
		public DateTime? MaxDate { get; private set; }
		public DateTime? MinDate { get; private set; }

		private PortfolioFullListItem m_Account;
		public PortfolioFullListItem Account
		{
			get
			{
				return m_Account;
			}
			set
			{
				m_Account = value;
				if (value != null)
				{
					DopSPN.PortfolioID = value.Portfolio.ID;
					DopSPN.LegalEntityID = value.BankLE.ID;
					OnPropertyChanged("AccountNumber");
					OnPropertyChanged("Bank");
					OnPropertyChanged("PFName");
					OnPropertyChanged("Account");
				}
			}
		}

		public decimal MSumm
		{
			get
			{
				if (IsDSV)
				{
					DopSPN.MSumm = MSummNakop + MSummEmployer + MSummOther + MSummPerc;
				}

				return DopSPN.MSumm ?? 0;
			}
			set
			{
				DopSPN.MSumm = value;
				OnPropertyChanged("MSumm");
			}
		}

		public decimal MSummNakop
		{
			get { return DopSPN.MSummNakop ?? 0; }
			set
			{	
				DopSPN.MSummNakop = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummNakop");
					OnPropertyChanged("MSumm");
				}
			}
		}

		public decimal MSummOther
		{
			get { return DopSPN.MSummOther ?? 0; }
			set
			{
				DopSPN.MSummOther = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummOther");
					OnPropertyChanged("MSumm");
				}
			}
		}

		public decimal MSummPerc
		{
			get { return DopSPN.MSummPerc ?? 0; }
			set
			{
				DopSPN.MSummPerc = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummPerc");
					OnPropertyChanged("MSumm");
				}
			}
		}

		public decimal MSummEmployer
		{
			get { return DopSPN.MSummEmployer ?? 0; }
			set
			{
				DopSPN.MSummEmployer = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummEmployer");
					OnPropertyChanged("MSumm");
				}
			}
		}

		public bool ReadAccessOnly => IsReadOnly || State != ViewModelState.Create && (State == ViewModelState.Read ||
		                                                                               !(AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.OFPR_manager) ||
		                                                                                 AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator)));

	    /// <summary>
		/// Требуется оповещение нижестоящих квартальных уточнений и уточнений за месяц
		/// </summary>
		private bool _broadcastToLesserDops;

		public override bool CanExecuteDelete()
		{
			return !IsReadOnly && State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType)
		{
			_broadcastToLesserDops = true;
			DataContainerFacade.Delete(DopSPN);
			base.ExecuteDelete(DocOperation.Delete);
		}

		protected override void AfterExecuteSave()
		{
			_broadcastToLesserDops = false;
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
					if (State == ViewModelState.Create)
						_broadcastToLesserDops = true;
					ID = DopSPN.ID = DataContainerFacade.Save(DopSPN);
					break;
			}
		}

		public override bool CanExecuteSave()
		{
			return !IsReadOnly && Validate() && IsDataChanged;
		}

		#region Validation
		private const string ERROR_DOCDATE = "Введите дату уточнения";
		private const string ERROR_OPDATE = "Введите дату проведения операции ДИ";
		private const string ERROR_REGNUM = "Введите регистрационный номер";
		private const string ERROR_SUMM = "Сумма должна быть больше нуля";
		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "OperationDate": return OperationDate.ValidateRequired();
					case "DocDate": if (DocDate == null) return ERROR_DOCDATE;
						return CheckDocDate();
					case "RegNum": return string.IsNullOrWhiteSpace(RegNum) ? ERROR_REGNUM : RegNum.ValidateMaxLength(32);
					case "MSumm": return MSumm.ValidateMaxFormat();
					case "Comment": return Comment.ValidateMaxLength(512);
					default: return null;
				}
			}
		}

		private string CheckDocDate()
		{
			if (DocDate.HasValue)
				if (MinDate.HasValue)
				{
					if (MaxDate.HasValue)
					{
						//Минимальной обе даты есть - проверяем диапазон
						if (MinDate.Value.Date > DocDate.Value.Date || DocDate.Value.Date > MaxDate.Value.Date)
							return string.Format(m_DocDateErrorFormatRange, MinDate, MaxDate);
					}
					else
					{
						//только минимальная дата - проверяем ее
						if (MinDate.Value.Date > DocDate.Value.Date)
							return string.Format(m_DocDateErrorFormatGreater, MinDate);
					}
				}
				else
				{
					//Минимальной даты нет - нечего проверять
					return null;
				}
			else
				//Дата уточнения не указана - ошибка
				return ERROR_DOCDATE;
			return null;
		}

		private bool Validate()
		{
			return string.IsNullOrEmpty(this["OperationDate"]) &&
				   string.IsNullOrEmpty(this["RegNum"]) &&
				   string.IsNullOrEmpty(this["MSumm"]) &&
				   string.IsNullOrEmpty(this["Comment"]) &&
				   string.IsNullOrEmpty(this["DocDate"]);
		}
		#endregion

		[Obsolete("Don't use calculation", true)]
		private void RefreshChSum()
		{
			ChSumm = MSumm - m_LastSum;
		}

		private const string m_DocDateErrorFormatGreater = "Значение должно быть не меньше {0}";
		private const string m_DocDateErrorFormatRange = "Значение должно быть в диапазоне от {0} до {1}";

		private int MonthToQuarter(long? month)
		{
			month = month ?? 1;
			return ((int)month.Value - 1) / 3 + 1;
		}

		private void ReInitMaxMinDatesAndLastSum()
		{
			//Мин дата - дата последней записи в квартале

			var lastAdd = AddSPNList.Where(a => MonthToQuarter(a.MonthID) == DopSPN.Quarter).OrderByDescending(a => a.MonthID).FirstOrDefault();
			if (lastAdd != null)
			{
				MinDate = lastAdd.NewDate;
				var lastDop = lastAdd.GetDopSPNList().OrderByDescending(a => a.Date).FirstOrDefault();
				if (lastDop != null)
				{
					MinDate = lastDop.Date;
				}
			}
		}

		protected void RefreshDueViewModel()
		{
			if (GetViewModel != null)
			{
				DueViewModel vm = GetViewModel(typeof(DueViewModel)) as DueViewModel;
				if (vm != null)
					vm.RefreshDopSPNList();
			}
		}

		private bool isReadOnly;
		public override bool IsReadOnly => isReadOnly;


	    public QuarterAccurateViewModel(ViewModelState action, long id, string docKind = "")
		//: base(typeof(DueListViewModel))
		{
			DataObjectTypeForJournal = typeof(DopSPN);
			State = action;
			DocKind = docKind;

			if (State == ViewModelState.Create)
			{
				ID = id;
				DopSPN = new DopSPN
				{
					//AddSpnID = id,
					//PeriodID = AddSPN.MonthID,
					OperationDate = DateTime.Today,
					DocKind = (int)DopSPN.Kinds.DueQuarter
				};

				var AddSPN = DataContainerFacade.GetByID<AddSPN>(id);

				if (AddSPN != null)
				{
					DopSPN.QuarterYear = AddSPN.YearID;
					DopSPN.QuarterPfrBankAccountID = AddSPN.PFRBankAccountID;
					DopSPN.PortfolioID = AddSPN.PortfolioID;
					DopSPN.Quarter = (int)((AddSPN.MonthID ?? 1) - 1) / 3 + 1;
					DopSPN.LegalEntityID = AddSPN.LegalEntityID;

					var dopList = BLServiceSystem.Client.GetDopSPNList(DopSPN.QuarterPfrBankAccountID ?? 0, DopSPN.PortfolioID ?? 0);
					var old = dopList.FirstOrDefault(d => d.Quarter == DopSPN.Quarter);
					DopSPN = old ?? DopSPN;
					if (old != null)
					{
						State = ViewModelState.Edit;
						DopSPN = old;
						ID = DopSPN.ID;
					}

					DopSPNList = dopList;
				}
			}
			else
			{
				ID = id;
				DopSPN = DataContainerFacade.GetByID<DopSPN>(ID);
				DopSPNList = BLServiceSystem.Client.GetDopSPNList(DopSPN.QuarterPfrBankAccountID ?? 0, DopSPN.PortfolioID ?? 0);
			}

			AddSPNList = BLServiceSystem.Client.GetAddSPNList(DopSPN.QuarterPfrBankAccountID ?? 0, DopSPN.PortfolioID ?? 0);
			Month = DopSPN.GetMonth();
			if (DopSPN.PortfolioID.HasValue && DopSPN.LegalEntityID.HasValue && DopSPN.QuarterPfrBankAccountID.HasValue)
				Account = BLServiceSystem.Client.GetPortfolioFullForBank(DopSPN.PortfolioID.Value, DopSPN.LegalEntityID.Value, DopSPN.QuarterPfrBankAccountID.Value);



			ReInitMaxMinDatesAndLastSum();
			RefreshConnectedCardsViewModels = RefreshDueViewModel;
			isReadOnly = UpdateReadOnly();

		}

		private bool UpdateReadOnly()
		{
			//Создание - можно редактировать
			if (DopSPN.ID == 0)
				return false;

			//Пенни могут появлятся в системе только через ДК, и никак иначе
			if (DopSPN.DocKind == (int)DopSPN.Kinds.PennyQuarter)
				return true;

			//Есть ДК - значит редактировать уже нельзя
			if (BLServiceSystem.Client.IsDateClosedByKDoc(DopSPN.PortfolioID.Value, new DateTime(DopSPN.Date.Value.Year, DopSPN.Quarter.Value * 3, 1)))
				return true;

			//есть ли вышестоящие квартальные уточнения
			if (DueHelper.GetQDIsBlockedByQuarterDop(DopSPN))
				return true;

			return false;
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>>
			{ 
				new KeyValuePair<Type, long>(typeof(DopSPN), DopSPN.ID)
			};

			//обновляем нижестоящие допники только, если удаляем или создаем уточнение за квартал
			if (_broadcastToLesserDops)
			{
				//получаем все месячные уточнения за временной диапазон квартального уточнения и совпадении по портфелю
				//оповещаем открытые карточки для динамического закрытия полей ввода
				var mList = DueHelper.GetAllMonthAccuratesForQuarter(DopSPN);
				if (mList != null && mList.Any())
					mList.ForEach(a => list.Add(new KeyValuePair<Type, long>(typeof(DopSPN), a.ID)));

				//выбираем все нижестоящие уточнения за квартал и оповещаем карточки
				var qList = DueHelper.GetAllLesserQDForQuarter(DopSPN);
				if (qList != null && qList.Any())
					qList.ForEach(a => list.Add(new KeyValuePair<Type, long>(typeof(DopSPN), a.ID)));
			}
			return list;
		}

		public event EventHandler RequestClose;

		public void CloseWindow()
		{
			if (RequestClose != null)
			{
				RequestClose(this, EventArgs.Empty);
			}
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(DopSPN), typeof(AddSPN) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(DopSPN))
			{
				if (DopSPN != null && id == DopSPN.ID)
				{
					var dspn = DataContainerFacade.GetByID<DopSPN>(id);
					if (dspn == null)
					{
						CloseWindow();
						return;
					}
					DopSPN = dspn;
					//обновляем видимость полей
					var isDataChanged = IsDataChanged;
					isReadOnly = UpdateReadOnly();
					OnPropertyChanged("IsReadOnly");
					OnPropertyChanged("ReadAccessOnly");
					if (!isDataChanged)
						IsDataChanged = false;
				}
			}

			// обработка закрытия окна, если сущность не сохранена в БД и удаляется родительская сущность
			if (type == typeof(AddSPN))
			{
				if (State == ViewModelState.Create || id == ID)
				{
					CloseWindow();
				}
			}
		}
	}
}
