﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class ViolationCategoryViewModel : ViewModelCard
    {
        private readonly CategoryF140 _categoryF140;

        private IList<CategoryF140> m_OherCategories;

        public string ViolationCharacter
        {
            get
            {
                return _categoryF140.ViolationCharacter;
            }
            set
            {
                _categoryF140.ViolationCharacter = value;
                OnPropertyChanged("ViolationCharacter");
            }
        }

        private readonly Regex _violationCharacterCodeRegex = new Regex(@"\b\d{3}\b", RegexOptions.IgnoreCase);
        public string ViolationCharacterCode
        {
            get {
                return _categoryF140.ViolationCharacterCode?.Trim();
            }
            set
            {
                _categoryF140.ViolationCharacterCode = value;
                OnPropertyChanged("ViolationTypeCode");
                OnPropertyChanged("ViolationCharacterCode");                
            }
        }

        public string ViolationType
        {
            get
            {
                return _categoryF140.ViolationType;
            }
            set
            {
                _categoryF140.ViolationType = value;
                OnPropertyChanged("ViolationType");
            }
        }

        private readonly Regex _violationTypeCodeRegex = new Regex(@"\b\d{2}\b", RegexOptions.IgnoreCase);
        public string ViolationTypeCode
        {
            get {
                return _categoryF140.ViolationTypeCode?.Trim();
            }
            set
            {
                _categoryF140.ViolationTypeCode = value;
                OnPropertyChanged("ViolationTypeCode");
                OnPropertyChanged("ViolationCharacterCode");
                
            }
        }

        protected override bool BeforeExecuteSaveCheck()
        {
            m_OherCategories = DataContainerFacade.GetList<CategoryF140>()
                .Where(cat => cat.ID != ID).ToList();
            if (IsCodesNotUnique())
            {
                DialogHelper.ShowError("Категория с такими значениями в полях \"Код вида нарушения\" и \"Код характера нарушения\" уже существует");
                OnPropertyChanged("ViolationTypeCode");
                OnPropertyChanged("ViolationCharacterCode");
                return false;
            }

            return true;
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    {
                        ID = DataContainerFacade.Save(_categoryF140);
                        _categoryF140.ID = ID;
                    }
                    break;
                default:
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {
            var used = _categoryF140.GetCategoryUsage();
            if (used == 0)
            {
                DataContainerFacade.Delete(_categoryF140);
                base.ExecuteDelete(DocOperation.Delete);
            }
            else 
            {
                DialogHelper.ShowAlert($"Невозможно удалить категорию нарушения, так как она используется (Всего {used})");
            }
        }

        private const string ERROR_EMPTY = "Значение должно быть строкой до 1024 символов";
        private const string ERROR_DUPLICATE_CODE = "Такая комбинация кодов вида нарушения и характера нарушения уже существует";
        private const string ERROR_INVALID_CODE = "Неверный формат кода";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "ViolationType": return string.IsNullOrWhiteSpace(ViolationType) || ViolationType.Length > 1024 ? ERROR_EMPTY : null;
                    case "ViolationTypeCode": return string.IsNullOrWhiteSpace(ViolationTypeCode) ? ERROR_INVALID_CODE :
                                                     !_violationTypeCodeRegex.IsMatch(ViolationTypeCode) ? ERROR_INVALID_CODE :
                                                     IsCodesNotUnique() ? ERROR_DUPLICATE_CODE : null;
                    case "ViolationCharacter": return string.IsNullOrWhiteSpace(ViolationCharacter) || ViolationCharacter.Length > 1024 ? ERROR_EMPTY : null;
                    case "ViolationCharacterCode": return string.IsNullOrWhiteSpace(ViolationCharacterCode) ? ERROR_INVALID_CODE :
                                                          !_violationCharacterCodeRegex.IsMatch(ViolationCharacterCode) ? ERROR_INVALID_CODE :
                                                          IsCodesNotUnique() ? ERROR_DUPLICATE_CODE : null;
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return string.IsNullOrWhiteSpace(this["ViolationCharacter"]) &&
                   string.IsNullOrWhiteSpace(this["ViolationCharacterCode"]) &&
                   ViolationCharacterCode.Length == 3 &&
                   string.IsNullOrWhiteSpace(this["ViolationType"]) &&
                   string.IsNullOrWhiteSpace(this["ViolationTypeCode"]) &&
                   ViolationTypeCode.Length == 2;
        }

        private bool IsCodesNotUnique()
        {
            return m_OherCategories.Any(c => c.ViolationCharacterCode == ViolationCharacterCode && c.ViolationTypeCode == ViolationTypeCode);

        }

        public ViolationCategoryViewModel(long id, ViewModelState action)
            : base(typeof(ViolationCategoriesListViewModel))
        {
            DataObjectTypeForJournal = typeof(CategoryF140);
            State = action;
            ID = id;


            m_OherCategories = DataContainerFacade.GetList<CategoryF140>()
                .Where(cat => cat.ID != ID).ToList();
           
            _categoryF140 = State == ViewModelState.Create ? new CategoryF140() : DataContainerFacade.GetByID<CategoryF140>(ID);
        }
    }
}
