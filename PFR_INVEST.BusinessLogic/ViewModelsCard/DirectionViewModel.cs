﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class DirectionViewModel : ViewModelCard
    {
        #region Properties

        public Direction Direction { get; private set; }

        private List<BudgetListItem> _mBudgetsList;
        public List<BudgetListItem> BudgetsList
        {
            get
            {
                return _mBudgetsList;
            }
            set
            {
                _mBudgetsList = value;
                OnPropertyChanged("BudgetsList");
            }
        }

        private BudgetListItem _mSelectedBudget;
        public BudgetListItem SelectedBudget
        {
            get
            {
                return _mSelectedBudget;
            }
            set
            {
                _mSelectedBudget = value;
                if (value != null)
                    Direction.BudgetID = value.ID;
                OnPropertyChanged("SelectedBudget");
            }
        }

        private readonly List<string> _mRecipientsList = new List<string>{
            "ОПФР", 
            "ИЦПУ", 
            "Не определен"};
        public List<string> RecipientsList => _mRecipientsList;

        public string SelectedRecipient
        {
            get
            {
                return Direction.Recipient == null ? null : Direction.Recipient.Trim();
            }
            set
            {
                Direction.Recipient = value;
                OnPropertyChanged("SelectedRecipient");
            }
        }

        private List<DirectionTransferListItem> m_Transfers;
        public List<DirectionTransferListItem> Transfers
        {
            get
            {
                return m_Transfers;
            }
            set
            {
                m_Transfers = value;
                OnPropertyChanged("Transfers");
            }
        }

        public DateTime? Date
        {
            get
            {
                return Direction.Date;
            }
            set
            {
                Direction.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public DateTime? DIDate
        {
            get
            {
                return Direction.DIDate;
            }
            set
            {
                Direction.DIDate = value;
                OnPropertyChanged("DIDate");
            }
        }

        public string Number
        {
            get
            {
                return Direction.Number;
            }
            set
            {
                Direction.Number = value;
                OnPropertyChanged("Number");
            }
        }

        public string Comment
        {
            get
            {
                return Direction.Comment;
            }
            set
            {
                Direction.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        #endregion

        private void ExecuteDeleteDop()
        {
            if (SelectedDop.OperationType == DirectionTransferListItem.DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeReturn)
            {
                DataContainerFacade.Delete(DataContainerFacade.GetByID<Return>(SelectedDop.ID));
            }
            else
            {
                DataContainerFacade.Delete(DataContainerFacade.GetByID<Transfer>(SelectedDop.ID));
            }
            JournalLogger.LogModelEvent(this, JournalEventType.DELETE, SelectedDop.OperationType == DirectionTransferListItem.DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeReturn ? "Возврат" : "Перечисление");
            RefreshTransfers();
        }

        private bool CanExecuteDeleteDop()
        {
            return SelectedDop != null && DeleteAccessAllowed;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        public DirectionTransferListItem SelectedDop { get; set; }

        protected override void ExecuteDelete(int delType = DocOperation.Delete)
        {
            /*var tlist = DataContainerFacade.GetListByProperty<Transfer>("DirectionID", Direction.ID).ToList();
            foreach (var item in tlist)
                DataContainerFacade.Delete(item);

            DataContainerFacade.Delete(Direction);
            */
            BLServiceSystem.Client.DeleteDirectionSchils(Direction.ID);
            base.ExecuteDelete(DocOperation.Delete);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<Direction, long>(Direction);
                    break;
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save<Direction, long>(Direction);
                    Direction.ID = ID;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        private const string INVALID_DATE = "Не указана дата";
        private const string INVALID_NUMBER = "Неверно указан номер";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Date": return Date != null ? null : INVALID_DATE;
                    case "Number": return !string.IsNullOrEmpty(Number) ? Number.ValidateMaxLength(32) : INVALID_NUMBER;
                    case "Comment": return Comment.ValidateMaxLength(512);
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return ValidateFields();
        }

        public void RefreshTransfers()
        {
            bool alreadyChanged = IsDataChanged;
            Transfers = BLServiceSystem.Client.GetTransfersListForDirection(ID);
            if (!alreadyChanged)
                IsDataChanged = false;
        }

        public void LoadBudget(long lID)
        {
            SelectedBudget = BudgetsList.Find(b => b.ID == lID);
        }

        public void Load(long lID)
        {
            try
            {
                ID = lID;
                Direction = DataContainerFacade.GetByID<Direction, long>(lID);

                if (Direction.BudgetID.HasValue)
                    LoadBudget(Direction.BudgetID.Value);

                RefreshTransfers();
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        public ICommand DeleteDop { get; private set; }

        public override Type DataObjectTypeForJournal => typeof (Direction);

        public DirectionViewModel()
            : base(typeof(SchilsCostsListViewModel))
        {
            try
            {
                BudgetsList = BLServiceSystem.Client.GetBudgetsListHib().ConvertAll(b => new BudgetListItem(b));
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }

            DeleteDop = new DelegateCommand(o => CanExecuteDeleteDop(), o => ExecuteDeleteDop());

            Direction = new Direction();

            SelectedBudget = BudgetsList.FirstOrDefault();
            SelectedRecipient = RecipientsList.FirstOrDefault();

            Date = DateTime.Now;
        }
    }
}
