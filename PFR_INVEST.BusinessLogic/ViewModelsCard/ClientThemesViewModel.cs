﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    public class ClientThemesViewModel : ViewModelCardDialog
    {
        public List<string> DrawTypes { get; set; }
        public List<string> ThemesList { get; set; }
        public string Name { get; set; }

        public ClientThemesViewModel()
        {
            DrawTypes = new List<string> { "Аппаратный", "Программный" };
            ThemesList = ThemeItem.ThemesList.Select(a=> a.Name).ToList();
        }
    }
}
