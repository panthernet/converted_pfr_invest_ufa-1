﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    public interface IDepClaimAuctionProvider
    {
        long? AuctionID { get; }
    }

    /// <summary>
    /// Интерфейс поставщика инфо аукциона
    /// </summary>
    public interface IDepClaimAuctionInfoProvider : IDepClaimAuctionProvider
    {
        //int? DepClaim2Count { get; }
        //int? DepClaimMaxCount { get; }
        //int? DepClaimCommonCount { get; }

        DepClaimSelectParams.Statuses? AuctionStatus { get; }
    }

    public interface IDepClaimAuctionFullInfoProvider : IDepClaimAuctionInfoProvider
    {
        int? DepClaimMaxCount { get; }
        int? DepClaimCommonCount { get; }
    }


    /// <summary>
    /// Интерфейс для моделей на которых активна опция "Импорт Выписки из реестра заявок"
    /// </summary>
    public interface IDepClaim2ImportProvider : IDepClaimAuctionFullInfoProvider { }

    /// <summary>
    /// Интерфейс для моделей источника аукциона
    /// </summary>
    public interface IDepClaimAuctionSourceProvider : IDepClaimAuctionInfoProvider { }

    /// <summary>
    /// Интерфейс источника Сводного реестра заявок или Заявок, с учетом максимальной суммы размещения
    /// </summary>
    public interface IDepClaimMaxExportProvider : IDepClaimAuctionFullInfoProvider
    {
        bool IsMaxClaim { get; }
        bool IsCommonClaim { get; }
    }

    /// <summary>
    /// Интерфейс моделей для которых активна опция "Импорт Сводного реестра заявок" и "Импорт Сводного реестра заявок, с учетом максимальной суммы размещения"
    /// </summary>
    public interface IDepClaimMaxImportProvider : IDepClaimAuctionFullInfoProvider { }

    /// <summary>
    /// Интерфейс для моделей на которых активна опция создания оферт
    /// </summary>
    public interface IDepClaimOfferCreateProvider : IDepClaimAuctionInfoProvider {
        bool CanGenerateOfferts { get; }
    }


    /// <summary>
    /// Интерфейс для моделей для которых активна опция импорта Выписки из реестра заявок, подлежащих удовлетворению
    /// </summary>
    public interface IDepClaim2ConfirmImportProvider : IDepClaimAuctionFullInfoProvider { }

    /// <summary>
    /// Интерфейс моделей для которых активна опция экспорта "Уведомление о параметрах отбора заявок"
    /// </summary>
    public interface IDepClaimAuctionExportProvider : IDepClaimAuctionProvider { }

    


    /// <summary>
    /// Интерфейс моделей для которых активна опция экспорта "Экспорт Выписки из реестра заявок"
    /// </summary>
    public interface IDepClaim2ExportProvider : IDepClaimAuctionInfoProvider { }

    /// <summary>
    /// Интерфейс моделей для которых активна опция экспорта "Экспорт Выписки из реестра заявок"
    /// </summary>
    public interface IDepClaim2ConfirmExportProvider : IDepClaimAuctionInfoProvider { }

    /// <summary>
    /// Интерфейс моделей, которые предоставляют данные о торговой бирже
    /// </summary>
    public interface IDepClaimStockProvider : IDepClaimAuctionFullInfoProvider
    {
        bool IsMoscowStockSelected { get; }

		bool IsSPVBStockSelected { get; }
    }

}
