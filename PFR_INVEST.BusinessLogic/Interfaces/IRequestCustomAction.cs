using System;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    /// ��������� ��� �������� ������������ �������� ��� View �� Model
    /// </summary>
    public interface IRequestCustomAction
    {
        event EventHandler RequestCustomAction;
    }
}