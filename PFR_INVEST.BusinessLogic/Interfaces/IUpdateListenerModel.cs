﻿using System;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
	/// <summary>
	/// Интерфес для моделей, которые получают обновления данных и обрабатывают их
	/// </summary>
	public interface IUpdateListenerModel
	{
		Type[] UpdateListenTypes { get; }

		void OnDataUpdate(Type type, long id); 
	}
}
