﻿namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    /// Закрыть модель после сохранения
    /// </summary>
    public interface ICloseViewModelAfterSave
    {
    }
}
