﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class ViewModelListObservable<T> : ViewModelList<T>
    //where T : BaseDataObject
    {
        public bool IsRefreshListOnCurrentChanging { get; set; }

        protected ViewModelListObservable(object param = null) : base(param)
        {

        }

        private RangeObservableCollection<T> _list = new RangeObservableCollection<T>();
        public new RangeObservableCollection<T> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            protected set
            {
                if (_list == value) return;
                _list = value;
                OnPropertyChanged("List");
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        /// <summary>
        /// Заменяет старую запись в гриде на новую без передёргивания грида и сброса  позиции объекта в списке
        /// </summary>
        /// <param name="oldItem">Старая запись из грида</param>
        /// <param name="newItem">Новая запись для грида</param>
        protected void UpdateRecord(T oldItem, T newItem)
        {
            var prev = Current;

            var niCondi = newItem != null
                          && (!(newItem is IMarkedAsDeleted) || !(newItem as IMarkedAsDeleted).IsDeleted());
            int index = List.IndexOf(oldItem);
            if (oldItem != null)
            {
                if(niCondi)
                    List.RemoveWithoutNotification(oldItem);
                else List.Remove(oldItem);
            }
            if (niCondi)
            {
                if(index != -1)
                    List.Insert(index, newItem);
                else List.Add(newItem);
            }

            if (prev != null && !List.Contains(prev))
                Current = newItem;
            else Current = prev;
        }

        /// <summary>
        /// Заменяет старые записи в гриде на новые без передёргивания грида
        /// </summary>
        /// <param name="oldItems">Старые записи из грида</param>
        /// <param name="newItems">Новые записи для грида</param>
        protected void UpdateRecords(IEnumerable<T> oldItems, IEnumerable<T> newItems)
        {
            var items = oldItems as IList<T> ?? oldItems.ToList();
            foreach (var newItem in newItems)
            {
                if (newItem == null || (newItem is IMarkedAsDeleted && (newItem as IMarkedAsDeleted).IsDeleted()))
                    continue;
                if (newItem is IIdentifiable)
                {
                    var old = items.FirstOrDefault(d => ((IIdentifiable)d).ID == ((IIdentifiable)newItem).ID);
                    if (old != null)
                    {
                        var index = List.IndexOf(old);
                        List.Insert(index, newItem);
                    }
                    else
                        List.Add(newItem);
                }
                else
                    List.Add(newItem);
            }
            foreach (var oldItem in items)
            {
                if (oldItem != null)
                    List.Remove(oldItem);
            }
        }
    }


    public abstract class ViewModelList<T> : ViewModelList 
    //where T : BaseDataObject
    {
        public T Current
        {
            get { return _current; }
            set
            {
                OnPropertyChanged("CurrentChanging");
                if (SetPropertyValue(nameof(Current), ref _current, value))
                {
                    OnPropertyChanged(nameof(Current));
                    OnPropertyChanged("Current");
                    OnPropertyChanged("CurrentPosition");
                }
            }
        }
        public int CurrentPosition => List.IndexOf(Current) + 1;

        protected ViewModelList(object param = null) : base(param)
        {
            PropertyChanged += ViewModelList_PropertyChanged;
        }

        private void ViewModelList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            if (IsSaveOnCurrentChanged && e.PropertyName.Equals("CurrentChanging"))
                SaveCurrent();
            if (e.PropertyName.Equals("CollectionChanged"))
                Console.WriteLine("Current setted");

        }

        private IList<T> _list = new ObservableCollection<T>();
        private T _current;

        public IList<T> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            protected set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        public void SetFirstAsCurrent()
        {
            if (_list.Count > 0)
                Current = _list.First();
        }

        public void SetCustomAsCurrent(Func<T> takeElementAction)
        {
            if (takeElementAction != null)
                Current = takeElementAction();
        }
        public bool IsSaveOnCurrentChanged { get; set; }

       // private volatile object _isSaving = new object();

        public bool SaveCurrent()
        {
            if (Current == null)
                return false;

           //Task.Factory.StartNew(() =>
          //  {
            //    lock (_isSaving)
           //     {
                    long? newId = null;
                    if (typeof(T).IsSubclassOf(typeof(BaseDataObject)) && (Current as BaseDataObject).Validate())
                        newId = DataContainerFacade.Save(Current);

                    var cur = (Current as IIdentifiable);
                    if (newId.HasValue
                        && cur != null
                        && cur.ID != newId.Value)
                        ((IIdentifiable) Current).ID = newId.Value;
            //    }
          //  });
            
            return true;
        }

    }


    public abstract class ViewModelList : ViewModelBase
    {
        public bool AllowBusyIndicator { get; set; }
        public event EventHandler OnDataRefreshing;
        protected void RaiseDataRefreshing()
        {
            OnDataRefreshing?.Invoke(this, EventArgs.Empty);
        }

        public ICommand RefreshList { get; protected set; }
        protected abstract void ExecuteRefreshList(object param);
        protected abstract bool CanExecuteRefreshList();

        public void ExecuteRefreshListExternal()
        {
            RefreshList?.Execute(null);
        }

        public object RefreshParameter
        {
            get;
            protected set;
        }

        protected ViewModelList(object param = null)
        {
            RefreshParameter = param;

            RefreshList = new DelegateCommand(o => CanExecuteRefreshList(),
                o => ExecuteRefreshListSafe(RefreshParameter));

            RefreshList.Execute(null);
            IsInitialized = true;
        }

        /// <summary>
        /// Обновление списка с обработкой исключений 
        /// </summary>
        private void ExecuteRefreshListSafe(object param)
        {
            try
            {
                if (AllowBusyIndicator)
                {
                    if (string.IsNullOrWhiteSpace(Header))
                        BusyHelper.Start();
                    else
                        BusyHelper.Start("Обновление списка");
                }
                ExecuteRefreshList(param);
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                if (IsInitialized)
                    RaiseGetDataError();
                else
                    throw new ViewModelInitializeException();
            }
            finally
            {
                if (AllowBusyIndicator)
                {
                    BusyHelper.Stop();
                }
            }
        }

        protected override void PostOpenInternal()
        {
            base.PostOpenInternal();
            if (!DontLogOpenEvent && Header != null && !(this is IVmNoDefaultLogging))
                JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.SELECT);
        }

        protected override void ExecuteDelete(int delType)
        {
            if (this is IUpdateRaisingModel)
                return;
            base.ExecuteDelete(delType);
            RefreshList.Execute(null);
        }
    }

    /// <summary>
    /// Редактируемый список для GridNavigator + GridControl
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class EditListViewModel<T> : ViewModelListObservable<T>
    {
        public ICommand InsertCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }
        public ICommand RefreshCommand { get; private set; }
       
        public EditListViewModel()
        {
            RemoveCommand = new DelegateCommand(x => Current != null, x =>
            {
                if (Current == null)
                    return;
                if (typeof(T).IsSubclassOf(typeof(BaseDataObject)))
                    DataContainerFacade.Delete(Current);

                List.Remove(Current);
            });
            InsertCommand = new DelegateCommand(x => true, x =>
            {
                var newItm = (T) Activator.CreateInstance(typeof(T));// ReflectHelper.GetDefaultConstructor(typeof (T)).Invoke(null);
                 List.Add(newItm);
                Current = newItm;
            });

            RefreshCommand = new DelegateCommand(x => true, x => Refresh());
            
        }

        
    }
}
