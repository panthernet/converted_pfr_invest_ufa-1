﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsPrint;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class SpnDepositWizardViewModel : ViewModelCardDialog, IWizardExModel
    {
        public int Step { get; set; }
        public bool IsClosed { get; set; }

        public string PrintHeader
        {
            get
            {
                switch (Step)
                {
                    case 3:
                        return "Печать документов для заседания Комиссии";
                    case 5:
                        return "Экспорт реестров от биржи в Excel";
                    case 8:
                        return "Экспорт реестра заявок, подлежащих удовлетворению";
                    default:
                        return "";
                }
            }
        }

        public SpnDepositWizardViewModel(long id)
        {
            IsOnlyOneInstanceAllowed = true;
            IsNotSavable = true;
            _auction = id < 1 ? new DepClaimSelectParams() : DataContainerFacade.GetByID<DepClaimSelectParams>(id);
            Stocks = DataContainerFacade.GetList<Stock>();
            _auction.PropertyChanged += (sender, e) =>
            {
                if (
                e.PropertyName == nameof(Period) ||
                e.PropertyName == nameof(PlacementDate))
                {
                    try
                    {
                        ReturnDate = PlacementDate.AddDays(Period);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        ReturnDate = null;
                    }
                }

                if (e.PropertyName == nameof(SelectDate))
                    // если номер аукциона не был изменен вручную, то выбираем новый номер при смене года, иначе контроль номера аукциона на совести пользователя
                    if (_previousSelectYear != SelectDate.Year && AuctionNum == _previousAuctionNum)
                    {
                        AuctionNum = BLServiceSystem.Client.GetMaxAuctionNumDepClaimSelectParams(SelectDate.Year) + 1;
                        _previousSelectYear = SelectDate.Year;
                        _previousAuctionNum = AuctionNum;
                    }
            };

            if (id > 0)
            {
                if (_auction.Step == 0)
                {
                    ID = id;
                    switch (_auction.AuctionStatusID)
                    {
                        case (int) DepClaimSelectParams.Statuses.New:
                            Step = 3;
                            break;
                        case (int) DepClaimSelectParams.Statuses.DepClaimImported:
                            if (IsMoscowStock)
                            {
                                if (_auction.DepClaimMaxCount > 0 && _auction.DepClaimCommonCount > 0)
                                    Step = 5;
                                else if (_auction.DepClaimMaxCount == 0 && _auction.DepClaimCommonCount >= 0 || _auction.DepClaimMaxCount > 0 && _auction.DepClaimCommonCount == 0)
                                    Step = 4;
                            }
                            break;
                        case (int) DepClaimSelectParams.Statuses.DepClaimConfirmImported:
                            Step = 8;
                            break;
                    }
                }
                else Step = _auction.Step;

                _previousSelectYear = SelectDate.Year;
                _previousAuctionNum = AuctionNum;
                var loc = _auction.DepClaimSelectLocation;
                // срабатывает автоматическое выставление наименования биржи в поле проведения аукциона
                SelectedStock = Stocks.FirstOrDefault(x => x.ID == _auction.StockId);
                _auction.DepClaimSelectLocation = loc;


                OnPropertyChanged(nameof(Auction));
                OnPropertyChanged(nameof(AuctionNum));
                OnPropertyChanged(nameof(MaxDepClaimAmount));
                OnPropertyChanged(nameof(Period));
                OnPropertyChanged(nameof(MaxInsuranceFee));
                OnPropertyChanged(nameof(MinDepClaimVolume));
                OnPropertyChanged(nameof(MinRate));
                OnPropertyChanged(nameof(PlacementDate));
                OnPropertyChanged(nameof(ReturnDate));
                OnPropertyChanged(nameof(SelectDate));
                OnPropertyChanged(nameof(DepClaimAcceptEnd));
                OnPropertyChanged(nameof(DepClaimAcceptStart));
                OnPropertyChanged(nameof(DepClaimListEnd));
                OnPropertyChanged(nameof(DepClaimListStart));
                OnPropertyChanged(nameof(FilterEnd));
                OnPropertyChanged(nameof(FilterStart));
                OnPropertyChanged(nameof(DepClaimSelectLocation));
                OnPropertyChanged(nameof(OfferReceiveEnd));
                OnPropertyChanged(nameof(OfferReceiveStart));
                OnPropertyChanged(nameof(OfferSendEnd));
                OnPropertyChanged(nameof(OfferSendStart));
                OnPropertyChanged(nameof(IsReadOnly));
            }
        }

        public bool FinalAction()
        {
            try
            {
                _auction.Step = 0;
                new DepClaimOfferCreateViewModel(Auction.ID, false).CreateOffers();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool OnCancel()
        {
            switch (Step)
            {
                case 1:
                case 2:
                    var dlgResult = true;
                    if (CanExecuteStep(1) && CanExecuteStep(2))
                    {
                        var res = DialogHelper.ShowYesNoCancelMessage("Сохранить изменения по текущему этапу и выйти?");
                        //сохраняем аукцион на выходе
                        if (res == true)
                            OnMoveNextCreateDepClaimParams2();
                        dlgResult = res != null;
                    }
                    return dlgResult;
            }
            return true;
        }

        public bool OnMoveNext()
        {
            switch (Step)
            {
                case 2:
                    //сохраняем аукцион на переходе далее
                    return OnMoveNextCreateDepClaimParams2();
                case 3:
                    return OnMoveNextPrintDocuments();
                case 5:
                    return OnMoveNextExportToExcel();
                case 6:
                    return OnMoveNextExportCutoffRate();
                case 8:
                    return OnMoveNextExportConfirmToExcel();
            }
            return true;
        }




        public bool IsMoscowStock => _auction.StockId.Equals((int) Stock.StockID.MoscowStock);

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {

            switch (Step)
            {
                case 2:
                    OnShowCreateDepClaim2();
                    return;
                case 4:
                    return;
            }
        }

        public void OnInitialization()
        {
            _auction.Step = _auction.Step > Step ? _auction.Step : Step;
            if(_auction.ID > 0)
                BLServiceSystem.Client.UpdateAuctionWizardStep(_auction.ID, _auction.Step);
            switch (Step)
            {
                case 1:
                    OnInitializationDepClaimCreate();
                    return;
            }
        }

        public bool CanExecuteNext()
        {
            return CanExecute();
        }

        public bool CanExecutePrev()
        {
            switch (Step)
            {
                case 1:
                case 5:
                case 8:
                    return false;
                default:
                    return true;
            }
        }

        #region Validation

        public override string this[string columnName]
        {
            get
            {
                const string error = "Неверный диапазон времени";
                switch (Step)
                {
                    case 1:
                        switch (columnName)
                        {
                            case nameof(Period):
                                if (Period > 99999)
                                    return "Недопустимо большое значение";
                                break;
                            case nameof(MinDepClaimVolume):
                                if (MinDepClaimVolume > MoneyRestrict)
                                    return "Недопустимо большое значение";
                                break;
                            case nameof(MaxDepClaimAmount):
                                if (MaxDepClaimAmount > 99999999)
                                    return "Недопустимо большое значение";
                                break;
                            case nameof(MinRate):
                                if (MinRate > 100)
                                    return "Значение  должно быть не больше 100 (%)";
                                break;
                            case nameof(MaxInsuranceFee):
                                if (MaxInsuranceFee > MoneyRestrict)
                                    return "Недопустимо большое значение";
                                break;
                            case nameof(SelectedStock):
                                if (SelectedStock == null)
                                    return "Поле обязательное для заполнения";
                                break;
                            case nameof(PlacementDate):
                                if (PlacementDate.Year < 1753)
                                    return "Необходимо ввести корректное значение даты";
                                break;
                            case nameof(ReturnDate):
                                if (ReturnDate == null || ReturnDate.Value.Year < 1753)
                                    return "Необходимо ввести корректные значения даты и срока размещения";
                                break;
                        }
                        return null;
                    case 2:
                        switch (columnName)
                        {
                            case nameof(DepClaimAcceptStart):
                                if (DepClaimAcceptStart > DepClaimAcceptEnd) return error;

                                if (DepClaimAcceptStart > DepClaimListStart) return error;
                                break;
                            case nameof(DepClaimAcceptEnd):
                                if (DepClaimAcceptStart > DepClaimAcceptEnd) return error;
                                break;

                            case nameof(DepClaimListStart):
                                if (DepClaimListStart > DepClaimListEnd) return error;

                                if (DepClaimListStart > FilterStart) return error;
                                if (DepClaimAcceptStart > DepClaimListStart) return error;
                                break;
                            case nameof(DepClaimListEnd):
                                if (DepClaimListStart > DepClaimListEnd) return error;
                                break;
                            case nameof(FilterStart):
                                if (FilterStart > FilterEnd) return error;

                                if (FilterStart > OfferSendStart) return error;
                                if (DepClaimListStart > FilterStart) return error;
                                break;
                            case nameof(FilterEnd):
                                if (FilterStart > FilterEnd) return error;
                                break;
                            case nameof(OfferSendStart):
                                if (OfferSendStart > OfferSendEnd) return error;

                                if (OfferSendStart > OfferReceiveStart) return error;
                                if (FilterStart > OfferSendStart) return error;
                                break;
                            case nameof(OfferSendEnd):
                                if (OfferSendStart > OfferSendEnd) return error;
                                break;
                            case nameof(OfferReceiveStart):
                                if (OfferReceiveStart > OfferReceiveEnd) return error;

                                if (OfferSendStart > OfferReceiveStart) return error;
                                break;
                            case nameof(OfferReceiveEnd):
                                if (OfferReceiveStart > OfferReceiveEnd) return error;
                                break;
                            case nameof(DepClaimSelectLocation):
                                if (string.IsNullOrEmpty(DepClaimSelectLocation)) return "Поле обязательное для заполнения";
                                break;
                        }
                        return null;
                    case 3:
                    case 5:
                    case 8:
                        if (columnName.Equals(nameof(FilePath)) && string.IsNullOrEmpty(FilePath)) return "Выберите директорию для сохранения";                        
                        return null;
                    case 6:
                        if (!IsMoscowStock && columnName.Equals(nameof(FilePath)) && string.IsNullOrEmpty(FilePath)) return "Выберите директорию для сохранения";
                        return columnName.Equals(nameof(CutoffRate)) ?CutoffRate.ValidateRange(0.01m, 100.0m) : null;
                    default:
                        return null;
                }
            }
        }

        private bool CanExecute()
        {
            switch (Step)
            {
                case 1:
                    if (!IsMoscowStock) return false;
                    return ValidateFields("Period|MaxDepClaimAmount|MinDepClaimVolume|MinRate|MaxInsuranceFee|SelectDate|SelectedStock|ReturnDate");
                case 2:
                    return ValidateFields("DepClaimAcceptStart|DepClaimAcceptEnd|DepClaimListStart|DepClaimListEnd|FilterStart|FilterEnd|OfferSendStart|OfferSendEnd|OfferReceiveStart|OfferReceiveEnd|DepClaimSelectLocation");
                case 3:
                case 5:
                case 8:
                    return ValidateFields($"{nameof(FilePath)}");
                case 6:
                    return ValidateFields($"{nameof(CutoffRate)}|{nameof(FilePath)}");
                case 9:
                    return true;
                default:
                    return false;
            }
        }

        private bool CanExecuteStep(int step)
        {
            var tmp = Step;
            try
            {
                Step = step;
                return CanExecute();
            }
            finally
            {
                Step = tmp;
            }
        }

        #endregion

        #region CreateDepClaimParams
        private static readonly decimal MoneyRestrict = new decimal(9999999999999999.99);
        public new bool IsReadOnly => Auction.AuctionStatus != DepClaimSelectParams.Statuses.New;

        public DateTime PlacementDate
        {
            get { return _auction.PlacementDate; }
            set
            {
                if (_auction.PlacementDate != value)
                {
                    _auction.PlacementDate = value;
                    OnPropertyChanged(nameof(PlacementDate));
                }
            }
        }

        public DateTime? ReturnDate
        {
            get { return _auction.ReturnDate == DateTime.MinValue ? (DateTime?)null : _auction.ReturnDate; }
            set
            {
                if (_auction.ReturnDate != value)
                {
                    _auction.ReturnDate = value ?? DateTime.MinValue;
                    OnPropertyChanged(nameof(ReturnDate));
                }
            }
        }

        private Stock _mSelectedStock;
        public Stock SelectedStock
        {
            get
            {
                return _mSelectedStock;
            }
            set
            {
                if (_mSelectedStock != value)
                {
                    _mSelectedStock = value;
                    if (value != null)
                    {
                        _auction.StockId = _mSelectedStock.ID;

                    }
                    OnPropertyChanged(nameof(SelectedStock));
                }
            }
        }

        public decimal MaxInsuranceFee
        {
            get { return _auction.MaxInsuranceFee; }
            set
            {
                if (_auction.MaxInsuranceFee != value)
                {
                    _auction.MaxInsuranceFee = value;
                    OnPropertyChanged(nameof(MaxInsuranceFee));
                }
            }
        }

        public decimal MinRate
        {
            get { return _auction.MinRate; }
            set
            {
                if (_auction.MinRate != value)
                {
                    _auction.MinRate = value;
                    OnPropertyChanged(nameof(MinRate));
                }
            }
        }

        public int MaxDepClaimAmount
        {
            get { return _auction.MaxDepClaimAmount; }
            set
            {
                if (_auction.MaxDepClaimAmount != value)
                {
                    _auction.MaxDepClaimAmount = value;
                    OnPropertyChanged(nameof(MaxDepClaimAmount));
                }
            }
        }

        private DepClaimSelectParams _auction;
        public DepClaimSelectParams Auction
        {
            get { return _auction; }
            set
            {
                _auction = value;
                OnPropertyChanged(nameof(Auction));
            }
        }

        public int Period
        {
            get { return _auction.Period; }
            set
            {
                if (_auction.Period != value)
                {
                    _auction.Period = value;
                    OnPropertyChanged(nameof(Period));
                }
            }
        }

        public decimal MinDepClaimVolume
        {
            get { return _auction.MinDepClaimVolume; }
            set
            {
                if (_auction.MinDepClaimVolume != value)
                {
                    _auction.MinDepClaimVolume = value;
                    OnPropertyChanged(nameof(MinDepClaimVolume));
                }
            }
        }

        public DateTime SelectDate
        {
            get { return _auction.SelectDate; }
            set
            {
                if (_auction.SelectDate != value)
                {
                    _auction.SelectDate = value;

                    OnPropertyChanged(nameof(SelectDate));
                    UpdateLocalNum();
                }
            }
        }

        List<Stock> _mStocks;
        public List<Stock> Stocks
        {
            get
            {
                return _mStocks;
            }
            set
            {
                _mStocks = value;
                OnPropertyChanged(nameof(Stocks));
            }
        }

        public int AuctionNum
        {
            get { return _auction.AuctionNum; }
            set
            {
                if (_auction.AuctionNum != value)
                {
                    _auction.AuctionNum = value;
                    OnPropertyChanged(nameof(AuctionNum));
                }
            }
        }

        public int LocalNum
        {
            get { return _auction.LocalNum; }
            set
            {
                if (_auction.LocalNum != value)
                {
                    _auction.LocalNum = value;
                    OnPropertyChanged(nameof(LocalNum));
                }
            }
        }

        private int _previousSelectYear;
        private int _previousAuctionNum;

        private void OnInitializationDepClaimCreate()
        {
            if (ID < 1)
            {
                SelectDate = DateTime.Now;
                PlacementDate = DateTime.Now;
                ReturnDate = DateTime.Now;
                SelectedStock = Stocks.FirstOrDefault(x => x.ID == (int) Stock.StockID.MoscowStock);
                AuctionNum = BLServiceSystem.Client.GetMaxAuctionNumDepClaimSelectParams(SelectDate.Year) + 1;
                Period = 0;
                _previousAuctionNum = BLServiceSystem.Client.GetMaxAuctionNumDepClaimSelectParams(SelectDate.Year) + 1;
                _previousSelectYear = SelectDate.Year;
                UpdateLocalNum();
            }
        }

        private void UpdateLocalNum()
        {
            //При создании всегда обновляем номер
            LocalNum = BLServiceSystem.Client.GetAuctionNextLocalNum(SelectDate);
        }

        #endregion

        #region CreateDepClaimParams 2

        public TimeSpan DepClaimAcceptEnd
        {
            get { return _auction.DepClaimAcceptEnd; }
            set
            {
                if (_auction.DepClaimAcceptEnd != value)
                {
                    _auction.DepClaimAcceptEnd = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan DepClaimAcceptStart
        {
            get { return _auction.DepClaimAcceptStart; }
            set
            {
                if (_auction.DepClaimAcceptStart != value)
                {
                    _auction.DepClaimAcceptStart = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan DepClaimListEnd
        {
            get { return _auction.DepClaimListEnd; }
            set
            {
                if (_auction.DepClaimListEnd != value)
                {
                    _auction.DepClaimListEnd = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan DepClaimListStart
        {
            get { return _auction.DepClaimListStart; }
            set
            {
                if (_auction.DepClaimListStart != value)
                {
                    _auction.DepClaimListStart = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan FilterEnd
        {
            get { return _auction.FilterEnd; }
            set
            {
                if (_auction.FilterEnd != value)
                {
                    _auction.FilterEnd = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan FilterStart
        {
            get { return _auction.FilterStart; }
            set
            {
                if (_auction.FilterStart != value)
                {
                    _auction.FilterStart = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public string DepClaimSelectLocation
        {
            get { return _auction.DepClaimSelectLocation; }
            set
            {
                if (_auction.DepClaimSelectLocation != value)
                {
                    _auction.DepClaimSelectLocation = value;
                    OnPropertyChanged(nameof(DepClaimSelectLocation));
                }
            }
        }

        public TimeSpan OfferReceiveEnd
        {
            get { return _auction.OfferReceiveEnd; }
            set
            {
                if (_auction.OfferReceiveEnd != value)
                {
                    _auction.OfferReceiveEnd = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan OfferReceiveStart
        {
            get { return _auction.OfferReceiveStart; }
            set
            {
                if (_auction.OfferReceiveStart != value)
                {
                    _auction.OfferReceiveStart = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan OfferSendEnd
        {
            get { return _auction.OfferSendEnd; }
            set
            {
                if (_auction.OfferSendEnd != value)
                {
                    _auction.OfferSendEnd = value;
                    UpdatePeriodValidation();
                }
            }
        }

        public TimeSpan OfferSendStart
        {
            get { return _auction.OfferSendStart; }
            set
            {
                if (_auction.OfferSendStart != value)
                {
                    _auction.OfferSendStart = value;
                    UpdatePeriodValidation();
                }
            }
        }

        private void UpdatePeriodValidation()
        {
            OnPropertyChanged(nameof(DepClaimAcceptEnd));
            OnPropertyChanged(nameof(DepClaimAcceptStart));
            OnPropertyChanged(nameof(DepClaimListEnd));
            OnPropertyChanged(nameof(DepClaimListStart));
            OnPropertyChanged(nameof(FilterEnd));
            OnPropertyChanged(nameof(FilterStart));
            OnPropertyChanged(nameof(OfferReceiveEnd));
            OnPropertyChanged(nameof(OfferReceiveStart));
            OnPropertyChanged(nameof(OfferSendEnd));
            OnPropertyChanged(nameof(OfferSendStart));
        }

        private void LoadDepClaimTimes()
        {
            var lastDepClaim = BLServiceSystem.Client.SelectLastDepClaimSelectParams(SelectedStock.ID);
            if (lastDepClaim == null)
            {
                DepClaimAcceptStart = new TimeSpan(0, 10, 30, 0);
                DepClaimAcceptEnd = new TimeSpan(0, 11, 00, 0);
                DepClaimListStart = DepClaimAcceptEnd;
                DepClaimListEnd = new TimeSpan(0, 11, 30, 0);
                FilterStart = DepClaimListEnd;
                FilterEnd = new TimeSpan(0, 12, 00, 0);
                OfferSendStart = FilterEnd;
                OfferSendEnd = new TimeSpan(0, 13, 30, 0);
                OfferReceiveStart = new TimeSpan(0, 12, 00, 0);
                OfferReceiveEnd = new TimeSpan(0, 14, 30, 0);
            }
            else
            {
                DepClaimAcceptStart = lastDepClaim.DepClaimAcceptStart;
                DepClaimAcceptEnd = lastDepClaim.DepClaimAcceptEnd;
                DepClaimListStart = lastDepClaim.DepClaimListStart;
                DepClaimListEnd = lastDepClaim.DepClaimListEnd;
                FilterStart = lastDepClaim.FilterStart;
                FilterEnd = lastDepClaim.FilterEnd;
                OfferSendStart = lastDepClaim.OfferSendStart;
                OfferSendEnd = lastDepClaim.OfferSendEnd;
                OfferReceiveStart = lastDepClaim.OfferReceiveStart;
                OfferReceiveEnd = lastDepClaim.OfferReceiveEnd;
            }
        }

        private void OnShowCreateDepClaim2()
        {
            //тянется с 1 формы
            DepClaimSelectLocation = _mSelectedStock.Name;
            LoadDepClaimTimes();
        }

        private bool OnMoveNextCreateDepClaimParams2()
        {
            try
            {
                UpdateLocalNum();
                ReturnDate = PlacementDate.AddDays(Period);
                _auction.StockName = _mSelectedStock.Name;
                ID = Auction.ID = DataContainerFacade.Save(_auction);
                if (ID > 0)
                {
                    _previousAuctionNum = AuctionNum;
                    _previousSelectYear = SelectDate.Year;
                }

                ViewModelManager.UpdateDataInAllModels(new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(DepClaimSelectParams), ID)});

                return true;
            }
            catch(Exception ex)
            {
                Logger.WriteException(ex, "Spn depo wizard step next " + Step);
                DialogHelper.ShowError("Во время операции произошла ошибка! Детальное сообщение смотрите в логе программы.");
                return false;
            }
        }


        #endregion

        #region PrintDocuments

        private DateTime _docDate = DateTime.Today;
        public DateTime DocDate
        {
            get { return _docDate; }
            set { _docDate = value; OnPropertyChanged(nameof(DocDate));}
        }

        private Person _authorizedPerson;
        public Person AuthorizedPerson
        {
            get { return _authorizedPerson; }
            set { _authorizedPerson = value; OnPropertyChanged(nameof(AuthorizedPerson)); }
        }

        private Person _performer;
        public Person Performer
        {
            get { return _performer; }
            set { _performer = value; OnPropertyChanged(nameof(Performer)); }
        }

        private Person _deputy;
        public Person Deputy
        {
            get { return _deputy; }
            set { _deputy = value; OnPropertyChanged(nameof(Deputy)); }
        }

        private string _filePath;

        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; OnPropertyChanged(nameof(FilePath));}
        }

        private bool OnMoveNextPrintDocuments()
        {
            try
            {
                //уведомление о параметрах отбора заявок
                if (IsMoscowStock)
                {
                    var p = new PrintDepClaimSelectParams(_auction.ID, false) {IsVisibleAfterGeneration = false};
                    p.Print(_filePath);
                }
                else
                {
                    var p = new PrintDepClaimSelectParams(_auction.ID, false) {IsVisibleAfterGeneration = false};
                    p.Print(_filePath);
                    var document = BLServiceSystem.Client.ExportAuctionInfo(_auction.ID);
                    DepClaimHelper.SaveXML(document, _filePath);
                }
                //Заключение о результатах проверки
                BanksVerificationViewModel.ExecuteAction(_auction, true, _docDate, _authorizedPerson, _deputy, _performer, _filePath, false);
                //Сформировать лимиты
                BanksVerificationViewModel.ExecuteAction(_auction, false, _docDate, _authorizedPerson, _deputy, _performer, _filePath, false);
                if (!IsMoscowStock)
                {
                    var xml = DataContainerFacade.ExportBankLimitsInfo(_auction.ID);
                    DepClaimHelper.SaveXML(xml, FilePath);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Spn depo wizard step next " + Step);
                DialogHelper.ShowError("Во время операции произошла ошибка! Детальное сообщение смотрите в логе программы.");
                return false;
            }
        }

        #endregion

        #region ExportDocumentsToExcel


        private bool OnMoveNextExportToExcel()
        {
            try
            {
                var print = new PrintExportDepClaim2(_auction, DocDate, Path.Combine(_filePath ?? "", "Выписка из реестров заявок.xls".AttachUniqueTimeStamp(true)));
                print.Print();
                JournalLogger.LogModelEvent(this, JournalEventType.EXPORT_DATA, "Выписка из реестра заявок");

                var list = BLServiceSystem.Client.GetDepClaimMaxList((int)DepClaimMaxListItem.Types.Common)
                    .Where(l => l.DepclaimselectparamsId == _auction.ID).ToList();
                new PrintExportDepclaimMax(list, (int)DepClaimMaxListItem.Types.Common, DocDate, _filePath).Print();
                JournalLogger.LogModelEvent(this, JournalEventType.EXPORT_DATA, "Сводный реестр заявок");

                list = BLServiceSystem.Client.GetDepClaimMaxList((int) DepClaimMaxListItem.Types.Max)
                    .Where(l => l.DepclaimselectparamsId == _auction.ID).ToList();
                new PrintExportDepclaimMax(list, (int)DepClaimMaxListItem.Types.Max, DocDate, _filePath).Print();
                JournalLogger.LogModelEvent(this, JournalEventType.EXPORT_DATA, "Сводный реестр заявок с учетом макс суммы");

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Spn depo wizard step next " + Step);
                DialogHelper.ShowError("Во время операции произошла ошибка! Детальное сообщение смотрите в логе программы.");
                return false;
            }
        }

        #endregion

        #region ExportConfirmDocumentsToExcel


        private bool OnMoveNextExportConfirmToExcel()
        {
            try
            {
                var print = new PrintExportDepClaim2Confirm(_auction, DocDate, _filePath);
                print.Print();
                JournalLogger.LogModelEvent(this, JournalEventType.EXPORT_DATA, "Выписка из реестра заявок подлежащих удовлетворению");
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Spn depo wizard step next " + Step);
                DialogHelper.ShowError("Во время операции произошла ошибка! Детальное сообщение смотрите в логе программы.");
                return false;
            }
        }

        #endregion

        #region ExportCutoffRate
        public decimal CutoffRate
        {
            get { return Auction.CutoffRate; }
            set
            {
                Auction.CutoffRate = value;
                OnPropertyChanged(nameof(CutoffRate));
            }
        }

        private bool OnMoveNextExportCutoffRate()
        {
            try
            {
                DataContainerFacade.Save(Auction);
                if (IsMoscowStock) return true;

                //Генерируем документ
                var document = BLServiceSystem.Client.ExportAuctionCutoffRate(Auction.ID);
                DepClaimHelper.SaveXML(document, _filePath);
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Spn depo wizard step next " + Step);
                DialogHelper.ShowError("Во время операции произошла ошибка! Детальное сообщение смотрите в логе программы.");
                return false;
            }
        }
        #endregion
    }
}
