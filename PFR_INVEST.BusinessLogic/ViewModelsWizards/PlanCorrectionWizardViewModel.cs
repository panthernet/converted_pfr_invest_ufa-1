﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class PlanCorrectionWizardViewModel<TRecord> : ViewModelBase
        where TRecord : PlanContractRecord
    {
        #region Selected Year

        private List<Year> _yearsList;
        public List<Year> YearsList
        {
            get
            {
                return _yearsList;
            }
            set
            {
                _yearsList = value;
                OnPropertyChanged("YearsList");
            }
        }

        private Year _selectedYear;
        public Year SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                _selectedYear = value;
                if (value != null)
                {
                    MonthsList = GetValidMonthList(value.ID);
                }
                OnPropertyChanged("SelectedYear");
            }
        }

        #endregion

        #region Selected Month

        private List<Month> _monthsList;
        public List<Month> MonthsList
        {
            get
            {
                return _monthsList;
            }
            set
            {
                _monthsList = value;
                if (_monthsList != null)
                    SelectedMonth = MonthsList.FirstOrDefault();
                OnPropertyChanged("MonthsList");
            }
        }

        private Month _selectedMonth;
        public Month SelectedMonth
        {
            get
            {
                return _selectedMonth;
            }
            set
            {
                _selectedMonth = value;
                OnPropertyChanged("SelectedMonth");
            }
        }

        #endregion

        public bool IsOpContentVisible => ((SPNOperationTypeItem)SelectedOp).TypeId != (long) Element.SpecialDictionaryItems.PaymentZLType;

        #region Выбор типа операции для планов на год и на месяц

        //ID поддерживаемых типов операций
        //private readonly long[] _availableOpIdList = { 8L, 10L, 11L };

        private void SetOpList(long direction)
        {
            var list = TransferWizardHelper.GetOperationTypesListForYearPlanMasked(direction);

            if (list.Count == 0)
                throw new Exception("Не найдено доступных типов операций для направления ID =" + direction);
            OpList = list;
            OnPropertyChanged("OpList");
            SelectedOp = OpList[0];


        }
        /// <summary>
        /// Список доступных содержаний операций
        /// </summary>
        public List<SPNOperation> OpContentList { get; private set; }
        private SPNOperation _selectedOpContent;
        /// <summary>
        /// Выбранное содержание операции
        /// </summary>
        public SPNOperation SelectedOpContent
        {
            get { return _selectedOpContent; }
            set
            {
                _selectedOpContent = value;
                if (_selectedOpContent != null)
                    OperationContentId = _selectedOpContent.ID;
                OnPropertyChanged("SelectedOpContent");
            }
        }
        /// <summary>
        /// Список достыпных типов операций
        /// </summary>
        public List<SPNOperation> OpList { get; private set; }
        private SPNOperation _selectedOp;
        /// <summary>
        /// Выбранный тип операции
        /// </summary>
        public SPNOperation SelectedOp { get { return _selectedOp; } 
            set 
            { 
                _selectedOp = value;
                if (_selectedOp != null)
                {
                    OperationId = _selectedOp.ID;
                    var listOpContent = _allSpnOperations.Where(op => OperationId == 8 ? op.ID == 8 : op.ID == 10 || op.ID == 11).ToList();
                    if (listOpContent.Count == 0)
                        throw new Exception("Не найдено доступных содержаний операций для направления ID =" + _direction);
                    OpContentList = listOpContent;
                    OnPropertyChanged("OpContentList");
                    SelectedOpContent = OpContentList[0];

                }
                OnPropertyChanged("SelectedOp");
                OnPropertyChanged("SelectedOpContent");
                OnPropertyChanged("IsOpContentVisible");                
            } 
        }

        #endregion

        private long _mLastYearID;
        private long _mLastMonthID;
        /// <summary>
        /// Тип операции (совместим со старой логикой, на случай если используется в дургих мастерах)
        /// </summary>
        protected long OperationId;

        /// <summary>
        /// Тип операции (для новой системы единого типа операции без содержания операции)
        /// </summary>
        protected long? OperationTypeId => (SelectedOp as SPNOperationTypeItem)?.TypeId;

        /// <summary> 
        /// Содержание операции
        /// </summary>
        protected long OperationContentId;

        private readonly long _direction;

        protected PlanCorrectionWizardViewModel(long operationId, long direction = 0)
        {
			ID = operationId;
            _direction = direction;
            _allSpnOperations = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(direction);
            //если задано направление, знаит есть выбор типа операции
            //создаем список операций и пропускаем обновление данных по годам
            if (direction != 0)
                SetOpList(direction);
            else
            {
                OperationId = operationId;
                InitializeDates();
            }
        }

        /// <summary>
        /// Модель содержит валидные данные
        /// </summary>
        /// <param name="checkForOp">Флаг учитывать ли наличие выбора типа операции (true - для пропуска проверки при создании моделей)</param>
        public bool IsModelContainCorrectData(bool checkForOp = true)
        {
            return YearsList != null && YearsList.Any() || checkForOp && SelectedOp != null && SelectedOpContent != null;
        }

        /// <summary>
        /// Инициализируем данные на основе выбранного типа операции
        /// </summary>
        public void InitializeDates()
        {
            try
            {
                YearsList = GetValidYearList();
                SelectedYear = YearsList.FirstOrDefault();
                if (SelectedYear == null)
                {
                    return;
                }

                if(MonthsList == null)
                    MonthsList = DataContainerFacade.GetList<Month>();
                
                SelectedMonth = MonthsList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
            }

            OnPropertyChanged("SelectedMonth");
            OnPropertyChanged("SelectedYear");
        }

        private List<TRecord> _mAvailableContractsList;
        private readonly List<SPNOperation> _allSpnOperations;

        public List<TRecord> AvailableContractsList
        {
            get
            {
                return _mAvailableContractsList;
            }
            set
            {
                _mAvailableContractsList = value;
                OnPropertyChanged("AvailableContractsList");
            }
        }


        protected abstract List<Year> GetValidYearList();

        protected abstract List<Month> GetValidMonthList(long yearID);

        public virtual void EnsureAvailableList()
        {
            if (SelectedYear == null || _mLastMonthID == SelectedMonth.ID && _mLastYearID == SelectedYear.ID)
                return;

            var plansList = GetUKPlansForYearMonthAndOperation(SelectedMonth.ID, SelectedYear.ID).Where(l => l.GetTransferList().GetContract().Status >= 0).ToList();

            AvailableContractsList = plansList.ConvertAll(Convert);

            _mLastMonthID = SelectedMonth.ID;
            _mLastYearID = SelectedYear.ID;
        }

        protected abstract TRecord Convert(SIUKPlan ukPlan);

        protected abstract List<SIUKPlan> GetUKPlansForYearMonthAndOperation(long monthID, long yearID);

        public void UpdateUKPlanSums()
        {
            foreach (var cr in AvailableContractsList)
            {
                DataContainerFacade.Save<SIUKPlan, long>(cr.UKPlan);
            }
            JournalLogger.LogModelEvent(this, JournalEventType.UPDATE, "Обновление плана УК", $"Год : {SelectedYear.Name}. Месяц: {SelectedMonth.Name}");
            RefreshRelatedViewModels();
        }

        protected void RefreshRelatedViewModels()
        {
            if (GetViewModel != null)
            {
                foreach (Type viewModelType in GetRelatedViewModelTypesToRefresh())
                {
                    ViewModelList viewModel = GetViewModel(viewModelType) as ViewModelList;
                    if (viewModel != null)
                        viewModel.RefreshList.Execute(null);
                }
            }
        }

        protected abstract IEnumerable<Type> GetRelatedViewModelTypesToRefresh();
    }
}
