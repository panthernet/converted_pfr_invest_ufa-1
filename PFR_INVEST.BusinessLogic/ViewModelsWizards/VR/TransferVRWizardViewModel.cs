﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.VR
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class TransferVRWizardViewModel : TransferWizardViewModel
    {
        
        public override bool IsPayAssVisible => true;

        public TransferVRWizardViewModel(bool forAPSI)
            : base(Document.Types.VR, forAPSI)
        {
        }

      
    }
}
