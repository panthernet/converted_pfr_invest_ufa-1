﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI.RopsAsvWizard;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.VR;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class TransferWizardViewModel : ViewModelBase, IRequestCustomAction
    {
        #region Transfer types lists

        private readonly List<SPNDirection> _transferDirections;
        public List<SPNDirection> TransferDirections => _transferDirections;

        public List<SPNOperation> TransferTypes_PFRtoUK { get; } = new List<SPNOperation>();

        protected List<SPNOperation> TransferTypesUKtoPFR = new List<SPNOperation>();
        public List<SPNOperation> TransferTypes_UKtoPFR => TransferTypesUKtoPFR;

        //Типы операций для исключения из стандартного списка
        protected string[] RopsAsvStringListForMaster => new[] { "Отзыв средств для перечисления в АСВ", "Отзыв средств для перечисления в РОПС" };
        #endregion

        #region Selected Transfer Direction
        private SPNDirection _selectedTransferDirection;
        public SPNDirection SelectedTransferDirection
        {
            get
            {
                return _selectedTransferDirection;
            }
            set
            {
                if (SelectedTransferDirection == value)
                    return;
                _selectedTransferDirection = value;

                if (CurrentTransferTypes != null && CurrentTransferTypes.Count > 0)
                    SelectedTransferType = CurrentTransferTypes[0];
                //else
                //{
                // SelectedTransferType = null;
                // }
                OnPropertyChanged(nameof(SelectedTransferDirection));
                OnPropertyChanged(nameof(CurrentTransferTypes));
                OnPropertyChanged(nameof(SelectedTransferType));
            }
        }
        #endregion

        #region Selected Transfer Type
        public List<SPNOperation> CurrentTransferTypes
        {
            get
            {
                var transfers = SelectedTransferDirection != null
                                        ? (SelectedTransferDirection.ID == 1 || SelectedTransferDirection.ID == 3 ? TransferTypes_UKtoPFR : TransferTypes_PFRtoUK)
                                        : null;
                return transfers;
                //return SelectedTransferDirection != null
                //           ? (SelectedTransferDirection.isFromUKToPFR.Value ? TransferTypes_UKtoPFR : TransferTypes_PFRtoUK)
                //           : null;
            }
        }

        private SPNOperation _selectedTransferType;
        public SPNOperation SelectedTransferType
        {
            get
            {
                return _selectedTransferType;
            }
            set
            {
                if (_selectedTransferType == value)
                    return;
                _selectedTransferType = value;
                _yearsList = GetYearsList();
                if (_yearsList != null)
                    UpdateSelectedYear();
                OnPropertyChanged(nameof(YearsList));
                OnPropertyChanged(nameof(SelectedYear));
                OnPropertyChanged(nameof(SelectedTransferType));
                OnPropertyChanged(nameof(IsPeriodVisible));
                UpdatePaymentAssignment();

            }
        }


        protected string UpdatePaymentAssignment()
        {
            if (!(this is TransferSIWizardViewModel || this is TransferVRWizardViewModel) ||
                SelectedTransferDirection == null || SelectedTransferType == null)
                return PayAssignment = string.Empty;

            var part = _contractType == (int)Document.Types.SI ? (long)Element.SpecialDictionaryItems.AppPartSI : (long)Element.SpecialDictionaryItems.AppPartVR;
            var payAss = BLServiceSystem.Client.GetPayAssForReport(part, Element.SiDirectionToCommonDirection(SelectedTransferDirection), SelectedTransferType.ID);
            return PayAssignment = payAss == null ? string.Empty : PaymentAssignmentHelper.ReplaceTagDataForNPF(payAss.Name, SelectedDate);

        }

        public bool IsPeriodVisible
        {
            get
            {
                switch (SelectedTransferType.ID)
                {
                    default: return false;
                }
            }
        }
        #endregion

        #region Pay Assignment
        private string _payAssignment;
        public string PayAssignment
        {
            get
            {
                return _payAssignment;
            }
            set
            {
                if (_payAssignment == value)
                    return;

                _payAssignment = value;
                OnPropertyChanged("PayAssignment");
            }
        }
        /// <summary>
        /// Видимость назначения платежа (override в дочерних моделях)
        /// </summary>
        public virtual bool IsPayAssVisible => false;

        #endregion

        #region Selected Year

        private List<Year> _yearsList;
        public List<Year> YearsList
        {
            get { return _yearsList; }
            set
            {
                _yearsList = value;
                OnPropertyChanged("YearsList");
            }
        }
        private readonly List<Year> _yearsListAll;

        private Year _selectedYear;
        public Year SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                _selectedYear = value;
                OnPropertyChanged("SelectedYear");
            }
        }

        #endregion

        #region Selected Month

        private List<Month> monthsList;
        public List<Month> MonthsList
        {
            get { return monthsList; }
            set { monthsList = value; OnPropertyChanged("MonthsList"); }
        }

        private Month _selectedMonth;
        public Month SelectedMonth
        {
            get
            {
                return _selectedMonth;
            }
            set
            {
                _selectedMonth = value;
                OnPropertyChanged("SelectedMonth");
            }
        }

        #endregion

        #region Selected Date
        private DateTime _selectedDate = DateTime.Now;
        public DateTime SelectedDate
        {
            get
            {
                return _selectedDate;
            }
            set { SetPropertyValue(nameof(SelectedDate), ref _selectedDate, value); }
        }
        #endregion

        #region Comments
        private string _comments = string.Empty;
        public string Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                _comments = value;
                OnPropertyChanged("Comments");
            }
        }
        #endregion

        private SPNOperation _mAPSIOperation;
        SPNOperation APSIOperation => _mAPSIOperation ?? (_mAPSIOperation = BLServiceSystem.Client.GetAPSIOperationHib());

        /// <summary>
        /// Доступно ли поле выбора Типа операции
        /// </summary>
        public virtual bool IsOperationTypeEnabled => CreateSITransfers;

        public bool CreateSITransfers { get; }

        protected readonly int _contractType;
        public int ContractType => _contractType;

        private const string OPER_FIN = "Финансирование выплат";
        private const string OPER_VYPL = "Выплаты правопреемникам";
        //private string dirGukPfr = "Из ГУК ВР в ПФР";

        public bool IsFinVypl => SelectedTransferType != null &&
                                 (
                                     string.Equals(SelectedTransferType.Name, OPER_FIN, StringComparison.OrdinalIgnoreCase) ||
                                     string.Equals(SelectedTransferType.Name, OPER_VYPL, StringComparison.OrdinalIgnoreCase)
                                 );

        public virtual bool IsImportAvailable => _contractType == (int)Document.Types.SI;
        public ICommand ImportCommand { get; set; }
        public List<SIImportListItem> ImportList { get; protected set; } = new List<SIImportListItem>();

        public TransferWizardViewModel(Document.Types contractType, bool forAPSI)
        {
            AllContractsList = new List<ModelContractRecord>();
            CreateSITransfers = !forAPSI;
            _contractType = (int)contractType;
            MonthsList = DataContainerFacade.GetList<Month>();
            //MonthsList.Insert(0, new Month() { Name = TransferRegisterCompanyMonthIdentifier.NoMonth }); - я непонял, а зачем это нужно, это первоначальная заливка.
            SelectedMonth = MonthsList.Find(
                pM => pM.ID == DateTime.Today.Month
            );

            ImportCommand = new DelegateCommand(a =>
            {
                ImportList = DialogHelper.ShowImportSIWizardDialog((SIRegister.Directions)(int)SelectedTransferDirection.ID)
                    .Where(b => !b.IsMismatched).ToList();
                if (!ImportList.Any()) return;
                var idList = new List<long>();
                ImportList.ToList().ForEach(bResult =>
                {
                    var name = bResult.UK.Trim().ToLower();
                    var cName = bResult.RegNum.Trim().ToLower();
                    var item = AvailableContractsList.FirstOrDefault(ci => ci.UkFormalizedName.ToLower() == name && ci.ContractName.ToLower() == cName);
                    if (item != null)
                        idList.Add(item.ContractID);
                    else
                        ImportList.Remove(bResult);
                });
                OnRequestCustomAction(idList);
            });

            _transferDirections = DataContainerFacade.GetList<SPNDirection>();

            switch (contractType)
            {
                case Document.Types.SI:
                    _transferDirections = _transferDirections.Where(trd => trd.ID == 1 || trd.ID == 2).ToList();
                    break;
                case Document.Types.VR:
                    _transferDirections = _transferDirections.Where(trd => trd.ID == 3 || trd.ID == 4).ToList();
                    break;
            }

            SelectedTransferDirection = TransferDirections[0];
            _yearsListAll = DataContainerFacade.GetList<Year>();

            if (CreateSITransfers)
            {
                //исключаем выплаты правопреемников, Отзыв средств на финансирование НЧТП, Отзыв средств на финансирование СПВ
                long[] excludedIdListForMaster = { APSIOperation.ID, 10L, 11L };


                //мастер перечислений - выбираем все операции кроме правопреемников
                TransferTypesUKtoPFR = new List<SPNOperation>(BLServiceSystem.Client.GetSPNOperationsUKtoPFRListHib().Where(op => !excludedIdListForMaster.Contains(op.ID)));
                //transferTypes_PFRtoUK = BLServiceSystem.Client.GetSPNOperationsPFRtoUKListHib();

                switch (contractType)
                {
                    case Document.Types.SI:
                        TransferTypesUKtoPFR = new List<SPNOperation>(BLServiceSystem.Client.GetSPNOperationsByTypeListHib(1).Where(op => !excludedIdListForMaster.Contains(op.ID)).OrderBy(a => a.Name));
                        TransferTypes_PFRtoUK = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(2).OrderBy(a => a.Name).ToList();
                        break;
                    case Document.Types.VR:
                        TransferTypesUKtoPFR = new List<SPNOperation>(BLServiceSystem.Client.GetSPNOperationsByTypeListHib(3).Where(op => !excludedIdListForMaster.Contains(op.ID)).OrderBy(a => a.Name));
                        TransferTypes_PFRtoUK = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(4).OrderBy(a => a.Name).ToList();
                        break;
                }

                //Если класс не для перечислений РОПС, АСВ то убрать эти типы перечислений из списка.
                if (!(this is TransferSIRopsAsvWizardViewModel))
                {
                    TransferTypesUKtoPFR =
                        TransferTypesUKtoPFR.Where(
                            t => !RopsAsvStringListForMaster.Contains(t.Name, StringComparer.CurrentCultureIgnoreCase))
                            .ToList();
                }
                else //Мастер перечислений РОПС и АСВ содержит только 2 типа операций
                {
                    TransferTypesUKtoPFR =
                        TransferTypesUKtoPFR.Where(
                            t => RopsAsvStringListForMaster.Contains(t.Name, StringComparer.CurrentCultureIgnoreCase))
                            .ToList();
                }

                SelectedTransferType = TransferTypes_UKtoPFR[0];

            }
            else SetVkTranfderType();

            OnPropertyChanged("SelectedTransferDirection");
            OnPropertyChanged("CurrentTransferTypes");
            OnPropertyChanged("SelectedTransferType");
            OnPropertyChanged("SelectedMonth");
            OnPropertyChanged("SelectedYear");

            UpdatePaymentAssignment();
        }


        private void UpdateSelectedYear()
        {
            if (YearsList.Count > 1)
            {
                Year result;
                int ycounter = 1;

                while (true)
                {
                    var startYear = (DateTime.Today.Year + ycounter).ToString();
                    result = YearsList.FirstOrDefault(a => a.Name == startYear);
                    if (result == null) ycounter++;
                    if (result != null || ycounter > 50) break;
                }
                SelectedYear = result ?? YearsList.FirstOrDefault();
            }
            else SelectedYear = YearsList.FirstOrDefault();
        }

        protected virtual void SetVkTranfderType()
        {
            //мастер годового плана - только правопреемники
            TransferTypesUKtoPFR = new List<SPNOperation> { APSIOperation };
            SelectedTransferType = TransferTypes_UKtoPFR[0];
        }

        public List<ModelContractRecord> AvailableContractsList { get; set; }
        public List<ModelContractRecord> AllContractsList { get; set; }
        public List<ModelContractRecord> SelectedContractsList { get; set; }
        //private bool _bAvailableContractsListInited;

        public virtual void EnsureAvailableList()
        {
            AvailableContractsList = new List<ModelContractRecord>();
            if (SelectedTransferType == null)
                return;
            if (SelectedTransferType.ShowPeriod == true && SelectedYear == null)
                return;

            if (SelectedTransferType.ShowPeriod == true)
            {
                //Если операция использует период							
                #region Period

                var availableContractsListWithCurrentYear = GetOperationsContracts();

                var aclWithoutCurrentYear = new List<ModelContractRecord>(availableContractsListWithCurrentYear);

                AllContractsList = availableContractsListWithCurrentYear;

                if (IsFinVypl)
                {

                    //Функционал фильтрации отключён http://jira.vs.it.ru/browse/DOKIPIV-213
                    //var UKPlanModelContractRecord = new List<ModelContractRecord>();
                    //foreach (var contractRecord in AvailableContractsListWithCurrent_Year)
                    //{
                    //    foreach (var item in siapt)
                    //    {
                    //        if (contractRecord.ContractName == item.ContractNumber &&
                    //            item.MonthID == this.SelectedMonth.ID &&
                    //            item.ReqTransferID == 0)
                    //        {
                    //            //if (!VRModelContractRecord.Contains(contractRecord))
                    //            {
                    //                UKPlanModelContractRecord.Add(contractRecord);
                    //            }
                    //        }
                    //    }

                    //}
                    //aclWithoutCurrentYear = UKPlanModelContractRecord;
                }

                AvailableContractsList = new List<ModelContractRecord>(aclWithoutCurrentYear);
                #endregion  Period
            }
            else
            {
                //Если для операции не используется период
                if (SelectedTransferType == null || SelectedTransferDirection == null)
                    return;
                //Функционал фильтрации отключён http://jira.vs.it.ru/browse/DOKIPIV-213 внутри метода сервиса
                AvailableContractsList = BLServiceSystem.Client.GetContractsWithoutRegister(_contractType, SelectedTransferType.ID, SelectedTransferDirection.ID, SelectedDate)
                    .ConvertAll(c => new ModelContractRecord(c)).ToList();
            }
        }

        protected virtual List<ModelContractRecord> GetOperationsContracts()
        {
            return BLServiceSystem.Client.GetContractsForUKsByType(_contractType).ConvertAll(
                c => new ModelContractRecord(c)).ToList();
        }

        public long RegisterID;
        /// <summary>
        /// Создание реестра и дерева данных
        /// </summary>
        public bool CreateRegisterEx()
        {
            long? opContentId;
            long? opTypeId;
            //выбираем тип операции для Мастера годового плана
            var item = SelectedTransferType as SPNOperationTypeItem;
            if (item != null)
            {
                opContentId = item.ID == 0 ? null : (long?)item.ID;
                opTypeId = item.TypeId;
            }
            else
            {
                opContentId = SelectedTransferType.ID;
                opTypeId = null;
            }

            //создание Реестра
            var register = new SIRegister
            {
                RegisterKind = "Постановление правления ПФР",
                DirectionID = SelectedTransferDirection.ID,
                OperationID = opContentId,
                OperationTypeID = opTypeId,
                RegisterDate = SelectedDate,
                RegisterNumber = string.Empty,
                CompanyYearID = SelectedTransferType.ShowPeriod == true ? (long?)SelectedYear.ID : null,
                Comment = Comments,
                PayAssignment = PayAssignment,
                CompanyMonthID = !(this is TransferVR13YearWizardViewModel && !(this is TransferSI8YearWizardViewModel)) && SelectedTransferType.ShowMonth.Value && SelectedMonth != null ? (long?)SelectedMonth.ID : null
            };
            //если не выбраны договора - выходим
            if (SelectedContractsList == null)
            {
                RegisterID = DataContainerFacade.Save<SIRegister, long>(register);
                JournalLogger.LogModelEvent(this, JournalEventType.INSERT, "Создание реестра.", $"Год реестра: {SelectedYear.Name}. Номер реестра: {RegisterID}.");
                return true;
            }

            var listSITransfer = new List<SITransfer>();
            var listReqTransfer = new List<ReqTransfer>();
            long idCounterSt = 0;
            foreach (var c in SelectedContractsList)
            {
                //создание списка
                var st = new SITransfer
                {
                    ContractID = c.ContractID,
                    //TransferRegisterID = RegisterID, 
                    InsuredPersonsCount = c.QuantityZL,
                    PlannedTransferSum = !CreateSITransfers ? (decimal?)c.Sum : null, //если правопреемники - записываем плановую сумму
                    ID = ++idCounterSt, //временный идентификатор для связи потомков  
                    StatusID = 1
                };
                listSITransfer.Add(st);

                //если не выплаты правопреемникам
                if (CreateSITransfers)
                {
                    //ДЛЯ РОПС и АВС записываем сумму в ИНВЕСТДОХОД
                    //Создание перечисления
                    var rt = new ReqTransfer
                    {
                        Sum = c.Sum,
                        InvestmentIncome = this is TransferSIRopsAsvWizardViewModel ? c.Sum : (SelectedTransferDirection.isFromUKToPFR.Value && CreateSITransfers ? (decimal?)c.InvestIncome : null),
                        TransferListID = st.ID,
                        TransferStatus = TransferStatusIdentifier.sBeginState,
                        //ID = idCounterSt++, //временный идентификатор для связи потомков  
                        ZLMoveCount = c.QuantityZL, //Задаём количество ЗЛ для перечисления
                        StatusID = 1
                    };
                    listReqTransfer.Add(rt);
                }
            }
            RegisterID = BLServiceSystem.Client.SaveTransferWizardResult(register, listSITransfer, listReqTransfer, IsFinVypl, SelectedYear.ID, SelectedMonth.ID, CreateSITransfers);
            if (RegisterID != 0)
                JournalLogger.LogModelEvent(this, JournalEventType.INSERT, "Создание реестра.", $"Год реестра: {SelectedYear.Name}. Номер реестра: {RegisterID}.");
            return RegisterID != 0;
        }


        public bool IsEnoughZL(Dictionary<long, int> contractData, out long zlCount)
        {
            var result = BLServiceSystem.Client.IsEnoughZLForTransferWizardOnDate(contractData, DateTime.Today);
            zlCount = result;
            return result == -1;
        }

        public virtual string AssignePaymentTitle => "Выплаты правопреемникам";

        public void DivideSum(decimal pSum)
        {
            var aSelected = SelectedContractsList;
            decimal dSum = pSum;
            //сумма всех средств в управлении по всем контрактам
            if (aSelected == null) return;
            decimal dWeightTotal = (from rec in aSelected
                                    where rec.ContractOperateSum.GetValueOrDefault() > 0
                                    select rec.ContractOperateSum.GetValueOrDefault()).Sum();

            if (dWeightTotal == 0)
            {
                Logger.WriteLine("TransferWizardViewModel.cs:DivideSum(decimal p_sum) - Общая сумма средств в управлении равна 0!");
                return;
            }

            if (aSelected.Count == 1)
            {
                aSelected[0].Sum = dSum;
            }
            else
            {
                decimal dSumRest = dSum;
                //Последний получает остаток - исключаем поргешность при пропорции
                for (int i = 0; i < aSelected.Count - 1; i++)
                {
                    var rec = aSelected[i];
                    decimal opSum = rec.ContractOperateSum.GetValueOrDefault();
                    decimal dContractSum = 0;
                    if (opSum > 0)
                    {
                        dContractSum = dSum / dWeightTotal * opSum;
                    }

                    //Округляем до копеек
                    dContractSum = decimal.Round(dContractSum, 2);

                    rec.Sum = dContractSum;
                    dSumRest = dSumRest - dContractSum;
                }

                aSelected[aSelected.Count - 1].Sum = dSumRest;
            }
        }

        public long GetZLBy(long contractID)
        {
            return BLServiceSystem.Client.GetZLCountForDate(contractID, DateTime.Today);
        }

        

        private List<Year> GetYearsList()
        {
            if (CreateSITransfers)
            {
                if (IsFinVypl)
                {
                    //на тот год на который содавался план
                    //return BLServiceSystem.Client.GetYearListCreateSIUKPlan(
                    //    (Document.Types)ContractType, SelectedTransferType.ID);

                    //Возвращаем все года http://jira.dob.datateh.ru/browse/DOKIPIV-536
                    return _yearsListAll;
                }
                return _yearsListAll;
            }
            //мастер годового плана - только года, по которым не создавались планы
            if (SelectedTransferType is SPNOperationTypeItem)
            {
                var createPlanYears = BLServiceSystem.Client.GetYearListForYearPlanCreation((Document.Types)ContractType, ((SPNOperationTypeItem)SelectedTransferType).TypeId);
                return createPlanYears;//this.yearsListAll.Where(y => createPlanYear.All(cr => cr.ID != y.ID)).ToList();
            }
            var createPlanYear = BLServiceSystem.Client.GetYearListCreateSIUKPlan((Document.Types)ContractType, SelectedTransferType.ID);
            return _yearsListAll.Where(y => createPlanYear.All(cr => cr.ID != y.ID)).ToList();
            //YearsList = BLServiceSystem.Client.GetValidYearListForYearPlanCorrection((Document.Types)ContractType);
        }

        public event EventHandler RequestCustomAction;

        protected void OnRequestCustomAction(List<long> list)
        {
            RequestCustomAction?.Invoke(list, null);
        }


        public class ModelContractRecord : BaseDataObject
        {
            private readonly Contract _contract;

            public ModelContractRecord(Contract pContract)
            {
                // _ukName = p_contract.ExtensionData == null ? null : (p_contract.ExtensionData.ContainsKey("leName") ? (string)p_contract.ExtensionData["leName"] : null);
                //_ukFormalizedName = p_contract.ExtensionData == null ? null : (p_contract.ExtensionData.ContainsKey("leFormName") ? (string)p_contract.ExtensionData["leFormName"] : null); 
                _contract = pContract;
            }

            public bool IsActive => !ContragentHelper.IsContractMarkedRed(_contract.DissolutionDate);

            private readonly string _ukName;
            public string UkName => _ukName ?? _contract.GetLegalEntity().GetContragent().Name;


            private readonly string _ukFormalizedName;
            public string UkFormalizedName => _ukFormalizedName ?? _contract.GetLegalEntity().FormalizedName;

            public string ContractName => _contract.ContractNumber;


            public decimal? ContractOperateSum => _contract.OperateSum;

            public long ContractID => _contract.ID;

            public decimal InvestIncome { get; set; }


            public decimal Sum { get; set; }

            public int QuantityZL { get; set; }

            public override string this[string columnName]
            {
                get
                {

                    switch (columnName)
                    {
                        case "Sum":
                            if (Sum < 0)
                                return "Отрицательное значение недопустимо";
                            break;
                        case "QuantityZL":
                            if (QuantityZL.ToString().Length > 15)
                                return "Максимальная длинна 15";

                            if (QuantityZL < 0)
                                return "Отрицательное значение недопустимо";

                            break;
                    }

                    return null;
                }
            }
        }
    }
}
