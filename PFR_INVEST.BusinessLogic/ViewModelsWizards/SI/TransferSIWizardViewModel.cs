﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.SI
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class TransferSIWizardViewModel : TransferWizardViewModel
    {
        public override bool IsPayAssVisible => true;

        public TransferSIWizardViewModel(bool forAPSI)
            : base(Document.Types.SI,forAPSI)
        {
           
        }
        
    }
}
