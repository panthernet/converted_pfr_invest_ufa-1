﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.SI
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class TransferSI8YearWizardViewModel : TransferWizardViewModel
    {
        /// <summary>
        /// Доступно ли поле выбора Типа операции
        /// </summary>
        public override bool IsOperationTypeEnabled => true;

        public override bool IsImportAvailable { get; } = false;

        private const long OP_VYP_PRAVO_ID = 8L;

        //Список ID наименований контрактов для годового плана СИ
        private readonly int[] _availableContractNames = { 1, 2, 3 };

        public TransferSI8YearWizardViewModel(bool forApsi)
            : base(Document.Types.SI, forApsi)
        {

        }

        protected override void SetVkTranfderType()
        {
            TransferTypesUKtoPFR = TransferWizardHelper.GetOperationTypesListForYearPlanMasked(1);
            //BLServiceSystem.Client.GetSPNOperationsByTypeListHib(1).Where(op => _availableOpIdList.Contains(op.ID)).ToList();


            if (TransferTypes_UKtoPFR.Count == 0)
                base.SetVkTranfderType();            
            else
                SelectedTransferType = TransferTypes_UKtoPFR[0];
        }


        public override void EnsureAvailableList()
        {          
            AvailableContractsList = GetOperationsContracts();
        }

        protected override List<ModelContractRecord> GetOperationsContracts()
        {
            //TODO нужен ли отсев по наименованиям контрактов для новых операций?

            if (SelectedTransferType.ID == OP_VYP_PRAVO_ID)
                return BLServiceSystem.Client.GetContractsForUKsByTypeAndName(_contractType, _availableContractNames, false).ConvertAll(
                    c => new ModelContractRecord(c)).Where(a=> a.IsActive).ToList();
            return BLServiceSystem.Client.GetContractsForUKsByType(_contractType, false).ConvertAll(
                c => new ModelContractRecord(c)).Where(a=> a.IsActive).ToList();
        }

        public override string AssignePaymentTitle => SelectedTransferType.Name;
    }
}
