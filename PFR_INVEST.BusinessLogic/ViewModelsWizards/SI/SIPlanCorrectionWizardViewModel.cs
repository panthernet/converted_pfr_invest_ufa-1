﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SIPlanCorrectionWizardViewModel : PlanCorrectionWizardViewModel<PlanContractRecord>
    {
        public SIPlanCorrectionWizardViewModel(long operationId, long direction = 0) : base(operationId, direction)
        {
        }

        protected override List<Year> GetValidYearList()
        {
            return BLServiceSystem.Client.GetValidYearListForYearPlanCorrection(Document.Types.SI, ((SPNOperationTypeItem)SelectedOp).TypeId);
        }

        protected override List<Month> GetValidMonthList(long yearID)
        {
            return BLServiceSystem.Client.GetValidMonthListForYearPlanCorrection(Document.Types.SI, yearID, ((SPNOperationTypeItem)SelectedOp).TypeId).OrderBy(a => a.ID).ToList();
           // return this.MonthsList;
        }

        protected override List<SIUKPlan> GetUKPlansForYearMonthAndOperation(long monthID, long yearID)
        {
            return BLServiceSystem.Client.GetUKPlansForYearMonthAndOperation(Document.Types.SI, monthID, yearID, OperationId, false);
        }

        protected override IEnumerable<Type> GetRelatedViewModelTypesToRefresh()
        {
            yield return typeof (SIAssignPaymentsListViewModel);
        }

        protected override PlanContractRecord Convert(SIUKPlan ukPlan)
        {
            return new PlanContractRecord(ukPlan);
        }


    }
}
