﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using Document = PFR_INVEST.DataObjects.Document;

namespace PFR_INVEST.BusinessLogic
{
	public abstract class MonthTransferCreationWizardModel : PlanCorrectionWizardViewModel<PlanCreateContractRecord>
	{
		private const string WORD_TEMPLATE = "Уведомление о корректировке.doc";
		private const string WORD_TEMPLATE_NO_PAYMENTS = "Уведомления о корректировке - нет выплат.doc";

		public string ErrorItem { get; private set; }
		public Document.Types Type;

		private readonly SPNDirection _dirOperSi;

	    protected MonthTransferCreationWizardModel(long operationId, long direction = 0)
			: base(operationId, direction)
		{
			if (this is VRMonthTransferCreationWizardModel)
			{
				Type = Document.Types.VR;
				_dirOperSi = DataContainerFacade.GetByID<SPNDirection>(3);
			}
			else
			{
				Type = Document.Types.SI;
				_dirOperSi = DataContainerFacade.GetByID<SPNDirection>(1);
			}

		}

		public long RegisterID { get; private set; }




		public static List<AddressTypeItem> AddressTypes { get; } = new List<AddressTypeItem> 
		{ 
		    new AddressTypeItem {Title = "Почтовый", Value = AddressType.Post},
		    new AddressTypeItem {Title = "Фактический", Value = AddressType.Fact}
		};

	    public AddressTypeItem SelectedAddressType { get; set; } = AddressTypes[0];

	    private _Application _mWApp;
		private _Application WordApp
		{
			get { return _mWApp ?? (_mWApp = new Application()); }
			set
			{
				_mWApp = value;
			}
		}

	    public bool OpenFolder { get; set; } = true;

	    /* private readonly List<ReqTransfer> m_CreatedTransfers = new List<ReqTransfer>();
		 private readonly List<Contract> m_ProcessedContracts = new List<Contract>();
		 private List<Contract> m_NoPaymentsContracts = new List<Contract>();*/

		public bool RegisterCreated;

		public void CreateRegister()
		{

			//Создание реестра
			var transferRegister = new SIRegister
			{
				RegisterKind = "Постановление правления ПФР",
				DirectionID = _dirOperSi.ID,
				OperationID = OperationContentId,
				OperationTypeID = ((SPNOperationTypeItem)SelectedOp).TypeId,
				RegisterDate = DateTime.Now,
				CompanyYearID = SelectedYear.ID,
				CompanyMonthID = SelectedMonth.ID
			};
			#region OLD
			/*transferRegister.ID = DataContainerFacade.Save<SIRegister, long>(transferRegister);
            RegisterID = transferRegister.ID;

            foreach (var cr in AvailableContractsList)
            {
                //var plan = cr.UKPlan;
                if (cr.FactSum == null || !(cr.FactSum > 0)) continue;
                //var contract = cr.UKPlan.GetTransferList().GetContract();
                //var legalEntity = contract.GetLegalEntity();

                //Создание списка
                var transferList = new SITransfer
                {
                    ContractID = cr.ContractID,
                    TransferRegisterID = transferRegister.ID,
                    InsuredPersonsCount = cr.QuantityZL,
                    //ExtensionData = new Dictionary<string, object>()
                };
                // transferList.ExtensionData["Contract"] = contract;
                //transferList.ExtensionData["LegalEntity"] = legalEntity;
                transferList.ID = DataContainerFacade.Save<SITransfer, long>(transferList);

                var transfer = new ReqTransfer
                {
                    TransferListID = transferList.ID,
                    TransferStatus = TransferStatusIdentifier.sBeginState,
                    Date = DateTime.Today,
                    Sum = cr.FactSum.GetValueOrDefault(),
                    InvestmentIncome = cr.InvestmentIncome
                };
                transfer.ID = DataContainerFacade.Save<ReqTransfer, long>(transfer);
                // transfer.ExtensionData = new Dictionary<string, object>();
                // transfer.ExtensionData["TransferList"] = transferList;

                cr.UKPlan.SetTransferId(transfer.ID);
                cr.UKPlan.AddFactSum(cr.FactSum);
                DataContainerFacade.Save<SIUKPlan, long>(cr.UKPlan);

                // m_CreatedTransfers.Add(transfer);
                // m_ProcessedContracts.Add(contract);
                //else
                //    m_NoPaymentsContracts.Add(cr.UKPlan.GetTransferList().GetContract());
            }*/
			#endregion
			var list = AvailableContractsList.Select(a => new MonthTransferWizardResultListItem
			{
				FactSum = a.FactSum,
				UKPlan = a.UKPlan,
				InvestmentIncome = a.InvestmentIncome ?? 0.00m,
				ContractID = a.ContractID,
				QuantityZL = a.QuantityZL ?? 0L
			}).ToList();
			RegisterID = BLServiceSystem.Client.SaveMonthTransferWizardResults(transferRegister, list);
		}



		public static bool CheckDirectoryName(string folder)
		{
			try
			{
				new DirectoryInfo(folder);
				return folder == Path.GetFullPath(folder);
			}
			catch
			{
				return false;
			}
		}

		public static bool DirectoryExists(string directoryName)
		{
			return Directory.Exists(directoryName);
		}

		public enum PrintResult
		{
			OK = 0,
			TemplateNotFound = 1,
			CannotCreateFile = 2,
			CannotCreateDirectory = 3
		}

		public string SelectedFolder;

		/*public PrintResult PrintRequests()
		{
			try
			{
				if (!Directory.Exists(SelectedFolder))
					Directory.CreateDirectory(SelectedFolder);
			}
			catch (Exception e)
			{
				Logger.WriteException(e);
				return PrintResult.CannotCreateDirectory;
			}

			foreach (var transfer in m_CreatedTransfers)
			{
				var res = Print(transfer);
				if (res == PrintResult.OK)
				{
					transfer.TransferStatus = TransferStatusIdentifier.sDemandGenerated;
					DataContainerFacade.Save<ReqTransfer, long>(transfer);
				}
				else
					return res;
			}

			foreach (var contract in m_NoPaymentsContracts)
			{
				var res = PrintNoPayments(contract);
				if (res != PrintResult.OK)
					return res;
			}

			RefreshRelatedViewModels();

			return PrintResult.OK;
		}*/

		private static string GetProperHeadName(string name)
		{
			try
			{
				string[] names = name.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				return names.Length >= 3 ? string.Join(" ", names[1], names[2]) : name;
			}
			catch
			{
				return name;
			}
		}

		private void GenerateDoc(ReqTransfer transfer, Contract contract)
		{
			var le = contract.GetLegalEntity();

			string address = string.Empty;
			switch (SelectedAddressType.Value)
			{
				case AddressType.Post:
					address = le == null ? string.Empty : le.PostAddress;
					break;
				case AddressType.Fact:
					address = le == null ? string.Empty : le.StreetAddress;
					break;
			}

			decimal sum = Math.Floor(transfer.Sum.GetValueOrDefault());
			decimal sumFrac = (transfer.Sum.GetValueOrDefault(0) - sum) * 100;

			decimal investmentIncome = Math.Floor(transfer.InvestmentIncome.GetValueOrDefault(0));
			decimal investmentIncomeFrac = (transfer.InvestmentIncome.GetValueOrDefault(0) - investmentIncome) * 100;

			OfficeTools.FindAndReplace(WordApp, "{LEFULLNAME}", le.FullName);
			OfficeTools.FindAndReplace(WordApp, "{ADDRESS}", address);

			OfficeTools.FindAndReplace(WordApp, "{HEADNAME}", GetProperHeadName(le.HeadFullName));

			OfficeTools.FindAndReplace(WordApp, "{CONTRACTDATE}", contract.ContractDate.Value.ToString("dd.MM.yy"));
			OfficeTools.FindAndReplace(WordApp, "{CONTRACTNUMBER}", contract.ContractNumber);

			OfficeTools.FindAndReplace(WordApp, "{MONTH}", DateTools.GetProperMonthName(SelectedMonth.ID, SelectedMonth.Name));
			OfficeTools.FindAndReplace(WordApp, "{YEAR}", SelectedYear.Name.Trim());

			OfficeTools.FindAndReplace(WordApp, "{SUM}", sum);
			OfficeTools.FindAndReplace(WordApp, "{SUMFRAC}", sumFrac.ToString("00"));
			OfficeTools.FindAndReplace(WordApp, "{INWORDS}", Сумма.Пропись(transfer.Sum.Value, Валюта.Рубли));

			OfficeTools.FindAndReplace(WordApp, "{INVINCOME}", investmentIncome);
			OfficeTools.FindAndReplace(WordApp, "{INVINCOMEFRAC}", investmentIncomeFrac.ToString("00"));
		}

		private PrintResult Print(ReqTransfer pTransfer)
		{
			Contract contract = pTransfer.GetTransferList().GetContract();

			string wordTemplateName = WORD_TEMPLATE;

			string extractedFilePath = TemplatesManager.ExtractTemplate(wordTemplateName);

			if (!File.Exists(extractedFilePath))
			{
				ErrorItem = wordTemplateName;
				return PrintResult.TemplateNotFound;
			}

			try
			{
				//Требование  <компания> <№ договора> ID.doc
				string toFile = Path.Combine(SelectedFolder,
				    $"Требование {contract.GetLegalEntity().FormalizedName} {contract.ContractNumber} {pTransfer.ID}.doc");
				if (File.Exists(toFile))
					File.Delete(toFile);
				File.Move(extractedFilePath, toFile);
				extractedFilePath = Path.GetFullPath(toFile);
			}
			catch (Exception e)
			{
				Logger.WriteException(e);
				WordApp = null;
				return PrintResult.CannotCreateFile;
			}

			if (File.Exists(extractedFilePath))
			{
				object filePath = extractedFilePath;

				WordApp = null;
				WordApp.Visible = false;
				WordApp.Documents.Open(ref filePath);

				GenerateDoc(pTransfer, contract);

				WordApp.Quit(WdSaveOptions.wdSaveChanges, WdOriginalFormat.wdWordDocument);
				WordApp = null;
			}
			else
			{
				ErrorItem = wordTemplateName;
				return PrintResult.TemplateNotFound;
			}

			return PrintResult.OK;
		}

		private void GenerateDocNoPayments(LegalEntity le, Contract contract)
		{
			string address = string.Empty;
			switch (SelectedAddressType.Value)
			{
				case AddressType.Fact:
					address = le == null ? string.Empty : le.StreetAddress;
					break;
				case AddressType.Post:
					address = le == null ? string.Empty : le.PostAddress;
					break;
			}

			OfficeTools.FindAndReplace(WordApp, "{LEFULLNAME}", le.FullName);
			OfficeTools.FindAndReplace(WordApp, "{LESHORTNAME}", le.ShortName);
			OfficeTools.FindAndReplace(WordApp, "{ADDRESS}", address);

			OfficeTools.FindAndReplace(WordApp, "{HEADNAME}", GetProperHeadName(le.HeadFullName));

			OfficeTools.FindAndReplace(WordApp, "{CONTRACTDATE}", contract.ContractDate.Value.ToString("dd.MM.yy"));
			OfficeTools.FindAndReplace(WordApp, "{CONTRACTNUMBER}", contract.ContractNumber);

			OfficeTools.FindAndReplace(WordApp, "{MONTH}", DateTools.GetProperMonthName(SelectedMonth.ID, SelectedMonth.Name));
			OfficeTools.FindAndReplace(WordApp, "{YEAR}", SelectedYear.Name.Trim());
		}

		private PrintResult PrintNoPayments(Contract p_Contract)
		{
			var legalEntity = p_Contract.GetLegalEntity();

			string wordTemplateName = WORD_TEMPLATE_NO_PAYMENTS;

			string extractedFilePath = TemplatesManager.ExtractTemplate(wordTemplateName);

			if (!File.Exists(extractedFilePath))
			{
				ErrorItem = wordTemplateName;
				return PrintResult.TemplateNotFound;
			}

			try
			{
				//Требование  <компания> <№ договора> ID.doc
				string toFile = Path.Combine(SelectedFolder,
				    $"Требование {legalEntity.FormalizedName} {p_Contract.ContractNumber} - нет выплат.doc");
				if (File.Exists(toFile))
					File.Delete(toFile);
				File.Move(extractedFilePath, toFile);
				extractedFilePath = Path.GetFullPath(toFile);
			}
			catch (Exception e)
			{
				Logger.WriteException(e);
				WordApp = null;
				return PrintResult.CannotCreateFile;
			}

			if (File.Exists(extractedFilePath))
			{
				object filePath = extractedFilePath;

				WordApp = null;
				WordApp.Visible = false;
				WordApp.Documents.Open(ref filePath);

				GenerateDocNoPayments(legalEntity, p_Contract);

				WordApp.Quit(WdSaveOptions.wdSaveChanges, WdOriginalFormat.wdWordDocument);
				WordApp = null;
			}
			else
			{
				ErrorItem = wordTemplateName;
				return PrintResult.TemplateNotFound;
			}

			return PrintResult.OK;
		}

		public void OpenCreatedFolder()
		{
			try
			{
				if (OpenFolder)
					Process.Start(SelectedFolder);
			}
		    catch
		    {
		        // ignored
		    }
		}

		protected override PlanCreateContractRecord Convert(SIUKPlan ukPlan)
		{
			return new PlanCreateContractRecord(ukPlan);
		}

		public abstract IEnumerable<Type> GetRelatedAssignPaymentListViewModelTypes();

		public class AddressTypeItem
		{
			public AddressType Value { get; set; }
			public string Title { get; set; }
		}
	}
}
