﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class PlanContractRecord
    {
        public SIUKPlan UKPlan { get; set; }

        public PlanContractRecord(SIUKPlan ukPlan)
        {
            UKPlan = ukPlan;
        }

        public string LegalEntityFormalizedName => UKPlan.GetTransferList().GetContract().GetLegalEntity().FormalizedName;

        public string ContractNumber => UKPlan.GetTransferList().GetContract().ContractNumber;

        public long ContractID => UKPlan.GetTransferList().GetContract().ID;

        public decimal? PlanSum
        {
            get
            {
                return UKPlan.PlanSum;
            }
            set
            {
                UKPlan.PlanSum = value;
            }
        }
    }
}
