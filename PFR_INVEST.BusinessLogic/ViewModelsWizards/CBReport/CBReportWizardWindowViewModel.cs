﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.CBReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class CBReportWizardWindowViewModel: ViewModelBase
    {
        public CBRApprovalRequest GenerateAppItem()
        {
            return new CBRApprovalRequest
            {
                Year = _selectedYear ?? 0,
                Quarter = _selectedQuarter?.Number ?? 0,
                Okud = SelectedReportType == null ? "" : BOReportForm1.GetOkudString(SelectedReportType.Value)
            };
        }

        public void ExecuteApprovement()
        {
            var item = new CBRApprovalRequest
            {
                Year = _selectedYear.Value,
                Quarter = _selectedQuarter.Number,
                Okud = BOReportForm1.GetOkudString(SelectedReportType.Value),
                Username1 = SelectedUser1?.Name,
                Login1 = SelectedUser1?.Login,
                Username2UK = SelectedUser2UK?.Name,
                Login2UK = SelectedUser2UK?.Login,
                Username2NPF = SelectedUser2NPF?.Name,
                Login2NPF = SelectedUser2NPF?.Login,
                Username3 = SelectedUser3?.Name,
                Login3 = SelectedUser3?.Login,
                ReportType = SelectedReportType.Value
            };
            var list = new List<KeyValuePair<Type, long>>();
            BLServiceSystem.Client.CBReportRequestApproval(item).ForEach(a=> list.Add(new KeyValuePair<Type, long>(typeof(BOReportForm1), a)));
            ViewModelManager.UpdateDataInAllModels(list);
        }

        #region SelectCBReport
        private List<int> _yearsList;
        public List<int> YearsList
        {
            get { return _yearsList; }
            set { _yearsList = value; OnPropertyChanged("YearsList"); }
        }

        public List<string> OkudList
        {
            get { return _okudList; }
            set { _okudList = value; OnPropertyChanged("OkudList"); }
        }

        private List<QuarterData> _quartersList;
        public List<QuarterData> QuartersList
        {
            get { return _quartersList; }
            set { _quartersList = value; OnPropertyChanged("QuartersList"); }
        }

        private int? _selectedYear;
        public int? SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                _selectedYear = value;
                RefreshFromYear(); OnPropertyChanged("SelectedYear");
            }
        }

        private QuarterData _selectedQuarter;
        public QuarterData SelectedQuarter
        {
            get { return _selectedQuarter; }
            set
            {
                _selectedQuarter = value; RefreshFromQuarter();
                OnPropertyChanged("SelectedQuarter");
            }
        }

        private string _selectedOkud;
        public string SelectedOkud
        {
            get { return _selectedOkud; }
            set { _selectedOkud = value; OnPropertyChanged("SelectedOkud"); }
        }

        public void InitializeSelectCB()
        {
            RefreshData(true);
        }

        private Dictionary<string, Dictionary<int, List<int>>> _cache;
        private List<string> _okudList;
        private List<User> _usersList;
        private User _selectedUser1;
        private User _selectedUser2UK;
        private User _selectedUser2NPF;
        private User _selectedUser3;

        private void RefreshData(bool isNew)
        {
           // var memY = isNew ? _selectedYear : null;
            _selectedYear = null;
            _cache = BLServiceSystem.Client.GetNotApprovedCBReportsYearsAndQuartersList(BOReportForm1.ReportTypeEnum.All);
            //выбираем все года
            _yearsList = new List<int>();
            _cache.Values.ForEach(b => _yearsList.AddRange(b.Keys.Select(a => a)));
            YearsList = _yearsList.Distinct().OrderByDescending(a => a).ToList();
            _selectedYear = _yearsList.FirstOrDefault();
            OnPropertyChanged("SelectedYear");
            RefreshFromYear(isNew);
        }

        private void RefreshFromYear(bool isNew = false)
        {
            //var memQ = isNew ? _selectedQuarter : null;
            if (!_selectedYear.HasValue || _cache == null)
            {
                SelectedQuarter = null;
                QuartersList = null;
                OkudList = null;
                SelectedOkud = null;
                return;
            }

            _quartersList = new List<QuarterData>();
            var year = _selectedYear.Value;
            _cache.Where(a => a.Value.ContainsKey(year))
                .ForEach(item =>
                {
                    var a = item.Value;
                    var list =
                        a[year].Where(q => _quartersList.FirstOrDefault(qq => qq.Number == q) == null)
                            .ToList()
                            .ConvertAll(b => new QuarterData { Number = b, Text = DateTools.GetQuarterRomeName(b) });
                    if (list.Count > 0)
                        _quartersList.AddRange(list);
                });
            QuartersList = _quartersList.OrderBy(a => a.Number).ToList();
            _selectedQuarter = _quartersList.LastOrDefault();
            OnPropertyChanged("SelectedQuarter");
            RefreshFromQuarter();
        }

        private void RefreshFromQuarter(bool isNew = false)
        {
            //var memO = isNew ? _selectedOkud : null;
            _okudList = new List<string>();
            _okudList = _cache.Where(a => a.Value.ContainsKey(_selectedYear.Value) && a.Value[_selectedYear.Value].Contains(_selectedQuarter.Number)).Select(a => a.Key).Distinct().OrderBy(a=> a).ToList();
            if (_okudList.Count == 3)
                _okudList.Insert(0, "Все");
            OnPropertyChanged("OkudList");
            SelectedOkud = _okudList.FirstOrDefault();
        }
        #endregion

        #region SelectApprovers

        public string ReportString => $@"{_selectedQuarter.Text} {_selectedYear} года, ОКУД - {(SelectedReportType == null ? "" : BOReportForm1.GetOkudString(SelectedReportType.Value))}";

        public BOReportForm1.ReportTypeEnum? SelectedReportType => _selectedOkud == null ? null : (BOReportForm1.ReportTypeEnum?)BOReportForm1.GetOkudTypeFromString(_selectedOkud);

        public List<User> UsersList
        {
            get { return _usersList; }
            set { _usersList = value; OnPropertyChanged("UsersList");}
        }

        public User SelectedUser1
        {
            get { return _selectedUser1; }
            set { _selectedUser1 = value; OnPropertyChanged("SelectedUser1"); }
        }

        public User SelectedUser2UK
        {
            get { return _selectedUser2UK; }
            set { _selectedUser2UK = value; OnPropertyChanged("SelectedUser2UK"); }
        }

        public User SelectedUser2NPF
        {
            get { return _selectedUser2NPF; }
            set { _selectedUser2NPF = value; OnPropertyChanged("SelectedUser2NPF"); }
        }

        public User SelectedUser3
        {
            get { return _selectedUser3; }
            set { _selectedUser3 = value; OnPropertyChanged("SelectedUser3"); }
        }

        public void InitializeSelectApprovers()
        {
            UsersList = AP.Provider.GetAuthUsers().Where(a => !string.IsNullOrEmpty(a.Name) && !string.IsNullOrEmpty(a.Login)).OrderBy(a=> a.Name).ToList();
            UpdateReportString();
        }

        public void UpdateReportString()
        {
            OnPropertyChanged("ReportString");
        }
        #endregion
    }
}
