﻿using System;
using System.Windows.Input;
using System.Runtime.CompilerServices;

namespace PFR_INVEST.BusinessLogic.Commands
{
    public class DelegateCommand : DelegateCommand<object>
    {
        public DelegateCommand(Func<object, bool> canExecute, Action<object> execute)
            : base(canExecute, execute)
        {
            //Для совместимости с командами модели и обработчиком ошибок
            this.SafeExecute = false;
        }

        public DelegateCommand(Action<object> execute) : this(o => true, execute) { }

    }

    public class DelegateCommand<T> : ICommand
    {
        readonly Func<T, bool> canExecute;
        readonly Action<T> execute;
        protected bool SafeExecute { get; set; }

        public DelegateCommand(Action<T> execute)
            : this(o => true, execute) { }

        public DelegateCommand(Func<T, bool> canExecute, Action<T> execute)
        {
            this.canExecute = canExecute;
            this.execute = execute;
            this.SafeExecute = true;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public bool CanExecute(T parameter)
        {
            return canExecute(parameter);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public void Execute(T parameter)
        {
            this.ExecuteInternal(parameter);
        }

        private void ExecuteInternal(T parameter)
        {
            execute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return this.CanExecute((T)parameter);
        }

        void ICommand.Execute(object parameter)
        {
            try
            {
                this.ExecuteInternal((T)parameter);
            }
            catch (Exception ex)
            {
                if (this.SafeExecute)
                {
#warning Используется из за отсутствия прямого доступа к логгеру
                    //Используется из за отсутствия прямого доступа к логгеру
                    ViewModelBase.HandleCommonError(ex);
                }
                else
                    throw;
            }
        }
    }
}
