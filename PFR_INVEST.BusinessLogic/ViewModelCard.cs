﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.CBRConnector;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ServiceItems;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class ViewModelCard : ViewModelBase, IDataErrorInfo
	{
        /// <summary>
        /// Заголовок окна карточки
        ///  </summary>
		public string NewCaption { get; set; }
		public virtual ICBRConnector CBRConnector { get; private set; }
		public static EventHandler OnSaveCardError;
		public EventHandler OnBeforeCloseSaveCancelled;

        protected IEnumerable<Type> ConnectedListViewModels = new List<Type>();

        public delegate void RefreshConnectedCardsViewModelsDelegate();
        public RefreshConnectedCardsViewModelsDelegate RefreshConnectedCardsViewModels;
        public delegate void RefreshListViewModelsDelegate(IEnumerable<Type> viewModelLists);
        public static RefreshListViewModelsDelegate RefreshListViewModels;
        /// <summary>
        /// Команда сохранения карточки
        /// </summary>
        public ICommand SaveCard
        {
            get;
            protected set;
        }
        /// <summary>
        /// Позволяет переопределить текст сообщения для стандартного диалога удаления
        /// </summary>
        public virtual string CustomDeleteMessage => "";

        protected void LogStatusChanged()
		{

		}

		/// <summary>
		/// Название документа для документирования - переопределить в производном классе
		/// </summary>
		public virtual string GetDocumentHeader()
		{
			return string.Empty;
		}

		/// <summary>
		/// Дополнительная информация для логирования
		/// </summary>
		/// <returns></returns>
		public virtual string DocumentAdditionalInfo => string.Empty;


        protected void RefreshConnectedViewModels()
		{
#if WEBCLIENT
            return;
#endif

            RefreshConnectedCardsViewModels?.Invoke();
            //Обновление связаных моделей из конструктора
			ViewModelManager.RefreshViewModels(ConnectedListViewModels.ToArray());
		}

        protected ViewModelCard(Type pListViewModel)
			: this(new[] { pListViewModel })
		{
			ID = 0;
		}

        protected ViewModelCard(IEnumerable<Type> pListViewModelCollection)
			: this()
		{
			ConnectedListViewModels = pListViewModelCollection;
		}

        protected ViewModelCard(params Type[] pListViewModelCollection)
			: this()
		{
			ConnectedListViewModels = pListViewModelCollection;
		}

        protected ViewModelCard()
		{
			SaveCard = new Commands.DelegateCommand(o => CanExecuteSaveInternal(),
				o => ExecuteSaveInternal());
#if !WEBCLIENT
            CBRConnector = new CBRServiceConnector();
#endif
            IsInitialized = true;
		}		

		#region Scale feature implementation
        private double _scaleX = 1.0f;
        /// <summary>
        /// Хранит величину скалирования по Х
        /// </summary>
        public double ScaleX
        {
            get { return _scaleX; }
            set
            {
                _scaleX = value;
                OnPropertyChanged("ScaleX");
            }
        }

        private double _scaleY = 1.0f;
        /// <summary>
        /// Хранит величину скалирования по Y
        /// </summary>
        public double ScaleY
        {
            get { return _scaleY; }
            set
            {
                _scaleY = value;
                OnPropertyChanged("ScaleY");
            }
        }

		public void Scale(double delta)
		{
			if (delta > 1.0f)
			{
			    if ((!(ScaleX < 1.5f)) || (!(ScaleY < 1.5f))) return;
			    ScaleX *= delta;
			    ScaleY *= delta;
			    if (ScaleX > 1.5f) ScaleX = 1.5f;
			    if (ScaleY > 1.5f) ScaleY = 1.5f;
			}
			else
			{
			    if ((!(ScaleX > 1.0f)) || (!(ScaleY > 1.0f))) return;
			    ScaleX *= delta;
			    ScaleY *= delta;
			    if (ScaleX < 1.0f) ScaleX = 1.0f;
			    if (ScaleY < 1.0f) ScaleY = 1.0f;
			}
		}

		#endregion

 

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error => Error;
        public string Error { get; protected set; }

		public abstract string this[string columnName] { get; }

        /// <summary>
        /// Универсальный метод валидации. Проверяет все поля сущности методом this[].
        /// </summary>
        /// <param name="fieldString">Строка наименований полей, со значениями, разделенными символом | Если не указана, то будут перебираться все свойства объекта путем рефлексии.</param>
        public virtual bool ValidateFields(string fieldString = null)
		{
			Error = string.Empty;
			var props = string.IsNullOrEmpty(fieldString) ?  GetType().GetProperties().Where(p => p.CanRead).Select(a=> a.Name) : fieldString.Split('|');
			foreach (var p in props)
			{
				var error = this[p];
			    if (string.IsNullOrEmpty(error)) continue;
			    Error = error;
			    return false;
			}
			return true;
		}

		#endregion

        #region Execute implementations

        /// <summary>
        /// Проверка перед сохранением. Место для всяких подтверждений и т.д.
        /// True - для продолжения процеса сохранения 
        /// </summary>
        /// <returns>True - для продолжения процеса сохранения </returns>
        protected virtual bool BeforeExecuteSaveCheck()
        {
            return true;
        }

        /// <summary>
        /// Действие по завершению сохранения
        /// </summary>
        protected virtual void AfterExecuteSave() { }

        /// <summary>
        /// Выполнить сохранение
        /// </summary>
        protected abstract void ExecuteSave();

        /// <summary>
        /// Условие выполнение сохранения. Если False - отмена
        /// </summary>
        /// <returns></returns>
        public abstract bool CanExecuteSave();

        /// <summary>
        /// Операция удаления
        /// </summary>
        /// <param name="delType">Тип операции, по умолчанию 1 = Удаление</param>
        protected override void ExecuteDelete(int delType = 1)
		{
			//save delete entry in db
            var entityType = GetModelEntityType();
            if (entityType != null)
            {
                delType = typeof(IMarkedAsDeleted).IsAssignableFrom(entityType) ? DocOperation.Archive : DocOperation.Delete;
                var name = entityType.GetCustomAttributes(typeof(UseRestoreAttribute), false).Length > 0
                    ? entityType.FullName
                    : GetType().Name;
                BLServiceSystem.Client.SaveDeleteEntryLog(name, LogCaption, ID, delType);
            }
            else
            {
                if (delType != DocOperation.Delete)
                    BLServiceSystem.Client.SaveDeleteEntryLog(GetType().Name, LogCaption, ID, delType);
            }

            JournalPropMappingReminder();
            RefreshConnectedViewModels();
			base.ExecuteDelete(delType);
		}

        /// <summary>
        /// Создает запись для объекта в системе восстановления при архивировании. Для случаев трекинга архивации при удалении объектов из 
        /// родительской модели и др. мест кроме родной модели (карточки)
        /// </summary>
        /// <param name="typeName">Наименования типа модели</param>
        /// <param name="caption">Текстовый заголовок</param>
        /// <param name="id">Идентификатор объекта</param>
        public static void CreateArchiveEntry(string typeName, string caption, long id)
        {
            BLServiceSystem.Client.SaveDeleteEntryLog(typeName, caption, id, DocOperation.Archive);
        }

        /// <summary>
        /// Операция удаления
        /// </summary>
        /// <param name="delType">Тип операции</param>
        /// <param name="inputID">Идентификатор</param>
        /// <param name="data">Дополнительные связанные данные</param>
        /// <param name="type">тип данных для записи восстановления</param>
        protected override void ExecuteDelete(int delType, long inputID, DeletedDataItem data = null, string type = null)
		{
			//save delete entry in db
            if (delType != DocOperation.Delete)
            {
                BLServiceSystem.Client.SaveDeleteEntryLog(type ?? GetType().Name, data?.DeleteLogCaption ?? LogCaption, inputID, delType, data);
            }

            JournalPropMappingReminder();
            RefreshConnectedViewModels();
			base.ExecuteDelete(delType);
		}

		private bool CanExecuteSaveInternal()
		{
			return !EditAccessDenied && CanExecuteSave();
		}

        /// <summary>
        /// Указывает провалилась ли предварительная проверка на возможность сохранения.
        /// Используется при работе с моделями, созданными вручную для обработки данных и принудительно вызывающими ExecuteSave
        /// </summary>
        public bool IsBeforeSaveCheckFailed { get; protected set; }

        /// <summary>
        /// Основная рутина сохранения карточки
        /// </summary>
        private void ExecuteSaveInternal()
        {
            var modelOperationState = State;
            try
            {

                if (!BeforeExecuteSaveCheck())
                {
                    IsBeforeSaveCheckFailed = true;
                    return;
                }
                IsBeforeSaveCheckFailed = false;
                ExecuteSave();
            }
            catch (ViewModelSaveUserCancelledException)
            {
                return;
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                RaiseSaveCardError();
                Logger.WriteException(e);

                return;
            }

            if (!(this is IVmNoDefaultLogging))
            {
                JournalPropMappingReminder();
                JournalLogger.LogModelEvent(this, GetLogEvent(modelOperationState));
            }

            if (ID >= 0)
            {
                State = ViewModelState.Edit;
                IsDataChanged = false;
            }
            //else
            //{
            //    State = ViewModelState.Edit;
            //    IsDataChanged = false;
            //}

#if !WEBCLIENT
            RefreshConnectedViewModels();
#endif

            OnCardSaved();

            IsDataChanged = false;

#if !WEBCLIENT
            if (this is IUpdateRaisingModel)
            {
                var m = (this as IUpdateRaisingModel);
                ViewModelManager.UpdateDataInAllModels(m?.GetUpdatedList());
            }
#endif
            AfterExecuteSave();
        }
#endregion

#region Logging implementation

        private static JournalEventType GetLogEvent(ViewModelState modelOperationState)
		{
			switch (modelOperationState)
			{
				case ViewModelState.Read:
					return JournalEventType.SELECT;
				case ViewModelState.Create:
					return JournalEventType.INSERT;
				case ViewModelState.Export:
					return JournalEventType.EXPORT_DATA;
				default:
					return JournalEventType.UPDATE;
			}
		}

        [Conditional("DEBUG")]
        private void JournalPropMappingReminder()
        {
            Debug.WriteLineIf(GetModelEntityType() == null, $"!!!ВНИМАНИЕ!!! Для модели {GetType().Name} не задан аттрибут для детализации журналирования ModelEntity!");
        }
#endregion

        protected override void PostOpenInternal()
        {
            base.PostOpenInternal();
            if (!DontLogOpenEvent && !(this is IVmNoDefaultLogging))
                JournalLogger.LogOpenEvent(this);
        }

        /// <summary>
        /// Вызывается после сохранения карточки и изменения статусов
        /// </summary>
        protected virtual void OnCardSaved() { }

		public void RaiseBeforeCloseSaveCancelled()
		{
		    OnBeforeCloseSaveCancelled?.Invoke(this, null);
		}

        protected void RaiseSaveCardError()
        {
            OnSaveCardError?.Invoke(this, null);
        }

        #region LATEST METHODS

        /// <summary>
        /// Функция удаления любой записи c IMarkedAsDeleted и IIdentifiable (последняя редакция)
        /// Создает запись об удалении объекта для последующего восстановления
        /// </summary>
        /// <typeparam name="T">Тип записи</typeparam>
        /// <param name="entity">Объект записи</param>
        /// <param name="dataList">Опциональный список дочерних записей для удаления</param>
        /// <param name="deleteAction">Опциональный метод удаления записи</param>
        protected void DeleteEntity<T>(T entity, List<DeletedData> dataList = null, Action deleteAction = null, string deleteEntryCaption = null)
            where T: IMarkedAsDeleted, IIdentifiable
        {
            var data = new DeletedDataItem { PreviousStatus = entity.StatusID, DataList = dataList};
            if (deleteAction == null)
                DataContainerFacade.Delete(entity);
            else deleteAction();
            data.DeleteLogCaption = typeof(T).GetCustomAttributes(typeof(DeleteLogCaptionAttribute), false).Select(a => (DeleteLogCaptionAttribute)a).FirstOrDefault()?.Text ?? LogCaption;
            ExecuteDelete(DocOperation.Archive, entity.ID, data, typeof(T).Name);
        }

        protected void DeleteEntity<T>(T entity, string deleteEntryCaption)
            where T : IMarkedAsDeleted, IIdentifiable
        {
            DeleteEntity(entity, null, null, deleteEntryCaption);
        }

        #endregion
	}
}
