﻿using System;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ReportDepositsPromtViewModel : ViewModelCardDialog
	{


		private DateTime? dateFrom;
		public DateTime? DateFrom { get { return dateFrom; } set { dateFrom = value; OnPropertyChanged("DateTo"); OnPropertyChanged("DateFrom"); } }

		private DateTime? dateTo;
		public DateTime? DateTo { get { return dateTo; } set { dateTo = value; OnPropertyChanged("DateTo"); OnPropertyChanged("DateFrom"); } }


		private bool useSettleDate = true;
		public bool UseSettleDate { get { return useSettleDate; } set { useSettleDate = value; OnPropertyChanged("UseSettleDate"); } }

		private bool useReturnDate;
		public bool UseReturnDate { get { return useReturnDate; } set { useReturnDate = value; OnPropertyChanged("UseReturnDate"); } }




		public ReportDepositsPromtViewModel()
		{
			UseSettleDate = true;
			UseReturnDate = false;
			DateFrom = new DateTime(DateTime.Now.Year - 2, 1, 1);
			DateTo = DateTime.Now.Date;
		}

		public override bool CanExecuteSave()
		{
			return IsValid();
		}

		public bool IsValid()
		{
			return ValidateFields();
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "DateFrom": return DateFrom.ValidateRequired() ?? DateFrom.ValidatePeriodStart(DateTo);
					case "DateTo": return DateTo.ValidateRequired() ?? DateTo.ValidatePeriodEnd(DateFrom);
				}
				return null;
			}
		}


		public ReportDepositsPrompt GetReportPrompt()
		{
			if (!IsValid())
				return null;
			var prompt = new ReportDepositsPrompt
			{
				DateFrom = DateFrom,
				DateTo = DateTo,
				UseReturnDate = UseReturnDate,
				UseSettleDate = UseSettleDate
			};

			return prompt;
		}
	}
}
