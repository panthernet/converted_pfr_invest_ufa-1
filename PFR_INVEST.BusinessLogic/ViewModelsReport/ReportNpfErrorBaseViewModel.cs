﻿using System;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class ReportNpfErrorBaseViewModel : ViewModelCardDialog, IRequestCloseViewModel, IRequestTopmost, IRequestLoadingIndicator
    {
        protected const int MAX_BAR_CHART_RECORDS = 19;

        //protected ErrorReportType ReportType { get; private set; }

        public event EventHandler RequestClose;
        public event EventHandler RequestTopMost;
        public event EventHandler RequestLoadingIndicator;

        /// <summary>
        /// Запросить отображение индикатора загрузки
        /// </summary>
        /// <param name="message">Сообщение</param>
        protected void OnRequestLoadingIndicator(string message = null)
        {
            RequestLoadingIndicator?.Invoke(message, null);
        }
        /// <summary>
        /// Запросить установку приложения в режим TopMost
        /// </summary>
        /// <param name="enabled">Вкл/выкл. Если sender != null тогда True</param>
        protected void OnRequestTopMost(bool enabled)
        {
            RequestTopMost?.Invoke(enabled ? this : null, null);
        }  

        private DateTime? _dateFrom;
        public DateTime? DateFrom { get { return _dateFrom; } set { _dateFrom = value; OnPropertyChanged("DateTo"); OnPropertyChanged("DateFrom"); } }

        private DateTime? _dateTo;
        public DateTime? DateTo { get { return _dateTo; } set { _dateTo = value; OnPropertyChanged("DateTo"); OnPropertyChanged("DateFrom"); } }

        public ICommand RunReportCommand { get; private set; }

        public ReportNpfErrorBaseViewModel()
        {

            State = ViewModelState.Export;
            RunReportCommand = new DelegateCommand(o => IsValid(), o => { });
            DateFrom = new DateTime(DateTime.Today.Year -1, 1, 1);
            DateTo = new DateTime(DateTime.Today.Year, 1, 1);
            SaveCard = new DelegateCommand(a=> CanExecuteSave(), a =>
            {
                ExecuteSave();
                RefreshConnectedViewModels();
                OnCardSaved();
                IsDataChanged = false;
                AfterExecuteSave();
            });
           // ReportType = type;
        }

        public override bool CanExecuteSave()
        {
            return IsValid();
        }


        protected virtual bool IsValid()
        {
            return "DateFrom|DateTo".Split('|').All(f => string.IsNullOrEmpty(this[f]));
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "DateFrom": return DateFrom.ValidateRequired() ?? DateFrom.ValidatePeriodStart(DateTo);
                    case "DateTo": return DateTo.ValidateRequired() ?? DateTo.ValidatePeriodEnd(DateFrom);
                }
                return null;
            }
        }

        protected void Close()
        {
            RequestClose?.Invoke(null, EventArgs.Empty);
        }
    }
}
