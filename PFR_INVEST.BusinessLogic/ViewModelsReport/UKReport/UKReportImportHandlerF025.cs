﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF025 : UKReportImportHandlerBase
    {
        public UKReportImportHandlerF025()
        {
            Mask = "*F025.xml";
            Shema = "F025.xsd";

        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F025 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {

            var edo = new EDO_ODKF025();
            if (string.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);

            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }


            var serializer = new XmlSerializer(typeof(EDO_ODKF025));

            //XmlReaderSettings settings = new XmlReaderSettings();
            //settings.ValidationType = ValidationType.Schema;
            //settings.Schemas.Add(string.Empty, DocumentBase.GetShema(Shema));

            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                var tmpName = Path.GetTempFileName();
                File.Copy(fileName, tmpName, true);
                var content = File.ReadAllText(tmpName, Encoding.GetEncoding("Windows-1252"));
                content = content.Replace("<total_amount></total_amount>", "<total_amount>0</total_amount>").Replace("<total_amount/>", "<total_amount>0</total_amount>");
                File.WriteAllText(tmpName, content, Encoding.GetEncoding("Windows-1252"));
                using (var reader = XmlReader.Create(tmpName, GetReaderSettings()))
                {

                    var doc = (EDO_ODKF025)serializer.Deserialize(reader);

                    doc.TrimAllStringProperties();

                    if (Items.Any(i => (i as ImportEdoOdkF025).edoLog.DocRegNumberOut.ToLower().Equals(doc.RegNumberOut.ToLower())))
                    {
                        return ActionResult.Error($"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                    }


                    var contract = GetContractByDogovorNum(doc.title.dogovor_num);

                    if (contract == null)
                    {
                        return ActionResult.Error($"Договор с регистрационным номером {doc.title.dogovor_num} не найден");

                    }


                    var xdoc = XDocument.Load(tmpName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    var IdDelete = GetIdReportloaded(xdoc.Root.Name.ToString(), doc.RegNumberOut);
                    var importEdoOdkF025 = new ImportEdoOdkF025();


                    if (IdDelete != null)
                    {

                        if (!ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Отчет по форме F025\nс регистрационным номером {doc.RegNumberOut} существует.\nУдалить старый и импортировать новый"))
                        {
                            return ActionResult.Success();
                        }

                    }

                    importEdoOdkF025.xmlBody = xdoc.ToString();
                    var rDate = DateUtil.ParseImportDate(doc.title.report_date);
                    importEdoOdkF025.F025 = new EdoOdkF025Hib
                    {
                                 ID = IdDelete == null ? 0 : (long)IdDelete,
                                 ContractId = contract.ID,
                                 ReportOnDate = DateUtil.ParseImportDate(doc.title.report_on_date),
                                 Name = doc.title.promoter.name,
                                 INN = doc.title.promoter.inn,
                                 KPP = doc.title.promoter.kpp,
                                 ReportDate = rDate,
                                 ReportTime = rDate.HasValue ? rDate.Value.TimeOfDay : (TimeSpan?)null,
                                 TotalAmount = doc.total_amount,
                                 Post = doc.authorized_persons.manage_company_person.post,
                                 ManageName = doc.authorized_persons.manage_company_person.name,
                                 Group1 = doc.group1.total,
                                 Group2 = doc.group2.total,
                                 Group3 = doc.group3.total,
                                 Group4 = doc.group4.total,
                                 Group5 = doc.group5.total,
                                 Group6 = doc.group6.total,
                                 Group7 = doc.group7.total,
                                 Group8 = doc.group8.total,
                                 Group81 = doc.group8_1.total,
                                 Group9 = doc.group9.total,
                                 Group10 = doc.group10.total,
                                 Group11 = doc.group11.total,
                                 Group12 = doc.group12.total,
                                 Group13 = doc.group13.total,
                                 Subgroup1 = doc.group13.subGroup1.total,
                                 Subgroup2 = doc.group13.subGroup2.total,
                                 Subgroup3 = doc.group13.subGroup3.total
                             };



                    if (doc.group1 != null)
                    {
                        importEdoOdkF025.F025.F025Groups1 = new List<F025Group1Hib>();
                        foreach (var lines in doc.group1.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups1.Add(new F025Group1Hib
                            {
                                        Account = lines.account,
                                        BankName = lines.bank_name,
                                        Amount = lines.amount,
                                        DocDate = DateUtil.ParseImportDate(lines.doc_date),
                                        DocNum = lines.doc_num
                                    });
                        }

                    }

                    if (doc.group2 != null)
                    {
                        importEdoOdkF025.F025.F025Groups2 = new List<F025Group2Hib>();
                        foreach (var lines in doc.group2.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups2.Add(new F025Group2Hib
                            {
                                Account = lines.account,
                                BankName = lines.bank_name,
                                Amount = lines.amount,
                                DocDate = DateUtil.ParseImportDate(lines.doc_date),
                                DocNum = lines.doc_num,
                                Interest = lines.interest
                            });

                        }

                    }

                    if (doc.group3 != null)
                    {
                        importEdoOdkF025.F025.F025Groups3 = new List<F025Group3Hib>();
                        foreach (var lines in doc.group3.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups3.Add(new F025Group3Hib
                            {
                                         MarketPrice = lines.market_price,
                                         Amount = lines.amount,
                                         MarketPriceSource = lines.market_price_source,
                                         Quantity = lines.quantity,
                                         SecurityClassification = lines.security_classification,
                                         StateRegNum = lines.state_reg_num
                                     });

                        }

                    }

                    if (doc.group4 != null)
                    {
                        importEdoOdkF025.F025.F025Groups4 = new List<F025Group4Hib>();
                        foreach (var lines in doc.group4.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups4.Add(new F025Group4Hib
                            {
                                Amount = lines.amount,
                                AcquisitionPrice = lines.acquisition_price,
                                Quantity = lines.quantity,
                                SecurityClassification = lines.security_classification,
                                StateRegNum = lines.state_reg_num
                            });

                        }
                    }

                    if (doc.group5 != null)
                    {
                        importEdoOdkF025.F025.F025Groups5 = new List<F025Group5Hib>();
                        foreach (var lines in doc.group5.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups5.Add(new F025Group5Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                SecurityClassification = lines.security_classification,
                                StateRegNum = lines.state_reg_num
                            });

                        }
                    }


                    if (doc.group6 != null)
                    {
                        importEdoOdkF025.F025.F025Groups6 = new List<F025Group6Hib>();
                        foreach (var lines in doc.group6.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups6.Add(new F025Group6Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                SecurityClassification = lines.security_classification,
                                StateRegNum = lines.state_reg_num,
                                MarketPriceSource = lines.market_price_source,
                                Region = lines.subject_rf
                            });

                        }
                    }


                    if (doc.group7 != null)
                    {
                        importEdoOdkF025.F025.F025Groups7 = new List<F025Group7Hib>();
                        foreach (var lines in doc.group7.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups7.Add(new F025Group7Hib
                            {
                                     Amount = lines.amount,
                                     MarketPrice = lines.market_price,
                                     Quantity = lines.quantity,
                                     StateRegNum = lines.state_reg_num,
                                     MarketPriceSource = lines.market_price_source,
                                     MunicipalName = lines.municipal_name
                                 });

                        }

                    }


                    if (doc.group8 != null)
                    {
                        importEdoOdkF025.F025.F025Groups8 = new List<F025Group8Hib>();
                        foreach (var lines in doc.group8.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups8.Add(new F025Group8Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                StateRegNum = lines.state_reg_num,
                                MarketPriceSource = lines.market_price_source,
                                IssuerName = lines.issuer_name
                            });

                        }
                    }

                    if (doc.group8_1 != null)
                    {
                        importEdoOdkF025.F025.F025Groups81 = new List<F025Group81Hib>();
                        foreach (var lines in doc.group8_1.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups81.Add(new F025Group81Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                CFI = lines.cfi,
                                ISIN = lines.isin,
                                IssuerName = lines.issuer_name,
                                MarketPriceSource = lines.market_price_source
                            });

                        }

                    }

                    if (doc.group9 != null)
                    {
                        importEdoOdkF025.F025.F025Groups9 = new List<F025Group9Hib>();
                        foreach (var lines in doc.group9.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups9.Add(new F025Group9Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                StateRegNum = lines.state_reg_num,
                                MarketPriceSource = lines.market_price_source,
                                IssuerName = lines.issuer_name,
                                SecurityCategory = lines.security_category
                            });

                        }
                    }

                    if (doc.group10 != null)
                    {
                        importEdoOdkF025.F025.F025Groups10 = new List<F025Group10Hib>();
                        foreach (var lines in doc.group10.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups10.Add(new F025Group10Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                StateRegNum = lines.state_reg_num,
                                MarketPriceSource = lines.market_price_source,
                                IssuerName = lines.issuer_name
                            });

                        }
                    }

                    if (doc.group11 != null)
                    {
                        importEdoOdkF025.F025.F025Groups11 = new List<F025Group11Hib>();
                        foreach (var lines in doc.group11.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups11.Add(new F025Group11Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                CertificateId = lines.certificate_id,
                                ManageCompanyName = lines.manage_company_name,
                                MarketPriceSource = lines.market_price_source,
                                RulesRegNum = lines.rules_reg_num
                            });

                        }
                    }


                    if (doc.group12 != null)
                    {
                        importEdoOdkF025.F025.F025Groups12 = new List<F025Group12Hib>();
                        foreach (var lines in doc.group12.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025Groups12.Add(new F025Group12Hib
                            {
                                Amount = lines.amount,
                                MarketPrice = lines.market_price,
                                Quantity = lines.quantity,
                                FondManagerName = lines.fond_manager_name,
                                FondName = lines.fond_name,
                                IssueNum = lines.issue_num,
                                SecurityClassification = lines.security_classification
                            });

                        }
                    }

                    if (doc.group13.subGroup1 != null)
                    {
                        importEdoOdkF025.F025.F025SubGroups1 = new List<F025SubGroup1Hib>();
                        foreach (var lines in doc.group13.subGroup1.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025SubGroups1.Add(new F025SubGroup1Hib
                            {
                                Amount = lines.amount,
                                BrokerName = lines.broker_name
                            });

                        }
                    }

                    if (doc.group13.subGroup2 != null)
                    {
                        importEdoOdkF025.F025.F025SubGroups2 = new List<F025SubGroup2Hib>();
                        foreach (var lines in doc.group13.subGroup2.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025SubGroups2.Add(new F025SubGroup2Hib
                            {
                                Amount = lines.amount,
                                IssuerName = lines.issuer_name,
                                StateRegNum = lines.state_reg_num
                            });

                        }
                    }

                    if (doc.group13.subGroup3 != null)
                    {
                        importEdoOdkF025.F025.F025SubGroups3 = new List<F025SubGroup3Hib>();
                        foreach (var lines in doc.group13.subGroup3.lines)
                        {
                            if (lines.amount == 0) continue;
                            importEdoOdkF025.F025.F025SubGroups3.Add(new F025SubGroup3Hib
                            {
                                Amount = lines.amount,
                                DebName = lines.deb_name
                            });

                        }
                    }

                    importEdoOdkF025.edoLog = new EDOLog
                    {
                                                    DocTable = xdoc.Root.Name.ToString(),
                                                    DocForm = xdoc.Root.Name.ToString(),
                                                    Filename = fName,
                                                    DocRegNumberOut = doc.RegNumberOut,
                                                    RegDate = DateTime.Now.Date,
                                                    ContractNum = doc.title.dogovor_num

                                                };

                    importEdoOdkF025.FileName = Path.GetFileName(fileName);
                    importEdoOdkF025.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF025.ReportOnDate = importEdoOdkF025.F025.ReportOnDate == null
                                                        ? string.Empty
                                                        : ((DateTime)importEdoOdkF025.F025.ReportOnDate).ToString(
                                                            "dd.MM.yyyy");
                    importEdoOdkF025.ReportOnDateNative = importEdoOdkF025.F025.ReportOnDate;

                    Items.Add(importEdoOdkF025);
                }

            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    ViewModelBase.DialogHelper.ShowAlert(
                        ex.InnerException.InnerException == null
                            ? $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}"
                            : $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                }

                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(MarketCostSIListViewModel), typeof(MarketCostVRListViewModel), typeof(LoadedODKLogListSIViewModel));
        }

        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {

                foreach (ImportEdoOdkF025 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF025(import.F025, import.edoLog, import.xmlBody);
                }

            }
            catch (Exception ex)
            {

                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }
    }
}
