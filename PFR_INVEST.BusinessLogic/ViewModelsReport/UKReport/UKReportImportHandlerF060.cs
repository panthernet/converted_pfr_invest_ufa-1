﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Integration.XBRLProcessor;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF060 : UKReportImportHandlerBase
    {
        private readonly List<Year> _years;

        public UKReportImportHandlerF060()
        {
            Mask = "*F060.xml";
            Shema = "F060.xsd";
            _years = DataContainerFacade.GetList<Year>();
        }

        public override string GetOpenFileMask(bool isXbrl)
        {
            return isXbrl ? $"Отчет F060 (.xml)|*.xml" : $"Отчет F060 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {

            var edo = new EDO_ODKF060();
            if (string.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);
            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }

            var serializer = new XmlSerializer(typeof(EDO_ODKF060));
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            var orignalFileName = fileName;
           // var isXbrl = File.ReadAllText(fileName).Contains("xbrli:");
            if (isXbrl)
            {
#if WEBCLIENT
                var p = new XbrlParser(fileName, Logger.Instance);
#else
                var p = new XbrlParser(fileName, ViewModelBase.Logger);
#endif
                var result = p.GenerateF060XML();
                if(string.IsNullOrEmpty(result))
                    return ActionResult.Error($"Ошибка преобразования XBRL файла {fileName}!");
                fileName = Path.GetTempFileName();
                File.WriteAllText(fileName, result, Encoding.GetEncoding("windows-1251"));
            }

            try
            {
                //throw new Exception("XXXX");
                using (var reader = XmlReader.Create(fileName, GetReaderSettings()))
                {
                    var doc = (EDO_ODKF060)serializer.Deserialize(reader);
                    if (string.IsNullOrEmpty(doc.RegNumberOut))
                        doc.RegNumberOut = "Не указан";
                    //if (Items.Any(i => (i as ImportEdoOdkF060).edoLog.DocRegNumberOut.ToLower().Equals(doc.RegNumberOut.ToLower())))
                    if (Items.Cast<ImportEdoOdkF060>().Any(a => a.F060.YearId.ToString().Equals(doc.Year, StringComparison.OrdinalIgnoreCase) &&
                                                                a.F060.Quartal == doc.Quartal && a.edoLog.ContractNum.Equals(doc.DogovorNumber, StringComparison.OrdinalIgnoreCase)))
                    {
                       // return ActionResult.Error($"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                        return ActionResult.Error($"Документ для договора {doc.DogovorNumber} от {doc.Year} года {doc.Quartal} квартала уже загружен для импорта");
                    }

                    doc.TrimAllStringProperties();
                    var contract = GetContractByDogovorNum(doc.DogovorNumber);
                    var ar = IsContractValidate(contract, doc);
                    if (!ar.IsSuccess)
                    {
                        return ar;
                    }

                    var xdoc = XDocument.Load(fileName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    var idDelete = GetDuplicateLoadedReportId(xdoc.Root.Name.ToString(), Convert.ToInt32(doc.Quartal), _years.FirstOrDefault(y => y.Name == doc.Year).ID, doc.DogovorNumber);
                    var importEdoOdkF060 = new ImportEdoOdkF060();
                    if (idDelete != null)
                    {
                        if (!ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Отчет по форме F060\nдля договора {doc.DogovorNumber} от {doc.Year} года {doc.Quartal} квартала существует.\nУдалить старый и импортировать новый?"))
                        {
                            return ActionResult.Success();
                        }
                    }

                    importEdoOdkF060.xmlBody = xdoc.ToString();
                    importEdoOdkF060.F060 = new EdoOdkF060
                    {

                        ID = idDelete ?? 0,
                        RegDate = DateTime.Now.Date,
                        ContractId = contract.ID,
                        YearId = _years.FirstOrDefault(y => y.Name == doc.Year).ID,
                        Detained1 = doc.Detained1,
                        Detained2 = doc.Detained2,
                        PaymentTransfer1 = doc.PaymentTransfer1,
                        PaymentTransfer2 = doc.PaymentTransfer2,
                        PaymentReceived1 = doc.PaymentReceived1,
                        PaymentReceived2 = doc.PaymentReceived2,
                        Profit1 = doc.Profit1,
                        Profit2 = doc.Profit2,
                        SchaEnd1 = doc.SCHAEnd1,
                        SchaEnd2 = doc.SCHAEnd2,
                        DetainedUKReward1 = doc.DetainedUKreward1,
                        DetainedUKReward2 = doc.DetainedUKreward2,
                        SchaStart1 = doc.SCHAStart1,
                        SchaStart2 = doc.SCHAStart2,
                        Quartal = Convert.ToInt32(doc.Quartal),
                        InvestCaseName = doc.InvestcaseName,
                        Kpp = doc.KPP,
                        RegNumberOut = doc.RegNumberOut

                    };

                    importEdoOdkF060.edoLog = new EDOLog
                    {
                        DocTable = xdoc.Root.Name.ToString(),
                        DocForm = xdoc.Root.Name.ToString(),
                        Filename = fName,
                        DocRegNumberOut = doc.RegNumberOut,
                        RegDate = DateTime.Now.Date,
                        ContractNum = doc.DogovorNumber

                    };

                    importEdoOdkF060.FileName = Path.GetFileName(orignalFileName);
                    importEdoOdkF060.PathName = Path.GetDirectoryName(orignalFileName);
                    importEdoOdkF060.ReportOnDate = $"{doc.Quartal} квартал {doc.Year}";
                    importEdoOdkF060.ReportOnDateNative = new DateTime(Convert.ToInt32(doc.Year), Convert.ToInt32(doc.Quartal) * 3 - 2, 1);

                    Items.Add(importEdoOdkF060);
                }
            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    ViewModelBase.DialogHelper.ShowAlert(
                        ex.InnerException.InnerException == null
                            ? $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}"
                            : $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                }
                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }

        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {
                foreach (ImportEdoOdkF060 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF060(import.F060, import.edoLog, import.xmlBody);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(F60VRListViewModel), typeof(F60SIListViewModel), typeof(LoadedODKLogListSIViewModel));
        }

        private ActionResult IsContractValidate(Contract contract, EDO_ODKF060 doc)
        {
            if (contract == null)
            {
                return ActionResult.Error($"Договор с регистрационным номером {doc.DogovorNumber} не найден");
            }
            var le = contract.GetLegalEntity();
            if (le == null)
            {
                return ActionResult.Error($"Для договора с регистрационным номером {doc.DogovorNumber}.\nНе найдено юридическое лицо ");
            }
            if (le.INN != doc.UKINN)
            {
                return ActionResult.Error($"ИНН договора {doc.DogovorNumber} не соответствует ожидаемому.\nОжидалось {le.INN}\nФактически {doc.UKINN}");
            }

            return ActionResult.Success();
        }
    }
}
