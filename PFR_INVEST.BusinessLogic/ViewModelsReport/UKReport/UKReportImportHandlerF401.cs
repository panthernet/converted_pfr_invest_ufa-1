﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF401 : UKReportImportHandlerBase
    {
        //private List<EdoOdkF401402> Requirements;

        public UKReportImportHandlerF401()
        {
            Mask = "*F401.xml";
            Shema = "F401.xsd";

            // Requirements = DataContainerFacade.GetList<EdoOdkF401402>();
        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F401 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {

            EDO_ODKF401 edo = new EDO_ODKF401();
            if (String.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            string fName = Path.GetFileName(fileName);

            if (Items.Any(i => string.Equals(i.FileName, fName, StringComparison.OrdinalIgnoreCase)))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }


            XmlSerializer serializer = new XmlSerializer(typeof(EDO_ODKF401));

            //XmlReaderSettings settings = new XmlReaderSettings();
            //settings.ValidationType = ValidationType.Schema;
            //settings.Schemas.Add(string.Empty, DocumentBase.GetShema(Shema));

            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            CultureInfo ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (XmlReader reader = XmlReader.Create(fileName, GetReaderSettings()))
                {

                    var doc = (EDO_ODKF401)serializer.Deserialize(reader);
                    doc.TrimAllStringProperties();

                    doc.Details = doc.Details.Where(d => !string.IsNullOrEmpty(d.DogPFRNum)).ToList();

                    if (doc.Details.Count == 0)
                    {
                        return ActionResult.Error(
                            $"Документ с регистрационным номером {doc.RegNumberOut}\nне содержит информации к требованию на оплату.");
                    }

                    if (Items.Any(i => string.Equals((i as ImportEdoOdkF401402).edoLog.DocRegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                    {
                        return ActionResult.Error(
                            $"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                    }


                    if (Items.Any(i => string.Equals((i as ImportEdoOdkF401402).edoLog.DocRegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                    {
                        return ActionResult.Error(
                            $"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                    }

                    if (
                        Items.Any(
                            item =>
                            (item as ImportEdoOdkF401402).F401402.FromDate == DateUtil.ParseImportDate(doc.FromDate) &&
                            (item as ImportEdoOdkF401402).F401402.ToDate == DateUtil.ParseImportDate(doc.ToDate) &&
                            (item as ImportEdoOdkF401402).F401402UK.Any(uk => doc.Details.Select(d => d.DogPFRNum).Contains(uk.ContractNumber)))
                        )
                    {
                        return ActionResult.Error(
                            $"Данные из файла {fName}\nдля одного или некольких договоров\nс периодом оплаты с {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\n уже загружены для импорта");
                    }

                    var requirements = DataContainerFacade.GetList<EdoOdkF401402>();
                    
                    if (
                        requirements.Any(r => string.Equals(r.RegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                    {
                        return ActionResult.Error(
                            $"Документ с регистрационным номером {doc.RegNumberOut} уже был импортирован");

                    }

                    requirements = requirements.Where(r => DateUtil.IsDateEquals(r.FromDate, DateUtil.ParseImportDate(doc.FromDate)) && DateUtil.IsDateEquals(r.ToDate, DateUtil.ParseImportDate(doc.ToDate))).ToList();


                    XDocument xdoc = XDocument.Load(fileName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    //                    long? IdDelete = this.GetIdReportloaded(xdoc.Root.Name.ToString(), doc.RegNumberOut);
                    ImportEdoOdkF401402 importEdoOdkF401402 = new ImportEdoOdkF401402();


                    importEdoOdkF401402.xmlBody = xdoc.ToString();
                    importEdoOdkF401402.F401402 = new EdoOdkF401402
                    {
                        ReportDate = DateUtil.ParseImportDate(doc.ReportDate),
                        RegDate = DateTime.Now.Date,
                        AuthorizedPerson = doc.AuthorizedPerson,
                        Executor = doc.Executor,
                        FromDate = DateUtil.ParseImportDate(doc.FromDate),
                        ToDate = DateUtil.ParseImportDate(doc.ToDate),
                        RegNumberOut = doc.RegNumberOut
                    };


                    foreach (var info in doc.Details)
                    {
                        Contract contract = GetContractByDogovorNum(info.DogPFRNum);

                        if (contract == null)
                        {
                            return ActionResult.Error($"Договор с регистрационным номером {info.DogPFRNum} не найден");
                        }

                        if (requirements.Count > 0)
                        {
                            var lists = requirements.Select(r => r.GetDetailList());

                            if (lists.Any(item => item.Any(i => i.ContractId == contract.ID)))
                            {
                                return ActionResult.Error(
                                    $"Для договора с регистрационным номером {info.DogPFRNum}\nтребование на оплату с {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\nуже существует");

                            }
                        }

                        importEdoOdkF401402.F401402UK.Add(new F401402UK
                        {
                            ContractId = contract.ID,
                            StatusID = 1,
                            DogSDNum = info.DogSDNum,
                            DogSDDate = DateUtil.ParseImportDate(info.DogSDDate),
                            AverageScha = info.AverageSCHA,
                            NewTransh = info.NewTransh,
                            PayFromAverage = info.PayFromAverage,
                            PayFromTransh = info.PayFromTransh,
                            Itogo = info.Itogo,
                            OutgoingDocDate = DateUtil.ParseImportDate(doc.ReportDate),
                            OutgoingDocNumber = doc.RegNumberOut,
                            ContractNumber = info.DogPFRNum
                        });
                    }

                    importEdoOdkF401402.edoLog = new EDOLog
                    {
                        DocTable = xdoc.Root.Name.ToString(),
                        DocForm = xdoc.Root.Name.ToString(),
                        Filename = fName,
                        DocRegNumberOut = doc.RegNumberOut,
                        RegDate = DateTime.Now.Date
                    };

                    importEdoOdkF401402.FileName = Path.GetFileName(fileName);
                    importEdoOdkF401402.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF401402.ReportOnDate = importEdoOdkF401402.F401402.ReportDate?.ToString("dd.MM.yyyy") ?? string.Empty;
                    importEdoOdkF401402.ReportOnDateNative = importEdoOdkF401402.F401402.ReportDate;
                    
                    Items.Add(importEdoOdkF401402);
                }

            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                    else
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                }
                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }

        public override ActionResult Import()
        {
            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            CultureInfo ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            ActionResult actionResult = ActionResult.Success();
            try
            {

                foreach (var ukReportImportItem in Items)
                {
                    var import = (ImportEdoOdkF401402) ukReportImportItem;
                    actionResult = BLServiceSystem.Client.ImportUKF401(import.F401402, import.F401402UK, import.edoLog, import.xmlBody);
                }
            }
            catch (Exception ex)
            {

                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(F401402UKListViewModel), typeof(LoadedODKLogListSIViewModel));
        }

        private DateTime? GetDateTime(string date)
        {
            DateTime dt;
            if (DateTime.TryParse(date, out dt))
            {
                return dt;
            }
            return null;

        }
    }
}
