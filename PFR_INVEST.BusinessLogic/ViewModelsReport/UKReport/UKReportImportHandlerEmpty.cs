﻿using System;
using PFR_INVEST.Core.BusinessLogic;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerEmpty : UKReportImportHandlerBase
    {

       
        public override bool CanExecuteOpenFile()
        {
            return false;
        }

        public override ActionResult Import()
        {
            throw new NotImplementedException();
        }

        public override void RefreshViewModels()
        {
            throw new NotImplementedException();
        }

        public override string GetOpenFileMask(bool b)
        {
            throw new NotImplementedException();
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {
            throw new NotImplementedException();
        }
    }
}
