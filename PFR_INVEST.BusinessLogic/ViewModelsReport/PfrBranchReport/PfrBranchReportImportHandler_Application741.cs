﻿using System;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_Application741 : PfrBranchReportImportHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.Applications741;

        const int DATA_ROW_NUMBER = 13;
        private const string Mask = "SRP_07_*.xls;SRP_07_*.xlsx";

        public override string GetOpenDirectoryMask()
        {
            return Mask;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет по региону SRP_07_(.xls, .xlsx)|{0}", Mask);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            PfrBranchReport r;

            if (TryParseFileName(fileName, out r))
            {
                errorMessage = null;
                return true;
            }
            errorMessage = string.Format("Некорректное имя файла '{0}'", fileName);
            return false;
        }

        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            PfrBranchReportImportContent_Application741 x = new PfrBranchReportImportContent_Application741();
            x.PfrBranchReport = r;

            if (LoadFile(fileName, LoadData, x))
            {
                Items.Add(x);
                return true;
            }
            return false;
        }

        public void LoadData(Worksheet worksheet, PfrBranchReportImportContentBase paramter, out bool isloadFileCorrect)
        {
            PfrBranchReportImportContent_Application741 x = (PfrBranchReportImportContent_Application741)paramter;

            x.Application741 = new PfrBranchReportApplication741
            {
                AppealDistribution = Convert.ToInt64(((Range)worksheet.UsedRange.Cells[DATA_ROW_NUMBER, 3]).Value),
                AppealPaymentSummary = Convert.ToInt64(((Range)worksheet.UsedRange.Cells[DATA_ROW_NUMBER, 4]).Value),
                AppealPaymentCourt = Convert.ToInt64(((Range)worksheet.UsedRange.Cells[DATA_ROW_NUMBER, 5]).Value),
                AppealRefuse = Convert.ToInt64(((Range)worksheet.UsedRange.Cells[DATA_ROW_NUMBER, 6]).Value),

                ResolutionPayment = Convert.ToInt64(((Range)worksheet.UsedRange.Cells[DATA_ROW_NUMBER, 7]).Value),
                ResolutionPaymentAdditional = Convert.ToInt64(((Range)worksheet.UsedRange.Cells[DATA_ROW_NUMBER, 8]).Value),
                ResolutionPaymentRefuse = Convert.ToInt64(((Range)worksheet.UsedRange.Cells[DATA_ROW_NUMBER, 9]).Value)
            };

            isloadFileCorrect = true;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

    }
}
