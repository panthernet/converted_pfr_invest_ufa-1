﻿using System;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
	public class PfrBranchReportImportXmlHandler_Empty : PfrBranchReportImportXmlHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.Empty;

        public override bool CanExecuteImport()
        {
            return false;
        }

        public override PfrBranchReportImportResponse DoImport()
        {
            throw new NotImplementedException();
        }
    }
}
