﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportExportHandler_CashExpenditures : PfrBranchReportExportHandlerBase
    {
        private RegionReportExportDlgViewModel Model;

        public PfrBranchReportExportHandler_CashExpenditures()
        {
        }

        public PfrBranchReportExportHandler_CashExpenditures(RegionReportExportDlgViewModel model)
            : this()
        {
            Model = model;
        }

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.CashExpenditures;

        public override bool CanExecuteExport()
        {
            if (Model.SelectedYear == null || !Model.Validate())
                return false;

            return true;
        }


        public override void ExecuteExport()
        {
            if (!CanExecuteExport())
                return;

            ExecuteExport(Model.SelectedYear.Value, Model.Responsible, Model.Performer);
        }

        public void ExecuteExport(int periodYear, Person responsible, Person performer)
        {
            var x = new PrintCashExpenditures
            {
                PeriodYear = periodYear,
                Responsible = responsible,
                Performer = performer
            };
            x.LoadData();


            // if we don't have complete data for report, ask user if he wants to make report
            if (x.IsReportDataIncomplete() && Model != null && !Model.AskReportDataIncomplete(x.GetDataIncompleteMessage()))
                return;

            x.Print(true);
        }


        /// <summary>
        /// This report supports annual reports only
        /// </summary>
        public override bool IsAnnual
        {
            get { return true; }
            set
            {                //base.IsAnnual = value;
            }
        }
    }
}
