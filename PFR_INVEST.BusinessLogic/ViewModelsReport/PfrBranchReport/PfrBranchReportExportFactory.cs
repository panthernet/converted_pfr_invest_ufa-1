﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportExportFactory
    {
        public static PfrBranchReportExportHandlerBase GetExportHandler(RegionReportExportDlgViewModel model)
        {
            switch (model.SelectedReportType.ReportType)
            {
                case PfrBranchReportType.BranchReportType.Empty:
                    return new PfrBranchReportExportHandler_Empty(model);
                case PfrBranchReportType.BranchReportType.CashExpenditures:
                    return new PfrBranchReportExportHandler_CashExpenditures(model);                               
                case PfrBranchReportType.BranchReportType.InsuredPerson:
                    return new PfrBranchReportExportHandler_InsuredPerson(model);
                case PfrBranchReportType.BranchReportType.Applications741:
                    return new PfrBranchReportExportHandler_Application741(model);
                case PfrBranchReportType.BranchReportType.AssigneePayment:
                    return new PfrBranchReportExportHandler_AssigneePayment(model);
                case PfrBranchReportType.BranchReportType.CertificationAgreement:
                    return new PfrBranchReportExportHandler_CertificationAgreement(model);                               

                case PfrBranchReportType.BranchReportType.DeliveryCost:
                    return new PfrBranchReportExportHandler_DeliveryCost(model);

                case PfrBranchReportType.BranchReportType.NpfNotice:
                    return new PfrBranchReportExportHandler_NpfNotice(model);

                case PfrBranchReportType.BranchReportType.SignatureContract:
                    return new PfrBranchReportExportHandler_SignatureContract(model);

                default:
                    return new PfrBranchReportExportHandler_Empty(model);
                    //throw new NotImplementedException();
            }
        }

    }
}
