﻿using System;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_AssigneePayment : PfrBranchReportImportHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.AssigneePayment;

        private const string Mask = "SRP_04_*.xls;SRP_04_*.xlsx";

        public override string GetOpenDirectoryMask()
        {
            return Mask;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет о средствах пенсионных накоплений SRP_04_(.xls, .xlsx)|{0}", Mask);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            PfrBranchReport r;

            if (TryParseFileName(fileName, out r))
            {
                errorMessage = null;
                return true;
            }
            errorMessage = string.Format("Некорректное имя файла '{0}'", fileName);
            return false;
        }

        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            PfrBranchReportImportContent_AssigneePayment x = new PfrBranchReportImportContent_AssigneePayment();
            x.PfrBranchReport = r;

            if (LoadFile(fileName, LoadData, x))
            {
                Items.Add(x);
                return true;
            }
            return false;
        }

        public void LoadData(Worksheet worksheet, PfrBranchReportImportContentBase parameter, out bool isloadFileCorrect)
        {
            PfrBranchReportImportContent_AssigneePayment x = (PfrBranchReportImportContent_AssigneePayment)parameter;

            x.AssigneePayment = new PfrBranchReportAssigneePayment
            {
                TotalPayment        = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[15, 4]).Value),
                OwnReservePayment   = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[17, 4]).Value),
                PFRReservePayment  = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[18, 4]).Value)
            };

            isloadFileCorrect = true;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

    }
}
