﻿using System;
using Microsoft.Office.Interop.Excel;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    internal static class SheetExtensions
    {
        public static long CellValue(this Worksheet sheet, long row, long col)
        {
            return long.Parse(sheet.Range[sheet.Cells[row, col], sheet.Cells[row, col]].Value2.ToString());
        }

        public static void SetValue(this Worksheet sheet, long row, long col, object value)
        {
            sheet.Range[sheet.Cells[row, col], sheet.Cells[row, col]].Value2 = value;
        }

        public static void SetValue(this Worksheet sheet, long row, long col, object value, String style)
        {
            Range range = sheet.Range[sheet.Cells[row, col], sheet.Cells[row, col]];
            range.Value2 = value;
            if (!String.IsNullOrEmpty(style))
                range.Style = style;
        }

        public static void SetValue(this Range range,  object value, String style) {
            range.Value2 = value;
            if (!String.IsNullOrEmpty(style))
                range.Style = style;
        }

        public static void SetFormula(this Worksheet sheet, long row, long col, object value)
        {
            sheet.Range[sheet.Cells[row, col], sheet.Cells[row, col]].Formula = value;
        }

        public static Range GetCellRange(this Worksheet sheet, long row, long col)
        {
            return sheet.Range[sheet.Cells[row, col], sheet.Cells[row, col]];
        }
    }
}
