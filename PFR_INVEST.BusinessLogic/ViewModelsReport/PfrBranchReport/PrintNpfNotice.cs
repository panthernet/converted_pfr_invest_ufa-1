﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PrintNpfNotice : PrintBase
    {
        public int PeriodYear { get; set; }
        public int? PeriodMonth { get; set; }
        public Person Responsible { get; set; }
        public Person Performer { get; set; }


        DataContainer Data;


        public PrintNpfNotice()
        {
            Name = "Отчет о поступивших в ПФР уведомлениях НПФ";
            TemplateName = Name + ".xlsx";
        }

        protected override void FillData()
        {
            Fields.Add("REPORT_DATE", GetReportDateWords(PeriodYear, PeriodMonth));

            PrintResponsiblePerformer(Responsible, Performer);
        }


        public override void GenerateDocument(Worksheet s)
        {
            PrintHelper ph = new PrintHelper(s);
            Data.Print(ph);
        }

        public void LoadData()
        {
            Data = new DataContainer();
            Data.LoadData(PeriodYear, PeriodMonth);
        }

        internal bool IsReportDataIncomplete()
        {
            return Data.IsDataIncomplete();
        }

        internal string GetDataIncompleteMessage()
        {
            List<PFRBranch> xL = Data.GetDataIncompleteRegions();
            return GetDataIncompleteMessage(xL);
        }

        class PrintHelper
        {
            public PrintHelper()
            { }

            public PrintHelper(Worksheet s)
                : this()
            {
                S = s;
                ItemCount = 0;
                RowNumber = 10;
            }

            public Worksheet S;
            public int RowNumber;
            public int ItemCount;

            internal void AddEmptyRows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "K" + (RowNumber - 1 + count));
                r.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);
            }

            private Range GetRow()
            {
                Range r = S.get_Range("A" + RowNumber, "K" + RowNumber);
                RowNumber++;
                return r;
            }

            private Range GetNumRowTotal()
            {
                Range r = S.get_Range("D8", "K8");
                return r;
            }

            private void SkipRows(int count)
            {
                RowNumber += count;
            }

            private Range GetNumRows(int count)
            {
                Range r = S.get_Range("D" + RowNumber, "K" + (RowNumber + count - 1));
                return r;
            }

            private Range GetARows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "A" + (RowNumber + count - 1));
                return r;
            }

            private Range GetBRows(int count)
            {
                Range r = S.get_Range("B" + RowNumber, "B" + (RowNumber + count - 1));
                return r;
            }

            private Range GetCRows(int count)
            {
                Range r = S.get_Range("C" + RowNumber, "C" + (RowNumber + count - 1));
                return r;
            }

            private Range GetABRows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "B" + (RowNumber + count - 1));
                return r;
            }

            private Range GetCKRows(int count)
            {
                Range r = S.get_Range("C" + RowNumber, "K" + (RowNumber + count - 1));
                return r;
            }

            private Range GetRows(string colFrom, string colTo, int count)
            {
                Range r = S.get_Range(colFrom + RowNumber, colTo + (RowNumber + count - 1));
                return r;
            }

            private Range AddRows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "K" + (RowNumber + count - 1));
                RowNumber += count;
                return r;
            }


            internal void Print(DataContainer x)
            {
                Range r;
                object[] a;

                r = GetNumRowTotal();
                a = x.S.GetRowNumbers();
                r.Value2 = a;
            }



            internal void Print(FedoContainer x)
            {
                Range r;

                if (x.Fedo.Special == 1)
                {
                    r = GetRow();
                    r.Style = "PfrSplitter";
                    return;
                }

                object[] a;

                r = GetABRows(1);
                r.Merge();
                r.Value2 = x.Fedo.Name;
                r.Style = "PfrFedo";


                r = GetNumRows(1);
                a = x.S.GetRowNumbers();
                r.Value2 = a;
                r.Style = "PfrFedoNumberTotal";

                SkipRows(1);
            }

            internal void Print(BranchContainer x)
            {
                Range r;
                //Range r2;
                //object[] a;
                object[,] aa;
                int nr = x.NpfList.Count;

                int n = nr == 0 ? 1 : nr;

                ItemCount++;
                r = GetARows(n + 1);
                r.Merge();
                r.Value2 = ItemCount;
                r.Style = "PfrBranchIndex";


                r = GetBRows(n + 1);
                r.Merge();
                r.Value2 = x.Branch.Name;
                r.Style = "PfrBranch";



                if (nr > 0)
                {
                    //Titles
                    r = GetCRows(nr);
                    aa = new object[nr, 1];
                    for (int j = 0; j < nr; j++)
                        aa[j, 0] = x.NpfList[j].Npf.FormalizedName;

                    r.Value2 = aa;
                    r.Style = "PfrNpf";


                    r = GetNumRows(nr);
                    aa = new object[nr, 8];

                    int i = 0;

                    //npf
                    foreach (var npf in x.NpfList)
                    {
                        npf.S.SetRow(i, aa);
                        i++;
                    }

                    r.Value2 = aa;
                    r.Style = "PfrNumber";
                }
                else
                {
                    r = GetCKRows(1);
                    r.Style = "PfrNpfEmpty";
                }

                SkipRows(n);


                //Last row of branch: total
                r = GetCRows(1);
                r.Value2 = "Итого:";
                r.Style = "PfrBranchTotal";


                r = GetNumRows(1);
                r.Value2 = x.S.GetRowNumbers();
                r.Style = "PfrNumberTotal";
                SkipRows(1);
            }

            private void Print(NpfContainer n)
            {
                //throw new NotImplementedException();
            }
        }


        class Sum
        {
            public const int ARRAY_SIZE = 6;

            public decimal[] s = new decimal[ARRAY_SIZE];

            public decimal NoticeTotal => NoticeNew + NoticeExisting;

            public decimal NoticeNew { get { return s[0]; } set { s[0] = value; } }
            public decimal NoticeExisting { get { return s[1]; } set { s[1] = value; } }
            public decimal NoticeToNpf { get { return s[2]; } set { s[2] = value; } }


            public decimal ContractTotal => ContractNew + ContractExisting - ContractToNpfReturned;

            public decimal ContractNew { get { return s[3]; } set { s[3] = value; } }
            public decimal ContractExisting { get { return s[4]; } set { s[4] = value; } }
            public decimal ContractToNpfReturned { get { return s[5]; } set { s[5] = value; } }


            public void Add(Sum x)
            {
                for (int i = 0; i < ARRAY_SIZE; i++)
                    s[i] += x.s[i];
            }

            public void Clear()
            {
                for (int i = 0; i < ARRAY_SIZE; i++)
                    s[i] = 0;
            }

            internal void AddNpfNotice(PfrBranchReportNpfNotice x)
            {
                NoticeNew += x.NoticeNew;
                //NoticeExisting += x.NoticeExisting;
                NoticeToNpf += x.NoticeToNpf;

                ContractNew += x.ContractNew;
                //ContractExisting += x.ContractExisting;
                ContractToNpfReturned += x.ContractToNpfReturned;
            }

            public object[] GetRowNumbers()
            {
                object[] x = new object[8];

                x[0] = NoticeTotal;
                x[1] = ContractTotal;
                x[2] = NoticeNew;
                x[3] = ContractNew;
                x[4] = NoticeExisting;
                x[5] = ContractExisting;
                x[6] = NoticeToNpf;
                x[7] = ContractToNpfReturned;

                return x;
            }

            internal void SetRow(int i, object[,] a)
            {
                a[i, 0] = NoticeTotal;
                a[i, 1] = ContractTotal;
                a[i, 2] = NoticeNew;
                a[i, 3] = ContractNew;
                a[i, 4] = NoticeExisting;
                a[i, 5] = ContractExisting;
                a[i, 6] = NoticeToNpf;
                a[i, 7] = ContractToNpfReturned;
            }
        }

        class DataContainer
        {
            public PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.NpfNotice;

            public DataContainer()
            {
                FedoList = new List<FedoContainer>();
                S = new Sum();
            }

            public List<FedoContainer> FedoList;
            public Sum S;

            public void CalcSum()
            {
                S.Clear();

                foreach (var x in FedoList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }
            }

            internal void LoadData(int periodYear, int? periodMonth)
            {
                bool isAnnual = periodMonth == null;

                List<FEDO> Fedos;
                List<PFRBranch> Branches;
                List<PfrBranchReport> Reports;
                List<PfrBranchReportNpfNotice> ReportData;
                List<LegalEntity> NPFs;

                Fedos = DataContainerFacade.GetList<FEDO>().OrderBy(x => x.ID).ToList();
                Branches = DataContainerFacade.GetList<PFRBranch>().ToList();

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportTypeID", Value = (long)ReportType });
                    pL.Add(new ListPropertyCondition { Name = "PeriodYear", Value = periodYear });
                    if (!isAnnual)
                        pL.Add(new ListPropertyCondition { Name = "PeriodMonth", Value = periodMonth });
                    pL.Add(new ListPropertyCondition { Name = "StatusID", Value = 1L });

                    Reports = DataContainerFacade.GetListByPropertyConditions<PfrBranchReport>(pL).ToList();
                }

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "BranchReportID", Values = (from x in Reports select x.ID).Cast<object>().ToArray(), Operation = "in" });
                    ReportData = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportNpfNotice>(pL).ToList();
                }

                {
                    object[] ids = (from x in ReportData select x.LegalEntityID).Distinct().Cast<object>().ToArray();

                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ID", Values = ids, Operation = "in" });
                    NPFs = DataContainerFacade.GetListByPropertyConditions<LegalEntity>(pL).ToList();
                }

                LoadData(Fedos, Branches, Reports, ReportData, NPFs, isAnnual);
            }

            internal void LoadData(List<FEDO> fedos, List<PFRBranch> branches, List<PfrBranchReport> reports, List<PfrBranchReportNpfNotice> reportData, List<LegalEntity> npfs, bool isAnnual)
            {
                Dictionary<long, FedoContainer> fD = new Dictionary<long, FedoContainer>();

                foreach (var x in fedos.OrderBy(x => x.ID))
                {
                    var y = new FedoContainer { Fedo = x };
                    FedoList.Add(y);
                    fD.Add(x.ID, y);
                }

                Dictionary<long, BranchContainer> bD = new Dictionary<long, BranchContainer>();

                foreach (var x in from y in branches orderby y.RegionNumber, y.Name select y)
                {
                    var y = new BranchContainer { Branch = x };
                    fD[x.FEDO_ID].AddBranch(y);
                    bD.Add(x.ID, y);
                }


                Dictionary<long, LegalEntity> nD = new Dictionary<long, LegalEntity>();

                foreach (var x in npfs)
                {
                    nD.Add(x.ID, x);
                }

                Dictionary<long, PfrBranchReport> rD = new Dictionary<long, PfrBranchReport>();

                foreach (var x in reports)
                {
                    rD.Add(x.ID, x);
                }


                List<ReportDataContainer> rdL = new List<ReportDataContainer>();

                foreach (var x in reportData)
                {
                    var y = new ReportDataContainer { Report = rD[x.BranchReportID], Data = x };
                    rdL.Add(y);
                }

                foreach (var x in from y in rdL orderby y.Report.PeriodYear, y.Report.PeriodMonth select y)
                {
                    var n = nD[x.Data.LegalEntityID];
                    var b = bD[x.Report.BranchID];

                    var nc = b.AddNpf(n, isAnnual);
                    nc.AddReportData(x);
                }

                CalcSum();
            }

            internal void Print(PrintHelper ph)
            {
                int n = GetRowCount();
                ph.AddEmptyRows(n);

                ph.Print(this);

                foreach (var x in FedoList)
                    x.Print(ph);
            }

            private int GetRowCount()
            {
                int n = 0;
                foreach (var x in FedoList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (FedoList == null || FedoList.Count == 0)
                    return true;

                foreach (var x in FedoList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }


            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in FedoList)
                    res.AddRange(x.GetDataIncompleteRegions());

                return res;
            }
        }

        class FedoContainer
        {
            public FedoContainer()
            {
                BranchList = new List<BranchContainer>();
                S = new Sum();
            }

            public FEDO Fedo { get; set; }
            public List<BranchContainer> BranchList { get; set; }
            public Sum S;

            public void CalcSum()
            {
                S.Clear();

                foreach (var x in BranchList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }
            }

            internal void AddBranch(BranchContainer y)
            {
                BranchList.Add(y);
            }

            internal void Print(PrintHelper ph)
            {
                ph.Print(this);
                foreach (var x in BranchList)
                    x.Print(ph);
            }

            internal int GetRowCount()
            {
                int n = 1;

                foreach (var x in BranchList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (BranchList == null || BranchList.Count == 0)
                    return true;

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        res.Add(x.Branch);

                return res;
            }
        }

        class BranchContainer
        {
            public BranchContainer()
            {
                NpfList = new List<NpfContainer>();
                S = new Sum();
            }

            public PFRBranch Branch { get; set; }
            public List<NpfContainer> NpfList { get; set; }
            public Sum S;



            public void CalcSum()
            {
                S.Clear();

                foreach (var x in NpfList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }
            }

            internal void AddItem(NpfContainer r)
            {
                NpfList.Add(r);
            }

            internal void Print(PrintHelper ph)
            {
                ph.Print(this);

                //foreach (var x in NpfList)
                //    x.Print(ph);
            }

            public int GetRowCount()
            {
                int n = 1 + NpfList.Count;
                if (n == 1)
                    n = 2;
                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (NpfList.Count == 0)
                    return true;

                foreach (var x in NpfList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal NpfContainer AddNpf(LegalEntity n, bool isAnnual)
            {
                NpfContainer nc = NpfList.FirstOrDefault(x => x.Npf == n);
                if (nc == null)
                {
                    nc = new NpfContainer { Npf = n, IsAnnual = isAnnual };
                    NpfList.Add(nc);
                }

                return nc;
            }
        }

        class NpfContainer
        {
            public bool IsAnnual;

            public NpfContainer()
            {
                DataList = new List<ReportDataContainer>();
                S = new Sum();
            }

            public LegalEntity Npf;
            public List<ReportDataContainer> DataList;
            public Sum S;

            internal bool IsDataIncomplete()
            {
                if (Npf == null)
                    return true;

                if (DataList.Count != (IsAnnual ? 12 : 1))
                    return true;

                foreach (var x in DataList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            //internal void Print(PrintHelper ph)
            //{
            //    throw new NotImplementedException();
            //}

            internal void AddReportData(ReportDataContainer x)
            {
                DataList.Add(x);
            }

            internal void CalcSum()
            {
                S.Clear();

                foreach (var x in DataList)
                {
                    S.AddNpfNotice(x.Data);
                }

                S.NoticeExisting = DataList[0].Data.NoticeExisting;
                S.ContractExisting = DataList[0].Data.ContractExisting;
            }
        }

        class ReportDataContainer
        {
            public PfrBranchReport Report;
            public PfrBranchReportNpfNotice Data;

            internal bool IsDataIncomplete()
            {
                if (Report == null || Data == null)
                    return true;



                return false;
            }
        }
    }
}
