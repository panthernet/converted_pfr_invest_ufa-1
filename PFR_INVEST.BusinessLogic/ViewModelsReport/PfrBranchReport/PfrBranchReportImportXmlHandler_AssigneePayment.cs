﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportXmlHandler_AssigneePayment : PfrBranchReportImportXmlHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.XmlAssigneePayment;

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

        public override bool LoadFile(string fileName, out string message)
        {
            if (!TryParseFileName(fileName))
            {
                message = string.Format("Некорректное имя файла '{0}'", fileName);
                return false;
            }

            bool isFileLoadedCorrect = LoadFile(fileName);

            message = null;

            return isFileLoadedCorrect;
        }

        public override bool LoadFile(string fileName)
        {
            return LoadFile(fileName, PopulateData);
        }

        private void PopulateData(string fileName, XDocument document, out bool isloadFileCorrect)
        {
            var start = DateTime.Now;

            try
            {
                var convert = document.Element("Convert");
                if (convert != null)
                {
                    var templateName = convert.Element("TemplateName");
                    if (templateName != null)
                    {
                        var templateNameValue = templateName.Value;
                        var nameParts = templateNameValue.Split(new[] { "!!" },
                            StringSplitOptions.RemoveEmptyEntries);
                        templateNameValue = nameParts[2];

                        if (!templateNameValue.ToLower().Contains("правопреемники (итог)"))
                        {
                            throw new PfrBranchReportImportException(
                                string.Format("Выбранный тип отчета не соответствует типу отчета из xml-файла: {0}",
                                    templateNameValue));
                        }
                    }

                    // Разделитель для дробных чисел
                    var decimalSeparator = convert.Element("DecimalSymbol").Value.Trim();
                    var numberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = decimalSeparator };

                    var rows = convert.Descendants("Sheet").First().Descendants("row");

                    // Дата отчета
                    var reportMonth = 0;
                    var reportYear = 0;
                    var startRowId = 13;
                    var lastRowId = rows.Last().Attribute("id").Value.To<int>();
                    var parsedData = new Dictionary<string, PfrBranchReportImportContent_AssigneePayment>();
                    var currentRegionName = "";

                    foreach (var row in rows)
                    {
                        var rowId = row.Attribute("id").Value.To<int>();
                        var columns = row.Descendants("column");

                        // Получение месяца отчета
                        if (rowId == 1)
                        {
                            var monthColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "6");
                            if (monthColumn != null)
                            {
                                var month = DateTools.Months.First(x => x.ToLower() == monthColumn.Value.ToLower());
                                reportMonth = DateTools.Months.ToList().IndexOf(month) + 1;
                            }
                        }
                        // Получение года отчета
                        else if (rowId == 2)
                        {
                            var yearColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "6");
                            if (yearColumn != null)
                            {
                                reportYear = yearColumn.Value.To(0);
                            }
                        }
                        else if (rowId >= startRowId && rowId <= lastRowId)
                        {
                            // Парсим данные
                            // Получаем номер п/п
                            // Наличие региона определяется наличием числового значения в колонке с id=1
                            var columnNumber = columns.FirstOrDefault(x => x.Attribute("id").Value == "1");
                            if (columnNumber != null)
                            {
                                // Проверяем, число ли там
                                if (columnNumber.Value.To(0) > 0)
                                {
                                    var regionName = columns.First(x => x.Attribute("id").Value == "2").Value.Trim();

                                    // Создаем объект отчета
                                    var r = new PfrBranchReport
                                    {
                                        CreatedDate = DateTime.Now,
                                        Branch = GetBranchByName(regionName),
                                        ReportTypeID = (long) ReportType,
                                        FileName = Path.GetFileName(fileName),
                                        PeriodMonth = reportMonth,
                                        PeriodYear = reportYear,
                                        StatusID = 1
                                    };
                                    if (r.PeriodYear < 1900 || r.PeriodYear > 9999)
                                        throw new InvalidDataException("Некорректный год");
                                    if (r.PeriodMonth != null && (r.PeriodMonth > 12 || r.PeriodMonth < 1))
                                        throw new InvalidDataException("Некорректный месяц");

                                    if (r.Branch == null)
                                        throw new InvalidDataException("Некорректное название региона: " + regionName);
                                    r.BranchID = r.Branch.ID;

                                    var report = new PfrBranchReportImportContent_AssigneePayment
                                    {

                                        PfrBranchReport = r,
                                        AssigneePayment = new PfrBranchReportAssigneePayment()
                                    };

                                    currentRegionName = regionName;
                                    parsedData.Add(currentRegionName, report);

                                    // Парсим строку с данными
                                    var reportRow = parsedData[currentRegionName].AssigneePayment;
                                    foreach (var xColumn in columns.Where(x => x.Attribute("id").Value.To<int>() >= 3))
                                    {
                                        var strValue = xColumn.Value;
                                        decimal decValue = 0m;
                                        if (!strValue.IsEmpty())
                                        {
                                            decimal.TryParse(strValue, NumberStyles.Number, numberFormatInfo,
                                                out decValue);
                                        }

                                        switch (xColumn.Attribute("id").Value)
                                        {
                                            case "3":
                                                // Расходы на выплаты правопреемникам умерших застрахованных лиц (КБК 392 10 01 505 13 04 321 КО
                                                reportRow.OwnReservePayment = decValue;
                                                break;

                                            case "4":
                                                // Расходы на выплаты правопреемникам умерших застрахованных лиц за счет резерва ПФР по обязательному пенсионному страхованию (КБК 392 10 01 505 13 04
                                                reportRow.PFRReservePayment = decValue;
                                                break;

                                            case "5":
                                                // Всего по КБК 392 10 01 505 13 04 321
                                                reportRow.TotalPayment = decValue;
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (var item in parsedData)
                    {
                        Items.Add(item.Value);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
                // Debugger.Break();
            }

            var delta = DateTime.Now - start;
            Debug.WriteLine("Parse AssigneePayment Report Duration = " + delta);

            isloadFileCorrect = true;
        }
    }
}
