﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PfrBranchReportExportHandler_InsuredPerson : PfrBranchReportExportHandlerBase
    {
         private RegionReportExportDlgViewModel Model;

        public PfrBranchReportExportHandler_InsuredPerson()
        {
        }

        public PfrBranchReportExportHandler_InsuredPerson(RegionReportExportDlgViewModel model)
            : this()
        {
            Model = model;
        }

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.InsuredPerson;


        public override bool CanExecuteExport()
        {
            if (Model.SelectedYear == null || !Model.Validate())
                return false;

            if (Model.SelectedMonth == null && Model.IsMonthly)
                return false;

            return true;
        }


        public override void ExecuteExport()
        {
            if (!CanExecuteExport())
                return;

            ExecuteExport(Model.SelectedYear.Value, Model.SelectedMonth, Model.Responsible, Model.Performer);
        }

        public void ExecuteExport(int periodYear, int? periodMonth, Person responsible, Person performer)
        {
            var x = new PrintInsuredPerson
            {
                PeriodYear = periodYear,
                PeriodMonth = periodMonth,
                Responsible = responsible,
                Performer = performer
            };
            x.LoadData();


            // if we don't have complete data for report, ask user if he wants to make report
            if (x.IsReportDataIncomplete() && Model != null && !Model.AskReportDataIncomplete(x.GetDataIncompleteMessage()))
                return;

            x.Print(true);
        }

        public override bool IsPeriodLengthVisible => true;

        public override bool IsPeriodLengthEnabled => true;
    }
}
