﻿using System;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_DeliveryCost : PfrBranchReportImportHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.DeliveryCost;

        private const string Mask = "SRP_09_*.xls;SRP_09_*.xlsx";

        public override string GetOpenDirectoryMask()
        {
            return Mask;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет по региону SRP_09_(.xls, .xlsx)|{0}", Mask);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            PfrBranchReport r;

            if (TryParseFileName(fileName, out r))
            {
                errorMessage = null;
                return true;
            }
            errorMessage = string.Format("Некорректное имя файла '{0}'", fileName);
            return false;
        }

        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            PfrBranchReportImportContent_DeliveryCost x = new PfrBranchReportImportContent_DeliveryCost();
            x.PfrBranchReport = r;

            if (LoadFile(fileName, LoadData, x))
            {
                Items.Add(x);
                return true;
            }

            return false;
        }

        public void LoadData(Worksheet worksheet, PfrBranchReportImportContentBase paramter, out bool isloadFileCorrect)
        {
            PfrBranchReportImportContent_DeliveryCost x = (PfrBranchReportImportContent_DeliveryCost)paramter;

            x.DeliveryCost = new PfrBranchReportDeliveryCost
            {
                Sum_221 = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[11, 3]).Value),
                Sum_226 = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[11, 4]).Value),
                Total = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[11, 5]).Value)
            };

            isloadFileCorrect = true;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

    }
}
