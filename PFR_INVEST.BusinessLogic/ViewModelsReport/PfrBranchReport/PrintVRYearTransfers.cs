﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager, DOKIP_ROLE_TYPE.OUFV_directory_editor, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager, DOKIP_ROLE_TYPE.OUFV_directory_editor, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class PrintVRYearTransfers : PrintBase
    {
        public class HierarhyTransferListItem : TransferListItem
        {

        }

        protected Worksheet _sheet;
        public long VrYearTransferRegisterId { get; set; }
        protected List<TransferListItem> items;

        public PrintVRYearTransfers()
        {
            Name = "Вр_финансирование_мастер_годового_плана";
            TemplateName = Name + ".xls";
        }

        public override void GenerateDocument(Worksheet s)
        {
            _sheet = s;
            int printRowNum = 7;
            int startKvartalColumn = 5;

            foreach (var transfer in items.GroupBy(info => info.TransferID)
                        .Select(group => new
                        {
                            Id = group.Key,
                            Count = group.Count()
                        }))
            {

                bool showFirstRow = false;
                startKvartalColumn = 5;

                decimal kvartalPlanSum = 0;
                decimal kvartalFactSum = 0;

                decimal yearPlanSum = 0;
                decimal yearFactSum = 0;

                List<TransferListItem> transferItems = items.Where(item => item.TransferID == transfer.Id).OrderBy(item => item.MonthID).ToList();
                transferItems.ForEach(transferMonthItem =>
                {

                    if (!showFirstRow)
                    {
                        _sheet.SetValue(printRowNum, 1, printRowNum - 6, "pfr_fin_plan_item");// nomer
                        _sheet.SetValue(printRowNum, 2, transferMonthItem.UKFormalizedName, "pfr_fin_plan_item");
                        _sheet.SetValue(printRowNum, 3, transferMonthItem.ContractNumber, "pfr_fin_plan_item");
                        _sheet.SetValue(printRowNum, 4, transferMonthItem.ContractDate.HasValue ? transferMonthItem.ContractDate.Value.ToString("dd/MM/yyyy") : "", "pfr_fin_plan_date_item");
                        showFirstRow = true;
                    }

                    _sheet.SetValue(printRowNum, startKvartalColumn++, transferMonthItem.PlanSum, "pfr_fin_plan_number_item");
                    kvartalPlanSum += transferMonthItem.PlanSum;
                    yearPlanSum += transferMonthItem.PlanSum;

                    _sheet.SetValue(printRowNum, startKvartalColumn++, transferMonthItem.FactSum, "pfr_fin_plan_number_item");
                    kvartalFactSum += transferMonthItem.FactSum;
                    yearFactSum += transferMonthItem.FactSum;

                    if (transferMonthItem.MonthID % 3 == 0)
                    {
                        _sheet.SetValue(printRowNum, startKvartalColumn++, kvartalPlanSum, "pfr_fin_plan_number_item");
                        _sheet.SetValue(printRowNum, startKvartalColumn++, kvartalFactSum, "pfr_fin_plan_number_item");
                        kvartalFactSum = kvartalPlanSum = 0;
                    }
                });

                _sheet.SetValue(printRowNum, startKvartalColumn++, yearPlanSum, "pfr_fin_plan_number_item");
                _sheet.SetValue(printRowNum, startKvartalColumn++, yearFactSum, "pfr_fin_plan_number_item");

                printRowNum++;
            }

            //print all totlals
            startKvartalColumn = 5;
            String[] columnsForCalcTotasSum = { "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL" };

            Range totalsText = _sheet.Range[_sheet.Cells[printRowNum, 1], _sheet.Cells[printRowNum, 4]];
            totalsText.Merge();
            totalsText.SetValue("          Итого", "pfr_fin_plan_item");

            foreach (String totalColumn in columnsForCalcTotasSum)
            {
                String formula = "=SUM({col}7:{col}{count})".Replace("{col}", totalColumn).Replace("{count}", printRowNum.ToString());
                _sheet.SetValue(printRowNum, startKvartalColumn++, formula, "pfr_fin_plan_number_item");
            }
        }

        protected override void FillData()
        {
            items = BLServiceSystem.Client.GetTransfersUkPlanListByRegId(Document.Types.VR, false, true, VrYearTransferRegisterId);
            if (items.Count > 0)
                Fields.Add("PLAN_YEAR", items.FirstOrDefault().Company);
        }
    }

}





