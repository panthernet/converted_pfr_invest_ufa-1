﻿using System;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportException: Exception
    {
        public PfrBranchReportImportException()
        { }

        public PfrBranchReportImportException(string message)
            : base(message)
        { }
    }
}
