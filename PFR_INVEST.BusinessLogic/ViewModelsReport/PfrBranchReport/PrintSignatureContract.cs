﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PrintSignatureContract : PrintBase
    {
        public int PeriodYear { get; set; }
        public int? PeriodMonth { get; set; }
        public Person Responsible { get; set; }
        public Person Performer { get; set; }


        DataContainer Data;


        public PrintSignatureContract()
        {
            Name = "Отчет сведения об организациях соглашения";
            TemplateName = Name + ".xlsx";
        }

        protected override void FillData()
        {
            Fields.Add("REPORT_DATE", GetReportDateWordsPrepositional
                (PeriodYear, PeriodMonth));

            PrintResponsiblePerformer(Responsible, Performer);
        }


        public override void GenerateDocument(Worksheet s)
        {
            PrintHelper ph = new PrintHelper(s);
            Data.Print(ph);
        }

        public void LoadData()
        {
            Data = new DataContainer();
            Data.LoadData(PeriodYear, PeriodMonth);
        }

        internal bool IsReportDataIncomplete()
        {
            return Data.IsDataIncomplete();
        }

        internal string GetDataIncompleteMessage()
        {
            List<PFRBranch> xL = Data.GetDataIncompleteRegions();
            return GetDataIncompleteMessage(xL);
        }

        class PrintHelper
        {
            private const int START_ROW = 5;

            public PrintHelper()
            { }

            public PrintHelper(Worksheet s)
                : this()
            {
                S = s;
                ItemCount = 0;
                RowNumber = START_ROW;
            }

            public Worksheet S;
            public int RowNumber;
            public int ItemCount;


            internal void AutoFit(int count)
            {
                Range r = S.get_Range("C" + START_ROW, "C" + (START_ROW - 1 + count));
                r.Rows.AutoFit();
            }

            internal void AddEmptyRows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "H" + (RowNumber - 1 + count));
                r.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);

                //r = (Range)S.get_Range("A" + RowNumber, "H" + (RowNumber - 1 + count).ToString());
                //r.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
                //r.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
                //r.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                //r.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
                //r.Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;

                r = GetColumn("A", count);
                r.Style = "PfrBranchIndex";
                r = GetColumn("B", count);
                r.Style = "PfrBranch";
                r = GetColumn("C", count);
                r.Style = "PfrBranchCategory";

                r = GetColumn("D", count);
                r.Style = "PfrBranchNumber";
                r = GetColumn("E", count);
                r.Style = "PfrBranchDate";
                r = GetColumn("F", count);
                r.Style = "PfrBranchAddress";
                r = GetColumn("G", count);
                r.Style = "PfrBranchPhone";
                r = GetColumn("H", count);
                r.Style = "PfrBranchName";

            }

            private Range GetColumn(string index, int count)
            {
                return S.get_Range(index + RowNumber, index + (RowNumber - 1 + count));
            }

            private Range GetRow()
            {
                Range r = S.get_Range("A" + RowNumber, "H" + RowNumber);
                RowNumber++;
                return r;
            }

            private void SkipRows(int count)
            {
                RowNumber += count;
            }

            private Range GetNumRows(int count)
            {
                Range r = S.get_Range("D" + RowNumber, "H" + (RowNumber + count - 1));
                return r;
            }

            private Range GetARows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "A" + (RowNumber + count - 1));
                return r;
            }

            private Range GetBRows(int count)
            {
                Range r = S.get_Range("B" + RowNumber, "B" + (RowNumber + count - 1));
                return r;
            }

            private Range GetCRows(int count)
            {
                Range r = S.get_Range("C" + RowNumber, "C" + (RowNumber + count - 1));
                return r;
            }

            private Range GetRows(string colFrom, string colTo, int count)
            {
                Range r = S.get_Range(colFrom + RowNumber, colTo + (RowNumber + count - 1));
                return r;
            }

            private Range AddRows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "H" + (RowNumber + count - 1));
                RowNumber += count;
                return r;
            }

            internal void Print(BranchContainer x)
            {
                Range r;
                //Range r2;
                //object[] a;
                object[,] aa;

                int nc = x.CreditList.Count;
                int n = x.NpfList.Count;

                var cL = (from y in x.CreditList orderby y.Npf.FormalizedName select y).ToList();
                var nL = (from y in x.NpfList orderby y.Npf.FormalizedName select y).ToList();


                ItemCount++;
                r = GetARows(nc + n + 3);
                r.Merge();
                r.Value2 = ItemCount;


                r = GetBRows(nc + n + 3);
                r.Merge();
                r.Value2 = string.Format("{0} / {1:000}", x.Branch.Name, x.Branch.RegionNumber);


                int ri = 0;
                r = GetNumRows(nc + n + 3);
                aa = new object[nc + n + 3, 5];

                string s = nc > 0 ? null : "-";
                for (int i = 0; i < 5; i++)
                    aa[ri, i] = s;

                ri++;

                for (int i = 0; i < nc; i++)
                {
                    aa[ri, 0] = cL[i].DataList[0].Data.ContractNumber;
                    aa[ri, 1] = cL[i].DataList[0].Data.ContractDate;
                    aa[ri, 2] = cL[i].DataList[0].Data.PostAddress;
                    aa[ri, 3] = cL[i].DataList[0].Data.Phone;
                    aa[ri, 4] = cL[i].DataList[0].Data.ContactName;
                    ri++;
                }

                s = n > 0 ? null : "-";
                for (int i = 0; i < 5; i++)
                    aa[ri, i] = s;
                ri++;


                for (int i = 0; i < n; i++)
                {
                    aa[ri, 0] = nL[i].DataList[0].Data.ContractNumber;
                    aa[ri, 1] = nL[i].DataList[0].Data.ContractDate;
                    aa[ri, 2] = nL[i].DataList[0].Data.PostAddress;
                    aa[ri, 3] = nL[i].DataList[0].Data.Phone;
                    aa[ri, 4] = nL[i].DataList[0].Data.ContactName;
                    ri++;
                }

                s = "-";
                for (int i = 0; i < 5; i++)
                    aa[ri, i] = s;
                r.Value = aa;
                ri++;


                //Titles
                r = GetCRows(1);
                r.Value2 = "Кредитные организации";
                SkipRows(1);

                if (nc > 0)
                {
                    aa = new object[nc, 1];
                    r = GetCRows(nc);
                    for (int j = 0; j < nc; j++)
                        aa[j, 0] = cL[j].Npf.FormalizedName;
                    r.Value2 = aa;
                    r.Style = "PfrNpf";
                    SkipRows(nc);
                }

                r = GetCRows(1);
                r.Value2 = "НПФ";
                SkipRows(1);

                if (n > 0)
                {
                    aa = new object[n, 1];
                    r = GetCRows(n);
                    for (int j = 0; j < n; j++)
                        aa[j, 0] = nL[j].Npf.FormalizedName;
                    r.Value2 = aa;
                    r.Style = "PfrNpf";
                    SkipRows(n);
                }

                r = GetCRows(1);
                r.Value2 = "Организации связи";
                SkipRows(1);
            }

        }


        class DataContainer
        {
            public PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.SignatureContract;

            public DataContainer()
            {
                BranchList = new List<BranchContainer>();
            }

            public List<BranchContainer> BranchList;


            internal void LoadData(int periodYear, int? periodMonth)
            {
                bool isAnnual = periodMonth == null;

                List<PFRBranch> Branches;
                List<PfrBranchReport> Reports;
                List<PfrBranchReportSignatureContract> ReportData;
                List<LegalEntity> Credits;
                List<LegalEntity> NPFs;

                Branches = DataContainerFacade.GetList<PFRBranch>().ToList();

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportTypeID", Value = (long)ReportType });
                    pL.Add(new ListPropertyCondition { Name = "PeriodYear", Value = periodYear });
                    if (!isAnnual)
                        pL.Add(new ListPropertyCondition { Name = "PeriodMonth", Value = periodMonth });
                    pL.Add(new ListPropertyCondition { Name = "StatusID", Value = 1L });

                    Reports = DataContainerFacade.GetListByPropertyConditions<PfrBranchReport>(pL).ToList();
                }

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportID", Values = (from x in Reports select x.ID).Cast<object>().ToArray(), Operation = "in" });
                    ReportData = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportSignatureContract>(pL).ToList();
                }

                {
                    object[] ids = (from x in ReportData where x.DataType == PfrBranchReportSignatureContract.enType.Credit && x.LegalEntityID != null select x.LegalEntityID).Distinct().Cast<object>().ToArray();

                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ID", Values = ids, Operation = "in" });
                    Credits = DataContainerFacade.GetListByPropertyConditions<LegalEntity>(pL).ToList();
                }

                {
                    object[] ids = (from x in ReportData where x.DataType == PfrBranchReportSignatureContract.enType.Npf && x.LegalEntityID != null select x.LegalEntityID).Distinct().Cast<object>().ToArray();

                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ID", Values = ids, Operation = "in" });
                    NPFs = DataContainerFacade.GetListByPropertyConditions<LegalEntity>(pL).ToList();
                }

                LoadData(Branches, Reports, ReportData, Credits, NPFs, isAnnual);
            }

            internal void LoadData(List<PFRBranch> branches, List<PfrBranchReport> reports, List<PfrBranchReportSignatureContract> reportData, List<LegalEntity> credits, List<LegalEntity> npfs, bool isAnnual)
            {
                Dictionary<long, BranchContainer> bD = new Dictionary<long, BranchContainer>();

                foreach (var x in from y in branches orderby y.RegionNumber, y.Name select y)
                {
                    var y = new BranchContainer { Branch = x, IsAnnual = isAnnual};
                    bD.Add(x.ID, y);
                    BranchList.Add(y);
                }

                Dictionary<long, PfrBranchReport> rD = new Dictionary<long, PfrBranchReport>();

                foreach (var x in from y in reports orderby y.PeriodYear, y.PeriodMonth select y)
                {
                    rD.Add(x.ID, x);
                    bD[x.BranchID].ReportList.Add(x);
                }

                Dictionary<long, LegalEntity> cD = new Dictionary<long, LegalEntity>();

                foreach (var x in credits)
                    cD.Add(x.ID, x);

                Dictionary<long, LegalEntity> nD = new Dictionary<long, LegalEntity>();

                foreach (var x in npfs)
                    nD.Add(x.ID, x);


                foreach (var x in from y in reportData orderby y.ID select y)
                {
                    switch (x.DataType)
                    {
                        case PfrBranchReportSignatureContract.enType.Credit:
                            {
                                var d = new ReportDataContainer { Report = rD[x.ReportID], Data = x };
                                var c = bD[d.Report.BranchID].AddCredit(cD[x.LegalEntityID.Value], isAnnual);
                                c.AddReportData(d);
                            }
                            break;
                        case PfrBranchReportSignatureContract.enType.Npf:
                            {
                                var d = new ReportDataContainer { Report = rD[x.ReportID], Data = x };
                                var c = bD[d.Report.BranchID].AddNpf(nD[x.LegalEntityID.Value], isAnnual);
                                c.AddReportData(d);
                            }
                            break;
                    }
                }
            }

            internal void Print(PrintHelper ph)
            {
                int n = GetRowCount();
                ph.AddEmptyRows(n);

                foreach (var x in BranchList)
                    x.Print(ph);

                ph.AutoFit(n);
            }

            private int GetRowCount()
            {
                int n = 0;
                foreach (var x in BranchList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (BranchList == null || BranchList.Count == 0)
                    return true;

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }


            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in BranchList)
                    res.AddRange(x.GetDataIncompleteRegions());

                return res;
            }
        }


        class BranchContainer
        {
            public bool IsAnnual;

            public BranchContainer()
            {
                CreditList = new List<NpfContainer>();
                NpfList = new List<NpfContainer>();
                ReportList = new List<PfrBranchReport>();
            }

            public PFRBranch Branch { get; set; }
            public List<NpfContainer> CreditList { get; set; }
            public List<NpfContainer> NpfList { get; set; }
            public List<PfrBranchReport> ReportList { get; set; }


            internal void Print(PrintHelper ph)
            {
                ph.Print(this);
            }

            public int GetRowCount()
            {
                return 3 + CreditList.Count + NpfList.Count;
            }

            internal bool IsDataIncomplete()
            {
                //if (CreditList.Count == 0)
                //    return true;

                //if (NpfList.Count == 0)
                //    return true;

                if (ReportList.Count != (IsAnnual ? 12 : 1))
                    return true;


                foreach (var x in CreditList)
                    if (x.IsDataIncomplete())
                        return true;

                foreach (var x in NpfList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal NpfContainer AddCredit(LegalEntity n, bool isAnnual)
            {
                NpfContainer nc = CreditList.FirstOrDefault(x => x.Npf == n);
                if (nc == null)
                {
                    nc = new NpfContainer { Npf = n, IsAnnual = isAnnual };
                    CreditList.Add(nc);
                }

                return nc;
            }

            internal NpfContainer AddNpf(LegalEntity n, bool isAnnual)
            {
                NpfContainer nc = NpfList.FirstOrDefault(x => x.Npf == n);
                if (nc == null)
                {
                    nc = new NpfContainer { Npf = n, IsAnnual = isAnnual };
                    NpfList.Add(nc);
                }

                return nc;
            }

            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                if (IsDataIncomplete())
                {
                    res.Add(Branch);
                    return res;
                }

                return res;
            }
        }

        class NpfContainer
        {
            public bool IsAnnual;


            public NpfContainer()
            {
                DataList = new List<ReportDataContainer>();
            }

            public LegalEntity Npf;
            public List<ReportDataContainer> DataList;


            internal bool IsDataIncomplete()
            {
                if (Npf == null)
                    return true;

                if (DataList.Count < 1)
                    return true;

                foreach (var x in DataList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal void AddReportData(ReportDataContainer x)
            {
                DataList.Add(x);
            }
        }

        class ReportDataContainer
        {
            public PfrBranchReport Report;
            public PfrBranchReportSignatureContract Data;

            internal bool IsDataIncomplete()
            {
                if (Report == null || Data == null)
                    return true;

                return false;
            }
        }
    }
}
