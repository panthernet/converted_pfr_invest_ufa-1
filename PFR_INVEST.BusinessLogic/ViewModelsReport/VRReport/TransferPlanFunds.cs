﻿
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.VRReport
{
    public class TransferPlanFunds : ReportExportVRHandlerBase
    {
        private readonly List<SIRegister> _siRegisters;

        private const long OPERATION_ID=13;
        public TransferPlanFunds()
        {
            _siRegisters = BLServiceSystem.Client.GetSiRegistersByContractOperationHib((int)Document.Types.VR,OPERATION_ID);
            Years = BLServiceSystem.Client.GetYearListCreateSIUKPlan(Document.Types.VR,OPERATION_ID);
            _siRegisters = _siRegisters.Where(r => Years.Select(y => y.ID).Contains((long)r.CompanyYearID)).ToList();
            SelectedYear = Years.FirstOrDefault();
        }

        public override string ReportType => "01";

        public override bool IsReportContainsData => SelectedYear!=null;

        public override bool IsYearEnabled => true;

        public override bool IsYearVisible => true;

        private Year _SelectedYear;
        public override Year SelectedYear
        {
            get
            {
                return _SelectedYear;
            }
            set
            {
                _SelectedYear = value;

            }
        }


        public override bool CanExecuteExport()
        {
            return IsReportContainsData && SelectedYear != null;
        }

        public override void ExecuteExport()
        {
            List<long> id = _siRegisters.Where(si => si.CompanyYearID == SelectedYear.ID).Select(si => si.ID).ToList();
            if (id.Count == 0)
            {
                ViewModelBase.DialogHelper.ShowExclamation("Не найдено данных для экспорта!", "Внимание");
                return;
            }
            PrintTransferPlanFunds printTr = new PrintTransferPlanFunds(id);
            printTr.Print();
        }
    }
}
