﻿
using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.VRReport
{
    public abstract class ReportExportVRHandlerBase
    {
        public abstract string ReportType { get; }

        public abstract bool IsReportContainsData { get; }

        public abstract bool CanExecuteExport();

        public abstract void ExecuteExport();


        public virtual bool IsYearEnabled => false;

        public virtual bool IsYearVisible => false;


        public virtual bool IsMonthEnabled => false;

        public virtual bool IsMonthVisible => false;

        public virtual bool IsResponsibleEnabled => false;

        public virtual bool IsResponsibleVisible => false;


        public virtual bool IsPerformerEnabled => false;

        public virtual bool IsPerformerVisible => false;


        public virtual bool IsPeriodLengthVisible => false;

        public virtual bool IsPeriodLengthEnabled => false;


        public virtual bool IsMonthly
        {
            get { return !_IsAnnual; }
            set { _IsAnnual = !value; }
        }

        private bool _IsAnnual;

        public virtual bool IsAnnual
        {
            get { return _IsAnnual; }
            set { _IsAnnual = value; }
        }

        public virtual List<Month> Monthes { get; set; }
        public virtual Month SelectedMonth { get; set; }
        public virtual List<Year> Years { get; set; }
        public virtual Year SelectedYear { get; set; }

        public virtual Person Responsible { get; set; }
        public virtual Person Performer { get; set; }
    }
}
