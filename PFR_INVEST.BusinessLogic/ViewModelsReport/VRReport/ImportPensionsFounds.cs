﻿
using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.VRReport;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    public class ImportPensionsFounds : ReportImportVRHandlerBase
    {
        private static List<string> header1Table = new List<string> { "Наименование инвестиционного портфеля ГУК Выплатного резерва:", "Отозвано из УК", null, "из них", null };

        private static List<string> header2Table = new List<string> { null, "Численность ЗЛ, чел.", "Сумма СПН, руб.", "Сумма СПН подлежащая передаче в ГУК ВР, руб.", "Сумма средств СПН подлежащая передаче в ТО ПФР, руб." };


        List<List<string>> _headerTable;
        protected override List<List<string>> HeaderTable
        {
            get
            {
                if (_headerTable == null)
                    _headerTable = new List<List<string>> { header1Table, header2Table };
                return _headerTable;
            }
        }

        SiImportOperation _operation;
        protected override SiImportOperation Operation
        {
            get
            {
                if (_operation == null)
                    _operation = new SiImportOperation("Передача СПН в доверительное управление");
                return _operation;
            }
        }

        protected override int RowDataIterator => 5;

        protected override string EndDataColumn => "E";

        protected override bool SetTransfer(int row, object[,] obj, out string message)
        {
            try
            {
                String docType = GetDocumnetType(obj[1, 1].ToString());
                List<Contract> contracts = BLServiceSystem.Client.GetGukConractsByContractName(docType);
                if (contracts.Count > 1)
                    throw new Exception(String.Format("Для «{0}» - договор типа «{1}» найдено больше 1 контракта.\nНайденные номера контрактов:{2}",
                                                        obj[1, 1], docType,
                                                        String.Join(",", contracts.Select(item => item.ContractNumber).ToArray())));               

                long zlcount = 0;
                if (obj[1, 2] == null || !long.TryParse(obj[1, 2].ToString(), out zlcount))
                    throw new Exception(String.Format("Неверное значение для столбца 'Численность ЗЛ, чел.', предпологалось целое число. Текущее значение = {0}, строка = {1}", obj[1, 2], row));

                decimal sum = 0;
                if (obj[1, 4] == null || !decimal.TryParse(obj[1, 4].ToString(), out sum))
                    throw new Exception(String.Format("Неверное значение для столбца 'Сумма средств СПН подлежащая передаче в ТО ПФР, руб.', предпологалось число. Текущее значение = {0}, строка = {1}", obj[1, 4], row));

                if (sum > 0 && contracts.Count > 0)
                {
                    contracts.ForEach(item =>
                    {
                        SITransfer siTransfer = new SITransfer
                        {
                            ContractID = item.ID,
                            InsuredPersonsCount = zlcount,
                            StatusID = 1
                        };

                        ReqTransfer reqTransfer = new ReqTransfer
                        {
                            DIDate = DateTime.Now.Date,
                            Sum = sum,
                            TransferStatus = Status,
                            StatusID = 1
                        };

                        ImportData.siTransferList.Add(new SiRegisterTransferData.SITransferData
                            {
                            siTransfer = siTransfer,
                            reqTransferList = new List<ReqTransfer> { reqTransfer }
                        }
                                                );
                    });
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = null;
            return true;
        }

        public override string ReportType => "05";

        public override string OpenFileMask => string.Format("Передача средств пенсионных накоплений в ГУК средствами выплатного резерва(.xls, .xlsx)|{0}", "*.xls;*.xlsx");
    }
}