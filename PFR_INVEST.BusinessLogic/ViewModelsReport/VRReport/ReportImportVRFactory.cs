﻿
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ReportImportVRFactory
    {
        public static ReportImportSIHandlerBase GetImportHandler(ReportImportVRDlgViewModel model)
        {
            switch (model.SelectedReportType.ReportType)
            {
                case "04":
                    return new ImportDiedFounds();
                case "05":
                    return new ImportPensionsFounds();
                case "06":
                    return new ImportReviewSpnToGuc();
                default:
                    return new ReportImportSIHandlerEmpty();
            }
        }
    }
}
