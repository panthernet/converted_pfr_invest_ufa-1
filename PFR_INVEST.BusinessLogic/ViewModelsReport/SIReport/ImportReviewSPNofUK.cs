﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    public class ImportReviewSPNofUK : ReportImportSIHandlerBase
    {
        private static readonly List<string> Header1Table = new List<string>
        {"", "№ п/п","Наименование УК","ИНН","Наименование инвестиционного портфеля",
           "Договор доверительного управления","Всего",null,null,"в том числе:",null,null,null,null,null,null,null};

        private static readonly List<string> Header2Table = new List<string>
        {"",null,null,null,null,null,"Численность застрахованных лиц, чел.",
          "Сумма СПН, руб.","в т.ч., инвест. доход, руб.","Накопительная часть трудовой пенсии",null,null,"Срочная выплата",null,null,
          "Единовременная выплата",null,null };

        private static readonly List<string> Header3Table = new List<string>
        {"",null,null,null,null,null,null,null,null,
           "Численность застрахованных лиц, чел.","Сумма СПН, руб.","в т.ч., инвест. доход, руб.",
           "Численность застрахованных лиц, чел.","Сумма СПН, руб.","в т.ч., инвест. доход, руб.",
           "Численность застрахованных лиц, чел.","Сумма СПН, руб.","в т.ч., инвест. доход, руб."};

        private static readonly List<List<string>> HeaderTable = new List<List<string>> { Header1Table, Header2Table, Header3Table };


        //private List<SiImportOperation> siImportOperations;
        private List<Contract> _contracts;

        private List<SIRegister> _siRegisters;

        private List<SiRegisterTransferData> _baseDataSiRegisters;
        private SiRegisterTransferData _importNchTp;
        private SiRegisterTransferData _importSpv;
        private SiRegisterTransferData _importEv;

        private readonly SiImportOperation _operationNchTp;
        private readonly SiImportOperation _operationSpv;
        private readonly SiImportOperation _operationEv;

        private List<long> _listDeleteSiRegisterID;

        private const string STATUS = "Начальное состояние";

        public ImportReviewSPNofUK()
        {
            _operationNchTp = new SiImportOperation("Отзыв средств на финансирование НЧТП");
            _operationSpv = new SiImportOperation("Отзыв средств на финансирование СПВ");
            _operationEv = new SiImportOperation("Отзыв средств на финансирование ЕВ");
        }

        public override string ReportType => "11";

        public override string OpenFileMask => string.Format("Отзыв СПН из УК (по видам выплат)(.xls, .xlsx)|{0}", "*.xls;*.xlsx");

        public override Year SelectedYear { get; set; }

        public override Month SelectedMonth { get; set; }

        public override bool ExecuteImport(out string message)
        {
           // message = null;
            var result = false;
            if (IsValidateImport(
                new List<SiRegisterTransferData> { _importNchTp, _importSpv, _importEv }, out message))
            {
                result = Import(out message);
                if (result) RefreshViewModels();
            }

            _canExecuteImport = false;
            return result;
        }

        /// <summary>
        /// Валидация импорта
        /// </summary>
        /// <param name="listImport"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool IsValidateImport(List<SiRegisterTransferData> listImport, out string message)
        {
            _listDeleteSiRegisterID = new List<long>();
            var contractName = string.Empty;

            foreach (var baseDataSiRegister in _baseDataSiRegisters)
            {
                foreach (var siTransferBase in baseDataSiRegister.siTransferList)
                {
                    foreach (var import in listImport)
                    {
                        //var siTransferList = Import.siTransferList.FirstOrDefault(
                        //        st => st.siTransfer.ContractID == siTransfer.siTransfer.ContractID);
                        if (import.siTransferList.Any(s => s.siTransfer.ContractID == siTransferBase.siTransfer.ContractID))
                        {
                            if (
                                siTransferBase.reqTransferList.Any(reqTr => !string.Equals(
                                    reqTr.TransferStatus, STATUS, StringComparison.InvariantCultureIgnoreCase)))
                            {
                                ViewModelBase.DialogHelper.ShowAlert(
                                   string.Format(
                                       "Импорт невозможен.\nНа указанный период реестр по операции \"{0}\" уже был сформирован, статус перечислений реестра отличен от \"{1}\".",
                                       ReportName,
                                       STATUS));

                                message =
                                    string.Format(
                                        "Импорт невозможен.\nНа указанный период реестр по операции \"{0}\" уже был сформирован, статус перечислений реестра отличен от \"{1}\".",
                                        ReportName,
                                        STATUS);
                                return false;
                            }
                            if (!_listDeleteSiRegisterID.Contains(baseDataSiRegister.siRegister.ID))
                            {
                                _listDeleteSiRegisterID.Add(baseDataSiRegister.siRegister.ID);
                                contractName += siTransferBase.siTransfer.GetContract().ContractNumber + " ";
                            }
                        }
                    }
                }
            }

            if (_listDeleteSiRegisterID.Count > 0)
            {
                if (
                    !ViewModelBase.DialogHelper.ShowConfirmation(
                        string.Format(
                            "Внимание!\nНа указанный период реестр по операции {0} уже был сформирован.\nУдалить ранее созданный реестр и создать новый?",
                            ReportName)))
                {
                    message = "Импорт прерван пользователем.";
                    return false;
                }
            }
            message = null;
            return true;
        }

        /// <summary>
        /// Валидация документа
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public override bool IsValidateDoc(Worksheet worksheet, out string message)
        {
            _contracts = BLServiceSystem.Client.GetContractsForUKsByType((int)Document.Types.SI);
            _siRegisters = BLServiceSystem.Client.GetSiRegistersByContractOperationHib((int)Document.Types.SI,null);
            _siRegisters =
                _siRegisters.Where(
                    r =>
                    r.OperationID == _operationNchTp.OperationId || r.OperationID == _operationEv.OperationId
                    || r.OperationID == _operationSpv.OperationId).ToList();
            _canExecuteImport = false;
           // List<List<object>> A2Q4 = (worksheet.get_Range("A2", "Q4").Value2 as List<List<object>>);


            if (IsHeaderValidate(worksheet.Range["A1"].Value2.ToString(), out message))
            {
                for (var c = 0; c < HeaderTable.Count; c++)
                {
                    if (!IsHeaderTableValidate((object[,])worksheet.Range[string.Format("A{0}", c + 2), (string.Format("Q{0}", c + 2))].Value2,
                    HeaderTable[c], out message))
                        return false;
                }
            }
            else
            {
                return false;
            }
            //импоррт данных в класс
            if (!SetDataImport(worksheet, out message))
            {
                //ViewModelBase.DialogHelper.Alert(message);
                return false;
            }
            //получение данных приложения
            if (!SetDataBase(out message))
            {
                //ViewModelBase.DialogHelper.Alert(message);
                return false;
            }

            _canExecuteImport = true;
            return true;
        }

        private bool IsHeaderValidate(string header, out string message)
        {
            message = null;
            if (header.ToLower().Contains(HeaderNameReport.ToLower()))
            {

                if (header.ToLower().Contains(SelectedYear.Name.ToLower())
                    && header.ToLower().Contains(SelectedMonth.Name.ToLower()))
                {
                    return true;
                }
                message = string.Format("Отчет не содержит данных на {0} {1}.",
                    SelectedMonth.Name.ToLower(), SelectedYear.Name.ToLower());
                return false;
            }
            message = string.Format("Документ не валидный!\nОжидалось\n{0}.\nФактически\n{1}",
                HeaderNameReport.TruncateAtWord(100), 
                header.TruncateAtWord(100));
            return false;

            // return true;
        }

        private bool IsHeaderTableValidate(object[,] obj, List<string> list, out string message)
        {

            for (var i = 1; i < list.Count; i++)
            {
                if (list[i] == null)
                {
                    if (obj[1, i] != obj[1, i])
                    {
                        message = string.Format("Ожидалось пустое значение ячейки.\nФактически {0}", obj[1, i]);
                        return false;
                    }
                }
                else
                {
                    if (!string.Equals(obj[1, i].ToString(), list[i], StringComparison.OrdinalIgnoreCase))
                    {
                        message = string.Format("Ожидалось {0} значение ячейки.\nФактически {1}", list[i], obj[1, i]);
                        return false;
                    }
                }

            }
            message = null;
            return true;

        }

        public override bool CanExecuteOpenFile()
        {
            return SelectedMonth != null && SelectedYear != null;
        }

        private bool _canExecuteImport;

        public override bool CanExecuteImport()
        {
            return _canExecuteImport;
        }

        /// <summary>
        /// получим данные из файла для импорта
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool SetDataImport(Worksheet worksheet, out string message)
        {

            _importNchTp = new SiRegisterTransferData(SelectedMonth.ID, SelectedYear.ID, _operationNchTp.DirectionId, _operationNchTp.OperationId);
            _importSpv = new SiRegisterTransferData(SelectedMonth.ID, SelectedYear.ID, _operationSpv.DirectionId, _operationSpv.OperationId);
            _importEv = new SiRegisterTransferData(SelectedMonth.ID, SelectedYear.ID, _operationEv.DirectionId, _operationEv.OperationId);

            var iterator = 6;
            var end = worksheet.Range["A" + iterator].Value2;
            while (end != null)
            {
                int res;
                if (int.TryParse(worksheet.Range["A" + iterator].Value2.ToString(), out res))
                {
                    if (
                        !SetTransfer(
                            (object[,])worksheet.Range[string.Format("A{0}", iterator), (string.Format("Q{0}", iterator))].Value2,
                            out message))
                    {
                        return false;
                    }
                }

                end = worksheet.Range["A" + (++iterator)].Value2;
            }
            return IsImportFilled(out  message);
        }

        private bool SetTransfer(object[,] obj, out string message)
        {

            try
            {
                //10 _importNchTp
                var zlcount = int.Parse(obj[1, 9].ToString());
                var sum = decimal.Parse(obj[1, 10].ToString());
                var investDohod = decimal.Parse(obj[1, 11].ToString());
                var contractID = GetContractId(obj[1, 2], obj[1, 3], obj[1, 5]);
                if (sum > 0)
                {
                    var siTransfer = new SITransfer
                    {
                                                    ContractID = contractID,
                                                    InsuredPersonsCount = zlcount,
                        StatusID = 1
                                                };
                    var reqTransfer = new ReqTransfer
                    {
                                                     DIDate = DateTime.Now.Date,
                                                     InvestmentIncome = investDohod,
                                                     Sum = sum,
                                                     TransferStatus = STATUS,
                        StatusID = 1
                                                 };
                    _importNchTp.siTransferList.Add(new SiRegisterTransferData.SITransferData
                    {
                                                            siTransfer = siTransfer,
                                                            reqTransferList = new List<ReqTransfer> { reqTransfer }
                                                        }
                        );
                }
                //13 _importSpv 
                zlcount = int.Parse(obj[1, 12].ToString());
                sum = decimal.Parse(obj[1, 13].ToString());
                investDohod = decimal.Parse(obj[1, 14].ToString());
                if (sum > 0)
                {
                    var siTransfer = new SITransfer
                                                {
                                                    ContractID = contractID,
                                                    InsuredPersonsCount = zlcount,
                                                    StatusID = 1
                                                };

                    var reqTransfer = new ReqTransfer
                                                 {
                                                     DIDate = DateTime.Now.Date,
                                                     InvestmentIncome = investDohod,
                                                     Sum = sum,
                                                     TransferStatus = TransferStatusIdentifier.sBeginState,
                                                     StatusID = 1
                                                 };
                    _importSpv.siTransferList.Add(
                        new SiRegisterTransferData.SITransferData
                        {
                                siTransfer = siTransfer,
                                reqTransferList =
                                    new List<ReqTransfer> { reqTransfer }
                            });

                }

                //16 _importEv
                zlcount = int.Parse(obj[1, 15].ToString());
                sum = decimal.Parse(obj[1, 16].ToString());
                investDohod = decimal.Parse(obj[1, 17].ToString());
                if (sum > 0)
                {
                    var siTransfer = new SITransfer
                    {
                        ContractID = contractID,
                        InsuredPersonsCount = zlcount,
                        StatusID = 1

                    };

                    var reqTransfer = new ReqTransfer
                    {
                        DIDate = DateTime.Now.Date,
                        InvestmentIncome = investDohod,
                        Sum = sum,
                        TransferStatus = TransferStatusIdentifier.sBeginState,
                        StatusID = 1
                    };
                    _importEv.siTransferList.Add(
                        new SiRegisterTransferData.SITransferData
                        {
                            siTransfer = siTransfer,
                            reqTransferList =
                                new List<ReqTransfer> { reqTransfer }
                        });
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = null;
            return true;
        }

        private long GetContractId(object ukName, object iNn, object regNum)
        {

            var contr = _contracts.FirstOrDefault(c => string.Equals(c.ContractNumber, regNum.ToString(), StringComparison.OrdinalIgnoreCase));

            if (contr == null) throw new Exception(string.Format("Импорт невозможен.\nКонтракт с номером договора - {0} не найден.", regNum));

            var leContract = contr.GetLegalEntity();
            //---------------FormalizedName--------------------------
            var leFormalizedName = DataContainerFacade.GetListByProperty<LegalEntity>("FormalizedName", ukName.ToString()).FirstOrDefault();
            if (leFormalizedName == null)
                throw new Exception(string.Format("Импорт невозможен.\nУК с наименованием \"{0}\" в системе не обнаружена", ukName));

            if (!string.Equals(leContract.FormalizedName, leFormalizedName.FormalizedName, StringComparison.OrdinalIgnoreCase))
                throw new Exception(string.Format("Импорт невозможен.\nУК с наименованием - \"{0}\" не имеет договора № {1}", ukName, regNum));
            //--------------------------------INN-----------------------------------------
            var leInn = DataContainerFacade.GetListByProperty<LegalEntity>("INN", iNn.ToString()).FirstOrDefault();
            if (leInn == null)
                throw new Exception(string.Format("Импорт невозможен.\nУК с ИНН \"{0}\" в системе не обнаружена", iNn));

            if (!string.Equals(leContract.INN, leInn.INN, StringComparison.OrdinalIgnoreCase))
                throw new Exception(string.Format("Импорт невозможен.\nУК с ИНН - \"{0}\" не имеет договора № {1}", iNn, regNum));

            //if (!string.Equals(leContract.FormalizedName, ukName.ToString(), StringComparison.OrdinalIgnoreCase))
            //    throw new Exception(string.Format("Импорт невозможен.\nКонтракт с номером договора {0}\nне имеет УК с формализованным наименованием - {1}", regNum, ukName));

            //if (!string.Equals(leContract.INN, iNn.ToString(), StringComparison.OrdinalIgnoreCase))
            //    throw new Exception(string.Format("Импорт невозможен.\nКонтракт с номером договора {0}\nне имеет ИНН - {1}", regNum,iNn));

            return contr.ID;
        }

        /// <summary>
        /// получим данные из базы о перечислениях на выбранный период
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool SetDataBase(out string message)
        {
            try
            {
                var srs = _siRegisters.Where(
                                    sir => sir.CompanyMonthID == SelectedMonth.ID && sir.CompanyYearID == SelectedYear.ID)
                                           .ToList();
                _baseDataSiRegisters = new List<SiRegisterTransferData>();

                foreach (var sr in srs)
                {
                    _baseDataSiRegisters.Add(new SiRegisterTransferData(sr));
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
            message = null;
            return true;
        }

        private bool Import(out string message)
        {
            message = "Сформированы реестры:\n";
            try
            {
                var resaltNChTP = _importNchTp.ExecuteSave();
                var resaltSPV = _importSpv.ExecuteSave();
                var resaltEV = _importEv.ExecuteSave();

                if (resaltNChTP != 0)
                    message += string.Format("№ {0} по операции «Отзыв средств на финансирование НЧТП»\n", resaltNChTP);
                if (resaltSPV != 0)
                    message += string.Format("№ {0} по операции «Отзыв средств на финансирование СПВ»\n", resaltSPV);
                if (resaltEV != 0)
                    message += string.Format("№ {0} по операции «Отзыв средств на финансирование ЕВ»\n\n", resaltEV);

                if (_listDeleteSiRegisterID.Count > 0)
                {
                    SiRegisterTransferData.ExecuteDelete(_siRegisters, _listDeleteSiRegisterID);
                    message = _listDeleteSiRegisterID.Aggregate(message, (current, item) => current + string.Format("Удален реестр перечислений № {0}\n", item));
                }

                if (!string.IsNullOrEmpty(message))
                    message += "\n\nДальнейшую обработку реестра производите из списка «Перечисления»";
                _importNchTp = new SiRegisterTransferData();
                _importSpv = new SiRegisterTransferData();
                _importEv = new SiRegisterTransferData();
                _listDeleteSiRegisterID = new List<long>();
            }
            catch (Exception ex)
            {

                message = ex.Message;
                return false;
            }
            return true;
        }

        private bool IsImportFilled(out string message)
        {
            if (_importNchTp.siTransferList.Count == 0 && _importSpv.siTransferList.Count == 0
                && _importEv.siTransferList.Count == 0)
            {
                message = "Импорт невозможен.\nОтсутствуют данные для импорта.";
                return false;
            }
            message = null;
            return true;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(TransfersSIListViewModel));
        }
    }
}

