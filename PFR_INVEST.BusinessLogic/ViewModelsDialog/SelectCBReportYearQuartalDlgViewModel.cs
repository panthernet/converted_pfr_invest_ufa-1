﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OARRS_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class SelectCBReportYearQuartalDlgViewModel : ViewModelCardDialog
    {
        private List<int> _yearsList;
        public List<int> YearsList
        {
            get { return _yearsList; }
            set { _yearsList = value; OnPropertyChanged("YearsList"); }
        }

        private List<QuarterData> _monthsList;
        public List<QuarterData> MonthsList
        {
            get { return _monthsList; }
            set { _monthsList = value; OnPropertyChanged(nameof(MonthsList)); }
        }

        public List<string> OkudList { get; set; }

        private List<QuarterData> _quartersList;
        public List<QuarterData> QuartersList
        {
            get { return _quartersList; }
            set { _quartersList = value; OnPropertyChanged("QuartersList"); }
        }

        private int? _selectedYear;
        public int? SelectedYear
        {
            get { return _selectedYear; }
            set { _selectedYear = value; RefreshQuarters(); OnPropertyChanged("SelectedYear"); }
        }

        private QuarterData _selectedMonth;
        public QuarterData SelectedMonth
        {
            get { return _selectedMonth; }
            set { _selectedMonth = value; OnPropertyChanged(nameof(SelectedMonth)); }
        }

        private QuarterData _selectedQuarter;
        public QuarterData SelectedQuarter
        {
            get { return _selectedQuarter; }
            set { _selectedQuarter = value; OnPropertyChanged("SelectedQuarter"); }
        }

        private string _selectedOkud;
        public string SelectedOkud
        {
            get { return _selectedOkud; }
            set { _selectedOkud = value; RefreshYears(); OnPropertyChanged("SelectedOkud"); }
        }

        public BOReportForm1.ReportTypeEnum SelectedReportType => BOReportForm1.GetOkudTypeFromString(SelectedOkud);

        public ICommand OkCommand { get; set; }

        private readonly bool _forMonthly;

        public SelectCBReportYearQuartalDlgViewModel(bool forceOne, bool skipOne = true, bool forMonthly = false)
        {
            _forMonthly = forMonthly;
            if (_forMonthly)
            {
                //MonthsList = DateTools.Months.ToList();
                var availableYears = BLServiceSystem.Client.GetBOReportMonthlyFreeYearsAnMonthsList();
                YearsList = availableYears.Keys.ToList();
                SelectedYear = YearsList.Contains(DateTime.Today.Year) ? DateTime.Today.Year : YearsList.FirstOrDefault();
                SelectedYear = SelectedYear == 0 ? null : SelectedYear;
                if (availableYears.Count != 0 && _selectedYear.HasValue)
                {
                    MonthsList = availableYears[_selectedYear.Value].OrderBy(a => a).ToList().ConvertAll(a => new QuarterData { Number = a-1, Text = DateTools.Months[a-1]});
                    SelectedMonth = MonthsList.FirstOrDefault();
                }
            }
            else
            {
                if (forceOne)
                {
                    OkudList = new List<string> {BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.One)};
                }
                else
                {
                    OkudList = new List<string> {BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Two), BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Three)};
                    if (!skipOne) OkudList.Insert(0, BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.One));
                }
                SelectedOkud = OkudList.First();
            }
            OkCommand = new DelegateCommand(a=> SelectedYear != null && (SelectedQuarter != null || SelectedMonth != null), a=> RequestCloseView(true));
        }

        private void RefreshYears()
        {
            SelectedQuarter = null;
            SelectedYear = null;
            QuartersList = null;
            var availableYears = _forMonthly ? BLServiceSystem.Client.GetBOReportMonthlyFreeYearsAnMonthsList() : BLServiceSystem.Client.GetBOReportForm1FreeYearsAndQaurtersList(SelectedReportType);
            YearsList = availableYears.Keys.ToList();
            _selectedYear = YearsList.Contains(DateTime.Today.Year) ? DateTime.Today.Year : YearsList.FirstOrDefault();
            SelectedYear = _selectedYear == 0 ? null : _selectedYear;
        }

        private void RefreshQuarters(Dictionary<int,List<int>> availableYears = null)
        {
            if (SelectedYear != null && SelectedYear != 0)
            {
                availableYears = availableYears ?? (_forMonthly ? BLServiceSystem.Client.GetBOReportMonthlyFreeYearsAnMonthsList() : BLServiceSystem.Client.GetBOReportForm1FreeYearsAndQaurtersList(SelectedReportType));
                QuartersList = availableYears[SelectedYear.Value].OrderBy(a => a).ToList().ConvertAll(a => new QuarterData { Number = a, Text = DateTools.GetQuarterRomeName(a) });
                SelectedQuarter = QuartersList.First();
            }
        }

    }
}
