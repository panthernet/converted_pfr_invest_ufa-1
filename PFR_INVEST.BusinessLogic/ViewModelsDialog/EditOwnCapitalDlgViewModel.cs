﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class EditOwnCapitalDlgViewModel : ViewModelBase//ViewModelCard, IUpdateRaisingModel, IDialogViewModel
    {
        public OwnCapitalHistory OwnCapital{ get; private set; }
        public DelegateCommand<object> SaveCard { get; private set; }
        public List<OwnCapitalHistory> Existing { get; private set; }public DateTime MinDate => new DateTime(1990, DateTime.Today.Month, DateTime.Today.Day);

        public EditOwnCapitalDlgViewModel(OwnCapitalHistory capital, List<OwnCapitalHistory> items)
        {
            this.ID = capital.ID;
            this.OwnCapital = capital;
            this.Existing = items;
            this.SaveCard = new DelegateCommand<object>(o => CanExecuteSave(), o => ExecuteSave());

            this.OwnCapital.PropertyChanged += (sender, e) => { this.IsDataChanged = true; };

            if(this.OwnCapital.ID <= 0)
                OwnCapital.ReportDate = DateTime.Now;
        }

        private bool CanExecuteSave()
        {
            return IsDataChanged && this.OwnCapital.Validate();
        }

        public bool HasImportedDuplicates()
        {
            return Existing.Any(x => x.IsImported == 1 && x.ReportDate.Date == this.OwnCapital.ReportDate.Date);
        }
        
        private bool BeforeExecuteSaveCheck()
        {
            if (this.Existing == null || this.Existing.Count == 0) return true;

            if (Existing.Any(x => x.IsImported == 0 && x.ReportDate.Date == this.OwnCapital.ReportDate.Date && x.ID != this.OwnCapital.ID))
            {
                return DialogHelper.ConfirmOwnCapitalRewrite();
            }

            if (HasImportedDuplicates())
            {
                DialogHelper.ShowAlert(string.Format("Для выбранной отчетной даты в истории изменений основного капитала уже существует проимпортированные данные, которые не могут быть перезаписаны." + Environment.NewLine + "Введите другую отчетную дату."));
                return false;
            }

            return true;
        }

        public void ExecuteSave()
        {
            if (!BeforeExecuteSaveCheck()) return;

            foreach (var cap in Existing.Where(c => c.IsImported == 0 && c.ReportDate.Date == this.OwnCapital.ReportDate.Date && c.ID != this.OwnCapital.ID))
            {
                DataContainerFacade.Delete<OwnCapitalHistory>(cap.ID);
            }

            this.OwnCapital.ID = DataContainerFacade.Save(this.OwnCapital);
        }

        public bool IsValid()
        {
            const string fields = "OwnCapital|ReportDate";
            if (fields.Split('|').Any(key => !string.IsNullOrEmpty(OwnCapital[key])))
                return false;

            return OwnCapital.ReportDate.Year >= 1990;
        }

        public bool? AskSaveOnClose()
        {
            if (!IsValid())
            {
                bool b = DialogHelper.ShowConfirmation("Вы уверены, что хотите закрыть диалог и потерять все изменения?");
                return b ? (bool?)false : null;
            }

            IsDataChanged = true;
            if (IsDataChanged)
            {
                return DialogHelper.ConfirmSaveOnClose();
            }

            return false;
        }

        public bool? AskOverwriteDataOnClose()
        {
            return BeforeExecuteSaveCheck();
        }

        public bool CanSaveOnClose()
        {
            bool b = IsValid();

            if (b) return true;

            DialogHelper.ShowAlert("Невозможно сохранить: некорректные значения полей");
            return false;
        }
/*
        IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>>();
            list.Add(new KeyValuePair<Type, long>(typeof(OwnCapitalHistory), this.OwnCapital.ID));
            if (this.OwnCapital.LegalEntityID > 0)
                list.Add(new KeyValuePair<Type, long>(typeof(LegalEntity), this.OwnCapital.LegalEntityID));

            return list;
        }

		private void RequestClose(bool? DialogResult)
		{
			if (OnCloseRequested != null)
				OnCloseRequested(this, new EventArgs<bool?>(DialogResult));
		}
	
        public event EventHandler<EventArgs<bool?>> OnCloseRequested;
*/	
    }
}
