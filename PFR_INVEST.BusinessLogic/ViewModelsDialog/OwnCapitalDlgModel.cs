﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class OwnCapitalDlgModel : ViewModelCard, IVmNoDefaultLogging, IUpdateRaisingModel
    {
        public LegalEntity Bank { get; private set; }

        private ObservableList<OwnCapitalHistory> items = new ObservableList<OwnCapitalHistory>();
        public ObservableList<OwnCapitalHistory> Items
        {
            get { return this.items; }
            private set { this.items = value; this.OnPropertyChanged("Items"); }
        }


        public ICommand AddOwnCapital { get; private set; }
        public ICommand EditOwnCapital { get; private set; }
        public ICommand DeleteOwnCapital { get; private set; }

        public override Type DataObjectTypeForJournal => typeof (OwnCapitalHistory);

        public OwnCapitalDlgModel(LegalEntity bank)
            : base(typeof(OwnCapitalDlgModel))
        {
            this.Bank = bank;
            this.ID = Bank == null ? 0 : this.Bank.ID;
            this.Items = new ObservableList<OwnCapitalHistory>(this.Bank.GetOwnCapitalHistory());
            this.Items.CollectionChanged += (sender, e) => { this.IsDataChanged = true; };
            this.AddOwnCapital = new DelegateCommand(o => CanExecuteAddOwnCapital(), o => ExecuteAddOwnCapital());
            this.EditOwnCapital = new DelegateCommand(o => CanExecuteEditOwnCapital(), o => ExecuteEditOwnCapital());
            this.DeleteOwnCapital = new DelegateCommand(o => CanExecuteDeleteOwnCapital(), o => ExecuteDeleteOwnCapital());
        }

        private OwnCapitalHistory _FocusedOwnCapital;

        public OwnCapitalHistory FocusedOwnCapital
        {
            get
            { return _FocusedOwnCapital; }
            set
            { _FocusedOwnCapital = value as OwnCapitalHistory; }
        }

        public bool CanExecuteEditOwnCapital()
        {
            return !EditAccessDenied && FocusedOwnCapital != null && FocusedOwnCapital.IsImported == 0;
        }

        private void ExecuteEditOwnCapital()
        {
            if (FocusedOwnCapital == null)
                return;

            if (DialogHelper.EditOwnCapital(FocusedOwnCapital, this.items.ToList(), false))
            {
                FocusedOwnCapital = null;
                RefreshList();
            }
        }

        private void ExecuteDeleteOwnCapital()
        {
            if (!DialogHelper.ShowQuestion("Удалить выбранный элемент?", "Удаление")) return;

            DataContainerFacade.Delete<OwnCapitalHistory>(FocusedOwnCapital);
            FocusedOwnCapital = null;
            RefreshConnectedViewModels();
            RefreshList();
        }

        private bool CanExecuteDeleteOwnCapital()
        {
            return !EditAccessDenied && FocusedOwnCapital != null && FocusedOwnCapital.IsImported == 0;
        }

        private void ExecuteAddOwnCapital()
        {
            var x = new OwnCapitalHistory () 
            { 
                ImportDate = DateTime.Now,
                IsImported = 1,
                LegalEntityID = this.Bank.ID,
                LegalEntityName = this.Bank.FormalizedName,
                LicenseNumber = this.Bank.RegistrationNum                
            };
            if (DialogHelper.EditOwnCapital(x, this.items.ToList(), true))
            {
                RefreshList();
            }
        }

        private bool CanExecuteAddOwnCapital()
        {
            return !EditAccessDenied;
        }

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;

        public void RefreshList()
        {
            try
            {
                this.Items = new ObservableList<OwnCapitalHistory>(this.Bank.GetOwnCapitalHistory());
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                if (IsInitialized)
                    RaiseGetDataError();
                else
                    throw new ViewModelInitializeException();
            }
        }


        IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>>();
            list.Add(new KeyValuePair<Type, long>(typeof(LegalEntity), this.Bank.ID));
            return list;        
        }
    }
}
