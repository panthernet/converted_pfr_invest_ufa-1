﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelImportExport.XLSModels;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ImportBankConclusionDlgViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }
        public ObservableList<BankConclusion> Items { get; }

        public List<string> FileTypesList { get; set; } = new List<string>
        {
            "Удовлетворяет требованиям постановления правительства РФ № 761",
            "Наличие международных санкций",
            "Участие в системе обязательного страхования вкладов физических лиц",
            "Наличие на сайте ГК АСВ информации о докапитализации",
            "Наличие в перечне в соответствии с ч.3 ст.2 от 21.07.2014 № 213-ФЗ"
        };

        public int SelectedFileTypeIndex
        {
            get { return _selectedFileTypeIndex; }
            set { _selectedFileTypeIndex = value; OnPropertyChanged(nameof(SelectedFileTypeIndex));}
        }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set
            {
                if (_fileName != value)
                {
                    _fileName = value;
                    OnPropertyChanged("FileName");
                    OnPropertyChanged("ImportDateString");
                    OnPropertyChanged("ImportDate");
                    OnPropertyChanged("KOImport");
                };
            }
        }

        public string ImportDateString => GetImportDateFromFileName();

        public DateTime ImportDate => GetImportDate();

        public BanksConclusion _KOImport;
        private int _selectedFileTypeIndex;

        public BanksConclusion KOImport
        {
            get
            {
                return _KOImport;
            }
            set
            {
                _KOImport = value;
                OnPropertyChanged("KOImport");
            }
        }

        private DateTime GetImportDate()
        {
            var importDate = GetImportDateFromFileName();
            if (string.IsNullOrWhiteSpace(importDate)) return DateTime.MinValue;

            DateTime dt;
            return !DateTime.TryParseExact(ImportDateString, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? DateTime.MinValue : DateTime.ParseExact(ImportDateString, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
        }

        public ImportBankConclusionDlgViewModel()
            : base(typeof(BankConclusionListViewModel))
        {
            ID = 0;
            OpenFileCommand = new DelegateCommand<object>(OnOpenFile);
            ImportCommand = new DelegateCommand(o => Items.Count > 0, ExecuteImport);
            Items = new ObservableList<BankConclusion>();
        }

        public bool ValidateCanImport(long elementId)
        {
            if (string.IsNullOrWhiteSpace(FileName)) return true;

            if (KOImport == null)
                KOImport = DataContainerFacade.GetListByPropertyConditions<BanksConclusion>(ListPropertyCondition.Equal("ImportDate", ImportDate),
                    ListPropertyCondition.Equal("ElementID", elementId)).FirstOrDefault();

            // Первый импорт 
            if (KOImport == null)
                return true;
            // Повторный импорт
            return DialogHelper.ConfirmImportKORewrite();
        }

        private void ExecuteImport(object o)
        {
            long elementId;
            switch (SelectedFileTypeIndex)
            {
                case 0:
                    elementId = 115001;
                    break;
                case 1:
                    elementId = 115002;
                    break;
                case 2:
                    elementId = 115003;
                    break;
                case 3:
                    elementId = 115004;
                    break;
                case 4:
                    elementId = 115005;
                    break;
                default:
                    throw new Exception();
            }

            if (!ValidateCanImport(elementId))
                return;

            try
            {
                if (BLServiceSystem.Client.ImportKO761(KOImport?.ID ?? -1, ImportDate, Items, true, elementId))
                {
                    JournalLogger.LogModelEvent(this, JournalEventType.IMPORT_DATA); //String.Join(",", Items.Select(item => item.Name).ToArray<String>())                   
                    DialogHelper.ShowAlert("Импорт завершён");                    
                }
                else
                    DialogHelper.ShowAlert("Во время выполнения операции импорта возникла ошибка'. Детальная информация об ошибке может быть найдена в логах приложения.");
            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert($"Во время выполнения операции импорта возникла ошибка '{ex.Message}'. Детальная информация об ошибке может быть найдена в логах приложения.");
                Logger.WriteException(ex);
            }

            RefreshConnectedViewModels();
        }

        private void OnOpenFile(object o)
        {
            string mask;
            switch (SelectedFileTypeIndex)
            {
                case 0:
                    mask = "Файлы кредитных организаций (*.xls,*.xlsx)|LB761*.xls?";
                    break;
                case 1:
                    mask = "Файлы кредитных организаций (*.xls,*.xlsx)|SANCTIONS*.xls?";                    
                    break;
                case 2:
                    mask = "Файлы кредитных организаций (*.xls,*.xlsx)|SSV*.xls?";
                    break;
                case 3:
                    mask = "Файлы кредитных организаций (*.xls,*.xlsx)|CAPITALIZATION*.xls?";
                    break;
                case 4:
                    mask = "Файлы кредитных организаций (*.xls,*.xlsx)|213FZ*.xls?";
                    break;
                default:
                    throw new Exception("Неизвестный тип файла!");
            }
            FileName = DialogHelper.OpenFile(mask);
            Items.Clear();

            if (FileName == null)
                return;

            var oldCI = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            try
            {
                var error = string.Empty;

                if (!CheckKOFileName(FileName, out error))
                    throw new Exception(error);

                var doc = new XLSBaseKOImport(FileName);
                foreach (var row in doc.Rows)
                {
                    var item = new BankConclusion
                    {
                        ImportDate = ImportDate,
                        Name = row.Items[2].ToString(),
                        Number = row.Items[1].ToString(),
                        Code = row.Items[3].ToString()
                    };
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert(ex.Message);
                Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCI;
            }
        }

        private string GetImportDateFromFileName()
        {
            string error;
            return !CheckKOFileName(FileName, out error) ? string.Empty : Path.GetFileNameWithoutExtension(FileName).Split('_')[1];
        }

        private bool CheckKOFileName(string iFileName, out string error)
        {
            error = string.Empty;
            var fileName = Path.GetFileNameWithoutExtension(iFileName);
            if (string.IsNullOrWhiteSpace(fileName))
            {
                error = $"Некорректное имя файла '{fileName}.'";
                return false;
            }

            //LB761_2013-09-09.xls
            var parts = fileName.Split('_');
            if (parts.Length != 2 || parts[1].Split('-').Length != 3)
            {
                error = $"Некорректное имя файла '{fileName}.' Имя файла должно содержать дату в формате год-месяц-день.";
                return false;
            }

            var date = parts[1];
            parts = date.Split('-');
            foreach (var num in parts)
            {
                var number = 0;
                if (!int.TryParse(num, out number))
                {
                    error = $"Некорректное имя файла '{fileName}.' Имя файла должно содержать дату в формате год-месяц-день.";
                    return false;
                }

            }

            DateTime importDate;
            if (!DateTime.TryParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out importDate) || importDate == DateTime.MinValue)
            {
                error = $"Некорректная дата импорта в названии имени файла '{fileName}.' Имя файла должно содержать дату в формате год-месяц-день.";
                return false;
            }

            return true;
        }

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;
    }
}
