﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.BusinessLogic.XMLModels;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ImportDepClaimBatchDlgViewModel : ImportDepClaimBaseViewModel, IVmNoDefaultLogging
    {
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }
        public ObservableList<DepClaim2> PDX02Items { get; }
		public RepositoryImpExpFile PDX02RepLog { get; private set; }
        public ObservableList<DepClaimMaxListItem> PDX08Items { get; }
		public RepositoryImpExpFile PDX08RepLog { get; private set; }
		public ObservableList<DepClaimMaxListItem> PDX09Items { get; }
		public RepositoryImpExpFile PDX09RepLog { get; private set; }
		public DepClaimSelectParams Auction { get; protected set; }
        private const string FORMAT = "dd.MM.yyyy";
        public IList<DepClaim2> ExistingsDepClaim2List { get; protected set; }


        public bool IsFileSelected => !(string.IsNullOrEmpty(PDX02FileName));

        private string _fileName;
        public string PDX02FileName
        {
            get { return _fileName; }
            set { if (_fileName != value) { _fileName = value; OnPropertyChanged("PDX02FileName"); OnPropertyChanged("IsFileSelected"); } }
        }

        public string PDX08FileName { get; set; }

        public string PDX09FileName { get; set; }

        public string PDX02ShortName => Path.GetFileName(PDX02FileName);

        public string PDX08ShortName => Path.GetFileName(PDX08FileName);

        public string PDX09ShortName => Path.GetFileName(PDX09FileName);

        public DateTime SelectDate => Auction.SelectDate;

        public ImportDepClaimBatchDlgViewModel(long auctionID)
            : base(typeof(DepClaim2ListViewModel),
                    typeof(DepClaim2ConfirmListViewModel),
                    typeof(DepClaimSelectParamsListViewModel),
                    typeof(DepClaimSelectParamsViewModel),
                    typeof(DepClaimMaxListViewModel))
        {
            ID = auctionID;
            //this.AuctionID = auctionID;
            OpenFileCommand = new DelegateCommand<object>(OnOpenFile);
            ImportCommand = new DelegateCommand(o => PDX02Items.Count > 0 && PDX08Items.Count > 0 && PDX09Items.Count > 0, ExecuteImport);
            PDX02Items = new ObservableList<DepClaim2>();
            PDX08Items = new ObservableList<DepClaimMaxListItem>();
            PDX09Items = new ObservableList<DepClaimMaxListItem>();
            UpdateAuction(auctionID);
        }

        public void UpdateAuction(long id)
        {
            Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(id);
            ExistingsDepClaim2List = Auction.GetDepClaim2List();
            OnPropertyChanged("SelectDate");
        }

        public override bool ValidateCanImport()
        {
			if (!base.ValidateCanImport())
			{
				return false;
			}

            if (ExistingsDepClaim2List.Count > 0)
            {
                DialogHelper.ShowAlert("Аукцион находится в статусе, для которого невозможен импорт заявок");
                return false;
            }
            return true;
        }

        private void ExecuteImport(object o)
        {
            IsAttemptFailed = false;
            if (!ValidateCanImport())
            {
                IsAttemptFailed = true;
                return;
            }
            if (ExecutePDX02Import() && ExecutePDX08Import())
                IsAttemptFailed = !ExecutePDX09Import();
            else IsAttemptFailed = true;
            RefreshConnectedViewModels();
        }

        private bool ExecutePDX02Import()
        {
			var repID = DataContainerFacade.Save(PDX02RepLog);
			foreach (var i in PDX02Items) { i.RepositoryID = repID; }

            var result = BLServiceSystem.Client.AuctionImportDepClaim2(ID, PDX02Items, true);
			if (result.IsSuccess)
			{
                if(IsVerbose)
				    DialogHelper.ShowAlert("Импорт завершён");
				if (PDX02Items != null && PDX02Items.Count > 0)
				{
					JournalLogger.LogEvent("Импорт данных", JournalEventType.IMPORT_DATA, null, "Импорт выписки из реестра заявок",
						   string.Join(",", PDX02Items.Select(item => item.FirmName).ToArray())
							);
				}
				return true;
			}
			else
			{
				DataContainerFacade.Delete(PDX02RepLog);
				DialogHelper.ShowError(result.ErrorMessage);
				return false;
			}
        }

        private bool ExecutePDX08Import()
        {
			var repID = DataContainerFacade.Save(PDX08RepLog);
			
			foreach (var item in PDX08Items)
            {
				item.RepositoryID = repID;
                DataContainerFacade.Save(item);
            }

            string msg = $@"Импорт сводного реестра заявок для аукциона от {Auction.SelectDate.Date:dd.MM.yyyy} выполнен.";
            JournalLogger.LogModelEvent(this, JournalEventType.IMPORT_DATA, null, msg);
			return true;
        }

        private void ClearLists()
        {
            PDX02Items.Clear();
            PDX08Items.Clear();
            PDX09Items.Clear();
        }

        private bool ExecutePDX09Import()
        {
            try
            {
                var repID = DataContainerFacade.Save(PDX09RepLog);

                foreach (var item in PDX09Items)
                {
                    item.RepositoryID = repID;
                    DataContainerFacade.Save(item);
                }

                string msg = $@"Импорт заявок с учетом максимальной суммы размещения для аукциона от {Auction.SelectDate.Date:dd.MM.yyyy} выполнен.";
                JournalLogger.LogModelEvent(this, JournalEventType.IMPORT_DATA, null, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void OnOpenFile(object o)
        {
            PDX02FileName = DialogHelper.OpenFile("Файлы заявок PDX02 (*.xml)|MD03562_PDX02_*_*.xml");
            ClearLists();
            if (PDX02FileName == null)
                return;

            const string fileMissing = "Комплект файлов неполный. Файл, соответствующий шаблону {0}, не найден.";
            const string manyFiles = "Не удалось найти однозначное соответствие комплекта файлов для файла выписки из реестра заявок ";

            var pdxNameFormat = new Regex("(?<prefix>MD\\d+)_(?<type>PDX0\\d)_(?<num1>\\d+)_(?<num2>\\d+).xml");

            var match = pdxNameFormat.Match(PDX02FileName);

            if (!match.Success)
            {
                DialogHelper.ShowAlert($"Не удалось распознать части наименования файла {PDX02ShortName}");
                return;
            }

            var mask08 = "MD03562_PDX08_" + match.Groups["num1"] + "_*.xml";
            var mask09 = "MD03562_PDX09_" + match.Groups["num1"] + "_*.xml";

            var pdx08Files = Directory.EnumerateFiles(Path.GetDirectoryName(PDX02FileName), mask08).ToList();
            var pdx09Files = Directory.EnumerateFiles(Path.GetDirectoryName(PDX02FileName), mask09).ToList();

            if (!pdx09Files.Any() && !pdx08Files.Any())
            {
                DialogHelper.ShowAlert("Комплект файлов неполный. Не удалось найти файлы, соответствующие шаблонам {0} и {1}", mask08, mask09);
                return;
            }

            if (!pdx08Files.Any())
            {
                DialogHelper.ShowAlert(fileMissing, mask08);
                return;
            }

            if (!pdx09Files.Any())
            {
                DialogHelper.ShowAlert(fileMissing, mask09);
                return;
            }

            if (pdx08Files.Count > 1 || pdx09Files.Count > 1)
            {
                DialogHelper.ShowAlert(manyFiles);
                return;
            }

            PDX08FileName = pdx08Files.First();
            PDX09FileName = pdx09Files.First();

            if (!OnOpenPDX02() || !OnOpenPDX08() || !OnOpenPDX09())
            {
                ClearLists();
            }
        }

        private bool OnOpenPDX02()
        {
            if (!DocumentBase.IsValidFileName(PDX02FileName))
            {
                DialogHelper.ShowAlert("Неверный формат названия импортируемого файла.");
                return false;
            }
            var serializer = new XmlSerializer(typeof(DocumentPDX02));
            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX02.xsd"));

            try
            {
                var missed = new List<PDX02Record>();
                var wrongNameList = new List<PDX02Record>();

                using (var reader = XmlReader.Create(PDX02FileName, settings))
                {

                    var doc = (DocumentPDX02)serializer.Deserialize(reader);
                    foreach (var r in doc.Body.Records)
                    {
                        var hasDate = (doc.Info.DateValue != DateTime.MinValue);
                        //var bL =   DataContainerFacade.GetListByProperty<LegalEntity>("StockCode", r.FIRMID);
                        var bL = BLServiceSystem.Client.GetBankListByStockCode(r.FIRMID);

                        if (!bL.Any())
                        {
                            missed.Add(r);
                            continue;
                        }

                        var bank = bL.First();

                        //Validate Bank Name
                        if (bank.FormalizedName.ToLower() != r.FIRMNAME.ToLower())
                        {
                            wrongNameList.Add(r);
                            continue;
                        }

                        var item = new DepClaim2
                        {
                            Amount = r.AMOUNT,
							OldAmount = r.AMOUNT,
                            Rate = r.RATE,
                            Payment = r.PAYMENT,
							OldPayment = r.PAYMENT,
							OldRate = r.RATE,
                            SettleDate = r.SETTLEDATE,
                            ReturnDate = r.RETURNDATE,
                            SecurityID = r.SECURITYID,
                            Status = DepClaim2.Statuses.New,
                            FirmName = r.FIRMNAME,
                            FirmID = r.FIRMID,
                            AuctionDate = Auction.SelectDate,
                            AuctionID = Auction.ID,
                            BankID = bank.ID,
							RecNum = r.RECNUM,
                            DocumentDate = hasDate ? (DateTime?)doc.Info.DateValue : null,
                            DocumentTime = hasDate ? (TimeSpan?)doc.Info.TimeValue : null,
                        };

                        if (item.Term == 0 || item.Part2Sum == 0)
                        {
                            item.Part2Sum = item.Amount + item.Payment;
							item.OldPart2Sum = item.Part2Sum;
                            item.Term = (item.ReturnDate - item.SettleDate).Days;
                        }

                        PDX02Items.Add(item);
                    }
                }
                if (missed.Count > 0 || wrongNameList.Count > 0)
                {
                    ClearLists();

                    var sb = new StringBuilder();
                    sb.Append("Импорт заявок невозможен, т.к. ");

                    if (missed.Any())
                        sb.AppendFormat("для указанных банков : {0} не задан биржевой код", string.Join(", ", missed.Select(m => $"\"{m.FIRMNAME}\"")));

                    if (missed.Any() && wrongNameList.Count > 0)
                        sb.Append(", также ");

                    if (wrongNameList.Count > 0)
                        sb.AppendFormat("для банков(биржевых кодов) : {0} название банка не совпадает c формализованным наименованием в справочнике банков", string.Join(", ", wrongNameList.Select(m =>
                            $"\"{m.FIRMNAME}({m.FIRMID})\"")));


                    var msg = sb.ToString();
                    DialogHelper.ShowAlert(msg);
                    return false;
                }
                //проверка на дупликаты идентификаторов
                var idstr = new StringBuilder();
                PDX02Items.GroupBy(x => x.RecNum)
                    .Where(group => group.Count() > 1)
                    .Select(group => group.Key).Distinct().ForEach(a =>
                    {
                        idstr.Append(a);
                        idstr.Append(",");
                    });
                var idString = idstr.ToString();
                if (!string.IsNullOrEmpty(idString))
                {
                    idString = idString.Remove(idString.Length - 1, 1);
                    DialogHelper.ShowError("Импортируемые заявки содержат повторяющиеся идентификаторы: " + idString);
                    ClearLists();
                    return false;
                }
                PDX02RepLog = GetRepositoryLogRecord(PDX02FileName);
                return true;
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        DialogHelper.ShowAlert(
                                "Содержимое файла {2} имеет неверный формат: {0}\n\nПодробности - {1}",
                                ex.Message,
                                ex.InnerException.Message,
                                PDX02ShortName);
                    }
                    else
                    {
                        DialogHelper.ShowAlert(
                                "Содержимое файла {3} имеет неверный формат: {0}\n\nОписание - {1}\n\nПодробности - {2}",
                                ex.Message,
                                ex.InnerException.InnerException.Message,
                                ex.InnerException.Message,
                                PDX02ShortName);
                    }

                }

                else
                {
                    DialogHelper.ShowAlert("Неверный формат документа {0}", PDX02ShortName);
                    Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка открытия документа {0}", PDX02ShortName);
                Logger.WriteException(ex);
            }
            return false;
        }

        private bool OnOpenPDX08()
        {
            if (string.IsNullOrEmpty(PDX08FileName)) return false;

            if (!DocumentBase.IsValidFileName(PDX08FileName))
            {
                DialogHelper.ShowAlert("Неверный формат названия импортируемого файла: {0}", PDX08ShortName);
                return false;
            }
            var serializer = new XmlSerializer(typeof(DocumentPDX08));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX08.xsd"));

            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (var reader = XmlReader.Create(PDX08FileName, settings))
                {
                    var doc = (DocumentPDX08)serializer.Deserialize(reader);

                    if (!doc.Body.Tab.TradeDate.Date.Equals(Auction.SelectDate))
                    {

                        DialogHelper.ShowAlert(
                                @"Импортируемый файл {2} содержит данные за {0}. Выберите файл с данными аукциона за {1}",
                                doc.Body.Tab.TradeDate.ToString(FORMAT),
                                Auction.SelectDate.ToString(FORMAT),
                                PDX08ShortName);
                        return false;
                    }

                    foreach (var r in doc.Body.Tab.Board.Records)
                    {
                        var item = new DepClaimMaxListItem
                        {
                            Auctno = doc.Body.Tab.Auctno,
                            BoardId = doc.Body.Tab.Board.BoardId,
                            DepclaimType = (int)DepClaimMaxListItem.Types.Max,
                            DepclaimselectparamsId = Auction.ID,
                            DocDate = doc.Info.DateValue,
                            DocTime = new TimeSpan(),
                            TradeDate = doc.Body.Tab.TradeDate,
                            SecurityId = r.SECURITYID,
                            QuantPart = r.QUANT_PART,
                            QuantReq = r.QUANT_REQ,
                            Rate = r.RATE,
                            SettleDate = r.SETTLEDATE,
                            SettleDate2 = r.SETTLEDATE2,
                            Totval1 = r.TOTVAL1,
                            Totval1N = r.TOTVAL1N,
                            Totval2 = r.TOTVAL2,
                            Warate = r.WARATE,
                            SelectDate = Auction.SelectDate
                        };
                        PDX08Items.Add(item);
                    }
                }
                //if (!IsCorrectSettleDate())
                //{
                //    DialogHelper.Alert("В импортируемых заявках дата размещения средств в депозит больше даты возврата средств.");
                //    this.Items.Clear();
                //}
				PDX08RepLog = GetRepositoryLogRecord(PDX08FileName);
                return true;
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        DialogHelper.ShowAlert(
                                "Содержимое файла {2} имеет неверый формат: {0}\n\nПодробности - {1}",
                                ex.Message,
                                ex.InnerException.Message,
                                PDX08ShortName);
                    }
                    else
                    {
                        DialogHelper.ShowAlert(
                                "Содержимое файла {3} имеет неверый формат: {0}\n\nОписание - {1}\n\nПодробности - {2}",
                                ex.Message,
                                ex.InnerException.InnerException.Message,
                                ex.InnerException.Message,
                                PDX08ShortName);
                    }

                }

                else
                {
                    DialogHelper.ShowAlert("Неверный формат документа {0}", PDX08ShortName);
                    Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка открытия документа {0}", PDX08ShortName);
                Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }
            return false;
        }

        private bool OnOpenPDX09()
        {
            if (string.IsNullOrEmpty(PDX09FileName)) return false;
            if (!DocumentBase.IsValidFileName(PDX09FileName))
            {
                DialogHelper.ShowAlert("Неверный формат названия импортируемого файла: {0}", PDX09ShortName);
                return false;
            }

            var serializer = new XmlSerializer(typeof(DocumentPDX09));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX09.xsd"));

            try
            {
                using (var reader = XmlReader.Create(PDX09FileName, settings))
                {
                    var doc = (DocumentPDX09)serializer.Deserialize(reader);

                    if (!doc.Body.Tab.TradeDate.Date.Equals(Auction.SelectDate))
                    {
                        DialogHelper.ShowAlert(@"Импортируемый файл {2} содержит данные за {0}. Выберите файл с данными аукциона за {1}",
                           doc.Body.Tab.TradeDate.ToString(FORMAT), Auction.SelectDate.ToString(FORMAT), PDX09ShortName);
                        return false;
                    }

                    foreach (var r in doc.Body.Tab.Board.Records)
                    {
                        var item = new DepClaimMaxListItem
                        {
                            Auctno = doc.Body.Tab.Auctno,
                            BoardId = doc.Body.Tab.Board.BoardId,
                            DepclaimType = (int)DepClaimMaxListItem.Types.Common,
                            DepclaimselectparamsId = Auction.ID,
                            DocDate = doc.Info.DateValue,
                            DocTime = new TimeSpan(),
                            TradeDate = doc.Body.Tab.TradeDate,
                            SecurityId = r.SECURITYID,
                            QuantPart = r.QUANT_PART,
                            QuantReq = r.QUANT_REQ,
                            Rate = r.RATE,
                            SettleDate = r.SETTLEDATE,
                            SettleDate2 = r.SETTLEDATE2,
                            Totval1 = r.TOTVAL1,
                            Totval1N = r.TOTVAL1N,
                            Totval2 = r.TOTVAL2,
                            Warate = r.WARATE,
                            SelectDate = Auction.SelectDate

                        };
                        PDX09Items.Add(item);
                    }
                }
                if (!IsCorrectSettleDate())
                {
                    DialogHelper.ShowAlert("В импортируемых заявках файла {0} дата размещения средств в депозит больше даты возврата средств.", PDX09ShortName);
                    return false;
                }
				PDX09RepLog = GetRepositoryLogRecord(PDX09FileName);
                return true;
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        DialogHelper.ShowAlert(
                                "Содержимое файла {2} имеет неверный формат: {0}\n\nПодробности - {1}",
                                ex.Message,
                                ex.InnerException.Message,
                                PDX09ShortName);
                    }
                    else
                    {
                        DialogHelper.ShowAlert(
                            string.Format(
                                "Содержимое файла {3} имеет неверный формат: {0}\n\nОписание - {1}\n\nПодробности - {2}",
                                ex.Message,
                                ex.InnerException.InnerException.Message,
                                ex.InnerException.Message,
                                PDX09ShortName));
                    }

                }

                else
                {
                    DialogHelper.ShowAlert("Неверный формат документа {0}", PDX09ShortName);
                    Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка открытия документа {0}", PDX09ShortName);
                Logger.WriteException(ex);
            }
            return false;
        }

        private bool IsCorrectSettleDate()
        {
            var isCorrect = !PDX09Items.All(c => c.SettleDate > c.SettleDate2);
            return isCorrect;
        }

		private RepositoryImpExpFile GetRepositoryLogRecord(string fileName)
		{
			var dt = DateTime.Now;
			var repositoryLogRecord = new RepositoryImpExpFile
			{
				AuctionDate = Auction.SelectDate,
				AuctionLocalNum = Auction.LocalNum,
				DDate = dt,
				ImpExp = 1,
				Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(fileName)),
				Status = 0,
				TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
				UserName = AP.Provider.UserName,
				Key = (int)RepositoryImpExpFile.Keys.AucImportPacket,
				Operacia = "Импорт пакета XML от ММВБ",
				Comment = "Работа с ЦБ и депозитами - Депозиты - Импорт - Импорт пакета XML от ММВБ",
			};
			return repositoryLogRecord;
		}
        
        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;
    }
}
