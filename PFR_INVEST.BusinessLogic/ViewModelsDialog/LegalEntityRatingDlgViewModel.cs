﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class LegalEntityRatingDlgViewModel : ViewModelBase
    {
        public LegalEntityRating Rating { get; private set; }

        public IList<RatingAgency> Agencies { get; private set; }
        public IList<Rating> Ratings { get; private set; }
        public IList<MultiplierRating> MultiplierRatings { get; private set; }
        public bool IsAgencyReadOnly { get; private set; }

        private MultiplierRating multiplierRating;
        public MultiplierRating MultiplierRating
        {
            get
            {
                return multiplierRating;
            }
            set
            {
                if (multiplierRating != value)
                {
                    multiplierRating = value;
                    Rating.MultiplierRatingID = value.ID;
                    //Rating.Multiplier = value.Multiplier;
                    OnPropertyChanged("Multiplier");
                }
            }
        }

        private long ratingID;
        public long RatingID
        {
            get
            {
                return ratingID;
            }
            set
            {
                if (ratingID != value)
                {
                    ratingID = value;
                    //Rating.RatingID = value;
                    MultiplierRating = MultiplierRatings.First(x => x.RatingID == value);
                    OnPropertyChanged("RatingID");
                }
            }
        }

        private long agencyID;
        public long AgencyID
        {
            get
            {
                return agencyID;
            }
            set
            {
                if (agencyID != value)
                {
                    agencyID = value;
                    //Rating.AgencyID = value;
                    MultiplierRating = null;
                    MultiplierRatings = DataContainerFacade.GetListByProperty<MultiplierRating>("AgencyID", agencyID);
                    Ratings = DataContainerFacade.GetList<Rating>().Where(x => MultiplierRatings.Any(mr => mr.RatingID == x.ID)).ToList();
                    OnPropertyChanged("AgencyID");
                    OnPropertyChanged("Ratings");
                    OnPropertyChanged("IsRatingReadOnly");
                }
            }
        }

        public decimal? Multiplier => MultiplierRating != null ? MultiplierRating.Multiplier : null;

        public bool IsRatingReadOnly
        {
            get
            {
                return !Agencies.Any(x => x.ID == agencyID);
            }
        }

        public DelegateCommand<object> SaveCard { get; private set; }

        public LegalEntityRatingDlgViewModel(LegalEntityRating rating, bool isAgencyReadOnly, IList<long> excludedAgenceis)
        {
            this.IsAgencyReadOnly = isAgencyReadOnly;
            this.Agencies = DataContainerFacade.GetList<RatingAgency>().Where(a => !excludedAgenceis.Contains(a.ID)).ToList();
            this.Ratings = DataContainerFacade.GetList<Rating>();
            this.Rating = rating;
            if (rating.MultiplierRatingID > 0)
            {
                var mr = DataContainerFacade.GetByID<MultiplierRating>(rating.MultiplierRatingID);
                if (mr.AgencyID.HasValue)
                {
                    AgencyID = mr.AgencyID.Value;
                    if (mr.RatingID.HasValue)
                    {
                        RatingID = mr.RatingID.Value;
                    }
                }
            }
            this.SaveCard = new DelegateCommand<object>(o =>
            {
                return MultiplierRating != null && string.IsNullOrEmpty(ValidateRating());
                //return string.IsNullOrWhiteSpace(this.Rating["AgencyID"])
                //    && string.IsNullOrWhiteSpace(this.Rating["RatingID"])
                //    && string.IsNullOrWhiteSpace(this.Rating["Date"])
                //    && string.IsNullOrWhiteSpace(this.Rating["Multiplier"]);

            },
                o =>
                {

                    if (MultiplierRating.ID <= 0)
                    {
                        var mr =
                            DataContainerFacade.GetListByPropertyConditions<MultiplierRating>(
                                new List<ListPropertyCondition>()
                                {
                                    new ListPropertyCondition()
                                    {
                                        Name = "RatingID",
                                        Operation = "eq",
                                        Value =
                                            this.RatingID
                                    },
                                    new ListPropertyCondition()
                                    {
                                        Name = "AgencyID",
                                        Operation = "eq",
                                        Value =
                                            this.AgencyID
                                    }
                                }).FirstOrDefault();
                        this.Rating.MultiplierRatingID = mr != null ? mr.ID : 0;

                    }
                    else
                    {
                        this.Rating.MultiplierRatingID = MultiplierRating.ID;
                    }
                });



            //if (this.Rating.AgencyID == 0 && this.Agencies.Count == 1)
            //    this.Rating.AgencyID = this.Agencies.First().ID;
            //if (!this.Rating.Multiplier.HasValue)
            //    this.Rating.Multiplier = 0.0M;
        }

        private string ValidateRating()
        {
            const string validateFields = "MultiplierRatingID|Date";

            foreach (string key in validateFields.Split('|'))
            {
                var error = Rating[key];
                if (!string.IsNullOrEmpty(error))
                {
                    return error;
                }
            }
            return null;
        }

    }
}
