﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.ViewModelsPrint;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    public class CBReportApprovementStatusDlgViewModel: ViewModelCardDialog
    {
        private BOReportForm1 _report;

        public bool IsApproveMode { get; }

        public bool ApproveControlsVisible1 => _report.IsApproved1 == (int)BOReportForm1.ReportStatus.InProcess && IsApproveMode && string.Equals(_report.ApproverLogin1, BLServiceSystem.Client.GetCurrentUserLogin(), StringComparison.InvariantCultureIgnoreCase) && EditAccessAllowed;
        public bool ApproveControlsVisible1Spec => _report.IsApproved1 == (int)BOReportForm1.ReportStatus.InProcess && IsApproveMode && string.Equals(_report.ApproverLogin1, BLServiceSystem.Client.GetCurrentUserLogin(), StringComparison.InvariantCultureIgnoreCase);
        public bool ApproveControlsVisible2 => _report.IsApproved2 == (int) BOReportForm1.ReportStatus.InProcess && IsApproveMode && string.Equals(_report.ApproverLogin2, BLServiceSystem.Client.GetCurrentUserLogin(), StringComparison.InvariantCultureIgnoreCase) && EditAccessAllowed;
        public bool ApproveControlsVisible2Spec => _report.IsApproved2 == (int) BOReportForm1.ReportStatus.InProcess && IsApproveMode && string.Equals(_report.ApproverLogin2, BLServiceSystem.Client.GetCurrentUserLogin(), StringComparison.InvariantCultureIgnoreCase);


        public int ReportYear => _report.Year;

        public string ReportQuartal => DateTools.GetQuarterRomeName(_report.Quartal, true);

        public string ReportOkud => _report.OKUD;

        public GridLength ControlsColumnWidth => IsApproveMode ? new GridLength(1, GridUnitType.Star) : GridLength.Auto; 

        public string Name1 => _report.Approver1;
        public string Name2 => _report.Approver2;

        public int Status1 => _report.IsApproved1;
        public int Status2 => _report.IsApproved2;

        public string StatusTip1 => _report.IsApproved1 == 2 ? "На согласовании" : (_report.IsApproved1 == 1 ? "Согласован" : "Отклонен");
        public string StatusTip2 => _report.IsApproved2 == 2 ? "На согласовании" : (_report.IsApproved2 == 1 ? "Согласован" : "Отклонен");

        public DateTime? Date1 => Status1 == (int)BOReportForm1.ReportStatus.Approved || Status1 == (int)BOReportForm1.ReportStatus.Declined ? _report.ApprovementDate1 : null;
        public DateTime? Date2 => Status2 == (int)BOReportForm1.ReportStatus.Approved || Status2 == (int)BOReportForm1.ReportStatus.Declined ? _report.ApprovementDate2 : null;

        public ICommand ApproveCommand { get; set; }
        public ICommand DisapproveCommand { get; set; }
        public ICommand OpenReportCommand { get; set; }

        public CBReportApprovementStatusDlgViewModel(long id, bool isApproveMode = false)
        {
            ID = id;
            Refresh();
            var name = BLServiceSystem.Client.GetCurrentUserLogin();
            IsApproveMode = isApproveMode;
            ApproveCommand = new DelegateCommand<string>(a =>
            {
                if (!DialogHelper.ShowConfirmation("Согласовать отчет?"))
                    return;
                if (a == "1")
                {
                    _report.IsApproved1 = (int) BOReportForm1.ReportStatus.Approved;
                    _report.ApprovementDate1 = DateTime.Today;
                }
                else
                {
                    _report.ApprovementDate2 = DateTime.Today;
                    _report.IsApproved2 = (int)BOReportForm1.ReportStatus.Approved;
                }
                DataContainerFacade.Save(_report);
                Refresh();
            });
            DisapproveCommand = new DelegateCommand<string>(a =>
            {
                if (!DialogHelper.ShowConfirmation("Отказать в согласовании отчета?"))
                    return;
                if (a == "1")
                {
                    _report.IsApproved1 = (int) BOReportForm1.ReportStatus.Declined;
                    _report.ApprovementDate1 = DateTime.Today;
                }
                else
                {
                    _report.IsApproved2 = (int)BOReportForm1.ReportStatus.Declined;
                    _report.ApprovementDate2 = DateTime.Today;
                }
                DataContainerFacade.Save(_report);
                Refresh();
            });
            OpenReportCommand = new DelegateCommand(a =>
            {
                if (!_report.FileID.HasValue)
                {
                    DialogHelper.ShowError("Для отчета не сгенерирован файл с данными. Согласование невозможно!");
                    return;
                }
                OpenCBReportFromDB(_report);
            });
        }

        public class CbReportDeserializedData3
        {
            [XmlElement("Кол1_НомерСтроки")]
            public int Number { get; set; }

            [XmlElement("Кол2_ВидАктива - Код")]
            public int TypeCode { get; set; }

            public string TypeCodeName => "Депозит";

            [XmlElement("Кол3_ИдентАктива")]
            public string Identifier { get; set; }

            [XmlElement("Кол4_НаименЭмитента")]
            public string EmName { get; set; }

            [XmlElement("Кол5_ИдентБрокера")]
            public string BrokerId { get; set; }

            [XmlElement("Кол6_КоличЦБ")]
            public int CbCount { get; set; }

            [XmlElement("Кол7_РыночнСтоим")]
            public long MarketPrice { get; set; }

            [XmlElement("Кол8_НакоплКупонДоход")]
            public long CouponIncome { get; set; }

            [XmlElement("Кол9_ТекущПроцентСтавка")]
            public decimal Rate { get; set; }

            [XmlElement("Кол10_ДатаРазмещения")]
            public DateTime PlacementDate { get; set; }

            [XmlElement("Кол11_ДатаОкончания")]
            public DateTime EndDate { get; set; }

            [XmlElement("Кол12_Периодичность - Код")]
            public int PeriodCode { get; set; }

            public string PeriodCodeName => "в конце срока";
        }

        public class CbReportData3
        {
            public DateTime ReportDate { get; set; }
            public string INN { get; set; }
            public string OGRN { get; set; }
            public string FIO { get; set; }
            public string Post { get; set; }

            public List<CbReportDeserializedData3> List { get; set; } = new List<CbReportDeserializedData3>();
        }

        public class CbReportData2
        {
            public DateTime ReportDate { get; set; }
            public string INN { get; set; }
            public string OGRN { get; set; }
            public string FIO { get; set; }
            public string Post { get; set; }

            public List<CbReportDeserializedData2> List { get; set; } = new List<CbReportDeserializedData2>();
        }

        public class CbReportDeserializedData2
        {
            [XmlElement("Кол1_Раздел3НомерСтроки")]
            public int Number { get; set; }

            [XmlElement("Код")]
            public int TypeCode { get; set; } //1 - передача, 2 - получение

            [XmlElement("Кол3_Раздел3ДатаОперации")]
            public DateTime? OperationDate { get; set; }

            [XmlElement("Кол4_Раздел3НаименованиеПолуч")]
            public string RcvName { get; set; }

            [XmlElement("Кол5_Раздел3ИНН")]
            public string INN { get; set; }

            [XmlElement("Кол6_Раздел3Назначение")]
            public string Assignmnent { get; set; }

            [XmlElement("Кол7_Раздел3НомерДатаДог")]
            public string ContractNumDate { get; set; }

            [XmlElement("Кол8_Раздел3Сумма")]
            public decimal Summ { get; set; }
        }
        

        private void OpenCBReportFromDB(BOReportForm1 report)
        {
            //TODO 11111!!!!
            ////  DialogHelper.ShowAlert("Функция временно не доступна!");
            //  return;
            try
            {
                var file = DataContainerFacade.GetByID<RepositoryNoAucImpExpFile>(report.FileID.Value);
                if (file == null)
                {
                    DialogHelper.ShowError("Файл данных для отчета не найден в БД! Не был выполнен экспорт отчета!");
                    return;
                }
                var content = GZipCompressor.Decompress(file.Repository);
                switch (report.RType)
                {
                    case BOReportForm1.ReportTypeEnum.One:
                        DialogHelper.CBReportExportToWord(_report.ID, true);
                        return;
                    case BOReportForm1.ReportTypeEnum.Two:
                        var d2 = GetDeserializedData2(content, report.Quartal, report.Year);
                        var c1 = string.Equals(_report.ApproverLogin1, BLServiceSystem.Client.GetCurrentUserLogin(), StringComparison.InvariantCultureIgnoreCase) && _report.IsApproved1 == (int) BOReportForm1.ReportStatus.InProcess;
                        var c2 = string.Equals(_report.ApproverLogin2, BLServiceSystem.Client.GetCurrentUserLogin(), StringComparison.InvariantCultureIgnoreCase) && _report.IsApproved2 == (int) BOReportForm1.ReportStatus.InProcess;
                        if (!c1 || !c2)
                        {
                            if (c1)
                            {
                                d2.List = d2.List.Where(a => !string.IsNullOrEmpty(a.ContractNumDate) && a.ContractNumDate != "-").ToList();
                                int index = 1;
                                d2.List.ForEach(a =>
                                {
                                    a.Number = index++;
                                });
                            }
                            if (c2)
                            {
                                d2.List = d2.List.Where(a => string.IsNullOrEmpty(a.ContractNumDate) || a.ContractNumDate == "-").ToList();
                                int index = 1;
                                d2.List.ForEach(a =>
                                {
                                    a.Number = index++;
                                });
                            }
                        }
                        else
                        {
                            d2.List = d2.List.OrderBy(a => a.Number).ToList();
                        }
                        GenerateExcelReport(d2);
                        break;
                    case BOReportForm1.ReportTypeEnum.Three:
                        var d3 = GetDeserializedData3(content, report.Quartal, report.Year);
                        GenerateExcelReport(d3);
                        break;
                    default:
                        throw new Exception("Неизвестный тип отчета!");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Generate excel CB report");
                DialogHelper.ShowError("При построении отчета возникла ошибка. Детальная информация доступна в логе приложения!");
            }
        }

        private static void GenerateExcelReport(object result)
        {
            var f2 = result as CbReportData2;
            if (f2 != null)
            {
                new PrintCbReportForm(f2).Print();
                return;
            }
            var f3 = result as CbReportData3;
            if (f3 != null)
            {
                new PrintCbReportForm(f3).Print();
                return;
            }
        }

        private static CbReportData3 GetDeserializedData3(byte[] content, int quarter, int year)
        {
            var result = new CbReportData3();
            using (var r = new StringReader(Encoding.Default.GetString(content)))
            {
                XDocument doc = XDocument.Load(r);
                XNamespace av = "http://www.it.ru/Schemas/Avior/ПУРЦБ";
                var list =
                    doc.Document.Descendants(av + "ОКУД0418003")
                        .FirstOrDefault()
                        .Descendants(av + "Отчетность")
                        .FirstOrDefault()
                        .Descendants(av + "Таблица_СведенияОбАктивах")
                        .FirstOrDefault()
                        .Descendants(av + "Таб_СведенияОбАктивахСтроки_1");
                var pismo = doc.Document?.Descendants(av + "ОКУД0418003").FirstOrDefault()?.Descendants(av + "СопроводительноеПисьмо").FirstOrDefault();
                var person = pismo?.Descendants(av + "ПодписьЕдиноличногоИсполнительногоОргана").FirstOrDefault();

                int month;
                if (quarter == 4)
                    month = 12;
                else month = DateTools.GetLastMonthOfQuarterForBO(quarter) - 1;
                result.ReportDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                result.INN = pismo?.Descendants(av + "ИНН").FirstOrDefault()?.Value;
                result.OGRN = pismo?.Descendants(av + "ОГРН").FirstOrDefault()?.Value;
                result.FIO = $"{person?.Descendants(av+ "ФамилияПодписанта").FirstOrDefault()?.Value} {person?.Descendants(av + "ИмяПодписанта").FirstOrDefault()?.Value} {person?.Descendants(av + "ОтчествоПодписанта").FirstOrDefault()?.Value}";
                result.Post = person?.Descendants(av + "ДолжностьПодписанта").FirstOrDefault()?.Value;

                foreach (var element in list)
                {
                    int num1;
                    long l1;
                    DateTime dt;
                    decimal d;
                    var item = new CbReportDeserializedData3();
                    if (int.TryParse(element.Descendants(av + "Кол1_НомерСтроки").FirstOrDefault()?.Value, out num1))
                        item.Number = num1;
                    if (int.TryParse(element.Descendants(av + "Кол2_ВидАктива").FirstOrDefault().Descendants(av + "Код").FirstOrDefault().Value, out num1))
                        item.TypeCode = num1;
                    item.Identifier = element.Descendants(av + "Кол3_ИдентАктива").FirstOrDefault().Value;
                    item.EmName = element.Descendants(av + "Кол4_НаименЭмитента").FirstOrDefault().Value;
                    item.BrokerId = element.Descendants(av + "Кол5_ИдентБрокера").FirstOrDefault().Value;
                    if (int.TryParse(element.Descendants(av + "Кол6_КоличЦБ").FirstOrDefault()?.Value, out num1))
                        item.CbCount = num1;
                    if (long.TryParse(element.Descendants(av + "Кол7_РыночнСтоим").FirstOrDefault()?.Value, out l1))
                        item.MarketPrice = l1;
                    if (long.TryParse(element.Descendants(av + "Кол8_НакоплКупонДоход").FirstOrDefault()?.Value, out l1))
                        item.CouponIncome = l1;
                    if (decimal.TryParse(element.Descendants(av + "Кол9_ТекущПроцентСтавка").FirstOrDefault().Value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out d))
                        item.Rate = d;
                    if (DateTime.TryParse(element.Descendants(av + "Кол10_ДатаРазмещения").FirstOrDefault().Value, out dt))
                        item.PlacementDate = dt;
                    if (DateTime.TryParse(element.Descendants(av + "Кол11_ДатаОкончания").FirstOrDefault().Value, out dt))
                        item.EndDate = dt;
                    if (int.TryParse(element.Descendants(av + "Кол12_Периодичность").FirstOrDefault().Descendants(av + "Код").FirstOrDefault().Value, out num1))
                        item.PeriodCode = num1;

                    result.List.Add(item);
                }
            }

            return result;
        }

        private static CbReportData2 GetDeserializedData2(byte[] content, int quarter, int year)
        {
            var result = new CbReportData2();
            using (var r = new StringReader(Encoding.Default.GetString(content)))
            { 
                XDocument doc = XDocument.Load(r);
                XNamespace av = "http://www.it.ru/Schemas/Avior/ПУРЦБ";
                var list =
                    doc.Document.Descendants(av + "ОКУД0418002")
                        .FirstOrDefault()
                        .Descendants(av + "Раздел3_ОперацииПередачаДенСр")
                        .FirstOrDefault()
                        .Descendants(av + "Таблица_ОперацииПередачаДенСр")
                        .FirstOrDefault()
                        .Descendants(av + "Таб_Раздел3Строки_1");
                var pismo = doc.Document?.Descendants(av + "ОКУД0418003").FirstOrDefault()?.Descendants(av + "СопроводительноеПисьмо").FirstOrDefault();
                var person = pismo?.Descendants(av + "ПодписьЕдиноличногоИсполнительногоОргана").FirstOrDefault();

                int month;
                if (quarter == 4)
                    month = 12;
                else month = DateTools.GetLastMonthOfQuarterForBO(quarter) - 1;
                result.ReportDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

                result.INN = doc.Document?.Descendants(av + "ОКУД0418002").FirstOrDefault()?.Descendants(av + "СопроводительноеПисьмо").FirstOrDefault()?
                    .Descendants(av + "ИНН").FirstOrDefault()?.Value;
                result.OGRN = doc.Document?.Descendants(av + "ОКУД0418002").FirstOrDefault()?.Descendants(av + "СопроводительноеПисьмо").FirstOrDefault()?
                    .Descendants(av + "ОГРН").FirstOrDefault()?.Value;
                result.FIO = $"{person?.Descendants(av + "ФамилияПодписанта").FirstOrDefault()?.Value} {person?.Descendants(av + "ИмяПодписанта").FirstOrDefault()?.Value} {person?.Descendants(av + "ОтчествоПодписанта").FirstOrDefault()?.Value}";
                result.Post = person?.Descendants(av + "ДолжностьПодписанта").FirstOrDefault()?.Value;

                foreach (var element in list)
                {
                    int num1;
                    DateTime dt;
                    decimal d;
                    var item = new CbReportDeserializedData2();
                    if (int.TryParse(element.Descendants(av + "Кол1_Раздел3НомерСтроки").FirstOrDefault()?.Value, out num1))
                        item.Number = num1;
                    if (int.TryParse(element.Descendants(av + "Кол2_Раздел3ТипОперации").FirstOrDefault().Descendants(av + "Код").FirstOrDefault().Value, out num1))
                        item.TypeCode = num1;
                    if (DateTime.TryParse(element.Descendants(av + "Кол3_Раздел3ДатаОперации").FirstOrDefault().Value, out dt))
                        item.OperationDate = dt;
                    item.RcvName = element.Descendants(av + "Кол4_Раздел3НаименованиеПолуч").FirstOrDefault().Value;
                    item.INN = element.Descendants(av + "Кол5_Раздел3ИНН").FirstOrDefault().Value;
                    item.Assignmnent = element.Descendants(av + "Кол6_Раздел3Назначение").FirstOrDefault().Value;
                    item.ContractNumDate = element.Descendants(av + "Кол7_Раздел3НомерДатаДог").FirstOrDefault().Value;

                    if (decimal.TryParse(element.Descendants(av + "Кол8_Раздел3Сумма").FirstOrDefault().Value, out d))
                        item.Summ = d;
                    result.List.Add(item);
                }
            }
            return result;
        }

        public void Refresh()
        {
            _report = DataContainerFacade.GetByID<BOReportForm1>(InternalID);
            OnPropertyChanged("Name1");
            OnPropertyChanged("Name2");
            OnPropertyChanged("Status1");
            OnPropertyChanged("StatusTip1");
            OnPropertyChanged("StatusTip2");
            OnPropertyChanged("Status2");
            OnPropertyChanged("Date1");
            OnPropertyChanged("Date2");

            OnPropertyChanged("ReportYear");
            OnPropertyChanged("ReportQuartal");
            OnPropertyChanged("ReportOkud");

            OnPropertyChanged(nameof(ApproveControlsVisible1));
            OnPropertyChanged(nameof(ApproveControlsVisible1Spec));
            OnPropertyChanged(nameof(ApproveControlsVisible2));
            OnPropertyChanged(nameof(ApproveControlsVisible2Spec));
            OnPropertyChanged(nameof(IsApproveMode));
        }
    }
}
