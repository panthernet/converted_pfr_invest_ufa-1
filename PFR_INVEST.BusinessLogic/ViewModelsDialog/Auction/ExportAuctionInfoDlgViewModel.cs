﻿
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using PFR_INVEST.DataObjects;
    using PFR_INVEST.DataObjects.XMLModel;

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ExportAuctionInfoDlgViewModel : ViewModelCard
    {
		public long AuctionID { get; set; }
				
	

		private string _Folder = null;
		public string Folder
		{
			get { return _Folder; }
			set
			{
				if (_Folder != value)
				{
					_Folder = value;
					OnPropertyChanged("Folder");
				}
			}
		}

		public XMLFileResult Document { get; set; }

		public ExportAuctionInfoDlgViewModel(long auctionId)
        {
            
            this.ID = auctionId;
			this.AuctionID = auctionId;
        }
		


        protected override void ExecuteSave()
        {
			
			//Генерируем документ
			Document = BLServiceSystem.Client.ExportAuctionInfo(this.AuctionID);
        }

        public override bool CanExecuteSave()
        {
            return Validate();
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {                   
					case "Folder": return Folder.ValidateRequired();
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return base.ValidateFields();
        }
    }
}
