﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.XMLModel.Auction;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class ImportDepClaim2_SPVBDlgViewModel : ImportDepClaimBaseViewModel, IVmNoDefaultLogging
	{

		//public long AuctionID { get; private set; }
		public DelegateCommand<object> OpenFileCommand { get; private set; }
		public DelegateCommand ImportCommand { get; private set; }
		public ObservableList<DepClaim2> Items { get; private set; }
		public DepClaimSelectParams Auction { get; private set; }
		public IList<DepClaim2> ExistingsDepClaim2List { get; private set; }
		public string SPVBCode { get; set; }

		private RepositoryImpExpFile repositoryLogRecord;

		private const string OP_NAME = "Импорт сводного реестра заявок от СПВБ";

		private string _fileName;
		public string FileName
		{
			get { return _fileName; }
			set { if (_fileName != value) { _fileName = value; OnPropertyChanged("FileName"); }; }
		}


		public ImportDepClaim2_SPVBDlgViewModel(long auctionID)
			: base(typeof(DepClaim2ListViewModel),
					typeof(DepClaim2ConfirmListViewModel),
					typeof(DepClaimSelectParamsListViewModel),
					typeof(DepClaimSelectParamsViewModel))
		{
			this.ID = auctionID;
			//this.AuctionID = auctionID;
			this.OpenFileCommand = new DelegateCommand<object>(OnOpenFile);
			this.ImportCommand = new DelegateCommand(o => this.Items.Count > 0, ExecuteImport);
			this.Items = new ObservableList<DepClaim2>();
			this.Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionID);
			this.ExistingsDepClaim2List = this.Auction.GetDepClaim2List();
		}

		public override bool ValidateCanImport()
		{
			if (this.Auction.StockId != (long)Stock.StockID.SPVB)
			{
				DialogHelper.ShowAlert("Данный тип импорта доступен только для биржи СПВБ");
				return false;
			}

			if (this.ExistingsDepClaim2List.Count(c => c.Status == DepClaim2.Statuses.New) == this.ExistingsDepClaim2List.Count)
			{
				return this.ExistingsDepClaim2List.Count <= 0 || DialogHelper.ConfirmImportDepClaim2Rewrite();
			}
			DialogHelper.ShowAlert("Аукцион находится в статусе, для которого невозможен импорт заявок");
			return false;
		}

		private void ExecuteImport(object o)
		{
			if (!this.ValidateCanImport())
				return;
			var repID = DataContainerFacade.Save(repositoryLogRecord);
			foreach (var i in this.Items) { i.RepositoryID = repID; }

			var result = BLServiceSystem.Client.AuctionImportDepClaim2(this.ID, this.Items, true);
			if (result.IsSuccess)
			{
				//Сохраняем код аукциона на бирже
				var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(Auction.ID);
				auction.SPVBCode = this.SPVBCode;
				DataContainerFacade.Save(auction);

				DialogHelper.ShowAlert("Импорт завершён");
				if (Items != null && Items.Count > 0)
				{
					JournalLogger.LogEvent("Импорт данных", JournalEventType.IMPORT_DATA, null, OP_NAME,
						   String.Join(",", Items.Select(item => item.FirmName).ToArray<String>())
							);
					//DataContainerFacade.Save(repositoryLogRecord);
				}

			}
			else
			{
				// сохраняются только успешно импортированные отчеты
				DataContainerFacade.Delete<RepositoryImpExpFile>(repID);
				DialogHelper.ShowError(result.ErrorMessage);
			}

			base.RefreshConnectedViewModels();
		}

		private void OnOpenFile(object o)
		{
			this.FileName = DialogHelper.OpenFile("Файлы заявок (*.xml)|*.xml");
			this.Items.Clear();
			if (this.FileName == null)
				return;
			//if (!DocumentBase.IsValidFileName(this.FileName))
			//{
			//    DialogHelper.ShowAlert("Неверный формат названия импортируемого файла.");
			//    return;
			//}
			XmlSerializer serializer = new XmlSerializer(typeof(SPVB_DepClaims));
			XmlReaderSettings settings = new XmlReaderSettings();
			//settings.ValidationType = ValidationType.Schema;
			//settings.Schemas.Add(string.Empty, DocumentPDX02.GetShema("PDX02.xsd"));

			try
			{

				var missed = new List<SPVB_DepClaims.DepClaimItem>();
				var wrongNameList = new List<SPVB_DepClaims.DepClaimItem>();

				using (var reader = XmlReader.Create(this.FileName, settings))
				{

					var doc = (SPVB_DepClaims)serializer.Deserialize(reader);

					if (!doc.IsValid)
					{
						DialogHelper.ShowAlert("Неверный формат документа");
						return;
					}

					this.SPVBCode = doc.Document.Body.Code;
					foreach (var r in doc.Document.Body.DepClaims)
					{
						var hasDate = (doc.Document.Body.SelectDate != DateTime.MinValue);

						var bL = BLServiceSystem.Client.GetBankListByStockCode(r.FIRMID);

						if (!bL.Any())
						{
							missed.Add(r);
							continue;
						}

						var bank = bL.First();

						//Validate Bank Name
						//if (!String.Equals(bank.FormalizedName, r.FIRMNAME, StringComparison.CurrentCultureIgnoreCase))
						//{
						//    wrongNameList.Add(r);
						//    continue;
						//}

						//Округление добаввленно временно, пока не будет указан допустимый формат
						var item = new DepClaim2()
						{
							Amount = Math.Round(r.AMOUNT, 2),
							OldAmount = Math.Round(r.AMOUNT, 2),
							Rate = Math.Round(r.RATE, 2),
							//Payment = r.PAYMENT,
							//OldPayment = r.PAYMENT,
							OldRate = Math.Round(r.RATE, 2),
							SettleDate = r.SETTLEDATE,
							ReturnDate = r.RETURNDATE,
							SecurityID = string.Empty,//r.SECURITYID,
							Status = DepClaim2.Statuses.New,
							FirmName = r.FIRMNAME,
							FirmID = r.FIRMID,
							AuctionDate = this.Auction.SelectDate,
							AuctionID = this.Auction.ID,
							BankID = bank.ID,
							RecNum = 0,//r.RECNUM,
							DocumentDate = r.UpdateDate,
							DocumentTime = r.UpdateTime.TimeOfDay
						};

						if (item.Term == 0 || item.Part2Sum == 0)
						{
							item.Term = (item.ReturnDate - item.SettleDate).Days;
                            item.OldPayment = item.Payment = Math.Round(BankHelper.CalcPayment(item.ReturnDate, item.SettleDate, item.Amount, item.Rate),2);
							item.OldPart2Sum = item.Part2Sum = item.Amount + item.Payment;
						}

						this.Items.Add(item);
					}
				}
				if (missed.Count > 0)//|| wrongNameList.Count > 0)
				{
					this.Items.Clear();

					var sb = new StringBuilder();
					sb.Append("Импорт заявок невозможен, т.к. ");

					if (missed.Any())
						sb.AppendFormat("для указанных банков : {0} не задан биржевой код", string.Join(", ", missed.Select(m => string.Format("\"{0}\"", m.FIRMNAME))));

					//Отключена проверка различных имён банков

					//if (missed.Any() && wrongNameList.Count > 0)
					//    sb.Append(", также ");


					//if (wrongNameList.Count > 0)
					//    sb.AppendFormat("для банков(биржевых кодов) : {0} название банка не совпадает c формализованным наименованием в справочнике банков", string.Join(", ", wrongNameList.Select(m => string.Format("\"{0}({1})\"", m.FIRMNAME, m.FIRMID))));


					string msg = sb.ToString();
					DialogHelper.ShowAlert(msg);
				}
				else
				{
					//var idstr = new StringBuilder();
					//Items.GroupBy(x => x.RecNum)
					//    .Where(group => group.Count() > 1)
					//    .Select(group => group.Key).Distinct().ForEach2(a =>
					//    {
					//        idstr.Append(a);
					//        idstr.Append(",");
					//    });
					//var idString = idstr.ToString();
					//if (!string.IsNullOrEmpty(idString))
					//{
					//    idString = idString.Remove(idString.Length - 1, 1);
					//    DialogHelper.ShowError("Импортируемые заявки содержат повторяющиеся идентификаторы: " + idString);
					//    Items.Clear();
					//}
				}
				var dt = DateTime.Now;
				repositoryLogRecord = new RepositoryImpExpFile()
				{
					AuctionDate = Auction.SelectDate,
					AuctionLocalNum = Auction.LocalNum,
					DDate = dt,
					ImpExp = 1,
					Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(FileName)),
					Status = 0,
					TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
					UserName = AP.Provider.UserName,
					Key = (int)RepositoryImpExpFile.Keys.DepClaimImportSPVB,
					Operacia = OP_NAME,
					Comment = "Работа с ЦБ и депозитами - Депозиты - Импорт - Импорт сводного реестра заявок от СПВБ",
				};
			}
			catch (InvalidOperationException ex)
			{
				if (ex.InnerException is XmlSchemaValidationException)
				{
					if (ex.InnerException.InnerException == null)
					{
						DialogHelper.ShowAlert(
							string.Format(
								"Неверный формат документа: {0}\n\nПодробности - {1}",
								ex.Message,
								ex.InnerException.Message));
					}
					else
					{
						DialogHelper.ShowAlert(
							string.Format(
								"Неверный формат документа: {0}\n\nОписание - {1}\n\nПодробности - {2}",
								ex.Message,
								ex.InnerException.InnerException.Message,
								ex.InnerException.Message));
					}

				}

				else
				{
					DialogHelper.ShowAlert("Неверный формат документа");
					Logger.WriteException(ex);
				}

			}
			catch (Exception ex)
			{
				DialogHelper.ShowAlert("Ошибка открытия документа");
				Logger.WriteException(ex);
			}
		}

		protected override void ExecuteSave()
		{
			throw new NotImplementedException();
		}

		public override bool CanExecuteSave()
		{
			return false;
		}

		public override string this[string columnName] => string.Empty;
	}
}
