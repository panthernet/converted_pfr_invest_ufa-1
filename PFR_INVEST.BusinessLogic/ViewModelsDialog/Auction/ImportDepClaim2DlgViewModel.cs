﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.BusinessLogic.XMLModels;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ImportDepClaim2DlgViewModel : ImportDepClaimBaseViewModel, IVmNoDefaultLogging
    {

        //public long AuctionID { get; private set; }
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }
        public ObservableList<DepClaim2> Items { get; private set; }
        public DepClaimSelectParams Auction { get; private set; }
        public IList<DepClaim2> ExistingsDepClaim2List { get; private set; }

		private RepositoryImpExpFile repositoryLogRecord;

		private const string OP_NAME = "Импорт выписки из реестра заявок";

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { if (_fileName != value) { _fileName = value; OnPropertyChanged("FileName"); }; }
        }


        public ImportDepClaim2DlgViewModel(long auctionID)
            : base(typeof(DepClaim2ListViewModel),
                    typeof(DepClaim2ConfirmListViewModel),
                    typeof(DepClaimSelectParamsListViewModel),
                    typeof(DepClaimSelectParamsViewModel))
        {
            ID = auctionID;
            //this.AuctionID = auctionID;
            OpenFileCommand = new DelegateCommand<object>(OnOpenFile);
            ImportCommand = new DelegateCommand(o => Items.Count > 0, ExecuteImport);
            Items = new ObservableList<DepClaim2>();
            Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionID);
            ExistingsDepClaim2List = Auction.GetDepClaim2List();
        }

        public override bool ValidateCanImport()
        {
			if (!base.ValidateCanImport())
			{
				return false;
			}

            if (ExistingsDepClaim2List.Count(c => c.Status == DepClaim2.Statuses.New) == ExistingsDepClaim2List.Count)
            {
                return ExistingsDepClaim2List.Count <= 0 || DialogHelper.ConfirmImportDepClaim2Rewrite();
            }
            DialogHelper.ShowAlert("Аукцион находится в статусе, для которого невозможен импорт заявок");
            return false;
        }

        private void ExecuteImport(object o)
        {
            IsAttemptFailed = false;
            if (!ValidateCanImport())
            {
                IsAttemptFailed = true;
                return;
            }
            try
            {
                var repID = DataContainerFacade.Save(repositoryLogRecord);
                foreach (var i in Items)
                {
                    i.RepositoryID = repID;
                }

                var result = BLServiceSystem.Client.AuctionImportDepClaim2(ID, Items, true);
                if (result.IsSuccess)
                {

                    DialogHelper.ShowAlert("Импорт завершён");
                    if (Items != null && Items.Count > 0)
                    {
                        JournalLogger.LogEvent("Импорт данных", JournalEventType.IMPORT_DATA, null, OP_NAME,
                            string.Join(",", Items.Select(item => item.FirmName).ToArray())
                        );
                        //DataContainerFacade.Save(repositoryLogRecord);
                    }
                }
                else
                {
                    // сохраняются только успешно импортированные отчеты
                    DataContainerFacade.Delete<RepositoryImpExpFile>(repID);
                    DialogHelper.ShowError(result.ErrorMessage);
                }

                RefreshConnectedViewModels();
            }
            catch
            {
                IsAttemptFailed = true;
                throw;
            }
        }

        private void OnOpenFile(object o)
        {
            FileName = DialogHelper.OpenFile("Файлы заявок PDX02 (*.xml)|MD03562_PDX02_*_*.xml");
            Items.Clear();
            if (FileName == null)
                return;
            if (!DocumentBase.IsValidFileName(FileName))
            {
                DialogHelper.ShowAlert("Неверный формат названия импортируемого файла.");
                return;
            }
            XmlSerializer serializer = new XmlSerializer(typeof(DocumentPDX02));
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentPDX02.GetShema("PDX02.xsd"));

            try
            {
                var missed = new List<PDX02Record>();
                var wrongNameList = new List<PDX02Record>();

                using (var reader = XmlReader.Create(FileName, settings))
                {

                    var doc = (DocumentPDX02)serializer.Deserialize(reader);
                    foreach (var r in doc.Body.Records)
                    {
                        var hasDate = (doc.Info.DateValue != DateTime.MinValue);
                        //var bL =   DataContainerFacade.GetListByProperty<LegalEntity>("StockCode", r.FIRMID);
                        var bL = BLServiceSystem.Client.GetBankListByStockCode(r.FIRMID);

                        if (!bL.Any())
                        {
                            missed.Add(r);
                            continue;
                        }

                        var bank = bL.First();

                        //Validate Bank Name
                        if (!String.Equals(bank.FormalizedName, r.FIRMNAME, StringComparison.CurrentCultureIgnoreCase))
                        {
                            wrongNameList.Add(r);
                            continue;
                        }

                        var item = new DepClaim2
                        {
                            Amount = r.AMOUNT,
							OldAmount = r.AMOUNT,
                            Rate = r.RATE,
                            Payment = r.PAYMENT,
							OldPayment = r.PAYMENT,
							OldRate = r.RATE,
                            SettleDate = r.SETTLEDATE,
                            ReturnDate = r.RETURNDATE,
                            SecurityID = r.SECURITYID,
                            Status = DepClaim2.Statuses.New,
                            FirmName = r.FIRMNAME,
                            FirmID = r.FIRMID,
                            AuctionDate = Auction.SelectDate,
                            AuctionID = Auction.ID,
                            BankID = bank.ID,
							RecNum = r.RECNUM,
                            DocumentDate = hasDate ? (DateTime?)doc.Info.DateValue : null,
                            DocumentTime = hasDate ? (TimeSpan?)doc.Info.TimeValue : null
                        };

                        if (item.Term == 0 || item.Part2Sum == 0)
                        {
                            item.Part2Sum = item.Amount + item.Payment;
							item.OldPart2Sum = item.Part2Sum;
                            item.Term = (item.ReturnDate - item.SettleDate).Days;
                        }

                        Items.Add(item);
                    }
                }
                if (missed.Count > 0 || wrongNameList.Count > 0)
                {
                    Items.Clear();

                    var sb = new StringBuilder();
                    sb.Append("Импорт заявок невозможен, т.к. ");

                    if (missed.Any())
                      sb.AppendFormat("для указанных банков : {0} не задан биржевой код", string.Join(", ", missed.Select(m => string.Format("\"{0}\"", m.FIRMNAME))));

                    if (missed.Any() && wrongNameList.Count > 0)
                        sb.Append(", также ");

                    if (wrongNameList.Count > 0)
                        sb.AppendFormat("для банков(биржевых кодов) : {0} название банка не совпадает c формализованным наименованием в справочнике банков", string.Join(", ", wrongNameList.Select(m => string.Format("\"{0}({1})\"", m.FIRMNAME, m.FIRMID))));


                    string msg = sb.ToString();
                    DialogHelper.ShowAlert(msg);
                }
                else
                {
                    var idstr = new StringBuilder();
                    Items.GroupBy(x => x.RecNum)
                        .Where(group => group.Count() > 1)
                        .Select(group => group.Key).Distinct().ForEach(a =>
                        {
                            idstr.Append(a);
                            idstr.Append(",");
                        });
                    var idString = idstr.ToString();
                    if (!string.IsNullOrEmpty(idString))
                    {
                        idString = idString.Remove(idString.Length - 1, 1);
                        DialogHelper.ShowError("Импортируемые заявки содержат повторяющиеся идентификаторы: " + idString);
                        Items.Clear();
                    }
                }
				var dt = DateTime.Now;
				repositoryLogRecord = new RepositoryImpExpFile
				{
					AuctionDate = Auction.SelectDate,
					AuctionLocalNum = Auction.LocalNum,
					DDate = dt,
					ImpExp = 1,
					Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(FileName)),
					Status = 0,
					TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
					UserName = AP.Provider.UserName,
					Key = (int)RepositoryImpExpFile.Keys.DepClaimImportSPVB,
					Operacia = OP_NAME,
					Comment = "Работа с ЦБ и депозитами - Депозиты - Импорт - Выписка из реестра заявок"
				};
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        DialogHelper.ShowAlert(
                            string.Format(
                                "Неверный формат документа: {0}\n\nПодробности - {1}",
                                ex.Message,
                                ex.InnerException.Message));
                    }
                    else
                    {
                        DialogHelper.ShowAlert(
                            string.Format(
                                "Неверный формат документа: {0}\n\nОписание - {1}\n\nПодробности - {2}",
                                ex.Message,
                                ex.InnerException.InnerException.Message,
                                ex.InnerException.Message));
                    }

                }

                else
                {
                    DialogHelper.ShowAlert("Неверный формат документа");
                    Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка открытия документа");
                Logger.WriteException(ex);
            }
        }

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;
    }
}
