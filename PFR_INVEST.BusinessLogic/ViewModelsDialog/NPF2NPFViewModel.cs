﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class NPF2NPFViewModel : ViewModelBase
    {
        private List<NPFforERZLNotifyListItem> contragentsList;

        public List<NPFforERZLNotifyListItem> ContragentsList
        {
            get { return this.contragentsList; }
            set { this.contragentsList = value; base.OnPropertyChanged("ContragentsList"); }
        }

        public NPF2NPFViewModel()
            : base()
        {
        }
    }
}
