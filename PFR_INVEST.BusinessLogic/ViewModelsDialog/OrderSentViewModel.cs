﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class OrderSentViewModel : ViewModelCard, IUpdateRaisingModel
    {
        private F401402UK m_Detail;
        public F401402UK Detail
        {
            get { return m_Detail; }
            set
            {
                if (m_Detail != value)
                {
                    m_Detail = value;
                    OnPropertyChanged("Detail");
                }
            }
        }

        public string RegNum
        {
            get
            {
                return Detail.ControlRegnum;
            }
            set
            {
                Detail.ControlRegnum = value;
                OnPropertyChanged("RegNum");
            }
        }

        public DateTime? SendDate
        {
            get
            {
                return Detail.ControlSendDate;
            }
            set
            {
                Detail.ControlSendDate = value;
                OnPropertyChanged("SendDate");
            }
        }

        public void RefreshF402View()
        {
            if (GetViewModelsList != null)
            {
                List<ViewModelBase> lst = GetViewModelsList(typeof(F401402UKViewModel));
                if (lst != null && lst.Count > 0)
                    lst.Cast<F401402UKViewModel>()
                        .Where(card => card.Detail.ID == Detail.ID)
                        .ToList()
                        .ForEach(card => card.RefreshControlGroup());
            }
        }

        public OrderSentViewModel(long id)
            : base(typeof(F401402UKListViewModel))
        {
            DataObjectTypeForJournal = typeof(F401402UK);
            ID = id;
            Detail = DataContainerFacade.GetByID<F401402UK, long>(id);
            Detail.ControlSendDate = DateTime.Today;
            RefreshConnectedCardsViewModels = RefreshF402View;
        }

        protected override void ExecuteSave()
        {
            Detail.StatusID = (int)F402Status.Sent;
            ID = DataContainerFacade.Save<F401402UK, long>(Detail);
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate());
        }

        string Validate()
        {
            const string errorMessage = "Неверный формат данных";
            const string validate = "RegNum|SendDate";

            if (Detail == null)
                return errorMessage;

            foreach (string key in validate.Split('|'))
            {
                if (!String.IsNullOrEmpty(this[key]))
                {
                    return errorMessage;
                }
            }

            return null;
        }

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Необходимо ввести данные";

                switch (columnName.ToUpper())
                {
                    case "REGNUM": if (string.IsNullOrWhiteSpace(Detail.ControlRegnum)) return errorMessage;
                        return null;
                    case "SENDDATE": if (!SendDate.HasValue) return errorMessage;
                        return null;
                    default: return null;
                }
            }
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>>() { 
				new KeyValuePair<Type, long>(typeof(F401402UKListItem), this.Detail.ID)
			};
        }
    }
}
