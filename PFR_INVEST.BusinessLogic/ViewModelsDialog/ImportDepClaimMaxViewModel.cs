﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ImportDepClaimMaxViewModel : ImportDepClaimBaseViewModel
    {
        #region  Properties

        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }
        public ObservableList<DepClaimMaxListItem> Items { get; private set; }
        public DepClaimSelectParams Auction { get; private set; }
        public IList<DepClaimMaxListItem> ExistingsDepClaimMaxList { get; private set; }
        private const string FORMAT = "dd.MM.yyyy";
        private readonly DepClaimMaxListItem.Types _existingDepclaimType;

		private RepositoryImpExpFile _repositoryLogRecord;

		private const string OP_MAX_NAME = "Импорт сводного реестра заявок, с учетом максимальной суммы размещения";
		private const string OP_NAME = "Импорт сводного реестра заявок";
		
		private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { if (_fileName != value) { _fileName = value; OnPropertyChanged("FileName"); }; }
        }

        #endregion

        #region конструктор
        public ImportDepClaimMaxViewModel(long auctionID, DepClaimMaxListItem.Types depclaimType)
            : base(typeof(DepClaimSelectParamsListViewModel), 
                   typeof(DepClaimMaxListViewModel),
                   typeof(DepClaimSelectParamsViewModel),
                   typeof(DepClaim2ListViewModel))
        {
            ID = auctionID;
			//this.AuctionID = auctionID;
            _existingDepclaimType = depclaimType;
            OpenFileCommand = new DelegateCommand<object>(OnOpenFile);
            ImportCommand = new DelegateCommand(o => Items.Count > 0, ExecuteImport);
            Items = new ObservableList<DepClaimMaxListItem>();
            Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionID);
            ExistingsDepClaimMaxList = Auction.DepClaimMaxList((int)_existingDepclaimType);

        }
        #endregion

        #region OpenFile

        private void OnOpenFile(object obj)
        {
            switch (_existingDepclaimType)
            {
                case DepClaimMaxListItem.Types.Max: OpenFilePDX08(obj); break;
                case DepClaimMaxListItem.Types.Common: OpenFilePDX09(obj); break;
            }            
        }


        private void OpenFilePDX08(object obj)
        {
            FileName = DialogHelper.OpenFile("Файлы заявок PDX08 (*.xml)|MD03562_PDX08_*_*.xml");
            Items.Clear();
            if (string.IsNullOrEmpty(FileName)) return;

            if (!DocumentBase.IsValidFileName(FileName))
            {
                DialogHelper.ShowAlert("Неверный формат названия импортируемого файла.");
                return;
            }
            var serializer = new XmlSerializer(typeof(DocumentPDX08));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX08.xsd"));

            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (var reader = XmlReader.Create(FileName, settings))
                {
                    var doc = (DocumentPDX08)serializer.Deserialize(reader);

                    if (!doc.Body.Tab.TradeDate.Date.Equals(Auction.SelectDate))
                    {

                        DialogHelper.ShowAlert(
                            $@"Импортируемый файл содержит данные за {doc.Body.Tab.TradeDate:dd.MM.yyyy}. Выберите файл с данными аукциона за {Auction.SelectDate:dd.MM.yyyy}");
                        return;
                    }

                    foreach (var r in doc.Body.Tab.Board.Records)
                    {
                        var item = new DepClaimMaxListItem
                        {
                                           Auctno = doc.Body.Tab.Auctno,
                                           BoardId = doc.Body.Tab.Board.BoardId,
                                           DepclaimType = (int)_existingDepclaimType,
                                           DepclaimselectparamsId = Auction.ID,
                                           DocDate = doc.Info.DateValue,
                                           DocTime = new TimeSpan(),
                                           TradeDate = doc.Body.Tab.TradeDate,
                                           SecurityId = r.SECURITYID,
                                           QuantPart = r.QUANT_PART,
                                           QuantReq = r.QUANT_REQ,
                                           Rate = r.RATE,
                                           SettleDate = r.SETTLEDATE,
                                           SettleDate2 = r.SETTLEDATE2,
                                           Totval1 = r.TOTVAL1,
                                           Totval1N = r.TOTVAL1N,
                                           Totval2 = r.TOTVAL2,
                                           Warate = r.WARATE,
                                           SelectDate = Auction.SelectDate
                                       };
                        Items.Add(item);
                    }
                }
				var dt = DateTime.Now;
				_repositoryLogRecord = new RepositoryImpExpFile
				{
					AuctionDate = Auction.SelectDate,
					AuctionLocalNum = Auction.LocalNum,
					DDate = dt,
					ImpExp = 1,
					Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(FileName)),
					Status = 0,
					TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
					UserName = AP.Provider.UserName,
					Key = (int)RepositoryImpExpFile.Keys.DepClaimImportMax,
					Operacia = OP_MAX_NAME,
					Comment = "Работа с ЦБ и депозитами - Депозиты - Импорт - Сводный реестр заявок, с учетом максимальной суммы размещения"
				};
				//if (!IsCorrectSettleDate())
                //{
                //    DialogHelper.Alert("В импортируемых заявках дата размещения средств в депозит больше даты возврата средств.");
                //    this.Items.Clear();
                //}
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                    DialogHelper.ShowAlert(
                        ex.InnerException.InnerException == null
                            ? $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}"
                            : $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                else
                {
                    DialogHelper.ShowAlert("Неверный формат документа");
                    Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка открытия документа");
                Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

        }

        private void OpenFilePDX09(object obj)
        {
            FileName = DialogHelper.OpenFile("Файлы заявок PDX09 (*.xml)|MD03562_PDX09_*_*.xml");
            Items.Clear();
            if (string.IsNullOrEmpty(FileName)) return;
            if (!DocumentBase.IsValidFileName(FileName))
            {
                DialogHelper.ShowAlert("Неверный формат названия импортируемого файла.");
                return;
            }

            var serializer = new XmlSerializer(typeof(DocumentPDX09));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX09.xsd"));

            try
            {
                using (var reader = XmlReader.Create(FileName, settings))
                {
                    var doc = (DocumentPDX09)serializer.Deserialize(reader);

                    if (!doc.Body.Tab.TradeDate.Date.Equals(Auction.SelectDate))
                    {

                        DialogHelper.ShowAlert(
                            $@"Импортируемый файл содержит данные за {doc.Body.Tab.TradeDate:dd.MM.yyyy}. Выберите файл с данными аукциона за {Auction.SelectDate:dd.MM.yyyy}");
                        return;
                    }

                    foreach (var r in doc.Body.Tab.Board.Records)
                    {
                        var item = new DepClaimMaxListItem
                        {
                            Auctno = doc.Body.Tab.Auctno,
                            BoardId = doc.Body.Tab.Board.BoardId,
                            DepclaimType = (int)_existingDepclaimType,
                            DepclaimselectparamsId = Auction.ID,
                            DocDate = doc.Info.DateValue,
                            DocTime = new TimeSpan(),
                            TradeDate = doc.Body.Tab.TradeDate,
                            SecurityId = r.SECURITYID,
                            QuantPart = r.QUANT_PART,
                            QuantReq = r.QUANT_REQ,
                            Rate = r.RATE,
                            SettleDate = r.SETTLEDATE,
                            SettleDate2 = r.SETTLEDATE2,
                            Totval1 = r.TOTVAL1,
                            Totval1N = r.TOTVAL1N,
                            Totval2 = r.TOTVAL2,
                            Warate = r.WARATE,
                            SelectDate = Auction.SelectDate

                        };
                        Items.Add(item);
                    }
                }
                if (!IsCorrectSettleDate())
                {
                    DialogHelper.ShowAlert("В импортируемых заявках дата размещения средств в депозит больше даты возврата средств.");
                    Items.Clear();
                }
				var dt = DateTime.Now;
				_repositoryLogRecord = new RepositoryImpExpFile
				{
					AuctionDate = Auction.SelectDate,
					AuctionLocalNum = Auction.LocalNum,
					DDate = dt,
					ImpExp = 1,
					Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(FileName)),
					Status = 0,
					TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
					UserName = AP.Provider.UserName,
					Key = (int)RepositoryImpExpFile.Keys.DepClaimImport,
					Operacia = OP_NAME,
					Comment = "Работа с ЦБ и депозитами - Депозиты - Импорт - Сводный реестр заявок"
				};
			}
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                    else
                    {
                        DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }

                }

                else
                {
                    DialogHelper.ShowAlert("Неверный формат документа");
                    Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка открытия документа");
                Logger.WriteException(ex);
            }

        }

        #endregion

        private void ExecuteImport(object o)
        {
            if (!ValidateCanImport()) return;

            foreach (var item in ExistingsDepClaimMaxList)
            {
                DataContainerFacade.Delete(item);
            }

			var repositoryID = DataContainerFacade.Save(_repositoryLogRecord);
			
			foreach (var item in Items)
            {
				item.RepositoryID = repositoryID;
                DataContainerFacade.Save(item);
            }

            var msg =
                $@"Импорт {(_existingDepclaimType == DepClaimMaxListItem.Types.Max ? "заявок с учетом максимальной суммы размещения" : "сводного реестра заявок")} для аукциона от {Auction.SelectDate.Date.ToString(FORMAT)} выполнен.";
            DialogHelper.ShowAlert(msg);
			JournalLogger.LogModelEvent(this, JournalEventType.IMPORT_DATA, null, msg);
            RefreshConnectedViewModels();
        }

        private bool IsCorrectSettleDate()
        {
            var isCorrect = !Items.All(c => c.SettleDate > c.SettleDate2);
            return isCorrect;
        }

        public override bool ValidateCanImport()
        {
			if (!base.ValidateCanImport())
			{
				return false;
			}

            if (!ExistingsDepClaimMaxList.Any()) return true;
            var message = _existingDepclaimType == DepClaimMaxListItem.Types.Max
                ? $@"Заявки с учетом максимальной суммы размещения для аукциона от {Auction.SelectDate.Date.ToString(FORMAT)} уже есть в базе и будут заменены импортированными"
                : $@"Сводный реестр заявок, полученных от биржи для аукциона от {Auction.SelectDate.Date.ToString(FORMAT)} уже есть в базе и будет заменен импортированным";
            return DialogHelper.ConfirmImportDepClaimMaxRewrite($@"{message}. Продолжить импорт?");
        }


        #region override
        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;

        #endregion
    }
}
