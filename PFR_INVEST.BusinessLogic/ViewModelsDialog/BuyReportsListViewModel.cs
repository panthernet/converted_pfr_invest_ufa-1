﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BuyReportsListViewModel : PagedLoadListModelBase<BuyOrSaleReportsListItem>
    {

        public BuyReportsListViewModel()
        {
            DataObjectTypeForJournal = typeof(OrdReport);
        }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountBuyOrSaleReportsListHib(false, null);
        }

        protected override List<BuyOrSaleReportsListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetBuyOrSaleReportsListByPageHib(false, null,(int)index);
        }

        #region old
        //private List<BuyOrSaleReportsListItem> m_ReportsList;
        //public List<BuyOrSaleReportsListItem> ReportsList
        //{
        //    get
        //    {
        //        RaiseDataRefreshing();
        //        return m_ReportsList;
        //    }
        //    set
        //    {
        //        m_ReportsList = value;
        //        OnPropertyChanged("ReportsList");
        //    }
        //}

        //protected override void ExecuteRefreshList(object param)
        //{
        //    ReportsList = BLServiceSystem.Client.GetBuyOrSaleReportsListHib(false, null);
        //}

        //protected override bool CanExecuteRefreshList()
        //{
        //    return true;
        //}
        #endregion

       
    }
}
