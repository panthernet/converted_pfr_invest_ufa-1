﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using System.Globalization;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelImportExport.SI;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OUFV_directory_editor)]
	public class ImportSIDlgViewModel : ViewModelBase, IRequestCloseViewModel, IRequestLoadingIndicator
	{
		public DelegateCommand<object> OpenFileCommand { get; private set; }
		public DelegateCommand ImportCommand { get; private set; }

		public List<SIImportListItem> Items => ImportHandler?.Items;

	    private readonly SIRegister.Directions _direction;
        public bool IsFromUk => _direction == SIRegister.Directions.FromUK || _direction == SIRegister.Directions.FromGUK;
        public bool IsToUk => _direction == SIRegister.Directions.ToUK || _direction == SIRegister.Directions.ToGUK;

        public ImportSIDlgViewModel(SIRegister.Directions direction)
        {
            _direction = direction;
			OpenFileCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenFile);
			ImportCommand = new DelegateCommand(o => CanExecuteImport(), ExecuteImport);
			ImportHandler = new SIImportHandler(direction);
		}

		private string _fileName;
		public string FileName
		{
			get { return _fileName; }
			set
			{
				_fileName = value;
				OnPropertyChanged("FileName");
			}
		}

		protected SIImportHandler ImportHandler { get; set; }


		private bool CanExecuteOpenFile()
		{
			return ImportHandler.CanExecuteOpenFile();
		}

		private void OpenFile(object o)
		{
			string[] fileNames = DialogHelper.OpenFiles(ImportHandler.OpenFileMask);

			if (fileNames == null)
				return;

			LoadFiles(fileNames);
			OnPropertyChanged("FileName");
		}

		private void LoadFiles(string[] fileNames)
		{
			if (fileNames == null || fileNames.Length == 0)
				return;

			var oldCi = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            OnLoadingIndicator("Обработка файлов...");

            try
            {
				foreach (var fileName in fileNames)
				{
					try
					{
						FileName = fileName;
						string message;
					    var res = !ImportHandler.LoadFile(fileName, out message) && !string.IsNullOrEmpty(message);
                        OnLoadingIndicator(null);
                        if(res)
                        {
                            DialogHelper.ShowAlert(message);
							continue;
						}
                        if(!string.IsNullOrEmpty(message))
                            DialogHelper.ShowAlert(message);
                        OnLoadingIndicator("Обработка файлов...");
                        OnPropertyChanged("Items");
					}

					catch (Exception ex)
					{
						DialogHelper.ShowAlert("Файл содержит некорректные данные. Импорт не может быть произведен.\nДетальная информация об ошибке может быть найдена в лог файле приложения.");
						Logger.WriteException(ex);
					}
				}
			}
			finally
			{
                Thread.CurrentThread.CurrentCulture = oldCi;
                OnLoadingIndicator(null);
            }
        }

		public bool CanExecuteImport()
		{
			return !string.IsNullOrEmpty(FileName) && ImportHandler.CanExecuteImport();
		}

		private void ExecuteImport(object o)
		{
			if (!CanExecuteImport())
				return;
            OnLoadingIndicator("Импорт записей...");
		    try
		    {
		        string message;

		        if (ImportHandler.ExecuteImport(out message))
		        {
		            JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.IMPORT_DATA, null, "Импорт данных УК для мастера");
		            //DialogHelper.ShowAlert($"Импорт файла {FileName} выполнен");
                    if(message != null)
		                DialogHelper.ShowAlert(message);
		        }
		        else
		        {
		            DialogHelper.ShowAlert($"Импорт файла {FileName}\n не выполнен");
		        }
		        FileName = "";
		        OnPropertyChanged("Items");
		    }
		    finally
		    {
		        OnLoadingIndicator(null);
                CloseWindow();
		    }
		}


		public event EventHandler RequestClose;

		public void CloseWindow()
		{
		    RequestClose?.Invoke(true, null);
		}

	    public event EventHandler RequestLoadingIndicator;

	    protected void OnLoadingIndicator(string  message)
	    {
	        RequestLoadingIndicator?.Invoke( message, null);
	    }
	}
}
