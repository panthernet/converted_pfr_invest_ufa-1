﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic
{
    using System.Linq;
    using Commands;

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class RatingDlgModel : ViewModelBase
    {
        public Rating rating { get; set; }

        private IList<string> _ratingNames;

        public string Name
        {
            get
            {
                return rating.RatingName;
            }
            set
            {
                if (rating.RatingName != value)
                {
                    rating.RatingName = value;
                    OnPropertyChanged("Name");
                }
            }
        }


        public DelegateCommand<object> SaveRatingCommand { get; private set; }

        public void RegisterCommands()
        {
            this.SaveRatingCommand = new DelegateCommand<object>(r => CanExecuteSave(),
                r => ExecuteSave());
        }

        public bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate());
        }

        public void ExecuteSave()
        {
            rating.RatingName = rating.RatingName.Trim();
            this._ratingNames = DataContainerFacade.GetList<Rating>().Where(x => x.ID != rating.ID).Select(x => x.RatingName.Trim()).ToList();
            if (_ratingNames.Any(x => x.Equals(rating.RatingName, StringComparison.InvariantCultureIgnoreCase)))
            {
                DialogHelper.ShowAlert("Рейтинг с таким именем уже существует");
                return;
            }

            var deleted = DataContainerFacade.GetListByProperty<Rating>("RatingName", rating.RatingName).FirstOrDefault();

            if (deleted != null)
            {
                rating = deleted;
                deleted.StatusID = 1;
            }

            rating.ID = DataContainerFacade.Save(rating);
        }

        public RatingDlgModel(Rating r)
            :base()
        {
            if (r != null)
            {
                rating = r;
            }
            else
            {
                rating = new Rating();
            }

            this._ratingNames = DataContainerFacade.GetList<Rating>().Where(x => x.ID != rating.ID).Select(x => x.RatingName).ToList();
            RegisterCommands();
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Name":
                        if (_ratingNames.Any(x => x.Equals(rating.RatingName))) return "Рейтинг с таким именем уже существует";
                        return rating["RatingName"];
                    default: return null;
                }
            }
        }
        
        private string Validate()
        {
            return this["Name"];
        }
    }
}
