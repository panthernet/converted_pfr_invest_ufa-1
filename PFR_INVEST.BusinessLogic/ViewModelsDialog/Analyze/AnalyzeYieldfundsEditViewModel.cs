﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using System.Linq;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class AnalyzeYieldfundsEditViewModel : AnalyzeEditorDialogBase<AnalyzeYieldfundsReport>
    {
        #region Properties/Fields
        private AnalyzeYieldfundsReport _report;
        public virtual string FormTitle => _report?.Act == 107 ? "Форма 6.1 Доходность инвестирования средств пенсионных накоплений, рассчитанная в соответствии с приказом Минфина России от 22.08.2005 № 107н «Об утверждении стандартов раскрытия информации об инвестировании средств пенсионных накоплений»"
                                    : _report?.Act == 140 ? "Форма 6.2 Доходность инвестирования средств пенсионных накоплений, рассчитанная в соответствии с приказом Минфина России от 18.11.2005 № 140н «Об утверждении порядка расчета результатов инвестирования средств пенсионных накоплений для их отражения в специальной части индивидуальных лицевых счетов»" : string.Empty;

        private RangeObservableCollection<AnalyzeYieldfundsData> _dataItemsCache;
        public RangeObservableCollection<AnalyzeYieldfundsData> DataItemsCache => _dataItemsCache ?? (_dataItemsCache = new RangeObservableCollection<AnalyzeYieldfundsData>());
        private List<SubjectSPN> _subjectsSPN;
        public int ActID => _report.Act;
        private RangeObservableCollection<AnalyzeYieldfundsData> _EditData;
        public RangeObservableCollection<AnalyzeYieldfundsData> EditData => _EditData ?? (_EditData = new RangeObservableCollection<AnalyzeYieldfundsData>());
        protected override bool isQuarkReport => _report != null && _report.Act == 107;

        #endregion

        public AnalyzeYieldfundsEditViewModel(AnalyzeYieldfundsReport report, RibbonStateBL state, ViewModelState vmState, bool isNewReport = false, bool isArchiveMode = false) : base(state, vmState, isNewReport)
        {
            IsArchiveMode = isArchiveMode;

            if (!isNewReport && report == null)
                throw new NullReferenceException("Переданный параметр report является null");
            if (!isNewReport && report.Id == 0)
                throw new ArgumentException("Не правильно вызван конструктор редактирования, ");

            _report = report ?? QuarkReportBase.InitNewReport(new AnalyzeYieldfundsReport());

            InitDataList(); //Загрузка возможных вариантов кварталов

            if (!IsNewReport)
            {
                SelectedYear = _report.Year;
                if (_report.Act == 107)
                    SelectedKvartal = Kvartals.FirstOrDefault(k => k.Item1 == _report.Kvartal);
            }

            var _avPortfByRibGr = new List<KeyValuePair<RibbonStateBL, string[]>>()
            {
                new KeyValuePair<RibbonStateBL, string[]>(RibbonStateBL.BackOfficeState, new [] {"Коэффициент индексации фиксированной выплаты к страховой пенсии"}),
                new KeyValuePair<RibbonStateBL, string[]>(RibbonStateBL.CBState, new [] {"Индекс потребительских цен"}),
                new KeyValuePair<RibbonStateBL, string[]>(RibbonStateBL.SIState, new [] {"ГУК расширенный инвестиционный портфель", "ГУК инвестиционный портфель государственных ценных бумаг", "ЧУК","ГУК"}),
                new KeyValuePair<RibbonStateBL, string[]>(RibbonStateBL.NPFState, new [] {"НПФ"}),
            };
            _subjectsSPN = new List<SubjectSPN>();

            _subjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => s.RibbonGroups.Split(',').Contains(state.ToString())));
            var availPortfolios = _avPortfByRibGr.First(x => x.Key == state).Value;

            if (availPortfolios != null)
                _subjectsSPN.RemoveAll(d => !availPortfolios.Contains(d.Name, StringComparer.CurrentCultureIgnoreCase));

            if (isNewReport)
            {
                InitNewReportData();
                LoadReportData(new List<AnalyzeYieldfundsData>());
            }
            else
            {
                LoadReportData(DataContainerFacade.GetClient().GetYieldfundsDataByReportId(_report.Id).Where(d => _subjectsSPN.Any(s => d.SubjectId == s.Id)).ToList());
            }
            if (!isArchiveMode)
            {
                ArchiveReports = new AnalyzeYieldfundsListViewModel(ActID) { IsArchiveMode = true, Year = _report.Year, Quark = _report.Kvartal };
                ArchiveReports.RefreshList.Execute(null);
            }

        }
        void InitNewReportData()
        {
            var tmp = new List<AnalyzeYieldfundsData>();

            foreach (var s in _subjectsSPN)
            {
                var newDataItem = new AnalyzeYieldfundsData
                {
                    SubjectId = s.Id,
                    SubjectName = s.Name,
                    SubjOrderId = s.OrderId
                };
                tmp.Add(newDataItem);

            }
            FillDataSources(tmp);
        }

        void LoadReportData(List<AnalyzeYieldfundsData> savedItems)
        {
            var notSavedItems = new List<AnalyzeYieldfundsData>();

            foreach (var s in _subjectsSPN)
            {

                if (savedItems.Any(n => n.SubjectId == s.Id))
                    continue;

                notSavedItems.Add(
                    new AnalyzeYieldfundsData
                    {
                        SubjectId = s.Id,
                        SubjectName = s.Name,
                        ReportId = _report.Id,
                        SubjOrderId = s.OrderId,
                        SubjectTypeFond = (SubjectSPN.FondType)s.Fondtype
                    });

            }

            savedItems.AddRange(notSavedItems);
            FillDataSources(savedItems);
        }

        void FillDataSources(List<AnalyzeYieldfundsData> dataItems)
        {
            DataItemsCache.Fill(dataItems.ToList());
            SetReadOnlyState(DataItemsCache);
            EditData.Fill(dataItems.ConvertAll<AnalyzeYieldfundsData>(p => new AnalyzeYieldfundsData(p)).OrderBy(d => d.SubjOrderId));
        }

        protected override bool CheckExistSavedReport()
        {

            bool existSavedReport = false;
            if(isQuarkReport)
            existSavedReport = DataContainerFacade.GetList<AnalyzeYieldfundsReport>().Any(
                        r => r.Status == 0 &&
                            r.Act == ActID &&
                            r.Year == SelectedYear &&
                            (r as QuarkReportBase).Kvartal == SelectedKvartal.Item1);
            else
            

                existSavedReport = DataContainerFacade.GetList<AnalyzeYieldfundsReport>().Any(
                        r => r.Status == 0 &&
                            r.Act == ActID &&
                            r.Year == SelectedYear);
            
            return existSavedReport;
        }

        protected override void ExecuteSaveAction()
        {
            if (IsNewReport)
            {
                _report.Kvartal = isQuarkReport ? (int?)SelectedKvartal.Item1 : null;
                _report.Year = SelectedYear ?? _curYear;
                _report.Id = DataContainerFacade.Save<AnalyzeYieldfundsReport>(_report);
            }

            _dataForSave.AddRange(EditData);
            foreach (var d in _dataForSave)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzeYieldfundsData>(d);


            if (!IsNewReport && IsDataChanged)
            {
                _report.UpdateDate = DateTime.Now;
                DataContainerFacade.Save<AnalyzeYieldfundsReport>(_report);
                IsDataChanged = false;
            }

        }

        bool isChangedValue(AnalyzeYieldfundsData chIt)
        {
            if (!chIt.Total.HasValue)
                return false;

            if (chIt.Id == 0)
            {
                chIt.ReportId = _report.Id;
                return true;
            }

            var oldItem = DataItemsCache.FirstOrDefault(i => i.SubjectId == chIt.SubjectId); //&& i.ReportId == chIt.ReportId);

            if (oldItem == null)
            {
                chIt.ReportId = _report.Id;
                return IsDataChanged = true;
            }

            if (oldItem.Total != chIt.Total)
            {
                chIt.ReportId = _report.Id;
                //oldItem.ReportId = chIt.ReportId = _report.Id;
                //oldItem.Total = chIt.Total;
                return IsDataChanged = true;
            }

            return false;
        }

        protected override List<AnalyzeYieldfundsReport> LoadReports()
        {
            if (_report == null)
                return new List<AnalyzeYieldfundsReport>();

            return DataContainerFacade.GetClient().GetYieldfundsReportsByYearKvartal(actId: _report.Act);
        }


        protected override void RefreshConnectedCards()
        {
            if (GetViewModel == null) return;
            var rVM = GetViewModel(typeof(AnalyzeYieldfundsListViewModel)) as AnalyzeYieldfundsListViewModel;
            if (rVM == null) return;
            rVM.RefreshList.Execute(null);
            rVM.Current = rVM.List.FirstOrDefault(d => ActID == 140 ? d.Year == SelectedYear : (d.Year == SelectedYear && d.Kvartal == SelectedKvartal.Item1));

        }
        protected override AnalyzeYieldfundsReport GetCurrentReport()
        {
            return _report;
        }

        /* Загрузка данных по другим разделам для сохранения в новом отчете при сохранении истории */
        private List<AnalyzeYieldfundsData> _dataForSave = new List<AnalyzeYieldfundsData>();
        protected override void ObtainAllReportData(AnalyzeYieldfundsReport report)
        {
            var data = DataContainerFacade.GetClient().GetYieldfundsDataByReportId(report.Id);
            foreach (var d in data)
            {
                if (!EditData.Any(e => e.SubjectId == d.SubjectId))
                    _dataForSave.Add(d);
            }
        }

        /* End */

        protected override void ReportDataResetID()
        {
            EditData.ForEach(i => i.Id = 0);
            _dataForSave.ForEach(i => i.Id = 0);
        }

        protected override bool CheckHaveChanges()
        {
            return EditData.Any(d =>
            {
                return _dataItemsCache.Any(x => x.Id == d.Id && x.Total != d.Total);
            });
        }
        protected override bool HasErrors()
        {
            return false;
        }
    }
}
