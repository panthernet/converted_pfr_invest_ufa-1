﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    /// <summary>
    /// Правило проверки отчетов 6
    /// </summary>
    public class IncomingstatementsCondition : AnalyzeConditionViewModelBase
    {
        private List<FilingType> _filingTypes = new List<FilingType>();
        private List<StatementType> _statementTypes = new List<StatementType>();

        protected override void PrepareDataOptions()
        {
            _filingTypes = new List<FilingType>(DataContainerFacade.GetList<FilingType>());
            _statementTypes = new List<StatementType>(DataContainerFacade.GetList<StatementType>());


            _normalDataCount = _filingTypes.Count() * _statementTypes.Count();
        }

        public override IEnumerable<AnalyzeReportDataBase> LoadDataFromDB()
        {
            var result = new List<AnalyzeReportDataBase>();
            result.AddRange(DataContainerFacade.GetClient().GetIncomingstatementsDataByYearKvartal(DateTime.Now.Year, null, false));
            result.AddRange(DataContainerFacade.GetClient().GetIncomingstatementsDataByYearKvartal(DateTime.Now.Year - 1, 4, false));
            return result;
        }
        public override List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data)
        {
            return data.ToList();

        }
    }
}
