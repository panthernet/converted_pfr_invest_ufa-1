﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class AnalyzeConditionEditorViewModel : ViewModelListObservable<AnalyzeCondition>
    {
        public AnalyzeConditionEditorViewModel()
        {
            IsSaveOnCurrentChanged = false;
           // StatusChanged = new PFR_INVEST.BusinessLogic.Commands.DelegateCommand(x => Current != null, x =>
           //{
           //    if (Current != null)
           //    {
           //        Current.Status = short.Parse(x.ToString());
           //        OnPropertyChanged(nameof(Current.IsActiveStatus));
           //    }
           //});
        }

        //public ICommand StatusChanged { get; private set; }
        protected override void ExecuteRefreshList(object param)
        {
            List.Fill(DataContainerFacade.GetList<AnalyzeCondition>());
            SetFirstAsCurrent();
        }

    }
}
