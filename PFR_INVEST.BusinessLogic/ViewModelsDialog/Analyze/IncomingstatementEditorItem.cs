﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    public class IncomingstatementEditorItem : ViewModelBase
    {
        public int StatementId { get; set; }
        public string StatementName { get; set; }
        public long ReportId { get; set; }

        private int? _SrochCount;
        public int? SrochCount
        {
            get { return _SrochCount; }
            set { SetPropertyValue(nameof(SrochCount), ref _SrochCount, value); }
        }

        private int? _UvedCount;
        public int? UvedCount
        {
            get { return _UvedCount; }
            set { SetPropertyValue(nameof(UvedCount), ref _UvedCount, value); }
        }

        private int? _DosrochCount;
        public int? DosrochCount
        {
            get { return _DosrochCount; }
            set { SetPropertyValue(nameof(DosrochCount), ref _DosrochCount, value); }
        }

        public bool IsReadOnly
            =>
                (_srochDataItem?.IsReadOnly).Value && (_uvedDataItem?.IsReadOnly).Value &&
                (_dosrochDataItem?.IsReadOnly).Value;

        AnalyzeIncomingstatementsData _srochDataItem;
        AnalyzeIncomingstatementsData _uvedDataItem;
        AnalyzeIncomingstatementsData _dosrochDataItem;

        private string[] statements = new[] { "о переходе из ПФР в НПФ", "о переходе из НПФ в ПФР", "о переходе из НПФ в НПФ" };
        public virtual bool IsThreeValueInput
            =>
                statements.Contains(StatementName,
                    StringComparer.CurrentCultureIgnoreCase);

        public IncomingstatementEditorItem(int statId, string statName, AnalyzeIncomingstatementsData srItm = null, AnalyzeIncomingstatementsData uvedItm = null, AnalyzeIncomingstatementsData dosrItm = null, int? filSubtype = null)
        {
            if (srItm == null)
                _srochDataItem = new AnalyzeIncomingstatementsData()
                {
                    StatementId = statId,
                    Changetype = 0,
                    FilingSubtype = filSubtype
                };
            else
            {
                _srochDataItem = srItm;
                _srochDataItem.FilingSubtype = filSubtype;
            }


            if (uvedItm == null)
                _uvedDataItem = new AnalyzeIncomingstatementsData()
                {
                    StatementId = statId,
                    Changetype = 1,
                    FilingSubtype = filSubtype
                };
            else
            {
                _uvedDataItem = uvedItm;
                _uvedDataItem.FilingSubtype = filSubtype;
            }

            if (dosrItm == null)
                _dosrochDataItem = new AnalyzeIncomingstatementsData()
                {
                    StatementId = statId,
                    Changetype = 2,
                    FilingSubtype = filSubtype
                };
            else
            {
                _dosrochDataItem = dosrItm;
                _dosrochDataItem.FilingSubtype = filSubtype;
            }

            StatementName = statName;

            fillFields();
            StatementId = statId;
        }

        

        //public IncomingstatementEditorItem(int statId)
        //{
        //    StatementId = statId;
        //    _srochDataItem = new AnalyzeIncomingstatementsData() { StatementId = statId, Changetype = 0 };
        //    _uvedDataItem = new AnalyzeIncomingstatementsData() { StatementId = statId, Changetype = 1 };
        //    _dosrochDataItem = new AnalyzeIncomingstatementsData() { StatementId = statId, Changetype = 2 };
        //}

        public bool SaveItems()
        {
            if (ReportId == 0)
                throw new NullReferenceException("ReportId is not setted");
            var isSaved = false;
            if ( DosrochCount.HasValue && (_dosrochDataItem.Id == 0 || DosrochCount != _dosrochDataItem.Total))
            {
                applyChanges(_dosrochDataItem, DosrochCount);
                DataContainerFacade.Save<AnalyzeIncomingstatementsData>(_dosrochDataItem);
                isSaved = true;
            }
            if (UvedCount.HasValue && (_uvedDataItem.Id == 0 || UvedCount != _uvedDataItem.Total))
            {
                applyChanges(_uvedDataItem, UvedCount);
                DataContainerFacade.Save<AnalyzeIncomingstatementsData>(_uvedDataItem);
                isSaved = true;
            }
            if (SrochCount.HasValue && (_srochDataItem.Id == 0 || SrochCount != _srochDataItem.Total))
            {
                applyChanges(_srochDataItem, SrochCount);
                DataContainerFacade.Save<AnalyzeIncomingstatementsData>(_srochDataItem);
                isSaved = true;
            }
            return isSaved;
        }

        public bool GetHasChanges()
        {
            return (DosrochCount.HasValue && (_dosrochDataItem.Id == 0 || DosrochCount != _dosrochDataItem.Total))
                || (UvedCount.HasValue && (_uvedDataItem.Id == 0 || UvedCount != _uvedDataItem.Total))
                || (SrochCount.HasValue && (_srochDataItem.Id == 0 || SrochCount != _srochDataItem.Total));
        }

        void fillFields()
        {
            SrochCount = _srochDataItem.Total;
            UvedCount = _uvedDataItem.Total;
            DosrochCount = _dosrochDataItem.Total;
        }

        public void ReportDateResetID()
        {
            if (_srochDataItem != null)
                _srochDataItem.Id = 0;
            if (_dosrochDataItem != null)
                _dosrochDataItem.Id = 0;
            if (_uvedDataItem != null)
                _uvedDataItem.Id = 0;
        }

        void applyChanges(AnalyzeIncomingstatementsData item, int? total)
        {
            item.Total = total;
            item.ReportId = ReportId;
        }
    }
}
