﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using System.Linq;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    public class AnalyzePaymentyearrateEditViewModel : AnalyzeEditorDialogBase<AnalyzePaymentyearrateReport>
    {
        #region Properties/Fields
        private AnalyzePaymentyearrateReport _report;
        //public override string FormTitle => $"Форма 7. Показатели назначения и выплаты пенсионных накоплений{(IsArchiveMode ? " (архив)" : string.Empty)}";
        public override string FormTitle => $"Форма 7. Показатели назначения и выплаты пенсионных накоплений";

        private RangeObservableCollection<AnalyzePaymentyearrateData> _dataItemsCache;
        public RangeObservableCollection<AnalyzePaymentyearrateData> DataItemsCache => _dataItemsCache ?? (_dataItemsCache = new RangeObservableCollection<AnalyzePaymentyearrateData>());

        private RangeObservableCollection<AnalyzePaymentyearrateData> _spnSummItems;
        public RangeObservableCollection<AnalyzePaymentyearrateData> SpnSummItems => _spnSummItems ?? (_spnSummItems = new RangeObservableCollection<AnalyzePaymentyearrateData>());

        private RangeObservableCollection<AnalyzePaymentyearrateData> _spnCountItems;
        public RangeObservableCollection<AnalyzePaymentyearrateData> SpnCountItems => _spnCountItems ?? (_spnCountItems = new RangeObservableCollection<AnalyzePaymentyearrateData>());

        private RangeObservableCollection<AnalyzePaymentyearrateData> _spnAverageItems;
        public RangeObservableCollection<AnalyzePaymentyearrateData> SpnAverageItems => _spnAverageItems ?? (_spnAverageItems = new RangeObservableCollection<AnalyzePaymentyearrateData>());

        private RangeObservableCollection<AnalyzePaymentyearrateData> _preemnikItems;
        public RangeObservableCollection<AnalyzePaymentyearrateData> PreemnikItems => _preemnikItems ?? (_preemnikItems = new RangeObservableCollection<AnalyzePaymentyearrateData>());

        private List<PaymentKind> _paymentKinds;


        //Журнал изменений для редактируемого отчета
        //private AnalyzePaymentyearrateListViewModel _ArchiveReports;

        
       
        #endregion

        public bool IsShowOSRPTab => _state == RibbonStateBL.RegionsState;
        

        private RibbonStateBL _state;

        public AnalyzePaymentyearrateEditViewModel(AnalyzePaymentyearrateReport report, RibbonStateBL state, ViewModelState vmState, bool isNewReport = false, bool isArchiveMode = false) : base(state, vmState, isNewReport)
        {
            IsArchiveMode = isArchiveMode;
            _state = state;
            if (!isNewReport && report == null)
                throw new NullReferenceException("Переданный параметр report является null");
            if (!isNewReport && report.Id == 0)
                throw new ArgumentException("Не правильно вызван конструктор редактирования, ");

            _report = isNewReport ? QuarkReportBase.InitNewReport(new AnalyzePaymentyearrateReport()) : report;


            if (!IsNewReport)
            {
                SelectedYear = _report.Year;
                SelectedKvartal = Kvartals.FirstOrDefault(k => k.Item1 == _report.Kvartal);
            }

            _paymentKinds = new List<PaymentKind>(DataContainerFacade.GetList<PaymentKind>());

            if (isNewReport)
            {
                InitNewReportData();
            }
            else
            {
                LoadReportData(DataContainerFacade.GetClient().GetPaymentyearrateDataByReportId(_report.Id));
            }
            if (!isArchiveMode)
            {
                ArchiveReports = new AnalyzePaymentyearrateListViewModel() { IsArchiveMode = true, Year = _report.Year, Quark = _report.Kvartal };
                ArchiveReports.RefreshList.Execute(null);
            }

        }

        void InitNewReportData()
        {
            var tmp = new List<AnalyzePaymentyearrateData>();

            foreach (var s in _paymentKinds)
            {
                var newDataItem = new AnalyzePaymentyearrateData
                {
                    PaymentkindId = s.Id,
                    PaymentkindName = s.Name,
                    PaymentkindGroup = s.GroupId,
                    PaymentkindEditType = s.EditType
                };
                tmp.Add(newDataItem);
            }

            FillDataSources(tmp);
        }

        void LoadReportData(List<AnalyzePaymentyearrateData> savedItems)
        {
            var notSavedItems = new List<AnalyzePaymentyearrateData>();

            foreach (var s in _paymentKinds)
            {

                if (savedItems.Any(n => n.PaymentkindId == s.Id))
                    continue;

                notSavedItems.Add(
                    new AnalyzePaymentyearrateData
                    {
                        PaymentkindId = s.Id,
                        PaymentkindName = s.Name,
                        PaymentkindGroup = s.GroupId,
                        PaymentkindEditType = s.EditType,
                        ReportId = _report.Id
                    });

            }

            savedItems.AddRange(notSavedItems);
            FillDataSources(savedItems);
        }

        void FillDataSources(List<AnalyzePaymentyearrateData> dataItems)
        {
            DataItemsCache.Fill(dataItems.ToList());
            SetReadOnlyState(DataItemsCache);
            SpnSummItems.Fill(
                dataItems.Where(i => i.PaymentkindGroup == 0).ToList().ConvertAll(p => new AnalyzePaymentyearrateData(p)).OrderBy(d => d.PaymentkindName)
                );
            SpnCountItems.Fill(
                dataItems.Where(i => i.PaymentkindGroup == 1).ToList().ConvertAll(p => new AnalyzePaymentyearrateData(p)).OrderBy(d => d.PaymentkindName)
                );
            SpnAverageItems.Fill(
                dataItems.Where(i => i.PaymentkindGroup == 2).ToList().ConvertAll(p => new AnalyzePaymentyearrateData(p)).OrderBy(d => d.PaymentkindName)
                );
            PreemnikItems.Fill(
                dataItems.Where(i => i.PaymentkindGroup == 3).ToList().ConvertAll(p => new AnalyzePaymentyearrateData(p)).OrderBy(d => d.PaymentkindName)
                );
        }

        protected override void ReportDataResetID()
        {
            SpnCountItems.ForEach(i => i.Id = 0);
            SpnSummItems.ForEach(i => i.Id = 0);
            SpnAverageItems.ForEach(i => i.Id = 0);
            PreemnikItems.ForEach(i => i.Id = 0);
        }

        protected override void ExecuteSaveAction()
        {
            if (IsNewReport)
            {
                _report.Kvartal = SelectedKvartal.Item1;
                _report.Year = SelectedYear ?? _curYear;
                _report.Id = DataContainerFacade.Save<AnalyzePaymentyearrateReport>(_report);

            }

            _dataForSave.AddRange(SpnCountItems);
            _dataForSave.AddRange(SpnSummItems);
            _dataForSave.AddRange(SpnAverageItems);
            _dataForSave.AddRange(PreemnikItems);

            foreach (var d in _dataForSave)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePaymentyearrateData>(d);
            //foreach (var d in SpnSummItems)
            //    if (isChangedValue(d))
            //        DataContainerFacade.Save<AnalyzePaymentyearrateData>(d);
            //foreach (var d in SpnAverageItems)
            //    if (isChangedValue(d))
            //        DataContainerFacade.Save<AnalyzePaymentyearrateData>(d);
            //foreach (var d in PreemnikItems)
            //    if (isChangedValue(d))
            //        DataContainerFacade.Save<AnalyzePaymentyearrateData>(d);


            if (!IsNewReport && IsDataChanged)
            {
                _report.UpdateDate = DateTime.Now;
                DataContainerFacade.Save<AnalyzePaymentyearrateReport>(_report);
                IsDataChanged = false;
            }

        }

        bool isChangedValue(AnalyzePaymentyearrateData chIt)
        {
            if (!(chIt.Total.HasValue))
                return false;

            if (chIt.Id == 0)
            {
                chIt.ReportId = _report.Id;
                return true;
            }

            var oldItem = DataItemsCache.FirstOrDefault(i => i.PaymentkindId == chIt.PaymentkindId &&
                                                                i.PaymentkindGroup == chIt.PaymentkindGroup);

            if (oldItem == null)
                return IsDataChanged = true;

            if (oldItem.Total != chIt.Total)
            {
                chIt.ReportId = _report.Id;
                //oldItem.ReportId = chIt.ReportId = _report.Id;
                //oldItem.Total = chIt.Total;
                return IsDataChanged = true;
            }

            return false;
        }

        protected override List<AnalyzePaymentyearrateReport> LoadReports()
        {
            return DataContainerFacade.GetClient().GetPaymentyearrateReportByYearKvartal();
        }

        protected override void RefreshConnectedCards()
        {
            if (GetViewModel == null) return;
            var rVM = GetViewModel(typeof(AnalyzePaymentyearrateListViewModel)) as AnalyzePaymentyearrateListViewModel;
            if (rVM == null) return;
            rVM.RefreshList.Execute(null);
            rVM.Current = rVM.List.FirstOrDefault(d => d.Year == SelectedYear && d.Kvartal == SelectedKvartal.Item1);

        }
        protected override AnalyzePaymentyearrateReport GetCurrentReport()
        {
            return _report;
        }

        /* Загрузка данных по другим разделам для сохранения в новом отчете при сохранении истории */
        private List<AnalyzePaymentyearrateData> _dataForSave = new List<AnalyzePaymentyearrateData>();
        protected override void ObtainAllReportData(AnalyzePaymentyearrateReport report)
        {
            var data = DataContainerFacade.GetClient().GetPaymentyearrateDataByReportId(report.Id);

            foreach (var d in data.Where(x=>x.PaymentkindGroup == 0))
            {
                if (!SpnSummItems.Any(e => e.PaymentkindId == d.PaymentkindId))
                    _dataForSave.Add(d);
            }
            foreach (var d in data.Where(x => x.PaymentkindGroup == 1))
            {
                if (!SpnCountItems.Any(e => e.PaymentkindId == d.PaymentkindId))
                    _dataForSave.Add(d);
            }
            foreach (var d in data.Where(x => x.PaymentkindGroup == 2))
            {
                if (!SpnAverageItems.Any(e => e.PaymentkindId == d.PaymentkindId))
                    _dataForSave.Add(d);
            }
            foreach (var d in data.Where(x => x.PaymentkindGroup == 3))
            {
                if (!PreemnikItems.Any(e => e.PaymentkindId == d.PaymentkindId))
                    _dataForSave.Add(d);
            }
        }

        /* End */

        protected override bool CheckHaveChanges()
        {
            return SpnSummItems.Any(d => isChangedValue(d)) ||
                   SpnCountItems.Any(d => isChangedValue(d)) ||
                   SpnAverageItems.Any(d => isChangedValue(d)) ||
                   PreemnikItems.Any(d => isChangedValue(d));
        }

        protected override bool HasErrors()
        {
            return false;
        }
    }
}
