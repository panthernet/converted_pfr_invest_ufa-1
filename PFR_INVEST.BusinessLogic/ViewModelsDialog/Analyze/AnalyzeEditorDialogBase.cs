﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Core;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze
{
    public abstract class AnalyzeEditorDialogBase<T> : ViewModelCardDialog where T : YearReportBase
    {
        #region Properties/Fields

        protected int? _SelectedYear;
        protected RibbonStateBL ribbonState;
        protected List<KeyValuePair<int?, int?>> existedReports = new List<KeyValuePair<int?, int?>>();
        protected virtual bool isQuarkReport => typeof(T).IsSubclassOf(typeof(QuarkReportBase));
        public int? SelectedYear
        {
            get { return _SelectedYear; }
            set
            {
                if (value == null)
                {
                    _SelectedYear = null;
                    _SelectedKvartal = null;
                    OnPropertyChanged(nameof(SelectedYear));
                    return;
                }
                _SelectedYear = value;
                if (IsNewReport)
                {
                    if (isQuarkReport && _KvartalsSource != null)
                    {
                        Kvartals.Fill(_KvartalsSource.Where(q => !existedReports.Where(r => r.Key == value).Select(r => r.Value).Contains(q.Item1)));
                        if (Kvartals.Count > 0)
                        {
                            if (_curYear == SelectedYear)
                            {
                                var curQuark = Kvartals.FirstOrDefault(k => k.Item1 == _curQuark);
                                SelectedKvartal = curQuark ?? new Func<Tuple<int, string>>(() =>
                                {
                                    if (Kvartals.Any(k => k.Item1 < _curQuark))
                                        return
                                            Kvartals.OrderByDescending(x => x.Item1)
                                                .FirstOrDefault(k => k.Item1 < _curQuark);

                                    return Kvartals.FirstOrDefault();

                                }).Invoke();

                            }
                            else
                            {
                                SelectedKvartal = Kvartals.FirstOrDefault(k => k.Item1 == Kvartals.Max(x => x.Item1));
                            }
                            //SelectedKvartal = _curYear == SelectedYear 
                            //    ? (curQuark ?? Kvartals.FirstOrDefault(k => k.Item1 == (Kvartals.Where(x=> x.Item1 < _curQuark).OrderByDescending(x=>x.Item1).FirstOrDefault()?.Item1 ?? 1) )) 
                            //    : 
                        }
                    }
                    OnPropertyChanged(nameof(IsAllowSelectQuark));
                    OnPropertyChanged(nameof(SelectedKvartal));
                }
                OnPropertyChanged(nameof(SelectedYear));
            }
        }

        public bool IsAllowSelectQuark => SelectedYear != null;

        private Tuple<int, string> _SelectedKvartal;

        public Tuple<int, string> SelectedKvartal
        {
            get { return _SelectedKvartal; }
            set { SetPropertyValue(nameof(SelectedKvartal), ref _SelectedKvartal, value); }
        }

        public virtual string FormTitle => "Аналитические показатели ДОКИП";

        public RangeObservableCollection<int> Years { get; set; }
        private List<Tuple<int, string>> _KvartalsSource;
        //private List<Tuple<int, string>> _Kvartals;
        public RangeObservableCollection<Tuple<int, string>> Kvartals { get; private set; }

        public bool IsNewReport { get; protected set; }

        protected int _curQuark = DueHelper.GetAddSPNQuarter(DateTime.Now.Month);
        protected int _curYear = DateTime.Now.Year;
        public bool IsArchiveMode { get; set; }
        protected bool _isSaved; //Хак для того чтобы не выводился диалог подтверждения сохранения после нажатия кнопки "сохранить"

        public ViewModelListObservable<T> ArchiveReports { get; set; } //Список архивных отчетов

        #endregion

        public AnalyzeEditorDialogBase(RibbonStateBL state, ViewModelState vmState, bool isNewReport = false)
        {
            ribbonState = state;
            State = vmState;
            IsNewReport = isNewReport;
            Kvartals = new RangeObservableCollection<Tuple<int, string>>();
            Years = new RangeObservableCollection<int>();
            // myRoles = AP.Provider.CurrentUserSecurity.GetRoles();
            //OkCommand = new DelegateCommand(a => !IsArchiveMode &&
            //       HasChangesBase(), a =>
            //{
            //    SaveCard.Execute(null);
            //    IsDataChanged = false;
            //    RequestCloseView(true);
            //});

            InitDataList();


            RefreshConnectedCardsViewModels = RefreshConnectedCards;
        }

        protected virtual void RefreshConnectedCards()
        {
        }


        protected virtual void InitDataList()
        {
            if (IsNewReport)
                initQuartalsForNewReport();
            else
            {
                _KvartalsSource =
                    new List<Tuple<int, string>>(
                        QuarkDisplayHelper.GetQuarks().OrderBy(y => y.Index).Select(q => new Tuple<int, string>(q.Index, q.Name)));
                Years.Fill(DataContainerFacade.GetList<Year>().OrderBy(y => y.Name).Select(y => int.Parse(y.Name)));
                Kvartals.Fill(_KvartalsSource);
            }
            if (IsNewReport) { SelectedYear = Years.Any(y => y == _curYear) ? _curYear : Years.Any(y => y < _curYear) ? Years.Where(y=> y <_curYear).LastOrDefault() : Years.FirstOrDefault(); }
        }

        /// <summary>
        /// Set IsReadOnly for data objects
        /// </summary>
        /// <param name="objs"></param>
        protected void SetReadOnlyState(IEnumerable<BaseDataObject> objs)
        {
            if (IsReadOnly || IsArchiveMode)
                foreach (var o in objs)
                {
                    o.IsReadOnly = true;
                }
        }

        private void initQuartalsForNewReport()
        {
            var reports = LoadReports().Where(r => r.Status == 0).ToList();
            //SelectedYear = null;
            existedReports.Clear();
            if (isQuarkReport)
            {
                reports.ToList().ForEach(r => existedReports.Add(new KeyValuePair<int?, int?>((r as QuarkReportBase)?.Year, (r as QuarkReportBase)?.Kvartal)));
            }
            else
            {
                reports.ToList().ForEach(r => existedReports.Add(new KeyValuePair<int?, int?>((r as YearReportBase)?.Year, null)));
            }

            if (isQuarkReport)
            {
                var ys = DataContainerFacade.GetList<Year>();
                var qs = QuarkDisplayHelper.GetQuarks();

                foreach (var g in reports.GroupBy(r => r.Year))
                {
                    int countReports = 0;
                    foreach (var q in qs)
                    {
                        if (g.Any(x => (x as QuarkReportBase).Kvartal == q.Index))
                            countReports++;
                    }
                    if (countReports >= 4)
                        ys.Remove(ys.First(x => int.Parse(x.Name) == g.Key));
                }
                Years.Fill(ys.OrderBy(y => y.Name).Select(y => int.Parse(y.Name)));

                _KvartalsSource = new List<Tuple<int, string>>(qs.OrderBy(q => q.Index).Select(q => new Tuple<int, string>(q.Index, q.Name)));
            }
            else
            {
                var ry = reports.Select(r => r.Year).Distinct();
                var ys =
                   DataContainerFacade.GetList<Year>()
                       .Where(y => !ry.Contains(int.Parse(y.Name)));

                Years.Fill(ys.OrderBy(y => y.Name).Select(y => int.Parse(y.Name)));
            }
        }
        
        protected override bool BeforeExecuteSaveCheck()
        {
            //Проверка сохраненного отчета в БД
            if (IsNewReport && CheckExistSavedReport())
            {
                DialogHelper.ShowAlert("В базе данных обнаружен сохраненный отчет на выбранный временной период, сохранение не может быть выполнено");
                return false;
            }
            return base.BeforeExecuteSaveCheck();
        }
        /// <summary>
        /// Сохранение отчета
        /// </summary>
        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Create:
                    ExecuteSaveAction();
                    break;
                case ViewModelState.Edit:
                    if (DialogHelper.ConfirmHistoryChanged())
                    {
                        var r = GetCurrentReport();
                        if (r == null)
                            throw new NullReferenceException("GetCurrentReport() returns null. Expected: current report.");
                        r.Status = -1;
                        DataContainerFacade.Save(r);
                        ObtainAllReportData(r);
                        IsNewReport = true;
                        r.UpdateDate = DateTime.Now;//Текущая дата изменения
                        r.Id = 0;//обнуляем ID текущему отчету чтобы создался новый
                        r.Status = 0; //выставляем статус Активен текущему отчету (0 - активен, -1 - архив)
                        ReportDataResetID();//сброс ID записей данных отчета чтобы они сохранились в новом отчете
                        ExecuteSaveAction();//стандартное сохранение данных текущего отчета
                    }
                    else
                    {
                        ExecuteSaveAction();
                    }
                    break;
            }
            //ExecuteSaveAction();
            _isSaved = true;
        }
        /// <summary>
        /// Загрузка значений, которые возможно были введены в другом разделе (для большинства отчетов)
        /// </summary>
        /// <param name="report"></param>
        protected abstract void ObtainAllReportData(T report);
        /// <summary>
        /// Сброс ID записей данных отчета чтобы они сохранились в новом отчете (после сохранения архивных значений)
        /// </summary>
        protected abstract void ReportDataResetID();
        /// <summary>
        /// Этот метод должен содержать код сохранения числовых данных отчета внутри наследников класса
        /// </summary>
        protected abstract void ExecuteSaveAction();
        /// <summary>
        /// Вернуть отчет в базовый класс (для сохранения истории изменений)
        /// </summary>
        /// <returns></returns>
        protected abstract T GetCurrentReport();


       /// <summary>
       /// Проверить существование сохраненного отчета на выбранный период
       /// </summary>
       /// <returns></returns>
        protected virtual bool CheckExistSavedReport()
        {
            bool existSavedReport = isQuarkReport ? DataContainerFacade.GetList<T>().Any(r => r.Status == 0 && r.Year == SelectedYear && (r as QuarkReportBase).Kvartal == SelectedKvartal.Item1)
                : DataContainerFacade.GetList<T>().Any(r => r.Year == SelectedYear);
            return existSavedReport;
        }

        /// <summary>
        /// Проверка возможности сохранения отчета
        /// </summary>
        /// <returns></returns>
        public override bool CanExecuteSave()
        {
            return !_isSaved && !IsArchiveMode && !IsReadOnly &&
            !HasErrors() && ((State == ViewModelState.Create && Years.Count > 0 && HasChangesBase()) ||
               (State == ViewModelState.Edit && HasChanges()));
        }
        /// <summary>
        /// Наличие изменений данных
        /// </summary>
        /// <returns></returns>
        protected virtual bool HasChanges()
        {
            return IsDataChanged = CheckHaveChanges();
        }

        /// <summary>
        /// Проверка изменений в дочерних классах
        /// </summary>
        /// <returns></returns>
        protected abstract bool CheckHaveChanges();

        /// <summary>
        /// Проверка наличия ошибок валидации
        /// </summary>
        /// <returns></returns>
        protected abstract bool HasErrors();
        /// <summary>
        /// Базовая проверка изменений - выбран временной период
        /// </summary>
        /// <returns></returns>
        private bool HasChangesBase()
        {
            return IsDataChanged = isQuarkReport ? SelectedYear != null && SelectedKvartal != null : SelectedYear != null;
        }
        /// <summary>
        /// Загрузка списка отчетов для вывода истории изменений в диалоге редактора отчетов
        /// </summary>
        /// <returns></returns>
        protected abstract List<T> LoadReports();

        public override string this[string columnName]
        {
            get
            {
                return GetValidationError();
            }
        }
        /// <summary>
        /// Индивидуальная проверка ошибок в наследниках класса
        /// </summary>
        /// <returns></returns>
        protected virtual string GetValidationError()
        {
            return string.Empty;
        }
    }
}
