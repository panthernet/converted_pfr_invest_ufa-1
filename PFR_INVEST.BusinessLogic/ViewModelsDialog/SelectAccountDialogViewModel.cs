﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class SelectAccountDialogViewModel : ViewModelBase
    {
        //private long selectedAccountID = 0;

        public long SelectedAccountID => (_selectedAccount != null)?_selectedAccount.ID:-1;

        private List<PFRAccountsListItem> pfrAccountsList;
        public List<PFRAccountsListItem> PFRAccountsList
        {
            get { return pfrAccountsList; }
            set { pfrAccountsList = value; OnPropertyChanged("PFRAccountsList"); }
        }
        public PFRAccountsListItem _selectedAccount = null;
        public PFRAccountsListItem SelectedAccount
        {
            get
            {
                return _selectedAccount;
            }
            set
            {
                _selectedAccount = value;
                OnPropertyChanged("SelectedAccount");
                OnPropertyChanged("SelectedAccountID");
                OnPropertyChanged("IsValid");
            }
        }

        private List<Element> accountAssignKindList;
        public List<Element> AccountAssignKindList
        {
            get { return accountAssignKindList; }
            set { accountAssignKindList = value; }
        }

        private Element selectedAssign;
        public Element SelectedAssign
        {
            get { return selectedAssign; }
            set { selectedAssign = value; OnPropertyChanged("SelectedAssign"); }
        }

        public bool IsValid => SelectedAccount != null;

        public SelectAccountDialogViewModel(long[] excludeIDs)
        {
            PFRAccountsList = new List<PFRAccountsListItem>();
            var list =BLServiceSystem.Client.GetPFRAccountsList();

            list.ForEach(a =>
            {
                if (excludeIDs != null && System.Array.Exists<long>(excludeIDs, id => id == a.ID))
                    return;
                PFRAccountsList.Add(a);
            });

            OnPropertyChanged("PFRAccountsList");

            AccountAssignKindList = BLServiceSystem.Client.GetElementByType(Element.Types.AccountAssignKind);
            SelectedAssign = AccountAssignKindList.FirstOrDefault(x => x.Order == 2); // 2 - не основной
        }

        public SelectAccountDialogViewModel():this(null){ }
    }
}
