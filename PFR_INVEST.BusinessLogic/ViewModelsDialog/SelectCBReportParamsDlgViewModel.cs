﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class SelectCBReportParamsDlgViewModel : ViewModelCardDialog
    {
        private string _defaultFileName;
        public string DefaultFileName
        {
            get { return _defaultFileName; }
            set { _defaultFileName = value; OnPropertyChanged("DefaultFileName"); }
        }

        private string _okato;
        public string OKATO
        {
            get { return _okato; }
            set { _okato = value; OnPropertyChanged("OKATO"); }
        }

        private string _inn;
        public string INN
        {
            get { return _inn; }
            set { _inn = value; OnPropertyChanged("INN"); }
        }

        private string _ogrn;
        public string OGRN
        {
            get { return _ogrn; }
            set { _ogrn = value; OnPropertyChanged("OGRN"); }
        }

        private string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; OnPropertyChanged("FullName"); }
        }

        private string _shortName;
        public string ShortName
        {
            get { return _shortName; }
            set { _shortName = value; OnPropertyChanged("ShortName"); }
        }

        private DateTime? _regDate;
        public DateTime? RegDate
        {
            get { return _regDate; }
            set { _regDate = value; OnPropertyChanged("RegDate"); }
        }

        private string _regNum;
        public string RegNum
        {
            get { return _regNum; }
            set { _regNum = value; OnPropertyChanged("RegNum"); }
        }

        private Person _selectedPerson;
        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set { _selectedPerson = value; OnPropertyChanged("SelectedPerson"); }
        }

        private List<Person> _personList;
        public List<Person> PersonList
        {
            get { return _personList; }
            set { _personList = value; OnPropertyChanged("PersonList"); }
        }

        private bool _isFileVisible;
        public bool IsFileVisible
        {
            get { return _isFileVisible; }
            set { _isFileVisible = value; OnPropertyChanged("IsFileVisible"); }
        }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; OnPropertyChanged("FileName"); }
        }

       // public ICommand OpenFileCommand { get; set; }
        public ICommand OkCommand { get; set; }

        public SelectCBReportParamsDlgViewModel(BOReportForm1 report, bool includeFileSelect = false):base(true)
        {
            DefaultFileName = report.OKUD == null ? $"Месячный отчет в ЦБ - {DateTime.Today.ToShortDateString()}" : string.Format(@"ПУРЦБ_ОКУД041800{1}-{0}",DateTime.Today.ToShortDateString(), report.RType == BOReportForm1.ReportTypeEnum.One ? 1 : (report.RType == BOReportForm1.ReportTypeEnum.Two ? 2 : 3));
            IsFileVisible = includeFileSelect;
            //OpenFileCommand = new DelegateCommand(a => FileName = DialogHelper.SaveFile("XTDD файлы (*.XTDD)|*.XTDD"));
            OkCommand = new DelegateCommand(a=> Validate(), a=> RequestCloseView(true));
            PersonList = DataContainerFacade.GetList<Person>().Where(p => !p.IsDisabled).OrderBy(a => a.FormattedFullName).ToList();
            BOReportForm1 prevReport = new BOReportForm1();
            if (string.IsNullOrEmpty(report.OKATO))
                prevReport = BLServiceSystem.Client.TryGetPreviousReport(report) ?? prevReport;

            OKATO = report.OKATO ?? prevReport.OKATO;
            INN = report.INN ?? prevReport.INN;
            OGRN = report.OGRN ?? prevReport.OGRN;
            FullName = report.FullName ?? prevReport.FullName;
            ShortName = report.ShortName ?? prevReport.ShortName;
            RegNum = report.RegNum ?? prevReport.RegNum;
            RegDate = report.RegDate ?? prevReport.RegDate;
            var pId = report.PersonID == 0 ? prevReport.PersonID : report.PersonID;
            SelectedPerson = PersonList.FirstOrDefault(a => a.ID == pId);

        }

        private bool Validate()
        {
            var fields = "OKATO|INN|OGRN|FullName|ShortName|RegDate|RegNum|SelectedPerson";
            if (_isFileVisible) fields += "|FileName";
            return fields.Split('|').All(e => string.IsNullOrEmpty(this[e]));
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "OKATO":
                        return _okato.ValidateNonEmpty() ?? _okato.ValidateMaxLength(8);
                    case "INN":
                        return _inn.ValidateNonEmpty() ?? ((INN.Length == 10 && DataTools.IsNumericString(INN)) ? null : "Неверный формат данных");
                    case "OGRN":
                        return _ogrn.ValidateNonEmpty() ?? _ogrn.ValidateMaxLength(13);
                    case "FullName":
                        return _fullName.ValidateNonEmpty() ?? _fullName.ValidateMaxLength(100);
                    case "ShortName":
                        return _shortName.ValidateNonEmpty() ?? _shortName.ValidateMaxLength(100);
                    case "RegDate":
                        return _regDate.ValidateRequired();
                    case "RegNum":
                        return _regNum.ValidateNonEmpty() ?? _regNum.ValidateMaxLength(15);
                    case "SelectedPerson":
                        return _selectedPerson != null ? null : "Неверный формат данных";
                    case "FileName":
                        return _fileName.ValidateNonEmpty();
                    default:
                        return null;
                }
            }
        }
    }
}
