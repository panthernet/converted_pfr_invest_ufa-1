﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OFPR_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OFPR_manager)]
    public sealed class AddSumForPortfolioViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        private ObservableCollection<PortfolioFullListItem> _portfolioList;
        private PortfolioFullListItem _selectedItem;
        private decimal _sum;
        private long mainAssignId;

        public string OkButtonText { get; set; }

        public string TitleText { get; set; }


        public AddSumForPortfolioViewModel(long offerId, decimal sum)
            :this()
        {
            OkButtonText = "Добавить";
            TitleText = "Добавить сумму по портфелю";

            Result = new OfferPfrBankAccountSum { OfferId = offerId };
            Sum = sum > 0 ? sum : 0;
            ExecuteRefreshList();
        }

        //Edit
        public AddSumForPortfolioViewModel(OfferPfrBankAccountSum sum)
            :this()
        {
            OkButtonText = "OK";
            TitleText = "Изменить сумму по портфелю";

            Result = sum;
            Sum = Result.Sum;
            ExecuteRefreshList();

            SetSelectedItem(Result);
        }

        public AddSumForPortfolioViewModel()
        {
            var elements = BLServiceSystem.Client.GetElementByType(Element.Types.AccountAssignKind);
            mainAssignId = elements.First(x => x.Order == 1).ID; // 1 - основной
        }

        private void SetSelectedItem(OfferPfrBankAccountSum s)
        {
            PortfolioFullListItem x = PortfolioList.FirstOrDefault(p =>
                p.PfrBankAccount.ID == s.PfrBankAccountId &&
                p.Year == s.Year &&
                p.PFType == s.Type &&
                p.PFName == s.Name &&
                p.PortfolioID == s.PortfolioId &&
                p.AccountNumber == s.AccountNum);


            if (x != null)
                SelectedItem = x;
        }

        public ObservableCollection<PortfolioFullListItem> PortfolioList
        {
            get
            {
                return _portfolioList;
            }
            set
            {
                _portfolioList = value;
                OnPropertyChanged("PortfolioList");
            }
        }

        public PortfolioFullListItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public delegate void SelectGridRowDelegate(long RID);

        public event SelectGridRowDelegate OnSelectGridRow;

        public void RaiseSelectGridRow(long RID)
        {
            if (OnSelectGridRow != null)
            {
                OnSelectGridRow(RID);
            }
        }

        public OfferPfrBankAccountSum Result { get; private set; }

        public decimal Sum
        {
            get { return _sum; }
            set
            {
                _sum = value;
                OnPropertyChanged("Sum");
            }
        }

        public int ValidationMaxDigits = 18;

        private void ExecuteRefreshList()
        {
            var list = new List<PortfolioFullListItem>();

            list.AddRange(BLServiceSystem.Client.GetPortfoliosByType());           

            list = list.Where(p => p.PfrBankAccount.CurrencyID == 1).ToList();
            PortfolioList = new ObservableCollection<PortfolioFullListItem>(list);
        }

        protected override void ExecuteSave()
        {
            Result.PfrBankAccountId = SelectedItem.PfrBankAccount.ID;
            Result.Year = SelectedItem.Year;
            Result.TypeValue = SelectedItem.Portfolio == null ? (long)Portfolio.Types.NoType : SelectedItem.Portfolio.TypeID;
            Result.TypeText = DataContainerFacade.GetByID<Element>(Result.TypeValue).Name;
            Result.Name = SelectedItem.PFName;
            Result.PortfolioId = SelectedItem.PortfolioID;
            Result.AccountNum = SelectedItem.AccountNumber;

            Result.Sum = Sum;

            Result.IsDirty = true;
        }

        public override bool CanExecuteSave()
        {
            return SelectedItem != null && string.IsNullOrEmpty(this["Sum"]);
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Sum": 
                        if (Sum.ValidateMaxFormat(ValidationMaxDigits) != null) return "Недопустимо большое значение";
                        if (Sum < 0) return "Значение не может быть меньше 0";
                        if (Sum == 0) return "Недопустимое значение 0";
                        break;
                }
                return string.Empty;
            }
        }

        public void SetSelectionByPortfolio(long p)
        {
            var accounts = this.PortfolioList.Where(x=>x.PortfolioID == p).Select(x=>x.PortfolioPFRBankAccount);
            var link = accounts.FirstOrDefault(x => x.AssignKindID == mainAssignId);
            SelectedItem = link != null ? this.PortfolioList.First(x => x.AccountID == link.PFRBankAccountID && x.PortfolioID == link.PortfolioID) : null;
        }

        public void SetSelectionByAccount(long p, long a)
        {
            SelectedItem = this.PortfolioList.First(x => x.AccountID == a && x.PortfolioID == p);
        }
    }
}
