﻿using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager)]
    public sealed class SIContactDlgViewModel : ViewModelCardDialog
    {
        #region Fields

        Contact contact;

        public string FIO
        {
            get { return contact == null ? null : contact.FIO; }
            set
            {
                if (contact != null)
                    contact.FIO = value;
                OnPropertyChanged("FIO");
            }
        }

        public string Phone
        {
            get { return contact == null ? null : contact.Phone; }
            set
            {
                if (contact != null)
                    contact.Phone = value;
                OnPropertyChanged("Phone");
            }
        }

        public string EMail
        {
            get { return contact == null ? null : contact.Email; }
            set
            {
                if (contact != null)
                    contact.Email = value;
                OnPropertyChanged("EMail");
            }
        }

        #endregion

        #region Execute Implementations
        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;

                    case ViewModelState.Create:
                    case ViewModelState.Edit:
                        contact.ID = ID = DataContainerFacade.Save(contact);
                        break;
                }
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return contact != null && contact.ID > 0;
        }

        protected override void ExecuteDelete(int delType = 1)
        {
            DataContainerFacade.Delete(contact);
            base.ExecuteDelete(DocOperation.Delete);
        }
        #endregion

        #region validation

        private const string NOT_NULL_FIELDS = "FIO|PHONE|EMAIL";
        private const string S_ERROR = "Неверный формат данных";

        public override string this[string columnName]
        {
            get
            {
                if (contact == null)
                    return S_ERROR;

                switch (columnName.ToUpper())
                {
                    case "FIO":
                        return !string.IsNullOrEmpty(contact.FIO) ? contact.FIO.ValidateMaxLength(512) : S_ERROR;
                    case "PHONE":
                        return !string.IsNullOrEmpty(contact.Phone) ? contact.Phone.ValidateMaxLength(32) : S_ERROR;
                    case "EMAIL":
                        return !string.IsNullOrEmpty(contact.Email) ? (!contact.Email.ValidateEmail() ? S_ERROR : contact.Email.ValidateMaxLength(64)) : S_ERROR;
                    default:
                        return null;
                }
            }
        }

        private string Validate()
        {
            if (contact == null)
                return S_ERROR;

            return NOT_NULL_FIELDS.Split('|').Any(fld => !string.IsNullOrEmpty(this[fld])) ? S_ERROR : null;
        }
        #endregion

        public SIContactDlgViewModel(long leID)
        {
            DataObjectTypeForJournal = typeof (Contact);
            contact = new Contact {LegalEntityID = leID};
        }

        public SIContactDlgViewModel(long identity, long leID) : base(typeof(SIListViewModel))
        {
            contact = DataContainerFacade.GetByID<Contact>(identity) ?? new Contact();
            contact.LegalEntityID = leID;
        }
    }
}
