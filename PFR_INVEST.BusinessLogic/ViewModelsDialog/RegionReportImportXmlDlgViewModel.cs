﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using System.Globalization;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.BranchReport;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class RegionReportImportXmlDlgViewModel : ViewModelBase, IRequestCloseViewModel
    {
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand<object> OpenCatalogCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }

		public RegionReportImportXmlDlgViewModel()
		{
			ReportTypes = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportType>(new List<ListPropertyCondition>
			{
				ListPropertyCondition.Equal("Source", (int)PfrBranchReportType.SourceMode.Xml)
			}).ToList();

            OpenFileCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenFile);
            OpenCatalogCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenCatalog);
            ImportCommand = new DelegateCommand(o => CanExecuteImport(), ExecuteImport);
        }


        public List<PfrBranchReportType> ReportTypes
        {
            get;
            set;
        }

        PfrBranchReportType _SelectedReportType;
        public PfrBranchReportType SelectedReportType
        {
            get { return _SelectedReportType; }
            set
            {
                _SelectedReportType = value;
                SetImportHandler();
                OnPropertyChanged("SelectedReportType");
                OnPropertyChanged("SelectedReportDescription");
                OnPropertyChanged("Items");
                OnPropertyChanged("FileName");
                OnPropertyChanged("IsMonthVisible");
            }
        }

        private void SetImportHandler()
        {
            ImportHandler = PfrBranchReportImportXmlFactory.GetImportHandler(this);
            ImportHandler.OnLoadAlredyLoadedFileHandler = new PfrBranchReportImportXmlHandlerBase.LoadAlredyLoadedFileHandler(ConfirmLoadAlreadyLoadedFile);
        }

        public bool ConfirmLoadAlreadyLoadedFile(PfrBranchReportImportContentBase fileParamInfo) {
            String msg = "Данные по отчету {regname} на {year} год и {month} месяц уже загружены ранее.\nЖелаете переписать данные?";
            msg = msg
				.Replace("{regname}", fileParamInfo.RegionName)
				.Replace("{year}", fileParamInfo.PeriodYear.ToString())
				.Replace("{month}", fileParamInfo.PeriodMonth.HasValue ? fileParamInfo.PeriodMonth.Value.ToString() : "");
            return DialogHelper.ShowConfirmation(msg); 
        }

        public string SelectedReportDescription
        {
            get
            {
                if (SelectedReportType == null) return null;
                return SelectedReportType.FullName;
            }
        }

        public ObservableList<PfrBranchReportImportContentBase> Items => ImportHandler.Items;

        public string FileName
        {
            get
            {
                var xL = (from x in Items select x.PfrBranchReport.FileName).Distinct();
                return string.Join("; ", xL.ToArray());
            }
            set { }
        }

        private PfrBranchReportImportXmlHandlerBase _ImportHandler;
        private PfrBranchReportImportXmlHandlerBase ImportHandler
        {
            get { return _ImportHandler ?? (_ImportHandler = new PfrBranchReportImportXmlHandler_Empty()); }
	        set
            { _ImportHandler = value; }
        }


        private bool CanExecuteOpenFile()
        {
            return ImportHandler.CanExecuteOpenFile();
        }

        private void OpenFile(object o)
        {
            string[] fileNames = DialogHelper.OpenFiles(ImportHandler.GetOpenFileMask());

            if (fileNames == null)
                return;

            LoadFiles(fileNames);
            OnPropertyChanged("FileName");
        }

        private void OpenCatalog(object o)
        {
            var directoryPath = DialogHelper.OpenCatalog();

            if (directoryPath == null)
                return;

            List<string> fileNames = new List<string>();

            foreach (string mask in ImportHandler.GetOpenDirectoryMask().Split(';'))
            {
                fileNames.AddRange(Directory.GetFiles(directoryPath, mask, SearchOption.TopDirectoryOnly));
            }

            if (fileNames.Count == 0)
                return;

            LoadFiles(fileNames.ToArray());
            OnPropertyChanged("FileName");
        }

        private void LoadFiles(string[] fileNames)
        {
            if (fileNames == null || fileNames.Length == 0)
                return;

            CultureInfo oldCI = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			
			try
            {
                foreach (string fileName in fileNames)
                {
                    try
                    {                       
                        string message;
                        if (!ImportHandler.LoadFile(fileName, out message) && !String.IsNullOrEmpty(message))
                        {
                            DialogHelper.ShowAlert(message);
                            continue;
                        }
                    }
                    catch (PfrBranchReportImportException ex)
                    {
                        DialogHelper.ShowAlert(ex.Message);
                        Logger.WriteException(ex);
                    }
                    catch (FileNotFoundException ex)
                    {
                        DialogHelper.ShowAlert(ex.Message);
                        Logger.WriteException(ex);
                    }
                    catch (Exception ex)
                    {
                        DialogHelper.ShowAlert("Файл содержит некорректные данные. Импорт не может быть произведен.\nДетальная информация об ошибке может быть найдена в лог файле приложения.");
                        Logger.WriteException(ex);
                    }
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCI;
            }
        }

        public bool CanExecuteImport()
        {
            return ImportHandler.CanExecuteImport();
        }

        private void ExecuteImport(object o)
        {
            if (!this.CanExecuteImport())
                return;

            try
            {
                //var importedCount = 0;

                var res = ImportHandler.DoImport();

                switch (res.Status)
                {
                    case PfrBranchReportImportResponse.enStatus.OK:
                        FinishImport(ImportHandler.Items.Count);
                        return;

                    case PfrBranchReportImportResponse.enStatus.Error:
                        AlertImportError();

                        break;
                    case PfrBranchReportImportResponse.enStatus.Duplicate:
                        {
                            string msg = GetConfirmDuplicateMessage(res);
                            bool IsOverwriteDuplicate = DialogHelper.ShowConfirmation(msg);//!!Todo:add dialog

                            if (IsOverwriteDuplicate)
                            {
                                var res2 = ImportHandler.DoImportDuplicate(res);
                                switch (res2.Status)
                                {
                                    case PfrBranchReportImportResponse.enStatus.OK:
                                        FinishImport(ImportHandler.Items.Count);
                                        return;
                                    case PfrBranchReportImportResponse.enStatus.Error:
                                        AlertImportError();
                                        break;
                                }
                            }
                            else
                            {
                                FinishImport(ImportHandler.Items.Count - res.DuplicateItemKeys.Length);
                                return;
                            }
                            break;
                        }
                }

            }
            catch (PfrBranchReportImportException ex)
            {
                Logger.WriteException(ex);
                DialogHelper.ShowAlert(string.Format("Во время выполнения операции импорта возникла ошибка '{0}'. Детальная информация об ошибке может быть найдена в логах приложения.", ex.Message));
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex);
                DialogHelper.ShowAlert(string.Format("Во время выполнения операции импорта возникла ошибка '{0}'. Детальная информация об ошибке может быть найдена в логах приложения.", ex.Message));
            }

            UpdateView();
            return;
        }


        private string GetConfirmDuplicateMessage(PfrBranchReportImportResponse res)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Данные по следующим отчетам уже импортированы ранее:");
            for (int i = 0; i < res.DuplicateItemKeys.Length && i < 3; i++)
            {
                var item = Items.First(p => p.ReportKey == res.DuplicateItemKeys[i]);

                if (item.PeriodMonth == null)
                    sb.AppendFormat("{0:00} {1} ({2:0000}) \r\n", item.RegionCode, item.RegionName, item.PeriodYear);
                else
                    sb.AppendFormat("{0:00} {1} ({2:0000}-{3:00}) \r\n", item.RegionCode, item.RegionName, item.PeriodYear, item.PeriodMonth);
            }
            if (res.DuplicateItemKeys.Length > 3)
                sb.AppendLine("и др.");

            sb.AppendLine("");
            sb.AppendLine("Вы хотите перезаписать данные отчетов?");
            sb.AppendLine("Для подтверждения импорта отчетов и удаления ранее импортированных нажмите 'Да'.");
            sb.AppendLine("Чтобы оставить текущие данные и не импортировать новые нажмите 'Нет'.");

            return sb.ToString();
        }

        private void AlertImportError()
        {
            DialogHelper.ShowAlert("Во время выполнения операции импорта возникла ошибка. Детальная информация об ошибке может быть найдена в логах приложения.");
        }

        private void FinishImport(int n)
        {
            if (n == 0)
                DialogHelper.ShowAlert("Импорт завершён. Ни один отчет импортирован не был.");
            else
                DialogHelper.ShowAlert("Импорт завершён успешно");

            CloseWindow();
        }

        private void UpdateView()
        {
            SelectedReportType = SelectedReportType;
        }


        public event EventHandler RequestClose;

        public void CloseWindow()
        {
            if (RequestClose != null)
                RequestClose(this, null);
        }

        public bool IsMonthVisible => ImportHandler.IsMonthVisible;
    }
}
