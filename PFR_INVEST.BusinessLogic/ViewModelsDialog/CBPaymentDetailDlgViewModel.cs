﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataAccess.Client;
using System.Linq;
using System.Windows.Input;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class CBPaymentDetailDlgViewModel : ViewModelCardDialog
    {
        public CBPaymentDetail Item { get; private set; }

        public List<Portfolio> PortfolioList { get; set; }
        public List<PaymentDetail> PaymentDetailList { get; set; }

        //public DelegateCommand<object> SaveCard { get; private set; }

        public ICommand OkCommand { get; set; }

        public CBPaymentDetailDlgViewModel(CBPaymentDetail item)
        {
            Item = item;
            ID = Item.ID;
            Item.PropertyChanged += (sender, e) => { IsDataChanged = true; };
            
            SaveCard = new DelegateCommand<object>(o => IsValid(), o =>
            {
                // Обрезаем пробелы в назначении платежа
                Item.ReportPaymentDetail = Item.ReportPaymentDetail.Trim();

                DataContainerFacade.Save(Item);
            });

            PortfolioList = DataContainerFacade.GetList<Portfolio>().OrderBy(a => a.Name).ToList();
            PaymentDetailList = DataContainerFacade.GetList<PaymentDetail>().OrderBy(a => a.PaymentDetails).ToList();

            OkCommand = new DelegateCommand(a => IsValid(), a => {
                var e = DataContainerFacade.GetListByPropertyConditions<CBPaymentDetail>(new List<ListPropertyCondition>
                {
                    ListPropertyCondition.Equal("PortfolioID", Item.PortfolioID),
                    ListPropertyCondition.Equal("PaymentDetailID", Item.PaymentDetailID),
                    ListPropertyCondition.NotEqual("ID", Item.ID),
                    ListPropertyCondition.NotEqual("StatusID", -1L)
                });

                if (e.Any())
                {
                    DialogHelper.ShowAlert(
                        "Введенное Назначение платежа по платежному поручению \"{0}\" для портфеля \"{1}\" уже есть в системе!",
                        PaymentDetailList.First(x => x.ID == Item.PaymentDetailID).PaymentDetails,
                        PortfolioList.First(x => x.ID == Item.PortfolioID).Name);
                    return;
                }

                SaveCard.Execute(null);
                IsDataChanged = false;

                RequestCloseView(true);
            });
        }

        private bool IsValid()
        {
            return IsDataChanged && Item.IsValid();
        }

        //public bool? AskSaveOnClose()
        //{
        //    //IsDataChanged = true;
        //    if (IsDataChanged)
        //    {
        //        // TODO
        //        return DialogHelper.ConfirmLegalEntityCourierSaveOnClose();
        //    }
        //    return false;
        //}

        //public bool CanSaveOnClose()
        //{
        //    bool b = IsValid();
        //    if (b) return true;

        //    DialogHelper.ShowAlert("Невозможно сохранить: некорректные значения полей");
        //    return false;
        //}
    }
}
