﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.UKReport;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class UKReportImportDlgViewModel : ViewModelBase, IRequestCloseViewModel
    {
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand<object> OpenCatalogCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }
        public DelegateCommand DeleteCommand { get; private set; }

        public bool IsXbrlAvailable => _selectedReportType != null && (_selectedReportType.ReportType == "F060" || _selectedReportType.ReportType == "F070");
        public Visibility ConvTypeVisibility => IsXbrlAvailable ? Visibility.Visible : Visibility.Collapsed;
        public bool IsXbrlMode { get; set; } = true;
        public bool IsXmlMode { get; set; } = false;

        public UKReportImportDlgViewModel()
        {
            ReportTypes = DataContainerFacade.GetList<UKReportType>();

            OpenFileCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenFile);
            OpenCatalogCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenCatalog);
            ImportCommand = new DelegateCommand(o => CanExecuteImport(), ExecuteImport);
            DeleteCommand = new DelegateCommand(o => CanExecuteDeleteItem(), ExecuteDeleteItem);

        }
        
        public List<UKReportType> ReportTypes
        {
            get;
            set;
        }

        UKReportType _selectedReportType;
        public UKReportType SelectedReportType
        {
            get { return _selectedReportType; }
            set
            {
                _selectedReportType = value;
                SetImportHandler();
                OnPropertyChanged("SelectedReportType");
                OnPropertyChanged("SelectedReportDescription");
                OnPropertyChanged("SelectedReportCode");
                OnPropertyChanged("Items");
                OnPropertyChanged("ConvTypeVisibility");
                //OnPropertyChanged("FileName");
            }
        }
      
        public bool IsEnabledSelectedReportType => !ImportHandler.IsImportDoWork;

        /// <summary>
        /// описание отчета
        /// </summary>
        public string SelectedReportDescription => SelectedReportType == null ? null : SelectedReportType.FullName;

        /// <summary>
        /// код отчета
        /// </summary>
        public string SelectedReportCode => SelectedReportType == null ? null : (SelectedReportType.ReportType + (SelectedReportType.ReportType == "F140" ? ", F141" : null));

        private List<IUKReportImportItem> _SelectedRemovedItems;
        public List<IUKReportImportItem> SelectedRemovedItems
        {
            get
            {
                return _SelectedRemovedItems;
            }

            set {
                _SelectedRemovedItems = value;
                ImportHandler.SelectedRemovedItems = _SelectedRemovedItems;
                OnPropertyChanged("_SelectedRemovedItems");
            }
        }




        private void SetImportHandler()
        {
            ImportHandler = UKReportImportFactory.GetImportHandler(this);
            if(ImportHandler is UKReportImportHandlerEmpty && this.SelectedReportType!=null)
                DialogHelper.ShowInformation(string.Format("По состоянию на {0}\nреализация импорта отчета\n\"{1}\"\nотсутствует.", DateTime.Now, this.SelectedReportType.ShortName),
                    "ПТК ДОКИП информирует");
        }


        /// <summary>
        /// список импортируемых файлов
        /// </summary>
        public ObservableList<IUKReportImportItem> Items => ImportHandler.Items;


        public Visibility ProgressVisibility => ImportHandler.IsImportDoWork ? Visibility.Visible : Visibility.Collapsed;


        private UKReportImportHandlerBase _ImportHandler;
        /// <summary>
        /// обработчик импорта
        /// </summary>
        public UKReportImportHandlerBase ImportHandler
        {
            get
            {
                if (_ImportHandler == null)
                    _ImportHandler = new UKReportImportHandlerEmpty();

                return _ImportHandler;
            }
            set
            { _ImportHandler = value; }
        }


        private  bool CanExecuteOpenFile()
        {
           // OnPropertyChanged("SelectedReportType");
            return ImportHandler.CanExecuteOpenFile() && this.SelectedReportType != null;
        }

        public bool CanExecuteImport()
        {
            OnPropertyChanged("ProgressVisibility");
            OnPropertyChanged("IsEnabledSelectedReportType");
            return ImportHandler.CanExecuteImport();
        }


        public bool CanExecuteDeleteItem()
        {
            return ImportHandler.CanExecuteDelete();
        }

        private void ExecuteDeleteItem(object o)
        {
            ImportHandler.ExecuteDelete();
            OnPropertyChanged("Items");
        }


        private void OpenFile(object o)
        {
            string[] fileNames = DialogHelper.OpenFiles(ImportHandler.GetOpenFileMask(IsXbrlAvailable && IsXbrlMode));

            if (fileNames == null)
                return;

            LoadFiles(fileNames);
            //OnPropertyChanged("FileName");
            OnPropertyChanged("Items");
        }



        private void OpenCatalog(object o)
        {
            var directoryPath = DialogHelper.OpenCatalog();

            if (directoryPath == null)
                return;

            List<string> fileNames = new List<string>();

            var arr = IsXbrlAvailable && IsXbrlMode ? ImportHandler.GetOpenFileMask(IsXbrlAvailable && IsXbrlMode).Split(';') : ImportHandler.GetOpenDirectoryMask().Split(';');
            var list = arr.Select(s => s.Contains("|") ? s.Split('|')[1] : s).ToList();

            foreach (string mask in list)
            {
                fileNames.AddRange(Directory.GetFiles(directoryPath, mask, SearchOption.TopDirectoryOnly));
            }

            if (fileNames.Count == 0)
                return;

            LoadFiles(fileNames.ToArray(), true);
            OnPropertyChanged("FileName");
        }

        private void LoadFiles(string[] fileNames, bool isCatalogLoad = false)
        {
            if (fileNames == null || fileNames.Length == 0)
                return;


            CultureInfo oldCI = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            try
            {
                var isXbrl = IsXbrlAvailable && IsXbrlMode;
                
                foreach (string fileName in fileNames)
                {
                    try
                    {

                        ActionResult result = ImportHandler.LoadFile(fileName, isXbrl);
                        if (!result.IsSuccess)
                        {
                            DialogHelper.ShowAlert(result.ErrorMessage);
                            continue;
                        }

                        if (!isCatalogLoad && result.IsTotalReport)
                        {
                            DialogHelper.ShowAlert("Данный файл является сводным и не содержит информации идентифицирующей УК.\nИмпорт данного файла не предусмотрен!");
                            continue;
                        }
                    }

                    catch (Exception ex)
                    {
                        DialogHelper.ShowAlert(ex.Message);
                        Logger.WriteException(ex);
                    }

                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCI;
            }
        }


        private void ExecuteImport(object o)
        {
           if (!this.CanExecuteImport())  return;

           //OnPropertyChanged("Maximum");
           //OnPropertyChanged("Current");

           ImportHandler.ExecuteImport();
           OnPropertyChanged("ProgressVisibility");
           //if (ImportHandler.IsProgressVisibility)
           //{
           //    TimerProgress = new System.Timers.Timer(1000);
           //    TimerProgress.Elapsed += new ElapsedEventHandler(OnTimedEvent);
           //    TimerProgress.Start();
           //}
           

        }

        //private void OnTimedEvent(object source, ElapsedEventArgs e)
        //{
        //  ImportHandler.Current = ImportHandler.GetCurrentProgress();
        //    OnPropertyChanged("Current");
        //    if (!ImportHandler.IsImportDoWork)
        //    {
        //        OnPropertyChanged("ProgressVisibility");
        //        TimerProgress.Close();
        //    }
        //}
     

        public event EventHandler RequestClose;

    }
}
