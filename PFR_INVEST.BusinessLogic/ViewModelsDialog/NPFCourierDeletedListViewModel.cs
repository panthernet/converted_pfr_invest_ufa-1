﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class NPFCourierDeletedListViewModel : ViewModelBase
    {
        public List<LegalEntityCourier> Items { get; private set; }

        public NPFCourierDeletedListViewModel() : base() { }

        public NPFCourierDeletedListViewModel(List<LegalEntityCourier> items) : this() 
        {
            Items = items;
        }

        public void ViewCourier(LegalEntityCourier x)
        {
            DialogHelper.ViewNpfCourier(x);
        }
    }
}
