﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using System.Linq;
using System.IO;
using System.Threading;
using PFR_INVEST.BusinessLogic.Journal;
using ex = Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects.ListItems;
using System;
using System.Globalization;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Drawing;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Exceptions;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class GarantFondDialogViewModel : ViewModelBase
    {
        private const string TEMPLATE_NAME = "Отчисления в гарантийный фонд.xls";
        private Application _mApp;
        private Worksheet _mSheet;
        private Range _range;

        private void Generate()
        {
            try
            {
                var nwList = new List<NetWealthListItem>();
                List<NetWealthListItem> part;

                var cList = DataContainerFacade.GetList<Contract>();

                var start = new DateTime(SelectedYear.Value, 1, 1);
                var end = new DateTime(SelectedYear.Value + 1, 1, 1).AddDays(-1);

                while ((part = BLServiceSystem.Client.GetNetWealthsF010ListByPage((int)Document.Types.SI, nwList.Count, start, end).ToList()).Count > 0)
                    nwList.AddRange(part);

                while ((part = BLServiceSystem.Client.GetNetWealthsF015ListByPage((int)Document.Types.SI, nwList.Count, start, end).ToList()).Count > 0)
                    nwList.AddRange(part);

                //nwList = nwList.OrderBy(x => x.UKName).ThenBy(x => x.ContractNumber).ToList();

                var nwGroup = nwList.GroupBy(x => x.ContractId);

                _range = _mSheet.Range[_mSheet.Cells[1, 1], _mSheet.Cells[1, 1]];
                _range.Value2 = (_range.Value2 as string).Replace("{YEAR}", SelectedYear.Value.ToString());

                _range = _mSheet.Range[_mSheet.Cells[3, 5], _mSheet.Cells[3, 5]];
                _range.Value2 = (_range.Value2 as string).Replace("{YEAR}", SelectedYear.Value.ToString());

                long count = nwGroup.Count();
                _range = _mSheet.Range[_mSheet.Cells[5, 1], _mSheet.Cells[5, 6]];
                for (var i = 1; i < count; i++)
                {
                    _range.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);
                }

                long rownum = 0;

                var list = nwGroup.OrderBy(x => x.First().UKName).ThenBy(x => x.First().ContractNumber).ToList();
                foreach (var nwG in list)
                {
                    var dayCount = nwG.Select(x => x.ReportOnDate).Distinct().Count();

                    var sum = nwG.Sum(x => x.NetWealthSum ?? (decimal?)0) / dayCount;

                    var nw = nwG.First();

                    var date = cList.FirstOrDefault(x => x.ID == nw.ContractId)?.ContractDate;

                    _range = _mSheet.Range[_mSheet.Cells[4 + rownum, 1], _mSheet.Cells[4 + rownum, 6]];
                    //Range.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);
                    //Range = m_sheet.get_Range(m_sheet.Cells[4 + rownum, 1], m_sheet.Cells[4 + rownum, 6]);
                    AllBorders(_range.Borders);

                    object[] o = new object[6];
                    o[0] = ++rownum;
                    o[1] = nw.UKName;
                    o[2] = nw.ContractNumber;
                    o[3] = (date.HasValue ? date.Value.ToString("dd.MM.yy") : string.Empty);
                    o[4] = sum.Value;
                    o[5] = Math.Round(sum.Value * 0.00025m * 0.975m, 4);

                    _range.Value2 = o;
                    
                    _range  = _mSheet.Range[_mSheet.Cells[3 + rownum, 5], _mSheet.Cells[3 + rownum, 6]];
                    _range.NumberFormat = "### ### ### ### ### ### ###.00";
                    _range.HorizontalAlignment = XlHAlign.xlHAlignRight;
                    // _range.AutoFit();
                }

                _mSheet.Range[_mSheet.Cells[4, 5], _mSheet.Cells[4, 5]].EntireColumn.AutoFit();
                _mSheet.Range[_mSheet.Cells[4, 6], _mSheet.Cells[4, 6]].EntireColumn.AutoFit();
                _mSheet.Range["C16", "C16"].Select();
                _mApp.DisplayAlerts = true;
                _mApp.Visible = true;
            }
            catch (Exception)
            {

            }
            finally
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_mSheet);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_range);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_mApp);
            }
        }

        private void AllBorders(Borders borders)
        {
            borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
            borders.Color = Color.Black;
        }

        private List<int> _yearList;
        public List<int> YearList
        {
            get { return _yearList; }
            set { _yearList = value; OnPropertyChanged("YearList"); }
        }

        public int? _selectedYear;
        public int? SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                _selectedYear = value;
                OnPropertyChanged("SelectedYear");
                OnPropertyChanged("IsYearSelected");
            }
        }

        public GarantFondDialogViewModel()
        {
            YearList = BLServiceSystem.Client.GetNetWealthsF010YearsList((int)Document.Types.SI).ToList();
            YearList.AddRange(BLServiceSystem.Client.GetNetWealthsF015YearsList((int)Document.Types.SI).ToList());
            YearList = YearList.Distinct().OrderBy(x => x).ToList();
        }

        public void Print()
        {
            try
            {
                object filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);

                if (File.Exists(filePath.ToString()))
                {
                    RaiseStartedPrinting();
                    _mApp = new Application();
                    _mApp.DisplayAlerts = false;
                    _mApp.Visible = false;

                    var oldCI = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    try
                    {
                        var workBook = _mApp.Workbooks.Open(filePath.ToString());
                        _mSheet = (Worksheet)workBook.Sheets[1];
                        Generate();
                        JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.SELECT, "CИ");
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = oldCI;
                    }
                    RaiseFinishedPrinting();
                }
                else
                {
                    RaiseErrorLoadingTemplate(TEMPLATE_NAME);
                }
            }
            catch (Exception ex)
            {
                throw new MSOfficeException(ex.Message);
            }
        }

        public bool IsYearSelected => SelectedYear.HasValue;
    }
}
