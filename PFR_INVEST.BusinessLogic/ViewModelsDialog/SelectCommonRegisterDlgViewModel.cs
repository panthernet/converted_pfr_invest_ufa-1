﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class SelectCommonRegisterDlgViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        public AsgFinTr.Sections Section { get; private set; }
        //public AsgFinTr.Directions Direction { get; private set; }
        public string Content { get; private set; }
        public IList<CommonRegisterListItem> RegisterList { get; private set; }

        public SelectCommonRegisterDlgViewModel(AsgFinTr.Sections section, long direction, string content = null, bool forPp = false)
        {
            Section = section;
           // Direction = direction;
            Content = content;
            RegisterList = forPp ? BLServiceSystem.Client.GetCommonRegisterListForPP(Section, direction) : BLServiceSystem.Client.GetCommonRegisterList(Section, direction, Content);
        }


        private CommonRegisterListItem _SelectedItem;
        public CommonRegisterListItem SelectedItem
        {
            get { return _SelectedItem; }
            set { if (_SelectedItem != value) { _SelectedItem = value; OnPropertyChanged("SelectedItem"); } }
        }


        protected override void ExecuteSave()
        {

        }

        public override bool CanExecuteSave()
        {
            return SelectedItem != null;
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    default: return null;
                }
            }
        }
    }
}
