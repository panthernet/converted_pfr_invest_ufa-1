﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class LookupPersonGridDlgViewModel : ViewModelListDialog
    {
        private readonly List<Person> _cachedList;
		public bool OnlyActive { get; private set; }

        public LookupPersonGridDlgViewModel(bool onlyActive = true)
        {
			this.OnlyActive = onlyActive;
            _cachedList = DataContainerFacade.GetList<Person>().Where(p=> !onlyActive || !p.IsDisabled).ToList();
            _list = new List<Person>(_cachedList);
        }

        private List<Person> _list;
        public List<Person> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        private string _searchString;
        public string Search
        {
            get { return _searchString; }
            set
            {
                _searchString = value;
                ExecuteRefreshList(null);
            }

        }

        private Person _selectedPerson;

        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set
            {
                _selectedPerson = value;
                OnPropertyChanged("SelectedPerson");
                OnPropertyChanged("OkButtonEnabled");
            }
        }

        public bool OkButtonEnabled => SelectedPerson != null;

        protected override void ExecuteRefreshList(object param)
        {
            List = !string.IsNullOrEmpty(_searchString)
                       ? _cachedList.Where(item => item.LastName.ToLower().Contains(_searchString.ToLower())).ToList()
                       : _cachedList;
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
