﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class VRInvDeclDlgViewModel : InvDeclDlgViewModel
    {
        protected override List<Contract> GetContracts(List<long> idList, bool showEnded)
        {
            return BLServiceSystem.Client.GetContractsListForInvDecl(idList, Document.Types.VR, showEnded);
        }

        protected override List<SIListItem> GetSIList(bool withContacts)
        {
            return BLServiceSystem.Client.GetSIListHibForInvDecl(withContacts, false);
        }
    }
}
