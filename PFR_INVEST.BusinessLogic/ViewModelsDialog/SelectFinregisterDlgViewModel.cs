﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class SelectFinregisterDlgViewModel : ViewModelCard, IVmNoDefaultLogging
    {
		public long RegisterID { get; private set; }
		public IList<FinregisterListItem> List { get; private set; }

		public SelectFinregisterDlgViewModel(long registerID)
		{
			this.RegisterID = registerID;
            this.List = BLServiceSystem.Client.GetFinregisterForCommonPP(this.RegisterID, true);
			
		}


		private FinregisterListItem _selectedItem;
		public FinregisterListItem SelectedItem
		{ 
			get { return _selectedItem; }
			set {
			    if (_selectedItem == value) return;
			    _selectedItem = value; OnPropertyChanged("SelectedItem");
			}
		}


		protected override void ExecuteSave()
		{
			
		}

		public override bool CanExecuteSave()
		{
			return SelectedItem != null;
		}

		public override string this[string columnName]
		{
			get 
			{
				switch (columnName) 
				{
					default: return null;
				} 
			}
		}
	}
}
