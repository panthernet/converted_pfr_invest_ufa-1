﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class SelectContractDlgViewModel : ViewModelCardDialog
	{
		public Document.Types Type { get; private set; }
		public IList<Contract> List { get; private set; }

		public SelectContractDlgViewModel()
		{
			//this.Type = type;
            this.List = BLServiceSystem.Client.GetContractsForUKs();
		}


		private Contract _selectedItem;
		public Contract SelectedItem
		{
			get { return _selectedItem; }
			set {
			    if (_selectedItem == value) return;
			    _selectedItem = value; OnPropertyChanged("SelectedItem");
			}
		}

		public override bool CanExecuteSave()
		{
			return SelectedItem != null;
		}
	}
}
