﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SILettersSelectYearMonthDlgViewModel : ViewModelBase
    {
        public bool IsMonthVisible { get; set; }

        public SILettersSelectYearMonthDlgViewModel(int year, int month, bool isMonthVisible = true)
        {
            IsMonthVisible = isMonthVisible;
            SelectedMonth = month;
            SelectedYear = year;
        }

        public SILettersSelectYearMonthDlgViewModel(bool isMonthVisible = true)
        {
            IsMonthVisible = isMonthVisible;
            SelectedMonth = DateTime.Now.Month;
            SelectedYear = DateTime.Now.Year;
        }

        private int? _selectedMonth;
        public int? SelectedMonth
        {
            get
            {
                return _selectedMonth;
            }
            set
            {
                if (_selectedMonth == value) return;
                _selectedMonth = value;
                OnPropertyChanged("SelectedMonth");
            }
        }

        private int? _selectedYear;
        public int? SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                if (_selectedYear == value) return;
                _selectedYear = value;
                OnPropertyChanged("SelectedYear");
            }
        }

        public List<int?> Monthes => new List<int?> { null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

        public List<int?> Years
        {
            get
            {
                var year = DateTime.Now.Year;
                var years = new List<int?> {null};

                for (int i = -10; i < 10; i++)
                {
                    years.Add(year + i);
                }

                return years;
            }
        }

    }
}
