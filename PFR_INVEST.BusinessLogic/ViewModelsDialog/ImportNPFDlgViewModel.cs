﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using System.Globalization;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
	using PFR_INVEST.BusinessLogic.SIReport;
	using PFR_INVEST.BusinessLogic.Journal;

	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_manager)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_manager)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_manager)]
	public class ImportNPFDlgViewModel : ViewModelBase, IRequestCloseViewModel
	{
		public long RegisterId { get; set; }

        public DelegateCommand<object> OpenFileCommand { get; private set; }
		public DelegateCommand ImportCommand { get; private set; }

		public List<Tuple<Finregister, LegalEntity, bool>> Items => (ImportHandler as FinRegisterImportHandler)?.Items;

	    public ImportNPFDlgViewModel(long registerId)
		{
			RegisterId = registerId;
			OpenFileCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenFile);
			ImportCommand = new DelegateCommand(o => CanExecuteImport(), ExecuteImport);

			ImportHandler = new FinRegisterImportHandler(registerId);
		}

		private string _fileName;
		public string FileName
		{
			get { return _fileName; }
			set
			{
				_fileName = value;
				OnPropertyChanged("FileName");
			}
		}

		private NPFImportHandlerBase _importHandler;
		protected NPFImportHandlerBase ImportHandler
		{
			get { return _importHandler ?? (_importHandler = new NPFImportHandlerEmpty()); }
			set
			{ _importHandler = value; }
		}


		private bool CanExecuteOpenFile()
		{
			return ImportHandler.CanExecuteOpenFile();
		}

		private void OpenFile(object o)
		{
			string[] fileNames = DialogHelper.OpenFiles(ImportHandler.OpenFileMask);

			if (fileNames == null)
				return;

			LoadFiles(fileNames);
			OnPropertyChanged("FileName");
		}


		private void LoadFiles(string[] fileNames)
		{
			if (fileNames == null || fileNames.Length == 0)
				return;


			CultureInfo oldCI = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

			try
			{
				foreach (string fileName in fileNames)
				{
					try
					{
						this.FileName = fileName;
						string message;
					    var res = !ImportHandler.LoadFile(fileName, out message) && !string.IsNullOrEmpty(message);
						if(!string.IsNullOrEmpty(message))
							DialogHelper.ShowAlert(message);
						OnPropertyChanged("Items");
					}

					catch (Exception ex)
					{
						// DialogHelper.Alert(ex.Message);
						DialogHelper.ShowAlert("Файл содержит некорректные данные. Импорт не может быть произведен.\nДетальная информация об ошибке может быть найдена в лог файле приложения.");
						Logger.WriteException(ex);
					}
				}
			}
			finally
			{
				Thread.CurrentThread.CurrentCulture = oldCI;
			}
		}

		public bool CanExecuteImport()
		{
			return !String.IsNullOrEmpty(FileName) && ImportHandler.CanExecuteImport();
		}

		private void ExecuteImport(object o)
		{
			if (!this.CanExecuteImport())
				return;


			string message;

			if (ImportHandler.ExecuteImport(out message))
			{
				JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.IMPORT_DATA, null, "Импорт реестра СПН");
				DialogHelper.ShowAlert(string.Format("Импорт файла {0} выполнен", FileName));
				DialogHelper.ShowAlert(message);
			}
			else
			{
				DialogHelper.ShowAlert(string.Format("Импорт файла {0}\n не выполнен", FileName));
			}
			FileName = "";
			OnPropertyChanged("Items");
		}


		public event EventHandler RequestClose;

		public void CloseWindow()
		{
			if (RequestClose != null)
				RequestClose(this, null);
		}
	}
}
