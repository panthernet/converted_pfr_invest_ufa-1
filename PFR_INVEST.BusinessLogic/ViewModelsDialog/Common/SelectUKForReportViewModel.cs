﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Common
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    public class SelectUKForReportViewModel : ViewModelCardDialog
    {
        public class ContragentLookup : INotifyPropertyChanged
        {
            private bool _isSelected;
            public Contragent Contragent { get; set; }

            public bool IsSelected
            {
                get { return _isSelected; }
                set { _isSelected = value; OnPropertyChanged(nameof(IsSelected)); }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private bool _IsAllSelected;
        public bool IsAllSelected
        {
            get { return _IsAllSelected; }
            set
            {
                _IsAllSelected = value;
                foreach (var i in UKList)
                {
                    i.IsSelected = _IsAllSelected;
                }
                OnPropertyChanged(nameof(UKList));

            }
        }

        public bool IsValid { get; private set; }
        private RangeObservableCollection<ContragentLookup> _UKList;
        public RangeObservableCollection<ContragentLookup> UKList { get { return _UKList ?? (_UKList = new RangeObservableCollection<ContragentLookup>()); } set { _UKList = value; } }

        public SelectUKForReportViewModel() : base(true)
        {
            Refresh();
        }

        public override void Refresh()
        {
            var _legals = DataContainerFacade.GetList<LegalEntity>();
            var _contracts = DataContainerFacade.GetList<Contract>().Where(x => x.TypeID == 1);
            var siUK = _legals.Join(_contracts, l => l.ID, c => c.LegalEntityID, (l, c) => l.ContragentID).Distinct();

            UKList.Fill(DataContainerFacade.GetList<Contragent>()
            .Where(d => d.StatusID == 1 && siUK.Contains(d.ID))
            .OrderBy(x => x.Name)
            .Select(x => new ContragentLookup() { Contragent = x }));
            UKList.ForEach(f => f.PropertyChanged += FOnPropertyChanged);
        }

        private void FOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "IsSelected")
                CanExecuteSave();
        }

        public override bool CanExecuteSave()
        {
            IsValid = UKList.Any(x => x.IsSelected);
            OnPropertyChanged(nameof(IsValid));
            return IsValid;
        }

        protected override void ExecuteSave()
        {
            ;

        }

        public override string this[string fieldName] => null;
    }
}
