﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Common
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    public class SelectPeriodAndBoolFlagViewModel : SelectDateAndBoolFlagViewModel
    {
       
        DateTime? _DateValue2;

        public DateTime? DateValue2
        {
            get { return _DateValue2; }

            set
            {
                if (SetPropertyValue(nameof(DateValue2), ref _DateValue2, value))
                    OnPropertyChanged(nameof(IsValid));
            }
        }

        public new bool IsValid => !AllowSelectDate || (AllowSelectDate && DateValue.HasValue && DateValue2.HasValue);

        public SelectPeriodAndBoolFlagViewModel(bool allowSelectDate = false, bool allowSelectFlag = true, string flagLabel = "Отображать старые наименования", string dateLabel = "Период")
            : base(allowSelectDate, allowSelectFlag, flagLabel, dateLabel)
        {
            if (allowSelectDate)
            {

                DateValue2 = DateTime.Now.Date;

            }

        }
    }
}
