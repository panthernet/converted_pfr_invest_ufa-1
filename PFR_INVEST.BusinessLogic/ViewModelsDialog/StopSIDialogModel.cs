﻿using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using System;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
	public class StopSIDialogModel : ViewModelCardDialog
    {
        public enum StopType
        {
            StopActivities,  //Прекращение деятельности
            StopСontract     //Расторжение договора
        }

        private readonly LegalEntity m_LegalEntity;
        private readonly Contract m_Contract;
        private readonly StopType m_Variant;

        private new readonly long ID;

        public DateTime? CloseDate
        {
            get {
                return m_Variant == StopType.StopActivities ? m_LegalEntity.CloseDate : m_Contract.DissolutionDate;
            }
            set
            {
                if (m_Variant == StopType.StopActivities)
                    m_LegalEntity.CloseDate = value;
                else m_Contract.DissolutionDate = value;
                OnPropertyChanged("CloseDate");
               // OnPropertyChanged("IsValid");
            }
        }

        public string Reason
        {
            get {
                return m_Variant == StopType.StopActivities ? m_LegalEntity.PDReason : m_Contract.DissolutionBase;
            }
            set
            {
                if (m_Variant == StopType.StopActivities)
                    m_LegalEntity.PDReason = value;
                else m_Contract.DissolutionBase = value;
                OnPropertyChanged("Reason");
            }
        }

        public StopSIDialogModel(long id, StopType var)
            :base(true)
        {
            ID = id;
            m_Variant = var;

            if (m_Variant == StopType.StopActivities)
                m_LegalEntity = DataContainerFacade.GetByID<LegalEntity, long>(ID);
            else m_Contract = DataContainerFacade.GetByID<Contract, long>(ID);
        }
        
        public override bool CanExecuteSave()
        {
            return base.ValidateFields();
            //OnPropertyChanged("IsValid");
            //return IsValid;
        }

        protected override void ExecuteSave()
        {
            if (m_Variant == StopType.StopActivities)
                DataContainerFacade.Save<LegalEntity, long>(m_LegalEntity);
            else DataContainerFacade.Save<Contract, long>(m_Contract);
            //логирование вроде бы не требуется, прекращение деятельности логируется в другом место после вызова метода Save()
        }

        public override string this[string fieldName]
        {
            get
            {                             
                switch (fieldName)
                {
					case "CloseDate": return CloseDate.ValidateRequired();
					case "Reason": return Reason.ValidateMaxLength(m_Variant == StopType.StopActivities ? 1024 : 256);
                }
                return null;
            }
        }

		//public bool IsValid { get; private set; }
    }
}
