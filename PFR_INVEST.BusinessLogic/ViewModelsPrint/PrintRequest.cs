﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class PrintRequest : ViewModelBase
    {
        private const string WORD_TEMPLATE = "Требование.doc";
        private const string WORD_TEMPLATE_DSV = "Требование ДСВ.doc";

        private readonly bool _mIsDSV;

        private readonly long _mRegisterID;
        private readonly AddressType _mAddressType;
        private readonly string _mFolderToSave;
        private readonly bool _mBMass;

        private _Application _mWApp;
        private _Application WordApp => _mWApp ?? (_mWApp = new Application());

	    public PrintRequest(SPNOperation operation, AddressType pAddressType, bool isVr, int year, int month)
        {
            _year = year;
            _month = month;
            _isVr = isVr;
            _mIsDSV = operation.Name.Contains(PortfolioIdentifier.DSV);
            _mAddressType = pAddressType;
            _mBMass = false;
        }

        private long _operationId;
        private readonly bool _isVr;

        private readonly int _year;
        private readonly int _month;

        public PrintRequest(long pRegisterID, AddressType pAddressType, string pFolderToSave, bool isVr, int year, int month)
        {
            _year = year;
            _month = month;
            _mRegisterID = pRegisterID;
            _isVr = isVr;
            var register = DataContainerFacade.GetByID<SIRegister, long>(_mRegisterID);
            var op = register.GetOperation();
            _operationId= isVr ? -1 : op.ID;
            _mIsDSV = op.Name.Contains(PortfolioIdentifier.DSV);

            _mFolderToSave = pFolderToSave;
            _mAddressType = pAddressType;
            _mBMass = true;
        }

        private void GenerateDoc(ReqTransfer transfer, Contract contract)
        {
            var le = contract.GetLegalEntity();

            string address;
            switch (_mAddressType)
            {
                case AddressType.Post:
                    address = le == null ? string.Empty : le.PostAddress;
                    break;
                case AddressType.Legal:
                    address = le == null ? string.Empty : le.LegalAddress;
                    break;
                case AddressType.Fact:
                    address = le == null ? string.Empty : le.StreetAddress;
                    break;
                default: return;
            }

          /*  switch (_operationId)
            {
                case (long)SIRegister.Operations.WithdrawNCTP:
                case (long)SIRegister.Operations.WithdrawSPV:
                case (long)SIRegister.Operations.WithdrawEV:
                case (long)SIRegister.Operations.DeadZL:
                case (long)SIRegister.Operations.SPNRedistributionMSK:
                    OfficeTools.FindAndReplace(WordApp, "LEGALENTITY.FULLNAME", le != null ? le.FullName : string.Empty);
                    OfficeTools.FindAndReplace(WordApp, "LEGALENTITY.SHORTNAME", le != null ? le.ShortName : string.Empty);
                    OfficeTools.FindAndReplace(WordApp, "LEGALENTITY.LETTERWHO", le?.LetterWho.Replace("\n", string.Empty) ?? string.Empty);
                    OfficeTools.FindAndReplace(WordApp, "LEGALENTITY.POSTADDRESS", le != null ? le.PostAddress : string.Empty);
                    OfficeTools.FindAndReplace(WordApp, "LEGALENTITY.LEGALADDRESS", le != null ? le.PostAddress : string.Empty);

                    switch (le != null ? le.Sex ?? -1 : -1)
                    {
                        case 0:
                            OfficeTools.FindAndReplace(WordApp, "CALCULATED.TITLE", "Уважаемый");
                            break;
                        case 1:
                            OfficeTools.FindAndReplace(WordApp, "CALCULATED.TITLE", "Уважаемая");
                            break;
                        default:
                            OfficeTools.FindAndReplace(WordApp, "CALCULATED.TITLE", "Уважаемый(ая)");
                            break;
                    }
                    var headFullName = le != null ? le.HeadFullName : string.Empty;
                    OfficeTools.FindAndReplace(WordApp, "LEGALENTITY.HEADNAME", !string.IsNullOrWhiteSpace(headFullName) ? headFullName.Substring(headFullName.IndexOf(' ') + 1) : string.Empty);
                    OfficeTools.FindAndReplace(WordApp, "CONTRACT.REGNUM", contract != null ? contract.ContractNumber : string.Empty);
                    OfficeTools.FindAndReplace(WordApp, "CONTRACT.REGDATE", contract?.ContractDate?.ToString("dd.MM.yyyy", new CultureInfo("ru-Ru")) ?? string.Empty);

                    break;
            }*/

            var sum = Math.Floor(transfer.Sum.GetValueOrDefault());
            var sumFrac = (transfer.Sum.GetValueOrDefault(0) - sum) * 100;

            var investmentIncome = Math.Floor(transfer.InvestmentIncome.GetValueOrDefault(0));
            var investmentIncomeFrac = (transfer.InvestmentIncome.GetValueOrDefault(0) - investmentIncome) * 100;

            OfficeTools.FindAndReplace(WordApp, "{LENAME}", le?.FullName);
            OfficeTools.FindAndReplace(WordApp, "{ADDRESS}", address);

            OfficeTools.FindAndReplace(WordApp, "{CONTRACTDATE}", contract.ContractDate?.ToString("dd.MM.yy"));
            OfficeTools.FindAndReplace(WordApp, "{CONTRACTNUMBER}", contract.ContractNumber);

            OfficeTools.FindAndReplace(WordApp, "{SUM}", sum);
            OfficeTools.FindAndReplace(WordApp, "{SUMFRAC}", sumFrac.ToString("00"));
            OfficeTools.FindAndReplace(WordApp, "{INWORDS}", Сумма.Пропись(transfer.Sum ?? 0, Валюта.Рубли, Заглавные.Первая));

            OfficeTools.FindAndReplace(WordApp, "{INVINCOME}", investmentIncome);
            OfficeTools.FindAndReplace(WordApp, "{INVINCOMEFRAC}", investmentIncomeFrac.ToString("00"));
        }

        public bool Print(ReqTransfer transfer, Contract contract = null)
        {
            var tlist = transfer.GetTransferList();
            contract = contract ?? tlist.GetContract();
            if (_operationId == 0 && !_isVr)
                _operationId = tlist.GetTransferRegister()?.OperationID ?? 0;
            string wordTemplateName = "";
            string name = "";
            switch (_operationId)
            {
                case (long)SIRegister.Operations.WithdrawNCTP:
                    name = "Требование о перечислении СПН (Накопительная)";
                    break;
                case (long)SIRegister.Operations.WithdrawSPV:
                    name = "Требование о перечислении СПН (Срочная)";
                    break;
                case (long)SIRegister.Operations.WithdrawEV:
                    name = "Требование о перечислении СПН (Единовременная)";
                    break;
                case (long)SIRegister.Operations.DeadZL:
                    name = "Требование о перечислении СПН (Правопреемники)";
                    break;
                case (long)SIRegister.Operations.SPNRedistributionMSK:
                    name = "Требование о перечислении СПН (МСК)";
                    break;
                case (long)SIRegister.Operations.SPNRedistribution:
                    name = "Требование о перечислении СПН (Переток)";
                    break;
                default:
                    wordTemplateName = _mIsDSV ? WORD_TEMPLATE_DSV : WORD_TEMPLATE;
                    break;
            }
            if (!string.IsNullOrEmpty(name))
            {
                new PrintSILetters().PrintReqTransfer(transfer, $"{name}.doc", name, _mFolderToSave, $"{name} {contract.GetLegalEntity().FormalizedName} {contract.ContractNumber}.doc", _year, _month, _mAddressType);
                return true;
            }
            

            var extractedFilePath = TemplatesManager.ExtractTemplate(wordTemplateName);
            if (!File.Exists(extractedFilePath))
            {
                RaiseErrorLoadingTemplate(wordTemplateName);
                return false;
            }

            if (_mBMass)
                try
                {
                    //Требование  <компания> <№ договора> ID.doc
                    var toFile = Path.Combine(_mFolderToSave ?? "",
                        $"Требование {contract.GetLegalEntity().FormalizedName} {contract.ContractNumber} {transfer.ID}.doc");
                    if (File.Exists(toFile))
                        File.Delete(toFile);
                    File.Move(extractedFilePath, toFile);
                    extractedFilePath = Path.GetFullPath(toFile);
                }
                catch (Exception e)
                {
                    Logger.WriteException(e);
                    return false;
                }

            if (File.Exists(extractedFilePath))
            {
                var filePath = (object) extractedFilePath;

                WordApp.Visible = false;
                WordApp.Documents.Open(ref filePath);

                GenerateDoc(transfer, contract);

                if (!_mBMass)
                    WordApp.Visible = true;
                else
                {
                    WordApp.Quit(WdSaveOptions.wdSaveChanges, WdOriginalFormat.wdWordDocument);
                    _mWApp = null;
                }
                return true;
            }
            RaiseErrorLoadingTemplate(wordTemplateName);
            return false;
        }

        public bool PrintMass()
        {
            if (!_mBMass)
                return false;

            try
            {
                if (!Directory.Exists(_mFolderToSave))
                    Directory.CreateDirectory(_mFolderToSave);
            }
            catch (Exception e)
            {
                Logger.WriteException(e);
                return false;
            }

            var reqTransfers = BLServiceSystem.Client.GetTransfersForSIRegisterHib(_mRegisterID)
                            .Where(tr => TransferStatusIdentifier.IsStatusInitialState(tr.TransferStatus)).ToList();
            if (!reqTransfers.Any())
                return false;

            foreach (var transfer in reqTransfers)
            {
                if (!Print(transfer))
                    return false;
                transfer.TransferStatus = TransferStatusIdentifier.sDemandGenerated;
                transfer.RequestCreationDate = DateTime.Today;
                DataContainerFacade.Save<ReqTransfer, long>(transfer);
            }

            return true;
        }
    }
}
