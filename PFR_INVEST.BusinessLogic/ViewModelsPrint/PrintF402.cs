﻿using System;
using System.Globalization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class PrintF402 : PrintToDoc
    {
        private readonly long _f402Id;
        private readonly AddressType _addressType;

        public PrintF402(long f402Id, AddressType addressType)
        {
            TemplateName = "Поручение на оплату.doc";
            Name = "Поручение на оплату";
            _f402Id = f402Id;
            _addressType = addressType;
        }

        protected override void FillData()
        {
            var detail = DataContainerFacade.GetByID<F401402UK, long>(_f402Id);
            var document = DataContainerFacade.GetByID<EdoOdkF401402, long>(detail.EdoID);
            var contract = detail.GetContract();
            var legalEntity = contract.GetLegalEntity();
            var contractSD = BLServiceSystem.Client.GetSDContractOnDate(document.FromDate.Value);

            Fields.Add("LEGALENTITY.FULLNAME", legalEntity.FullName.ConvertToWordLineBreaks());
            Fields.Add("LEGALENTITY.FORLETTER", legalEntity.ForLetter.ConvertToWordLineBreaks());
            Fields.Add("LEGALENTITY.LETTERWHO", legalEntity.LetterWho.ConvertToWordLineBreaks());
            switch (_addressType)
            {
                case AddressType.Fact:
                    Fields.Add("ADDRESS", legalEntity.StreetAddress.ConvertToWordLineBreaks());
                    break;
                case AddressType.Legal:
                    Fields.Add("ADDRESS", legalEntity.LegalAddress.ConvertToWordLineBreaks());
                    break;
                case AddressType.Post:
                    Fields.Add("ADDRESS", legalEntity.PostAddress.ConvertToWordLineBreaks());
                    break;
            }
            Fields.Add("LEGALENTITY.LEGALADDRESS", legalEntity.StreetAddress.ConvertToWordLineBreaks());
            Fields.Add("LEGALENTITY.STREETADDRESS", legalEntity.LegalAddress.ConvertToWordLineBreaks());
            Fields.Add("LEGALENTITY.POSTADDRESS", legalEntity.PostAddress.ConvertToWordLineBreaks());
            switch (legalEntity.Sex)
            {
                case 0:
                    Fields.Add("TITLE", "Уважаемый");
                    break;
                case 1:
                    Fields.Add("TITLE", "Уважаемая");
                    break;
                default:
                    Fields.Add("TITLE", "Уважаемый(ая)");
                    break;
            }

            var headFullName = legalEntity.HeadFullName.ConvertToWordLineBreaks();
            Fields.Add("LEGALENTITY.HEADFULLNAME", headFullName.Substring(headFullName.IndexOf(' ') + 1));

            Fields.Add("LEGALENTITY.SHORTNAME", legalEntity.ShortName.Trim().ConvertToWordLineBreaks());

            if (contractSD != null)
            {
                Fields.Add("F402_UK.DOGSDNUM", contractSD.ContractNumber);
                Fields.Add("F402_UK.DOGSDDATE", contractSD.ContractDate?.ToString("dd.MM.yyyy", new CultureInfo("ru-Ru")));
            }
            else
            {
                Fields.Add("F402_UK.DOGSDNUM", string.Empty);
                Fields.Add("F402_UK.DOGSDDATE", string.Empty);
            }
            Fields.Add("CONTRACT.REGNUM", contract.ContractNumber);
            Fields.Add("CONTRACT.REGDATE", contract.ContractDate?.ToString("dd.MM.yyyy", new CultureInfo("ru-Ru")));

            Fields.Add("DOCUMENT.FROMDATE", document.FromDate.Value.ToString("MMMM yyyy", new CultureInfo("ru-Ru")).ToLower());

            var itogo = detail.Itogo ?? 0;
            var rub = Math.Floor(itogo);
            var kop = (itogo - rub) * 100;

            var culture = CultureInfo.CreateSpecificCulture("ru-RU");
            Fields.Add("F402_UK.ITOGO.RUB", rub.ToString("# ### ### ### ### ### ###", culture).Trim());
            Fields.Add("F402_UK.ITOGO.KOP", kop.ToString("00"));
            Fields.Add("F402_UK.ITOGO.STR", Сумма.Пропись(itogo, Валюта.Рубли, Заглавные.Первая));
        }
    }

    public enum AddressType
    {
        Post,
        Fact,
        Legal
    }
}
