﻿using System;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.DataObjects;
using Document = Microsoft.Office.Interop.Word.Document;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using System.IO;
using System.Globalization;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    /// <summary>
    /// Справка об отсутствии акцепта
    /// </summary>
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintNotAcceptedDepClaimOffer : PrintToDoc
    {
        //private string DateFormat = "dd.MM.yyyy";
        //private string TimeFormat = "hh\\:mm";
        public DepClaimOfferViewModel Offer { get; private set; }
        public Person ChiefPerson { get; private set; }
        public Person ExecuterPerson { get; private set; }
        public Person ControllerPerson { get; private set; }

        public string Folder { get; set; }
        public bool KeepOpen { get; set; }

        public PrintNotAcceptedDepClaimOffer(long offerID, Person chiefPerson, Person executerPerson, Person controllerPerson)
        {
            this.KeepOpen = true;
            this.Folder = Path.GetTempPath();
            this.Offer = new DepClaimOfferViewModel(offerID, ViewModelState.Read);
            this.ChiefPerson = chiefPerson;
            this.ExecuterPerson = executerPerson;
            this.ControllerPerson = controllerPerson;


            this.Name = string.Format("Справка об отсутствии акцепта №{0} ({1})", this.Offer.OfferNumber, this.Offer.BankName);
            this.TemplateName = "Справка об отсутствии акцепта.doc";

        }

        protected override void FillData()
        {
            CultureInfo ci = new CultureInfo("ru");
            DateTimeFormatInfo dtfi = ci.DateTimeFormat;

            Fields.Add("DepositDay", this.Offer.DepClaim.SettleDate.Day.ToString());
            Fields.Add("DepositMonth", dtfi.MonthGenitiveNames[this.Offer.DepClaim.SettleDate.Month - 1]);
            Fields.Add("DepositYear", this.Offer.DepClaim.SettleDate.ToString("yyyy"));
            Fields.Add("AuctionDay", this.Offer.Auction.SelectDate.Day.ToString());
            Fields.Add("AuctionMonth", dtfi.MonthGenitiveNames[this.Offer.Auction.SelectDate.Month - 1]);
            Fields.Add("AuctionYear", this.Offer.Auction.SelectDate.ToString("yyyy"));
            Fields.Add("BankName", this.Offer.Bank.FormalizedNameFull);
            Fields.Add("OfferNumber", this.Offer.Offer.Number.ToString());

            var max = Math.Max(this.ChiefPerson.FIOShort.Length, this.ExecuterPerson.FIOShort.Length);
            max = Math.Max(max, this.ControllerPerson.FIOShort.Length);
            max = Math.Max(max, 25);

            Fields.Add("ChiefFIO", this.ChiefPerson.FIOShort.PadRight(max, ' '));
            Fields.Add("ExecuterFIO", this.ExecuterPerson.FIOShort.PadRight(max, ' '));
            Fields.Add("ControllerFIO", this.ControllerPerson.FIOShort.PadRight(max, ' '));
        }

        protected override void GenerateDocument(Application app, Document doc)
        {

            base.GenerateDocument(app, doc);
            //object missing = Missing.Value;
            //doc.SaveAs(this.Folder +"\\" + this.Name + ".doc", 
            //    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, 
            //    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

            //if(!this.KeepOpen)
            //    app.Quit(ref missing, ref missing, ref missing);
            //doc.Close(missing, missing, missing);
        }




    }
}
