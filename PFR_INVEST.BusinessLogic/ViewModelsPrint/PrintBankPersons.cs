﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using Application = Microsoft.Office.Interop.Word.Application;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintBankPersons : ViewModelBase
    {
        const int WEEK = 7;
        const int TWO_WEEKS = 14;

        private Application _wApp;
        private object _missing = System.Reflection.Missing.Value;
        private object _isVisible = true;
        private readonly PrintBankPersonsDlgViewModel.BankPersonsExportType _filter;

        //private readonly Dictionary<string, DBField> order;
        private Microsoft.Office.Interop.Word.Document _wDoc;

        public decimal FixedPercent { get; private set; }
        private readonly long _selectedStock;
        private readonly bool _isWithoutCertificates;
        private readonly bool _onlyValidBanks;
        //public PrintBankPersons() : this(true) { }

        public PrintBankPersons(PrintBankPersonsDlgViewModel.BankPersonsExportType exportType, long selectedStock, bool isWithoutCertificates, bool onlyValidBanks)
        {
            _onlyValidBanks = onlyValidBanks;
            _filter = exportType;
            _selectedStock = selectedStock == 0 ? 0 : (106000 + selectedStock);
            _isWithoutCertificates = isWithoutCertificates;
        }

        /// <summary>
        /// Имя шаблона word
        /// </summary>
        public string GetTemplateName()
        {
            return "Уполномоченные лица банков.doc";
        }

        /// <summary>
        /// заполнение документа
        /// </summary>
        public void GenerateDoc(string fName)
        {
            var dt = DateTime.Now.Date;
            var replaces = new Dictionary<string, string> {{"{CURRENTDATE}", dt.ToShortDateString()}};


            foreach (var key in replaces.Keys)
                OfficeTools.FindAndReplace(_wApp, key, replaces[key]);

            var cbTable = _wDoc.Tables[1];


            var ids = BLServiceSystem.Client.GetCommonBankListItems().Where(a => BankCalculator.IsActiveStatus(a.PFRAGRSTAT)).OrderBy(a=> a.ShortName).Select(a => a.ID);

            //дотсаем все сертификаты и доверенности
            var certificates = _isWithoutCertificates ? new List<LegalEntityCourierCertificate>() : DataContainerFacade.GetList<LegalEntityCourierCertificate>();
            var letters = DataContainerFacade.GetList<LegalEntityCourierLetterOfAttorney>();
            //достаем все уп. лица
            var persons = DataContainerFacade.GetList<LegalEntityCourier>().Where(x => x.Type == LegalEntityCourier.Types.Person && !x.IsDeleted).ToList();            
            
            //фильтруем тип экспорта
            if (_filter ==  PrintBankPersonsDlgViewModel.BankPersonsExportType.Expiring)
            {
                certificates = certificates.Where(c => c.StatusID != -1 && c.ExpireDate.HasValue && (c.ExpireDate.Value - dt).Days <= TWO_WEEKS && (c.ExpireDate.Value - dt).Days >= 0).ToList();
                letters = letters.Where(l => l.StatusID != -1 && l.ExpireDate.HasValue && (l.ExpireDate.Value - dt).Days <= TWO_WEEKS && (l.ExpireDate.Value - dt).Days >= 0).ToList();
            }else if (_filter == PrintBankPersonsDlgViewModel.BankPersonsExportType.Active)
            {
                certificates = certificates.Where(c => c.StatusID != -1 && (!c.ExpireDate.HasValue || (c.ExpireDate.Value - dt).Days > 0)).ToList();
                letters = letters.Where(l => l.StatusID != -1 && (!l.ExpireDate.HasValue || (l.ExpireDate.Value - dt).Days > 0)).ToList();    
            }
            //фильтруем биржу если требуется
            if (_selectedStock != 0)
            {
                certificates = certificates.Where(a => a.StockID == _selectedStock).ToList();
                letters = letters.Where(a => a.StockID == _selectedStock).ToList();
            }

            //получаем все идентификаторы уп.лиц использующиеся в доверенностях и сертификатах
            var personIds = _filter == PrintBankPersonsDlgViewModel.BankPersonsExportType.All ?
                persons.Select(a=> a.ID) :
                certificates.Select(x => x.LegalEntityCourierID).Union(letters.Select(l => l.LegalEntityCourierID)).Distinct();
            //фильтруем и сортируем список уп.лиц
            var sortedPersonsList = persons.Where(a => personIds.Contains(a.ID)).OrderBy(a => a.LegalEntityName).ThenBy(a => string.Format(@"{1} {0} {2}", a.FirstNameTrim, a.LastNameTrim, a.PatronymicNameTrim))
                .ToList();

            //фильтруем по банкам
            if (_onlyValidBanks)
            {
                sortedPersonsList = persons.Where(a => ids.Contains(a.LegalEntityID)).OrderBy(a => a.LegalEntityName).ThenBy(a => string.Format(@"{1} {0} {2}", a.FirstNameTrim, a.LastNameTrim, a.PatronymicNameTrim)).ToList();
            }

            int rowCount = 0;
            foreach (var person in sortedPersonsList)
            {
                var pId = person.ID;
                //сортируем доверенности по дате истечения
                var cLetters = letters.Where(l => l.LegalEntityCourierID == pId).OrderBy(l => l.ExpireDate);
                //сортируем серификаты по дате истечения
                var cCerts = certificates.Where(c => c.LegalEntityCourierID == pId).OrderBy(c => c.ExpireDate).ToList();

                //если режим НЕ ВСЕ, отсеиваем лишние
                if (_filter != PrintBankPersonsDlgViewModel.BankPersonsExportType.All)
                {
                    if (!cLetters.Any()) continue;

                    var toRemove = (from c in cCerts let @let = cLetters.FirstOrDefault(a => a.StockID == c.StockID) where @let == null select c).ToList();
                    toRemove.ForEach(a => cCerts.Remove(a));
                }
                else
                {
                    //если режим ВСЕ и нет сертификатов и доверенностей - выводим просто ФИО и банк
                    if (!cLetters.Any() && !cCerts.Any())
                    {
                        var row = cbTable.Rows.Add(ref _missing);
                        rowCount++;
                        row.Cells[1].Range.Text = person.LegalEntityName;
                        row.Cells[2].Range.Text = string.Format(@"{1} {0} {2}", person.FirstNameTrim, person.LastNameTrim, person.PatronymicNameTrim);
                        continue;
                    }
                }
                //записываем доверенности
                foreach (var l in cLetters)
                {
                    var row = cbTable.Rows.Add(ref _missing);
                    rowCount++;
                    row.Cells[1].Range.Text = person.LegalEntityName;
                    row.Cells[2].Range.Text = string.Format(@"{1} {0} {2}", person.FirstNameTrim, person.LastNameTrim, person.PatronymicNameTrim);
                    row.Cells[3].Range.Text = l.Number;
                    row.Cells[4].Range.Text = l.IssueDate?.ToShortDateString() ?? string.Empty;

                    if (l.ExpireDate.HasValue)
                    {
                        row.Cells[5].Range.Text = l.ExpireDate.Value.ToShortDateString();
                        var dd = (l.ExpireDate.Value.Date - dt).Days;
                        row.Cells[5].Range.HighlightColorIndex = WdColorIndex.wdAuto;
                        if (dd < 0)
                        {
                            row.Cells[5].Range.HighlightColorIndex = WdColorIndex.wdDarkRed;
                        }
                        else
                            if (dd <= TWO_WEEKS)
                            {
                                if (dd >= WEEK)
                                {
                                    row.Cells[5].Range.HighlightColorIndex = WdColorIndex.wdYellow;
                                }
                                else if (dd >= 0)
                                {
                                    row.Cells[5].Range.HighlightColorIndex = WdColorIndex.wdRed;
                                }
                            }
                    }
                    if (!string.IsNullOrWhiteSpace(l.Number) || l.IssueDate.HasValue || l.ExpireDate.HasValue)
                        row.Cells[6].Range.Text = l.StockName;

                    //записываем в эту же строчку сертификат, если есть
                    var cert = cCerts.FirstOrDefault(a=> a.StockID == l.StockID);
                    if (cert != null)
                    {
                        SetCertData(row, false, person, cert, dt);
                        cCerts.Remove(cert);
                    }
                }
                //записываем сертификаты
                foreach (var c in cCerts)
                {
                    var row = cbTable.Rows.Add(ref _missing);
                    rowCount++;
                    SetCertData(row, true, person, c, dt);
                }
            }

            #region Извращенское слияние ячеек по вертикали из-за убогости интеропа с Вордом
            int? col1StartIndex = null;
            string col1Text = null;
            var dic1 = new Dictionary<int, int>();

            rowCount++;
            //получаем диапазоны для слияния по 1 колонке
            for (int i = 1; i <= rowCount; i++)
            {
                if (col1Text != cbTable.Cell(i, 1).Range.Text && i != 1 || i == rowCount)
                {
                    //если последняя запись, добавляем еще+1 для верного счета
                    if (i == rowCount && col1Text == cbTable.Cell(i, 1).Range.Text)
                        i++;
                    //если 2 и более одинаковых строк - запоминаем
                    if (col1StartIndex != i - 1 && col1StartIndex != null)
                        dic1.Add(col1StartIndex.Value, i - 1);
                    col1StartIndex = null;
                    col1Text = null;
                }

                if (!col1StartIndex.HasValue)
                {
                    //начинаем КВН
                    col1StartIndex = i;
                    col1Text = cbTable.Cell(i, 1).Range.Text;
                }

            }

            int? col2StartIndex = null;
            string col2Text = null;
            var dic2 = new Dictionary<int, int>();
            //получаем диапазоны для слияния по 2 колонке
            for (int i = 1; i <= rowCount; i++)
            {
                if (col2Text != cbTable.Cell(i, 2).Range.Text && i != 1 || i == rowCount)
                {
                    //если последняя запись, добавляем еще+1 для верного счета
                    if (i == rowCount && col2Text == cbTable.Cell(i, 2).Range.Text)
                        i++;
                    //если 2 и более одинаковых строк - запоминаем
                    if (col2StartIndex != i - 1 && col2StartIndex != null)
                        dic2.Add(col2StartIndex.Value, i - 1);
                    col2StartIndex = null;
                    col2Text = null;
                }

                if (!col2StartIndex.HasValue)
                {
                    //начинаем КВН
                    col2StartIndex = i;
                    col2Text = cbTable.Cell(i, 2).Range.Text;
                }

            }

            //проводим слияние по 1 колонке
            dic1.ForEach(a =>
            {
                for (int i = a.Key+1; i <= a.Value; i++)
                    cbTable.Cell(i, 1).Range.Text = null;
                cbTable.Cell(a.Key, 1).Merge(cbTable.Cell(a.Value, 1));
            });

            //проводим слияние по 2 колонке
            dic2.ForEach(a =>
            {
                for (int i = a.Key + 1; i <= a.Value; i++)
                    cbTable.Cell(i, 2).Range.Text = null;
                cbTable.Cell(a.Key, 2).Merge(cbTable.Cell(a.Value, 2));
            });
            #endregion 
            
        }

        private void SetCertData(Row row, bool insertName, LegalEntityCourier person, LegalEntityCourierCertificate c, DateTime dt)
        {
            if (insertName)
            {
                row.Cells[1].Range.Text = person.LegalEntityName;
                row.Cells[2].Range.Text = string.Format(@"{1} {0} {2}", person.FirstNameTrim, person.LastNameTrim, person.PatronymicNameTrim);
            }

            if (c.ExpireDate.HasValue)
            {
                row.Cells[7].Range.Text = c.ExpireDate.Value.ToShortDateString();
                var dd = (c.ExpireDate.Value.Date - dt).Days;
                row.Cells[7].Range.HighlightColorIndex = WdColorIndex.wdAuto;
                if (dd < 0)
                {
                    row.Cells[7].Range.HighlightColorIndex = WdColorIndex.wdDarkRed;
                }
                else
                    if (dd <= TWO_WEEKS)
                    {
                        if (dd >= WEEK)
                        {
                            row.Cells[7].Range.HighlightColorIndex = WdColorIndex.wdYellow;
                        }
                        else if (dd >= 0)
                        {
                            row.Cells[7].Range.HighlightColorIndex = WdColorIndex.wdRed;
                        }
                    }
            }
            if ((!string.IsNullOrWhiteSpace(c.Signature) || insertName || c.ExpireDate.HasValue) && (row.Cells[6].Range.Text == null || string.IsNullOrEmpty(row.Cells[6].Range.Text.Trim('\r','\a'))))
                row.Cells[6].Range.Text = c.StockName;
        }

     /*   public void GenerateDocOld(string fName)
        {
            var dt = DateTime.Now.Date;
            var replaces = new Dictionary<string, string> {{"{CURRENTDATE}", dt.ToShortDateString()}};


            foreach (var key in replaces.Keys)
                OfficeTools.FindAndReplace(_wApp, key, replaces[key]);

            var cbTable = _wDoc.Tables[1];

            var xL = DataContainerFacade.GetList<LegalEntityCourier>();

            var persons = xL.Where(x => x.Type == LegalEntityCourier.Types.Person && !x.IsDeleted).ToList();
            if (_filter ==PrintBankPersonsDlgViewModel.BankPersonsExportType.Expiring)
            {
                var toDate = dt.AddDays(TWO_WEEKS);
                persons = persons.Where(x =>
                    (x.LetterOfAttorneyExpireDate.HasValue && (x.LetterOfAttorneyExpireDate.Value - dt).Days <= TWO_WEEKS) ||
                    (x.SignatureExpireDate.HasValue && (x.SignatureExpireDate.Value - dt).Days <= TWO_WEEKS)
                    ).ToList();
            }

            foreach (var p in persons)
            {
                var row = cbTable.Rows.Add(ref _missing);
                row.Cells[1].Range.Text = p.LegalEntityName;
                row.Cells[2].Range.Text = p.FirstName;
                row.Cells[3].Range.Text = p.LastName;
                row.Cells[4].Range.Text = p.LetterOfAttorneyNumber;
                row.Cells[5].Range.Text = p.LetterOfAttorneyIssueDate.HasValue ? p.LetterOfAttorneyIssueDate.Value.ToShortDateString() : string.Empty;

                if (p.LetterOfAttorneyExpireDate.HasValue)
                {
                    row.Cells[6].Range.Text = p.LetterOfAttorneyExpireDate.Value.ToShortDateString();
                    var dd = (p.LetterOfAttorneyExpireDate.Value.Date - dt).Days;
                    row.Cells[6].Range.HighlightColorIndex = WdColorIndex.wdAuto;
                    if (dd < 0)
                    {
                        row.Cells[6].Range.HighlightColorIndex = WdColorIndex.wdDarkRed;
                    }
                    else
                        if (dd <= TWO_WEEKS)
                        {
                            if (dd >= WEEK)
                            {
                                row.Cells[6].Range.HighlightColorIndex = WdColorIndex.wdYellow;
                            }
                            else if (dd >= 0)
                            {
                                row.Cells[6].Range.HighlightColorIndex = WdColorIndex.wdRed;
                            }
                        }
                }

                row.Cells[7].Range.Text = p.Signature;
                if (p.SignatureExpireDate.HasValue)
                {
                    row.Cells[8].Range.Text = p.SignatureExpireDate.Value.ToShortDateString();
                    var dd = (p.SignatureExpireDate.Value.Date - dt).Days;
                    row.Cells[8].Range.HighlightColorIndex = WdColorIndex.wdAuto;
                    if (dd < 0)
                    {
                        row.Cells[8].Range.HighlightColorIndex = WdColorIndex.wdDarkRed;
                    }
                    else
                        if (dd <= TWO_WEEKS)
                        {
                            if (dd >= WEEK)
                            {
                                row.Cells[8].Range.HighlightColorIndex = WdColorIndex.wdYellow;
                            }
                            else if (dd >= 0)
                            {
                                row.Cells[8].Range.HighlightColorIndex = WdColorIndex.wdRed;
                            }
                        }
                }
            }
        }
        */
        /// <summary>
        /// вызов на печать
        /// </summary>
        public void Print()
        {
            var wordTemplateName = GetTemplateName();
            object filePath = TemplatesManager.ExtractTemplate(wordTemplateName);

            if (File.Exists(filePath.ToString()))
            {
                try
                {
                    _wApp = new Application();
                    _wDoc = _wApp.Documents.Add(ref filePath, ref _missing, ref _missing, ref _isVisible);

                    GenerateDoc(wordTemplateName);
                    _wDoc.SaveAs(wordTemplateName.AttachUniqueTimeStamp(true));
                    _wApp.Visible = true;
                }
                finally
                {
                    if (_wDoc != null)
                    {
                        Marshal.FinalReleaseComObject(_wDoc);
                        _wDoc = null;
                    }
                    if (_wApp != null)
                    {
                        Marshal.FinalReleaseComObject(_wApp);
                        _wApp = null;
                    }
                }
            }
            else
            {
                RaiseErrorLoadingTemplate(wordTemplateName);
            }
        }
    }

}
