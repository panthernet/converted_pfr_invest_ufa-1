﻿using System;
using System.Globalization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class PrintSILetters  : PrintToDoc
    {
        private readonly long _reqTransId;
        private int? _month;
        private int? _year;

        public PrintSILetters(long reqTransId, string template, string title) : this(reqTransId, template, title, -1, -1) {}

        public PrintSILetters(long reqTransId, string template, string title, int year, int month)
        {
            _reqTransId = reqTransId;
            TemplateName = template;
            Name = title;
            _month = month > 0 ? month : -1;
            _year = year > 0 ? year : -1;
        }

        public PrintSILetters()
        {
        }

        private ReqTransfer _reqTransfer;
        private AddressType _addressType = AddressType.Post;
        public void PrintReqTransfer(ReqTransfer req, string template, string title, string folder, string fileName, int year, int? month, AddressType addressType)
        {
            _addressType = addressType;
            _reqTransfer = req;
            TemplateName = template;
            Name = title;
            _month = month;
            _year = year < 2000 ? year + 2000 : year;
            Print(folder, fileName);
        }

        protected override void FillData()
        {
            var req = _reqTransfer ?? DataContainerFacade.GetByID<ReqTransfer, long>(_reqTransId);
            var transfer = req != null ? DataContainerFacade.GetByID<SITransfer, long>(req.TransferListID) : null;
            var contract = transfer?.GetContract();
            var le = contract?.GetLegalEntity();

            string addr;
            switch (_addressType)
            {
                case AddressType.Legal:
                    addr = le?.LegalAddress ?? string.Empty;
                    break;
                case AddressType.Fact:
                    addr = le?.StreetAddress ?? string.Empty;
                    break;
                default:
                    addr = le?.PostAddress ?? string.Empty;
                    break;
            }

            Fields.Add("LEGALENTITY.FULLNAME", le != null ? le.FullName : string.Empty);
            Fields.Add("LEGALENTITY.SHORTNAME", le != null ? le.ShortName : string.Empty);
            Fields.Add("LEGALENTITY.LETTERWHO", le?.LetterWho.Replace("\n", string.Empty) ?? string.Empty);
            Fields.Add("LEGALENTITY.POSTADDRESS", addr);
            Fields.Add("LEGALENTITY.LEGALADDRESS", addr);

            switch (le != null ? le.Sex ?? -1 : -1)
            {
                case 0:
                    Fields.Add("CALCULATED.TITLE", "Уважаемый");
                    break;
                case 1:
                    Fields.Add("CALCULATED.TITLE", "Уважаемая");
                    break;
                default:
                    Fields.Add("CALCULATED.TITLE", "Уважаемый(ая)");
                    break;
            }

            var headFullName = le != null ? le.HeadFullName : string.Empty;
            Fields.Add("LEGALENTITY.HEADNAME", !string.IsNullOrWhiteSpace(headFullName) ? headFullName.Substring(headFullName.IndexOf(' ') + 1) : string.Empty);            
            Fields.Add("CONTRACT.REGNUM", contract != null ? contract.ContractNumber : string.Empty);
            Fields.Add("CONTRACT.REGDATE", contract?.ContractDate?.ToString("dd.MM.yyyy", new CultureInfo("ru-Ru")) ?? string.Empty);
            
            var summ = req?.Sum ?? 0;
            var investIncome = req?.InvestmentIncome ?? 0;
            var summSPN = summ >= investIncome ? summ - investIncome : summ;
			// http://jira.dob.datateh.ru/browse/DOKIPIV-1114
			if (TransferDirectionIdentifier.IsFromUKToPFR(transfer.GetTransferRegister().DirectionID) && investIncome < 0)
	        {
		        summSPN = summ;
	        }

	        var rub = Math.Floor(summSPN);
            var kop = (summSPN - rub) * 100;

            Fields.Add("REQ_TRANSFER.SUM.RUB", rub.ToString("N0", new CultureInfo("ru-RU")));
            Fields.Add("REQ_TRANSFER.SUM.KOP", kop.ToString("00"));
            Fields.Add("REQ_TRANSFER.SUM.STR", Сумма.Пропись(summSPN, Валюта.Рубли, Заглавные.Первая));

			var isMinus = investIncome < 0;

			rub = Math.Floor(Math.Abs(investIncome));
			kop = (Math.Abs(investIncome) - rub) * 100;

			rub *= (isMinus ? -1 : 1);

            Fields.Add("REQ_TRANSFER.INVESTDOHOD.RUB", rub.ToString("N0", new CultureInfo("ru-RU")));
            Fields.Add("REQ_TRANSFER.INVESTDOHOD.KOP", kop.ToString("00"));
            
            var sum = Сумма.Пропись(Math.Abs(investIncome), Валюта.Рубли, Заглавные.Первая);
            Fields.Add("REQ_TRANSFER.INVESTDOHOD.STR", (isMinus ? "Минус " : "") + sum);

            if (_month > 0 && _year > 0)
            {
                Fields.Add("CALCULATED.UNTIL_DATE", GetReportDateWordsPrepositional(_year ?? DateTime.MinValue.Year, _month));
                Fields.Add("CALCULATED.UNTIL_DATE2", $"{new CultureInfo("ru-RU").DateTimeFormat.GetMonthName(_month ?? 1).ToLower()} {_year ?? DateTime.MinValue.Year} года");
            }
            if (_year > 0)
                Fields.Add("YEAR_ONLY", (_year ?? DateTime.MinValue.Year).ToString());
        }
    }
}
