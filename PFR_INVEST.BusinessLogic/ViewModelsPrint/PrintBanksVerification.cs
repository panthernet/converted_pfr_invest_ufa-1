﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using Document = Microsoft.Office.Interop.Word.Document;
using PFR_INVEST.BusinessLogic.XMLModels;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintBanksVerification : PrintToDoc
    {
        private readonly IList<LegalEntity> _banks;
        private readonly DateTime _date;
        private readonly IList<RatingAgency> _ratingAgencies;
        private readonly Person _authorizedPerson;
        private readonly Person _perfomer;
        private readonly Person _deputy;
        private readonly IList<Rating> _ratings;
        private readonly IList<LegalEntityRating> _legalEntityRatings;
        private readonly IList<MultiplierRating> _multiplierRatings;
        private readonly bool _isNewForm;

        private PrintBanksVerification(IList<LegalEntity> banks, IList<RatingAgency> ratingAgencies, DateTime date, Person authorizedPerson, Person deputy, Person perfomer, IList<LegalEntityRating> legalEntityRatings, IList<Rating> ratings, IList<MultiplierRating> multiplierRatings, string customFilePath = null)
        {
            Name = "Заключение о результатах проверки";
            TemplateName = string.Concat(Name, ".doc");
            _banks = banks;
            _ratingAgencies = ratingAgencies;
            _date = date.Date;
            _authorizedPerson = authorizedPerson;
            _perfomer = perfomer;
            _deputy = deputy;
            _ratings = ratings;
            _legalEntityRatings = legalEntityRatings;
            _multiplierRatings = multiplierRatings;
            FilePathSave = customFilePath;
        }

        public PrintBanksVerification(BanksVerificationDataContainer dc, DateTime date, Person authorizedPerson, Person deputy, Person perfomer, bool isNewForm, string customFilePath = null)
            : this(dc.Banks, dc.RatingAgencies, date, authorizedPerson, deputy, perfomer, dc.LegalEntityRatings, dc.Ratings, dc.MultiplierRatings, customFilePath)
        {
            _isNewForm = isNewForm;
            if (_isNewForm)
            {
                Name = "Заключение о результатах проверки2";
                TemplateName = string.Concat(Name, ".doc");
            }
        }

        protected void FillPersonsData()
		{
			var apDict = DataContainerFacade.GetEntityDBProjection(_authorizedPerson);
			FillPersonData("AUTH", apDict);
			FillPersonFIO("AUTH", apDict);
			var dpDict = DataContainerFacade.GetEntityDBProjection(_deputy);
			FillPersonData("DEPUTY", dpDict);
			FillPersonFIO("DEPUTY", dpDict);
			var pfDict = DataContainerFacade.GetEntityDBProjection(_perfomer);
			FillPersonData("PERFORMER", pfDict);
			FillPersonFIO("PERFORMER", pfDict);
		}

		protected void FillPersonFIO(string prefix, Dictionary<string, object> ent)
		{
			if (ent == null) return;
			string fieldTemplate = string.IsNullOrWhiteSpace(prefix) ? "{1}" : "{0}.{1}";
			var fn = ent["PERSON_FIRST_NAME"] as string;
			Fields[string.Format(fieldTemplate, prefix, "PERSON_F")] = (!string.IsNullOrEmpty(fn)) ? fn.First() + "." : string.Empty;

			var mn = ent["PERSON_MIDDLE_NAME"] as string;
			Fields[string.Format(fieldTemplate, prefix, "PERSON_M")] = (!string.IsNullOrEmpty(mn)) ? mn.First() + "." : string.Empty;

			var ln = ent["PERSON_LAST_NAME"] as string;
			Fields[string.Format(fieldTemplate, prefix, "PERSON_L")] = (!string.IsNullOrEmpty(ln)) ? ln.First() + "." : string.Empty;
		}

		protected void FillPersonData(string prefix, Dictionary<string, object> ent)
		{
			if (ent == null) return;
			string fieldTemplate = string.IsNullOrWhiteSpace(prefix) ? "{1}" : "{0}.{1}";
			foreach (var kvp in ent)
			{
				var key = string.Format(fieldTemplate, prefix, kvp.Key);

				string val;
				if (kvp.Value is DateTime?)
					val = (kvp.Value as DateTime?).Value.ToString("dd.MM.yyyy");
				else
					val = kvp.Value?.ToString() ?? string.Empty;

				Fields[key] = val;
			}
		}

        protected override void FillData()
        {
			FillPersonsData();
            Fields.Add("CURDATE", _date.ToString("dd.MM.yyyy"));
            if (_authorizedPerson != null)
            {
                Fields.Add("AUTHPERSON",
                    !string.IsNullOrEmpty(_authorizedPerson.MiddleName)
                        ? $"{_authorizedPerson.LastName} {_authorizedPerson.FirstName.First()}.{_authorizedPerson.MiddleName.First()}."
                        : $"{_authorizedPerson.LastName} {_authorizedPerson.FirstName.First()}.");
                Fields.Add("AUTHPERSONPOSITION", _authorizedPerson.Position);
            }
            if (_deputy != null)
            {
                Fields.Add("DEPUTY",
                    !string.IsNullOrEmpty(_deputy.MiddleName)
                        ? $"{_deputy.LastName} {_deputy.FirstName.First()}.{_deputy.MiddleName.First()}."
                        : $"{_deputy.LastName} {_deputy.FirstName.First()}.");
                Fields.Add("DEPUTYPOSITION", _deputy.Position);
            }
            if (_perfomer != null)
            {
                Fields.Add("PERFOMER",
                    !string.IsNullOrEmpty(_perfomer.MiddleName)
                        ? $"{_perfomer.LastName} {_perfomer.FirstName.First()}.{_perfomer.MiddleName.First()}."
                        : $"{_perfomer.LastName} {_perfomer.FirstName.First()}.");
                Fields.Add("PERFOMERPOSITION", _perfomer.Position);
                Fields.Add("PERFOMERPHONE", _perfomer.Phone);
            }

            Fields.Add("CURRENT_DATE", _banks.Count > 0 ? (_banks[0].MoneyDate?.ToString("dd.MM.yyyy") ?? "") : "");

        }

        protected override void GenerateDocument(Application app, Document doc)
        {
            base.GenerateDocument(app, doc);
            var worldAgencies = new List<RatingAgency>();
            if(!_isNewForm)
                worldAgencies = _ratingAgencies.Where(item => item.AgencyType.ToLower() == RatingAgencyIdentifier.RatingAgencyTypes[RatingAgencyIdentifier.RatingAgencyType.World].ToLower()).OrderBy(item => item.ID).ToList();
            var columnAgencyMapping = new Dictionary<long, int>();

            ProcessBanks(doc, columnAgencyMapping, worldAgencies);

            ProcessPersons(doc);
        }

        private void ProcessPersons(Document doc)
        {
            var table = doc.Tables[2];
            if (_authorizedPerson == null)
            {
                table.Cell(1, 1).Row.Delete();
                if (_deputy == null)
                {
                    table.Cell(1, 1).Row.Delete();
                    if (_perfomer == null)
                        table.Cell(1, 1).Row.Delete();
                }
                else if (_perfomer == null)
                    table.Cell(2, 1).Row.Delete();
            }
            if (_authorizedPerson != null && _deputy == null)
            {
                table.Cell(2, 1).Row.Delete();
                if (_perfomer == null)
                    table.Cell(2, 1).Row.Delete();
            }
            if (_authorizedPerson != null && _deputy != null && _perfomer == null)
                table.Cell(3, 1).Row.Delete();
        }

        private void ProcessBanksNewForm(Document doc)
        {
            var table = doc.Tables[1];
            var bankList = _banks.Where(a=> a.PFRAGRSTAT.ToLower() == "заключено").OrderBy(a => a.NumberForDocuments ?? int.MaxValue).ThenBy(a => a.ShortName).ToList();
            var licenses = DataContainerFacade.GetList<License>();
            var koStatuses = DataContainerFacade.GetList<BanksConclusion>();

            var count = 0;
            table.AllowAutoFit = true;
            foreach (var bank in bankList)
            {
                var bankStatuses = bank.GetBankStatuses();
                var maxKODate02 = koStatuses.Where(a => a.ElementID == 115002).Max(a => (DateTime?)a.ImportDate);
                var maxKODate03 = koStatuses.Where(a => a.ElementID == 115003).Max(a => (DateTime?)a.ImportDate);
                var maxKODate04 = koStatuses.Where(a => a.ElementID == 115004).Max(a => (DateTime?)a.ImportDate);
                var maxKODate05 = koStatuses.Where(a => a.ElementID == 115005).Max(a => (DateTime?)a.ImportDate);

                table.Rows.Add();

                table.Cell(table.Rows.Count, 1).Range.Text = (++count).ToString();
                table.Cell(table.Rows.Count, 2).Range.Text = bank.ShortName;
                try
                {
                    //на 2016 офисе дает исключение
                    table.Cell(table.Rows.Count, 2).SetHeight(25, WdRowHeightRule.wdRowHeightAuto);
                }
                catch
                {
                    // ignored
                }


                var cell4Value = licenses.Where(a => a.LegalEntityID == bank.ID).Any(a => a.RegistrationDate.HasValue && !string.IsNullOrEmpty(a.Number));
                var cell5Value = true;
                var cell6Value = bankStatuses.FirstOrDefault(a => a.ImportDate == maxKODate03 && a.ImportElementID == 115003 && a.BankStatus == "ДА") != null;
                var cell7Value = bankStatuses.FirstOrDefault(a => a.ImportDate == maxKODate04 && a.ImportElementID == 115004 && a.BankStatus == "ДА") != null;
                var cell8Value = bankStatuses.FirstOrDefault(a => a.ImportDate == maxKODate05 && a.ImportElementID == 115005 && a.BankStatus == "ДА") != null;
                var sumValue = (long) ((bank.Money ?? 0M) / 1000000);
                var cell3Value = cell4Value && cell5Value && cell6Value && (cell7Value || cell8Value) &&
                                 (sumValue >= 250000 || (sumValue >= 25000 && bankStatuses.FirstOrDefault(a => a.ImportDate == maxKODate02 && a.ImportElementID == 115002 && a.BankStatus == "ДА") != null));

                table.Cell(table.Rows.Count, 3).Range.Text = cell3Value ? "соответствует" : "не соответствует";
                table.Cell(table.Rows.Count, 4).Range.Text = cell4Value ? "да" : "нет";
                table.Cell(table.Rows.Count, 5).Range.Text = cell5Value ? "да" : "нет";
                table.Cell(table.Rows.Count, 6).Range.Text = cell6Value ? "да" : "нет";
                table.Cell(table.Rows.Count, 7).Range.Text = cell7Value ? "да" : "нет";
                table.Cell(table.Rows.Count, 8).Range.Text = cell8Value ? "да" : "нет";

                //столбец 9 - Размер собственных средств 
                table.Cell(table.Rows.Count, 9).Range.Text = sumValue.ToString("### ### ### ### ### ###").Trim();
            }
            //на 2016 офисе съезжает размер таблицы
            table.PreferredWidth = 100f;
        }

        private void ProcessBanks(Document doc, IDictionary<long, int> columnAgencyMapping, IList<RatingAgency> worldAgencies)
        {
            if (_isNewForm)
            {
                ProcessBanksNewForm(doc);
                return;
            }

            var table = doc.Tables[1];

            int wN = worldAgencies.Count;
            if (wN > 0)
            {
                table.Cell(2, 5).Split(1, wN);
                table.Cell(3, 5).Split(1, wN);
                for (int i = 0; i < wN; i++)
                {
                    RatingAgency agency = worldAgencies[i];
                    int columnIndex = i + 5;
                    table.Cell(2, columnIndex).Range.Text = agency.Name;
                    columnAgencyMapping.Add(agency.ID, columnIndex);
                    table.Cell(3, columnIndex).Range.Text = columnIndex.ToString();
                }
            }
            else
            {
                int columnIndex = 5;
                table.Cell(3, columnIndex).Range.Text = columnIndex.ToString();
                if (wN == 0)//Поправка на пустую колонку
                    wN = 1;
            }

            var bankList = _banks.OrderBy(a => a.NumberForDocuments ?? int.MaxValue).ThenBy(a => a.ShortName).ToList();

            var count = 0;
            table.AllowAutoFit = true;
            foreach (var bank in bankList)
            {
                var iBank = bank;
                var leRatingLinks = _legalEntityRatings.Where(item => item.LegalEntityID == iBank.ID);
                var grouppedRatingAgencies = _multiplierRatings.Where(x => leRatingLinks.Any(l => l.MultiplierRatingID == x.ID)).GroupBy(item => item.AgencyID).ToList();

                table.Rows.Add();

                table.Cell(table.Rows.Count, 1).Range.Text = (++count).ToString();
                table.Cell(table.Rows.Count, 2).Range.Text = bank.ShortName;
                try
                {
                    //на 2016 офисе дает исключение
                   table.Cell(table.Rows.Count, 2).SetHeight(25, WdRowHeightRule.wdRowHeightAuto);
                }
                catch
                {
                    // ignored
                }

                bool includedToThe761List = IsIncludedToThe761List(bank, _date);

                table.Cell(table.Rows.Count, 4).Range.Text = includedToThe761List ? "да" : "нет";
                table.Cell(table.Rows.Count, 3).Range.Text = includedToThe761List &&
                                                             grouppedRatingAgencies.Any(item => item.Any(rat => rat.Multiplier >= 0))
                    ? "соответствует"
                    : "не соответствует";

                foreach (var g in grouppedRatingAgencies)
                {
                    var rating = g.FirstOrDefault(item => g != null && item.Multiplier == g.Max(rat => rat.Multiplier ?? 0));
                    if (rating != null)
                    {
                        var r = _ratings.First(item => item.ID == rating.RatingID);
                        if (rating.AgencyID.HasValue && columnAgencyMapping.ContainsKey(rating.AgencyID.Value))
                            table.Cell(table.Rows.Count, columnAgencyMapping[rating.AgencyID.Value]).Range.Text = r.RatingName;
                    }
                }

                table.Cell(table.Rows.Count, 5 + wN).Range.Text = ((long)((bank.Money ?? 0M) / 1000000)).ToString("### ### ### ### ### ###").Trim();

                table.Cell(3, 5 + wN).Range.Text = (5 + wN).ToString();
            }
            //на 2016 офисе съезжает размер таблицы
            table.PreferredWidth = 100f;
        }

        public static bool IsIncludedToThe761List(LegalEntity bank, DateTime date)
        {
            // TODO: исключить из этого класса обращения к Extension методам и DataContainerFacade: все данные, по которым строится отчет, должны журналироваться:
            // http://jira.vs.it.ru/browse/PFRPTKIV-3
            IList<BankConclusion> importedStatuses = bank.GetBankStatuses().ToList();

            var pastImport =
                importedStatuses.Where(item => item.ImportDate <= date)
                    .OrderByDescending(item => item.ImportDate)
                    .FirstOrDefault();
            return pastImport != null && pastImport.BankStatusBool;
        }

        public static bool IsKOValidBank(LegalEntity bank, DateTime date, IList<LegalEntityRating> legalEntityRatings, IList<MultiplierRating> multiplierRatings)
        {
            var includedToThe761List = IsIncludedToThe761List(bank, date);
            var iBank = bank;
            var leRatingLinks = legalEntityRatings.Where(item => item.LegalEntityID == iBank.ID);
            var grouppedRatingAgencies = multiplierRatings.Where(x => leRatingLinks.Any(l => l.MultiplierRatingID == x.ID)).GroupBy(item => item.AgencyID).ToList();
            return includedToThe761List && grouppedRatingAgencies.Any(item => item.Any(rat => rat.Multiplier >= 0));
        }
    }
}
