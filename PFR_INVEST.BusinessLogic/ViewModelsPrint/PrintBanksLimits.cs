﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.XMLModels;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintBanksLimits : PrintToExcel
    {
        private readonly IList<LegalEntity> _banks;
        private readonly DateTime _date;
        private readonly Person _authorizedPerson;
        private readonly Person _perfomer;
        private readonly DepClaimSelectParams _selectParams;
        private readonly IList<DepClaim2> _depclaims;

        public Dictionary<long, decimal> LimitOnValues { get; }
        public Dictionary<long, decimal> LimitInfoOnValues { get; }

        private readonly int _globalCount;

        public PrintBanksLimits(IList<LegalEntity> banks, DateTime date, Person authorizedPerson, Person perfomer, DepClaimSelectParams selectParams, IList<DepClaim2> depclaims, int count = 0)
        {
            _globalCount = count;
            Name = "Расчет лимитов";
            TemplateName = string.Concat(Name, ".xls");
            _banks = banks;
            _date = date;
            _authorizedPerson = authorizedPerson;
            _perfomer = perfomer;
            _selectParams = selectParams;
            LimitOnValues = new Dictionary<long, decimal>();
            LimitInfoOnValues = new Dictionary<long, decimal>();
            _depclaims = depclaims;
        }

        public PrintBanksLimits(BanksVerificationDataContainer dc, DateTime date, Person authorizedPerson, Person perfomer, DepClaimSelectParams selectParams)
            : this(dc.Banks, date, authorizedPerson, perfomer, selectParams, dc.Depclaims)
        {
        }

        protected override string PrintingInformation => _banks.Count == 1 ? $@"Печать лимитов для банкa ""{_banks.First().FormalizedName}""" : "Печать лимитов для банков";

        protected void FillPersonsData()
        {
            var apDict = DataContainerFacade.GetEntityDBProjection(_authorizedPerson);
            FillEntityData("AUTH", apDict);
            FillPersonFIO("AUTH", apDict);
            var pfDict = DataContainerFacade.GetEntityDBProjection(_perfomer);
            FillEntityData("PERFORMER", pfDict);
            FillPersonFIO("PERFORMER", pfDict);
        }

        protected void FillPersonFIO(string prefix, Dictionary<string, object> ent)
        {
            if (ent == null) return;
            string fieldTemplate = string.IsNullOrWhiteSpace(prefix) ? "{1}" : "{0}.{1}";
            var fn = ent["PERSON_FIRST_NAME"] as string;
            Fields[string.Format(fieldTemplate, prefix, "PERSON_F")] = (!string.IsNullOrEmpty(fn)) ? fn.First() + "." : string.Empty;

            var mn = ent["PERSON_MIDDLE_NAME"] as string;
            Fields[string.Format(fieldTemplate, prefix, "PERSON_M")] = (!string.IsNullOrEmpty(mn)) ? mn.First() + "." : string.Empty;

            var ln = ent["PERSON_LAST_NAME"] as string;
            Fields[string.Format(fieldTemplate, prefix, "PERSON_L")] = (!string.IsNullOrEmpty(ln)) ? ln.First() + "." : string.Empty;
        }

        protected void FillAuctionData()
        {
            var aucDic = DataContainerFacade.GetEntityDBProjection(_selectParams);
            FillEntityData(string.Empty, aucDic);
        }

        protected void FillEntityData(string prefix, Dictionary<string, object> ent)
        {
            if (ent == null) return;
            string fieldTemplate = string.IsNullOrWhiteSpace(prefix) ? "{1}" : "{0}.{1}";

            foreach (var kvp in ent)
            {
                var key = string.Format(fieldTemplate, prefix, kvp.Key);

                string val;
                if (kvp.Value is DateTime?)
                    val = (kvp.Value as DateTime?).Value.ToLongDateString();
                else
                    val = kvp.Value?.ToString() ?? string.Empty;

                Fields[key] = val;
            }
        }

        protected override void FillData()
        {
            var dft = Thread.CurrentThread.CurrentCulture;
            var ruRu = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentCulture = ruRu;

            try
            {
                FillPersonsData();
                FillAuctionData();
                Fields.Add("CURDATE", _date.ToLongDateString());
                Fields.Add("SELECTDATE", _selectParams.SelectDate.ToLongDateString());
                if (_authorizedPerson != null)
                {
                    Fields.Add("AUTHPERSON",
                        !string.IsNullOrEmpty(_authorizedPerson.MiddleName)
                            ? $"{_authorizedPerson.LastName} {_authorizedPerson.FirstName.First()}.{_authorizedPerson.MiddleName.First()}."
                            : $"{_authorizedPerson.LastName} {_authorizedPerson.FirstName.First()}.");
                    Fields.Add("AUTHPERSONPOSITION", _authorizedPerson.Position);
                }
                else
                {
                    Fields.Add("AUTHPERSON", string.Empty);
                    Fields.Add("AUTHPERSONPOSITION", string.Empty);
                }


                if (_perfomer != null)
                {
                    Fields.Add("PERFOMER",
                        !string.IsNullOrEmpty(_perfomer.MiddleName)
                            ? $"{_perfomer.LastName} {_perfomer.FirstName.First()}.{_perfomer.MiddleName.First()}."
                            : $"{_perfomer.LastName} {_perfomer.FirstName.First()}.");
                    Fields.Add("PERFOMERPOSITION", _perfomer.Position);
                    Fields.Add("PERFOMERPHONE", _perfomer.Phone);
                }
                else
                {
                    Fields.Add("PERFOMER", string.Empty);
                    Fields.Add("PERFOMERPOSITION", string.Empty);
                    Fields.Add("PERFOMERPHONE", string.Empty);
                }
                Fields.Add("LOW_DATE", _banks.Count > 0 ? (_banks.Where(a=> a.MoneyDate != null).Max(a=> a.MoneyDate)?.ToString("dd.MM.yyyy") ?? "") : "");
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = dft;
            }
        }

        protected override void GenerateDocument(Application app)
        {
            base.GenerateDocument(app);

            var s = (Worksheet) Workbook.Sheets[1];
            try
            {
                const int n = 0;

                var row = 11;
                decimal moneySum = 0;
                decimal limitMoneySum = 0;
                decimal placedAndReturnSum = 0;
                decimal returnedSum = 0;
                decimal resultValueSum = 0;

                var bankList = _banks;
                //выставляем порядковый номер
                var count = _banks.Count == 1 && _globalCount != 0 ? (_globalCount - 1) : 0;

                foreach (var bank in bankList)
                {
                    row++;
                    s.Range[s.Cells[row, 1], s.Cells[row, 1]].EntireRow.Insert(XlInsertShiftDirection.xlShiftDown);
                    s.Range[s.Cells[row, 1], s.Cells[row, 1]].Value2 = ++count;
                    s.Range[s.Cells[row, 2], s.Cells[row, 2]].Value2 = bank.ShortName;

                    //рейтинговый коэф.
                    decimal? max = BankCalculator.LimitMult(bank);
                    {
                        var rate = max.Value;

                        s.Range[s.Cells[row, 4 + n], s.Cells[row, 4 + n]].Value2 = rate;

                        decimal limitMoney = BankCalculator.Limit4Money(bank)*1000000;

                        var bank1 = bank;
                        var deposits = _depclaims.Where(p => p.BankID == bank1.ID);
                        decimal placedAndReturned = 0;
                        decimal returned = 0;
                        foreach (var deposit in deposits)
                        {
                            //4.3.1. Сумма средств страховых взносов, размещенная на депозитах в кредитной организации на начало рабочего дня, предшествующего дню проведения отбора заявок;
                            //4.3.2. Сумма средств страховых взносов, подлежащая размещению на депозитах в кредитной организации в рабочий день, предшествующий дню проведения отбора заявок;
                            //4.3.3. сумма средств страховых взносов, подлежащая размещению на депозитах в i-ой кредитной организации в день проведения отбора заявок.
                            if (deposit.SettleDate <= _selectParams.SelectDate && deposit.ReturnDate > _selectParams.SelectDate)
                                placedAndReturned += deposit.Amount;

                            //4.5.1. Сумма средств страховых взносов, размещенная на депозитах в кредитной организации, подлежащая возврату в рабочий день, предшествующий дню проведения отбора заявок;
                            //4.5.2. Сумма средств страховых взносов, размещенная на депозитах в кредитной организации, подлежащая возврату кредитной организацией в день проведения отбора заявок;
                            //4.5.3. Сумма средств страховых взносов, размещенная на депозитах в кредитной организации, подлежащая возврату кредитной организацией в день перечисления средств кредитной организации по итогам проведенного отбора заявок;
                            if (deposit.ReturnDate == _selectParams.SelectDate.AddDays(-1)
                                || deposit.ReturnDate == _selectParams.SelectDate
                                || deposit.ReturnDate == _selectParams.PlacementDate)
                                returned += deposit.Amount;
                        }


                        var resultValue = limitMoney - placedAndReturned + returned;

                        LimitOnValues.Add(bank.ID, Math.Round(resultValue));
                        LimitInfoOnValues.Add(bank.ID, Math.Round(limitMoney));

                        // Display in millions
                        decimal money = (bank.Money ?? 0M)/1000000;
                        limitMoney = limitMoney/1000000;
                        placedAndReturned = placedAndReturned/1000000;
                        returned = returned/1000000;
                        resultValue = resultValue/1000000;

                        //Round
                        money = Math.Round(money);
                        limitMoney = Math.Round(limitMoney);
                        placedAndReturned = Math.Round(placedAndReturned);
                        returned = Math.Round(returned);
                        resultValue = Math.Round(resultValue);

                        //остальные значения
                        s.Range[s.Cells[row, 3 + n], s.Cells[row, 3 + n]].Value2 = (long) money;
                        s.Range[s.Cells[row, 5 + n], s.Cells[row, 5 + n]].Value2 = (long) limitMoney;

                        s.Range[s.Cells[row, 6 + n], s.Cells[row, 6 + n]].Value2 = placedAndReturned;
                        s.Range[s.Cells[row, 7 + n], s.Cells[row, 7 + n]].Value2 = returned;
                        s.Range[s.Cells[row, 8 + n], s.Cells[row, 8 + n]].Value2 = resultValue;

                        s.Range[s.Cells[row, 9 + n], s.Cells[row, 9 + n]].Value2 = "-";

                        moneySum += money;
                        limitMoneySum += limitMoney;
                        placedAndReturnSum += placedAndReturned;
                        returnedSum += returned;
                        resultValueSum += resultValue;
                    }
                }

                if (_banks.Count > 1)
                {
                    s.Range[s.Cells[12 + _banks.Count, 3 + n], s.Cells[12 + _banks.Count, 3 + n]].Value2 = moneySum;
                    s.Range[s.Cells[12 + _banks.Count, 5 + n], s.Cells[12 + _banks.Count, 5 + n]].Value2 = limitMoneySum;
                    s.Range[s.Cells[12 + _banks.Count, 6 + n], s.Cells[12 + _banks.Count, 6 + n]].Value2 = placedAndReturnSum;
                    s.Range[s.Cells[12 + _banks.Count, 7 + n], s.Cells[12 + _banks.Count, 7 + n]].Value2 = returnedSum;
                    s.Range[s.Cells[12 + _banks.Count, 8 + n], s.Cells[12 + _banks.Count, 8 + n]].Value2 = resultValueSum;
                }
                else ((Range) s.Rows[12 + _banks.Count]).Delete(XlDeleteShiftDirection.xlShiftUp);
            }
            finally
            {
                if (s != null)
                    Marshal.FinalReleaseComObject(s);
            }
        }
    }
}
