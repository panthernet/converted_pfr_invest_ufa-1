﻿
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.BusinessLogic.ViewModelsPrint
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;using Microsoft.Office.Interop.Excel;
    using Core.Tools;
    using DataObjects;
    using DataObjects.ListItems;
    using Application = Microsoft.Office.Interop.Excel.Application;
    using DataAccess.Client;

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintExportDepClaimAllocation : PrintToExcel
    {
        private const string FORMAT = "dd.MM.yyyy";

        //private DateTime Date { get; set; }
        public DepClaimSelectParams Auction { get; private set; }
        public IList<DepClaimOfferListItem> Offers { get; private set; }
        public Person AuthorizedPerson { get; set; }
        public Person Performer { get; set; }
        public DateTime? ClaimDate { get; set; }

        public PrintExportDepClaimAllocation(long auctionID)
        {
            //this.Date = date;
            Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionID);
			//this.Offers = BLServiceSystem.Client.GetDepClaimOfferListFiltered(this.Auction.ID, new List<DepClaimOffer.Statuses>() { DepClaimOffer.Statuses.Accepted });
			Offers = BLServiceSystem.Client.GetDepClaimOfferListFiltered(Auction.ID, new List<DepClaimOffer.Statuses>() { DepClaimOffer.Statuses.Accepted, DepClaimOffer.Statuses.NotAccepted, DepClaimOffer.Statuses.NotSigned, DepClaimOffer.Statuses.Signed });

            Name = string.Format("ЗАЯВКА Депозиты {0:dd.MM.yy}", Auction.SelectDate);
            TemplateName = "ЗАЯВКА Депозиты.xls";
        }

        private void GenerateDoc()
        {
            //if (!DepClaims.Any())
            //return;
            //Worksheet.get_Range("F4").Cells.Value2 = this.Auction.SelectDate.ToString(format);
            //Worksheet.get_Range("F5").Cells.Value2 = this.Date.ToString(format);




            //var totalAmount = this.DepClaims.Sum(dc => dc.Amount);
            //var totalPayment = this.DepClaims.Sum(dc => dc.Payment);
            //var totalReturn = totalAmount + totalPayment;

            //Worksheet.get_Range("E15").Cells.Value2 = totalAmount.ToString("0");
            //Worksheet.get_Range("F15").Cells.Value2 = totalPayment.ToString("0");
            //Worksheet.get_Range("G15").Cells.Value2 = totalReturn.ToString("0");

            //int start = 13;
            //int index = 1;
            //object obj = Worksheet.get_Range("A" + start).EntireRow.Copy();
            //foreach (var dc in this.DepClaims)
            //{
            //    Worksheet.get_Range("A" + start).EntireRow.Insert(obj);
            //    Worksheet.get_Range("A" + start).Cells.Value2 = index;
            //    Worksheet.get_Range("B" + start).Cells.Value2 = dc.FirmName;
            //    Worksheet.get_Range("C" + start).Cells.Value2 = dc.FirmID;
            //    Worksheet.get_Range("D" + start).Cells.Value2 = dc.Rate.ToString("#.0");
            //    Worksheet.get_Range("E" + start).Cells.Value2 = dc.Amount.ToString("0");
            //    Worksheet.get_Range("F" + start).Cells.Value2 = dc.Payment.ToString("0");
            //    Worksheet.get_Range("G" + start).Cells.Value2 = (dc.Amount + dc.Payment).ToString("0");
            //    Worksheet.get_Range("H" + start).Cells.Value2 = dc.SettleDate.ToString(format);
            //    Worksheet.get_Range("I" + start).Cells.Value2 = dc.ReturnDate.ToString(format);
            //    Worksheet.get_Range("J" + start).Cells.Value2 = (dc.ReturnDate - dc.SettleDate).Days;
            //    index++;
            //    start++;

            //}
            //Worksheet.get_Range("A" + start).EntireRow.Delete();
            //Worksheet.get_Range("A1").Select();

        }



        protected override void FillData()
        {
            var sum = Offers.Sum(o => o.DepClaim.Amount);
			var ci = CultureInfo.GetCultureInfo("ru-RU");
            Fields.Add("SumValue", sum.ToString("n2", ci));
            Fields.Add("SumText", Сумма.Пропись(sum, Валюта.Рубли, Заглавные.Первая));
            
            var payAss = BLServiceSystem.Client.GetPayAssForReport((long)Element.SpecialDictionaryItems.AppPartDEPO, (long)Element.SpecialDictionaryItems.CommonTransferDirectionFromPFR);

            Fields.Add("PayAss", payAss == null ? "" : payAss.Name);


            if (AuthorizedPerson != null)
            {
                Fields.Add("AUTHPERSON",
                    !string.IsNullOrEmpty(AuthorizedPerson.MiddleName)
                        ? string.Format("{0} {1}.{2}.", AuthorizedPerson.LastName, AuthorizedPerson.FirstName.First(), AuthorizedPerson.MiddleName.First())
                        : string.Format("{0} {1}.", AuthorizedPerson.LastName, AuthorizedPerson.FirstName.First()));
                Fields.Add("AUTHPERSONPOSITION", AuthorizedPerson.Position);
            }
            else
            {
                Fields.Add("AUTHPERSON", string.Empty);
                Fields.Add("AUTHPERSONPOSITION", string.Empty);
            }

            if (Performer != null)
            {
                Fields.Add("PERFOMER",
                    !string.IsNullOrEmpty(Performer.MiddleName)
                        ? string.Format("{0} {1}.{2}.", Performer.LastName, Performer.FirstName.First(), Performer.MiddleName.First())
                        : string.Format("{0} {1}.", Performer.LastName, Performer.FirstName.First()));
                Fields.Add("PERFOMERPOSITION", Performer.Position);
                Fields.Add("PERFOMERPHONE", Performer.Phone);
            }
            else
            {
                Fields.Add("PERFOMER", string.Empty);
                Fields.Add("PERFOMERPOSITION", string.Empty);
                Fields.Add("PERFOMERPHONE", string.Empty);
            }

        }

        //Add persons here
        protected override void GenerateDocument(Application app)
        {
            if(ClaimDate == null)
                throw new Exception("Не задана дата заявки!");
            var ci = CultureInfo.GetCultureInfo("ru-RU");
            base.GenerateDocument(app);
            var ws = (Worksheet)app.Worksheets[1];
            var index = 12;
            var obj = ws.Range["A" + index].EntireRow.Copy();
            if (Offers.FirstOrDefault() != null)
            {
                ws.Range["E7"].Cells.Value2 = "от " + ClaimDate.Value.ToString("dd.MM.yyyy", CultureInfo.CreateSpecificCulture("ru-RU")) + " года";
            }

            foreach (var o in Offers)
            {
                ws.Range["A" + index].EntireRow.Insert(obj, true);
                var bank = DataContainerFacade.GetByID<LegalEntity>(o.DepClaim.BankID);
                ws.Range["A" + index].Cells.Value2 = bank.ShortName;
                ws.Range["B" + index].Cells.Value2 = o.Offer.Number;
                ws.Range["C" + index].Cells.Value2 = bank.PFRAGRDATE?.ToString(FORMAT) ?? string.Empty;
                ws.Range["D" + index].Cells.Value2 = bank.PFRAGR;
                ws.Range["E" + index].Cells.Value2 = bank.INN;
                ws.Range["F" + index].Cells.Value2 = bank.CorrAcc;
                ws.Range["G" + index].Cells.Value2 = !string.IsNullOrWhiteSpace(bank.CorrNote) ? bank.CorrNote.Trim() : string.Empty;
                ws.Range["H" + index].Cells.Value2 = bank.BIK; 
                ws.Range["I" + index].Cells.Value2 = o.DepClaim.Amount.ToString("n2", ci);


                //var row = ws.get_Range("A" + index).EntireRow;
                //row.Replace("{BankName}", bank.ShortName);
                //row.Replace("{OfferNum}", o.Offer.Number);
                //row.Replace("{PFRDate}", string.Empty ?? bank.PFRAGR);
                //row.Replace("{PFRAGR}", bank.PFRAGR);
                //row.Replace("{INN}", bank.INN);
                //row.Replace("{BIK}", bank.BIK);
                //row.Replace("{CorrAcc}", " " + bank.CorrAcc);
                //row.Replace("{Ammount}", o.DepClaim.Amount.ToString("n2", ci));
                index++;
            }
            ws.Range["A" + index].EntireRow.Delete();
        }
    }
}
