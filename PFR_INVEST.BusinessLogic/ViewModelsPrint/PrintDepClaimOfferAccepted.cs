﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;


namespace PFR_INVEST.BusinessLogic
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	public sealed class PrintDepClaimOfferAccepted : PrintToDoc
	{

		private Dictionary<long, LegalEntity> _banks;
		private readonly CultureInfo _culture = CultureInfo.CreateSpecificCulture("ru-RU");

		public PrintDepClaimOfferAccepted(DepClaimSelectParams auction)
		{
			Auction = auction;
			//Offers = BLServiceSystem.Client.GetDepClaimOfferListFiltered(Auction.ID, new List<DepClaimOffer.Statuses> { DepClaimOffer.Statuses.Accepted });
			Offers = BLServiceSystem.Client.GetDepClaimOfferListFiltered(Auction.ID, new List<DepClaimOffer.Statuses> { DepClaimOffer.Statuses.Accepted, DepClaimOffer.Statuses.NotAccepted, DepClaimOffer.Statuses.NotSigned, DepClaimOffer.Statuses.Signed });

			Name = "Сводный реестр договоров банковского депозита";
			TemplateName = $"{Name}.doc";
		}

		public Person AuthorizedPerson { get; set; }
		public Person Performer { get; set; }
		public Person Deputy { get; set; }
		public DateTime? Date { get; set; }
		public int Number { get; set; }
		public decimal Rate
		{
			get { return Auction.CutoffRate; }
			set { Auction.CutoffRate = value; }
		}

		public DepClaimSelectParams Auction { get; private set; }

		public List<DepClaimOfferListItem> Offers { get; set; }

		protected override void FillData()
		{
			if (AuthorizedPerson != null)
			{
				Fields.Add("AUTHPERSON",
					!string.IsNullOrEmpty(AuthorizedPerson.MiddleName)
						? $"{AuthorizedPerson.LastName} {AuthorizedPerson.FirstName.First()}.{AuthorizedPerson.MiddleName.First()}."
					    : $"{AuthorizedPerson.LastName} {AuthorizedPerson.FirstName.First()}.");
				Fields.Add("AUTHPERSONPOSITION", AuthorizedPerson.Position);
			}
			if (Deputy != null)
			{
				Fields.Add("DEPUTY",
					!string.IsNullOrEmpty(Deputy.MiddleName)
						? $"{Deputy.LastName} {Deputy.FirstName.First()}.{Deputy.MiddleName.First()}."
					    : $"{Deputy.LastName} {Deputy.FirstName.First()}.");
				Fields.Add("DEPUTYPOSITION", Deputy.Position);
			}
			if (Performer != null)
			{
				Fields.Add("PERFOMER",
					!string.IsNullOrEmpty(Performer.MiddleName)
						? $"{Performer.LastName} {Performer.FirstName.First()}.{Performer.MiddleName.First()}."
					    : $"{Performer.LastName} {Performer.FirstName.First()}.");
				Fields.Add("PERFOMERPOSITION", Performer.Position);
				Fields.Add("PERFOMERPHONE", Performer.Phone);
			}


			Fields.Add("DATE", Date?.ToString("dd MMMM yyyy", _culture) ?? "");
			Fields.Add("NUMBER", Number.ToString(_culture));
			Fields.Add("SELECTDATE", Auction.SelectDate.ToString("dd MMMM yyyy", _culture));
			Fields.Add("MAXINSURANCEFEE", ((long)Math.Round(Auction.MaxInsuranceFee)).ToString("n0", _culture));
			Fields.Add("MINRATE", Auction.MinRate.ToString("n2", _culture));
			Fields.Add("RATE", Rate.ToString("n2", _culture));
			Fields.Add("PLACEMENTDATE", Auction.PlacementDate.ToString("dd MMMM yyyy", _culture));
			Fields.Add("RETURNDATE", Auction.ReturnDate.ToString("dd MMMM yyyy", _culture));
		}

		protected override void GenerateDocument(Application app, Microsoft.Office.Interop.Word.Document doc)
		{
			ProcessPersons(doc);
			var table = doc.Tables[1];

			_banks = DataContainerFacade.GetListByPropertyConditions<LegalEntity>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition
                                                                                                {
                                                                                                    Name = "ID",
                                                                                                    Operation = "in",
                                                                                                    Values = Offers.Select(item => item.Offer.BankID).Cast<object>().ToArray()
                                                                                                }
                                                                                       }).ToDictionary(item => item.ID, item => item);
			int i = 1, rowNumber = 3;
			decimal sum = 0;
		    decimal sum2 = 0;
			decimal sumCredit = 0;
			foreach (var offerListItem in Offers.OrderBy(o => o.Offer.Number))
			{
				table.Rows.Add(table.Cell(rowNumber, 1));
				table.Cell(rowNumber, 3).Range.Font.Bold = 0;
				table.Cell(rowNumber, 5).Range.Font.Bold = 0;
				table.Cell(rowNumber, 1).Range.Text = $"{i}.";
				table.Cell(rowNumber, 2).Range.Text = offerListItem.Offer.Number.ToString(_culture);
				table.Cell(rowNumber, 3).Range.Text = _banks[offerListItem.Offer.BankID].ShortName;
				table.Cell(rowNumber, 4).Range.Text = offerListItem.DepClaim.Amount.ToString("n2", _culture);
				table.Cell(rowNumber, 5).Range.Text = offerListItem.DepClaim.Rate.ToString("n2", _culture);
				table.Cell(rowNumber, 6).Range.Text = offerListItem.DepClaim.Payment.ToString("n2", _culture);
				sum += offerListItem.DepClaim.Payment;
			    sum2 += offerListItem.DepClaim.Amount;
                sumCredit += offerListItem.DepClaim.Amount;
				i++;
				rowNumber++;
			}

			Fields.Add("SUM", sum.ToString("n2", _culture));
			Fields.Add("SUM2", sum2.ToString("n2", _culture));
			Fields.Add("SUM_CREDIT", sumCredit.ToString("n2", _culture));

			base.GenerateDocument(app, doc);
		}

		private void ProcessPersons(_Document doc)
		{
			var table = doc.Tables[2];
			if (AuthorizedPerson == null)
			{
				table.Cell(1, 1).Row.Delete();
				if (Deputy == null)
				{
					table.Cell(1, 1).Row.Delete();
					if (Performer == null)
						table.Cell(1, 1).Row.Delete();
				}
				else if (Performer == null)
					table.Cell(2, 1).Row.Delete();
			}
			if (AuthorizedPerson != null && Deputy == null)
			{
				table.Cell(2, 1).Row.Delete();
				if (Performer == null)
					table.Cell(2, 1).Row.Delete();
			}
			if (AuthorizedPerson != null && Deputy != null && Performer == null)
				table.Cell(3, 1).Row.Delete();
		}
	}
}
