﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Helper
{
    /// <summary>
    /// Помощник при работе со Страховыми Взносами
    /// </summary>
    public static class DueHelper
    {
        /// <summary>
        /// Проверяем заблокированы ли возможности добавления/удаления уточнений на квартал для СВ/ДСВ
        /// </summary>
        /// <param name="addSpnId">Идентификатор СВ/ДСВ</param>
        public static bool GetIsQuarterDopOpBlocked(long addSpnId)
        {
            if (addSpnId < 1)
                throw new ArgumentException("Идентификатор не задан!");
            var addSPn = DataContainerFacade.GetByID<AddSPN>(addSpnId);
            if (addSPn == null)
                throw new Exception("Не удается найти СВ/ДСВ по идентификатору: " + addSpnId);

            var pf = addSPn.GetPortfolio();
            //пока только для ДСВ, для СВ требуются разборки с ДК
            if (pf != null && pf.TypeEl != Portfolio.Types.DSV) return false;

            //получаем список вышестоящих кварталов
            var iQuartersArray = GetQuartersArray(GetAddSPNQuarter(addSPn.MonthID), false, false);

            //если выбранное квартальное уточнение последнее, выходим
            if (iQuartersArray.Length <= 0) return false;

            var count = DataContainerFacade.GetListByPropertyConditionsCount<DopSPN>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal("DocKind", (long) DopSPN.Kinds.DueQuarter),
                ListPropertyCondition.Equal("QuarterYear", addSPn.YearID),
                ListPropertyCondition.Equal("PortfolioID", addSPn.PortfolioID),
                ListPropertyCondition.In("Quarter", iQuartersArray),
            });
            return count > 0;
        }

        /// <summary>
        /// Проверяем заблокированы ли возможности добавления/удаления уточнений на месяц для СВ/ДСВ
        /// </summary>
        /// <param name="addSpnId">Идентификатор СВ/ДСВ</param>
        public static bool GetIsAccurateMonthOpBlocked(long addSpnId)
        {
            if (addSpnId < 1)
                throw new ArgumentException("Идентификатор не задан!");
            var addSPn = DataContainerFacade.GetByID<AddSPN>(addSpnId);
            if(addSPn == null)
                throw new Exception("Не удается найти СВ/ДСВ по идентификатору: "+ addSpnId);

            var pf = addSPn.GetPortfolio();
            //пока только для ДСВ, для СВ требуются разборки с ДК
            if (pf != null && pf.TypeEl != Portfolio.Types.DSV) return false;

            var count = DataContainerFacade.GetListByPropertyConditionsCount<DopSPN>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal("DocKind", (long) DopSPN.Kinds.DueQuarter),
                ListPropertyCondition.Equal("QuarterYear", addSPn.YearID),
                ListPropertyCondition.Equal("PortfolioID", addSPn.PortfolioID),
                ListPropertyCondition.In("Quarter", GetQuartersArray(GetAddSPNQuarter(addSPn.MonthID), false)),
            });
            return count > 0;
        }

        /// <summary>
        /// Проверяем не блокируются ли поля у уточнения за квартал из-за наличия вышестоящего уточнения за квартал совпадающего по портфелю
        /// </summary>
        /// <param name="dopSpn">Уточнение за квартал</param>
        public static bool GetQDIsBlockedByQuarterDop(DopSPN dopSpn)
        {
            if (dopSpn == null) return false;
            if (dopSpn.DocKind != (long)DopSPN.Kinds.DueQuarter)
                throw new ArgumentException("Параметр dopSpn не является уточнением за квартал!");

            var pf = dopSpn.GetPortfolio();
            //пока только для ДСВ, для СВ требуются разборки с ДК
            if (pf != null && pf.TypeEl != Portfolio.Types.DSV) return false;
            //получаем нужный квартал
            var iQuarter = (int)dopSpn.Quarter;
            //получаем список вышестоящих кварталов
            var iQuartersArray = GetQuartersArray(iQuarter, false, false);

            //если выбранное квартальное уточнение последнее, выходим
            if (iQuartersArray.Length <= 0) return false;

            //ищем все квартальные уточнения, удовлетворяющие условиям http://jira.vs.it.ru/browse/DOKIPIV-422
            var count = DataContainerFacade.GetListByPropertyConditionsCount<DopSPN>(new List<ListPropertyCondition>
            {
                //квартальное уточнение
                ListPropertyCondition.Equal("DocKind", (long) DopSPN.Kinds.DueQuarter),
                //год уточнения
                ListPropertyCondition.Equal("QuarterYear", dopSpn.QuarterYear),
                //портфель уточнения
                ListPropertyCondition.Equal("PortfolioID", dopSpn.PortfolioID),
                //проверяем не только наличие уточнения за текущий квартал, но и наличие вышестоящих квартальных уточнений вплоть до 4 квартала
                ListPropertyCondition.In("Quarter", iQuartersArray)
            });
            return count > 0;
        }

        /// <summary>
        /// Проверяем не блокируются ли поля у уточнения за месяц из-за наличия уточнения за квартал совпадающее по портфелю и временному диапазону
        /// </summary>
        /// <param name="dopSpn">Уточнение за месяц</param>
        /// <param name="quarter">Квартал уточнения, если не указан посчитается сам на основании PeriodID</param>
        public static bool GetIsBlockedByQuarterDop(DopSPN dopSpn, int quarter = 0)
        {
            if (dopSpn == null) return false;
            if (dopSpn.DocKind != (long)DopSPN.Kinds.DueMonth) 
                throw new ArgumentException("Параметр dopSpn не является уточнением за месяц!");

            var pf = dopSpn.GetPortfolio();
            //пока только для ДСВ, для СВ требуются разборки с ДК
            if (pf != null && pf.TypeEl != Portfolio.Types.DSV) return false;
            //получаем нужный квартал
            var iQuarter = quarter == 0 ? GetAddSPNQuarter(dopSpn.PeriodID) : quarter;
            //ищем все квартальные уточнения, удовлетворяющие условиям http://jira.vs.it.ru/browse/DOKIPIV-422
            var count = DataContainerFacade.GetListByPropertyConditionsCount<DopSPN>(new List<ListPropertyCondition>
            {
                //квартальное уточнение
                ListPropertyCondition.Equal("DocKind", (long) DopSPN.Kinds.DueQuarter),
                //год уточнения
                ListPropertyCondition.Equal("QuarterYear", dopSpn.YearID),
                //портфель уточнения
                ListPropertyCondition.Equal("PortfolioID", dopSpn.PortfolioID),
                //проверяем не только наличие уточнения за текущий квартал, но и наличие вышестоящих квартальных уточнений вплоть до 4 квартала
                ListPropertyCondition.In("Quarter", GetQuartersArray(iQuarter, false))
            });
            return count > 0;
        }

        /// <summary>
        /// Проверяем не блокируются ли поля у ДСВ из-за наличия уточнения за квартал совпадающее по портфелю и временному диапазону
        /// </summary>
        /// <param name="addSpn">ДСВ</param>
        /// <param name="quarter">Квартал уточнения, если не указан посчитается сам на основании MonthID</param>
        public static bool GetIsBlockedByQuarterDop(AddSPN addSpn, int quarter = 0)
        {
            if (addSpn == null) return false;
            if (addSpn.Kind != (long)AddSPN.Kinds.Due)
                throw new ArgumentException("Параметр addSpn не является ДСВ!");

            var pf = addSpn.GetPortfolio();
            //пока только для ДСВ, для СВ требуются разборки с ДК
			if (pf != null && pf.TypeEl != Portfolio.Types.DSV) return false;
            //получаем нужный квартал
            var iQuarter = quarter == 0 ? GetAddSPNQuarter(addSpn.MonthID) : quarter;
            //ищем все квартальные уточнения, удовлетворяющие условиям http://jira.vs.it.ru/browse/DOKIPIV-422
            var count = DataContainerFacade.GetListByPropertyConditionsCount<DopSPN>(new List<ListPropertyCondition>
            {
                //квартальное уточнение
                ListPropertyCondition.Equal("DocKind", (long) DopSPN.Kinds.DueQuarter),
                //год ДСВ
                ListPropertyCondition.Equal("QuarterYear", addSpn.YearID),
                //портфель ДСВ
                ListPropertyCondition.Equal("PortfolioID", addSpn.PortfolioID),
                //проверяем не только наличие уточнения за текущий квартал, но и наличие вышестоящих квартальных уточнений вплоть до 4 квартала
                ListPropertyCondition.In("Quarter", GetQuartersArray(iQuarter, false))
            });
            return count > 0;
        }

        /// <summary>
        /// Выборка всех уточнений за месяц попадающих под диапазон уточнения за квартал
        /// </summary>
        /// <param name="quarterDopSpn">Уточнение за квартал</param>
        public static IList<DopSPN> GetAllMonthAccuratesForQuarter(DopSPN quarterDopSpn)
        {
            if (quarterDopSpn.DocKind != (long)DopSPN.Kinds.DueQuarter)
                throw new ArgumentException("Параметр quarterDopSpn не является уточнением за квартал!");

            //ищем все месячные уточнения, удовлетворяющие условиям http://jira.vs.it.ru/browse/DOKIPIV-422
            return DataContainerFacade.GetListByPropertyConditions<DopSPN>(new List<ListPropertyCondition> { 
                    //месячное уточнение
					ListPropertyCondition.Equal("DocKind", (long)DopSPN.Kinds.DueMonth),
                    //год уточнения
					ListPropertyCondition.Equal("YearID", quarterDopSpn.QuarterYear),
                    //портфель уточнения
					ListPropertyCondition.Equal("PortfolioID", quarterDopSpn.PortfolioID),
                    //список месяцев, попадающих под указанный квартал
					ListPropertyCondition.In("PeriodID", GetDopSPNMonthsList(quarterDopSpn.Quarter).ToArray()),
				});
        }

        /// <summary>
        /// Выборка всех нижестоящих уточнений за квартал относительно указанного
        /// </summary>
        /// <param name="quarterDopSpn">Уточнение за квартал</param>
        public static IList<DopSPN> GetAllLesserQDForQuarter(DopSPN quarterDopSpn)
        {
            if (quarterDopSpn.DocKind != (long)DopSPN.Kinds.DueQuarter)
                throw new ArgumentException("Параметр quarterDopSpn не является уточнением за квартал!");

            //получаем список нижестоящих кварталов
            var iQuartersArray = GetQuartersArray((int)quarterDopSpn.Quarter, true, false);

            //если выбранное квартальное уточнение за первый квартал, выходим
            if (iQuartersArray.Length <= 0) return new List<DopSPN>();

            //ищем все квартальные уточнения, удовлетворяющие условиям
            return DataContainerFacade.GetListByPropertyConditions<DopSPN>(new List<ListPropertyCondition>
            {
                //квартальное уточнение
                ListPropertyCondition.Equal("DocKind", (long) DopSPN.Kinds.DueQuarter),
                //год уточнения
                ListPropertyCondition.Equal("QuarterYear", quarterDopSpn.QuarterYear),
                //портфель уточнения
                ListPropertyCondition.Equal("PortfolioID", quarterDopSpn.PortfolioID),
                //проверяем наличие нижестоящих кварталов
                ListPropertyCondition.In("Quarter", iQuartersArray)
            });
        }

        /// <summary>
        /// Получаем список месяцев, которые охватывает это квартальное уточнение
        /// </summary>
        private static List<object> GetDopSPNMonthsList(int? quarter)
        {
            if(quarter == null)
                throw new ArgumentNullException("quarter","Параметру quarter не задано значение!");
            switch (quarter)
            {
                case 1: return new List<object> { 1L, 2L, 3L };
                case 2: return new List<object> { 1L, 2L, 3L, 4L, 5L, 6L};
                case 3: return new List<object> { 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L };
                case 4: return new List<object> { 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L };
                default:
                    throw new ArgumentOutOfRangeException("quarter");
            }           
        }

        /// <summary>
        /// Возвращает квартал, соответствующий указанному месяцу
        /// </summary>
        /// <param name="month">Месяц</param>
        public static int GetAddSPNQuarter(long? month)
        {
            if (month == null)
                throw new ArgumentNullException("month", "Параметру month не задано значение!");
            if(month < 1 || month > 12)
                throw new ArgumentOutOfRangeException("month");return (int)(((long) month) - 1) / 3 + 1;
        }

        /// <summary>
        /// Возвращает список кварталов на основании настроек
        /// </summary>
        /// <param name="quarter">Текущий квартал</param>
        /// <param name="isBefore">Если True - возвращает список кварталов предшествующих текущему, включая текущий, иначе последующих.</param>
        /// <param name="includeSelf">Включать в список указанный квартал</param>
        /// <returns></returns>
        public static object[] GetQuartersArray(int quarter, bool isBefore, bool includeSelf = true)
        {
            if(includeSelf)
                switch (quarter)
                {
                    case 1: return isBefore ? new object[] { 1 } : new object[] { 1, 2, 3, 4 };
                    case 2: return isBefore ? new object[] { 1, 2 } : new object[] { 2, 3, 4 };
                    case 3: return isBefore ? new object[] { 1, 2, 3 } : new object[] { 3, 4 };
                    case 4: return isBefore ? new object[] { 1, 2, 3, 4 } : new object[] { 4 };
                    default: throw new ArgumentOutOfRangeException("quarter");
                }

            switch (quarter)
            {
                case 1: return isBefore ? new object[] {  } : new object[] { 2, 3, 4 };
                case 2: return isBefore ? new object[] { 1 } : new object[] { 3, 4 };
                case 3: return isBefore ? new object[] { 1, 2 } : new object[] { 4 };
                case 4: return isBefore ? new object[] { 1, 2, 3 } : new object[] {  };
                default: throw new ArgumentOutOfRangeException("quarter");
            }
        }

    }
}
