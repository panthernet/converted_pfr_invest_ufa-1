﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Helper
{
    public static class ContractHelper
    {
        /// <summary>
        /// Активен ли договор УК на указанную дату
        /// </summary>
        /// <param name="contract">Договор УК</param>
        /// <param name="date">Дата. Если Null - берется текущая</param>
        public static bool IsContractActive(Contract contract, DateTime? date = null)
        {
            if (date == null) date = DateTime.Now;
            return (!contract.DissolutionDate.HasValue || contract.DissolutionDate > date.Value) 
                && (contract.ContractEndDate > date || (contract.ContractSupAgreementEndDate.HasValue && contract.ContractSupAgreementEndDate > date));
        }

        /// <summary>
        /// Получить дату окончания договора, с учетом доп.соглашений. Если нет соглашений о продлении, возвращает null
        /// </summary>
        /// <param name="list">Список соглашений</param>
        public static DateTime? GetContractSupAgrEndDate(IEnumerable<SupAgreement> list)
        {
            return list.Where(c => SupAgreementIdentifier.IsProlongation(c.Kind))
                .OrderByDescending(c => c.SupendDate).Select(a=> a.SupendDate).FirstOrDefault();
        }

        private static readonly long[] _siContractNameIDs = { 1, 2, 3 };
        private static readonly long[] _vrContractNameIDs = { 4, 5 };

        public static int GetContractTypeID(long contractNameID)
        {
            if (_siContractNameIDs.Contains(contractNameID))
                return (int)Document.Types.SI;
            if (_vrContractNameIDs.Contains(contractNameID))
                return (int)Document.Types.VR;
            throw new ArgumentException(string.Format("Failed to find contract type by contract name ID: {0}", contractNameID), "contractNameID");
        }
    }
}
