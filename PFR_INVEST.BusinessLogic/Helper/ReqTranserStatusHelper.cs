﻿using System;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Helper
{
    public static class ReqTranserStatusHelper
    {
        /// <summary>
        /// Откат из статуса СРН перечислены. В том числе, для частного случая при изменении и сохранении п/п из БО
        /// </summary>
        /// <param name="transfer">Перечисление</param>
        /// <param name="isTransferFromPFRToUK">Направление в УК?</param>
        /// <param name="skipPPDelete">Не удалять п/п при откате статуса</param>
        public static void RollbackStatusFromSPNTransferred(ReqTransfer transfer, bool isTransferFromPFRToUK, bool skipPPDelete = false)
        {
            if (!isTransferFromPFRToUK)//СПН перечислены -> Требование отправлено в УК
            {
                transfer.TransferStatus = TransferStatusIdentifier.sDemandSentInUK;
                transfer.PaymentOrderNumber = null;
                transfer.PaymentOrderDate = null;
                transfer.SPNDebitDate = null;
                transfer.PFRBankAccountID = null;
                transfer.PortfolioID = null;
            }
            else//СПН перечислены -> Заявка оформлена
            {
                transfer.TransferStatus = TransferStatusIdentifier.sClaimCompleted;
                transfer.PaymentOrderNumber = null;
                transfer.PaymentOrderDate = null;
                transfer.SPNDebitDate = null;
                transfer.InvestmentIncome = null;
                transfer.PFRBankAccountID = null;
                transfer.PortfolioID = null;
            }
            //удаляем связи п/п с перечислением при возврате в БО http://jira.vs.it.ru/browse/DOKIPIV-395
            //не удаляем связи, если откат произошел автоматически при удалении еденичного п/п
            if (!skipPPDelete)
            {
                ClearLinkForPP(transfer);
            }

            DataContainerFacade.Save(transfer);
        }

		/// <summary>
		/// Удаляет связь п/п с перечислением
		/// </summary>
		/// <param name="transfer">Объект перечисления</param>
	    private static void ClearLinkForPP(ReqTransfer transfer)
	    {
		    var list = transfer.GetCommonPPList();
		    list.ForEach(a => a.ClearLink());
		    DataContainerFacade.BatchSave<AsgFinTr, long>(list);
	    }

	    /// <summary>
        /// Обработка отката статуса перечисления относительно текущего статуса
        /// </summary>
        /// <param name="vm">Модель перечисления</param>
        /// <param name="viewModelsToRefresh">ВОзвращаемый список моделей для обновления</param>
        public static void RollbackStatus(ReqTransferViewModel vm, out Type[] viewModelsToRefresh)
        {
            //Требование сформировано -> Начальное состояние
            if (TransferStatusIdentifier.IsStatusRequestFormed(vm.Transfer.TransferStatus))
            {
                vm.Transfer.TransferStatus = TransferStatusIdentifier.sBeginState;
                vm.Transfer.RequestCreationDate = null;
                DataContainerFacade.Save<ReqTransfer, long>(vm.Transfer);

				// Удаляем связи п/п с перечислением http://jira.dob.datateh.ru/browse/DOKIPIV-1105
				ClearLinkForPP(vm.Transfer);
            }
            //Заявка оформлена -> Начальное состояние
            else if (TransferStatusIdentifier.IsStatusRequestRegistered(vm.Transfer.TransferStatus))
            {
                vm.Transfer.TransferStatus = TransferStatusIdentifier.sBeginState;
                vm.Transfer.Date = null;
                DataContainerFacade.Save<ReqTransfer, long>(vm.Transfer);

				// Удаляем связи п/п с перечислением http://jira.dob.datateh.ru/browse/DOKIPIV-1105
				ClearLinkForPP(vm.Transfer);
            }
            //Требование отправлено в УК -> Требование сформировано
            else if (TransferStatusIdentifier.IsStatusRequestSentToUK(vm.Transfer.TransferStatus))
            {
                vm.Transfer.TransferStatus = TransferStatusIdentifier.sDemandGenerated;
                vm.Transfer.RequestNumber = string.Empty;
                vm.Transfer.RequestFormingDate = null;
                DataContainerFacade.Save<ReqTransfer>(vm.Transfer);

				// Удаляем связи п/п с перечислением http://jira.dob.datateh.ru/browse/DOKIPIV-1105
				ClearLinkForPP(vm.Transfer);
            }
            //СПН перечислены -> 
            else if (TransferStatusIdentifier.IsStatusSPNTransfered(vm.Transfer.TransferStatus))
            {
                RollbackStatusFromSPNTransferred(vm.Transfer, vm.IsTransferFromPFRToUK);
            }
            //Требование получено УК -> СПН перечислены
            else if (TransferStatusIdentifier.IsStatusRequestReceivedByUK(vm.Transfer.TransferStatus))
            {
                vm.Transfer.TransferStatus = TransferStatusIdentifier.sSPNTransferred;
                vm.Transfer.RequestDeliveryDate = null;
                vm.Transfer.SetSPNTransferDate = null;
                DataContainerFacade.Save<ReqTransfer>(vm.Transfer);
            }
            //Акт подписан -> Требование получено УК
            else if (TransferStatusIdentifier.IsStatusActSigned(vm.Transfer.TransferStatus))
            {
                //для акта подписан требуется вызов сервиса, поэтому пока вынесен отдельно
                // в Mainwindow_Commands.cs -> RollbackStatus_Executed
                viewModelsToRefresh = null;
                return;
            }

            viewModelsToRefresh = new[]
            {
                typeof (TransfersSIArchListViewModel),
                typeof (TransfersVRArchListViewModel),
                typeof (TransfersVRListViewModel),
                typeof (TransfersSIListViewModel),
                typeof (SIAssignPaymentsListViewModel),
                typeof (VRAssignPaymentsListViewModel),
                typeof (SPNMovementsSIListViewModel),
                typeof (SPNMovementsVRListViewModel),
            };
        }
    }
}
