using System;

namespace PFR_INVEST.BusinessLogic
{
	public class PrintingEventArgs : EventArgs
	{
	    public string Message { get; }

	    public PrintingEventArgs(string message)
		{
			Message = message;
		}
	}
}