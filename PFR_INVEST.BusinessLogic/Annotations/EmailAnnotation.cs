﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.BusinessLogic.Annotations
{
    public class EmailAnnotation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value == null) return null;
            if(!(value is string)) return new ValidationResult("E-mail адрес должен быть строкой!");

            return !((string) value).Trim().Split(',', ';').All(mail => DataTools.IsEmail(mail.Trim())) ? new ValidationResult(FormatErrorMessage(validationContext.DisplayName)) : null;
        }

        public override string FormatErrorMessage(string name)
        {
            return "Неверный адрес E-mail!";
        }
    }
}
