﻿using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    /// <summary>
    /// Заготовка класса для модели списка, используемого в диалогах выбора значений
    /// Выключает внутреннее логирование действий
    /// </summary>
    public abstract class ViewModelListDialog: ViewModelList, IVmNoDefaultLogging
    {
        protected ViewModelListDialog()
        {

        }

        protected ViewModelListDialog(object param = null)
            :base(param)
        {

        }

        /// <summary>
        /// NOT USED
        /// </summary>
        protected bool IsSelectLogEnabled { get; set; }
    }
}
