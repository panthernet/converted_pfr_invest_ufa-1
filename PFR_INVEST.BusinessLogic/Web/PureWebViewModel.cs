﻿using System;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects.Attributes;

namespace PFR_INVEST.BusinessLogic.Web
{
    public abstract class PureWebFormViewModel: PureWebViewModel, IDataErrorInfo, IValidatableViewModel
    {
        public long ID { get; protected set; }

        public override long WebGetEntryId()
        {
            return !string.IsNullOrEmpty(WebLastModelError) ? 0 : ID;
        }

        #region Validation
        /// <summary>
        /// Содержит наименования полей для валидации, разделенные символом |
        /// </summary>
        protected string ValidatableFields = null;
        protected string DefaultValidationErrorMessage = "Требуется значение!";

        protected bool IsValid(string fields)
        {
            if (string.IsNullOrEmpty(fields)) return true;
            return fields.Split('|').Select(a => this[a]).Any(a => a != null);
        }

        protected string Validate(string fields)
        {
            if (string.IsNullOrEmpty(fields)) return null;
            return fields.Split('|').Select(a => this[a]).FirstOrDefault(a=> !string.IsNullOrEmpty(a));
        }
        
        public virtual string Validate()
        {
            return Validate(ValidatableFields);
        }
        #endregion

        #region IDataErrorInfo
#if WEBCLIENT
        [JsonIgnore]
#endif
        public virtual string this[string columnName] => null;

#if WEBCLIENT
        [JsonIgnore]
#endif
        public virtual string Error => LastError;
        #endregion

    }


    public abstract class PureWebDialogViewModel : PureWebViewModel, IDataErrorInfo, IValidatableViewModel
    {
#region IDataErrorInfo & Validation

        /// <summary>
        /// Содержит наименования полей для валидации, разделенные символом |
        /// </summary>
        protected string ValidatableFields = null;

        protected string DefaultValidationErrorMessage = "Требуется значение!";


#if WEBCLIENT
        [JsonIgnore]
#endif
        public virtual string this[string columnName] => null;

#if WEBCLIENT
        [JsonIgnore]
#endif
        public virtual string Error => LastError;

        public virtual string Validate()
        {
            return Validate(ValidatableFields);
        }

        protected string Validate(string fields)
        {
            if (string.IsNullOrEmpty(fields)) return null;
            return fields.Split('|').Select(a => this[a]).FirstOrDefault(a=> !string.IsNullOrEmpty(a));
        }
#endregion

        public override long WebGetEntryId()
        {
            return 0;
        }
    }

    public abstract class PureWebViewModel : WebViewModel
    {
        private ViewModelState _state = ViewModelState.Read;
        private bool? _isEditAccessDenied;

#if WEBCLIENT
        [JsonIgnore]
#endif
        public ViewModelState State
        {
            get { return _state; }
            set
            {
                if (_state == value) return;
                if (value == ViewModelState.Edit && EditAccessDenied)
                    _state = ViewModelState.Read;
                else _state = value;
            }
        }

        protected PureWebViewModel()
        {
            VerifyReadAccess();
        }

#region Access - Read

        public virtual bool IsReadOnly()
        {
            return State == ViewModelState.Read || EditAccessDenied;
        }

        private void VerifyReadAccess()
        {
            var accessDenied = true;
            var attributes = GetType().GetCustomAttributes(true).Where(attr => attr is ReadAccessAttribute).Cast<ReadAccessAttribute>();
            var roles = attributes.SelectMany(attr => attr.Roles).Distinct().ToList();
            foreach (var role in roles)
            {
                if (AP.Provider.CurrentUserSecurity.CheckUserInRole(role))
                    accessDenied = false;
            }

            //проверяем на доступ VIEW прав
            if (accessDenied)
                accessDenied = !AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);

            if (accessDenied)
                throw new ViewModelReadAccessException();
        }

#endregion

#region Access - Edit

        public virtual bool IsEditAccessDenied()
        {
            if (_isEditAccessDenied.HasValue) return _isEditAccessDenied ?? true;
            var attributes = GetType().GetCustomAttributes(true).Where(attr => attr is EditAccessAttribute).Cast<EditAccessAttribute>();
            var roles = attributes.SelectMany(attr => attr.Roles).Distinct();
            foreach (var role in roles)
            {
                if (!AP.Provider.CurrentUserSecurity.CheckUserInRole(role)) continue;
                _isEditAccessDenied = false;
                break;
            }

            return _isEditAccessDenied ?? true;
        }

        public bool IsEditAccessAllowed()
        {
            return !IsEditAccessDenied();
        }

#endregion

#region Access - Viewer

        private bool? _isUnderViewerAccess;

        /// <summary>
        /// Возвращает True если модель доступна юзеру для просмотра с учетом Viewer прав (или выше)
        /// </summary>
        public virtual bool IsUnderViewerAccess
        {
            get
            {
                if (_isUnderViewerAccess.HasValue) return _isUnderViewerAccess ?? true;

                var attributes = GetType().GetCustomAttributes(true).Where(attr => attr is ReadAccessAttribute).Cast<ReadAccessAttribute>();
                var roles = attributes.SelectMany(attr => attr.Roles).Distinct().ToList();
                _isUnderViewerAccess = AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);
                return _isUnderViewerAccess ?? true;
            }
        }

#endregion

#region Access - Delete

        private bool? _isDeleteAccessGranted;

        /// <summary>
        /// Проверка на права удаления текущей модели
        /// </summary>
        public bool IsDeleteAccessAllowed()
        {
            return IsDeleteAccess(GetType()) && State != ViewModelState.Read;
        }

        /// <summary>
        /// Проверка на права удаления текущей модели
        /// </summary>
        public bool IsDeleteAccessDenied()
        {
            return !IsDeleteAccessAllowed();
        }


        /// <summary>
        /// Проверка на права удаления типа модели
        /// </summary>
        public bool IsDeleteAccess(Type type)
		{
		    if (_isDeleteAccessGranted.HasValue) return _isDeleteAccessGranted.Value;
		    if (AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator))
		    {
		        _isDeleteAccessGranted = true;
		        return _isDeleteAccessGranted.Value;
		    }
		    var attributes = type.GetCustomAttributes(true).Where(attr => attr is DeleteAccessAttribute).Cast<DeleteAccessAttribute>();
		    var roles = attributes.SelectMany(attr => attr.Roles).Distinct();
		    if (roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role))) {
		        _isDeleteAccessGranted = true;
		        return _isDeleteAccessGranted.Value;
		    }
		    _isDeleteAccessGranted = false;

		    return _isDeleteAccessGranted.Value;
		}

	    /// <summary>
	    /// Незаивисимая проверка на право удаления модели
	    /// </summary>
	    /// <param name="type">Тип модели</param>
	    /// <param name="aType"></param>
	    public static bool CheckGlobalAccess(Type type, Type aType)
		{
			if (AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator))
				return true;
		    var attributes = type.GetCustomAttributes(true).Where(attr => attr.GetType() == aType).Cast<ViewModelAccessAttribute>();
		    var roles = attributes.SelectMany(attr => attr.Roles).Distinct();
		    return roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role));
		}

#endregion

#region Journaling
        public ModelEntityAttribute GetModelEntityAttribute()
        {
            return (ModelEntityAttribute)GetType().GetCustomAttributes(typeof(ModelEntityAttribute), false).FirstOrDefault();
        }

        public Type GetModelEntityType()
        {
            var attr =
                (ModelEntityAttribute)
                GetType().GetCustomAttributes(typeof(ModelEntityAttribute), false).FirstOrDefault();
            return attr?.Type;
        }

        public string GetModelEntityJComment()
        {
            var attr =
                (ModelEntityAttribute)
                GetType().GetCustomAttributes(typeof(ModelEntityAttribute), false).FirstOrDefault();
            return attr?.JournalDescription;
        }
#endregion
    }
}
