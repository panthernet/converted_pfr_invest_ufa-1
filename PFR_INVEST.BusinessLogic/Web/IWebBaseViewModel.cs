﻿namespace PFR_INVEST.BusinessLogic.Web
{
    public interface IWebBaseViewModel
    {
        /// <summary>
        /// Ссылка на партиал, ассоциированный с формой
        /// </summary>
        string AjaxPartial { get; }
        /// <summary>
        /// Ссылка на корневую вьюху
        /// </summary>
        string AjaxMain { get; }
        /// <summary>
        /// Загрузить скрипты для грида по умолчанию на форме
        /// </summary>
        bool AttachDefaultGridScripts { get; }
        /// <summary>
        /// Ссылка на контроллер
        /// </summary>
        string AjaxController { get; }
    }
}