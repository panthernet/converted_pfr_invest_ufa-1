﻿using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.Proxy.DataContracts;

namespace PFR_INVEST.BusinessLogic
{
    public class ACCListItem : DB2AccOperationListItem
    {
        public ACCListItem(DB2AccOperationListItem ent)
        {
            this.ACCOUNT = ent.ACCOUNT;
            this.ACCOUNT_TYPE = ent.ACCOUNT_TYPE;
            this.CURRENCY = ent.CURRENCY;
            this.DATE = ent.DATE;
            this.ID = ent.ID;
            this.KIND = ent.DOCKIND == 3 ? ent.CONTENT : ent.KIND;
            this.LEGALENTITYNAME = ent.LEGALENTITYNAME;
            this.PORTFOLIO = ent.PORTFOLIO;
            this.IsRUB = CurrencyIdentifier.IsRUB(this.CURRENCY);

            this.SUMM = ent.SUMM;
            this.SUMMRUBLE = ent.SUMMRUBLE;
            this.DOCKIND = ent.DOCKIND;
        }

        public bool IsRUB { get; private set; }

        public string Quarter => DateTools.GetQarterYearInWordsForDate(DATE);

        public string Month => DateTools.GetMonthInWordsForDate(DATE);

        public string Date => this.DATE.ToShortDateString();

        public decimal? CurrSumm => !this.IsRUB ? (decimal?)this.SUMM : null;

        public decimal? RubSumm => (decimal?)this.SUMMRUBLE;
    }
}
