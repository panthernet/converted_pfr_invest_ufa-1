﻿using System;

using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class BudgetCorrectionListItem
    {
        private BudgetCorrection m_BudgetCorrection = null;
        private decimal? m_PreviousSum = null;

        public BudgetCorrectionListItem(BudgetCorrection _BudgetCorrection, decimal? prevSum)
        {
            if (_BudgetCorrection.Sum.HasValue && prevSum.HasValue)
                m_Modification = _BudgetCorrection.Sum - prevSum;
            m_BudgetCorrection = _BudgetCorrection;
            m_PreviousSum = prevSum;
        }

        public long ID => m_BudgetCorrection.ID;

        public decimal? Total => m_BudgetCorrection.Sum;

        private decimal? m_Modification = null;
        public decimal? Modification => m_Modification;

        public DateTime? Date
        {
            get
            {
                if (m_BudgetCorrection != null)
                    return m_BudgetCorrection.Date;
                return null;
            }
        }
    }

    public class BudgetListItem
    {
        private Budget m_Budget = null;

        public BudgetListItem(Budget _Budget)
        {
            m_Budget = _Budget;
        }

        public long ID => m_Budget.ID;

        public string Name => m_Budget.GetPortfolio().Year;
    }
}
