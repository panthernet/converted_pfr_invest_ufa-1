﻿using System;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class CostsListItem
    {
        public CostsListItem(Cost val)            
        {
            this.ID = val.ID;
            var portfolio = val.GetPortfolio();
            var curr = val.GetCurrency();

            Portfolio = portfolio.Year;
            Currency = curr.Name;

            Kind = val.Kind;
            RegNum = val.PaymentNumber;            
            DateValue = val.PaymentDate;
            {
                if (Currency.Trim().ToLower().Contains("рубл"))
                    CurrSumm = string.Empty;
                else
                {
                    CurrSumm = (val.Sum ?? 0).ToString();
                }
            }

            {               
                RubSumm = val.Total;
            }

            {               
                SumPP = val.PaymentSumm;
            }

        }

        public long ID { get; private set; }
        public string Portfolio { get; private set; }
        public string Kind { get; private set; }
        public string Currency { get; private set; }
        public string RegNum { get; private set; }

        public int? MonthValue
        {
            get
            {
                if (DateValue.HasValue) return DateValue.Value.Month;
                else return 0;
            }
        }

        public string Quarter
        {
            get
            {
                string quarterStr = " квартал";
                switch (MonthValue)
                {
                    case 1:
                    case 2:
                    case 3:
                        return "I" + quarterStr;
                    case 4:
                    case 5:
                    case 6:
                        return "II" + quarterStr;
                    case 7:
                    case 8:
                    case 9:
                        return "III" + quarterStr;
                    case 10:
                    case 11:
                    case 12:
                        return "IV" + quarterStr;
                }
                return "";
            }
        }
       
        public int? Year
        {
            get
            {
                if (DateValue.HasValue)
                    return DateValue.Value.Year;
                else
                    return null;
            }
        }
        public string DateText
        {
            get
            {
                if (DateValue.HasValue)
                    return DateValue.Value.ToShortDateString();
                else
                    return string.Empty;
            }
        }
        public DateTime? DateValue { get; private set; }       
        public string MonthText
        {
            get
            {
                if (DateValue.HasValue)
                    return DateTools.GetMonthInWordsForDate(DateValue.Value);
                else
                    return string.Empty;
            }
        }

        public string CurrSumm { get; private set; }
        public decimal? RubSumm { get; private set; }
        public decimal? SumPP { get; private set; }
    }
}
