﻿using System;

namespace PFR_INVEST.BusinessLogic
{
    public class PrepaymentAccurateListItem : BaseListItem
    {
        private decimal prevItogo;

        public PrepaymentAccurateListItem(Proxy.DBEntity dbEn, long num, Decimal _prevItogo)
            : base(dbEn)
        {
            this.NUM = num;
            prevItogo = _prevItogo;
        }

        public PrepaymentAccurateListItem(Proxy.DBEntity dbEn)
            : base(dbEn)
        {
        }

        public long NUM
        {
            get;
            set;
        }

        public long ID => this.GetLongField("ID");

        public string RegNumber => this.GetStringField("REGNUM");

        public decimal Summ => this.GetDecimalField("SUMM");

        public decimal Diff
        {
            get
            {
                return Summ - prevItogo;
                //return this.GetDecimalField("CHSUMM");
            }
            set
            {
                prevItogo = value;
            }
        }

        public object Date
        {
            get
            {
                DateTime d = this.GetDateTimeField("DATE");
                return d.Year < 1900 ? null : d.ToShortDateString();
            }
        }

    }
}
