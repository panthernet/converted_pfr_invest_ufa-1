﻿namespace PFR_INVEST.BusinessLogic
{
    public class F24ListItem
    {
        public F24ListItem(string field, decimal? total)
        {
            this.Field = field;
            this.Total = total ?? 0.00m;
        }

        public string Field { get; set; }
        public decimal? Total { get; set; }
    }
}
