﻿
namespace PFR_INVEST.BusinessLogic
{
    public class F20ListItem
    {
        public F20ListItem(string field, decimal? current, decimal? pie, decimal? start)
        {
            this.Field = field;
            this.Current = current ?? 0.00m;
            this.Pie = pie ?? 0.00m;
            this.Start = start ?? 0.00m;
        }

        public string Field { get; set; }
        public decimal? Current { get; set; }
        public decimal? Pie { get; set; }
        public decimal? Start { get; set; }
    }
}
