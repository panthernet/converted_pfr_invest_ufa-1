﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using Application = Microsoft.Office.Interop.Excel.Application;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class OpfrTransfersImportHandler
    {
        public List<OpfrTransferImportListItem> Items { get; set; }

        public string OpenFileMask => "Файлы Excel (*.xls, *.xlsx)|*.xls;*.xlsx";

        private readonly long _registerId;
        private readonly List<PFRBranch> _branchList;
        private readonly List<OpfrCost> _costs;
        public OpfrTransfersImportHandler(long regId)
        {
            _registerId = regId;
            _branchList = DataContainerFacade.GetList<PFRBranch>();
            _costs = DataContainerFacade.GetList<OpfrCost>();
            Items = new List<OpfrTransferImportListItem>();
        }

        /// <summary>
        /// Предварительный импорт данных из файла в память
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <param name="message">Исходящее сообщение</param>
        public bool LoadFile(string fileName, out string message)
        {
            message = "";
            try
            {
                Application excelApp = null;
                Workbook workbook = null;
                Worksheet worksheet = null;
                object oMissing = System.Reflection.Missing.Value;
                try
                {
                    excelApp = new Application { Visible = false, DisplayAlerts = false};
                    workbook = excelApp.Workbooks.Open(fileName);
                    worksheet = (Worksheet)workbook.Sheets[1];

                    var firstStr = Convert.ToString( ((Range) worksheet.UsedRange.Cells[14, 2]).Value);
                    var secStr = Convert.ToString(((Range) worksheet.UsedRange.Cells[15, 2]).Value);
                    if (!(firstStr ?? "").ToLower().Contains("распорядитель") || !(secStr ?? "").Contains("2"))
                    {
                        message = "Неверный формат файла!";
                        return false;
                    }
                    //var rows = new List<XLSRow>();
                    for (var i = 17; i <= worksheet.UsedRange.Rows.Count; i++)
                    {
                        var items = new object[6];
                        for (var j = 1; j <= 6; j++)
                            items[j-1] = ((Range)worksheet.UsedRange.Cells[i, j]).Value;
                        int res;
                        if (items[0] == null) continue;
                        if(!int.TryParse(items[0].ToString(), out res)) continue;

                        var item = new OpfrTransferImportListItem
                        {
                            Opfr = items[1].ToString(),
                            Kosgu261 = Convert.ToDecimal(items[2]),
                            Kosgu221 = Convert.ToDecimal(items[3]),
                            Kosgu226 = Convert.ToDecimal(items[4])
                        };
                        var branch =
                            _branchList.FirstOrDefault(a => a.Name.Trim().ToLower() == item.Opfr.Trim().ToLower());
                        if (branch == null)
                        {
                            message = "В БД не найден ОПФР с наименованием " + item.Opfr;
                            return false;
                        }
                        item.BranchID = branch.ID;
                        Items.Add(item);
                    }

                }
                finally
                {
                    if (worksheet != null)
                        Marshal.FinalReleaseComObject(worksheet);

                    if (workbook != null)
                    {
                        workbook.Close(false, oMissing, oMissing);
                        Marshal.FinalReleaseComObject(workbook);
                    }
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.FinalReleaseComObject(excelApp);
                    }
                }
                return Items != null && Items.Count > 0;
            }
            catch(Exception ex)
            {
                message = ex.Message;
                Logger.Instance.Fatal("Import OPFR transfer", ex);
                return false;
            }
        }

        public bool CanExecuteImport()
        {
            return Items != null && Items.Count > 0;
        }

        public bool CanExecuteOpenFile()
        {
            return true;
        }

        /// <summary>
        /// Запуск операции импорта
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message">Исходящее сообщение</param>
        public bool ExecuteImport(string filename, out string message)
        {
            var result = Import(filename, out message);
            Clear();            
            return result;
        }

        #region Virtual props for wizard
        private List<OpfrCost> _virtualCosts;
        private List<PFRBranch> _virtualBranches;
        public ObservableCollection<OpfrTransferListItem> ResultTransfersList { get; set; } = new ObservableCollection<OpfrTransferListItem>();
        public string ResultFileName;
        #endregion

        internal bool Import(OpfrRegister register, string filename, out string message, bool isVirtual = false)
        {
            try
            {
                var transfers = new List<OpfrTransfer>();
                for (int i = 0; i < Items.Count; i++)
                {
                    var wItem = Items[i];
                    var cost = _costs.FirstOrDefault(a => a.Name.EndsWith("221"));
                    if (cost == null)
                    {
                        message = "Не найден КОСГУ 221";
                        return false;
                    }
                    var tr = new OpfrTransfer
                    {
                        OpfrRegisterID = _registerId,
                        Sum = wItem.Kosgu221,
                        StatusID = 1,
                        PfrBranchID = wItem.BranchID,
                        OpfrCostID = cost.ID,
                        DIDate = DateTime.Now.Date,
                        PortfolioID = register.ImportPortfolioID,
                        Status = (long)Element.SpecialDictionaryItems.OpfrTransferStatusInitial
                    };
                    transfers.Add(tr);

                    cost = _costs.FirstOrDefault(a => a.Name.EndsWith("226"));
                    if (cost == null)
                    {
                        message = "Не найден КОСГУ 226";
                        return false;
                    }
                    tr = new OpfrTransfer
                    {
                        OpfrRegisterID = _registerId,
                        Sum = wItem.Kosgu226,
                        StatusID = 1,
                        PfrBranchID = wItem.BranchID,
                        OpfrCostID = cost.ID,
                        DIDate = DateTime.Now.Date,
                        PortfolioID = register.ImportPortfolioID,
                        Status = (long)Element.SpecialDictionaryItems.OpfrTransferStatusInitial
                    };
                    transfers.Add(tr);

                    cost = _costs.FirstOrDefault(a => a.Name.EndsWith("261"));
                    if (cost == null)
                    {
                        message = "Не найден КОСГУ 261";
                        return false;
                    }
                    tr = new OpfrTransfer
                    {
                        OpfrRegisterID = _registerId,
                        Sum = wItem.Kosgu261,
                        StatusID = 1,
                        PfrBranchID = wItem.BranchID,
                        OpfrCostID = cost.ID,
                        DIDate = DateTime.Now.Date,
                        PortfolioID = register.ImportPortfolioID,
                        Status = (long)Element.SpecialDictionaryItems.OpfrTransferStatusInitial
                    };
                    transfers.Add(tr);

                }

                if (!isVirtual)
                    return ExecuteImportPart(register, transfers, filename, out message);

                message = null;
                _virtualCosts = _virtualCosts ?? DataContainerFacade.GetList<OpfrCost>();
                _virtualBranches = _virtualBranches ?? DataContainerFacade.GetList<PFRBranch>();
                long count = 1;
                ResultTransfersList.Clear();
                new ObservableCollection<OpfrTransferListItem>(
                        transfers.Select(
                            a =>
                                new OpfrTransferListItem(a, _virtualCosts.FirstOrDefault(b => b.ID == a.OpfrCostID)?.Name,
                                    _virtualBranches.FirstOrDefault(b => b.ID == a.PfrBranchID)?.Name)
                                {
                                    ID = count++
                                }))
                                    .ForEach(a=> ResultTransfersList.Add(a));

                ResultFileName = filename;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
            return true;
        }

        public static bool ExecuteImportPart(OpfrRegister register, List<OpfrTransfer> transfers, string filename, out string message)
        {
            var idList = DataContainerFacade.BatchSave<OpfrTransfer, long>(transfers, true);
            var fId = SaveFileToRepo(filename, out message);
            if (fId == -1) return false;
            if (!string.IsNullOrEmpty(message))
                return false;
            if (register.FileID.HasValue)
                DataContainerFacade.Delete<RepositoryNoAucImpExpFile>(register.FileID.Value);
            register.FileID = fId;
            DataContainerFacade.Save(register);
            //обновление для подписчиков
            var type = typeof(OpfrTransfer);
            ViewModelBase.ViewModelManager.UpdateDataInAllModels(idList.Select(a => new KeyValuePair<Type, long>(type, a)).ToList());
            return true;
        }

        private bool Import(string filename, out string message)
        {
            var register = DataContainerFacade.GetByID<OpfrRegister>(_registerId);
            return Import(register, filename, out message);
        }

        /// <summary>
        /// Сохранение файлов в репозиторий импорта/экспорта
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <param name="message">Исходящее сообщение</param>
        private static long SaveFileToRepo(string filename, out string message)
        {
            message = null;
            try
            {
                var dt = DateTime.Now;
                var r = new RepositoryNoAucImpExpFile
                {
                    DDate = dt,
                    ImpExp = 1, //импорт
                    Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(filename)),
                    Status = 1,
                    TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
                    UserName = AP.Provider.UserName,
                    Comment = "Работа с ОПФР - Распоряжения Правления - Импорт",
                    Key = (int)RepositoryImpExpFile.Keys.OpfrImport

                };
                return DataContainerFacade.Save(r);

            }catch(Exception ex)
            {
                message = $"Ошибка при сохранении файла '{filename}' в репозиторий!\n{ex.Message}";
                return -1;
            }
        }

        public void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(OpfrListViewModel));            
        }


        internal void Clear()
        {
            Items.Clear();
        }
    }
}
