﻿
namespace PFR_INVEST.BusinessLogic.SIReport
{
    using System;
    using System.IO;

    using Microsoft.Office.Interop.Excel;
    using DataObjects;

    public abstract class NPFImportHandlerBase
    {
        //public abstract string ReportType { get; }//Тип (код) отчета

        public abstract string OpenFileMask { get; } //Маска для выбора файла

        public abstract bool CanExecuteImport();

        public abstract bool ExecuteImport(out string message); //Импорт

        public abstract bool IsValidateDoc(Worksheet worksheet, out string message); //Валидация и заполнение модели

        public virtual void RefreshViewModels() { }

        public virtual bool CanExecuteOpenFile() { return false; }

        public virtual Month SelectedMonth { get; set; }

        public virtual Year SelectedYear { get; set; }

        public virtual string HeaderNameReport { get; set; }
      
        public virtual string ReportName { get; set; }

        protected virtual string FileName { get; private set; }

        //Загрузка данных из файла в память
        public virtual bool LoadFile(string fileName, out string message)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException($"Файл {fileName} не был найден.");
            FileName = fileName;

            bool result;
            message = null;

            Application excelApp = null;
            Workbook workbook = null;

            try
            {
                excelApp = new Application { Visible = false };
                workbook = excelApp.Workbooks.Open(fileName);
                var worksheet = (Worksheet)workbook.Sheets[1];
                result = IsValidateDoc(worksheet, out message);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                result = false;
            }
            finally
            {
                if (excelApp != null)
                {
                    for (var i = 0; i < excelApp.Sheets.Count; i++)
                    {
                        var sheet = (Worksheet)excelApp.Sheets[i + 1];
                        ReleaseComObject(sheet);
                    }

                    excelApp.Workbooks.Close();
                    for (var i = 0; i < excelApp.Workbooks.Count; i++)
                    {
                        var w = excelApp.Workbooks[i + 1];
                        ReleaseComObject(w);
                    }

                    if (workbook != null)
                        ReleaseComObject(workbook);
                    excelApp.Quit();
                    ReleaseComObject(excelApp);
                }
            }
            return result;
        }

        private static void ReleaseComObject(object comObject)
        {
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(comObject);
        }
    }
}
