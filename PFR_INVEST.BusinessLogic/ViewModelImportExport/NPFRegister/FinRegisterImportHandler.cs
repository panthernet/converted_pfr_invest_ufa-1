﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    public class FinRegisterImportHandler : NPFImportHandlerBase
	{
		protected Register Register { get; set; }

		public override string OpenFileMask => "Реестр СПН(.xls, .xlsx)|*.xls;*.xlsx";

	    public List<Tuple<Finregister, LegalEntity, bool>> Items { get; set; } = new List<Tuple<Finregister, LegalEntity, bool>>();

	    public List<LegalEntity> NPFList { get; set; } = new List<LegalEntity>();

	    private bool _canExecuteImport;

		public override bool CanExecuteImport()
		{
			return _canExecuteImport;
		}

		public override bool CanExecuteOpenFile()
		{
			return true;
		}

		public override bool ExecuteImport(out string message)
		{
			var result = Import(out message);
			if (result) RefreshViewModels();

			Items = null;
			_canExecuteImport = false;
			return result;
		}

		public bool Import(out string message)
		{
			try
			{
				//Выставляем ЧФР равно 0 если не задано
				Items.Where(x => x.Item3).ToList().ForEach(x => x.Item1.CFRCount = x.Item1.CFRCount ?? 0.00m);
				Items.Where(x=> x.Item3).ToList().ForEach(x => x.Item1.ID = DataContainerFacade.Save<Finregister, long>(x.Item1));
				message = $"Импортировано записей: {Items.Count(x => x.Item3)}";
			}
			catch (Exception ex)
			{
				message = ex.Message;
				return false;
			}

			return true;
		}

		public override bool IsValidateDoc(Worksheet worksheet, out string message)
		{
			_canExecuteImport = false;

			//импоррт данных в класс
			if (!SetDataImport(worksheet, out message))
			{
				//ViewModelBase.DialogHelper.Alert(message);
                Items = new List<Tuple<Finregister, LegalEntity, bool>>();                
				return false;
			}

			if (!ValidateUniqueFinRegister(out message))
			{
				return false;
			}

			message = null;
			_canExecuteImport = true;
			return true;
		}

		public FinRegisterImportHandler(long registerId)
		{
			Register = DataContainerFacade.GetByID<Register, long>(registerId);
		}

		private bool ValidateUniqueFinRegister(out string message)
		{
			var duplicateItems = Items.Select(x => x.Item2.INN).GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key);
			if (duplicateItems.Any())
			{
				message = $"В импортируемом файле найдены одинаковые записи ИНН: {duplicateItems.Select(x => $"\"{x}\"").Aggregate((x, y) => $"{x}, {y}")}";
				return false;
			}

			var existsItems = DataContainerFacade.GetListByProperty<Finregister>("RegisterID", Register.ID);
			// выбор записей, которые уже были импортированы (ИНН и ID НПФ для финреестров не хранятся, поэтому сопоставление косвенное по лицевому счету)
			var errorRecords = from i in Items
							   join e in existsItems on new { i.Item1.CrAccID, i.Item1.DbtAccID } equals new { e.CrAccID, e.DbtAccID }
							   select i;
			if (errorRecords.Any())
			{
				message = $"Обнаружены записи, которые были занесены ранее в тот же реестр НПФ: {errorRecords.Select(x => $"\"{x.Item2.INN}\"").Aggregate((x, y) => $"{x}, {y}")}";
				return false;
			}

			message = null;
			return true;
		}

		/// <summary>
		/// получим данные из файла для импорта
		/// </summary>
		/// <param name="worksheet"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		private bool SetDataImport(Worksheet worksheet, out string message)
		{
			NPFList = BLServiceSystem.Client.GetNPFListLEHib(StatusFilter.NotDeletedFilter).ToList();

			Items = new List<Tuple<Finregister, LegalEntity, bool>>();

			int iterator = 1;
			var end = worksheet.Range["A" + iterator].Value2;

			var accPFR = BLServiceSystem.Client.GetPFRRoubleAccount();
			if (accPFR == null)
				throw new Exception("Лиц счет ПФР не найден");

			while (end != null)
			{
				var inn = worksheet.Range["A" + iterator].Value2.ToString();

				//Обрезаем текст, если по ошибке вычитало не ИНН а что-то другоее длинное
				var sINN = inn;
				if (sINN.Length > 50)
					sINN = sINN.Substring(0, 50) + "...";
				var npf = NPFList.FirstOrDefault(x => x.INN == inn);
				if (npf == null)
				{

					message = string.Format("Не удалось найти НПФ с заданным ИНН ({0})", sINN);
					return false;
				}
			    bool isValid = true;

				decimal sum;
				if (!decimal.TryParse(worksheet.Range["B" + iterator].Value2.ToString(), out sum))
				{
					message = string.Format("Не удалось прочитать значение суммы СПН ({0}) для НПФ с ИНН ({1})", worksheet.Range["B" + iterator].Value2.ToString(), sINN);
					return false;
				}

			    var tCount = sum.ToString(System.Globalization.CultureInfo.InvariantCulture)
			        .TrimEnd('0')
			        .SkipWhile(c => c != '.')
			        .Skip(1)
			        .Count();
			    if (tCount > 2)
			    {
			        isValid = false;
			    }

                long zl;
				if (!long.TryParse(worksheet.Range["C" + iterator].Value2.ToString(), out zl))
				{
					message = string.Format("Не удалось прочитать значение количества ЗЛ ({0}) для НПФ с ИНН ({1})", worksheet.Range["C" + iterator].Value2.ToString(), sINN);
					return false;
				}

			    var fr = new Finregister
			    {
			        RegisterID = Register.ID,
			        ZLCount = zl,
			        Count = sum,
			        Status = RegisterIdentifier.FinregisterStatuses.Created
			    };
			    if (Register.RegDate.HasValue && !string.IsNullOrWhiteSpace(Register.RegNum))
				{
					fr.Date = Register.RegDate;
					fr.RegNum = Register.RegNum;
				}

				var accNPF = BLServiceSystem.Client.GetContragentRoubleAccount(npf.ContragentID.Value);

				if (accNPF == null)
					throw new Exception("Лиц счет НПФ не найден");

				if (RegisterIdentifier.IsToNPF(Register.Kind.ToLower()) || RegisterIdentifier.IsTempAllocation(Register.Kind.ToLower()))
				{
					fr.CrAccID = accPFR.ID;
					fr.DbtAccID = accNPF.ID;
				}
				else
				{
					fr.CrAccID = accNPF.ID;
					fr.DbtAccID = accPFR.ID;
				}

				Items.Add(new Tuple<Finregister, LegalEntity, bool>(fr, npf, isValid));

				end = worksheet.Range["A" + (++iterator)].Value2;
			}

			return IsImportFilled(out  message);
		}

		private bool IsImportFilled(out string message)
		{

			if (Items.Count == 0 || Items.All(a=> !a.Item3))
			{
				message = "Импорт невозможен.\nОтсутствуют данные для импорта.";
				return false;
			}

			message = null;
			return true;
		}

		public override void RefreshViewModels()
		{
			ViewModelBase.ViewModelManager.RefreshViewModels(typeof(RegistersListViewModel));
			ViewModelBase.ViewModelManager.RefreshViewModels(typeof(NPFTempAllocationListViewModel));
		}
	}
}

