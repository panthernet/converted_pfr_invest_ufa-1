﻿namespace PFR_INVEST.BusinessLogic.XLSModels
{
    public class XLSDocumentKOImport761 : XLSDocumentBase
    {
        public const string NUM = "№ п/п";
        public const string BANK = "Наименование банка";
        public const string REGNUM = "Рег.№";

        public XLSDocumentKOImport761(string fileName) : base(fileName) { }

        protected override bool CanContainExtraColumns => false;

        protected override bool CanContainEmptyColumns => false;

        protected override string[] GetColumnNames()
        {
            return new string[] { NUM, BANK, REGNUM };
        }

        protected override string[] GetNonEmptyColumns()
        {
            return new string[] { NUM, BANK, REGNUM };
        }


    }

}
