﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace PFR_INVEST.BusinessLogic.XLSModels
{
    public class XLSColumn
    {
        public string Name { get; set; }
    }

    public class XLSRow<T>
    {
        public IDictionary<T, object> Items { get; set; }

        public XLSRow()
        {
            Items = new Dictionary<T, object>();
        }
    }

    public abstract class XLSDocumentBase
    {
        Application excelApp;
        Workbook Workbook;
        Worksheet worksheet;

        public IList<XLSColumn> Columns { get; private set; }
        public IList<XLSRow<string>> Rows { get; private set; }

        public XLSDocumentBase(string fileName)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException($"Файл {fileName} не был найден");

            excelApp = new Application { Visible = false };
            Workbook = excelApp.Workbooks.Open(fileName);
            worksheet = (Worksheet)Workbook.Sheets[1];

            LoadData();
        }

        private void LoadData()
        {
            try
            {
                PopulateColumns();
                ValidateColumns();

                PopulateRows();
                ValidateRows();
            }
            finally
            {
                if (excelApp != null)
                {
                    for (int i = 0; i < excelApp.Sheets.Count; i++)
                    {
                        Worksheet sheet = (Worksheet)excelApp.Sheets[i + 1];
                        ReleaseComObject(sheet);
                        sheet = null;
                    }

                    excelApp.Workbooks.Close();
                    for (int i = 0; i < excelApp.Workbooks.Count; i++)
                    {
                        Workbook workbook = excelApp.Workbooks[i + 1];
                        ReleaseComObject(workbook);
                        workbook = null;
                    }

                    ReleaseComObject(Workbook);
                    Workbook = null;
                    excelApp.Quit();
                    ReleaseComObject(excelApp);
                    excelApp = null;
                }
            }
        }

        private static void ReleaseComObject(object comObject)
        {
            int count = 0;
            while (count == 0)
            {
                count = System.Runtime.InteropServices.Marshal.ReleaseComObject(comObject);
            }
        }
        /// <summary>
        /// Свойство отвечает за то, что документ может содержать пустые столбцы 
        /// </summary>
        protected abstract bool CanContainEmptyColumns { get; }
        /// <summary>
        /// Свойство отвечает за то, что документ может содержать дополнительные столбцы 
        /// </summary>
        /// <returns></returns>
        protected abstract bool CanContainExtraColumns { get; }

        /// <summary>
        /// Функция возвращает список столбцов, котрые должны присутствовать в документе
        /// </summary>
        /// <returns></returns>
        protected abstract string[] GetColumnNames();
        
        /// <summary>
        /// Функция возвращает список столбцов, котороые должны содержать данные
        /// </summary>
        /// <returns></returns>
        protected abstract string[] GetNonEmptyColumns();

        private void PopulateColumns()
        {
            Columns = new List<XLSColumn>();
            IList<string> columns = new List<string>();

            if(worksheet.UsedRange.Columns.Count == 0)
                throw new Exception("Пустой документ.");

            for (int i = 1; i <= worksheet.UsedRange.Columns.Count; i++) 
            {
                var column = (string)((Range)worksheet.UsedRange.Cells[1, i]).Value;
                if (!string.IsNullOrEmpty(column))
                    column = column.Trim();
                Columns.Add(
                    new XLSColumn
                    {
                        Name = column
                    });
            }

        }

        private void ValidateColumns()
        {
            IList<string> columns = new List<string>(GetColumnNames().Select(a=> a.Trim()));

            foreach (var column in Columns)
            {
                if (string.IsNullOrWhiteSpace(column.Name))
                {
                    if (CanContainEmptyColumns)
                        continue;
                    throw new Exception("Документ не может содержать пустые столбцы.");
                }

                if(Array.Exists(columns.ToArray(), c => c.Equals(column.Name)))
                {
                    columns.Remove(column.Name);
                }
                else
                {
                    if (!CanContainExtraColumns)
                        throw new Exception($"Найден столбец '{column.Name}', который не должен содержаться в документе.");
                }
            }


            if (columns.Count > 0)
                throw new Exception("Документ не содержит все обязательные столбцы.");

        }

        private void PopulateRows()
        {
            Rows = new List<XLSRow<string>>();

            for (int i = 2; i <= worksheet.UsedRange.Rows.Count; i++)
            {
                var row = new XLSRow<string>
                {
                    Items = new Dictionary<string, object>()
                };

                for (int j = 1; j <= worksheet.UsedRange.Columns.Count; j++)
                {
                    row.Items[((Range)worksheet.UsedRange.Cells[1, j]).Value.ToString()] = ((Range)worksheet.UsedRange.Cells[i, j]).Value;
                }

                Rows.Add(row);
            }
        }

        private void ValidateRows()
        {
            if (Rows == null || Rows.Count == 0)
                throw new Exception("Пустой документ.");

            string[] mandatoryColumns = GetNonEmptyColumns();

            foreach (var row in Rows)
            {
                if(row.Items == null)
                    throw new Exception("Документ не может содержать пустые строки.");

                foreach (var column in mandatoryColumns)
                {
                    if (row.Items[column] == null)
                        throw new Exception(string.Format("Столбец '{0}' не может содержать пустые значения.", column));
                }
            }
        }
    }

}
