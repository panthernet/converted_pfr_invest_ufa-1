﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class RejAppImportHandler
    {
        private readonly List<string> _filenames = new List<string>();
        private bool _canExecuteImport;

        private List<RejectApplication> _items = new List<RejectApplication>();
        public List<RejectApplication> Items
        {
            get { return _items; }
            set
            {
                _items = value;
            }
        }

        public string OpenFileMask => $"Отказные заявления (.csv)|{"*.csv"}";

        /// <summary>
        /// Предварительный импорт данных из файла в память
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <param name="message">Исходящее сообщение</param>
        public bool LoadFile(string fileName, out string message)
        {
            try
            {
                _canExecuteImport = false;      
                message = null;
                var branches = DataContainerFacade.GetList<PFRBranch>(false);
                var docTypes = DataContainerFacade.GetList<NpfDocType>(false);
                var codes = DataContainerFacade.GetList<NPFErrorCode>(false);
                var index = _filenames.Count; //запоминаем индекс имени файла для дальнейшего сопоставления при записи в хранилище
                
                //var tmp = DataContainerFacade.GetList<RejectApplication>();
                using (var sr = new StreamReader(fileName, Encoding.GetEncoding("Windows-1251"), true))
                {
                    var content = sr.ReadToEnd();
                    var strings = content.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var a in strings)
                    {

                        var arr = a.Split(new[] {';'}, StringSplitOptions.None);
                        Array.ForEach(arr, x => arr[Array.IndexOf(arr, x)] = x.Trim());
                        if (arr.Length < 8)
                        {
                            message = "Неверный формат входных данных!";
                            return false;
                        }
                        RejectApplication ra;
                        try
                        {
                            ra = new RejectApplication()
                            {
                                RegNum = arr[0],
                                RegDate = string.IsNullOrEmpty(arr[1]) ? null : (DateTime?)DateTime.ParseExact(arr[1], "dd.MM.yyyy", CultureInfo.InvariantCulture),
                                SelectionIdentifier = string.IsNullOrEmpty(arr[2]) ? null : arr[2],
                                DocType = string.IsNullOrEmpty(arr[3]) ? null : arr[3],
                                ErrorCode = string.IsNullOrEmpty(arr[4]) ? null : arr[4],
                                FromIdentifier = string.IsNullOrEmpty(arr[5]) ? null : arr[5],
                                Portfolio = string.IsNullOrEmpty(arr[6]) ? null : arr[6],
                                ContractDockCode = string.IsNullOrEmpty(arr[7]) ? null : arr[7],
                                FileId = index //временно (до импорта) храним индекс в списке имен, вместо реального Id
                            };
                        }
                        catch 
                        {
                            message = "Неверный формат входных данных!!";
                            return false;
                        }

                        if(string.IsNullOrEmpty(ra.RegNum))
                        {
                            message = string.Format("У отказного заявления отсутствует регистрационный номер!");
                            return false;
                        }

                        //проверка на уникальность рег номера
                        if(DataContainerFacade.GetListByPropertyCount<RejectApplication>("RegNum", ra.RegNum) > 0)
                        {
                            message = $"Отказное заявление с регистрационным номером {ra.RegNum} уже существует!";
                            return false;
                        }

                        //проверка наличия идентификатора НПФ (также на удаленные)
                        if (!string.IsNullOrEmpty(ra.SelectionIdentifier))
                        {
                            var lrlist = DataContainerFacade.GetListByPropertyConditions<LegalEntityIdentifier>(new List<ListPropertyCondition>()
                            {
                                ListPropertyCondition.Equal("Identifier", ra.SelectionIdentifier),
                                ListPropertyCondition.NotEqual("StatusID", -1L)
                            });
                            if (lrlist.Count < 1)
                            {
                                message = $"В БД отсутствует идентификатор {ra.SelectionIdentifier} для отказа с рег. номером {ra.RegNum}!";
                                return false;
                            } else ra.SelectionName = lrlist[0].LegalEntityName;
                        }

                        //проверка наличия идентификатора НПФ (также на удаленные)
                        if (!string.IsNullOrEmpty(ra.FromIdentifier))
                        {
                            var lrlist = DataContainerFacade.GetListByPropertyConditions<LegalEntityIdentifier>(new List<ListPropertyCondition>()
                            {
                                ListPropertyCondition.Equal("Identifier", ra.FromIdentifier),
                                ListPropertyCondition.NotEqual("StatusID", -1L)
                            });

                            if (lrlist.Count < 1)
                            {
                                message = $"В БД отсутствует идентификатор {ra.FromIdentifier} для отказа с рег. номером {ra.RegNum}!";
                                return false;
                            } else ra.FromName = lrlist[0].LegalEntityName;
                        }
                        // проверка наличия типа заявления
                        if(!string.IsNullOrEmpty(ra.DocType) && docTypes.FirstOrDefault(d => d.DocType == ra.DocType) == null)
                        {
                            message = $"В БД отсутствует тип заявления {ra.DocType} для отказа с рег. номером {ra.RegNum}!";
                            return false;
                        }

                        //проверка наличия кода ошибки (также на удаленные)
                        if(!string.IsNullOrEmpty(ra.ErrorCode) && codes.FirstOrDefault(e=> e.Code == ra.ErrorCode && e.IsErrorCode == 1 && e.StatusID != -1L) == null)
                        {
                            message = $"В БД отсутствует код ошибки {ra.ErrorCode} для отказа с рег. номером {ra.RegNum}!";
                            return false;
                        }

                        //проверка наличия кода стыковки с договором (также на удаленные)
                        if (!string.IsNullOrEmpty(ra.ContractDockCode) && codes.FirstOrDefault(e => e.Code == ra.ContractDockCode && e.IsErrorCode == 0 && e.StatusID != -1L) == null)
                        {
                            message = $"В БД отсутствует код стыковки с договором {ra.ContractDockCode} для отказа с рег. номером {ra.RegNum}!";
                            return false;
                        }

                        //проверка наличия кода региона
                        var region = Convert.ToInt32(ra.RegNum.Substring(0, 3));
                        var result = branches.FirstOrDefault(reg => reg.RegionNumber == region);
                        if(result == null)
                        {
                            message = $"Не найден код региона {region}!";
                            return false;
                        }
                        ra.Region = result.ID;
                        _items.Add(ra);
                    }
                }
                ValidateContent();
                if(_canExecuteImport)
                    _filenames.Add(fileName);

                return true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        private void ValidateContent()
        {
            _canExecuteImport = _items != null && _items.Count > 0;
        }


        public bool CanExecuteImport()
        {
            return _canExecuteImport;
        }

        public bool CanExecuteOpenFile()
        {
            return true;
        }

        /// <summary>
        /// Запуск операции импорта
        /// </summary>
        /// <param name="message">Исходящее сообщение</param>
        public bool ExecuteImport(out string message)
        {
            var result = Import(out message);
            if (result) RefreshViewModels();

            Clear();            
            return result;
        }

        private bool Import(out string message)
        {
            try
            {
                for (int i = 0; i < _filenames.Count; i++)
                {
                    var result = SaveFileToRepo(_filenames[i], out message);
                    if (!string.IsNullOrEmpty(message) || result < 0)
                        return false;
                    var itemIndex = i;
                    //заменяем временный индекс на реальный Id
                    Items.Where(a=> a.FileId == itemIndex).ForEach(a=> a.FileId = result);
                }
                Items.ForEach(x => x.ID = DataContainerFacade.Save<RejectApplication, long>(x));
                message = $"Импортировано записей: {Items.Count}";
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Сохранение файлов в репозиторий импорта/экспорта
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <param name="message">Исходящее сообщение</param>
        private long SaveFileToRepo(string filename, out string message)
        {
            message = null;
            try
            {
                var dt = DateTime.Now;
                var r = new RepositoryNoAucImpExpFile
                {
                    DDate = dt,
                    ImpExp = 1, //импорт
                    Repository = GZipCompressor.Compress(ExcelTools.SafeReadFile(filename)),
                    Status = 1,
                    TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
                    UserName = AP.Provider.UserName,
                    Comment = "Работа с НПФ - Отказные заявления - Импорт",
                };
                return DataContainerFacade.Save(r);

            }catch(Exception ex)
            {
                message = $"Ошибка при сохранении файла '{filename}' в репозиторий!\n{ex.Message}";
                return -1;
            }
        }

        public void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(RejAppListViewModel));            
        }


        internal void Clear()
        {
            Items.Clear();
            _filenames.Clear();
            _canExecuteImport = false;
        }
    }
}
