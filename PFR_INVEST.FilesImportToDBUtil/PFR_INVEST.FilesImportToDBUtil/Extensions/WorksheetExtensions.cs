﻿using System;
using Microsoft.Office.Interop.Excel;

namespace PFR_INVEST.FilesImportToDBUtil
{
    public static class WorksheetExtensions
    {
        public static string GetCellStringValue(this Worksheet worksheet, int rowIndex, int columnIndex)
        {
            return Convert.ToString(((Range)worksheet.UsedRange.Cells[rowIndex, columnIndex]).Value);
        }
    }
}
