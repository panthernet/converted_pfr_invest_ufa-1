﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.FilesImportToDBUtil.Infrastructure;
using PFR_INVEST.FilesImportToDBUtil.Managers;
using PFR_INVEST.FilesImportToDBUtil.Models;

namespace PFR_INVEST.FilesImportToDBUtil.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        private readonly DBManager dbManager;

        private readonly ObservableCollection<ImportResult> importResults = new ObservableCollection<ImportResult>();

        private string importFilePath;
        private ImportState state;

        public MainWindowViewModel(DBManager dbManager)
        {
            this.dbManager = dbManager;
            ExecuteImportCommand = new DelegateCommand(ExecuteImport);
        }

        public ObservableCollection<ImportResult> ImportResults
        {
            get { return importResults; }
        }

        public string ImportFilePath
        {
            get
            {
                return importFilePath;
            }
            set
            {
                importFilePath = value;
                OnPropertyChanged(() => ImportFilePath);
            }
        }

        public ImportState State
        {
            get
            {
                return state;
            }
            private set
            {
                state = value;
                OnPropertyChanged(() => State);
            }
        }

        public ICommand ExecuteImportCommand { get; set; }

        public void ExecuteImport(object param)
        {
            State = ImportState.Running;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate
            {
                Import();
            };
            worker.RunWorkerCompleted += delegate
            {
                State = ImportState.Finished;
            };
            worker.RunWorkerAsync();
        }

        private void Import()
        {
            Worksheet worksheet = OpenFileFirstWorksheet(importFilePath);
            var importFileEntries = GetFileImportEntries(worksheet);

            foreach (var entry in importFileEntries)
            {
                var result = ImportFile(entry);
                result.FileName = Path.GetFileName(entry.FilePath);
                ExecuteInMainThread(() =>
                    importResults.Add(result));
            }
            ExecuteInMainThread(() =>
                {
                    ImportResult totalResult = new ImportResult { IsSuccessfully = importResults.All(x => x.IsSuccessfully) };

                    totalResult.Message = totalResult.IsSuccessfully ?
                        "Импорт успешно завершен" :
                        string.Format("Импорт завершен с ошибками. Успешно выполнено {0} из {1}", importResults.Count(x => x.IsSuccessfully), importResults.Count);
                    importResults.Add(totalResult);
                });
        }

        private ImportResult ImportFile(ImportFileEntry entry)
        {
            try
            {
                if (!File.Exists(entry.FilePath))
                    return new ImportResult { IsSuccessfully = false, Message = string.Format("Файл не найден: {0}", entry.FilePath) };

                byte[] fileContent = GetFileContent(entry.FilePath);
                bool isExecuted = dbManager.UpdateDBColumn(entry.TableName, entry.ColumnName, entry.Id, fileContent);

                return new ImportResult
                {
                    IsSuccessfully = isExecuted,
                    Message = isExecuted ? "Успешно импортирован" : string.Format("Не найдена запись в таблице {0} с ID = {1}", entry.TableName, entry.Id)
                };
            }
            catch (Exception e)
            {
                return new ImportResult { IsSuccessfully = false, Message = e.Message };
            }
        }

        private void ExecuteInMainThread(System.Action action)
        {
            App.Current.Dispatcher.Invoke(action);
        }

        private IEnumerable<ImportFileEntry> GetFileImportEntries(Worksheet worksheet)
        {
            int a = worksheet.Cells.Columns.Count;
            List<ImportFileEntry> items = new List<ImportFileEntry>();

            for (int rowIndex = 1; rowIndex <= worksheet.UsedRange.Rows.Count; rowIndex++)
            {
                string filePath = worksheet.GetCellStringValue(rowIndex, 1);
                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    if (!Path.IsPathRooted(filePath))
                        filePath = Path.Combine(Path.GetDirectoryName(ImportFilePath), filePath);
                    ImportFileEntry item = new ImportFileEntry
                    {
                        FilePath = filePath,
                        TableName = worksheet.GetCellStringValue(rowIndex, 2),
                        ColumnName = worksheet.GetCellStringValue(rowIndex, 3),
                        Id = worksheet.GetCellStringValue(rowIndex, 4)
                    };
                    items.Add(item);
                }
            }

            return items;
        }

        private Worksheet OpenFileFirstWorksheet(string filePath)
        {
            Application excelApp = new Application { Visible = false };
            Workbook workbook = excelApp.Workbooks.Open(filePath);
            return (Worksheet)workbook.Sheets[1];
        }

        private byte[] GetFileContent(string filePath)
        {
            return File.ReadAllBytes(filePath);
        }

        private bool UpdateDBColumn(string tableName, string columnName, byte[] content)
        {
            return true;
        }

        public enum ImportState
        {
            NotStarted,
            Running,
            Finished
        }
    }
}
