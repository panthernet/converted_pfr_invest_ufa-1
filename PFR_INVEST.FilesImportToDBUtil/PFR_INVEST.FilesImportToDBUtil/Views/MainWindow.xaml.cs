﻿using System.Windows;
using PFR_INVEST.FilesImportToDBUtil.ViewModels;
using Microsoft.Win32;
using PFR_INVEST.FilesImportToDBUtil.Managers;

namespace PFR_INVEST.FilesImportToDBUtil.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel(new DBManager());
        }

        private MainWindowViewModel ViewModel
        {
            get { return DataContext as MainWindowViewModel; }
        }

        private void OnSelectFileButtonClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                DefaultExt = ".xls",
                Filter = "Excel-файлы (*.xls,*.xlsx)|*.xls;*.xlsx"
            };

            if (dialog.ShowDialog() == true)
            {
                ViewModel.ImportFilePath = dialog.FileName;
            }
        }

        private void OnCloseFormButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
