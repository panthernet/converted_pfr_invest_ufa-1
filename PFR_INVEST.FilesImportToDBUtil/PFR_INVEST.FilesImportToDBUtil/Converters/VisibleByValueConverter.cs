﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PFR_INVEST.FilesImportToDBUtil.Converters
{
    public class VisibleByValueConverter : IValueConverter
    {
        private readonly TrueByValueConverter trueByValueConverter = new TrueByValueConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isEqual = (bool)trueByValueConverter.Convert(value, targetType, parameter, culture);
            return isEqual ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("'ConvertBack' operation is not supported.");
        }
    }
}
