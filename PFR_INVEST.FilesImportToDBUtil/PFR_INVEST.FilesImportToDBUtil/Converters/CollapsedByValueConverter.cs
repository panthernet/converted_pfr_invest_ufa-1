﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PFR_INVEST.FilesImportToDBUtil.Converters
{
    public class CollapsedByValueConverter : IValueConverter
    {
        private static VisibleByValueConverter visibleByValueConverter = new VisibleByValueConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility inverseVisibility = (Visibility)visibleByValueConverter.Convert(value, targetType, parameter, culture);
            return inverseVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("'ConvertBack' operation is not supported.");
        }
    }
}
