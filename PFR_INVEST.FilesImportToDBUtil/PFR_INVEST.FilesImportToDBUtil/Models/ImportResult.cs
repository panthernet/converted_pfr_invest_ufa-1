﻿namespace PFR_INVEST.FilesImportToDBUtil.Models
{
    public class ImportResult
    {
        public bool IsSuccessfully { get; set; }
        public string FileName { get; set; }
        public string Message { get; set; }
    }
}
