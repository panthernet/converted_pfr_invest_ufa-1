﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PFR_INVEST.DataObjects
{
    public static class DataObjectExtension
	{
		//Clone with serialization
		public static T Clone<T>(this T source) where T : class
		{
			if (!typeof(T).IsSerializable)
			{
				throw new ArgumentException("The type must be serializable.", "source");
			}

			// Don't serialize a null object, simply return the default for that object
			if (Object.ReferenceEquals(source, null))
			{
				return default(T);
			}

			IFormatter formatter = new BinaryFormatter();
			Stream stream = new MemoryStream();
			using (stream)
			{
				formatter.Serialize(stream, source);
				stream.Seek(0, SeekOrigin.Begin);
				return (T)formatter.Deserialize(stream);
			}
		}

		//Copy with Reflection
		public static T CopyTo<T>(this T input, T output) where T : class
		{
			var props = typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite).OrderBy(x => x.Name).ToList();

			foreach (var p in props)
			{
				p.SetValue(output, p.GetValue(input, null), null);
			}

			return output;
		}

	    public static RES CopyToRaw<T, RES>(this T input) where T : class where RES: class, new()
	    {
	        var props = typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite).OrderBy(x => x.Name).ToList();
	        var rProps = typeof(RES).GetProperties().Where(p => p.CanRead && p.CanWrite).OrderBy(x => x.Name).ToList();
	        var output = new RES();
	        foreach (var p in rProps)
	        {
	            var iProp = props.FirstOrDefault(a => a.Name == p.Name);
	            if (iProp != null)
	            {
#if WEBCLIENT
	                if(typeof(IIdentifiable).IsAssignableFrom(iProp.PropertyType))
	                    continue;
#endif
	                p.SetValue(output, iProp.GetValue(input, null), null);
	            }
	        }

	        return output;
	    }

	    public static void CopyToRaw<T, RES>(this T input, RES output) where T : class where RES: class
	    {
	        var props = typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite).OrderBy(x => x.Name).ToList();
	        var rProps = typeof(RES).GetProperties().Where(p => p.CanRead && p.CanWrite).OrderBy(x => x.Name).ToList();
	        foreach (var p in rProps)
	        {
	            var iProp = props.FirstOrDefault(a => a.Name == p.Name);
	            if (iProp != null)
	            {
#if WEBCLIENT
	                if(typeof(IIdentifiable).IsAssignableFrom(iProp.PropertyType))
	                    continue;
#endif
	                p.SetValue(output, iProp.GetValue(input, null), null);
	            }
	        }
	    }

        public static T CopyTo<T>(this T input) where T : class, new()
        {
            var props = typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite).OrderBy(x => x.Name).ToList();
            var output = new T();
            foreach (var p in props)
            {
                p.SetValue(output, p.GetValue(input, null), null);
            }

            return output;
        }

		//Copy with Reflection, Replace null with 0 for  decimal?
		public static T CopyToWithoutNull<T>(this T input, T output) where T : class
		{
			var props = typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite).OrderBy(x => x.Name).ToList();

			foreach (var p in props)
			{
				var val = p.GetValue(input, null);
				if (p.PropertyType == typeof(decimal?) && val == null)
					val = default(decimal);
				p.SetValue(output, val, null);
			}

			return output;
		}

		public static T Copy<T>(this T input) where T : class, new()
		{
			var copy = new T();
			input.CopyTo(copy);
			return copy;
		}
	}



}
