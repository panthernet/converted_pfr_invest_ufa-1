﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Constants.Files
{
    public static class Sizes
    {
        public const long Size1Mb = 1024 * 1024;
        public const long Size4Mb = 1024 * 1024 * 4;
    }
}
