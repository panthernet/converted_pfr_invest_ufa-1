﻿namespace PFR_INVEST.Constants.Identifiers
{
    public class TransferDirectionIdentifier
    {
        public static bool IsFromPFRToUK(string p_sDirection)
        {
            return p_sDirection != null && (p_sDirection.Trim().ToLower() == "из пфр в ук" || p_sDirection.Trim().ToLower() == "из пфр в гук вр") ;
        }

        public static bool IsFromUKToPFR(string p_sDirection)
        {
            return p_sDirection != null && (p_sDirection.Trim().ToLower() == "из ук в пфр" || p_sDirection.Trim().ToLower() == "из гук вр в пфр");
        }



		public static bool IsFromPFRToUK(long? DirectionID)
		{
			// 1 из УК в ПФР
			// 2 из ПФР в УК
			// 3 из ГУК ВР в ПФР
			// 4 из ПФР в ГУК ВР
			return DirectionID == 2 || DirectionID == 4;
		}

		public static bool IsFromUKToPFR(long? DirectionID)
		{
			 // 1 из УК в ПФР
			 // 2 из ПФР в УК
			 // 3 из ГУК ВР в ПФР
			 // 4 из ПФР в ГУК ВР
			return DirectionID == 1 || DirectionID == 3;
		}
    }
}
