﻿using System;

namespace PFR_INVEST.Constants.Identifiers
{
    public enum F402Status
    {
        Started = 1,
        Built = 2,
        Sent = 3,
        Delivered = 4,
        PaidInTime = 5,
        PaidTimeExceeded = 6
    }

    //public class F402StatusIdentifier
    //{
    //    public const string STARTED = "Начальное состояние";
    //    public const string BUILT = "Поручение на оплату услуг сформировано";
    //    public const string SENT = "Поручение на оплату услуг отправлено";
    //    public const string DELIVERED = "Поручение вручено УК";
    //    public const string PAID_IN_TIME = "Оплачено в срок";
    //    public const string PAID_TIME_EXCEEDED = "Оплачено с нарушением срока";

    //    public static bool IsStatusStarted(string p_sStatus)
    //    {
    //        return p_sStatus != null && p_sStatus.Trim().Equals(STARTED, StringComparison.OrdinalIgnoreCase);
    //    }

    //    public static bool IsStatusBuilt(string p_sStatus)
    //    {
    //        return p_sStatus != null && p_sStatus.Trim().Equals(BUILT, StringComparison.OrdinalIgnoreCase);
    //    }

    //    public static bool IsStatusSent(string p_sStatus)
    //    {
    //        return p_sStatus != null && p_sStatus.Trim().Equals(SENT, StringComparison.OrdinalIgnoreCase);
    //    }

    //    public static bool IsStatusDelivered(string p_sStatus)
    //    {
    //        return p_sStatus != null && p_sStatus.Trim().Equals(DELIVERED, StringComparison.OrdinalIgnoreCase);
    //    }

    //    public static bool IsStatusPaidInTime(string p_sStatus)
    //    {
    //        return p_sStatus != null && p_sStatus.Trim().Equals(PAID_IN_TIME, StringComparison.OrdinalIgnoreCase);
    //    }

    //    public static bool IsStatusPaidTimeExceeded(string p_sStatus)
    //    {
    //        return p_sStatus != null && p_sStatus.Trim().Equals(PAID_TIME_EXCEEDED, StringComparison.OrdinalIgnoreCase);
    //    }
    //}
}
