﻿
namespace PFR_INVEST.Constants.Identifiers
{
    public static class AccountIdentifier
    {
        public const string NPF_PERSONAL_ACCOUNT = "Лиц счет НПФ";

        public static class MeasureNames
        {
            public const string Roubles = "рубли";
            public const string ZL = "зл";
            public const string DeadZL = "умершие зл";
        }

        public static class MeasureIDs
        {
            public const long Roubles = 1;
            public const long ZL = 2;
            public const long DeadZL = 3;
        }
    }
}
