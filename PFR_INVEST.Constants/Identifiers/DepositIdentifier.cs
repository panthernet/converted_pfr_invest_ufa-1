﻿namespace PFR_INVEST.Constants.Identifiers
{
    public struct DepositIdentifier
    {
        public const string Placed = "Размещен";//ID=1
        public const string Returned = "Возвращен";//ID=2
        public const string PartiallyReturned = "Возвращен частично";//ID=3
        public const string ReturnedWithViolation = "Возвращен с нарушениями";//ID=4

        public const string PosteRestante = "До востребования";
        public const string Urgent = "Срочный";
        public const string Unknown = "Не определен";

        public static string GetDepositStatusText(long id)
        {
            switch (id)
            {
                case 1: return Placed;
                case 2: return Returned;
                case 3: return PartiallyReturned;
                case 4: return ReturnedWithViolation;
                default: return Unknown;
            }

        }
    }
}
