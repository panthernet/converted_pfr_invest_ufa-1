﻿namespace PFR_INVEST.Constants.Identifiers
{
    public static class DueDocKindIdentifier
    {
        public const string DSV = " ДСВ";
        public const string Due = "Страховые взносы";
        public const string DueDSV = "Страховые взносы ДСВ";
        public const string Penalty = "Пени и штрафы";
        public const string PenaltyTreasurity = "Пени и штрафы. Документ казначейства";
        
        public const string DueAccurate = "Уточнение страховых взносов";
        public const string DueAccurateQuarter = "Уточнение страховых взносов за квартал";
        public const string DueDSVAccurate = "Уточнение ДСВ";
		// public const string DueDSVAccurateQuarter = "Уточнение ДСВ за квартал"; // http://jira.dob.datateh.ru/browse/DOKIPIV-1097
		public const string DueDSVAccurateYear = "Уточнение ДСВ за год";
        public const string AccurateQuarter = "Уточнение за квартал";
        public const string PenaltyAccurate = "Уточнение пени и штрафов";
        public const string PenaltyAccurateQuarter = "Уточнение пени и штрафов за квартал";

        public const string Prepayment = "Поступление авансового платежа";//"Авансовый платеж";
        public const string PrepaymentDSV = "Поступление авансового платежа ДСВ";//"Авансовый платеж";
        public const string PrepaymentAccurate = "Уточнение авансового платежа";
        public const string PrepaymentAccurateDSV = "Уточнение авансового платежа ДСВ";
        public const string DueDead = "Страховые взносы умерших ЗЛ";
        public const string DueDeadDSV = "Страховые взносы умерших ЗЛ ДСВ";
        public const string DueDeadAccurate = "Уточнение страховых взносов умерших ЗЛ";
        public const string DueDeadAccurateDSV = "Уточнение страховых взносов умерших ЗЛ ДСВ";
        public const string DueExcess = "Излишне учтенные страховые взносы";
        public const string DueExcessDSV = "Излишне учтенные страховые взносы ДСВ";
        public const string DueExcessAccurate = "Уточнение излишне учтенных страховых взносов";
        public const string DueExcessAccurateDSV = "Уточнение излишне учтенных страховых взносов ДСВ";
        public const string DueUndistributed = "Неразнесенные страховые взносы";//"Страховые взносы, не разнесенные по лицевым счетам";
        public const string DueUndistributedDSV = "Неразнесенные страховые взносы ДСВ";//"Страховые взносы, не разнесенные по лицевым счетам";
        public const string DueUndistributedAccurate = "Уточнение страховых взносов, не разнесенных по лицевым счетам";//"Уточнение страховых взносов, неразнесенных по лицевым счетам";
        public const string DueUndistributedAccurateDSV = "Уточнение страховых взносов, не разнесенных по лицевым счетам ДСВ";//"Уточнение страховых взносов, неразнесенных по лицевым счетам";
        public const string Treasurity = "Документ казначейства";

    }
}
