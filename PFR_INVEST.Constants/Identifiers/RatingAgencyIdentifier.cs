﻿using System.Collections.Generic;

namespace PFR_INVEST.Constants.Identifiers
{
    public class RatingAgencyIdentifier
    {
        public static readonly Dictionary<RatingAgencyType, string> RatingAgencyTypes = new Dictionary<RatingAgencyType, string>
                                                                                            {
                                                                                                { RatingAgencyType.National, "Национальное" }, 
                                                                                                { RatingAgencyType.World, "Международное" }
                                                                                            };

        public enum RatingAgencyType
        {
            World,
            National
        }
    }
}
