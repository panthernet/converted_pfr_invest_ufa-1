﻿namespace PFR_INVEST.Constants.Identifiers
{
    public static class AccDocKindIdentifier
    {
        public const string EnrollmentPercents = "Зачисление процентов по счетам";
        public const string EnrollmentOther = "Зачисление прочих поступлений";
        public const string Withdrawal = "Списание со счета";
        public const string Transfer = "Перечисление на счет";
    }
}
