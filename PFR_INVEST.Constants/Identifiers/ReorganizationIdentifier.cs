﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PFR_INVEST.Constants.Identifiers
{
	/// <summary>
	///  -- --------------
  ///1 Преобразование
  ///2 Слияние
  ///3 Присоединение
  ///4 Выделение
  ///5 Разделение
	/// </summary>
    public static class ReorganizationIdentifier
    {
		/// <summary>
		/// 1 Преобразование
		/// </summary>
        public const int TypeTransformation = 1;
		
		/// <summary>
		/// 2 Слияние
		/// </summary>
        public const int TypeMerge = 2;

		/// <summary>
		/// 3 Присоединение
		/// </summary>
        public const int TypeJoin = 3;

		/// <summary>
		/// 4 Выделение
		/// </summary>
        public const int TypeExclusion = 4;

		/// <summary>
		/// 5 Разделение
		/// </summary>
        public const int TypeSeparation = 5;
    }		
}
