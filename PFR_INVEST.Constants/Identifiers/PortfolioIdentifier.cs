﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.Constants.Identifiers
{
    public class PortfolioIdentifier
    {      
		// Используется для текстового сравнения
        public const string DSV = "ДСВ";
       

		
        public enum PortfolioTypes
        {            
            DSV = 2,
            SPN = 3,
            ALL = 4,
            NOT_DSV = 5,
            NOT_SPN = 6          
        }

        public struct PortfolioPBAParams
        {
			[DataContract]
			public class PortfolioFilter 
			{
				[DataMember]
				public long[] PortfolioTypeID { get; set; }
				[DataMember]
				public bool Exclude { get; set; }

				public PortfolioFilter() { }

				public PortfolioFilter(bool exclude, params long[] portfolioTypeIDs) 
				{
					Exclude = exclude;
					PortfolioTypeID = portfolioTypeIDs;
				}
			}

			public class PortfolioPBAParamsPortfolioAccountLink
			{
				public long PortfolioID;
				public long PfrBankAccountID;
			}

            public long? PortfolioID;
            public long[] HiddenPortfolioIDs;
            public long? PfrBankAccountID;
            public long[] HiddenPfrBankAccountID;			
			public PortfolioFilter PortfolioTypeFilter;
            public string[] CurrencyName;
            public string[] AccountType;
            public string[] LegalEntityFormalizedNameContains;
            public string[] ContragentContains;
            public string[] ContragentTypeContains;
            public PortfolioPBAParamsPortfolioAccountLink[] HiddenPortfolioAccountLinks;
            public string BankPortfolioContainAccountType;
			public long? SelectedPortfolioID;
        }
		
    }
}
