﻿
namespace PFR_INVEST.Constants.Identifiers
{
    public class CurrencyIdentifier
    {
        public const string RUB_PART = "руб";
        public const string RUBLES = "Рубли";
        public const string DOLLARS = "Доллары США";
        public static bool IsRUB(string currencyName)
        {
            return !string.IsNullOrEmpty(currencyName) && currencyName.ToLower().Contains(RUB_PART);
        }
    }
}
