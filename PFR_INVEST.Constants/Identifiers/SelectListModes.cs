﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Constants.Identifiers
{
    public enum SelectListModes
    {
        PortionResultSet = 0,
        FullResultSet = 1
    }
}
