﻿using System;

namespace PFR_INVEST.Constants.Identifiers
{
    public class TransferOperationIdentifier
    {
        public const string SPNRedistribution =               "Перераспределение СПН";
        public const string SPNRedistributionDSV =            "ДСВ Перераспределение СПН";
        public const string SPNRedistributionMSK =            "МСК Перераспределение СПН";
        public const string SPNReturn =                       "Возврат СПН - расторжение";
        public const string SPNYearPortfolioDissolution =     "Расформирование портфеля СПН года";
        public const string DSVQuarterPortfolioDissolution =  "ДСВ Расформирование портфеля квартала";
        public const string MSKHalfYearPortfolioDissolution = "МСК Расформирование портфеля полугодия";
        public const string DeadZL =                          "Выплаты правопреемникам";
        public const string DSVCofinancing =                  "ДСВ софинансирование";

        public static bool IsAPSIOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == DeadZL.ToLower();
        }

        public static bool IsSPNRedistributionOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == SPNRedistribution.ToLower();
        }

        public static bool IsSPNRedistributionDSVOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == SPNRedistributionDSV.ToLower();
        }

        public static bool IsSPNRedistributionMSKOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && string.Equals(p_sOpertion.Trim(), SPNRedistributionMSK, StringComparison.CurrentCultureIgnoreCase);
        }

        public static bool IsSPNReturnOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == SPNReturn.ToLower();
        }

        public static bool IsSPNYearPortfolioDissolutionOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == SPNYearPortfolioDissolution.ToLower();
        }

        public static bool IsDSVQuarterPortfolioDissolutionOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == DSVQuarterPortfolioDissolution.ToLower();
        }

        public static bool IsMSKHalfYearPortfolioDissolutionOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == MSKHalfYearPortfolioDissolution.ToLower();
        }

        public static bool IsDSVCofinancingOpertion(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower() == DSVCofinancing.ToLower();
        }

        public static bool IsDSVOperation(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower().Contains("дсв");
        }

        public static bool IsMSKOperation(string p_sOpertion)
        {
            return p_sOpertion != null && p_sOpertion.Trim().ToLower().Contains("мск");
        }
    }

    public class TransferRegisterCompanyMonthIdentifier
    {
        public static string NoMonth = "";
    }
}
