﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestAllocationRequestListItem : BaseTest
    {



        [TestMethod()]
        public void TestAllocationRequestListItemConstructor()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn);
        }

        [TestMethod()]
        public void TestADATE()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            DateTime expected = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime actual;
            target.ADATE = expected;
            actual = target.ADATE;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBankCount()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            long expected = 0; // TODO: Initialize to an appropriate value
            long actual;
            target.BankCount = expected;
            actual = target.BankCount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestClaimCount()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            long expected = 0; // TODO: Initialize to an appropriate value
            long actual;
            target.ClaimCount = expected;
            actual = target.ClaimCount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestClaimVolume()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
            Decimal actual;
            target.ClaimVolume = expected;
            actual = target.ClaimVolume;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDEPKIND()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.DEPKIND = expected;
            actual = target.DEPKIND;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDepAmputation()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
            Decimal actual;
            target.DepAmputation = expected;
            actual = target.DepAmputation;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDepRange()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
            Decimal actual;
            target.DepRange = expected;
            actual = target.DepRange;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestLOCATIONVOLUME()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
            Decimal actual;
            target.LOCATIONVOLUME = expected;
            actual = target.LOCATIONVOLUME;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMINRATE()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
            Decimal actual;
            target.MINRATE = expected;
            actual = target.MINRATE;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPERIOD()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            long expected = 0; // TODO: Initialize to an appropriate value
            long actual;
            target.PERIOD = expected;
            actual = target.PERIOD;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPFID()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            long expected = 0; // TODO: Initialize to an appropriate value
            long actual;
            target.PFID = expected;
            actual = target.PFID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPFName()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.PFName = expected;
            actual = target.PFName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSTATUS()
        {
            DBEntity dbEn = null; // TODO: Initialize to an appropriate value
            AllocationRequestListItem target = new AllocationRequestListItem(dbEn); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.STATUS = expected;
            actual = target.STATUS;
            Assert.AreEqual(expected, actual);
        }
    }
}
