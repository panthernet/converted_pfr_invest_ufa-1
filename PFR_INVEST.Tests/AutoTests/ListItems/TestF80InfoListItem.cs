﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF80InfoListItem : BaseTest
    {



        [TestMethod()]
        public void TestF80InfoListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent);
        }

        [TestMethod()]
        public void TestCBKind()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            string actual;
            actual = target.CBKind;
        }

        [TestMethod()]
        public void TestCBPrice()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            decimal actual;
            actual = target.CBPrice;
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            string actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestDealKind()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            string actual;
            actual = target.DealKind;
        }

        [TestMethod()]
        public void TestDealPrice()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            decimal actual;
            actual = target.DealPrice;
        }

        [TestMethod()]
        public void TestDiff()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            decimal actual;
            actual = target.Diff;
        }

        [TestMethod()]
        public void TestEmi()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            string actual;
            actual = target.Emi;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            string actual;
            actual = target.Num;
        }

        [TestMethod()]
        public void TestOrg()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F80InfoListItem(ent); 
            string actual;
            actual = target.Org;
        }
    }
}
