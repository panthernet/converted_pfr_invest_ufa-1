﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF50InfoListItem : BaseTest
    {



        [TestMethod()]
        public void TestF50InfoListItemConstructor()
        {
            string name = string.Empty; 
            string count = string.Empty; 
            string price = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F50InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F50InfoListItem(name, count, price);
        }

        [TestMethod()]
        public void TestCount()
        {
            string name = string.Empty; 
            string count = string.Empty; 
            string price = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F50InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F50InfoListItem(name, count, price); 
            string expected = string.Empty; 
            string actual;
            target.Count = expected;
            actual = target.Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestName()
        {
            string name = string.Empty; 
            string count = string.Empty; 
            string price = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F50InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F50InfoListItem(name, count, price); 
            string expected = string.Empty; 
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPrice()
        {
            string name = string.Empty; 
            string count = string.Empty; 
            string price = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F50InfoListItem target = new PFR_INVEST.ViewModels.ListItems.F50InfoListItem(name, count, price); 
            string expected = string.Empty; 
            string actual;
            target.Price = expected;
            actual = target.Price;
            Assert.AreEqual(expected, actual);
        }
    }
}
