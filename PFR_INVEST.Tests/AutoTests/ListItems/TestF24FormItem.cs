﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF24FormItem : BaseTest
    {

        [TestMethod()]
        public void TestF24FormItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity);
        }

        [TestMethod()]
        public void TestFullName()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.FullName = expected;
            actual = target.FullName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            long expected = 0; 
            long actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestInitials()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.Initials = expected;
            actual = target.Initials;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestKPP()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.KPP = expected;
            actual = target.KPP;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPostName()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.PostName = expected;
            actual = target.PostName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegDate()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.RegDate = expected;
            actual = target.RegDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.RegNum = expected;
            actual = target.RegNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRepCreateDate()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.RepCreateDate = expected;
            actual = target.RepCreateDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRepDataDate()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.RepDataDate = expected;
            actual = target.RepDataDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReportType()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F24FormItem target = new PFR_INVEST.ViewModels.ListItems.F24FormItem(entity); 
            string expected = string.Empty;
            string actual;
            target.ReportType = expected;
            actual = target.ReportType;
            Assert.AreEqual(expected, actual);
        }
    }
}
