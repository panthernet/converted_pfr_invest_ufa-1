﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestSIControlListItem : BaseTest
    {



        [TestMethod()]
        public void TestSIControlListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
        }

        [TestMethod()]
        public void TestContrDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.ContrDate;
        }

        [TestMethod()]
        public void TestDocClassif()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.DocClassif;
        }

        [TestMethod()]
        public void TestExecDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.ExecDate;
        }

        [TestMethod()]
        public void TestExecutor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.Executor;
        }

        [TestMethod()]
        public void TestExtra()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.Extra;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestInDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.InDate;
        }

        [TestMethod()]
        public void TestInNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.InNum;
        }

        [TestMethod()]
        public void TestMonth()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestOutNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.OutNum;
        }

        [TestMethod()]
        public void TestSendDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.SendDate;
        }

        [TestMethod()]
        public void TestSender()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.Sender;
        }

        [TestMethod()]
        public void TestYear()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SIControlListItem target = new PFR_INVEST.ViewModels.ListItems.SIControlListItem(ent);
            string actual;
            actual = target.Year;
        }
    }
}
