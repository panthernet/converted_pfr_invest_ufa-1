﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF40ListItem : BaseTest
    {


        [TestMethod()]
        public void TestF40ListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent);
        }

		[TestMethod()]
		public void TestIsEmpty() 
		{
			PFR_INVEST.Proxy.DBEntity ent = null;
			PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent);
			Assert.IsTrue(target.IsEmpty);		
		}

        [TestMethod()]
        public void TestBank()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.Bank;
        }

        [TestMethod()]
        public void TestBir()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.Bir;
        }

        [TestMethod()]
        public void TestBuyCount()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            int actual;
            actual = target.BuyCount;
        }

        [TestMethod()]
        public void TestBuyVol()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.BuyVol;
        }

        [TestMethod()]
        public void TestCBType()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.CBType;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestMonth()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.Name;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestSellCount()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            int actual;
            actual = target.SellCount;
        }

        [TestMethod()]
        public void TestYear()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F40ListItem target = new PFR_INVEST.ViewModels.ListItems.F40ListItem(ent); 
            string actual;
            actual = target.Year;
        }
    }
}
