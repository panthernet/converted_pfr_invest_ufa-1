﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestDepositListItem : BaseTest
    {

        [TestMethod()]
        public void TestDepositListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
        }

        [TestMethod()]
        public void TestAccNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            string expected = string.Empty;
            string actual;
            target.AccNum = expected;
            actual = target.AccNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBank()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            string expected = string.Empty;
            string actual;
            target.Bank = expected;
            actual = target.Bank;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBankID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            long expected = 0;
            long actual;
            target.BankID = expected;
            actual = target.BankID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDepositKind()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            string expected = string.Empty;
            string actual;
            target.DepositKind = expected;
            actual = target.DepositKind;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDepositYield()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            decimal expected = new decimal();
            decimal actual;
            target.DepositYield = expected;
            actual = target.DepositYield;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            long expected = 0;
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsArchive()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            string actual;
            actual = target.IsArchive;
        }

        [TestMethod()]
        public void TestLocationDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            System.DateTime? expected = new System.DateTime();
            System.DateTime? actual;
            target.LocationDate = expected;
            actual = target.LocationDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLocationVolume()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            decimal expected = new decimal();
            decimal actual;
            target.LocationVolume = expected;
            actual = target.LocationVolume;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestPortfolioID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            long expected = 0;
            long actual;
            target.PortfolioID = expected;
            actual = target.PortfolioID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            string expected = string.Empty;
            string actual;
            target.RegNum = expected;
            actual = target.RegNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRequestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            long expected = 0;
            long actual;
            target.RequestID = expected;
            actual = target.RequestID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReturnDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            System.DateTime? expected = new System.DateTime();
            System.DateTime? actual;
            target.ReturnDate = expected;
            actual = target.ReturnDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStatus()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            long expected = 0;
            long actual;
            target.Status = expected;
            actual = target.Status;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTermLocation()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.DepositListItem target = new PFR_INVEST.ViewModels.ListItems.DepositListItem(ent);
            long actual;
            actual = target.TermLocation;
        }
    }
}
