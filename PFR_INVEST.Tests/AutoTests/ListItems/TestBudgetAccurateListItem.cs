﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestBudgetAccurateListItem : BaseTest
    {


        [TestMethod()]
        public void TestBudgetAccurateListItemConstructor()
        {
            DBEntity dbEn = null; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn);
        }

        [TestMethod()]
        public void TestBudgetAccurateListItemConstructor1()
        {
            DBEntity dbEn = null; 
            long num = 1; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn, num);
        }

        [TestMethod()]
        public void TestBudgetAccurateListItemConstructor2()
        {
            DBEntity dbEn = null; 
            long num = 1; 
            Decimal baseSum = new Decimal(); 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn, num, baseSum);
        }

        [TestMethod()]
        public void TestBaseSum()
        {
            DBEntity dbEn = null; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn); 
            Decimal expected =12; 
            Decimal actual;
            target.BaseSum = expected;
            actual = target.BaseSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDate()
        {
            DBEntity dbEn = null; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn); 
            object actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestDiff()
        {
            DBEntity dbEn = null; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn); 
            Decimal actual;
            actual = target.Diff;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity dbEn = null; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestNUM()
        {
            DBEntity dbEn = null; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn); 
            long expected = 0; 
            long actual;
            target.NUM = expected;
            actual = target.NUM;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DBEntity dbEn = null; 
            BudgetAccurateListItem target = new BudgetAccurateListItem(dbEn); 
            Decimal actual;
            actual = target.Summ;
        }
    }
}
