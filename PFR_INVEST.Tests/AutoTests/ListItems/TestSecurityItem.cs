﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestSecurityItem : BaseTest
    {



        [TestMethod()]
        public void TestSecurityItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
        }

        [TestMethod()]
        public void TestCurrency()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            string actual;
            actual = target.Currency;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestIssue()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            string actual;
            actual = target.Issue;
        }

        [TestMethod()]
        public void TestKind()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            string actual;
            actual = target.Kind;
        }

        [TestMethod()]
        public void TestName()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            string actual;
            actual = target.Name;
        }

        [TestMethod()]
        public void TestNominal()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            decimal actual;
            actual = target.Nominal;
        }

        [TestMethod()]
        public void TestPlacementDate()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            System.DateTime actual;
            actual = target.PlacementDate;
        }

        [TestMethod()]
        public void TestRepaymentDate()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.SecurityItem target = new PFR_INVEST.ViewModels.ListItems.SecurityItem(dbEn);
            System.DateTime actual;
            actual = target.RepaymentDate;
        }
    }
}
