﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestBaseListItem : BaseTest
    {



        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestBaseListItemConstructor()
        {
            BaseListItem_Accessor target = new BaseListItem_Accessor();
        }

        [TestMethod()]
        public void TestBaseListItemConstructor1()
        {
            DBEntity ent = null; // TODO: Initialize to an appropriate value
            BaseListItem target = new BaseListItem(ent);
        }

        [TestMethod()]
        public void TestClone()
        {
            BaseListItem target = new BaseListItem(null); // TODO: Initialize to an appropriate value
            BaseListItem expected = null; // TODO: Initialize to an appropriate value
            BaseListItem actual;
            actual = target.Clone();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestGetDateTimeField()
        {
            BaseListItem_Accessor target = new BaseListItem_Accessor(); // TODO: Initialize to an appropriate value
            string fildName = string.Empty; // TODO: Initialize to an appropriate value
            DateTime expected = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime actual;
            actual = target.GetDateTimeField(fildName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestGetDecimalField()
        {
            BaseListItem_Accessor target = new BaseListItem_Accessor(); // TODO: Initialize to an appropriate value
            string fildName = string.Empty; // TODO: Initialize to an appropriate value
            Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
            Decimal actual;
            actual = target.GetDecimalField(fildName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestGetDoubleField()
        {
            BaseListItem_Accessor target = new BaseListItem_Accessor(); // TODO: Initialize to an appropriate value
            string fildName = string.Empty; // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.GetDoubleField(fildName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestGetLongField()
        {
            BaseListItem_Accessor target = new BaseListItem_Accessor(); // TODO: Initialize to an appropriate value
            string fildName = string.Empty; // TODO: Initialize to an appropriate value
            long expected = 0; // TODO: Initialize to an appropriate value
            long actual;
            actual = target.GetLongField(fildName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestGetStringField()
        {
            BaseListItem_Accessor target = new BaseListItem_Accessor(); // TODO: Initialize to an appropriate value
            string fildName = string.Empty; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetStringField(fildName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItemPropertyChanged()
        {
            BaseListItem target = new BaseListItem(null); // TODO: Initialize to an appropriate value
            string propertyName = string.Empty; // TODO: Initialize to an appropriate value
            target.ItemPropertyChanged(propertyName);
        }

        [TestMethod()]
        public void TestDBEntity()
        {
            BaseListItem target = new BaseListItem(null); // TODO: Initialize to an appropriate value
            DBEntity actual;
            actual = target.DBEntity;
        }
    }
}
