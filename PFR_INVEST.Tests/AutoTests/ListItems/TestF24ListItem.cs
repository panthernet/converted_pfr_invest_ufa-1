﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF24ListItem : BaseTest
    {



        [TestMethod()]
        public void TestF24ListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string total = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F24ListItem target = new PFR_INVEST.ViewModels.ListItems.F24ListItem(entity, field, total);
        }

        [TestMethod()]
        public void TestField()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string total = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F24ListItem target = new PFR_INVEST.ViewModels.ListItems.F24ListItem(entity, field, total); 
            string expected = string.Empty; 
            string actual;
            target.Field = expected;
            actual = target.Field;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotal()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            string field = string.Empty; 
            string total = string.Empty; 
            PFR_INVEST.ViewModels.ListItems.F24ListItem target = new PFR_INVEST.ViewModels.ListItems.F24ListItem(entity, field, total); 
            string expected = string.Empty; 
            string actual;
            target.Total = expected;
            actual = target.Total;
            Assert.AreEqual(expected, actual);
        }
    }
}
