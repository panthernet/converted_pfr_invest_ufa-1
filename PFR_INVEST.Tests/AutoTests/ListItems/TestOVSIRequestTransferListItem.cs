﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestOVSIRequestTransferListItem : BaseTest
    {



        [TestMethod()]
        public void TestPPNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            string actual;
            actual = target.PPNum;
        }

        [TestMethod()]
        public void TestPPDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            System.DateTime actual;
            actual = target.PPDate;
        }

        [TestMethod()]
        public void TestNUM()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            long expected = 0;
            long actual;
            target.NUM = expected;
            actual = target.NUM;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            System.DateTime actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestANum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            string actual;
            actual = target.ANum;
        }

        [TestMethod()]
        public void TestADate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            System.DateTime actual;
            actual = target.ADate;
        }

        [TestMethod()]
        public void TestOVSIRequestTransferListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
        }

        [TestMethod()]
        public void TestStatus()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            string actual;
            actual = target.Status;
        }

        [TestMethod()]
        public void TestSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            long n = 0;
            PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem target = new PFR_INVEST.ViewModels.ListItems.OVSIRequestTransferListItem(ent, n);
            string actual;
            actual = target.Sum;
        }
    }
}
