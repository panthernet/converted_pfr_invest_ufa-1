﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestTransferRequestListItem : BaseTest
    {



        [TestMethod()]
        public void TestTransferRequestListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
        }

        [TestMethod()]
        public void TestAccountNumber()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.AccountNumber;
        }

        [TestMethod()]
        public void TestBIK()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.BIK;
        }

        [TestMethod()]
        public void TestBank()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.Bank;
        }

        [TestMethod()]
        public void TestContragentName()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.ContragentName;
        }

        [TestMethod()]
        public void TestCorrAccountNumber()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.CorrAccountNumber;
        }

        [TestMethod()]
        public void TestCount()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            decimal actual;
            actual = target.Count;
        }

        [TestMethod()]
        public void TestFormalizedName()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.FormalizedName;
        }

        [TestMethod()]
        public void TestFullName()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.FullName;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestINN()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            string actual;
            actual = target.INN;
        }

        [TestMethod()]
        public void TestTrancheSum()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.TransferRequestListItem target = new PFR_INVEST.ViewModels.ListItems.TransferRequestListItem(dbEn);
            decimal expected = new decimal();
            decimal actual;
            target.TrancheSum = expected;
            actual = target.TrancheSum;
            Assert.AreEqual(expected, actual);
        }
    }
}
