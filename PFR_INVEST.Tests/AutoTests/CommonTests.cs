﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.ActiveDirectory;
using PFR_INVEST.Helpers;
using PFR_INVEST.Tests.AutoTests.ViewModels;

namespace PFR_INVEST.Tests.AutoTests
{
    [TestClass]
    public class CommonTests : BaseTest
    {
        [TestMethod]
        public void TestCombo()
        {
            PerformanceChecker chk = new PerformanceChecker();
            ComboBoxPFR2 comboBoxPfr2 = new ComboBoxPFR2();
            object tmp;
            tmp = comboBoxPfr2.IsTextEditable;
            tmp = comboBoxPfr2.DisplayMember;

            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestAD()
        {
            PerformanceChecker chk = new PerformanceChecker();
            ADDataSource adDataSource = new ADDataSource();
            object tmp;
            tmp = adDataSource.CurrentUserSecurity;
            /*tmp = adDataSource.GetADUser();
            tmp = adDataSource.GetUsers();
            tmp = adDataSource.GetRoles();
            adDataSource.RefreshUserSecurity();*/

            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRole()
        {
            PerformanceChecker chk = new PerformanceChecker();
            Role role = new Role();
            object tmp;
            tmp = role.Name;
            tmp = role.Description;
            tmp = role.Users;

            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestUser()
        {
            PerformanceChecker chk = new PerformanceChecker();
            User user = new User();
            object tmp;
            tmp = user.Login;
            tmp = user.Name;
            tmp = user.Roles;

            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestStringFormatConverter()
        {
            object tmp;
            PerformanceChecker chk = new PerformanceChecker();
            StringFormatConverter stringFormatConverter = new StringFormatConverter();
            tmp = stringFormatConverter.CultureName;
            tmp = stringFormatConverter.StringFormat;
            
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSettings()
        {
            object tmp;
            PerformanceChecker chk = new PerformanceChecker();
            tmp = Properties.Settings.Default.ClientTheme;
            tmp = Properties.Settings.Default.DBFPath;
            tmp = Properties.Settings.Default.MQManagerName;
            tmp = Properties.Settings.Default.MQQueueName;
            tmp = Properties.Settings.Default.MQXMLPath;
            tmp = Properties.Settings.Default.UserSettingsPath;

            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRibbon()
        {
            object tmp;
            PerformanceChecker chk = new PerformanceChecker();
           

            chk.CheckPerformance();
        }
    }
}
