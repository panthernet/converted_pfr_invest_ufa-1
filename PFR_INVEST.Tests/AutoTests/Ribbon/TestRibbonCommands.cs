﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Windows.Controls.Ribbon;
using RibbonCommands = PFR_INVEST.Ribbon.RibbonCommands;

namespace PFR_INVEST.Tests.AutoTests.Ribbon
{
    [TestClass()]
    public class TestRibbonCommands : BaseTest
    {


        [TestMethod()]
        public void TestAddDataFromReport()
        {
            RibbonCommand actual = RibbonCommands.AddDataFromReport;
        }

        [TestMethod()]
        public void TestAddMarketPrice()
        {
            RibbonCommand actual = RibbonCommands.AddMarketPrice;
        }

        [TestMethod()]
        public void TestAddOwnFunds()
        {
            RibbonCommand actual;
            actual = RibbonCommands.AddOwnFunds;
        }

        [TestMethod()]
        public void TestAddUser()
        {
            RibbonCommand actual;
            actual = RibbonCommands.AddUser;
        }

        [TestMethod()]
        public void TestAnnulLicence()
        {
            RibbonCommand actual;
            actual = RibbonCommands.AnnulLicence;
        }

        [TestMethod()]
        public void TestCorrectFinRegister()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CorrectFinRegister;
        }

        [TestMethod()]
        public void TestCreateAccount()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateAccount;
        }

        [TestMethod()]
        public void TestCreateAccountKind()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateAccountKind;
        }

        [TestMethod()]
        public void TestCreateAccountTransfer()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateAccountTransfer;
        }

        [TestMethod()]
        public void TestCreateAccountWithdrawal()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateAccountWithdrawal;
        }

        [TestMethod()]
        public void TestCreateBank()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateBank;
        }

        [TestMethod()]
        public void TestCreateBankAccount()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateBankAccount;
        }

        [TestMethod()]
        public void TestCreateBudget()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateBudget;
        }

        [TestMethod()]
        public void TestCreateBudgetAccurate()
        {
            RibbonCommand actual = RibbonCommands.CreateBudgetAccurate;
        }

        [TestMethod()]
        public void TestCreateContragent()
        {
            RibbonCommand actual = RibbonCommands.CreateContragent;
        }

        [TestMethod()]
        public void TestCreateDeadZL()
        {
            RibbonCommand actual = RibbonCommands.CreateDeadZL;
        }

        [TestMethod()]
        public void TestCreateDeposit()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDeposit;
        }

        [TestMethod()]
        public void TestCreateDirection()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDirection;
        }

        [TestMethod()]
        public void TestCreateDraft()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDraft;
        }

        [TestMethod()]
        public void TestCreateDue()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDue;
        }

        [TestMethod()]
        public void TestCreateDueDSV()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDueDSV;
        }

        [TestMethod()]
        public void TestCreateDueDead()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDueDead;
        }

        [TestMethod()]
        public void TestCreateDueDeadAccurate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDueDeadAccurate;
        }

        [TestMethod()]
        public void TestCreateDueExcess()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDueExcess;
        }

        [TestMethod()]
        public void TestCreateDueExcessAccurate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDueExcessAccurate;
        }

        [TestMethod()]
        public void TestCreateDueUndistributed()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDueUndistributed;
        }

        [TestMethod()]
        public void TestCreateDueUndistributedAccurate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateDueUndistributedAccurate;
        }

        [TestMethod()]
        public void TestCreateERZLNotify()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateERZLNotify;
        }

        [TestMethod()]
        public void TestCreateEnrollmentOther()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateEnrollmentOther;
        }

        [TestMethod()]
        public void TestCreateEnrollmentPercents()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateEnrollmentPercents;
        }

        [TestMethod()]
        public void TestCreateFinRegister()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateFinRegister;
        }

        [TestMethod()]
        public void TestCreateIncomeSecurities()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateIncomeSecurities;
        }

        [TestMethod()]
        public void TestCreateInsurance()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateInsurance;
        }

        [TestMethod()]
        public void TestCreateLegalEntity()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateLegalEntity;
        }

        [TestMethod()]
        public void TestCreateLetterToNPF()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateLetterToNPF;
        }

        [TestMethod()]
        public void TestCreateMonthAccurate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateMonthAccurate;
        }

        [TestMethod()]
        public void TestCreateMonthAccurateDSV()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateMonthAccurateDSV;
        }

        [TestMethod()]
        public void TestCreateOVSIReqTransfer()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateOVSIReqTransfer;
        }

        [TestMethod()]
        public void TestCreateOVSITransfer()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateOVSITransfer;
        }

        [TestMethod()]
        public void TestCreateOrder()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateOrder;
        }

        [TestMethod()]
        public void TestCreateOrderToPay()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateOrderToPay;
        }

        [TestMethod()]
        public void TestCreatePFRAccount()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePFRAccount;
        }

        [TestMethod()]
        public void TestCreatePFRBranch()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePFRBranch;
        }

        [TestMethod()]
        public void TestCreatePFRContact()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePFRContact;
        }

        [TestMethod()]
        public void TestCreatePenalty()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePenalty;
        }

        [TestMethod()]
        public void TestCreatePeriodAccurate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePeriodAccurate;
        }

        [TestMethod()]
        public void TestCreatePeriodAccurateDSV()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePeriodAccurateDSV;
        }

        [TestMethod()]
        public void TestCreatePortfolio()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePortfolio;
        }

        [TestMethod()]
        public void TestCreatePrecisePenalty()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePrecisePenalty;
        }

        [TestMethod()]
        public void TestCreatePrepayment()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePrepayment;
        }

        [TestMethod()]
        public void TestCreatePrepaymentAccurate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePrepaymentAccurate;
        }

        [TestMethod()]
        public void TestCreatePrintReq()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreatePrintReq;
        }

        [TestMethod()]
        public void TestCreateRate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateRate;
        }

        [TestMethod()]
        public void TestCreateRegister()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateRegister;
        }

        [TestMethod()]
        public void TestCreateReorganization()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateReorganization;
        }

        [TestMethod()]
        public void TestCreateRequest()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateRequest;
        }

        [TestMethod()]
        public void TestCreateReturn()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateReturn;
        }

        [TestMethod()]
        public void TestCreateSI()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateSI;
        }

        [TestMethod()]
        public void TestCreateSIContract()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateSIContract;
        }

        [TestMethod()]
        public void TestCreateSIDocument()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateSIDocument;
        }

        [TestMethod()]
        public void TestCreateSIRegister()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateSIRegister;
        }

        [TestMethod()]
        public void TestCreateSIRequest()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateSIRequest;
        }

        [TestMethod()]
        public void TestCreateSecurity()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateSecurity;
        }

        [TestMethod()]
        public void TestCreateSecurityInOrder()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateSecurityInOrder;
        }

        [TestMethod()]
        public void TestCreateTransfer()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateTransfer;
        }

        [TestMethod()]
        public void TestCreateTreasuryDoc()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateTreasuryDoc;
        }

        [TestMethod()]
        public void TestCreateUKPaymentPlan()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateUKPaymentPlan;
        }

        [TestMethod()]
        public void TestCreateViolationCategory()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateViolationCategory;
        }

        [TestMethod()]
        public void TestCreateYield()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateYield;
        }

        [TestMethod()]
        public void TestCreateZLRedist()
        {
            RibbonCommand actual;
            actual = RibbonCommands.CreateZLRedist;
        }

        [TestMethod()]
        public void TestDeleteUser()
        {
            RibbonCommand actual;
            actual = RibbonCommands.DeleteUser;
        }

        [TestMethod()]
        public void TestEditRate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.EditRate;
        }

        [TestMethod()]
        public void TestEditSI()
        {
            RibbonCommand actual;
            actual = RibbonCommands.EditSI;
        }

        [TestMethod()]
        public void TestEditSIName()
        {
            RibbonCommand actual;
            actual = RibbonCommands.EditSIName;
        }

        [TestMethod()]
        public void TestFastDepositInput()
        {
            RibbonCommand actual;
            actual = RibbonCommands.FastDepositInput;
        }

        [TestMethod()]
        public void TestFastReportInput()
        {
            RibbonCommand actual;
            actual = RibbonCommands.FastReportInput;
        }

        [TestMethod()]
        public void TestFromUKEnterGetRequirementDate()
        {
            RibbonCommand actual;
            actual = RibbonCommands.FromUKEnterGetRequirementDate;
        }

        //[TestMethod()]
        //public void TestFromUKEnterPPData()
        //{
        //    RibbonCommand actual;
        //    actual = RibbonCommands.FromUKEnterPPData;
        //}

        [TestMethod()]
        public void TestFromUKEnterRequirementData()
        {
            RibbonCommand actual;
            actual = RibbonCommands.FromUKEnterRequirementData;
        }

        [TestMethod()]
        public void TestFromUKEnterTransferActData()
        {
            RibbonCommand actual;
            actual = RibbonCommands.FromUKEnterTransferActData;
        }

        [TestMethod()]
        public void TestGenerateRegister()
        {
            RibbonCommand actual;
            actual = RibbonCommands.GenerateRegister;
        }

        [TestMethod()]
        public void TestGiveFinRegister()
        {
            RibbonCommand actual;
            actual = RibbonCommands.GiveFinRegister;
        }

        [TestMethod()]
        public void TestLoadDBF()
        {
            RibbonCommand actual;
            actual = RibbonCommands.LoadDBF;
        }

        [TestMethod()]
        public void TestLoadODKReports()
        {
            RibbonCommand actual;
            actual = RibbonCommands.LoadODKReports;
        }

        [TestMethod()]
        public void TestOrderDelivered()
        {
            RibbonCommand actual;
            actual = RibbonCommands.OrderDelivered;
        }

        [TestMethod()]
        public void TestOrderPaid()
        {
            RibbonCommand actual;
            actual = RibbonCommands.OrderPaid;
        }

        [TestMethod()]
        public void TestOrderSent()
        {
            RibbonCommand actual;
            actual = RibbonCommands.OrderSent;
        }

        [TestMethod()]
        public void TestPrintOrder()
        {
            RibbonCommand actual;
            actual = RibbonCommands.PrintOrder;
        }

        [TestMethod()]
        public void TestRecalcUKPortfolios()
        {
            RibbonCommand actual;
            actual = RibbonCommands.RecalcUKPortfolios;
        }

        [TestMethod()]
        public void TestResetClientSettings()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ResetClientSettings;
        }

        [TestMethod()]
        public void TestReturnDeposit()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ReturnDeposit;
        }

        [TestMethod()]
        public void TestSaveCard()
        {
            RibbonCommand actual;
            actual = RibbonCommands.SaveCard;
        }

        [TestMethod()]
        public void TestShowACCList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowACCList;
        }

        [TestMethod()]
        public void TestShowAccountsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowAccountsList;
        }

        [TestMethod()]
        public void TestShowActivesMarketCost()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowActivesMarketCost;
        }

        [TestMethod()]
        public void TestShowActivesMarketCostScope()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowActivesMarketCostScope;
        }

        [TestMethod()]
        public void TestShowArchiveList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowArchiveList;
        }

        [TestMethod()]
        public void TestShowArchiveOVSITransfersList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowArchiveOVSITransfersList;
        }

        [TestMethod()]
        public void TestShowAssignPaymentsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowAssignPaymentsList;
        }

        [TestMethod()]
        public void TestShowBalance()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowBalance;
        }

        [TestMethod()]
        public void TestShowBanksList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowBanksList;
        }

        [TestMethod()]
        public void TestShowBuyReportsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowBuyReportsList;
        }

        [TestMethod()]
        public void TestShowClientThemes()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowClientThemes;
        }

        [TestMethod()]
        public void TestShowContragentsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowContragentsList;
        }

        [TestMethod()]
        public void TestShowCorrespondenceList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowCorrespondenceList;
        }

        [TestMethod()]
        public void TestShowCostsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowCostsList;
        }

        [TestMethod()]
        public void TestShowDBFSettings()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowDBFSettings;
        }

        [TestMethod()]
        public void TestShowDeadZLList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowDeadZLList;
        }

        [TestMethod()]
        public void TestShowDemandList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowDemandList;
        }

        [TestMethod()]
        public void TestShowDepositsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowDepositsList;
        }

        [TestMethod()]
        public void TestShowDueDSVList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowDueDSVList;
        }

        [TestMethod()]
        public void TestShowDueList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowDueList;
        }

        [TestMethod()]
        public void TestShowExchangeRateManager()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowExchangeRateManager;
        }

        [TestMethod()]
        public void TestShowF402List()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowF402List;
        }

        [TestMethod()]
        public void TestShowF40List()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowF40List;
        }

        [TestMethod()]
        public void TestShowF50List()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowF50List;
        }

        [TestMethod()]
        public void TestShowF60List()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowF60List;
        }

        [TestMethod()]
        public void TestShowF70List()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowF70List;
        }

        [TestMethod()]
        public void TestShowF80List()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowF80List;
        }

        [TestMethod()]
        public void TestShowIncomeOnSecuritiesList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowIncomeOnSecuritiesList;
        }

        [TestMethod()]
        public void TestShowInsuranceArchList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowInsuranceArchList;
        }

        [TestMethod()]
        public void TestShowInsuranceList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowInsuranceList;
        }

        [TestMethod()]
        public void TestShowMQSettings()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowMQSettings;
        }

        [TestMethod()]
        public void TestShowMarketPricesList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowMarketPricesList;
        }

        [TestMethod()]
        public void TestShowNetWealthsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowNetWealthsList;
        }

        [TestMethod()]
        public void TestShowNetWealthsSumList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowNetWealthsSumList;
        }

        [TestMethod()]
        public void TestShowOVSITransferList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowOVSITransferList;
        }

        [TestMethod()]
        public void TestShowOrdersList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowOrdersList;
        }

        [TestMethod()]
        public void TestShowOwnFundsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowOwnFundsList;
        }

        [TestMethod()]
        public void TestShowPFRAccountsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowPFRAccountsList;
        }

        [TestMethod()]
        public void TestShowPFRBranchesList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowPFRBranchesList;
        }

        [TestMethod()]
        public void TestShowPortfoliosList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowPortfoliosList;
        }

        [TestMethod()]
        public void TestShowRatesList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowRatesList;
        }

        [TestMethod()]
        public void TestShowRegistersList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowRegistersList;
        }

        [TestMethod()]
        public void TestShowReports()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowReports;
        }

        [TestMethod()]
        public void TestShowRolesList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowRolesList;
        }

        [TestMethod()]
        public void TestShowSIContractsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowSIContractsList;
        }

        [TestMethod()]
        public void TestShowSIControlList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowSIControlList;
        }

        [TestMethod()]
        public void TestShowSIList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowSIList;
        }

        [TestMethod()]
        public void TestShowSPNMovementsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowSPNMovementsList;
        }

        [TestMethod()]
        public void TestShowSchilsCostsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowSchilsCostsList;
        }

        [TestMethod()]
        public void TestShowSecuritiesList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowSecuritiesList;
        }

        [TestMethod()]
        public void TestShowSellReportsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowSellReportsList;
        }

        [TestMethod()]
        public void TestShowServerSettings()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowServerSettings;
        }

        [TestMethod()]
        public void TestShowTempAllocationList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowTempAllocationList;
        }

        [TestMethod()]
        public void TestShowTemplatesManager()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowTemplatesManager;
        }

        [TestMethod()]
        public void TestShowTransferToNPFList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowTransferToNPFList;
        }

        [TestMethod()]
        public void TestShowTransferWizard()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowTransferWizard;
        }

        [TestMethod()]
        public void TestShowUsersList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowUsersList;
        }

        [TestMethod()]
        public void TestShowViolationCategoriesList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowViolationCategoriesList;
        }

        [TestMethod()]
        public void TestShowViolationsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowViolationsList;
        }

        [TestMethod()]
        public void TestShowYieldsList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowYieldsList;
        }

        [TestMethod()]
        public void TestShowZLRedistList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowZLRedistList;
        }

        [TestMethod()]
        public void TestShowZLTransfersList()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ShowZLTransfersList;
        }

        [TestMethod()]
        public void TestSuspendActivity()
        {
            RibbonCommand actual;
            actual = RibbonCommands.SuspendActivity;
        }

        [TestMethod()]
        public void TestSuspendLicence()
        {
            RibbonCommand actual;
            actual = RibbonCommands.SuspendLicence;
        }

        //[TestMethod()]
        //public void TestToUKEnterPPData()
        //{
        //    RibbonCommand actual;
        //    actual = RibbonCommands.ToUKEnterPPData;
        //}

        [TestMethod()]
        public void TestToUKEnterTransferActData()
        {
            RibbonCommand actual;
            actual = RibbonCommands.ToUKEnterTransferActData;
        }

        [TestMethod()]
        public void TestСreateCosts()
        {
            RibbonCommand actual;
            actual = RibbonCommands.СreateCosts;
        }
    }
}
