﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTransferSchilsViewModel : BaseTest
    {

        [TestMethod()]
        public void TestTransferSchilsViewModelConstructor()
        {
            TransferSchilsViewModel target = new TransferSchilsViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            TransferSchilsViewModel target = new TransferSchilsViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            TransferSchilsViewModel_Accessor target = new TransferSchilsViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            TransferSchilsViewModel_Accessor target = new TransferSchilsViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            TransferSchilsViewModel target = new TransferSchilsViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

    

        [TestMethod()]
        public void TestAccount()
        {
            TransferSchilsViewModel target = new TransferSchilsViewModel(); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDirectionsList()
        {
            TransferSchilsViewModel target = new TransferSchilsViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.DirectionsList = expected;
            actual = target.DirectionsList;
            Assert.AreEqual(expected, actual);
        }

    
        [TestMethod()]
        public void TestSelectAccount()
        {
            TransferSchilsViewModel target = new TransferSchilsViewModel(); 
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedDirection()
        {
            TransferSchilsViewModel target = new TransferSchilsViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedDirection = expected;
            actual = target.SelectedDirection;
            Assert.AreEqual(expected, actual);
        }
    }
}
