﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRegistersListViewModel : BaseTest
    {



        [TestMethod()]
        public void TestRegistersListViewModelConstructor()
        {
            RegistersListViewModel target = new RegistersListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            RegistersListViewModel_Accessor target = new RegistersListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshRegistersList()
        {
            RegistersListViewModel_Accessor target = new RegistersListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshRegistersList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            RegistersListViewModel_Accessor target = new RegistersListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshRegistersList()
        {
            RegistersListViewModel_Accessor target = new RegistersListViewModel_Accessor();
            target.ExecuteRefreshRegistersList();
        }

        [TestMethod()]
        public void TestApprovedDocID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.ApprovedDocID = expected;
            actual = target.ApprovedDocID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestApprovedDocName()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            string expected = string.Empty;
            string actual;
            target.ApprovedDocName = expected;
            actual = target.ApprovedDocName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContent()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            string expected = string.Empty;
            string actual;
            target.Content = expected;
            actual = target.Content;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentName()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            string expected = string.Empty;
            string actual;
            target.ContragentName = expected;
            actual = target.ContragentName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCount()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            decimal expected = 12;
            decimal actual;
            target.Count = expected.ToString();
            actual = Convert.ToDecimal(target.Count);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCreditAccountID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.CreditAccountID = expected;
            actual = target.CreditAccountID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDate()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            DateTime expected = new DateTime();
            DateTime actual;
            target.Date = expected;
            actual = target.Date;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDebetAccountID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.DebetAccountID = expected;
            actual = target.DebetAccountID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestERZL_ID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.ERZL_ID = expected;
            actual = target.ERZL_ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFZ_ID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.FZ_ID = expected;
            actual = target.FZ_ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestKind()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            string expected = string.Empty;
            string actual;
            target.Kind = expected;
            actual = target.Kind;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfolioID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.PortfolioID = expected;
            actual = target.PortfolioID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefreshRegistersList()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            ICommand actual;
            actual = target.RefreshRegistersList;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            string expected = string.Empty;
            string actual;
            target.RegNum = expected;
            actual = target.RegNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegisterID()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            long expected = 0;
            long actual;
            target.RegisterID = expected;
            actual = target.RegisterID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegistersList()
        {
            RegistersListViewModel target = new RegistersListViewModel();
            DB2RegistersListItem[] expected = null;
            DB2RegistersListItem[] actual;
            target.RegistersList = expected;
            actual = target.RegistersList;
            Assert.AreEqual(expected, actual);
        }
    }
}
