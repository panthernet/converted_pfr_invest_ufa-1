﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestAccountKindViewModel : BaseTest
    {


        [TestMethod()]
        public void TestAccountKindViewModelConstructor()
        {
            AccountKindViewModel target = new AccountKindViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            AccountKindViewModel target = new AccountKindViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            AccountKindViewModel_Accessor target = new AccountKindViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoadAccountKind()
        {
            AccountKindViewModel target = new AccountKindViewModel(); 
            long id = 1; 
            target.LoadAccountKind(id);
        }

        

        [TestMethod()]
        public void TestID()
        {
            AccountKindViewModel target = new AccountKindViewModel(); 
            long expected = 1; 
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        
        [TestMethod()]
        public void TestMeasure()
        {
            AccountKindViewModel target = new AccountKindViewModel(); 
            string expected = "test string"; 
            string actual;
            target.Measure = expected;
            actual = target.Measure;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestName()
        {
            AccountKindViewModel target = new AccountKindViewModel(); 
            string expected = "test string"; 
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }
    }
}
