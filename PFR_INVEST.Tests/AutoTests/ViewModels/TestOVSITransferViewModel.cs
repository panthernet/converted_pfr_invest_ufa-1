﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestOVSITransferViewModel : BaseTest
    {


        [TestMethod()]
        public void TestOVSITransferViewModelConstructor()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            OVSITransferViewModel_Accessor target = new OVSITransferViewModel_Accessor(1, VIEWMODEL_STATES.Create); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestRefreshTransfers()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create); 
            target.RefreshTransfers();
        }


        [TestMethod()]
        public void TestContractsList()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create); 
            List<Contract> expected = null;
            List<Contract> actual;
            target.ContractsList = expected;
            actual = target.ContractsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContract()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create);
            Contract expected = null;
            Contract actual;
            target.SelectedContract = expected;
            actual = target.SelectedContract;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedUK()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create); 
            LegalEntity expected = null;
            LegalEntity actual;
            target.SelectedUK = expected;
            actual = target.SelectedUK;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTransferRequests()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create); 
            List<ReqTransfer> expected = null;
            List<ReqTransfer> actual;
            target.TransferRequests = expected;
            actual = target.TransferRequests;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUKList()
        {
            OVSITransferViewModel target = new OVSITransferViewModel(1, VIEWMODEL_STATES.Create);
            List<LegalEntity> expected = null;
            List<LegalEntity> actual;
            target.UKList = expected;
            actual = target.UKList;
            Assert.AreEqual(expected, actual);
        }
    }
}
