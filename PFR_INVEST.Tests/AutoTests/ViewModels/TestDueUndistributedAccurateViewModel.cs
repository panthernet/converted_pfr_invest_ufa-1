﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueUndistributedAccurateViewModel : BaseTest
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void TestDueUndistributedAccurateViewModelConstructor()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //PrivateObject param0 = null; 
        //DueUndistributedAccurateViewModel_Accessor target = new DueUndistributedAccurateViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestLoad()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
            long lId = 0;
            target.Load(lId);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestValidate()
        //{
        //    PrivateObject param0 = null;
        //    DueUndistributedAccurateViewModel_Accessor target = new DueUndistributedAccurateViewModel_Accessor(param0);
        //    string expected = string.Empty;
        //    string actual;
        //    actual = target.Validate();
        //    Assert.AreEqual(expected, actual);
        //}

        [TestMethod()]
        public void TestChSumm()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
            Decimal expected = new Decimal();
            Decimal actual;
            target.ChSumm = expected;
            actual = target.ChSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItem()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
            string columnName = string.Empty;
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        public void TestSrcAccount()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DueUndistributedViewModel parentVM = null;
            DueUndistributedAccurateViewModel target = new DueUndistributedAccurateViewModel(parentVM);
            Decimal expected = new Decimal();
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }
    }
}
