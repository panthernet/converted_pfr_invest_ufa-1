﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestUKPaymentPlanViewModel : BaseTest
    {

        [TestMethod()]
        public void TestUKPaymentPlanViewModelConstructor()
        {
            APSIRegisterViewModel regVM = null; 
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(regVM);
        }

        [TestMethod()]
        public void TestUKPaymentPlanViewModelConstructor1()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel();
        }

      

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCreateNewTransfers()
        {
            UKPaymentPlanViewModel_Accessor target = new UKPaymentPlanViewModel_Accessor(); 
            target.CreateNewTransfers();
        }

      
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            UKPaymentPlanViewModel_Accessor target = new UKPaymentPlanViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestFillInRegFields()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel();
            long lId = 1;
            target.Load(lId);
            target.FillInRegFields();
        }

        [TestMethod()]
        public void TestLoad()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

        
        [TestMethod()]
        public void TestRefreshPlanSum()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            target.RefreshPlanSum();
        }

        [TestMethod()]
        public void TestRefreshTransfers()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            target.RefreshTransfers();
        }

      

        [TestMethod()]
        public void TestCompany()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Company = expected;
            actual = target.Company;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContent()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Content = expected;
            actual = target.Content;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContractDate()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            string actual;
            actual = target.ContractDate;
        }

        [TestMethod()]
        public void TestContractsList()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.ContractsList = expected;
            actual = target.ContractsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFactSum()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            Decimal expected = 12;
            Decimal actual;
            target.FactSum = expected;
            actual = target.FactSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestInvestIncome()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.InvestIncome = expected;
            actual = target.InvestIncome;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOperation()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            DBEntity actual;
            actual = target.Operation;
        }

        [TestMethod()]
        public void TestPlanSum()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.PlanSum = expected;
            actual = target.PlanSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegister()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.Register = expected;
            actual = target.Register;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContract()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedContract = expected;
            actual = target.SelectedContract;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedTransfer()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            UKPaymentListItem expected = null; 
            UKPaymentListItem actual;
            target.SelectedTransfer = expected;
            actual = target.SelectedTransfer;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedUK()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedUK = expected;
            actual = target.SelectedUK;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTransferRequests()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            List<UKPaymentListItem> expected = null; 
            List<UKPaymentListItem> actual;
            target.TransferRequests = expected;
            actual = target.TransferRequests;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTransferRequestsGrid()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            List<OVSIRequestTransferListItem> expected = null; 
            List<OVSIRequestTransferListItem> actual;
            target.TransferRequestsGrid = expected;
            actual = target.TransferRequestsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUKList()
        {
            UKPaymentPlanViewModel target = new UKPaymentPlanViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.UKList = expected;
            actual = target.UKList;
            Assert.AreEqual(expected, actual);
        }
    }
}
