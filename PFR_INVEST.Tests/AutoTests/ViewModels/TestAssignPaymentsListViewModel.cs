﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestAssignPaymentsListViewModel : BaseTest
    {




        [TestMethod()]
        public void TestAssignPaymentsListViewModelConstructor()
        {
            AssignPaymentsListViewModel target = new AssignPaymentsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            AssignPaymentsListViewModel_Accessor target = new AssignPaymentsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            AssignPaymentsListViewModel_Accessor target = new AssignPaymentsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            AssignPaymentsListViewModel_Accessor target = new AssignPaymentsListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            AssignPaymentsListViewModel_Accessor target = new AssignPaymentsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }


        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            AssignPaymentsListViewModel_Accessor target = new AssignPaymentsListViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedRegID()
        {
            AssignPaymentsListViewModel target = new AssignPaymentsListViewModel();
            long expected = 1;
            long actual;
            target.SelectedRegID = expected;
            actual = target.SelectedRegID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedReqID()
        {
            AssignPaymentsListViewModel target = new AssignPaymentsListViewModel();
            long expected = 1;
            long actual;
            target.SelectedReqID = expected;
            actual = target.SelectedReqID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedStatus()
        {
            AssignPaymentsListViewModel target = new AssignPaymentsListViewModel();
            string expected = "test string";
            string actual;
            target.SelectedStatus = expected;
            actual = target.SelectedStatus;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedTrID()
        {
            AssignPaymentsListViewModel target = new AssignPaymentsListViewModel();
            long expected = 1;
            long actual;
            target.SelectedTrID = expected;
            actual = target.SelectedTrID;
            Assert.AreEqual(expected, actual);
        }
    }
}
