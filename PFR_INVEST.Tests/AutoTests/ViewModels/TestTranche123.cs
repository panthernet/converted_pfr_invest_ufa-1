﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTranche123 : BaseTest
    {

        [TestMethod()]
        public void TestTranche123Constructor()
        {
            Tranche123 target = new Tranche123();
        }

        [TestMethod()]
        public void TestNpfName()
        {
            Tranche123 target = new Tranche123(); 
            string expected = string.Empty; 
            string actual;
            target.NpfName = expected;
            actual = target.NpfName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSpnSumm()
        {
            Tranche123 target = new Tranche123(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.SpnSumm = expected;
            actual = target.SpnSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTranche1()
        {
            Tranche123 target = new Tranche123(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Tranche1 = expected;
            actual = target.Tranche1;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTranche2()
        {
            Tranche123 target = new Tranche123(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Tranche2 = expected;
            actual = target.Tranche2;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTranche3()
        {
            Tranche123 target = new Tranche123(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Tranche3 = expected;
            actual = target.Tranche3;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestZlCount()
        {
            Tranche123 target = new Tranche123(); 
            long expected = 0; 
            long actual;
            target.ZlCount = expected;
            actual = target.ZlCount;
            Assert.AreEqual(expected, actual);
        }
    }
}
