﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF70ViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF70ViewModelConstructor()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            F70ViewModel target = new F70ViewModel(id); 
            target.RefreshLists();
        }

       

        [TestMethod()]
        public void TestContractDate()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            DateTime expected = new DateTime(); 
            DateTime actual;
            target.ContractDate = expected;
            actual = target.ContractDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContractsList4NPF()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.ContractsList4NPF = expected;
            actual = target.ContractsList4NPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFullName()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            string expected = string.Empty; 
            string actual;
            target.FullName = expected;
            actual = target.FullName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            string expected = string.Empty; 
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestNPFList()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            List<DB2LegalEntityCard> expected = null; 
            List<DB2LegalEntityCard> actual;
            target.NPFList = expected;
            actual = target.NPFList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfolioName()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            string expected = string.Empty; 
            string actual;
            target.PortfolioName = expected;
            actual = target.PortfolioName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestQuartersList()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            List<int> expected = null; 
            List<int> actual;
            target.QuartersList = expected;
            actual = target.QuartersList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContract()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedContract = expected;
            actual = target.SelectedContract;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedNPF()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            DB2LegalEntityCard expected = null; 
            DB2LegalEntityCard actual;
            target.SelectedNPF = expected;
            actual = target.SelectedNPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedQuarter()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            int expected = -1; 
            int actual;
            target.SelectedQuarter = expected;
            actual = target.SelectedQuarter;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedYear()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedYear = expected;
            actual = target.SelectedYear;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYearsList()
        {
            long id = 1; 
            F70ViewModel target = new F70ViewModel(id); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.YearsList = expected;
            actual = target.YearsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
