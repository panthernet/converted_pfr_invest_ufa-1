﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ActiveDirectory;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRoleViewModel : BaseTest
    {


        [TestMethod()]
        public void TestRoleViewModelConstructor()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        
        [TestMethod()]
        public void TestAddedUsers()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
            List<User> expected = null;
            List<User> actual;
            target.AddedUsers = expected;
            actual = target.AddedUsers;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDescription()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
            string expected = string.Empty;
            string actual;
            target.Description = expected;
            actual = target.Description;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestName()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
            string expected = string.Empty;
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRemovedUsers()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
            List<User> expected = null;
            List<User> actual;
            target.RemovedUsers = expected;
            actual = target.RemovedUsers;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRole()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
            Role expected = null;
            Role actual;
           
            actual = target.Role;
           
        }

        [TestMethod()]
        public void TestUsers()
        {
            Role Role = null;
            RoleViewModel target = new RoleViewModel(Role);
            List<User> actual;
            actual = target.Users;
        }
    }
}
