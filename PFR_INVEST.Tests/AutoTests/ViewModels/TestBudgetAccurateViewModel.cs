﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBudgetAccurateViewModel : BaseTest
    {

        [TestMethod()]
        public void TestBudgetAccurateViewModelConstructor()
        {
            BudgetAccurateViewModel target = new BudgetAccurateViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            BudgetAccurateViewModel target = new BudgetAccurateViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            BudgetAccurateViewModel_Accessor target = new BudgetAccurateViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            BudgetAccurateViewModel target = new BudgetAccurateViewModel();
            long lId = 1;
            target.Load(lId);
            target.Load(0);
        }


        [TestMethod()]
        public void TestBudgetsList()
        {
            BudgetAccurateViewModel target = new BudgetAccurateViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.BudgetsList = expected;
            actual = target.BudgetsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedBudget()
        {
            BudgetAccurateViewModel target = new BudgetAccurateViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedBudget = expected;
            actual = target.SelectedBudget;
            Assert.AreEqual(expected, actual);
        }
    }
}
