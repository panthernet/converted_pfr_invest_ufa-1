﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestWithdrawalViewModel : BaseTest
    {
        [TestMethod()]
        public void TestWithdrawalViewModelConstructor()
        {
            WithdrawalViewModel target = new WithdrawalViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            WithdrawalViewModel_Accessor target = new WithdrawalViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            WithdrawalViewModel_Accessor target = new WithdrawalViewModel_Accessor(); 
            target.ExecuteSave();
        }


        [TestMethod()]
        public void TestLoad()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

       

        [TestMethod()]
        public void TestAccount()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrencyList()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

       
        [TestMethod()]
        public void TestRate()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectAccount()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedCurrency()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotal()
        {
            WithdrawalViewModel target = new WithdrawalViewModel(); 
            string actual;
            actual = target.Total;
        }
    }
}
