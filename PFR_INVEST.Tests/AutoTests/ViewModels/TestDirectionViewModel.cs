﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDirectionViewModel : BaseTest
    {



        [TestMethod()]
        public void TestDirectionViewModelConstructor()
        {
            DirectionViewModel target = new DirectionViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DirectionViewModel target = new DirectionViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            DirectionViewModel_Accessor target = new DirectionViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            DirectionViewModel target = new DirectionViewModel();
            long lId = 1;
            target.Load(lId);
        }


        [TestMethod()]
        public void TestBudgetsList()
        {
            DirectionViewModel target = new DirectionViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.BudgetsList = expected;
            actual = target.BudgetsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopTransfers()
        {
            DirectionViewModel target = new DirectionViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DopTransfers = expected;
            actual = target.DopTransfers;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopTransfersGrid()
        {
            DirectionViewModel target = new DirectionViewModel();
            List<DopTransfersListItem> expected = null;
            List<DopTransfersListItem> actual;
            target.DopTransfersGrid = expected;
            actual = target.DopTransfersGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedBudget()
        {
            DirectionViewModel target = new DirectionViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedBudget = expected;
            actual = target.SelectedBudget;
            Assert.AreEqual(expected, actual);
        }
    }
}
