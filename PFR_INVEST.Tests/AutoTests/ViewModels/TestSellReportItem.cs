﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSellReportItem : BaseTest
    {
        [TestMethod()]
        public void TestSellReportItemConstructor()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent);
        }

        [TestMethod()]
        public void TestCount()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            long actual;
            actual = target.Count;
        }

        [TestMethod()]
        public void TestCurrency()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            string actual;
            actual = target.Currency;
        }

        [TestMethod()]
        public void TestDateDEPO()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            DateTime actual;
            actual = target.DateDEPO;
        }

        [TestMethod()]
        public void TestDateOrder()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            DateTime actual;
            actual = target.DateOrder;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestNKD()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            Decimal actual;
            actual = target.NKD;
        }

        [TestMethod()]
        public void TestNKDTotal()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            Decimal actual;
            actual = target.NKDTotal;
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestPrice()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            decimal actual;
            actual = target.Price;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestSecName()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            string actual;
            actual = target.SecName;
        }

        [TestMethod()]
        public void TestSum()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            decimal actual;
            actual = target.Sum;
        }

        [TestMethod()]
        public void TestSumNom()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            decimal actual;
            actual = target.SumNom;
        }

        [TestMethod()]
        public void TestSumWithoutNKD()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            decimal actual;
            actual = target.SumWithoutNKD;
        }

        [TestMethod()]
        public void TestYield()
        {
            DBEntity ent = null; 
            SellReportItem target = new SellReportItem(ent); 
            Decimal actual;
            actual = target.Yield;
        }
    }
}
