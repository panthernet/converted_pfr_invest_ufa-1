﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestUsersViewModel : BaseTest
    {

        [TestMethod()]
        public void TestUsersViewModelConstructor()
        {
            UsersViewModel target = new UsersViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddUser()
        {
            UsersViewModel_Accessor target = new UsersViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteAddUser();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteDeleteUser()
        {
            UsersViewModel_Accessor target = new UsersViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteDeleteUser();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteDeleteUser()
        {
            UsersViewModel_Accessor target = new UsersViewModel_Accessor(); 
            target.ExecuteDeleteUser();
        }

        [TestMethod()]
        public void TestAddUser()
        {
            UsersViewModel target = new UsersViewModel(); 
            ICommand actual;
            actual = target.AddUser;
        }

        [TestMethod()]
        public void TestDeleteUser()
        {
            UsersViewModel target = new UsersViewModel(); 
            ICommand actual;
            actual = target.DeleteUser;
        }

        [TestMethod()]
        public void TestUsers()
        {
            UsersViewModel target = new UsersViewModel(); 
            ObservableCollection<UserViewModel> expected = null; 
            ObservableCollection<UserViewModel> actual;
            target.Users = expected;
            actual = target.Users;
            Assert.AreEqual(expected, actual);
        }
    }
}
