﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestZLRedistViewModel : BaseTest
    {

        [TestMethod()]
        public void TestZLRedistViewModelConstructor()
        {
            ZLRedistViewModel target = new ZLRedistViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddNotify()
        {
            ZLRedistViewModel_Accessor target = new ZLRedistViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteAddNotify();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteEditNotify()
        {
            ZLRedistViewModel_Accessor target = new ZLRedistViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteEditNotify();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteEditNotify()
        {
            ZLRedistViewModel_Accessor target = new ZLRedistViewModel_Accessor(); 
            target.ExecuteEditNotify();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            ZLRedistViewModel_Accessor target = new ZLRedistViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoadZLRedist()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            long id = 1; 
            target.LoadZLRedist(id);
        }

        [TestMethod()]
        public void TestRefreshLists()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            target.RefreshLists();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshNotify()
        {
            ZLRedistViewModel_Accessor target = new ZLRedistViewModel_Accessor(); 
            List<DBEntity> nots = null; 
            string selectedNPFName = string.Empty; 
            target.RefreshNotify(nots, selectedNPFName);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestUpdateNotify()
        //{
        //    ZLRedistViewModel_Accessor target = new ZLRedistViewModel_Accessor(); 
        //    VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
        //    long not_id = 1; 
        //    target.UpdateNotify(action, not_id);
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestUpdateReverseVMs()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            List<ERZLNotifyViewModel> list = null; 
            target.UpdateReverseVMs(list);
        }

       
        [TestMethod()]
        public void TestAddNotify()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            ICommand actual;
            actual = target.AddNotify;
        }

        [TestMethod()]
        public void TestEditNotify()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            ICommand actual;
            actual = target.EditNotify;
        }

        [TestMethod()]
        public void TestFZList()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            List<FZListItem> expected = null; 
            List<FZListItem> actual;
            target.FZList = expected;
            actual = target.FZList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFocusedVM()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            ERZLNotifyViewModel expected = null; 
            ERZLNotifyViewModel actual;
            target.FocusedVM = expected;
            actual = target.FocusedVM;
            Assert.AreEqual(expected, actual);
        }

      
        [TestMethod()]
        public void TestNotifyCount()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            int actual;
            actual = target.NotifyCount;
        }

        [TestMethod()]
        public void TestNotifyFullList()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.NotifyFullList = expected;
            actual = target.NotifyFullList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNotifyList()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            List<ERZLNotifyInZLRedistListItem> expected = null; 
            List<ERZLNotifyInZLRedistListItem> actual;
            target.NotifyList = expected;
            actual = target.NotifyList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNotifyVMList()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            List<ERZLNotifyViewModel> expected = null; 
            List<ERZLNotifyViewModel> actual;
            target.NotifyVMList = expected;
            actual = target.NotifyVMList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedFZ()
        {
            ZLRedistViewModel target = new ZLRedistViewModel(); 
            FZListItem expected = null; 
            FZListItem actual;
            target.SelectedFZ = expected;
            actual = target.SelectedFZ;
            Assert.AreEqual(expected, actual);
        }
    }
}
