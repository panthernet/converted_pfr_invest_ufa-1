﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPFRAccountViewModel : BaseTest
    {


        [TestMethod()]
        public void TestPFRAccountViewModelConstructor()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PFRAccountViewModel_Accessor target = new PFRAccountViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            long id = 1;
            target.Load(id);
        }

        [TestMethod()]
        public void TestRefreshLists()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            target.RefreshLists();
        }

       
        [TestMethod()]
        public void TestCurrencyList()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLegalEntitiesList()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            List<DB2LegalEntityCard> expected = null;
            List<DB2LegalEntityCard> actual;
            target.LegalEntitiesList = expected;
            actual = target.LegalEntitiesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedCurrency()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedLegalEntity()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            DB2LegalEntityCard expected = null;
            DB2LegalEntityCard actual;
            target.SelectedLegalEntity = expected;
            actual = target.SelectedLegalEntity;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedType()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedType = expected;
            actual = target.SelectedType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTypesList()
        {
            PFRAccountViewModel target = new PFRAccountViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.TypesList = expected;
            actual = target.TypesList;
            Assert.AreEqual(expected, actual);
        }
    }
}
