﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRecalcUKPortfoliosViewModel : BaseTest
    {

        [TestMethod()]
        public void TestRecalcUKPortfoliosViewModelConstructor()
        {
            RecalcUKPortfoliosViewModel target = new RecalcUKPortfoliosViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            RecalcUKPortfoliosViewModel target = new RecalcUKPortfoliosViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            RecalcUKPortfoliosViewModel_Accessor target = new RecalcUKPortfoliosViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestIsRecalcuationFinished()
        {
            RecalcUKPortfoliosViewModel target = new RecalcUKPortfoliosViewModel();
            bool expected = false;
            bool actual;
            actual = target.IsRecalcuationFinished();
            
        }

        [TestMethod()]
        public void TestLoadContracts()
        {
            RecalcUKPortfoliosViewModel target = new RecalcUKPortfoliosViewModel();
            target.LoadContracts();
        }

        [TestMethod()]
        public void TestRecalcNextContract()
        {
            RecalcUKPortfoliosViewModel target = new RecalcUKPortfoliosViewModel();
            target.RecalcNextContract();
        }

        
    }
}
