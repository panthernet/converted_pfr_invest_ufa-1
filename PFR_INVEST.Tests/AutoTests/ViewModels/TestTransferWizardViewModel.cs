﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using PFR_INVEST.Proxy;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTransferWizardViewModel : BaseTest
    {

        [TestMethod()]
        public void TestTransferWizardViewModelConstructor()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true);
        }

        [TestMethod()]
        public void TestCreateRegister()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(false); 
            target.CreateRegister();
        }

        [TestMethod()]
        public void TestDivideSum()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            Decimal p_sum = new Decimal(); 
            target.DivideSum(p_sum);
        }

        [TestMethod()]
        public void TestEnsureAvailableList()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            target.EnsureAvailableList();
        }

        [TestMethod()]
        public void TestAvailableContractsList()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            List<TransferWizardViewModel.ModelContractRecord> expected = null; 
            List<TransferWizardViewModel.ModelContractRecord> actual;
            target.AvailableContractsList = expected;
            actual = target.AvailableContractsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrentTransferTypes()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            List<SPNOperation> actual;
            actual = target.CurrentTransferTypes;
        }

        [TestMethod()]
        public void TestMonthsList()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            List<Month> expected = null;
            List<Month> actual;
            target.MonthsList = expected;
            actual = target.MonthsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContractsList()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            List<TransferWizardViewModel.ModelContractRecord> expected = null; 
            List<TransferWizardViewModel.ModelContractRecord> actual;
            target.SelectedContractsList = expected;
            actual = target.SelectedContractsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedDate()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            DateTime expected = new DateTime(); 
            DateTime actual;
            target.SelectedDate = expected;
            actual = target.SelectedDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedMonth()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            Month expected = null;
            Month actual;
            target.SelectedMonth = expected;
            actual = target.SelectedMonth;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedTransferDirection()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            SPNDirection expected = null;
            SPNDirection actual;
            target.SelectedTransferDirection = expected;
            actual = target.SelectedTransferDirection;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedTransferType()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            SPNOperation expected = null;
            SPNOperation actual;
            target.SelectedTransferType = expected;
            actual = target.SelectedTransferType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedYear()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            Year expected = null;
            Year actual;
            target.SelectedYear = expected;
            actual = target.SelectedYear;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTransferDirections()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true); 
            List<SPNDirection> actual;
            actual = target.TransferDirections;
        }

        [TestMethod()]
        public void TestTransferTypes_PFRtoUK()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true);
            List<SPNOperation> actual;
            actual = target.TransferTypes_PFRtoUK;
        }

        [TestMethod()]
        public void TestTransferTypes_UKtoPFR()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(true);
            List<SPNOperation> actual;
            actual = target.TransferTypes_UKtoPFR;
        }

        [TestMethod()]
        public void TestYearsList()
        {
            TransferWizardViewModel target = new TransferWizardViewModel(false);
            List<Year> expected = null;
            List<Year> actual;
            target.YearsList = expected;
            actual = target.YearsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
