﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF14ViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF14ViewModelConstructor()
        {
            long id = 1;
            F14ViewModel target = new F14ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F14ViewModel target = new F14ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    F14ViewModel_Accessor target = new F14ViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshNetWealthsList()
        {
            long id = 1;
            F14ViewModel target = new F14ViewModel(id);
            target.RefreshNetWealthsList();
        }

        [TestMethod()]
        public void TestCalcDateTime()
        {
            long id = 1;
            F14ViewModel target = new F14ViewModel(id);
            DateTime actual;
            actual = target.CalcDateTime;
        }

       

        [TestMethod()]
        public void TestNetWealthsList()
        {
            long id = 0;
            F14ViewModel target = new F14ViewModel(id);
            List<NetWealthsInnerListItem> expected = null;
            List<NetWealthsInnerListItem> actual;
            target.NetWealthsList = expected;
            actual = target.NetWealthsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
