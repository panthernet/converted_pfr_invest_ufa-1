﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPortfoliosListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestPortfoliosListViewModelConstructor()
        {
            DUE_TYPE type = DUE_TYPE.Normal;
            PortfoliosListViewModel target = new PortfoliosListViewModel(type);
        }

        [TestMethod()]
        public void TestPortfoliosListViewModelConstructor1()
        {
            PortfoliosListViewModel target = new PortfoliosListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            PortfoliosListViewModel_Accessor target = new PortfoliosListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            PortfoliosListViewModel_Accessor target = new PortfoliosListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            PortfoliosListViewModel_Accessor target = new PortfoliosListViewModel_Accessor(); 
            DUE_TYPE type = new DUE_TYPE(); 
            target.ExecuteRefresh(type);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            PortfoliosListViewModel_Accessor target = new PortfoliosListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestPortfolioList()
        {
            PortfoliosListViewModel target = new PortfoliosListViewModel(); 
            List<PortfolioListItemFull> expected = null; 
            List<PortfolioListItemFull> actual;
            target.PortfolioList = expected;
            actual = target.PortfolioList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefresh()
        {
            PortfoliosListViewModel target = new PortfoliosListViewModel(); 
            ICommand actual;
            actual = target.Refresh;
        }

        [TestMethod()]
        public void TestSelectedItem()
        {
            PortfoliosListViewModel target = new PortfoliosListViewModel(); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.SelectedItem = expected;
            actual = target.SelectedItem;
            Assert.AreEqual(expected, actual);
        }
    }
}
