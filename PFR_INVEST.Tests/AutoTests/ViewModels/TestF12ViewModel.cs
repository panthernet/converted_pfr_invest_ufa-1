﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF12ViewModel : BaseTest
    {
        [TestMethod()]
        public void TestF12ViewModelConstructor()
        {
            long id = 1;
            F12ViewModel target = new F12ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F12ViewModel target = new F12ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PrivateObject param0 = null;
            F12ViewModel_Accessor target = new F12ViewModel_Accessor(1);
            target.ExecuteSave();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshNetWealthsList()
        {
            PrivateObject param0 = null;
            F12ViewModel_Accessor target = new F12ViewModel_Accessor(1);
            target.RefreshNetWealthsList();
        }

        [TestMethod()]
        public void TestCalcDateTime()
        {
            long id = 1;
            F12ViewModel target = new F12ViewModel(id);
            DateTime expected = DateTime.Today.AddDays(-3);
            DateTime actual;
            target.CalcDateTime = expected;
            actual = target.CalcDateTime;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestNetWealthsList()
        {
            long id = 1;
            F12ViewModel target = new F12ViewModel(id);
            List<NetWealthsInnerListItem> expected = null;
            List<NetWealthsInnerListItem> actual;
            target.NetWealthsList = expected;
            actual = target.NetWealthsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
