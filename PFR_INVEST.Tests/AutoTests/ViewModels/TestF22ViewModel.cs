﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Proxy;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF22ViewModel : BaseTest
    {

        [TestMethod()]
        public void TestF22ViewModelConstructor()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCreateList()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            DBEntity[] entities = null;
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            actual = target.CreateList(entities);
            //Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        public void TestGroup10List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group10List = expected;
            actual = target.Group10List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup11List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group11List = expected;
            actual = target.Group11List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup12List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group12List = expected;
            actual = target.Group12List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup1List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group1List = expected;
            actual = target.Group1List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup2List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group2List = expected;
            actual = target.Group2List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup3List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group3List = expected;
            actual = target.Group3List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup4List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group4List = expected;
            actual = target.Group4List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup5List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group5List = expected;
            actual = target.Group5List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup6List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group6List = expected;
            actual = target.Group6List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup7List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group7List = expected;
            actual = target.Group7List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup8List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group8List = expected;
            actual = target.Group8List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGroup9List()
        {
            long id = 0;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.Group9List = expected;
            actual = target.Group9List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItem()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            string columnName = string.Empty;
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        public void TestReportType()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.ReportType = expected;
            actual = target.ReportType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSubGroup1List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.SubGroup1List = expected;
            actual = target.SubGroup1List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSubGroup2List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.SubGroup2List = expected;
            actual = target.SubGroup2List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSubGroup3List()
        {
            long id = 1;
            F22ViewModel target = new F22ViewModel(id);
            List<F22GroupListItem> expected = null;
            List<F22GroupListItem> actual;
            target.SubGroup3List = expected;
            actual = target.SubGroup3List;
            Assert.AreEqual(expected, actual);
        }
    }
}
