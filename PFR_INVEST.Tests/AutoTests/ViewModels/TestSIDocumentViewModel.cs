﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSIDocumentViewModel : BaseTest
    {


        [TestMethod()]
        public void TestSIDocumentViewModelConstructor()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
        }

        [TestMethod()]
        public void TestCanExecuteDeleteFile()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteDeleteFile();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteOpenAttachment()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteOpenAttachment();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSelectFile()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectFile();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteDeleteFile()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            target.ExecuteDeleteFile();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteOpenAttachment()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);

            target.ExecuteOpenAttachment();
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    SIDocumentViewModel_Accessor target = new SIDocumentViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

      

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestUpdateAttachment()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            target.UpdateAttachment();
        }

        

        [TestMethod()]
        public void TestAddInfoList()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.AddInfoList = expected;
            actual = target.AddInfoList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDeleteFile()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            ICommand actual;
            actual = target.DeleteFile;
        }

        [TestMethod()]
        public void TestDocClassList()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DocClassList = expected;
            actual = target.DocClassList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDocInfo()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            string actual;
            actual = target.DocInfo;
        }

        [TestMethod()]
        public void TestFIOList()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.FIOList = expected;
            actual = target.FIOList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsRegNumber()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            bool expected = false;
            bool actual;
            target.IsRegNumber = expected;
            actual = target.IsRegNumber;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsSimpleNumber()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            bool expected = false;
            bool actual;
            target.IsSimpleNumber = expected;
            actual = target.IsSimpleNumber;
            Assert.AreEqual(expected, actual);
        }

       
        [TestMethod()]
        public void TestOpenAttachment()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            ICommand actual;
            actual = target.OpenAttachment;
        }

        [TestMethod()]
        public void TestRNType()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            SIDocumentViewModel.REGNUMBER_TYPE expected = new SIDocumentViewModel.REGNUMBER_TYPE();
            SIDocumentViewModel.REGNUMBER_TYPE actual;
            target.RNType = expected;
            actual = target.RNType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectFile()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            ICommand actual;
            actual = target.SelectFile;
        }

        [TestMethod()]
        public void TestSelectedAddInfo()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedAddInfo = expected;
            actual = target.SelectedAddInfo;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedDocClass()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedDocClass = expected;
            actual = target.SelectedDocClass;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedFIO()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedFIO = expected;
            actual = target.SelectedFIO;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedFileName()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            string expected = string.Empty;
            string actual;
            target.SelectedFileName = expected;
            actual = target.SelectedFileName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedUK()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedUK = expected;
            actual = target.SelectedUK;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUKList()
        {
            long id = 1;
            VIEWMODEL_STATES state = VIEWMODEL_STATES.Edit;
            SIDocumentViewModel target = new SIDocumentViewModel(id, state);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.UKList = expected;
            actual = target.UKList;
            Assert.AreEqual(expected, actual);
        }
    }
}
