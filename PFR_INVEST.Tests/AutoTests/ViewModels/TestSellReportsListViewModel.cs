﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSellReportsListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestSellReportsListViewModelConstructor()
        {
            SellReportsListViewModel target = new SellReportsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            SellReportsListViewModel_Accessor target = new SellReportsListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            SellReportsListViewModel_Accessor target = new SellReportsListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            SellReportsListViewModel_Accessor target = new SellReportsListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            SellReportsListViewModel_Accessor target = new SellReportsListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

      
        [TestMethod()]
        public void TestList()
        {
            SellReportsListViewModel target = new SellReportsListViewModel(); 
            List<SellReportItem> expected = null; 
            List<SellReportItem> actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefresh()
        {
            SellReportsListViewModel target = new SellReportsListViewModel(); 
            ICommand actual;
            actual = target.Refresh;
        }
    }
}
