﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDepositsListViewModel : BaseTest
    {



        [TestMethod()]
        public void TestDepositsListViewModelConstructor()
        {
            DepositsListViewModel target = new DepositsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            DepositsListViewModel_Accessor target = new DepositsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            DepositsListViewModel_Accessor target = new DepositsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            DepositsListViewModel_Accessor target = new DepositsListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            DepositsListViewModel_Accessor target = new DepositsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        //[TestMethod()]
        //public void TestReturnDeposit()
        //{
        //    DepositsListViewModel target = new DepositsListViewModel();
        //    long id = 1;
        //    target.ReturnDeposit(id);
        //}

        [TestMethod()]
        public void TestDepositsList()
        {
            DepositsListViewModel target = new DepositsListViewModel();
            List<DepositListItem> expected = null;
            List<DepositListItem> actual;
            target.DepositsList = expected;
            actual = target.DepositsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefresh()
        {
            DepositsListViewModel target = new DepositsListViewModel();
            ICommand actual;
            actual = target.Refresh;
        }
    }
}
