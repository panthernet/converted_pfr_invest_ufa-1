﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestGivenFinRegisterViewModel : BaseTest
    {

        [TestMethod()]
        public void TestGivenFinRegisterViewModelConstructor()
        {
            GivenFinRegisterViewModel target = new GivenFinRegisterViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            GivenFinRegisterViewModel target = new GivenFinRegisterViewModel();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            GivenFinRegisterViewModel_Accessor target = new GivenFinRegisterViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            GivenFinRegisterViewModel target = new GivenFinRegisterViewModel();
            long fr_id = 1;
            target.Load(fr_id);
        }

      
    }
}
