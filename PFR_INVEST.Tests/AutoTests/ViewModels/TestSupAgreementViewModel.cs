﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSupAgreementViewModel : BaseTest
    {

        [TestMethod()]
        public void TestSupAgreementViewModelConstructor()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SupAgreementViewModel target = new SupAgreementViewModel(id, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SupAgreementViewModel target = new SupAgreementViewModel(id, action); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    SupAgreementViewModel_Accessor target = new SupAgreementViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

       

     
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestisLenCorrect()
        {
            object o = null; 
            int maxLen = 0; 
            bool expected = false; 
            bool actual;
            actual = SupAgreementViewModel_Accessor.isLenCorrect(o, maxLen);
            Assert.AreEqual(expected, actual);
        }

      
        [TestMethod()]
        public void TestUKFormalName()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SupAgreementViewModel target = new SupAgreementViewModel(id, action); 
            string actual;
            actual = target.UKFormalName;
        }

        [TestMethod()]
        public void TestUKFullName()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SupAgreementViewModel target = new SupAgreementViewModel(id, action); 
            string actual;
            actual = target.UKFullName;
        }
    }
}
