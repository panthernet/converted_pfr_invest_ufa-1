﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSIContractItem : BaseTest
    {

        [TestMethod()]
        public void TestSIContractItemConstructor()
        {
            DBEntity ent = new DBEntity(WCFClient.Client.GetSIContract(1));
            SIContractItem target = new SIContractItem(ent,1);
        }

        [TestMethod()]
        public void TestContrDate()
        {
            DBEntity ent = new DBEntity(WCFClient.Client.GetSIContract(1));
            SIContractItem target = new SIContractItem(ent,1);
            DateTime actual;
            actual = target.ContrDate;
        }

        [TestMethod()]
        public void TestContractID()
        {
            DBEntity ent = new DBEntity(WCFClient.Client.GetSIContract(1));
            SIContractItem target = new SIContractItem(ent,1);
            long actual;
            actual = target.ContractID;
        }

        [TestMethod()]
        public void TestContractName()
        {
            DBEntity ent = new DBEntity(WCFClient.Client.GetSIContract(1));
            SIContractItem target = new SIContractItem(ent,1);
            string actual;
            actual = target.ContractName;
        }

        [TestMethod()]
        public void TestContragentType()
        {
            DBEntity ent = new DBEntity(WCFClient.Client.GetSIContract(1));
            SIContractItem target = new SIContractItem(ent,1);
            string actual;
            actual = target.ContragentType;
        }

        [TestMethod()]
        public void TestDebitPercent()
        {
            DBEntity ent = new DBEntity(WCFClient.Client.GetSIContract(1));
            SIContractItem target = new SIContractItem(ent,1);
            string actual;
            actual = target.DebitPercent;
        }

        [TestMethod()]
        public void TestIncomePercent()
        {
            DBEntity ent = new DBEntity(WCFClient.Client.GetSIContract(1));
            SIContractItem target = new SIContractItem(ent, 1);
            string actual;
            actual = target.IncomePercent;
        }

        [TestMethod()]
        public void TestName()
        {
            DBEntity ent = null;
            SIContractItem target = new SIContractItem(ent, 1);
            string actual;
            actual = target.Name;
        }

        [TestMethod()]
        public void TestSIID()
        {
            DBEntity ent = null;
            SIContractItem target = new SIContractItem(ent, 1);
            long actual;
            actual = target.SIID;
        }

        [TestMethod()]
        public void TestSIName()
        {
            DBEntity ent = null;
            SIContractItem target = new SIContractItem(ent, 1);
            string actual;
            actual = target.SIName;
        }
    }
}
