﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestCostsViewModel : BaseTest
    {


        [TestMethod()]
        public void TestCostsViewModelConstructor()
        {
            CostsViewModel target = new CostsViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            CostsViewModel target = new CostsViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            CostsViewModel_Accessor target = new CostsViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestEnsureRate()
        {
            CostsViewModel target = new CostsViewModel();
            object p = null;
            target.EnsureRate(p);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            CostsViewModel_Accessor target = new CostsViewModel_Accessor();
            target.ExecuteSave();
        }



        [TestMethod()]
        public void TestLoad()
        {
            CostsViewModel target = new CostsViewModel();
            long lId = 1;
            target.Load(lId);
        }
        
        [TestMethod()]
        public void TestAccount()
        {
            CostsViewModel target = new CostsViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBanksList()
        {
            CostsViewModel target = new CostsViewModel();
            List<DB2LegalEntityCard> expected = null;
            List<DB2LegalEntityCard> actual;
            target.BanksList = expected;
            actual = target.BanksList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrencyList()
        {
            CostsViewModel target = new CostsViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDatePP()
        {
            CostsViewModel target = new CostsViewModel();
            DateTime expected = new DateTime();
            DateTime actual;
            target.DatePP = expected;
            actual = target.DatePP;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDiDate()
        {
            CostsViewModel target = new CostsViewModel();
            string actual;
            actual = target.DiDate;
        }

       
        [TestMethod()]
        public void TestRate()
        {
            CostsViewModel target = new CostsViewModel();
            Decimal actual;
            actual = target.Rate;
        }

        [TestMethod()]
        public void TestSelectAccount()
        {
            CostsViewModel target = new CostsViewModel();
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedBank()
        {
            CostsViewModel target = new CostsViewModel();
            DB2LegalEntityCard expected = null;
            DB2LegalEntityCard actual;
            target.SelectedBank = expected;
            actual = target.SelectedBank;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedCurrency()
        {
            CostsViewModel target = new CostsViewModel();
            DBEntity expected = new DBEntity(new Dictionary<string,DBField>());
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumPP()
        {
            CostsViewModel target = new CostsViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.SumPP = expected;
            actual = target.SumPP;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            CostsViewModel target = new CostsViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotal()
        {
            CostsViewModel target = new CostsViewModel();
            Decimal actual;
            actual = target.Total;
        }
    }
}
