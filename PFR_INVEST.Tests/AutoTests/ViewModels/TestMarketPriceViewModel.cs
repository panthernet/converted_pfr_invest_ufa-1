﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestMarketPriceViewModel : BaseTest
    {


        [TestMethod()]
        public void TestMarketPriceViewModelConstructor()
        {
            long secID = 1;
            MarketPriceViewModel target = new MarketPriceViewModel(secID);
        }

        [TestMethod()]
        public void TestMarketPriceViewModelConstructor1()
        {
            MarketPriceViewModel target = new MarketPriceViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            MarketPriceViewModel target = new MarketPriceViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            MarketPriceViewModel_Accessor target = new MarketPriceViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            MarketPriceViewModel target = new MarketPriceViewModel();
            long lId = 1;
            target.Load(lId);
        }

        
        [TestMethod()]
        public void TestCurrencyVisible()
        {
            MarketPriceViewModel target = new MarketPriceViewModel();
            Visibility actual;
            actual = target.CurrencyVisible;
        }


        [TestMethod()]
        public void TestParentSecurity()
        {
            MarketPriceViewModel target = new MarketPriceViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.ParentSecurity = expected;
            actual = target.ParentSecurity;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRubVisible()
        {
            MarketPriceViewModel target = new MarketPriceViewModel();
            Visibility actual;
            actual = target.RubVisible;
        }
    }
}
