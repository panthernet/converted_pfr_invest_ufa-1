﻿using System.Windows.Input;
using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.Proxy.DataContracts;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestMarketCostListViewModel : BaseTest
    {

        [TestMethod()]
        public void TestMarketCostListViewModelConstructor()
        {
            MarketCostListViewModel target = new MarketCostListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            MarketCostListViewModel_Accessor target = new MarketCostListViewModel_Accessor();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            MarketCostListViewModel_Accessor target = new MarketCostListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshList()
        {
            MarketCostListViewModel target = new MarketCostListViewModel();
            ICommand actual;
            actual = target.RefreshList;
            
        }

      
        [TestMethod()]
        public void TestMarketCostList()
        {
            MarketCostListViewModel target = new MarketCostListViewModel();
            List<DB2MarketCostItem> expected = null;
            List<DB2MarketCostItem> actual;
            target.MarketCostList = expected;
            actual = target.MarketCostList;
            Assert.AreEqual(expected, actual);
        }
    }
}
