﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRatesListViewModel : BaseTest
    {

        [TestMethod()]
        public void TestRatesListViewModelConstructor()
        {
            RatesListViewModel target = new RatesListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            RatesListViewModel_Accessor target = new RatesListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            RatesListViewModel_Accessor target = new RatesListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            RatesListViewModel_Accessor target = new RatesListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            RatesListViewModel_Accessor target = new RatesListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestRatesList()
        {
            RatesListViewModel target = new RatesListViewModel();
            List<RatesListItem> expected = null;
            List<RatesListItem> actual;
            target.RatesList = expected;
            actual = target.RatesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            RatesListViewModel_Accessor target = new RatesListViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }
    }
}
