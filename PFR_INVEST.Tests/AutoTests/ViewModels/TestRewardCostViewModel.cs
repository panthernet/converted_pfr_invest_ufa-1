﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRewardCostViewModel : BaseTest
    {


        [TestMethod()]
        public void TestRewardCostViewModelConstructor()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RewardCostViewModel target = new RewardCostViewModel(id, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RewardCostViewModel target = new RewardCostViewModel(id, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    RewardCostViewModel_Accessor target = new RewardCostViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        


        [TestMethod()]
        public void TestUKFormalName()
        {
            long id = 0;
            VIEWMODEL_STATES action = new VIEWMODEL_STATES();
            RewardCostViewModel target = new RewardCostViewModel(id, action);
            string actual;
            actual = target.UKFormalName;
        }

        [TestMethod()]
        public void TestUKFullName()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            RewardCostViewModel target = new RewardCostViewModel(id, action);
            string actual;
            actual = target.UKFullName;
        }
    }
}
