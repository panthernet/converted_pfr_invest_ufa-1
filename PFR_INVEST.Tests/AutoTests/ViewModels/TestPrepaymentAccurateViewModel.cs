﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPrepaymentAccurateViewModel : BaseTest
    {


        [TestMethod()]
        public void TestPrepaymentAccurateViewModelConstructor()
        {
            PrepaymentViewModel parentVM = null; 
            PrepaymentAccurateViewModel target = new PrepaymentAccurateViewModel(parentVM);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PrepaymentViewModel parentVM = null; 
            PrepaymentAccurateViewModel target = new PrepaymentAccurateViewModel(parentVM); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    PrepaymentAccurateViewModel_Accessor target = new PrepaymentAccurateViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestLoad()
        {
            PrepaymentViewModel parentVM = null; 
            PrepaymentAccurateViewModel target = new PrepaymentAccurateViewModel(parentVM); 
            long lId = 1; 
            target.Load(lId);
        }

        

        [TestMethod()]
        public void TestChSumm()
        {
            PrepaymentViewModel parentVM = null; 
            PrepaymentAccurateViewModel target = new PrepaymentAccurateViewModel(parentVM); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.ChSumm = expected;
            actual = target.ChSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            PrepaymentViewModel parentVM = null; 
            PrepaymentAccurateViewModel target = new PrepaymentAccurateViewModel(parentVM); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestSrcAccount()
        {
            PrepaymentViewModel parentVM = null; 
            PrepaymentAccurateViewModel target = new PrepaymentAccurateViewModel(parentVM); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            PrepaymentViewModel parentVM = null; 
            PrepaymentAccurateViewModel target = new PrepaymentAccurateViewModel(parentVM); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }
    }
}
