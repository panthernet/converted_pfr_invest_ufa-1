﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF402ListItem : BaseTest
    {


        [TestMethod()]
        public void TestF402ListItemConstructor()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
        }

        [TestMethod()]
        public void TestAgrNum()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            string actual;
            actual = target.AgrNum;
        }

        [TestMethod()]
        public void TestDeliveryDate()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            DateTime actual;
            actual = target.DeliveryDate;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestMonth()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestOrderDate()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            DateTime actual;
            actual = target.OrderDate;
        }

        [TestMethod()]
        public void TestOrderNum()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            string actual;
            actual = target.OrderNum;
        }

        [TestMethod()]
        public void TestOrderSumm()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            string actual;
            actual = target.OrderSumm;
        }

        [TestMethod()]
        public void TestPayDate_Plan()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            DateTime actual;
            actual = target.PayDate_Plan;
        }

        [TestMethod()]
        public void TestPayDate_Real()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            DateTime actual;
            actual = target.PayDate_Real;
        }

        [TestMethod()]
        public void TestState()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            string actual;
            actual = target.State;
        }

        [TestMethod()]
        public void TestUKName()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            string actual;
            actual = target.UKName;
        }

        [TestMethod()]
        public void TestYear()
        {
            DBEntity ent = null;
            F402ListItem target = new F402ListItem(ent);
            string actual;
            actual = target.Year;
        }
    }
}
