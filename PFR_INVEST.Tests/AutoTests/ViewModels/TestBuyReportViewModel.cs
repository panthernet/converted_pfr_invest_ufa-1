﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBuyReportViewModel : BaseTest
    {



        [TestMethod()]
        public void TestBuyReportViewModelConstructor()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    BuyReportViewModel_Accessor target = new BuyReportViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestRefreshLists()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            target.RefreshLists();
        }

      

        [TestMethod()]
        public void TestCount()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            long expected = 12;
            long actual;
            target.Count = expected;
            actual = target.Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDateDepo()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            DateTime expected = DateTime.Today;
            DateTime actual;
            target.DateDepo = expected;
            actual = target.DateDepo;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDateOrder()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            DateTime expected = DateTime.Today;
            DateTime actual;
            target.DateOrder = expected;
            actual = target.DateOrder;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestExpended()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            Decimal expected = 12;
            Decimal actual;
            target.Expended = expected;
            actual = target.Expended;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsCurrency()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            bool actual;
            actual = target.IsCurrency;
        }

        
        [TestMethod()]
        public void TestNKD()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            Decimal expected = 12;
            Decimal actual;
            target.NKD = expected;
            actual = target.NKD;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNKDPerOneObl()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            Decimal expected = 12;
            Decimal actual;
            target.NKDPerOneObl = expected;
            actual = target.NKDPerOneObl;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNKDTotal()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            Decimal expected = 12;
            Decimal actual;
            target.NKDTotal = expected;
            actual = target.NKDTotal;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPaymentsExist()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            Visibility expected = new Visibility();
            Visibility actual;
            target.PaymentsExist = expected;
            actual = target.PaymentsExist;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPaymentsList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.PaymentsList = expected;
            actual = target.PaymentsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPrice()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            Decimal expected = 12;
            Decimal actual;
            target.Price = expected;
            actual = target.Price;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRate()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            Decimal expected = 12;
            Decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecuritiesList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.SecuritiesList = expected;
            actual = target.SecuritiesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSecurity()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedSecurity = expected;
            actual = target.SelectedSecurity;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSum()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            string expected = string.Empty;
            string actual;
            target.Sum = expected;
            actual = target.Sum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumNom()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            string expected = string.Empty;
            string actual;
            target.SumNom = expected;
            actual = target.SumNom;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumWithoutNKD()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            string expected = string.Empty;
            string actual;
            target.SumWithoutNKD = expected;
            actual = target.SumWithoutNKD;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumWithoutNKDRub()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            decimal expected = 12;
            decimal actual;
            target.SumWithoutNKDRub = expected.ToString();
            actual = Convert.ToDecimal(target.SumWithoutNKDRub);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYield()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            BuyReportViewModel target = new BuyReportViewModel(id, action);
            string expected = string.Empty;
            string actual;
            target.Yield = expected;
            actual = target.Yield;
            Assert.AreEqual(expected, actual);
        }
    }
}
