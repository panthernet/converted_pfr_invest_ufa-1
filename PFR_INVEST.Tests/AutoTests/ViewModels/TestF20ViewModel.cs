﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF20ViewModel : BaseTest
    {
        
        [TestMethod()]
        public void TestF20ViewModelConstructor()
        {
            long identity = 1; 
            F20ViewModel target = new F20ViewModel(identity);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long identity = 1; 
            F20ViewModel target = new F20ViewModel(identity); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshF20List()
        {
            long identity = 1;
            F20ViewModel target = new F20ViewModel(identity); 
            target.RefreshF20List();
        }

        [TestMethod()]
        public void TestF20Form()
        {
            long identity = 1; 
            F20ViewModel target = new F20ViewModel(identity); 
            F20FormItem expected = null; 
            F20FormItem actual;
            target.F20Form = expected;
            actual = target.F20Form;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestF20List()
        {
            long identity = 1; 
            F20ViewModel target = new F20ViewModel(identity); 
            List<F20ListItem> expected = null; 
            List<F20ListItem> actual;
            target.F20List = expected;
            actual = target.F20List;
            Assert.AreEqual(expected, actual);
        }

       
    }
}
