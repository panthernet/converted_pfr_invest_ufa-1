﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPrepaymentViewModel : BaseTest
    {




        [TestMethod()]
        public void TestPrepaymentViewModelConstructor()
        {
            PrepaymentViewModel target = new PrepaymentViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddDopAPS()
        {
            PrepaymentViewModel_Accessor target = new PrepaymentViewModel_Accessor(); 
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteAddDopAPS();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectDstAccount()
        {
            PrepaymentViewModel_Accessor target = new PrepaymentViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSelectDstAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectSrcAccount()
        {
            PrepaymentViewModel_Accessor target = new PrepaymentViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSelectSrcAccount();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PrepaymentViewModel_Accessor target = new PrepaymentViewModel_Accessor(); 
            target.ExecuteSave();
        }


        [TestMethod()]
        public void TestLoad()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

      

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestAddDopAPS()
        {
            PrepaymentViewModel_Accessor target = new PrepaymentViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.AddDopAPS = expected;
            actual = target.AddDopAPS;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopAPSs()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.DopAPSs = expected;
            actual = target.DopAPSs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopAPSsGrid()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            List<PrepaymentAccurateListItem> expected = null; 
            List<PrepaymentAccurateListItem> actual;
            target.DopAPSsGrid = expected;
            actual = target.DopAPSsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItem()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            string columnName = string.Empty; 
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSelectDstAccount()
        {
            PrepaymentViewModel_Accessor target = new PrepaymentViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.SelectDstAccount = expected;
            actual = target.SelectDstAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSelectSrcAccount()
        {
            PrepaymentViewModel_Accessor target = new PrepaymentViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.SelectSrcAccount = expected;
            actual = target.SelectSrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSrcAccount()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            PrepaymentViewModel target = new PrepaymentViewModel(); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }
    }
}
