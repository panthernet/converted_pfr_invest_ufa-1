﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;
using System.Windows.Input;
using System.Windows;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestERZLNotifyViewModel : BaseTest
    {

        [TestMethod()]
        public void TestERZLNotifyViewModelConstructor()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            List<DB2LegalEntityCard> npfs = null;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action, npfs);
        }

        [TestMethod()]
        public void TestERZLNotifyViewModelConstructor1()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
        }

        [TestMethod()]
        public void TestERZLNotifyViewModelConstructor2()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            List<ContragentListItem> cl_NPF2OTHER = null;
            List<ContragentListItem> cl_OTHER2NPF = null;
            Dictionary<string, DBField> pfr_acc = null;
            List<DB2LegalEntityCard> npfs = null;
            Dictionary<string, DBField> strct = null;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action, cl_NPF2OTHER, cl_OTHER2NPF, pfr_acc, npfs, strct);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddNPF2OTHER()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            List<ContragentListItem> cl_NPF2OTHER = null;
            List<ContragentListItem> cl_OTHER2NPF = null;
            Dictionary<string, DBField> pfr_acc = null;
            List<DB2LegalEntityCard> npfs = null;
            Dictionary<string, DBField> strct = null;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action, cl_NPF2OTHER, cl_OTHER2NPF, pfr_acc, npfs, strct);

            bool expected = true;
            bool actual;
            actual = target.CanExecuteAddNPF2OTHER();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddOTHER2NPF()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            List<ContragentListItem> cl_NPF2OTHER = null;
            List<ContragentListItem> cl_OTHER2NPF = null;
            Dictionary<string, DBField> pfr_acc = null;
            List<DB2LegalEntityCard> npfs = null;
            Dictionary<string, DBField> strct = null;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action, cl_NPF2OTHER, cl_OTHER2NPF, pfr_acc, npfs, strct);

            bool expected = true;
            bool actual;
            actual = target.CanExecuteAddOTHER2NPF();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteOK()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            List<ContragentListItem> cl_NPF2OTHER = null;
            List<ContragentListItem> cl_OTHER2NPF = null;
            Dictionary<string, DBField> pfr_acc = null;
            List<DB2LegalEntityCard> npfs = null;
            Dictionary<string, DBField> strct = null;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action, cl_NPF2OTHER, cl_OTHER2NPF, pfr_acc, npfs, strct);



            bool expected = false;
            bool actual;
            actual = target.CanExecuteOK();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

       
        
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteOK()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            target.ExecuteOK();
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    ERZLNotifyViewModel_Accessor target = new ERZLNotifyViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestInit()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
           
            target.Init(zl_id, action);
        }

        [TestMethod()]
        public void TestIsReverse()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            ERZLNotifyViewModel vm = null;
            bool expected = false;
            bool actual;
            actual = target.IsReverse(vm);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefreshContragentsLists()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            target.RefreshContragentsLists();
        }

        [TestMethod()]
        public void TestRefreshERZL()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            long erzlID = 0;
            target.RefreshERZL(erzlID);
        }

        [TestMethod()]
        public void TestRefreshLegalEntitiesList()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            target.RefreshLegalEntitiesList();
        }

        [TestMethod()]
        public void TestRefreshNots()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            target.RefreshNots();
        }

        /*[TestMethod()]
        public void TestUpdateInnerReverse()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            List<ERZLNotifyViewModel> vmList = null;
            target.UpdateInnerReverse(vmList);
        }

        [TestMethod()]
        public void TestUpdateInnerReverse1()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            target.UpdateInnerReverse();
        }*/

        [TestMethod()]
        public void TestAddNPF2OTHER()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            ICommand actual;
            actual = target.AddNPF2OTHER;
        }

        [TestMethod()]
        public void TestAddOTHER2NPF()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            ICommand actual;
            actual = target.AddOTHER2NPF;
        }

        [TestMethod()]
        public void TestCompany()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            string expected = string.Empty;
            string actual;
            target.Company = expected;
            actual = target.Company;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContent()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            string expected = string.Empty;
            string actual;
            target.Content = expected;
            actual = target.Content;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentsList_NPF2OTHER()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            List<ContragentListItem> expected = null;
            List<ContragentListItem> actual;
            target.ContragentsList_NPF2OTHER = expected;
            actual = target.ContragentsList_NPF2OTHER;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentsList_OTHER2NPF()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            List<ContragentListItem> expected = null;
            List<ContragentListItem> actual;
            target.ContragentsList_OTHER2NPF = expected;
            actual = target.ContragentsList_OTHER2NPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstNPF()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            DB2ContragentCard expected = null;
            DB2ContragentCard actual;
            target.DstNPF = expected;
            actual = target.DstNPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestF_NPF_Count()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            long expected = 12;
            long actual;
            target.F_NPF_Count = expected;
            actual = target.F_NPF_Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestF_PFR_Count()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            long expected = 12;
            long actual;
            target.F_PFR_Count = expected;
            actual = target.F_PFR_Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestHasReverse()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            bool expected = false;
            bool actual;
            target.HasReverse = expected;
            actual = target.HasReverse;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            string expected = string.Empty;
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsEdit()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            Visibility expected = Visibility.Collapsed;
            Visibility actual;
            target.IsEdit = expected;
            actual = target.IsEdit;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsEditBool()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            bool actual;
            actual = target.IsEditBool;
        }

       

        [TestMethod()]
        public void TestNPF2OTHEREdit()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            bool actual;
            actual = target.NPF2OTHEREdit;
        }

        [TestMethod()]
        public void TestNPF2PFREdit()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            bool actual;
            actual = target.NPF2PFREdit;
        }

        [TestMethod()]
        public void TestNPFList()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            List<DB2LegalEntityCard> expected = null;
            List<DB2LegalEntityCard> actual;
            target.NPFList = expected;
            actual = target.NPFList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNotifyNum()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            long expected = 0;
            long actual;
            target.NotifyNum = expected;
            actual = target.NotifyNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNots()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.Nots = expected;
            actual = target.Nots;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOKCommand()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            ICommand actual;
            actual = target.OKCommand;
        }

        [TestMethod()]
        public void TestOTHER2NPFEdit()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            bool actual;
            actual = target.OTHER2NPFEdit;
        }

        [TestMethod()]
        public void TestPFR2NPFEdit()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            bool actual;
            actual = target.PFR2NPFEdit;
        }

        [TestMethod()]
        public void TestSelectedNPF()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            DB2LegalEntityCard expected = null;
            DB2LegalEntityCard actual;
            target.SelectedNPF = expected;
            actual = target.SelectedNPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestT_NPF_Count()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            long expected = 12;
            long actual;
            target.T_NPF_Count = expected;
            actual = target.T_NPF_Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestT_PFR_Count()
        {
            long zl_id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ERZLNotifyViewModel target = new ERZLNotifyViewModel(zl_id, action);
            long expected = 12;
            long actual;
            target.T_PFR_Count = expected;
            actual = target.T_PFR_Count;
            Assert.AreEqual(expected, actual);
        }
    }
}
