﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSecurityViewModel : BaseTest
    {



        [TestMethod()]
        public void TestSecurityViewModelConstructor()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action);
        }

      
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanDeletePayment()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            bool expected = false; 
            bool actual;
            actual = target.CanDeletePayment();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteDeletePayment()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            target.ExecuteDeletePayment();
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    SecurityViewModel_Accessor target = new SecurityViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            target.RefreshLists();
        }

       

        [TestMethod()]
        public void TestViewPayment()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            long paymentID = 0; 
            target.ViewPayment(paymentID);
        }

        [TestMethod()]
        public void TestCurrencyList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrencyText()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            string expected = string.Empty; 
            string actual;
            target.CurrencyText = expected;
            actual = target.CurrencyText;
            Assert.AreEqual(expected, actual);
        }

     

        [TestMethod()]
        public void TestMarketPricesList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            List<MarketPriceListItem> expected = null; 
            List<MarketPriceListItem> actual;
            target.MarketPricesList = expected;
            actual = target.MarketPricesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPaymentsList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            List<PaymentItem> expected = null; 
            List<PaymentItem> actual;
            target.PaymentsList = expected;
            actual = target.PaymentsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecKindText()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            string expected = string.Empty; 
            string actual;
            target.SecKindText = expected;
            actual = target.SecKindText;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecurityKindList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.SecurityKindList = expected;
            actual = target.SecurityKindList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedCurrency()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPayment()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            PaymentItem expected = null; 
            PaymentItem actual;
            target.SelectedPayment = expected;
            actual = target.SelectedPayment;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSecKind()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityViewModel target = new SecurityViewModel(id, action); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedSecKind = expected;
            actual = target.SelectedSecKind;
            //Assert.AreEqual(expected, actual);
        }
    }
}
