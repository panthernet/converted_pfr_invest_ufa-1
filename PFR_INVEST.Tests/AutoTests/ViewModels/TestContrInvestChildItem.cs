﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestContrInvestChildItem : BaseTest
    {


        [TestMethod()]
        public void TestContrInvestChildItemConstructor()
        {
            DBEntity ent = null;
            ContrInvestChildItem target = new ContrInvestChildItem(ent);
        }

        [TestMethod()]
        public void TestEntity()
        {
            DBEntity ent = null;
            ContrInvestChildItem target = new ContrInvestChildItem(ent);
            DBEntity actual;
            actual = target.Entity;
        }

        [TestMethod()]
        public void TestMax()
        {
            DBEntity ent = null;
            ContrInvestChildItem target = new ContrInvestChildItem(ent);
            long expected = -1;
            long actual;
            target.Max = expected;
            actual = target.Max;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMin()
        {
            DBEntity ent = null;
            ContrInvestChildItem target = new ContrInvestChildItem(ent);
            long expected = -1;
            long actual;
            target.Min = expected;
            actual = target.Min;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYear()
        {
            DBEntity ent = null;
            ContrInvestChildItem target = new ContrInvestChildItem(ent);
            string expected = string.Empty;
            string actual;
            target.Year = expected;
            actual = target.Year;
            Assert.AreEqual(expected, actual);
        }
    }
}
