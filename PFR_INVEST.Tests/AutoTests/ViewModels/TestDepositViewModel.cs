﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDepositViewModel : BaseTest
    {


        [TestMethod()]
        public void TestDepositViewModelConstructor()
        {
            long portfolioID = 1;
            long depClaimID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(portfolioID, depClaimID, action);
        }

        [TestMethod()]
        public void TestDepositViewModelConstructor1()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    DepositViewModel_Accessor target = new DepositViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRecalcFields()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            target.RecalcFields();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            target.RefreshLists();
        }

        //[TestMethod()]
        //public void TestReturn()
        //{
        //    long id = 1;
        //    VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
        //    DepositViewModel target = new DepositViewModel(id, action);
        //    target.Return();
        //}

      

        [TestMethod()]
        public void TestBanksList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            List<DB2LegalEntityCard> expected = null;
            List<DB2LegalEntityCard> actual;
            target.BanksList = expected;
            actual = target.BanksList;
            Assert.AreEqual(expected, actual);
        }

       
        [TestMethod()]
        public void TestPortfoliosList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.PortfoliosList = expected;
            actual = target.PortfoliosList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReturnDate()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            string expected = string.Empty;
            string actual;
            target.ReturnDate = expected;
            actual = target.ReturnDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedBank()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            DB2LegalEntityCard expected = null;
            DB2LegalEntityCard actual;
            target.SelectedBank = expected;
            actual = target.SelectedBank;
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedPortfolio()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedPortfolio = expected;
            actual = target.SelectedPortfolio;
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStatus()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            DepositViewModel target = new DepositViewModel(id, action);
            string expected = string.Empty;
            string actual;
            target.Status = expected;
            actual = target.Status;
            Assert.AreEqual(expected, actual);
        }
    }
}
