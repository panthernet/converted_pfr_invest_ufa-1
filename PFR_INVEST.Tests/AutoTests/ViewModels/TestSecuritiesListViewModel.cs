﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSecuritiesListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestSecuritiesListViewModelConstructor()
        {
            SecuritiesListViewModel target = new SecuritiesListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            SecuritiesListViewModel_Accessor target = new SecuritiesListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            SecuritiesListViewModel_Accessor target = new SecuritiesListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            SecuritiesListViewModel_Accessor target = new SecuritiesListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            SecuritiesListViewModel_Accessor target = new SecuritiesListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestRefresh()
        {
            SecuritiesListViewModel target = new SecuritiesListViewModel(); 
            ICommand actual;
            actual = target.Refresh;
        }

        [TestMethod()]
        public void TestSecuritiesList()
        {
            SecuritiesListViewModel target = new SecuritiesListViewModel(); 
            List<SecurityItem> expected = null; 
            List<SecurityItem> actual;
            target.SecuritiesList = expected;
            actual = target.SecuritiesList;
            Assert.AreEqual(expected, actual);
        }
    }
}
