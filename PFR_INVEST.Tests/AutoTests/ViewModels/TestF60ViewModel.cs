﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF60ViewModel : BaseTest
    {



        [TestMethod()]
        public void TestF60ViewModelConstructor()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    F60ViewModel_Accessor target = new F60ViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            target.RefreshLists();
        }

      
        [TestMethod()]
        public void TestContractDate()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            DateTime expected = new DateTime();
            DateTime actual;
            target.ContractDate = expected;
            actual = target.ContractDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContractsList4NPF()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.ContractsList4NPF = expected;
            actual = target.ContractsList4NPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDate1()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string expected = DateTime.Today.ToShortDateString();
            string actual;
            target.Date1 = expected;
            actual = target.Date1;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDate2()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.Date2 = expected;
            actual = target.Date2;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFullName()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.FullName = expected;
            actual = target.FullName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        public void TestPortfolioName()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.PortfolioName = expected;
            actual = target.PortfolioName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestQuartersList()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            List<int> expected = null;
            List<int> actual;
            target.QuartersList = expected;
            actual = target.QuartersList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContract()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedContract = expected;
            actual = target.SelectedContract;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedQuarter()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            int expected = -1;
            int actual;
            target.SelectedQuarter = expected;
            actual = target.SelectedQuarter;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedYear()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedYear = expected;
            actual = target.SelectedYear;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYearsList()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.YearsList = expected;
            actual = target.YearsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Testr11()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r11;
        }

        [TestMethod()]
        public void Testr12()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r12;
        }

        [TestMethod()]
        public void Testr21()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r21;
        }

        [TestMethod()]
        public void Testr22()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r22;
        }

        [TestMethod()]
        public void Testr31()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r31;
        }

        [TestMethod()]
        public void Testr32()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r32;
        }

        [TestMethod()]
        public void Testr41()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r41;
        }

        [TestMethod()]
        public void Testr42()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r42;
        }

        [TestMethod()]
        public void Testr51()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r51;
        }

        [TestMethod()]
        public void Testr52()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r52;
        }

        [TestMethod()]
        public void Testr61()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r61;
        }

        [TestMethod()]
        public void Testr62()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r62;
        }

        [TestMethod()]
        public void Testr71()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r71;
        }

        [TestMethod()]
        public void Testr72()
        {
            long id = 1;
            F60ViewModel target = new F60ViewModel(id);
            string actual;
            actual = target.r72;
        }
    }
}
