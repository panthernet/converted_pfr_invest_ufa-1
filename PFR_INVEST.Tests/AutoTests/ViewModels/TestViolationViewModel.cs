﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestViolationViewModel : BaseTest
    {


        [TestMethod()]
        public void TestViolationViewModelConstructor()
        {
            long id = 1; 
            ViolationViewModel target = new ViolationViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1; 
            ViolationViewModel target = new ViolationViewModel(id); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    ViolationViewModel_Accessor target = new ViolationViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            ViolationViewModel target = new ViolationViewModel(id); 
            target.RefreshLists();
        }
    }
}
