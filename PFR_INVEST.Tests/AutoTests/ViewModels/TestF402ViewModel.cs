﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF402ViewModel : BaseTest
    {



        [TestMethod()]
        public void TestF402ViewModelConstructor()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshControlGroup()
        {
            F402ViewModel target = new F402ViewModel(1);
            target.RefreshControlGroup();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            target.RefreshLists();
        }

       
        [TestMethod()]
        public void TestFactDate()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            string actual;
            actual = target.FactDate;
        }

        [TestMethod()]
        public void TestGetDate()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            string actual;
            actual = target.GetDate;
        }

        [TestMethod()]
        public void TestINN()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestITOGO()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.ITOGO = expected;
            actual = target.ITOGO;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsAddressPopupVisible()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            bool expected = false;
            bool actual;
            target.IsAddressPopupVisible = expected;
            actual = target.IsAddressPopupVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsOrderDeliveredPopupVisible()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            bool expected = false;
            bool actual;
            target.IsOrderDeliveredPopupVisible = expected;
            actual = target.IsOrderDeliveredPopupVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsOrderPaidPopupVisible()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            bool expected = false;
            bool actual;
            target.IsOrderPaidPopupVisible = expected;
            actual = target.IsOrderPaidPopupVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsOrderSendingPopupVisible()
        {
            long id = 1;
            F402ViewModel target = new F402ViewModel(id);
            bool expected = false;
            bool actual;
            target.IsOrderSendingPopupVisible = expected;
            actual = target.IsOrderSendingPopupVisible;
            Assert.AreEqual(expected, actual);
        }

       

        [TestMethod()]
        public void TestMonth()
        {
            long id = 0;
            F402ViewModel target = new F402ViewModel(id);
            string expected = string.Empty;
            string actual;
            target.Month = expected;
            actual = target.Month;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSendDate()
        {
            long id = 0;
            F402ViewModel target = new F402ViewModel(id);
            string actual;
            actual = target.SendDate;
        }

        [TestMethod()]
        public void TestSetDate()
        {
            long id = 0;
            F402ViewModel target = new F402ViewModel(id);
            string actual;
            actual = target.SetDate;
        }
    }
}
