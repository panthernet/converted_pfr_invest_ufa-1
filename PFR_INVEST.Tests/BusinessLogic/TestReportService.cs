﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.BusinessLogic
{
	[TestClass]
	public class TestReportService : BaseTest
	{
		[TestMethod]
		public void TestIPUKTotal()
		{
			using (PerformanceChecker.New())
			{
			    var prompt = new ReportIPUKPrompt { ContractID = 100, DateTo = DateTime.Today, DateFrom = new DateTime(DateTime.Today.Year - 1, 1, 1) };
			    GetClient().Client.CreateReportIPUKTotal(prompt);
			}
		}

		[TestMethod]
		public void TestIPUK()
		{
			using (PerformanceChecker.New())
			{
			    var prompt = new ReportIPUKPrompt { ContractID = 1347, DateTo = DateTime.Today, DateFrom = new DateTime(DateTime.Today.Year - 1, 1, 1) };
			    GetClient().Client.CreateReportIPUK(prompt);
			}
		}

		[TestMethod]
		public void TestDeposits()
		{
			using (PerformanceChecker.New())
			{
				var prompt = new ReportDepositsPrompt { DateTo = DateTime.Today, DateFrom = new DateTime(DateTime.Today.Year - 1, 1, 1), UseSettleDate = true };
				var file = GetClient().Client.CreateReportDeposits(prompt);
			}
		}


		[TestMethod]
		public void TestBalanceUK()
		{
			using (PerformanceChecker.New())
			{
				var prompt = new ReportBalanceUKPrompt { Date = DateTime.Now };
				var file = GetClient().Client.CreateReportBalanceUK(prompt);
			}
		}


		[TestMethod]
		public void TestRSASCA()
		{
			//До 2007
			using (PerformanceChecker.New())
			{
				var prompt = new ReportRSASCAPrompt { Date = new DateTime(2006, 1, 1) };
				var file = GetClient().Client.CreateReportRSASCA(prompt);
			}

			//После 2007
			using (PerformanceChecker.New())
			{
				var prompt = new ReportRSASCAPrompt { Date = DateTime.Now };
				var file = GetClient().Client.CreateReportRSASCA(prompt);
			}
		}

		[TestMethod]
		public void TestReportF060()
		{
			using (PerformanceChecker.New())
			{
				ReportF060Prompt prompt = new ReportF060Prompt { YearID = 13, Quarter = 4, Units = 1000, Type = Document.Types.SI };
				var file = GetClient().Client.CreateReportF060(prompt);
			}
		}

		[TestMethod]
		public void TestReportF070()
		{
			using (PerformanceChecker.New())
			{
				ReportF070Prompt prompt = new ReportF070Prompt { YearID = 13, Quarter = 4, Units = 1000, Type = Document.Types.SI };
				var file = GetClient().Client.CreateReportF070(prompt);
			}
		}
		[TestMethod]
		public void TestReportProfitability()
		{
			using (PerformanceChecker.New())
			{
				ReportProfitabilityPrompt prompt = new ReportProfitabilityPrompt { YearID = 13, Quarter = 4, Type = Document.Types.SI };
				var file = GetClient().Client.CreateReportProfitability(prompt);
			}
		}

		[TestMethod]
		public void TestReportOwnedFounds()
		{
			using (PerformanceChecker.New())
			{
				ReportOwnedFoundsPrompt prompt = new ReportOwnedFoundsPrompt { YearID = 13, Quarter = 4, Type = Document.Types.SI };
				var file = GetClient().Client.CreateReportOwnedFounds(prompt);
			}
		}

	}
}
