﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Tests.Shared;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.Tests.BusinessLogic
{
    [TestClass]
    public class TestXMLParse : BaseTest
    {
        [TestMethod]
        public void TestXMLParsePDX02()
        {
            var path = @"PDX02.xml";
            //string shema = @"PDX02.xsd";

            var serializer = new XmlSerializer(typeof(DocumentPDX02));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX02.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                var doc = (DocumentPDX02)serializer.Deserialize(reader);

                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Info.DateValue);
                Assert.AreEqual("001727418/BL", doc.Info.Number);
                Assert.AreEqual("MM0000100000", doc.Info.SenderID);
                Assert.AreEqual("MD0356200000", doc.Info.RecieverID);
                Assert.AreEqual("PDX02", doc.Info.TypeID);
                Assert.AreEqual("1.0", doc.Body.Version);
                Assert.AreEqual(4, doc.Body.Records.Count());

                var rec = doc.Body.Records[0];
                Assert.AreEqual("MC0007400000", rec.FIRMID);
                Assert.AreEqual("ВТБ 24", rec.FIRMNAME);
                Assert.AreEqual(123130759, rec.RECNUM);
                Assert.AreEqual("PFDEP_T028DT", rec.SECURITYID);
                Assert.AreEqual(5.6M, rec.RATE);
                Assert.AreEqual(5000000000M, rec.AMOUNT);
                Assert.AreEqual(21479452.05M, rec.PAYMENT);
                Assert.AreEqual(DateTime.Parse("2013-08-08"), rec.SETTLEDATE);
                Assert.AreEqual(DateTime.Parse("2013-09-05"), rec.RETURNDATE);
            }
        }

        [TestMethod]
        public void TestXMLParsePDX06()
        {
            var path = @"PDX06.xml";

            var serializer = new XmlSerializer(typeof(DocumentPDX06));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("PDX06.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {

                var doc = (DocumentPDX06)serializer.Deserialize(reader);

                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Info.DateValue);
                Assert.AreEqual("001727424/BL", doc.Info.Number);
                Assert.AreEqual("MM0000100000", doc.Info.SenderID);
                Assert.AreEqual("MD0356200000", doc.Info.RecieverID);
                Assert.AreEqual("PDX06", doc.Info.TypeID);

                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Body.Tab.TradeDate);

                Assert.AreEqual("1.0", doc.Body.Version);
                Assert.AreEqual(4, doc.Body.Records.Count());

                var rec = doc.Body.Records[0];
                Assert.AreEqual("MC0134700000", rec.FIRMID);
                Assert.AreEqual("Россельхоз Б", rec.FIRMNAME);
                Assert.AreEqual(1, rec.RECNUM);
                Assert.AreEqual("PFDEP_T028DT", rec.SECURITYID);
                Assert.AreEqual(5.6M, rec.RATE);
                Assert.AreEqual(3500000000M, rec.AMOUNT);
                Assert.AreEqual(15035616.44M, rec.PAYMENT);
                Assert.AreEqual(DateTime.Parse("2013-08-08"), rec.SETTLEDATE);
                Assert.AreEqual(DateTime.Parse("2013-09-05"), rec.RETURNDATE);

            }

        }

        [TestMethod]
        public void TestXMLParsePDX08()
        {
            var path = @"PDX08.xml";
            var shema = @"PDX08.xsd";

            var serializer = new XmlSerializer(typeof(DocumentPDX08));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));

            using (var reader = GetInputXMLFile(path, settings))
            {

                var doc = (DocumentPDX08)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Info.DateValue);
                Assert.AreEqual("001727417/BL", doc.Info.Number);
                Assert.AreEqual("MM0000100000", doc.Info.SenderID);
                Assert.AreEqual("MD0356200000", doc.Info.RecieverID);
                Assert.AreEqual("PDX08", doc.Info.TypeID);
                Assert.AreEqual("1.0", doc.Body.Version);
                Assert.AreEqual(1, doc.Body.Tab.Auctno);
                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Body.Tab.TradeDate);
                Assert.AreEqual("DPPF", doc.Body.Tab.Board.BoardId);
                Assert.AreEqual(2, doc.Body.Tab.Board.Records.Count());
                var rec = doc.Body.Tab.Board.Records[0];
                Assert.AreEqual("PFDEP_T028DT", rec.SECURITYID);
                Assert.AreEqual(5.6M, rec.RATE);
                Assert.AreEqual(14000000000M, rec.TOTVAL1);
                Assert.AreEqual(14000000000M, rec.TOTVAL1N);
                Assert.AreEqual(14060142465.75M, rec.TOTVAL2);
                Assert.AreEqual(5.6M, rec.WARATE);
                Assert.AreEqual(DateTime.Parse("2013-08-08"), rec.SETTLEDATE);
                Assert.AreEqual(DateTime.Parse("2013-09-05"), rec.SETTLEDATE2);
                Assert.AreEqual(4, rec.QUANT_PART);
                Assert.AreEqual(4, rec.QUANT_REQ);
            }

        }

        [TestMethod]
        public void TestXMLParsePDX09()
        {
            var path = @"PDX09.xml";
            var shema = @"PDX09.xsd";

            var serializer = new XmlSerializer(typeof(DocumentPDX09));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));

            using (var reader = GetInputXMLFile(path, settings))
            {

                var doc = (DocumentPDX09)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Info.DateValue);
                Assert.AreEqual("001727416/BL", doc.Info.Number);
                Assert.AreEqual("MM0000100000", doc.Info.SenderID);
                Assert.AreEqual("MD0356200000", doc.Info.RecieverID);
                Assert.AreEqual("PDX09", doc.Info.TypeID);
                Assert.AreEqual("1.0", doc.Body.Version);
                Assert.AreEqual(1, doc.Body.Tab.Auctno);
                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Body.Tab.TradeDate);
                Assert.AreEqual("DPPF", doc.Body.Tab.Board.BoardId);
                Assert.AreEqual(1, doc.Body.Tab.Board.Records.Count());
                var rec = doc.Body.Tab.Board.Records[0];
                Assert.AreEqual("PFDEP_T028DT", rec.SECURITYID);
                Assert.AreEqual(5.6M, rec.RATE);
                Assert.AreEqual(20000000000M, rec.TOTVAL1);
                Assert.AreEqual(20000000000M, rec.TOTVAL1N);
                Assert.AreEqual(20085917808.22M, rec.TOTVAL2);
                Assert.AreEqual(5.6M, rec.WARATE);
                Assert.AreEqual(DateTime.Parse("2013-08-08"), rec.SETTLEDATE);
                Assert.AreEqual(DateTime.Parse("2013-09-05"), rec.SETTLEDATE2);
                Assert.AreEqual(4, rec.QUANT_PART);
                Assert.AreEqual(4, rec.QUANT_REQ);
            }

        }

        [TestMethod]
        public void TestXMLParsePDX01()
        {
            var path = @"PDX01.xml";
            var shema = @"PDX01.xsd";

            var serializer = new XmlSerializer(typeof(DocumentPDX01));

            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));

            using (var reader = GetInputXMLFile(path, settings))
            {
                var doc = (DocumentPDX01)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2013-08-06"), doc.Info.DateValue);
                Assert.AreEqual(TimeSpan.Parse("11:00:00"), doc.Info.TimeValue);
                Assert.AreEqual("1.0", doc.Body.Version);
                Assert.AreEqual(DateTime.Parse("2013-08-07"), doc.Body.Tab.LimitDateValue);
                Assert.AreEqual("PFDEP_T028DT", doc.Body.Tab.SecurityId);
                var rec = doc.Body.Tab.Records[0];
                Assert.AreEqual("MC0009800000", rec.FirmId);
                Assert.AreEqual("ГПБ (ОАО)", rec.ShortName);
                Assert.AreEqual(130493000000M, rec.Limit);
                Assert.AreEqual(145208000000M, rec.LimitInfo);
            }
        }


        [TestMethod]
        public void TestXMLParseF026()
        {
            EDO_ODKF026 doc = null;
            var path = @"F026.xml";
            var shema = @"F026.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF026));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF026)serializer.Deserialize(reader);
                Assert.AreEqual(136790218545.12M, doc.group1.total);
                Assert.AreEqual("\"Газпромбанк\" (Открытое акционерное общество)", doc.group1.lines[0].bank_name);
                Assert.AreEqual("40701810600000018740", doc.group1.lines[0].account);
                Assert.AreEqual(18034839.69M, doc.group1.lines[0].amount);
            }
        }


        [TestMethod]
        public void TestXMLParseF025()
        {
            EDO_ODKF025 doc = null;
            var path = @"F025.xml";
            var shema = @"F025.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF025));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF025)serializer.Deserialize(reader);
                Assert.AreEqual("22-03У004", doc.title.dogovor_num);
                Assert.AreEqual(DateTime.Parse("2003-10-08"), DateUtil.ParseImportDate(doc.title.dogovor_date).Value);
                Assert.AreEqual(73559410.52M, doc.group1.total);
                Assert.AreEqual("Банк ВТБ (открытое акционерное общество)", doc.group1.lines[0].bank_name);
                Assert.AreEqual("40702810800290000025", doc.group1.lines[0].account);
                Assert.AreEqual(73559410.52M, doc.group1.lines[0].amount);
                Assert.AreEqual("032-Р", doc.group1.lines[0].doc_num);
                Assert.AreEqual(DateTime.Parse("2004-02-10"), DateUtil.ParseImportDate(doc.group1.lines[0].doc_date));
            }
        }

        [TestMethod]
        public void TestXMLParseF015()
        {
            EDO_ODKF015 doc = null;
            var path = @"F015.xml";
            var shema = @"F015.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF015));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF015)serializer.Deserialize(reader);

                Assert.AreEqual(DateTime.Parse("2013-09-26"), DateUtil.ParseImportDate(doc.title.report_on_date));
                Assert.AreEqual(DateTime.Parse("2013-09-30T14:00:42"), DateUtil.ParseImportDate(doc.title.report_date));
                Assert.AreEqual("7706016118", doc.title.promoter.inn);
                Assert.AreEqual("Пенсионный фонд Российской Федерации", doc.title.promoter.name);
                Assert.AreEqual("770601001", doc.title.promoter.kpp);
                Assert.AreEqual("22-03У004", doc.title.dogovor_num);
                Assert.AreEqual(1828867044.69M, doc.a060);
                Assert.AreEqual("VTB00001-001-10177416F015", doc.RegNumberOut);


            }
        }

        [TestMethod]
        public void TestXMLParseF016()
        {
            EDO_ODKF016 doc = null;
            var path = @"F016.xml";
            var shema = @"F016.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF016));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF016)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2013-09-26"), DateUtil.ParseImportDate(doc.title.report_on_date));
                Assert.AreEqual(DateTime.Parse("2013-10-01T15:31:44"), DateUtil.ParseImportDate(doc.title.report_date));
                Assert.AreEqual(7706016118M, doc.title.promoter.inn);
                Assert.AreEqual("Пенсионный фонд Российской Федерации", doc.title.promoter.name);
                Assert.AreEqual(770601001M, doc.title.promoter.kpp);
                Assert.AreEqual("Начальник отдела по контролю за операциями с активами ПФР", doc.authorized_persons.specdep_person.post);
                Assert.AreEqual("Семашко О.В.", doc.authorized_persons.specdep_person.name);
                Assert.AreEqual(1829897277864.5M, doc.a060);
                Assert.AreEqual("VTB00001-001-10176353F016", doc.RegNumberOut);

            }
        }

        [TestMethod]
        public void TestXMLParseF040()
        {
            EDO_ODKF040 doc = null;
            var path = @"F040.xml";
            var shema = @"F040.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF040));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));


            var xdoc = XDocument.Load(GetInputFile(path, "WINDOWS-1251"));

            if (xdoc.Root.Attributes().Where(a => a.Name.LocalName.Equals("xmlns")).Any())
            {
                var docNew = new XDocument();
                var root = new XElement(xdoc.Root.Name.LocalName, xdoc.Root.Attribute("VERSION"), xdoc.Root.Attribute("DATACREATE"));
                root.Add(xdoc.Root.Nodes());
                docNew.Add(root);
                xdoc = docNew;
            }

            using (var reader = XmlReader.Create(new StringReader(xdoc.ToString()), settings))
            {
                doc = (EDO_ODKF040)serializer.Deserialize(reader);
                Assert.IsNotNull(DateUtil.ParseImportDate(doc.ObSvedenya.DogovorDate));
                Assert.AreEqual(DateTime.Parse("2003-10-08"), DateUtil.ParseImportDate(doc.ObSvedenya.DogovorDate).Value);
                Assert.AreEqual("22-03У018", doc.ObSvedenya.DogovorNumber);
                Assert.AreEqual("Общество с ограниченной ответственностью \"ФИНАНСОВАЯ ИНВЕСТИЦИОННАЯ КОМПАНИЯ \"ИНТЕРФИНАНС\"", doc.ObSvedenya.UKName);
                Assert.AreEqual("VTB00001-001-9864152F040", doc.Title.RegNumberOut);

                Assert.AreEqual("ЗАО \"Сбербанк КИБ\"", doc.Forma.Details5.AkciiRAOBroker);
                Assert.AreEqual(20M, doc.Forma.Details5.AkciiRAOQtyBuy);
                Assert.AreEqual(1616985.3M, doc.Forma.Details5.AkciiRAOAmountSell);

                Assert.AreEqual(1655450.8M, doc.Forma.RRZ5.AkciiRAOAmountBuyCount);
                Assert.AreEqual(1616985.3M, doc.Forma.RRZ5.AkciiRAOAmountSellCount);

            }
        }

        [TestMethod]
        public void TestXMLParseF050()
        {
            EDO_ODKF050 doc = null;
            var path = @"F050.xml";
            var shema = @"F050.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF050));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF050)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2003-10-08"), DateUtil.ParseImportDate(doc.DogovorDate));
                Assert.AreEqual("22-03У057", doc.DogovorNumber);
                Assert.AreEqual("БКС УК ЗАО - Сбалансированный", doc.InvestCaseName);
                Assert.AreEqual("VTB00001-001-6499973", doc.RegNumberOut);
                Assert.AreEqual(23880985.5M, doc.ItogoCB2);
                Assert.AreEqual("RU32045MOS0", doc.CBInfo[2].GRN);
                Assert.AreEqual(8, doc.CBInfo[2].IDTypeAsset);
                Assert.AreEqual(1040M, doc.CBInfo[2].Qty);
                Assert.AreEqual("Субфедеральная облигация", doc.CBInfo[2].TypeAsset);
                Assert.AreEqual("Правительство г.Москвы", doc.CBInfo[2].Name);
            }
        }

        [TestMethod]
        public void TestXMLParseF060()
        {
            EDO_ODKF060 doc = null;
            var path = @"F060.xml";
            var shema = @"F060.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF060));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF060)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2003-10-08"), DateUtil.ParseImportDate(doc.DogovorDate));
                Assert.AreEqual("22-03У052", doc.DogovorNumber);
                Assert.AreEqual("Закрытое акционерное общество \"Управляющая компания \"Достояние\" Д.У. средствами пенсионных накоплений", doc.UKName);
                Assert.AreEqual(5467199.32M, doc.PaymentReceived1);
                Assert.AreEqual(11758670.9M, doc.PaymentReceived2);
                Assert.AreEqual("7707014681", doc.UKINN);
                Assert.AreEqual("VTB00133-58199F060", doc.RegNumberOut);


            }
        }

        [TestMethod]
        public void TestXMLParseF070()
        {
            EDO_ODKF070 doc = null;
            var path = @"F070.xml";
            var shema = @"F070.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF070));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF070)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2003-10-08"), DateUtil.ParseImportDate(doc.DogovorDate));
                Assert.AreEqual("22-03У052", doc.DogovorNumber);
                Assert.AreEqual("7707014681", doc.UKINN);
                Assert.AreEqual("Закрытое акционерное общество \"Управляющая компания \"Достояние\" Д.У. средствами пенсионных накоплений", doc.UKName);
                Assert.AreEqual(218360.54M, doc.Proc1);
                Assert.AreEqual(9418.26M, doc.SDPay1);
                Assert.AreEqual(116259.36M, doc.MaxSDPay);
            }
        }

        [TestMethod]
        public void TestXMLParseF080()
        {
            EDO_ODKF080 doc = null;
            var path = @"F080.xml";
            var shema = @"F080.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF080));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));
            var ci = new CultureInfo("en-US");

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF080)serializer.Deserialize(reader);
                Assert.AreEqual(DateTime.Parse("2003-10-08"), DateUtil.ParseImportDate(doc.DogovorDate));
                Assert.AreEqual("22-03У048", doc.DogovorNumber);
                Assert.AreEqual("Общество с ограниченной ответственностью ВТБ Капитал Пенсионный резерв Д.У. средствами пенсионных накоплений", doc.UKName);
                Assert.AreEqual("Семашко О.В.", doc.SDPerson);
                Assert.AreEqual("Зензинов Б.В.", doc.UKPerson);
                Assert.AreEqual("Субфедеральная бумага", doc.Deal[1].CBKind);
                Assert.AreEqual(DateTime.Parse("2013-07-22"), DateUtil.ParseImportDate(doc.Deal[1].GRDate));
                Assert.AreEqual(749275.0M, Convert.ToDecimal(doc.Deal[1].MarketPrice, ci));
                Assert.AreEqual(798.15M, Convert.ToDecimal(doc.Deal[1].FactPrice, ci));
                Assert.AreEqual(6523M, Convert.ToDecimal(doc.Deal[1].PriceDeviation, ci));
            }
        }

        [TestMethod]
        public void TestXMLParseF140()
        {
            EDO_ODKF140 doc = null;
            var path = @"F140.xml";
            var shema = @"F140.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF140));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF140)serializer.Deserialize(reader);
                DateTime dt;
                Assert.AreEqual("покупка актива", doc.SpecDepTreat);
                Assert.AreEqual(string.Empty, doc.StopDate);
                Assert.AreEqual(DateTime.Parse("2013-10-01"), DateUtil.ParseImportDate(doc.RealStopDate));
                Assert.AreEqual(true, DateTime.TryParse(doc.RealStopDate, out dt));
                Assert.AreEqual(DateTime.Parse("2013-10-03"), DateUtil.ParseImportDate(doc.DateOfReport));
                Assert.AreEqual(false, DateTime.TryParse(doc.StopDateDay, out dt));
                Assert.AreEqual(null, doc.RegNumberOut);


            }
        }

        [TestMethod]
        public void TestXMLParseF401()
        {
            EDO_ODKF401 doc = null;
            var path = @"F401.xml";
            var shema = @"F401.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF401));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF401)serializer.Deserialize(reader);

                Assert.AreEqual("VTB00001-001-1046629", doc.RegNumberOut);
                Assert.AreEqual(DateTime.Parse("2013-10-01"), DateUtil.ParseImportDate(doc.FromDate));
                Assert.AreEqual(DateTime.Parse("2013-10-31"), DateUtil.ParseImportDate(doc.ToDate));

                Assert.IsFalse(doc.Details == null);
                Assert.IsTrue(string.IsNullOrEmpty(doc.Details.First().DogPFRNum));
                Assert.AreEqual(28036.66M, doc.Details.First().Itogo);
                
                Assert.AreEqual("22-03У038", doc.Details[1].DogPFRNum);

            }
        }

       [TestMethod]
        public void TestXMLParseF402()
        {
            EDO_ODKF402 doc = null;
            var path = @"F402.xml";
            var shema = @"F402.xsd";

            var serializer = new XmlSerializer(typeof(EDO_ODKF402));

            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema(shema));
            settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));

            using (var reader = GetInputXMLFile(path, settings))
            {
                doc = (EDO_ODKF402)serializer.Deserialize(reader);

                Assert.AreEqual("VTB00001-001-1052125", doc.RegNumberOut);
                Assert.AreEqual(DateTime.Parse("2013-10-01"), DateUtil.ParseImportDate(doc.FromDate));
                Assert.AreEqual(DateTime.Parse("2013-10-31"), DateUtil.ParseImportDate(doc.ToDate));
                Assert.IsFalse(doc.Details == null);
                Assert.IsTrue(string.IsNullOrEmpty(doc.Details.First().DogPFRNum));
                Assert.AreEqual(28036.66M, doc.Details.First().Itogo);
                Assert.AreEqual(DateTime.Parse("2013-11-21"), DateUtil.ParseImportDate(doc.Details.First().PayDate));


                Assert.AreEqual("22-03У038", doc.Details[1].DogPFRNum);
                Assert.AreEqual(DateTime.Parse("2013-11-21"), DateUtil.ParseImportDate(doc.Details[1].PayDate));

            }
        }


    }
}
