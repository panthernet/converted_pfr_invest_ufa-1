﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.Helpers;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.BusinessLogic
{
    [TestClass]
    public class TestMiscCases : BaseTest
    {
		[TestMethod]
		public void TestNotifications()
		{
		    try
		    {
		        var tmp = DataContainerFacade.GetLoginMessageItems();

		        /* Проверка заполняемости отчетов аналитических показателей */
		        var isEnabled = BLServiceSystem.Client.IsInfoAlertsEnabled();
		        {
		            var col = new AnalyzeConditionCollection();
		            var roles = AP.Provider.CurrentUserSecurity.GetRoles();
		            var msgs = col.CheckUnfilledReports(new[] {DOKIP_ROLE_TYPE.Administrator});
		        }

		        var rops = DevExpressHelper.ROPSRetriesWrapper(10, () => DataContainerFacade.GetClient().GetROPSLimitMessage());

		        var msg = BLServiceSystem.Client.GetCBReportForApproveMessage();
		    }
		    catch (Exception ex)
		    {
		        Assert.Fail(ex.ToString());
            }
        }
    }
}
