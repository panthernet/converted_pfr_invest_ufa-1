﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataAccess.Client;
using System.IO;
using PFR_INVEST.BusinessLogic.BranchReport;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.BusinessLogic.SIReport;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Tests.BusinessLogic
{
    [TestClass]
    public class ReportChange : BaseTest
    {        

        [TestMethod]
        public void UpdateTemplateCashExpenditures()
        {
            ReportImportVRDlgViewModel model = new ReportImportVRDlgViewModel()
            {
                SelectedMonth = new Month() { ID = 1, Name = "Январь" }, // 
                SelectedYear = new Year() { ID = 13, Name = "2013" },
                SelectedReportType = new SIReportType()
                {
                    ReportType = "05",
                    FullName = "Суммы средств пенсионных накоплений подлежащих включению в состав выплатного резерва, а также средств пенсионных накоплений для осуществления срочных пенсионных выплат для передачи в ГУК ВР и суммы средств пенсионных накоплений в размере квартальной выплаты, подлежащие передаче в ТО ПФР"
                }
            };

            
            ReportImportSIHandlerBase importHandler =  ReportImportVRFactory.GetImportHandler(model);
            importHandler.HeaderNameReport = model.SelectedReportType.FullName;
            importHandler.SelectedMonth = new Month() { ID = 1, Name = "Январь" };
            importHandler.SelectedYear = new Year() { ID = 13, Name = "2013" };

            String msg = "";
            importHandler.LoadFile(@"C:\Users\Alexander\Desktop\PFR\import_contracts\2_2_1.xls", out msg);

            if (importHandler.CanExecuteImport())
            {
                String errorMsg = "";
                Assert.IsTrue((importHandler.ExecuteImport(out msg)));
            }

            List<Contract> cntrcts = BLServiceSystem.Client.GetGukConractsByContractName("Договор доверительного управления средствами ВР");//.Cast<Contract>().ToList();
            Assert.IsTrue(cntrcts.Count > 0);




            //BLServiceSystem.Client.SaveJornalLog(DataObjects.Journal.JournalEventType.CHANGE_STATE, "Check log", 1, "Просмотр лога");
            
            //List<Attach> attaches = BLServiceSystem.Client.GetLinkedDocumentsListHib(13181);
            /*
            updateTemplate();

            PrintVRYearTransfers printVrPlan = new PrintVRYearTransfers();
            printVrPlan.VrYearTransferRegisterId = 1759;
            /*
            PLAN_YEAR
            */
            //printVrPlan.Print(true);
            
        }


        protected void updateTemplate() {

            string filePath = @"d:\Development\PFR_PTK_DOKIP\PFR_INVEST_DWH_DESIGN\Templates\";
            //string fileName = "Отчет о поступивших заявлениях.xlsx";
            string fileName = "Вр_финансирование_мастер_годового_плана.xls";
            string reportFileName = fileName;

            Template t = DataContainerFacade.GetListByProperty<Template>("FileName", reportFileName).FirstOrDefault();

            if (t == null)
            {
                t = new Template();
                t.UNID = 0;
                t.FileName = reportFileName;
                t.DepartmentID = 1;
                t.Date = new DateTime(2013, 10, 3);
            }

            t.Body = File.ReadAllBytes(filePath + fileName);
            Assert.IsTrue(t.Body != null && t.Body.Length > 0);
            DataContainerFacade.Save<Template>(t);

        }


    }
}
