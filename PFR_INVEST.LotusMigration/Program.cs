﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.IO;
using System.Text;

namespace PFR_INVEST.DBUtils
{
    using PFR_INVEST.Core.Logger;

    class Program
    {

        static void Main(string[] args)
        {
            //string schemeName = ConfigurationManager.AppSettings.Get("SchemeName");
            string connectionString = GetConnectionString();

            string fileLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log", DateTime.Now.Date.ToString("dd-MM-yyyy") + ".txt");
            InitLogger(fileLog);

            //List<string> executeActions = ConfigurationManager.AppSettings.Get("ExecuteActions").ToUpper().Split(';').ToList();

            WriteLog(string.Empty, LogSeverity.Warning);
            WriteLog("Утилита запущена.", LogSeverity.Info);
           // WriteLog("Будут выполнены следующие действия: ", LogSeverity.Info);

            //foreach (string action in executeActions)
            {
                IActionHandler handler = ActionsFactory.Create(connectionString, args);

                if (handler == null)
                {
                    WriteLog(string.Format("Невозможно обработать команду"), LogSeverity.Error);
                }
                else
                {
                    string action = handler.ActionName;

                    WriteLog(string.Format("Выполняется - {0}", action), LogSeverity.Info);
                    //WriteLog("Инициализация обработчика для " + action, LogSeverity.Info);

                    //WriteLog(string.Format("Обработчик для {0} создан", action), LogSeverity.Info);
                    //WriteLog(string.Format("Выполняется {0} ", action), LogSeverity.Info);

                    if (handler.Execute())
                    {
                        WriteLog(string.Format("{0} выполнился успешно ", action), LogSeverity.Info);
                    }
                    else
                    {
                        WriteLog(string.Format("Во время выполнения {0} возникли ошибки, для получения детальной информации смотри лог файл {1}", action, fileLog), LogSeverity.Warning);
                    }
                    WriteLog(string.Format("Операция {0} завершена.", action), LogSeverity.Info);
                }
            }

            WriteLog("Утилита завершена.", LogSeverity.Info);
        }

        
        private static string GetConnectionString()
        {
            string dsn = ConfigurationManager.AppSettings.Get("DSN");

            if (!String.IsNullOrEmpty(dsn))
                return string.Format("DSN={0}", dsn);


            string databaseName = ConfigurationManager.AppSettings.Get("DatabaseName");
            string hostName = ConfigurationManager.AppSettings.Get("HostName");
            string port = ConfigurationManager.AppSettings.Get("Port");
            string userName = ConfigurationManager.AppSettings.Get("UserName");
            string password = ConfigurationManager.AppSettings.Get("Password");

            return string.Format(
                    "Driver=IBM DB2 ODBC DRIVER;Database={0};Hostname={1};Protocol=TCPIP;Port={2};Uid={3};Pwd={4};",
                    databaseName, hostName, port, userName, password);
        }

        private static void InitLogger(string fileLog)
        {
            var severity = ConfigurationManager.AppSettings.Get("LogSeverity");
            LogSeverity severityValue;
            if (string.IsNullOrEmpty(severity))
                severityValue = LogSeverity.Error;
            else
                severityValue = (LogSeverity)Enum.Parse(typeof(LogSeverity), severity, true);

            Logger.Instance.Severity = severityValue;
            Logger.Instance.Attach(new ObserverLogToFile(fileLog));
        }


        static void WriteLog(string message, LogSeverity logSeverity)
        {
            switch (logSeverity)
            {
                case LogSeverity.Debug:
                    Logger.Instance.Debug(message);
                    break;
                case LogSeverity.Error:
                    Logger.Instance.Error(message);
                    break;
                case LogSeverity.Fatal:
                    Logger.Instance.Fatal(message);
                    break;
                case LogSeverity.Info:
                    Logger.Instance.Info(message);
                    break;
                case LogSeverity.Warning:
                    Logger.Instance.Warning(message);
                    break;
                default:
                    Logger.Instance.Info(message);
                    break;
            }

            Console.WriteLine(message);
        }
    }
}
