﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;

    using System.Linq;

    public class AuditAllTablesHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "AuditAllTables"; }
        }


        private string[] Args;

        public string SchemaNameLotus;
        public string SchemaNameBasic;

        public string FileName;
        public string FilePath;

        List<string> IgnoreTables;

        public AuditAllTablesHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            SchemaNameLotus = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            SchemaNameBasic = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 2)
                throw new ArgumentNullException();


            FileName = args[1];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            string s = ConfigurationManager.AppSettings.Get("IgnoreAuditTableList");

            IgnoreTables = new List<string>();

            if (!string.IsNullOrEmpty(s))
            {
                foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    IgnoreTables.Add(t.Trim());
            }
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах Pfr_Lotus");
            SchemaInfoBase sLotus = SchemaInfoBase.GetSchemaAllTables(SchemaNameLotus);

            Logger.Instance.Info("Выборка данных о таблицах Pfr_Basic");
            SchemaInfoBase sBasic = SchemaInfoBase.GetSchemaAllTables(SchemaNameBasic);

            Logger.Instance.Info("Аудит таблиц");


            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Tables found");


            ProcessNoImportTables(sb, sBasic);

            sBasic.Tables = sBasic.Tables.Where(p => p.IsMigratedTable()).ToList();

            sBasic.LoadRowCount();
            sLotus.LoadRowCount();

            PrintSource(sb, sBasic, sLotus);

            sLotus.ApplyMapData(sBasic);

            PrintLotusMissed(sb, sLotus);
            //PrintLotus(sb, sLotus);
            PrintLotusFull(sb, sLotus);
            PrintLotusPartial(sb, sLotus);


            SaveToFile(FilePath, sb.ToString());

            Logger.Instance.Info("Готово");
        }

        private void ProcessNoImportTables(StringBuilder sb, SchemaInfoBase sBasic)
        {
            sb.AppendLine("Таблицы, в которых отсутствуют колонки Lotus_ID, Lotus_Table:");
            sb.AppendLine();

            int n = 0;

            foreach (var x in sBasic.Tables)
            {
                if (!x.IsMigratedTable())
                {
                    sb.AppendLine(x.Name);
                    n++;
                }
            }
            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
        }


        private void PrintSource(StringBuilder sb, SchemaInfoBase sBasic, SchemaInfoBase sLotus)
        {
            sb.AppendLine("-----  PFR_BASIC  ---------------------------------------");
            sb.AppendLine();

            int n = 0;

            foreach (var t in sBasic.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                sb.AppendLine(t.Name);
                sb.AppendFormat("{0} Records", t.RowCount);
                sb.AppendLine();

                t.SelectMapData();

                sb.AppendLine("Mapping");
                foreach (var x in t.MapIndex)
                {
                    sb.AppendFormat("{0} - {1}", x.Key ?? "[NULL]", x.Value);
                    sb.AppendLine();
                }

                sb.AppendLine();
                n++;
            }

            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
        }

        private void PrintLotusMissed(StringBuilder sb, SchemaInfoBase sLotus)
        {
            sb.AppendLine("======  PFR_LOTUS  no mapping ======================================");
            sb.AppendLine();

            int n = 0;

            foreach (var t in sLotus.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                if (t.MapIndex.Count != 0) //not Empty 
                    continue;

                sb.AppendLine(t.Name);
                sb.AppendFormat("{0} Records", t.RowCount);
                sb.AppendLine();

                sb.AppendLine();
                n++;
            }

            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
        }

        private void PrintLotus(StringBuilder sb, SchemaInfoBase sLotus)
        {
            sb.AppendLine("======  PFR_LOTUS  ======================================");
            sb.AppendLine();

            int n = 0;

            foreach (var t in sLotus.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                if (t.MapIndex.Count == 0) //Empty 
                    continue;

                    sb.AppendLine(t.Name);
                sb.AppendFormat("{0} Records", t.RowCount);
                sb.AppendLine();

                sb.AppendLine("Mapping");
                foreach (var x in t.MapIndex)
                {
                    sb.AppendFormat("{0} - {1}", x.Key ?? "[NULL]", x.Value);
                    sb.AppendLine();
                }

                sb.AppendLine();
                n++;
            }

            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();
            
        }

        private void PrintLotusFull(StringBuilder sb, SchemaInfoBase sLotus)
        {
            sb.AppendLine("======  PFR_LOTUS  full  ======================================");
            sb.AppendLine();

            int n = 0;

            foreach (var t in sLotus.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                if (t.MapIndex.Count == 0) //Empty 
                    continue;

                if (!t.IsFullMapped())
                    continue;

                sb.AppendLine(t.Name);
                sb.AppendFormat("{0} Records", t.RowCount);
                sb.AppendLine();

                sb.AppendLine("Mapping");
                foreach (var x in t.MapIndex)
                {
                    sb.AppendFormat("{0} - {1}", x.Key ?? "[NULL]", x.Value);
                    sb.AppendLine();
                }

                sb.AppendLine();
                n++;
            }

            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();

        }

        private void PrintLotusPartial(StringBuilder sb, SchemaInfoBase sLotus)
        {
            sb.AppendLine("======  PFR_LOTUS  partial  ======================================");
            sb.AppendLine();

            int n = 0;

            foreach (var t in sLotus.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                if (t.MapIndex.Count == 0) //Empty 
                    continue;

                if (t.IsFullMapped())
                    continue;

                sb.AppendLine(t.Name);
                sb.AppendFormat("{0} Records", t.RowCount);
                sb.AppendLine();

                sb.AppendLine("Mapping");
                foreach (var x in t.MapIndex)
                {
                    sb.AppendFormat("{0} - {1}", x.Key ?? "[NULL]", x.Value);
                    sb.AppendLine();
                }

                sb.AppendLine();
                n++;
            }

            sb.AppendLine();
            sb.AppendFormat("###############  Count : {0}", n);
            sb.AppendLine();

        }


    }
}
