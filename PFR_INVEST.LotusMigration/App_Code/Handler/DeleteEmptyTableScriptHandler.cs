﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;

    public class DeleteEmptyTableScriptHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "DeleteEmptyTableScriptHandler"; }
        }


        private string[] Args;

        public string SchemaName;

        public string FileName;
        public string FilePath;

        public string SearchString;

        List<string> IgnoreTables;

        public DeleteEmptyTableScriptHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            if (args[1].ToLower().Contains("lotus"))
                SchemaName = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            else
                SchemaName = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 3)
                throw new ArgumentNullException();

            FileName = args[2];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            string s = ConfigurationManager.AppSettings.Get("IgnoreSearchTableList");

            IgnoreTables = new List<string>();

            if (!string.IsNullOrEmpty(s))
            {
                foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    IgnoreTables.Add(t.Trim());
            }
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах");

            SchemaInfoBase s = SchemaInfoBase.GetSchemaAllTables(SchemaName);

            Logger.Instance.Info("Поиск количества строк в таблицах");

            Console.WriteLine(SearchString);

            StringBuilder sb = new StringBuilder();

            //sb.AppendLine("Table, RowCount");
            sb.AppendLine("--Delete all empty tables script.");
            sb.AppendLine();

            foreach (var t in s.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                Console.WriteLine(t.Name);
                //sb.Append(t.GetRowCount(OdbcConnection, SearchString));

                t.GetRowCount();
                if (t.RowCount > 0)
                    continue;

                sb.AppendFormat("DROP TABLE \"PFR_LOTUS\".\"{0}\";", t.Name);
                sb.AppendLine();
            }

            sb.AppendLine();
            sb.AppendLine("COMMIT;");

            SaveToFile(FilePath, sb.ToString());


            Logger.Instance.Info("Готово");
        }

    }
}
