﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;

    using System.Linq;

    public class AuditTemplateAllTablesHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "AuditTemplateAllTables"; }
        }


        private string[] Args;

        public string SchemaNameLotus;
        public string SchemaNameBasic;

        public string FileName;
        public string FilePath;

        List<string> IgnoreTables;

        public AuditTemplateAllTablesHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            SchemaNameLotus = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            SchemaNameBasic = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 2)
                throw new ArgumentNullException();


            FileName = args[1];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            string s = ConfigurationManager.AppSettings.Get("IgnoreAuditTableList");

            IgnoreTables = new List<string>();

            if (!string.IsNullOrEmpty(s))
            {
                foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    IgnoreTables.Add(t.Trim());
            }
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах Pfr_Lotus");
            SchemaInfoBase sLotus = SchemaInfoBase.GetSchemaAllTables(SchemaNameLotus);

            Logger.Instance.Info("Выборка данных о таблицах Pfr_Basic");
            SchemaInfoBase sBasic = SchemaInfoBase.GetSchemaAllTables(SchemaNameBasic);

            Logger.Instance.Info("Аудит таблиц");


            StringBuilder sb = new StringBuilder();


            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.AppendLine("<!--Status-->");
            sb.AppendLine("<!-- -1 - Empty table-->");
            sb.AppendLine("<!-- 0 - Not Mapped (default)-->");
            sb.AppendLine("<!-- 50 - partially mapped-->");
            sb.AppendLine("<!-- 100 - Finished-->");
            sb.AppendLine("<!--  -->");
            

            sb.AppendLine("<settings>");
            PrintSchema(sb, sLotus);

            sb.AppendLine();
            sb.AppendLine();

            PrintSchema(sb, sBasic);
            sb.AppendLine("</settings>");

            SaveToFile(FilePath, sb.ToString());

            Logger.Instance.Info("Готово");
        }

        private void PrintSchema(StringBuilder sb, SchemaInfoBase s)
        {
            sb.AppendFormat("<scheme name='{0}'>", s.SchemaName);
            sb.AppendLine();

            foreach (var t in s.Tables)
            {
                sb.AppendFormat("  <table name='{0}' status='' comment='' ignore='' />", t.Name);
                sb.AppendLine();
            }

            sb.AppendLine("</scheme>");
        }
    }
}
