﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;

    public class InsertScriptByTypeTemplateHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "InsertScriptByTypeTemplate"; }
        }


        private string[] Args;
        //public string ActionName { get; private set; }

        public string LotusSchemaName;
        public string PfrSchemaName;

        public string LotusTableName;
        public string PfrTableName;

        public string FileName;
        public string FilePath;

        public InsertScriptByTypeTemplateHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            LotusSchemaName = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            PfrSchemaName = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 3)
                throw new ArgumentNullException();

            LotusTableName = args[1].ToUpper();
            PfrTableName = args[2].ToUpper();

            if (args.Length == 3)
                FileName = LotusTableName + ".sql";
            else
                FileName = args[3];

            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных из таблиц");


            TableInfo lotusTable = GetTableInfo<TableInfo>(LotusSchemaName, LotusTableName);
            TableInfo pfrTable = GetTableInfo<TableInfo>(PfrSchemaName, PfrTableName);


            Logger.Instance.Info("Генерация скрипта");

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(
@"
-- Remove previous impored data. Be carefull.
--delete from ""{2}"".""{3}"" where Lotus_TABLE = '{1}';


-- Add Lotus_ID, Lotus_Table fields if required
call pfr_lotus.Create_Lotus_Fields( '{2}', '{3}');

-- Data insert
insert into ""{2}"".""{3}""
(
LOTUS_ID, LOTUS_TABLE

{4}
)
select t.ID, '{1}'

{5}
from ""{0}"".""{1}"" t;

call pfr_lotus.Fill_Lotus_Imported('{0}','{1}', '{2}', '{3}');
", LotusSchemaName, LotusTableName, PfrSchemaName, PfrTableName, pfrTable.GetInsertCols(), lotusTable.GetSelectCols());

            SaveToFile(FilePath, sb.ToString());


            Logger.Instance.Info("Скрипт создан");
        }


        public class TableInfo : TableInfoBase
        {
            public string GetInsertCols()
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(GetInsertRows(ColInfoBase.enType.bigint, "BigInt"));
                sb.Append(GetInsertRows(ColInfoBase.enType.integer, "Integer"));
                sb.Append(GetInsertRows(ColInfoBase.enType.decimalType, "Decimal"));
                sb.Append(GetInsertRows(ColInfoBase.enType.varchar, "Varchar"));
                sb.Append(GetInsertRows(ColInfoBase.enType.date, "Date"));
                sb.Append(GetInsertRows(ColInfoBase.enType.time, "Time"));
                sb.Append(GetInsertRows(ColInfoBase.enType.blob, "Blob"));
                sb.Append(GetInsertRows(ColInfoBase.enType.other, "Other"));

                sb.AppendLine();

                return sb.ToString();
            }

            public string GetSelectCols()
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(GetSelectRows(ColInfoBase.enType.bigint, "BigInt"));
                sb.Append(GetSelectRows(ColInfoBase.enType.decimalType, "Decimal"));
                sb.Append(GetSelectRows(ColInfoBase.enType.varchar, "Varchar"));
                sb.Append(GetSelectRows(ColInfoBase.enType.date, "Date"));
                sb.Append(GetSelectRows(ColInfoBase.enType.time, "Time"));
                sb.Append(GetSelectRows(ColInfoBase.enType.blob, "Blob"));
                sb.Append(GetSelectRows(ColInfoBase.enType.other, "Other"));

                sb.AppendLine();

                return sb.ToString();
            }
        }
    }
}
