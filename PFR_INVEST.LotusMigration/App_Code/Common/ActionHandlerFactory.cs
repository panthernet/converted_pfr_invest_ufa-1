﻿namespace PFR_INVEST.DBUtils
{

    public static class ActionsFactory
    {
        public static IActionHandler Create(string connectionString, string[] args)
        {
            if(args.Length<1)
                return null;

            switch (args[0].ToLower())
            {
                case "i":
                case "-i":
                case "insertscripttemplate":
                    return new InsertScriptTemplateHandler(connectionString, args);

                case "it":
                case "-it":
                case "insertscripttemplatebytype":
                    return new InsertScriptByTypeTemplateHandler(connectionString, args);

                case "s":
                case "-s":
                case "searchalltables":
                    return new SearchAllTablesHandler(connectionString, args);

                case "sl":
                case "-sl":
                case "searchlikealltables":
                    return new SearchLikeAllTablesHandler(connectionString, args);

                case "sx":
                case "-sx":
                case "searchxml":
                    return new SearchXmlAllTablesHandler(connectionString, args);


                case "portfolioyear":
                    return new PortfolioYearHandler(connectionString, args);

                case "xt":
                case "xmltemplate":
                    return new XmlScriptTemplateHandler(connectionString, args);


                case "sd":
                case "-sd":
                case "searchdecimal":
                    return new SearchDecimalAllTablesHandler(connectionString, args);

                case "sdate":
                case "-sdate":
                case "searchdate":
                    return new SearchDateAllTablesHandler(connectionString, args);

                case "sc":
                case "-sc":
                case "searchcolumn":
                    return new SearchColumnValueAllTablesHandler(connectionString, args);

                case "selectrowcount":
                    return new SelectRowCountHandler(connectionString, args);


                case "deleteemptytablescript":
                    return new DeleteEmptyTableScriptHandler(connectionString, args);

                case "trim":
                case "-trim":
                    return new TrimAllTablesHandler(connectionString, args);

                case "audit":
                case "-audit":
                    return new AuditAllTablesHandler(connectionString, args);

                case "audittemplate":
                case "-audittemplate":
                    return new AuditTemplateAllTablesHandler(connectionString, args);

                case "audit2":
                case "-audit2":
                    return new Audit2AllTablesHandler(connectionString, args);

                case "guidscript":
                case "-guidscript":
                    return new GuidScriptHandler(connectionString, args);


                case "auditlotus":
                case "-auditlotus":
                    return new AuditLotusHandler(connectionString, args);

                case "scriptlotusfields":
                case "-scriptlotusfields":
                    return new ScriptLotusFieldsHandler(connectionString, args);

                case "selectedpaperssplit":
                case "-selectedpaperssplit":
                    return new SelectedPapersSplitHandler(connectionString, args);

                case "f7script":
                    return new F7ScriptHandler(connectionString, args);


                    
            }


            return null;
        }
    }
}
