﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.LotusMigration.App_Code.Common
{
    public class ColInfoBase
    {
        public enum enType
        {
            bigint = 10,
            integer = 11,
            decimalType = 12,
            varchar = 20,
            date = 30,
            time = 31,
            blob = 40,
            other = 50
        };

        private string _TypeName;

        public int ColNo;
        public string ColName;
        public string Remarks;


        public int Length;
        public int Scale;

        public bool IsIdentity;

        public bool IsNullable;

        public int MatchCount = 0;

        public List<string> ValueList;

        public string TypeName
        {
            get { return _TypeName; }
            set
            {
                _TypeName = value;
                DataType = ParseDataType(value);
            }
        }

        private enType ParseDataType(string value)
        {
            switch (value.ToLower())
            {
                case "bigint": return enType.bigint;
                case "integer": return enType.integer;
                case "decimal": return enType.decimalType;
                case "varchar": return enType.varchar;
                case "date": return enType.date;
                case "time": return enType.time;
                case "blob": return enType.blob;

                default: return enType.other;
            }
        }

        public enType DataType;

        public bool IsServiceRowLotus
        {
            get
            {
                if (IsIdentity)
                    return true;
                if (ColName == "ID")
                    return true;
                if (ColName == "ID_LOTUS")
                    return true;

                return false;
            }
        }

        public bool IsServiceRowPfr
        {
            get
            {
                if (IsIdentity)
                    return true;
                if (ColName == "LOTUS_ID")
                    return true;
                if (ColName == "LOTUS_TABLE")
                    return true;

                return false;
            }
        }
    }
}
