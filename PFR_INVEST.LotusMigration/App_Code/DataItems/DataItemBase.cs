﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using PFR_INVEST.LotusMigration.App_Code.Common;

namespace PFR_INVEST.LotusMigration.App_Code.DataItems
{
    public class DataItemBase
    {
        public delegate void ItemHandler(OdbcDataReader reader);

        public static void ReadAllRecords(string commandText, ItemHandler h)
        {
            if (string.IsNullOrEmpty(commandText))
                return;

            using (OdbcCommand command = new OdbcCommand(commandText, ConnectionManager.Connection))
            {
                command.CommandType = System.Data.CommandType.Text;
                command.CommandTimeout = 0;
                List<string> tableNames = new List<string>();
                using (OdbcDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                h(reader);
                            }
                        }
                    }
                }
            }
        }

        public static object IsDbNullValue(object value)
        {
            return IsDbNullValue(value, null);
        }

        public static object IsDbNullValue(object value, object defaultValue)
        {
            if (value == DBNull.Value)
                return defaultValue;
            return value;
        }

        public static void ExecuteNonQuery(string commandText)
        {
            using (OdbcCommand command = new OdbcCommand(commandText, ConnectionManager.Connection))
            {
                command.CommandType = System.Data.CommandType.Text;
                List<string> tableNames = new List<string>();
                command.ExecuteNonQuery();
            }
        }

        public static object ExecuteScalar(string commandText)
        {
            using (OdbcCommand command = new OdbcCommand(commandText, ConnectionManager.Connection))
            {
                command.CommandType = System.Data.CommandType.Text;
                List<string> tableNames = new List<string>();
                return command.ExecuteScalar();
            }
        }
    }
}
