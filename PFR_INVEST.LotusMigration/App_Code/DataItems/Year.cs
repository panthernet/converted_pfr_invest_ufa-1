﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;

namespace PFR_INVEST.LotusMigration.App_Code.DataItems
{
    public class YearList : List<Year>
    { }

    public class Year : DataItemBase
    {
        public long ID;
        public string Name;
        public long? YearID;

        public Year()
        {
        }

        public Year(System.Data.Odbc.OdbcDataReader reader)
        {
            // TODO: Complete member initialization
            ID = (long)reader["ID"];
            Name = (string)reader["NAME"];
        }

        internal static YearList SelectAll()
        {
            YearList res = new YearList();

            string commandText =
    string.Format(@"
select 
ID, NAME

from PFR_BASIC.YEARS
");

            ReadAllRecords(commandText, delegate(OdbcDataReader reader)
            {
                Year p = new Year(reader);
                res.Add(p);
            });

            return res;
        }
    }
}
