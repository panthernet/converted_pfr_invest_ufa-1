﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using PFR_INVEST.DataObjects.Interfaces;

namespace db2connector.Contract
{
    [XmlRoot("KIPSettings")]
    [Serializable]
    public class KIPSettings: IKnownType
    {
        /// <summary>
        /// Работа с СИ
        /// </summary>
        public const int SECTION_SI = 1;
        /// <summary>
        /// Работа с ВР
        /// </summary>
        public const int SECTION_VR = 2;
        /// <summary>
        /// Работа с НПФ
        /// </summary>
        public const int SECTION_NPF = 3;

        [XmlArray("OperationMappings")]
        [XmlArrayItem("OperationMapping")]
        public List<OperationMapping> OperationMappings { get; set; }

        [XmlRoot("OperationMapping")]
        public class OperationMapping
        {
            [XmlAttribute("Code")]
            public string Code { get; set; }

            [XmlElement("Item")]
            public List<Item> Items { get; set; }
        }

        [XmlRoot("Item")]
        public class Item
        {
            [XmlAttribute("Section")]
            public int Section { get; set; }

            [XmlAttribute("Operation")]
            public int Operation { get; set; }
        }

        public static KIPSettings Load(string fileName)
        {
            var serializer = new XmlSerializer(typeof(KIPSettings));
            var settings = new XmlReaderSettings();
            using (var reader = XmlReader.Create(fileName, settings))
            {
                var res = (KIPSettings)serializer.Deserialize(reader);
                return res;
            }
        }
    }
}
