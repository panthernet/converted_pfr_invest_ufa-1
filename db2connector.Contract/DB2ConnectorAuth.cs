﻿using System.ServiceModel;
using db2connector.Contract;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;

namespace db2connector
{
    /// <summary>
    /// Часть интерфейса, отвечающая за работу с системой авторизации и управления правами доступа
    /// </summary>
    public partial interface IDB2Connector
    {

        [OperationContract]
        ServiceAuthMsg InitService(object[] prms);

        [OperationContract]
        ServiceAuthMsg ChangeCurrentUserPassword(string userName, string oldPassword, string password);

        [OperationContract]
        object GetToken();

        [OperationContract]
        AuthResponse GetGroupsChildUser(AuthRequest request);

        [OperationContract]
        AuthResponse GetRoles(AuthRequest request);

        [OperationContract]
        AuthResponse AddUserToGroup(AuthRequest request);

        [OperationContract]
        AuthResponse RemoveUserFromGroup(AuthRequest request);

        [OperationContract]
        AuthResponse GetUsersNotInGroup(AuthRequest request);

        [OperationContract]
        User IsUserExist(string dn);

        [OperationContract]
        bool IsUserInRole(string userName, string role);

        [OperationContract]
        bool IsUserInAtLeastOneRole(string userName, string[] roles);

        [OperationContract]
        AuthResponse GetUserInfo(AuthRequest request);
        //[OperationContract]
        //ADValidationResult InitDirectoryService(string userName, string password, string domain);

        //[OperationContract]
        //string InitDirectoryServiceByCertificate();

        //[OperationContract]
       // string GetAccountNameByCert(string subject);

        //[OperationContract]
       // TokenResponse GetToken(TokenRequest tokenRequest);

        //[OperationContract]
        //GroupsResponse GetGroups(GroupsRequest groupsRequest);

        //[OperationContract]
        //GroupsResponse GetUserGroups(UserGroupRequest userGroupsRequest);

       // [OperationContract]
       // UsersResponse GetGroupsChildUser(GroupUsersRequest groupUsersRequest);

        //[OperationContract]
        //UserResponse GetUserInfo(UserRequest userRequest);

       // [OperationContract]
       // UserResponse AddUser(UserRequest userRequest);

      //  [OperationContract]
      //  UserResponse RemoveUser(UserRequest userRequest);

       // [OperationContract]
       // UserResponse AddUserToGroup(UserGroupRequest userGroupRequest);

        //[OperationContract]
       // UserResponse RemoveUserFromGroup(UserGroupRequest userGroupRequest);

       // [OperationContract]
       // UsersResponse GetUsers(UsersRequest usersRequest);

       // [OperationContract]
       // UsersResponse GetUsersNotInGroup(GroupUsersRequest groupUsersRequest);

        //[OperationContract]
       // bool IsUserInRole(string userName, string domain, string role);

        //[OperationContract]
       // ADUser IsUserExist(string dn);

        //[OperationContract]
        //bool IsUserInAtLeastOneRole(string userName, string domain, string[] roles);

        //[OperationContract]
        //ADPasswordSetResult ChangeCurrentUserPassword(string userName, string oldPassword, string password, string domain);
    }
}
