﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.Proxy
{
    [DataContract]
    public partial class DB2AccountKind
    {
        #region Fields
        // ID - id типа счета
        private long id = 0;

        // MEASURE - Единица измерения
        private string measure = "";
        #endregion

        #region Properties
        [DataMember]
        public long ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Measure
        {
            get { return measure; }
            set { measure = value; }
        }

        #region Extended properties
        #endregion

        #endregion

        public DB2AccountKind()
        { }
    }
}
