﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.Proxy
{
    [DataContract]
    public partial class DB2Account
    {
        #region Fields
        // ID - id счета
        private long id = 0;

        // NAME - Название счета
        private string name = "";

        // CONTRAGENTID - id контрагента-хозяина
        private long contragentID = 0;

        // ACCOUNTTYPEID - id типа счета
        private long accountTypeID = 0;
        #endregion

        #region Properties
        [DataMember]
        public long ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public long ContragentID
        {
            get { return contragentID; }
            set { contragentID = value; }
        }

        [DataMember]
        public long AccountTypeID
        {
            get { return accountTypeID; }
            set { accountTypeID = value; }
        }
        #endregion

        public DB2Account()
        { 
        
        }
    }
}
