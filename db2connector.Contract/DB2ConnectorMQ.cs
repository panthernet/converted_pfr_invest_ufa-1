﻿using System.ServiceModel;

namespace db2connector
{
    public partial interface IDB2Connector
    {
        [OperationContract]
        long LoadODKReports(string managerName, string qName, string path);

        [OperationContract]
        long LoadEDOReports(string path);
    }
}
