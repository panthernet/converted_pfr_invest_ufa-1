namespace db2connector.Contract.ActiveDirectory
{
    public interface IActiveDirectory
    {
        ADGroup GetGroupInfoByToken(long groupToken);
    }
}