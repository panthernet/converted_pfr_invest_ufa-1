﻿namespace db2connector.Contract.ActiveDirectory
{
    public enum ADPasswordSetResult
    {
        Success = 0,
        SuccessNoLogin = 1,
        Fail = 2,
        FailPwdPolicy = 3,
        FailPwdWrong = 4
    }
}
