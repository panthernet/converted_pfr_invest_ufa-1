﻿using System.Runtime.Serialization;

namespace db2connector.Contract.ActiveDirectory.MessageBase
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public abstract class RequestBase
    {
        [DataMember]
        public string ClientTag;

        [DataMember]
        public string AccessToken;

        [DataMember]
        public string Version;

        [DataMember]
        public string RequestId;
    }
}
