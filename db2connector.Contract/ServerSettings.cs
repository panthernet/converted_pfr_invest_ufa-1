﻿using PFR_INVEST.DataObjects.Interfaces;

namespace db2connector.Contract
{
    public class ServerSettings: IKnownType
    {
        public string DbName { get; set; }
        public bool ShowClientNotificationSettings { get; set; }
        public bool IsOpfrTransfersEnabled { get; set; }
        public int PageSize { get; set; }
    }
}