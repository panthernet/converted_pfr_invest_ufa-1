﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PFR_INVEST.BusinessLogic.XMLModels;
using PFR_INVEST.Core;
using PFR_INVEST.Core.MQConnector;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.Reports;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.DataObjects.XMLModel.Auction;
using PFR_INVEST.DataObjects.XMLModel.KIP;
using PFR_INVEST.Integration.NSI.JSON;
using PFR_INVEST.Integration.NSI;
using PFR_INVEST.Integration.NSI.BL;
using NHibernate.Persister.Entity;

namespace PFR_INVEST.TestConsole
{
	internal class Program
	{
		// static Program Program;

		private static void Main(string[] args)
		{

			try
			{
				//var KIP = new PFR_INVEST.Integration.KIPClient.KIPClientFacade();

				//var res = KIP.SendDocument(File.ReadAllBytes(@"D:\temp\pkip_import\I_RECALL_MSK_UK.zip"));

				Type type = typeof(LegalEntity);

				var meta = DataAccessSystem.GetClassMetadata(type) as SingleTableEntityPersister;
				var mapping = meta.PropertyNames.Select(name => new { Name = name, DBField = meta.GetPropertyColumnNames(name).FirstOrDefault() })
									.Where(m => !string.IsNullOrEmpty(m.DBField)).ToList();
				var res = mapping.Select(m => new DBUpdater.PropertyMapping()
				{
					DBField = m.DBField,
					PropertyInfo = type.GetProperty(m.Name)
				}).ToList();



				//var text = File.ReadAllText(@"D:\temp\NSI_json\create.json.txt");
				//MessageProcessor.Instance.ParseMessage(text);




				//var value = JsonConvert.DeserializeObject<Message>(text);
				//                using (var session = DataAccessSystem.OpenSession())
				//                {

				//                    var tempAllAcc = session.CreateQuery(@"select a from Account a 
				//																	join a.Contragent c 
				//																	join c.LegalEntities le 
				//																	join le.Identifiers id
				//														where id.Identifier = 'ПФР'	")
				//                                            .SetMaxResults(1).List<Account>().FirstOrDefault();				



				//                }
			}
			catch (Exception ex)
			{
				//Log(ex);
			}
		}

		public static void TextMQ()
		{
			MQCoreConnector mq = new MQCoreConnector("DEV.TEST", "");
			//mq.QueueMessage("Bla-Bla-" + DateTime.Now.TimeOfDay.ToString(), "DEV.TEST.CHANEL_1");
			//var msg = mq.GetMessage("DEV.TEST.CHANEL_1");
			mq.SubscribeTopic("DEV.TEST", msg => { Console.WriteLine("Message got = " + msg); });
		}

		public static void TextSerialize()
		{
			using (var session = DataAccessSystem.OpenSession())
			{


				//var reg = session.Get<RegisterHib>(id);
				//var unclosed = reg.FinregistersList.FirstOrDefault(fr => fr.Count != fr.DraftsList.Sum(p => p.DraftAmount));






				{
					//XmlSerializer serializer = new XmlSerializer(typeof(KIP_Import));

					//XmlReaderSettings settings = new XmlReaderSettings();
					//settings.ValidationType = ValidationType.Schema;
					//settings.Schemas.Add(string.Empty, KIP_Import.GetShema("I_TRANS_MSK_1.0.1.xsd"));
					//XmlReader reader = XmlReader.Create(@"D:\WORK\PFR\Temp\I_RECALL_MSK_UK.xml", settings);

					//var res = (KIP_Import)serializer.Deserialize(reader);
				}

				////////////////////////////////////////////////////////////////////////////////////////////////////

				//{
				//    XmlSerializer serializer = new XmlSerializer(typeof(KIP_Import));
				//    XmlWriterSettings settings = new XmlWriterSettings();

				//}

				//var auction = session.Get<DepClaimSelectParams>(auctionID);
				////var limits = GetBanksLimit(auctionID);

				//File.Delete(@"D:\WORK\PFR\Temp\test.xml");
				//var fs2 = File.OpenWrite(@"D:\WORK\PFR\Temp\test.xml");

				//var doc = new SPVB_CutoffRate(auction);

				//XmlSerializer serializer2 = new XmlSerializer(typeof(SPVB_CutoffRate));
				//serializer2.Serialize(fs2, doc);



			}
		}

		public static List<BankLimitListItem> GetBanksLimit(long auctionID)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var retVal = new List<BankLimitListItem>();


				var auction = session.Get<DepClaimSelectParams>(auctionID);
				var stockID = auction.StockId;
				DateTime selectDate = auction.SelectDate;
				//все банки с кодом биржи аукциона
				var banks = session.CreateQuery(@"select distinct le from LegalEntity le 
															join fetch le.BankStockCodes sc
															join fetch le.DepClaims2 dc
															where sc.StockID = (:stockID)")
											.SetParameter<long>("stockID", stockID)
											.List<LegalEntityHib>();

				foreach (var bank in banks)
				{
					decimal limitMoney = BankHelper.Limit4Money(bank) * 1000000;

					var bank1 = bank;
					var deposits = bank.DepClaims2;
					decimal placedAndReturned = 0;
					decimal returned = 0;
					foreach (var deposit in deposits)
					{
						if (deposit.SettleDate <= selectDate && deposit.ReturnDate > selectDate)
							placedAndReturned += deposit.Amount;

						if (deposit.ReturnDate == selectDate.AddDays(-1)
							|| deposit.ReturnDate == selectDate
							|| deposit.ReturnDate == selectDate)
							returned += deposit.Amount;
					}
					var resultValue = limitMoney - placedAndReturned + returned;

					// Display in millions
					limitMoney = limitMoney / 1000000;
					resultValue = resultValue / 1000000;

					//Round
					limitMoney = Math.Round(limitMoney);
					resultValue = Math.Round(resultValue);

					retVal.Add(new BankLimitListItem()
					{
						BankName = bank.FormalizedName,
						StockID = stockID,
						StockCode = bank.BankStockCodes.First(sc => sc.StockID == stockID).StockCode,
						LimitTotal = limitMoney,
						LimitClaim = resultValue
					});
				}
				return retVal;
			}
		}

		#region report

		public static void Report1()
		{
			Document.Types type = Document.Types.SI;

			ReportInvestRatioPrompt prompt = new ReportInvestRatioPrompt() { YearID = 9, Multiplier = 1.0m, Type = Document.Types.SI };
			using (var session = DataAccessSystem.OpenSession())
			{
				DateTime from = new DateTime((int)prompt.YearID + 2000, 1, 1);
				DateTime to = new DateTime((int)prompt.YearID + 2000, 12, 31);

				var items = session.CreateSQLQuery(@" SELECT
															c.REGNUM as ContractNumber, c.REGDATE, c.REGCLOSE, le.FORMALIZEDNAME as UKName, c.PORTFNAME as Portfolio,
															S0.Sum as S0, Sn.Sum as Sn, Sm.SUM as Sm , CAST(CAST(D.SUM as DECIMAL (19,4)) * :mult as DECIMAL (19,4)) as D,
															Coalesce(Sk1.SUM ,Sk2.SUM) as Sk,
															RV.R as R, RV.V as V
														FROM
															PFR_BASIC.CONTRACT c
															JOIN PFR_BASIC.LEGALENTITY le ON le.ID = c.LE_ID
															LEFT JOIN
															(
																--Начальное СЧА
																	SELECT * FROM (
																		SELECT
																			ID_CONTRACT, REPORTONDATE as DATE, ITOGOSCHA1 as Sum,
																			RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORTONDATE DESC) AS rn ,
																			RANK() OVER(PARTITION BY ID_CONTRACT,REPORTONDATE ORDER BY ID DESC) AS rnID
																		FROM
																			PFR_BASIC.EDO_ODKF010
																		WHERE
																			REPORTONDATE + 1 DAY = :from 	AND ID_CONTRACT is not null
																		)
																	WHERE	rn = 1 AND rnID = 1
																	UNION
																	SELECT * FROM (
																		SELECT
																			ID_CONTRACT, REPORT_ON_TIME as DATE, A090 as Sum,
																			RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORT_ON_TIME DESC) AS rn ,
																			RANK() OVER(PARTITION BY ID_CONTRACT,REPORT_ON_TIME ORDER BY ID DESC) AS rnID
																		FROM
																			PFR_BASIC.EDO_ODKF015
																		WHERE
																			REPORT_ON_TIME + 1 DAY = :from  AND ID_CONTRACT is not null
																		)
																	WHERE	rn = 1 AND rnID = 1
															) S0 ON c.ID = S0.ID_CONTRACT
															LEFT JOIN
															(
																-- все перечисления в УК
																SELECT st.CONTR_ID as ID_CONTRACT, SUM(rt.SUM) as SUM
																FROM
																	PFR_BASIC.SI_REGISTER sr
																	JOIN PFR_BASIC.OPERATION o ON o.ID = sr.OPERATION_ID

																	JOIN PFR_BASIC.SI_TRANSFER st ON sr.ID = st.SI_REG_ID
																									AND st.CONTR_ID is not null
																	JOIN PFR_BASIC.REQ_TRANSFER rt ON st.ID = rt.SI_TR_ID
																									AND rt.CONTRL_SPN_DATE is not null
																									AND rt.STATUS_ID <> -1
																WHERE
																	o.OPERATION not in ('')
																	--AND sr.DIR_SPN_ID in (1,3) -- из УК
																	AND sr.DIR_SPN_ID in (2,4) -- в УК
																	AND rt.CONTRL_SPN_DATE BETWEEN :from AND :to
																GROUP BY
																	st.CONTR_ID
															) Sn ON c.ID = Sn.ID_CONTRACT
															LEFT JOIN
															(
																-- все перечисления из УК, кроме расторжения
																SELECT st.CONTR_ID as ID_CONTRACT, SUM(rt.SUM) as SUM
																FROM
																	PFR_BASIC.SI_REGISTER sr
																	JOIN PFR_BASIC.OPERATION o ON o.ID = sr.OPERATION_ID

																	JOIN PFR_BASIC.SI_TRANSFER st ON sr.ID = st.SI_REG_ID
																									AND st.CONTR_ID is not null
																	JOIN PFR_BASIC.REQ_TRANSFER rt ON st.ID = rt.SI_TR_ID
																									AND rt.CONTRL_SPN_DATE is not null
																									AND rt.STATUS_ID <> -1
																WHERE
																	o.OPERATION not in ('Возврат СПН - расторжение')
																	AND sr.DIR_SPN_ID in (1,3) -- из УК
																	--AND sr.DIR_SPN_ID in (2,4) -- в УК
																	AND rt.CONTRL_SPN_DATE BETWEEN :from AND :to
																GROUP BY
																	st.CONTR_ID
															) Sm ON c.ID = Sm.ID_CONTRACT
															LEFT JOIN
															(
																--Финальное СЧА
																SELECT * FROM (
																	SELECT
																		ID_CONTRACT, REPORTONDATE as DATE, ITOGOSCHA1 as Sum,
																		RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORTONDATE DESC) AS rn ,
																		RANK() OVER(PARTITION BY ID_CONTRACT,REPORTONDATE ORDER BY ID DESC) AS rnID
																	FROM
																		PFR_BASIC.EDO_ODKF010
																	WHERE
																		REPORTONDATE  = :to 	AND ID_CONTRACT is not null
																	)
																WHERE	rn = 1 AND rnID = 1
																UNION
																SELECT * FROM (
																	SELECT
																		ID_CONTRACT, REPORT_ON_TIME as DATE, A090 as Sum,
																		RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORT_ON_TIME DESC) AS rn ,
																		RANK() OVER(PARTITION BY ID_CONTRACT,REPORT_ON_TIME ORDER BY ID DESC) AS rnID
																	FROM
																		PFR_BASIC.EDO_ODKF015
																	WHERE
																		REPORT_ON_TIME = :to  AND ID_CONTRACT is not null
																	)
																WHERE	rn = 1 AND rnID = 1
															) Sk1 ON c.ID = Sk1.ID_CONTRACT
															LEFT JOIN
															(
																-- перечисления из УК - расторжения
																SELECT st.CONTR_ID as ID_CONTRACT, SUM(rt.SUM) as SUM
																FROM
																	PFR_BASIC.SI_REGISTER sr
																	JOIN PFR_BASIC.OPERATION o ON o.ID = sr.OPERATION_ID

																	JOIN PFR_BASIC.SI_TRANSFER st ON sr.ID = st.SI_REG_ID
																									AND st.CONTR_ID is not null
																	JOIN PFR_BASIC.REQ_TRANSFER rt ON st.ID = rt.SI_TR_ID
																									AND rt.CONTRL_SPN_DATE is not null
																									AND rt.STATUS_ID <> -1
																WHERE
																	o.OPERATION in ('Возврат СПН - расторжение')
																	AND sr.DIR_SPN_ID in (1,3) -- из УК
																	--AND sr.DIR_SPN_ID in (2,4) -- в УК
																	AND rt.CONTRL_SPN_DATE BETWEEN :from AND :to
																GROUP BY
																	st.CONTR_ID
															) Sk2 ON c.ID = Sk2.ID_CONTRACT
															LEFT JOIN
															(
																--Коеф
																SELECT ID_CONTRACT, DETAINED2 AS R, DETAINEDUKREWARD2 as V
																FROM
																	PFR_BASIC.EDO_ODKF060
																WHERE
																	QUARTAL = 4
																	AND ID_YEAR = YEAR( CAST(:to AS DATE) )-2000
															)RV ON c.ID = RV.ID_CONTRACT
															LEFT JOIN
															(
																--Среднее СЧА
																SELECT ID_CONTRACT, AVG(SUM) as SUM
																FROM
																(
																	SELECT
																		ID_CONTRACT, REPORTONDATE as DATE, ITOGOSCHA1 as Sum

																	FROM
																		PFR_BASIC.EDO_ODKF010
																	WHERE
																		REPORTONDATE  BETWEEN :from AND :to AND ID_CONTRACT is not null

																	UNION

																	SELECT
																		ID_CONTRACT, REPORT_ON_TIME as DATE, A090 as Sum

																	FROM
																		PFR_BASIC.EDO_ODKF015
																	WHERE
																		REPORT_ON_TIME BETWEEN :from AND :to AND ID_CONTRACT is not null
																)
																GROUP BY ID_CONTRACT
															) D ON c.ID = D.ID_CONTRACT

														WHERE
															c.REGCLOSE >= :from
															AND c.REGDATE <= :to
												")
												  .AddScalar("ContractNumber", NHibernateUtil.String)
												  .AddScalar("UKName", NHibernateUtil.String)
												  .AddScalar("Portfolio", NHibernateUtil.String)
												  .AddScalar("S0", NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))
												  .AddScalar("Sm", NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))
												  .AddScalar("Sn", NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))
												  .AddScalar("D",  NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))
												  .AddScalar("Sk", NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))
												  .AddScalar("R",  NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))
												  .AddScalar("V",  NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))

												 .SetParameter<DateTime?>("from", from)
												 .SetParameter<DateTime?>("to", to)
												 .SetParameter<decimal>("mult", prompt.Multiplier)
					//.SetParameter<long>("type", (long)prompt.Type)
												 .SetResultTransformer(Transformers.AliasToBean<ReportInvestRatioListItem>())
												 .List<ReportInvestRatioListItem>();

				Warning[] warnings;
				string[] streamids;
				string mimeType, encoding, extension;
				string f = Path.GetTempFileName();
				string filenameToSave = Path.Combine(Path.GetTempPath(), "test.xls");

				List<ReportParameter> param = new List<ReportParameter>();
				param.Add(new ReportParameter("ReportYear", (prompt.YearID + 2000).ToString()));

				ReportDataSource rds = new ReportDataSource("Report", items);

				LocalReport report = new LocalReport();
				//report.ReportEmbeddedResource = "PFR_INVEST.TestConsole.Report1.rdlc";
				//report.ReportPath = @"D:\WORK\PFR\PFR_PTK_DOKIP\PFR_INVEST_DWH_DESIGN\Templates\Reports\Собственные средства совокупно.rdlc";
				report.ReportPath = @"D:\WORK\PFR\PFR_PTK_DOKIP\PFR_INVEST_DEV\PFR_INVEST_UFA\PFR_INVEST.TestConsole\Расчет коэффициента прироста и коэффициента расходов для УК.rdlc";
				report.DataSources.Add(rds);
				report.SetParameters(param);

				FileStream newFile = new FileStream(filenameToSave, FileMode.Create);

				string renderFormat = "Excel";
				byte[] bytes = report.Render(renderFormat, null, out mimeType, out encoding, out extension, out streamids, out warnings);
				newFile.Write(bytes, 0, bytes.Length);
				newFile.Close();

				//ExcelTools.RenameExcelSheets(filenameToSave, new List<string> { "A", "B", "CC" });

				string argument = @"/select, " + filenameToSave;

				//System.Diagnostics.Process.Start("explorer.exe", argument);

				var p = Process.Start(filenameToSave);
			}
		}
		#endregion

		#region Common Hib DataAccess Functions

		public static Type GetType(string p_type)
		{
			return Assembly.GetAssembly(typeof(BaseDataObject)).GetType(p_type);
		}

		public static IList GetList(string p_type)
		{
			var type = GetType(p_type);
			using (var session = OpenSessionByType(type))
			{
				var list = session.CreateCriteria(type).List();
				return list;
			}
		}

		public static IList GetListLimit(string p_type, int limit)
		{
			var type = GetType(p_type);
			using (var session = OpenSessionByType(type))
			{
				var list = session.CreateCriteria(type).SetMaxResults(limit).List();
				return list;
			}
		}

		public static IList GetListByProperty(string p_type, string propertyName, object value)
		{
			var type = GetType(p_type);
			using (var session = OpenSessionByType(type))
			{
				var list = session.CreateCriteria(type).Add(Expression.Eq(propertyName, value)).List();
				return list;
			}
		}

		public static object GetByID(string p_type, object p_iId)
		{
			try
			{
				object result;
				var type = GetType(p_type);
				using (var session = OpenSessionByType(type))
				{
					result = session.Get(type, p_iId);
				}
				return result;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public static object Save(string p_type, object p_Object)
		{
			var type = GetType(p_type);
			using (var session = OpenSessionByType(type))
			using (var transaction = session.BeginTransaction())
			{
				session.SaveOrUpdate(p_Object);
				try
				{
					transaction.Commit();
				}
				catch (Exception)
				{
					throw;
				}
				var oId = session.GetIdentifier(p_Object);
				return oId;
			}
		}

		public static void Delete(string p_type, object p_Object)
		{
			var type = GetType(p_type);
			using (var session = OpenSessionByType(type))
			using (var transaction = session.BeginTransaction())
			{
				session.Delete(p_Object);
				try
				{
					session.Flush();
					transaction.Commit();
				}
				catch (Exception)
				{
					throw;
				}
			}
		}

		public static object GetObjectProperty(string p_type, object p_iId, string p_sPropertyName)
		{
			object result;
			var type = GetType(p_type);
			using (var session = OpenSessionByType(type))
			{
				result = session.Get(type, p_iId);
				if (result == null)
					return null;

				var t = result.GetType();

				var prop = t.GetProperty(p_sPropertyName);
				if (prop == null)
					throw new Exception("Property not found");

				result = prop.GetValue(result, null);
				NHibernate.NHibernateUtil.Initialize(result);
			}

			return result;
		}

		private static ISession OpenSessionByType(Type type)
		{
			return DataAccessSystem.OpenSession();
		}

		#endregion Common Hib DataAccess Functions
	}
}