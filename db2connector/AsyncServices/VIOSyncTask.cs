﻿using System;
using PFR_INVEST.Integration.VIO.MQ;
using PFR_INVEST.Proxy.Tools;

namespace db2connector.AsyncServices
{
	/// <summary>
	/// Задача загрузки обновлений справочников из системы ВИО
	/// </summary>
	public class VIOSyncTask : IAsyncTask
    {
		public void Process()
		{
#if !WEBCLIENT
            // во избежании вылета сервера без логирования исключения
            try
		    {
                MQConnector.Instance.SubscribeToMQ(ProcessMessage, IISService.LocalInstance);
            }
		    catch (Exception ex)
		    {
		        LogManager.LogException(ex);
		    }
#endif
		}

		private static void ProcessMessage(string message)
		{
			IISService.LocalInstance.ParseVIOMessage(message);
		}
	}
}
