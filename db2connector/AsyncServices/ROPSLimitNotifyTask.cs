﻿using System;
using System.Globalization;
using System.Linq;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace db2connector.AsyncServices
{
	public class ROPSLimitNotifyTask: IAsyncTask
    {
		public LoginMessageItem Message { get; set; }
		public void Process()
		{
			var ci = CultureInfo.GetCultureInfo("RU-ru");
			Message = null;
			var balance = 0.00m;
			var date = DateTime.Now;

			var service = IISService.LocalInstance;
			balance = service.GetROPSBalance(DateTime.Now);

			using (var session = DataAccessSystem.OpenSession())
			{
				var lastROPS = session.CreateQuery(@"select r from ROPSSum r where r.Date <= :date AND r.StatusID <> -1 order by r.Date desc")
						.SetParameter("date", date)
						.SetMaxResults(1)
						.List<ROPSSum>().FirstOrDefault();
			    if (lastROPS == null || balance > lastROPS.Sum) return;

			    var msg = balance == lastROPS.Sum ? $"По состоянию на {date.ToString("dd.MM.yyyy")} сумма фактического размера резерва равна минимальному установленному постановлением № {lastROPS.DirectionNum} от {lastROPS.DirectionDate.ToString("dd.MM.yyyy")}" :
			        $"По состоянию на {date.ToString("dd.MM.yyyy")} сумма фактического размера резерва ниже минимального установленного постановлением № {lastROPS.DirectionNum} от {lastROPS.DirectionDate.ToString("dd.MM.yyyy")} на {Math.Round(lastROPS.Sum - balance, 2, MidpointRounding.AwayFromZero).ToString("n2", ci)}";
			    var key = "LoginMessage.ROPSLimit." + date.ToString("dd.MM.yyyy");
			    Message = new LoginMessageItem { Message = msg, Title = "РОПС", Key = key, Order = -1000 };
			}
		}
	}
}
