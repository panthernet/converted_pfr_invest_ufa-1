﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;

namespace db2connector.AsyncServices
{
	public static class AsyncServicesSystem
	{
		public static GenericScheduleService ScheduleService { get; private set; }

		private static void InitScheduleService(GenericScheduleService pSvr)
		{
            var isKipEnabled = !ConfigurationManager.AppSettings.AllKeys.Contains("KIPServiceEnabled") || Convert.ToBoolean(ConfigurationManager.AppSettings["KIPServiceEnabled"]);
            var isNsiEnabled = !ConfigurationManager.AppSettings.AllKeys.Contains("NSIServiceEnabled") || Convert.ToBoolean(ConfigurationManager.AppSettings["NSIServiceEnabled"]);
            var isVioEnabled = !ConfigurationManager.AppSettings.AllKeys.Contains("VIOServiceEnabled") || Convert.ToBoolean(ConfigurationManager.AppSettings["VIOServiceEnabled"]);
            var list = new List<IAsyncTask>
		    {
                new LegalEntityStatusRefreshTask(),
			    //Оповещение пользователей ПТК ДОКИП о снижении фактического остатка резерва ниже (или равно) минимально допустимого(ому).  
                new ROPSLimitNotifyTask(),
                new OnesImportResetTask()
            };
            if(isKipEnabled)
                list.Add(new KIPNotifyTask());
            if(isNsiEnabled)
                list.Add(new NSISyncTask());
            if (isVioEnabled)
                list.Add(new VIOSyncTask());
            list.ForEach(a=> pSvr.AddDelegate(a.Process));

			pSvr.StartService();
		}

		public static void StartAsyncServices()
        {
            lock (typeof(AsyncServicesSystem))
            {
                if (ScheduleService != null)
                {
                    if(ScheduleService.ProcessThread == null || !ScheduleService.ProcessThread.IsAlive || 
                        (ScheduleService.ProcessThread.ThreadState != ThreadState.Running && ScheduleService.ProcessThread.ThreadState != ThreadState.WaitSleepJoin))
                        InitScheduleService(ScheduleService);        
                    return;
                }
                ScheduleService = new GenericScheduleService();
                InitScheduleService(ScheduleService);
            }
        }
	}
}
