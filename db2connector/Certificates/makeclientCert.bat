c:\openssl\bin\openssl genrsa -des3 -out client.key 1024
c:\openssl\bin\openssl req -new -key client.key -out client.csr
c:\openssl\bin\openssl x509 -req -days 365 -in client.csr -CA server.crt -CAkey server.key -set_serial 01 -out client.crt

c:\openssl\bin\openssl pkcs12 -export -out client.pfx -inkey client.key -in client.crt 
pause
