﻿using System.Text;
using System.Text.RegularExpressions;
using log4net.Appender;
using log4net.Core;

namespace PFR_INVEST.Proxy.Tools
{
    /// <summary>
    ///  НЕ РАБОТАЕТ из-за ущербного синтаксиса параметров DB2 комманд.
    /// </summary>
    internal class FullQueryAppender : ForwardingAppender
    {
        protected override void Append(LoggingEvent loggingEvent)
        {
            var loggingEventData = loggingEvent.GetLoggingEventData();

            if (loggingEventData.Message.Contains("?"))
            {
                StringBuilder messageBuilder = new StringBuilder();
                string message = loggingEventData.Message;
                var counter = 0;
                while (message.Contains("?"))
                {
                    var index = message.IndexOf('?');
                    message = message.Remove(index, 1);
                    var newPrm = $"p{counter++}";
                    message = message.Insert(index, newPrm);
                    message = message.Replace(newPrm, $"@{newPrm}");
                }


                var queries = Regex.Split(message, @"command\s\d+:");

                foreach (var query in queries)
                    messageBuilder.Append(ReplaceQueryParametersWithValues(query));

                loggingEventData.Message = messageBuilder.ToString();
            }

            base.Append(new LoggingEvent(loggingEventData));
        }

        private static string ReplaceQueryParametersWithValues(string query)
        {
            return Regex.Replace(query, @"@p\d(?=[,);\s])(?!\s*=)", match =>
            {
                Regex parameterValueRegex = new Regex($@".*{match}\s*=\s*(.*?)\s*[\[].*");
                return parameterValueRegex.Match(query).Groups[1].ToString();
            });
        }
    }
}
