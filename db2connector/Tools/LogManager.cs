﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;
using db2connector;
using NHibernate.Linq;
using PFR_INVEST.Common.Logger;
using ILog = log4net.ILog;

namespace PFR_INVEST.Proxy.Tools
{
	internal class LogManager
	{
		public static readonly ILog MainLog = log4net.LogManager.GetLogger("db2connector");
        public static readonly ILog AuthLog = log4net.LogManager.GetLogger("Auth");
        public static readonly ILog KIPLog = log4net.LogManager.GetLogger("Integration.KIP.Log");
        public static readonly ILog PlainSQLLog = log4net.LogManager.GetLogger("PlainSQLLog");
        public static readonly ILog SysLog = log4net.LogManager.GetLogger("syslog");
        public static readonly ILog ASSLog = log4net.LogManager.GetLogger("AsyncService.Log");
        

        private static LogManager _singleton;

	    public static void Log(string message, ILog logger = null, LogSeverity severity = LogSeverity.Info)
	    {
	        logger = logger ?? MainLog;
            switch (severity)
            {
                case LogSeverity.Debug:
                    logger.Debug(message);
                    return;
                case LogSeverity.Warning:
                    logger.Warn(message);
                    return;
                case LogSeverity.Error:
                    logger.Error(message);
                    return;
                case LogSeverity.Fatal:
                    logger.Fatal(message);
                    return;
                default:
                    logger.Info(message);
                    return;
            }
        }

        public static LogManager Instance
		{
            get { return _singleton ?? (_singleton = new LogManager()); }
		}

		public LogManager()
		{
		    IsSqlTraceEnabled = ConfigurationManager.AppSettings["SqlQueriesTraceEnabled"] != null && Convert.ToBoolean(ConfigurationManager.AppSettings["SqlQueriesTraceEnabled"]);
            _isAsyncOpLogEnabled = ConfigurationManager.AppSettings["IsAsyncOpLogEnabled"] != null && Convert.ToBoolean(ConfigurationManager.AppSettings["IsAsyncOpLogEnabled"]);
		    //InitializeLogger();
		}

	    private static bool _isAsyncOpLogEnabled;

	    private static readonly object Locker = new object();

        public bool IsSqlTraceEnabled { get; set; }


	    public static void Log2KIP(string message, LogSeverity severity = LogSeverity.Info)
	    {
		    if (KIPLog == null) return;
            Log(message, KIPLog, severity);
	    }

	    public void LogKIPException(string message, Exception ex)
	    {
		    if (KIPLog == null) return;
            LogException(ex, message, KIPLog);
	    }


		public static void LogAuthEx(Exception ex, string message = null)
		{
		    if (AuthLog == null) return;
            LogException(ex, message, AuthLog);
		}


		public static void LogAuth(string message, LogSeverity severity = LogSeverity.Info)
		{
		    if (AuthLog == null) return;
            Log(message, AuthLog, severity);
		}


		public static void LogException(Exception ex, string message = null, ILog logger = null)
		{
		    logger = logger ?? MainLog;
			try
			{
                Log(message, logger, LogSeverity.Fatal);
                if (ex is FaultException<ExceptionDetail>)
			    {
			        var fe = (ex as FaultException<ExceptionDetail>).Detail;
			        while (fe.InnerException != null)
			        {
			            fe = fe.InnerException;
			        }
			        Log(fe.ToString(), logger, LogSeverity.Fatal);
			        Log(fe.StackTrace, logger, LogSeverity.Fatal);
			    }
			    else
			    {
                    var ie = ex;
                    while (ie.InnerException != null)
                    {
                        ie = ie.InnerException;
                    }
                    Log(ie == null ? ex.ToString() : ie.ToString(), logger, LogSeverity.Fatal);
                    Log(ie == null ? ex.StackTrace : ie.StackTrace, logger, LogSeverity.Fatal);
                    Log(Environment.NewLine, logger, LogSeverity.Fatal);
			    }

			}
			catch (Exception genEx)
			{
				Log(genEx.Message, logger, LogSeverity.Fatal);
			}
		}
        
        public static void LogLeefSysLog(IISService serviceBase, string userName, string eventName, string resultName, string detailMessage, LogSeverity severity = LogSeverity.Info, Dictionary<string, string> additionalAttributes = null)
        {
            var clientIpAddr = serviceBase?.HostName ?? "";
            var clientHostName = serviceBase?.HostName ?? "";
            //OperationContext context = OperationContext.Current;
            //MessageProperties prop = context?.IncomingMessageProperties;
            //RemoteEndpointMessageProperty endpoint = prop?[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            //if (endpoint != null)
            //{
            //    clientIpAddr = endpoint.Address;
            //    clientHostName = endpoint.Address;
            //}
            var currentIpAddress = GetLocalIPAddress();

            var sb = new StringBuilder();
            var appVer = Assembly.GetExecutingAssembly().GetName().Version.ToString(2);

            sb.Append($"LEEF:1.0|АйТи|DOKIP|v{appVer}|{eventName} ({resultName})|");

            var attrs = new List<NHibernate.Linq.Tuple<string, string>>
            {
                new NHibernate.Linq.Tuple<string, string> {First = "Cat", Second = "Application"},
                new NHibernate.Linq.Tuple<string, string> {First = "devTime", Second = DateTime.UtcNow.ToString("MMM dd yyyy hh:mm:ss")},
                //new Tuple<string, string> {First = "devTimeFormat", Second = "Application"},
                //new Tuple<string, string> {First = "proto", Second = "Application"},
                new NHibernate.Linq.Tuple<string, string> {First = "sev", Second = ((int)severity*2).ToString()},
                new NHibernate.Linq.Tuple<string, string> {First = "src", Second = clientIpAddr},
                new NHibernate.Linq.Tuple<string, string> {First = "dst", Second = currentIpAddress},
                //new Tuple<string, string> {First = "srcPort", Second = "Application"},
                //new Tuple<string, string> {First = "dstPort", Second = "Application"},
                //new Tuple<string, string> {First = "srcMAC", Second = "Application"},
                //new Tuple<string, string> {First = "dstMAC", Second = "Application"},
                new NHibernate.Linq.Tuple<string, string> {First = "identHostName", Second = clientHostName},
                //new Tuple<string, string> {First = "identNetBios", Second = "Application"}
                new NHibernate.Linq.Tuple<string, string> {First = "msg", Second = detailMessage},
                new NHibernate.Linq.Tuple<string, string> {First = "usrName", Second = userName}
            };
            if (additionalAttributes != null && additionalAttributes.Count > 0)
            {
                attrs.AddRange(additionalAttributes.Select(aa => new NHibernate.Linq.Tuple<string, string> {First = $"@{aa.Key}", Second = aa.Value}));
            }

            foreach (var item in attrs)
            {
                sb.Append($"{item.First}={item.Second}\t");
            }

            Log(sb.ToString(), SysLog, severity);
        }

        public static void LogLeefSysLogEx(IISService serviceBase, string userName, string eventName, string resultName, string detailMessage, string hostName, LogSeverity severity = LogSeverity.Info, Dictionary<string, string> additionalAttributes = null)
        {
            var clientIpAddr = HttpContext.Current.Request.UserHostAddress ?? "";
            try
            {
                if (string.IsNullOrEmpty(clientIpAddr) || clientIpAddr.StartsWith("::"))
                    clientIpAddr = ((RemoteEndpointMessageProperty) OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name]).Address;
            }
            catch
            {
                //
            }
            var clientHostName = hostName ?? "";
            var currentIpAddress = GetLocalIPAddress();

            var sb = new StringBuilder();
            var appVer = Assembly.GetExecutingAssembly().GetName().Version.ToString(2);

            sb.Append($"LEEF:1.0|АйТи|DOKIP|v{appVer}|{eventName} ({resultName})|");

            var attrs = new List<NHibernate.Linq.Tuple<string, string>>
            {
                new NHibernate.Linq.Tuple<string, string> {First = "Cat", Second = "Application"},
                new NHibernate.Linq.Tuple<string, string> {First = "devTime", Second = DateTime.UtcNow.ToString("MMM dd yyyy hh:mm:ss")},
                new NHibernate.Linq.Tuple<string, string> {First = "sev", Second = ((int)severity*2).ToString()},
                new NHibernate.Linq.Tuple<string, string> {First = "src", Second = clientIpAddr},
                new NHibernate.Linq.Tuple<string, string> {First = "dst", Second = currentIpAddress},
                new NHibernate.Linq.Tuple<string, string> {First = "identHostName", Second = clientHostName},
                new NHibernate.Linq.Tuple<string, string> {First = "msg", Second = detailMessage},
                new NHibernate.Linq.Tuple<string, string> {First = "usrName", Second = userName}
            };
            if (additionalAttributes != null && additionalAttributes.Count > 0)
            {
                attrs.AddRange(additionalAttributes.Select(aa => new NHibernate.Linq.Tuple<string, string> { First = $"@{aa.Key}", Second = aa.Value }));
            }

            foreach (var item in attrs)
            {
                sb.Append($"{item.First}={item.Second}\t");
            }

            Log(sb.ToString(), SysLog, severity);
        }


        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            return "";
        }

	    public static void ASSLogWrap(string p0)
	    {
            if(!_isAsyncOpLogEnabled) return;
	        ASSLog.Info(p0);
	    }
	}
}
