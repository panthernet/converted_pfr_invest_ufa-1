﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Odbc;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Threading.Tasks;
using db2connector;
using db2connector.AsyncServices;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Engine.Query;
using NHibernate.Hql.Ast;
using NHibernate.Hql.Ast.ANTLR;
using Npgsql;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.Proxy;
using PFR_INVEST.Proxy.Tools;
using ServerSettings = db2connector.Contract.ServerSettings;

#if WEBCLIENT
using NHibernate.Impl;
#else
using db2connector.Contract;
#endif
namespace PFR_INVEST.Proxy
{
	public class IISServiceBase : IDisposable
	{
        public static readonly ServiceDBType DBType;
	    public static bool IsDecimalFixNeeded;

        //private const string DATA_SOURCE_NAME = "DataSourceName";
        private const string PAGE_SIZE = "PageSize";
	    public static readonly string DB_CONNECTION_STRING = ConfigurationManager.AppSettings["DBConnectionString"] ?? "DSN=PFR-INVEST;";
        /// <summary>
        /// Хранилище данных сессии
        /// </summary>
        public static Dictionary<string, SessionData> SessionsDataList = new Dictionary<string, SessionData>();
		/// <summary>
		/// Объект межпотоковой синхронизации
		/// </summary>
		protected static readonly object Locker = new object();
		/// <summary>
		/// Последний известный идентификатор сессии (для случаев, когда требуется логирование выхода при аномальном выключении клиента)
		/// Получается, что на момент логирования операции контекста уже не существует
		/// </summary>
		protected string LastKnownSessionId { get; set; }
		/// <summary>
		/// Класс данных сессии
		/// </summary>        
		public class SessionData
		{
			/// <summary>
			/// Был ли логирован выход из системы
			/// </summary>
			public volatile bool IsCloseOpLogged;
		}

		public virtual bool IsUserAuthenticated { get; } = true;



        public virtual void CheckAuthentication()
		{
			if (IsUserAuthenticated == false)
				throw new Exception("Session in not authenticated");
		}

		public int GetMetricActiveUser()
		{
		    lock (Locker)
		    {
		        return SessionsDataList.Count(u => !u.Value.IsCloseOpLogged);
		    }
		}

	    public bool GetMetricDecimalFix()
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				//Стандартно 100.000 будет возвращено из БД как 100000
				var res = session.CreateSQLQuery("VALUES(100.000)").List<decimal>().First();
				return res == 100000;
			}
		}

        /// <summary>
        /// Если мы соединены с БД DB2
        /// </summary>
        public static bool IsDB2 { get; private set; }

        /// <summary>
        /// Если мы соединены с БД PostgreSQL
        /// </summary>
        public static bool IsPostgreSQL{ get; private set; }

	    static IISServiceBase()
	    {
            DBType = ServiceDBType.DB2;
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DBType"))
            {
                var value = Convert.ToString(ConfigurationManager.AppSettings["DBType"]);
                DBType = value == "DB2" ? ServiceDBType.DB2 : ServiceDBType.PostgreSQL;
                IsDB2 = DBType == ServiceDBType.DB2;
                IsPostgreSQL = DBType == ServiceDBType.PostgreSQL;
            }
	    }

		public IISServiceBase()
		{
		    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;

            if (OperationContext.Current != null && OperationContext.Current.SessionId != null)
			{
				lock (Locker)
				{
					var sessionID = OperationContext.Current.SessionId;

					if (!SessionsDataList.ContainsKey(sessionID))
						SessionsDataList.Add(sessionID, new SessionData());
					else 
						SessionsDataList[sessionID].IsCloseOpLogged = false;
				}
				OperationContext.Current.OperationCompleted += Current_OperationCompleted;
			}

		    if (DBType == ServiceDBType.PostgreSQL)
		    {
		        DATA_SCHEME = DATA_SCHEME.ToLower();
		        TABLE_CONTRAGENT = TABLE_CONTRAGENT.ToLower();
                TABLE_LEGALENTITY = TABLE_LEGALENTITY.ToLower();
                TABLE_BANKACCOUNT = TABLE_BANKACCOUNT.ToLower();
                TABLE_ACCOUNT = TABLE_ACCOUNT.ToLower();
                TABLE_ERZL = TABLE_ERZL.ToLower();
                TABLE_PORTFOLIO = TABLE_PORTFOLIO.ToLower();
                TABLE_FINREGISTER = TABLE_FINREGISTER.ToLower();
                TABLE_REGISTER = TABLE_REGISTER.ToLower();
                TABLE_APPROVEDOC = TABLE_APPROVEDOC.ToLower();
                TABLE_FZ = TABLE_FZ.ToLower();
                TABLE_ASG_FIN_TR = TABLE_ASG_FIN_TR.ToLower();
                TABLE_CONTRACT = TABLE_CONTRACT.ToLower();
                TABLE_YEARS = TABLE_YEARS.ToLower();
                TABLE_CURRENCY = TABLE_CURRENCY.ToLower();
                TABLE_PFRBANKACCOUNT = TABLE_PFRBANKACCOUNT.ToLower();
                TABLE_ACC_BANK_TYPE = TABLE_ACC_BANK_TYPE.ToLower();
                TABLE_CURS = TABLE_CURS.ToLower();
                TABLE_MONTHS = TABLE_MONTHS.ToLower();
                TABLE_EDO_ODKF010 = TABLE_EDO_ODKF010.ToLower();
                TABLE_EDO_ODKF012 = TABLE_EDO_ODKF012.ToLower();
                TABLE_EDO_ODKF014 = TABLE_EDO_ODKF014.ToLower();
                TABLE_EDO_ODKF015 = TABLE_EDO_ODKF015.ToLower();
                TABLE_EDO_ODKF016 = TABLE_EDO_ODKF016.ToLower();
                TABLE_EDO_ODKF020 = TABLE_EDO_ODKF020.ToLower();
                TABLE_EDO_ODKF022 = TABLE_EDO_ODKF022.ToLower();
                TABLE_EDO_ODKF024 = TABLE_EDO_ODKF024.ToLower();
                TABLE_EDO_ODKF025 = TABLE_EDO_ODKF025.ToLower();
                TABLE_EDO_ODKF026 = TABLE_EDO_ODKF026.ToLower();
                TABLE_EDO_ODKF040 = TABLE_EDO_ODKF040.ToLower();
                TABLE_EDO_ODKF050 = TABLE_EDO_ODKF050.ToLower();
                TABLE_EDO_ODKF060 = TABLE_EDO_ODKF060.ToLower();
                TABLE_EDO_ODKF070 = TABLE_EDO_ODKF070.ToLower();
                TABLE_EDO_ODKF080 = TABLE_EDO_ODKF080.ToLower();
                TABLE_EDO_ODKF140 = TABLE_EDO_ODKF140.ToLower();
		        TABLE_EDO_ODKF401402 = TABLE_EDO_ODKF401402.ToLower();
		        TABLE_EDO_LOG = TABLE_EDO_LOG.ToLower();
		        TABLE_EDO_EXEPTION = TABLE_EDO_EXEPTION.ToLower();

                TABLE_F40_RRZ = TABLE_F40_RRZ.ToLower();
                TABLE_F40_DETAILS = TABLE_F40_DETAILS.ToLower();
                TABLE_SI_REGISTER = TABLE_SI_REGISTER.ToLower();
                TABLE_SI_TRANSFER = TABLE_SI_TRANSFER.ToLower();
                TABLE_SI_UKPLAN = TABLE_SI_UKPLAN.ToLower();
                TABLE_REQ_TRANSFER = TABLE_REQ_TRANSFER.ToLower();
                TABLE_DIRECTION_SPN = TABLE_DIRECTION_SPN.ToLower();
                TABLE_OPERATION = TABLE_OPERATION.ToLower();
                TABLE_RATING = TABLE_RATING.ToLower();
            }
		    IsDecimalFixNeeded = GetMetricDecimalFix();
		}

        #region DB connection methods

        void Current_OperationCompleted(object sender, EventArgs e)
		{
			LastKnownSessionId = OperationContext.Current.SessionId;
		}

		/*private DbConnection m_connection;
		public DbConnection connection
		{
			get
			{
				CheckAuthentication();
				if (m_connection == null
					|| m_connection.State == ConnectionState.Broken
					|| m_connection.State == ConnectionState.Closed)
				{
					try
					{
						CloseDBConnection();
					}
					catch
					{

					}

					CreateDBConnection();
				}

				return m_connection;
			}
		}*/

		private int _pageSize;
		public int PageSize
		{
			get
			{
				if (_pageSize > 0)
					return _pageSize;
				var s = ConfigurationManager.AppSettings[PAGE_SIZE];
				if (string.IsNullOrEmpty(s) || !int.TryParse(s, out _pageSize) || _pageSize < 1)
				{
					return _pageSize = 5000;
				}

				return _pageSize;
			}
		}

		public void Dispose()
		{
			lock (Locker)
			{
				var sessionId = OperationContext.Current == null ? (string.IsNullOrEmpty(LastKnownSessionId) ? null : LastKnownSessionId) : OperationContext.Current.SessionId;
				if (sessionId != null)
				{
					if (SessionsDataList.ContainsKey(sessionId) && !SessionsDataList[sessionId].IsCloseOpLogged)
					{
						SaveJornalLog(JournalEventType.LOGOUT, "По обрыву связи и таймауту", 0, string.Empty);
					}
					if (SessionsDataList.ContainsKey(sessionId))
						SessionsDataList.Remove(sessionId);
				}
				else LogManager.Log("Возможно, не прошла очистка данных сессии из-за нулевого Id сессии!");
			}
		}

		/*public void CloseDBConnection(IDbConnection conn)
		{
			if (m_connection == null)
				return;
			try
			{
                m_connection.Close();
                m_connection.Dispose();
			}
			catch
			{
			    // ignored
			}
			finally
			{
                m_connection = null;
			}
		}*/

		public static string DATA_SCHEME = "PFR_BASIC";
		public static string TABLE_CONTRAGENT = "CONTRAGENT";
		public static string TABLE_LEGALENTITY = "LEGALENTITY";
		public static string TABLE_BANKACCOUNT = "BANKACCOUNT";
		public static string TABLE_ACCOUNT = "ACCOUNT";
		public static string TABLE_ERZL = "ERZL";
		public static string TABLE_PORTFOLIO = "PORTFOLIO";
		public static string TABLE_FINREGISTER = "FINREGISTER";
		public static string TABLE_REGISTER = "REGISTER";
		public static string TABLE_APPROVEDOC = "APPROVEDOC";
		public static string TABLE_FZ = "FZ";
		public static string TABLE_ASG_FIN_TR = "ASG_FIN_TR";
		public static string TABLE_CONTRACT = "CONTRACT";
		public static string TABLE_YEARS = "YEARS";
		public static string TABLE_CURRENCY = "CURRENCY";
		public static string TABLE_PFRBANKACCOUNT = "PFRBANKACCOUNT";
		public static string TABLE_ACC_BANK_TYPE = "ACC_BANK_TYPE";
		public static string TABLE_CURS = "CURS";
		public static string TABLE_MONTHS = "MONTHS";
		public static string TABLE_F40_RRZ = "F40_RRZ";
		public static string TABLE_F40_DETAILS = "F40_DETAILS";
		public static string TABLE_SI_REGISTER = "SI_REGISTER";
		public static string TABLE_SI_TRANSFER = "SI_TRANSFER";
		public static string TABLE_SI_UKPLAN = "SI_UKPLAN";
		public static string TABLE_REQ_TRANSFER = "REQ_TRANSFER";
		public static string TABLE_DIRECTION_SPN = "DIRECTION_SPN";
		public static string TABLE_OPERATION = "OPERATION";
		public static string TABLE_RATING = "RATING";

        public static string TABLE_EDO_LOG = "EDO_LOG";
        public static string TABLE_EDO_EXEPTION = "EDO_EXEPTION";
        public static string TABLE_EDO_ODKF010 = "EDO_ODKF010";
        public static string TABLE_EDO_ODKF012 = "EDO_ODKF012";
        public static string TABLE_EDO_ODKF014 = "EDO_ODKF014";
        public static string TABLE_EDO_ODKF015 = "EDO_ODKF015";
        public static string TABLE_EDO_ODKF016 = "EDO_ODKF016";
        public static string TABLE_EDO_ODKF020 = "EDO_ODKF020";
        public static string TABLE_EDO_ODKF022 = "EDO_ODKF022";
        public static string TABLE_EDO_ODKF024 = "EDO_ODKF024";
        public static string TABLE_EDO_ODKF025 = "EDO_ODKF025";
        public static string TABLE_EDO_ODKF026 = "EDO_ODKF026";
        public static string TABLE_EDO_ODKF040 = "EDO_ODKF040";
        public static string TABLE_EDO_ODKF050 = "EDO_ODKF050";
        public static string TABLE_EDO_ODKF060 = "EDO_ODKF060";
        public static string TABLE_EDO_ODKF070 = "EDO_ODKF070";
        public static string TABLE_EDO_ODKF080 = "EDO_ODKF080";
        public static string TABLE_EDO_ODKF140 = "EDO_ODKF140";
        public static string TABLE_EDO_ODKF401402 = "EDO_ODKF401402";


        public static string DATE_FORMAT_STRING = "yyyy-MM-dd";

		public ServiceData GetServiceVersion()
        {
		    try
		    {
		        var db = ConfigurationManager.AppSettings["DBConnectionString"];
		        return new ServiceData
		        {
		            Version = Assembly.GetExecutingAssembly().GetName().Version,
                    DBText = IsDB2 ? $"DB2: {string.Join(";",db.Split(';').Where(a=> a.ToLower().StartsWith("dsn")).Select(a=> a.ToLower().Replace("dsn=","")).ToArray())}" 
                        : $"PostgreSQL: {string.Join("; ",db.Split(';').Where(a=> a.StartsWith("server") || a.StartsWith("port")|| a.StartsWith("database")).Select(a=> a.Replace("server=","").Replace("port=","").Replace("database=","db=")).ToArray())}",
                    AuthText = "A"
		        };
		    }
		    catch (Exception ex)
		    {
                LogManager.Log("Ping exception!!!");
                LogManager.LogException(ex);
                throw;
		    }
        }

		public virtual string GetServiceType()
		{
			return "Regular Service";
		}

	    internal static volatile bool IsDBInitialized;

	    public ServerSettings GetServerSettings()
	    {
	        ConfigurationManager.RefreshSection("appSettings");
	        var s = new ServerSettings {DbName = GetSourceNameFromConnString(DB_CONNECTION_STRING)};
	        bool val;
	        if (!bool.TryParse(ConfigurationManager.AppSettings.Get("IsOpfrTransfersEnabled"), out val))
	            val = false;
	        s.IsOpfrTransfersEnabled = val;
	        if (!bool.TryParse(ConfigurationManager.AppSettings.Get("ShowClientNotificationSettings"), out val))
	            val = true;
	        s.ShowClientNotificationSettings = val;
	        s.PageSize = PageSize;
	        return s;
	    }

        public ServerSettings OpenDBConnection()
		{
		    IsDBInitialized = true;
            AsyncServicesSystem.StartAsyncServices();//Запускаем асинхронные обновления
			TraceClientDisconnect();
		    return GetServerSettings();
		}

	    private string GetSourceNameFromConnString(string connStr)
	    {
	        var result = "PTK";
	        if (DBType == ServiceDBType.DB2)
	        {
	            var item = connStr.Split(';').FirstOrDefault(a => a.StartsWith("DSN="));
                if(item != null) result = item.Replace("DSN=","");
	        }else if (DBType == ServiceDBType.PostgreSQL)
	        {
                var item = connStr.Split(';').FirstOrDefault(a => a.StartsWith("database="));
                if (item != null) result = item.Replace("database=", "");

            }
            return result;
        }

		protected virtual void TraceClientDisconnect()
		{
		}

		public virtual void SaveJornalLog(JournalEventType code, string name, long? documentId, string otype)
		{
		}

		/// <summary>
		/// Финальное закрытие соединения с сервисом
		/// </summary>
		public void CloseConnection()
		{
			//Асинхронно закрываем все соединения
			var sessionID = OperationContext.Current.SessionId;
			
			Task.Factory.StartNew(() =>
			{
				try
				{
					SaveJornalLog(JournalEventType.LOGOUT, "По закрытию соединения", 0, string.Empty);
					lock (Locker)
					{
						if (SessionsDataList.ContainsKey(sessionID))
							SessionsDataList[sessionID].IsCloseOpLogged = true;
					}


					////отлавливаем исключение, т.к. пытаемся закрыть канал при незавершенном сообщении
					//try
					//{
					//    if (OperationContext.Current.Channel.State != CommunicationState.Closed && OperationContext.Current.Channel.State != CommunicationState.Closing)
					//        OperationContext.Current.Channel.Close();
					//}
					//catch { }
					//CloseDBConnection();
				}
				catch { }
			});


		}
        #endregion

        #region Basic DB methods


        protected void DBCommandWrapper(string query, Action<DBCommandLogging> method)
	    {
	        using (var cmd = new DBCommandLogging(query))
	        {
	            method(cmd);
	        }
	    }

        protected T DBCommandWrapper<T>(string query,Func<DBCommandLogging, T> method)
        {
            using (var cmd = new DBCommandLogging(query))
            {
                return method(cmd);
            }
        }

        protected void AddParamWithValue(DbParameterCollection col, string paramName, object value)
        {
            if (DBType == ServiceDBType.DB2)
                ((OdbcParameterCollection)col).AddWithValue(paramName, value);
            else
                ((NpgsqlParameterCollection)col).AddWithValue(paramName, value);

        }

        #endregion

        #region Cell Readers
        protected static string ReadStringValue(DbDataReader reader, int ordinal)
		{
			string result;

			try
			{
				result = reader.IsDBNull(ordinal) ? "" : reader.GetValue(ordinal).ToString();
			}
			catch
			{
				result = "";
			}

			return result;
		}

		protected static int ReadInt32Value(DbDataReader reader, int ordinal)
		{
			int result;

			try
			{
				result = reader.IsDBNull(ordinal) ? 0 : Convert.ToInt32(reader.GetValue(ordinal)) /*reader.GetInt32(ordinal)*/;
			}
			catch
			{
				result = 0;
			}

			return result;
		}

		protected static long ReadInt64Value(DbDataReader reader, int ordinal)
		{
			long result;

			try
			{
				if (reader.IsDBNull(ordinal))
				{
					result = 0;
					//Log("DBNull");
				}
				else
					result = Convert.ToInt64(reader.GetValue(ordinal));//reader.GetInt64(ordinal);

				//Log("res="+result + " reader=" + reader.GetValue(ordinal));
			}
			catch (Exception e)
			{
				LogManager.LogException(e);
				result = 0;
			}

			return result;
		}

		protected static long? ReadNullableInt64Value(DbDataReader reader, int ordinal)
		{
			long? result;

			try
			{
				result = reader.IsDBNull(ordinal) ? null : (long?)Convert.ToInt64(reader.GetValue(ordinal));
			}
			catch (Exception e)
			{
				LogManager.LogException(e);
				result = null;
			}

			return result;
		}


		protected static decimal? FixDecimal(decimal? value, int scale)
		{
		    if (IsPostgreSQL)
		        return value;
#if DB2_DECIMAL_FIX
			scale = 0;//Фикс сдвига разряда
#endif
			if (!value.HasValue)
				return value;

			var result = value;
			for (int i = 0; i < scale; i++)
			{
				result = result * 0.1M;
			}
			return result;
		}

		protected static decimal? ReadDecimalNullableValue(DbDataReader reader, int ordinal, short scale)
		{
			decimal? result;

			try
			{
				if (reader.IsDBNull(ordinal))
				{
					result = null;
				}
				else
				{
					result = Convert.ToDecimal(reader.GetValue(ordinal));
					result = FixDecimal(result, scale);
				}
			}
			catch
			{
				result = 0;
			}

			return result;
		}

		protected static decimal ReadDecimalValue(DbDataReader reader, int ordinal, short scale)
		{
			decimal result;

			try
			{
				if (reader.IsDBNull(ordinal))
				{
					result = 0;
				}
				else
				{
					result = Convert.ToDecimal(reader.GetValue(ordinal));
					result = FixDecimal(result, scale) ?? 0;
				}
			}
			catch
			{
				result = 0;
			}

			return result;
		}

		protected static DateTime ReadDateTimeValue(DbDataReader reader, int ordinal)
		{
			DateTime result;

			try
			{
				result = reader.IsDBNull(ordinal) ? DateTime.MinValue : reader.GetDateTime(ordinal);
			}
			catch
			{
				try
				{
					result = reader.IsDBNull(ordinal) ? DateTime.MinValue : reader.GetDateTime(ordinal);
				}
				catch { }
				result = DateTime.MinValue;
			}

			return result;
		}

		protected String GetSqlByHql(IQuery query, ISession session)
		{
			var sessionImp = (ISessionImplementor)session;
			var translatorFactory = new ASTQueryTranslatorFactory();
#if !WEBCLIENT
            var translators = translatorFactory.CreateQueryTranslators(query.QueryString, null, false, sessionImp.EnabledFilters, sessionImp.Factory);
#else
		    var translators = ((SessionFactoryImpl) session.SessionFactory).Settings.QueryTranslatorFactory.CreateQueryTranslators((IQueryExpression) query, null, false, sessionImp.EnabledFilters,
		        sessionImp.Factory);
#endif
            return translators[0].SQLString;
		}
#endregion
	}
}
