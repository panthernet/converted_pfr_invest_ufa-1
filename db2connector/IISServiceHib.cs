﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Xml;
using db2connector.AsyncServices;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Metadata;
using NHibernate.Persister.Entity;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using PFR_INVEST.Auth.ServerData;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions.PfrBranchReport;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.BranchReport;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.ServiceItems;
using PFR_INVEST.Proxy;
using PFR_INVEST.Proxy.Tools;
using Formatting = System.Xml.Formatting;
using IQueryable = NHibernate.Persister.Entity.IQueryable;
using Template = PFR_INVEST.DataObjects.Template;
#if WEBCLIENT
using NHibernate.Util;
#endif

namespace db2connector
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    [HibDataBehavior]
    public partial class IISServiceHib : IISServiceShared
    {
        public decimal NormalizePlainSQLDecimal(decimal value)
        {
            return IsDB2 ? value / 100m : value;
        }

        internal static bool IsMockDb;
        static IISServiceHib()
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MockDBConnection"))
                IsMockDb = Convert.ToBoolean(ConfigurationManager.AppSettings["MockDBConnection"]);

            DataAccessSystem.LogException = LogExceptionForHib;
            //MQCoreConnector.LogException = LogExceptionForHib;
            //MQCoreConnector.LogMessage = LogMessageForHib;
            AuthType = ServiceAuthType.AD;
            if (ConfigurationManager.AppSettings.AllKeys.Contains("AuthType"))
            {
                var value = Convert.ToString(ConfigurationManager.AppSettings["AuthType"]);
                switch (value)
                {
                    case "AD":
                        AuthType = ServiceAuthType.AD;
                        break;
                    case "MOCK":
                    case "NONE":
                        AuthType = ServiceAuthType.MOCK;
                        break;
                    case "ECASA":
                        AuthType = ServiceAuthType.ECASA;
                        break;
                    case "QUICKECASA":
                        AuthType = ServiceAuthType.QUICKECASA;
                        break;
                }
                LogManager.LogAuth("Выбран коннектор: " + value);
            }

            LogManager.Log2KIP("KIP interaction online");
        }

        private static void LogExceptionForHib(Exception pEx)
        {
            LogManager.LogException(pEx);
        }

        private static void LogMessageForHib(string pMessage)
        {
            LogManager.Log(pMessage);
        }

#region Common Hib DataAccess Functions

        public Type GetType(string pType)
        {
            return Assembly.GetAssembly(typeof(BaseDataObject)).GetType(pType);
        }

        public IList GetList<T>()
        {
            return GetList(typeof(T).FullName);
        }

        public IList GetList(string pType)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            {
                return session.CreateCriteria(type).List();
            }
        }

        public IList GetListLimit(string pType, int limit)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            {
                var list = session.CreateCriteria(type).AddOrder(Order.Asc(Projections.Id())).SetMaxResults(limit).List();
                return list;
            }
        }

        public IList GetListPage(string pType, int startIndex, int limit)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            {
                var list = session.CreateCriteria(type).AddOrder(Order.Asc(Projections.Id())).SetFirstResult(startIndex).SetMaxResults(limit).List();
                return list;
            }
        }

        public IList GetListPageConditions(string pTypeName, int startIndex, List<ListPropertyCondition> propertyConditionList)
        {
            using (var session = OpenSessionByType(GetType(pTypeName)))
            {
                var c = session.CreateCriteria(pTypeName);
                return GetPropertyConditionsCriteria(c, propertyConditionList)
                    .AddOrder(Order.Asc(Projections.Id()))
                    .SetFirstResult(startIndex)
                    .SetMaxResults(PageSize)
                    .List();
            }
        }

        public IList GetListByProperty<T>(string propertyName, object value)
        {
            return GetListByProperty(typeof(T).FullName, propertyName, value, null);
        }

        public IList GetListByProperty(string pType, string propertyName, object value)
        {
            return GetListByProperty(pType, propertyName, value, null);
        }

        private IList GetListByProperty(string pType, string propertyName, object value, ISession session)
        {
            bool needCleanup = session == null;
            var type = GetType(pType);
            try
            {
                session = session ?? OpenSessionByType(type);
                var list = session.CreateCriteria(type).Add(Restrictions.Eq(propertyName, value)).List();
                return list;
            }
            finally
            {
                if (needCleanup)
                    session?.Close();
            }
        }

        public long GetListByPropertyCount(string pType, string propertyName, object value)
        {
            return GetListByPropertyCount(pType, propertyName, value, null);
        }

        public long GetListCount(string pType)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            {
                return session.CreateCriteria(type)
                    .SetProjection(Projections.RowCountInt64())
                    .UniqueResult<long>();
            }
        }

        private long GetListByPropertyCount(string pType, string propertyName, object value, ISession session)
        {
            bool needCleanup = session == null;
            var type = GetType(pType);
            try
            {
                session = session ?? OpenSessionByType(type);
                return session.CreateCriteria(type).Add(Restrictions.Eq(propertyName, value))
                    .SetProjection(Projections.RowCountInt64())
                    .UniqueResult<long>();
            }
            finally
            {
                if (needCleanup)
                    session?.Close();
            }
        }

        public IList GetListByPropertyCondition(string pType, string propertyName, object value, string operation)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            {
                var list = session.CreateCriteria(type);
                switch (operation.ToLower())
                {
                    case "eq":
                        list.Add(Restrictions.Eq(propertyName, value));
                        break;
                    case "like":
                        list.Add(Restrictions.Like(propertyName, value));
                        break;
                    case "neq":
                        list.Add(value != null
                            ? Restrictions.Not(Restrictions.Eq(propertyName, value))
                            : Restrictions.IsNotNull(propertyName));
                        break;
                    case "in":
                        if (value is ArrayList) value = (value as ArrayList).ToArray();
                        list.Add(Restrictions.In(propertyName, (object[])value));
                        break;

                    case "isnull":
                        list.Add(Restrictions.IsNull(propertyName));
                        break;
                    case "isnotnull":
                        list.Add(Restrictions.IsNotNull(propertyName));
                        break;

                    //todo: Заполнить необходимые операции при необходимости
                    default:
                        throw new ArgumentException($"Операция {operation} отсутствует в описании метода");
                }

                return list.List();
            }
        }

        public IList GetListByPropertyConditions<T>(List<ListPropertyCondition> propertyConditionList)
        {
            return GetListByPropertyConditions(typeof(T).FullName, propertyConditionList);
        }

        public IList GetListByPropertyConditions<T>(List<ListPropertyCondition> propertyConditionList, ISession session)
        {
            return GetListByPropertyConditions(typeof(T).FullName, propertyConditionList, session);
        }

        public string GetXMLByPropertyConditions<T>(List<ListPropertyCondition> propertyConditionList)
        {
            return GetXMLByPropertyConditions(typeof(T).FullName, propertyConditionList);
        }

        private static ICriteria GetPropertyConditionsCriteria(ICriteria c, List<ListPropertyCondition> propertyConditionList)
        {
            if (propertyConditionList != null)
                foreach (var p in propertyConditionList)
                {
                    switch (p.Operation.ToLower())
                    {
                        case "eq":
                            c.Add(Restrictions.Eq(p.Name, p.Value));
                            break;
                        case "like":
                            c.Add(Restrictions.Like(p.Name, p.Value));
                            break;
                        case "neq":
                            c.Add(p.Value != null
                                ? Restrictions.Not(Restrictions.Eq(p.Name, p.Value))
                                : Restrictions.IsNotNull(p.Name));
                            break;
                        case "in":
                            c.Add(Restrictions.In(p.Name, p.Values));
                            break;
                        case "le":
                            c.Add(Restrictions.Le(p.Name, p.Value));
                            break;
                        case "lt":
                            c.Add(Restrictions.Lt(p.Name, p.Value));
                            break;
                        case "ge":
                            c.Add(Restrictions.Ge(p.Name, p.Value));
                            break;
                        case "gt":
                            c.Add(Restrictions.Gt(p.Name, p.Value));
                            break;

                        case "isnull":
                            c.Add(Restrictions.IsNull(p.Name));
                            break;
                        case "isnotnull":
                            c.Add(Restrictions.IsNotNull(p.Name));
                            break;

                        //todo: Заполнить необходимые операции при необходимости
                        default:
                            throw new ArgumentException($"Операция {p.Operation} отсутствует в описании метода");
                    }
                }
            return c;
        }

        public IList GetListByPropertyConditions(string pType, List<ListPropertyCondition> propertyConditionList)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            {
                var c = GetPropertyConditionsCriteria(session.CreateCriteria(type), propertyConditionList);
                return c.List();
            }
        }

        public IList GetListByPropertyConditions(Type type, ISession session, List<ListPropertyCondition> propertyConditionList)
        {
            var c = GetPropertyConditionsCriteria(session.CreateCriteria(type), propertyConditionList);
            return c.List();
        }

        public IList GetListByPropertyConditions(string pType, List<ListPropertyCondition> propertyConditionList,
            ISession session)
        {
            var type = GetType(pType);
            return GetPropertyConditionsCriteria(session.CreateCriteria(type), propertyConditionList).List();
        }

        /// <summary>
        /// Получение словаря для сопоставления имени поля в БД и поля в типизированной сущности
        /// </summary>
        private static Dictionary<PropertyInfo, string> GetObjectHibernateDictionary(IClassMetadata meta,
            IQueryable tMeta, Type type)
        {
            if (meta == null || tMeta == null)
                return null;

            //словарь для сопоставления имени поля в БД и поля в типизированной сущности
            var d = new Dictionary<PropertyInfo, string>();

            for (int i = 0; i < meta.PropertyNames.Length; i++)
            {
                var iName = meta.PropertyNames[i];
                var cList = tMeta.GetPropertyColumnNames(i);
                var prInfo = type.GetProperty(iName);
                var tableName = (tMeta.TableName.Contains(".") ? tMeta.TableName.Split('.')[1] : tMeta.TableName).Trim('"');

                if (cList.Length == 1 && prInfo != null)
                {
                    if (string.IsNullOrEmpty(cList.First()))
                        d[prInfo] = prInfo.Name;
                    else
                        d[prInfo] = $"{tableName}_{cList.First().Trim('"')}";
                }
            }

            if (tMeta.IdentifierColumnNames.Length == 1 && !string.IsNullOrEmpty(tMeta.IdentifierColumnNames.First()))
            {
                d[type.GetProperty(tMeta.IdentifierPropertyName)] = tMeta.IdentifierColumnNames.First();
            }

            return d;
        }

        public Dictionary<string, object> GetEntityDBProjection(string pType, object source)
        {
            var type = GetType(pType);

            if (source != null && type != source.GetType()) return null;

            var meta = DataAccessSystem.GetClassMetadata(pType);
            var tMeta = meta as SingleTableEntityPersister;

            if (meta == null || tMeta == null)
                return null;

            var d = GetObjectHibernateDictionary(meta, tMeta, type);

            var dict = new Dictionary<string, object>();

            foreach (var pair in d)
            {
                object val = null;
                if (source != null)
                    val = pair.Key.GetValue(source, null);
                var nKey = pair.Value;
                if (pair.Value.Contains("."))
                    nKey = pair.Value.Split('.')[1];
                dict[nKey.Replace("\"", "").ToUpper()] = val;
            }

            return dict;
        }

        public string GetXMLByList(string pType, IList list)
        {
            var type = GetType(pType);

            var meta = DataAccessSystem.GetClassMetadata(pType);
            var tMeta = meta as SingleTableEntityPersister;
            var aMeta = (AbstractEntityPersister) tMeta;
            var tableName = (tMeta.TableName.Contains(".") ? tMeta.TableName.Split('.')[1] : tMeta.TableName).Trim('"');

            if (meta == null || aMeta == null)
                return null;

            var d = GetObjectHibernateDictionary(meta, tMeta, type);
            var idInfo = type.GetProperty("ID");

            string result;

            using (var ms = new MemoryStream())
            using (var xWriter = new XmlTextWriter(ms, Encoding.UTF8))
            {
                xWriter.Formatting = Formatting.Indented;
                xWriter.Indentation = 4;
                xWriter.IndentChar = ' ';

                // это только часть общего отчета по данным, полный отчет будет сформирован и сохранен позднее
                xWriter.WriteStartDocument();
                xWriter.WriteStartElement("TABLE");
                xWriter.WriteAttributeString("NAME", tableName);

                foreach (var row in list)
                {
                    xWriter.WriteStartElement("ROW");
                    if (idInfo != null) xWriter.WriteAttributeString("ID", idInfo.GetValue(row, null).ToString());
                    foreach (var pair in d)
                    {
                        var val = pair.Key.GetValue(row, null);
                        xWriter.WriteElementString(pair.Value, val?.ToString() ?? string.Empty);
                    }

                    xWriter.WriteEndElement();
                }

                xWriter.WriteEndElement();
                xWriter.WriteEndDocument();
                xWriter.Flush();
                var buffer = ms.ToArray();
                result = Encoding.UTF8.GetString(buffer);
            }
            result = result.Substring(result.IndexOf("<TABLE"));
            return result;
        }

        public string GetXMLByPropertyConditions(string pType, List<ListPropertyCondition> propertyConditionList)
        {
            //var c = GetPropertyConditionsCriteria(session.CreateCriteria(type), propertyConditionList);
            var tList = GetListByPropertyConditions(pType, propertyConditionList);

            return GetXMLByList(pType, tList);
        }

        /// <summary>
        /// Метод выполняет поиск записей определенного типа и возвращает только часть записей
        /// </summary>
        /// <param name="pType">Наименование записей</param>
        /// <param name="xml">Содержимое отчета</param>
        /// <returns>Список записей p_type</returns>
        public IList GetListByXML(string pType, string xml)
        {
            if (string.IsNullOrEmpty(xml.Trim())) return new List<object>();
            var type = GetType(pType);

            var meta = DataAccessSystem.GetClassMetadata(pType);
            var tMeta = meta as SingleTableEntityPersister;

            if (meta == null || tMeta == null)
                return null;

            //словарь для сопоставления имени поля в БД и поля в типизированной сущности
            Dictionary<string, PropertyInfo> d = new Dictionary<string, PropertyInfo>();
            var tableName = (tMeta.TableName.Contains(".") ? tMeta.TableName.Split('.')[1] : tMeta.TableName).Trim('"');

            for (int i = 0; i < meta.PropertyNames.Length; i++)
            {
                var iName = meta.PropertyNames[i];
                var cList = tMeta.GetPropertyColumnNames(i);
                var prInfo = type.GetProperty(iName);


                if (cList.Length == 1 && prInfo != null)
                {
                    if (string.IsNullOrEmpty(cList.First()))
                    {
                        // поля, вычисляемые по другим таблицам
                        d[prInfo.Name] = prInfo;
                    }
                    else
                    {
                        d[$"{tableName}_{cList.First().Trim('"')}"] = prInfo;
                    }
                }
            }

            if (tMeta.IdentifierColumnNames.Length == 1 && !string.IsNullOrEmpty(tMeta.IdentifierColumnNames.First()))
            {
                d[tMeta.IdentifierColumnNames.First()] = type.GetProperty(tMeta.IdentifierPropertyName);
            }

            //var idInfo = type.GetProperty("ID");

            var iList = new List<object>();

            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(xml.ToCharArray(), 0, xml.Length)))
            using (var xReader = XmlReader.Create(ms))
            {
                bool targetRows = false;
                bool readCompleted = false;
                object item = null;
                while (!readCompleted && xReader.Read())
                {
                    if (xReader.IsStartElement())
                    {
                        switch (xReader.Name)
                        {
                            case "TABLE": //определяем, что это начало описания нужного типа записей
                                var atr = xReader.GetAttribute("NAME");
                                if (!string.IsNullOrEmpty(atr) && atr == tableName)
                                    targetRows = true;
                                else
                                    targetRows = false;
                                break;
                            case "ROW": //проверяем, что идет чтение нужного типа записей и создаем объект
                                if (targetRows)
                                    item = type.GetConstructor(new Type[0])?.Invoke(new object[0]);
                                break;
                            default: //чтение полей записи
                                if (targetRows && item != null && d.Keys.Contains(xReader.Name))
                                {
                                    var pInfo = d[xReader.Name];
                                    if (!xReader.IsEmptyElement && xReader.Read())
                                    {
                                        pInfo.SetValue(item, GetTypedValue(pInfo.PropertyType, xReader.Value), null);
                                    }
                                    else
                                    {
                                        pInfo.SetValue(item, GetTypedValue(pInfo.PropertyType, null), null);
                                    }
                                }
                                break;
                        }
                    }

                    if (xReader.NodeType == XmlNodeType.EndElement)
                    {
                        switch (xReader.Name)
                        {
                            case "TABLE": //проверка завершения нужного типа записей, выход из цикла
                                if (targetRows)
                                    readCompleted = true;
                                break;
                            case "ROW": // проверка завершения чтения полей записи, добавления в список
                                if (targetRows && item != null)
                                    iList.Add(item);
                                break;
                        }
                    }
                }
            }
            return iList;
        }

        private static object GetTypedValue(Type type, string obj)
        {
            if (type == typeof(string)) return obj?.Replace("\n", "\r\n");

            Type rawType;

            if (type.IsNullable())
            {
                if (string.IsNullOrEmpty(obj))
                {
                    return null;
                }

                rawType = type.NullableOf();
            }
            else
                rawType = type;

            if (rawType == typeof(int))
                return Convert.ToInt32(obj);
            if (rawType == typeof(long))
                return Convert.ToInt64(obj);
            if (rawType == typeof(DateTime))
            {
                DateTime dt;
                if (DateTime.TryParse(obj, out dt))
                    return dt;
                if (DateTime.TryParse(obj, new CultureInfo("ru-RU"), DateTimeStyles.None, out dt))
                    return dt;
                throw new Exception($"Не удалось распознать значение даты {obj}");
                //return Convert.ToDateTime(obj,);
            }
            if (rawType == typeof(bool))
                return Convert.ToBoolean(obj);
            if (rawType == typeof(decimal))
                return Convert.ToDecimal(obj);
            if (rawType == typeof(float))
                return Convert.ToDouble(obj);
            if (rawType == typeof(TimeSpan))
            {
                TimeSpan v;
                if (TimeSpan.TryParse(obj, out v))
                    return v;
                return null;
            }
            return obj;
        }

        public long GetListByPropertyConditionsCount<TT>(List<ListPropertyCondition> propertyConditionList)
        {
            return GetListByPropertyConditionsCount(typeof(TT).FullName, propertyConditionList);
        }

        public long GetListByPropertyConditionsCount(string pType, List<ListPropertyCondition> propertyConditionList)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            {
                var c = GetPropertyConditionsCriteria(session.CreateCriteria(type), propertyConditionList);

                return c.SetProjection(Projections.RowCountInt64())
                    .UniqueResult<long>();
            }
        }

        public TT GetByID<TT>(object id)
        {
            return (TT)GetByID(typeof(TT).FullName, id);
        }

        public object GetByID(string pType, object pIId)
        {
            try
            {
                object result;
                var type = GetType(pType);
                using (var session = OpenSessionByType(type))
                {
                    result = session.Get(type, pIId);
                }
                return result;
            }
            catch (Exception ex)
            {
                LogExceptionForHib(ex);
                throw;
            }
        }

        /// <summary>
        /// Сохранение списка сущностей
        /// </summary>
        /// <param name="typeFullName">Полное имя типа</param>
        /// <param name="sList">Список сущностей</param>
        /// <param name="returnIds">Возвращать идентификаторы сущностей</param>
        public IEnumerable<object> BatchSave(string typeFullName, IEnumerable<object> sList, bool returnIds = false)
        {
            return TransactionWrapper(session =>
            {
                var idList = new List<object>();
                sList.ForEach(a =>
                {
                    var id = SaveEntity(a, session);
                    if (returnIds)
                        idList.Add(id);
                });

                return idList;
            });
        }

        public void BatchDelete(string typeFullName, List<object> dList)
        {
            var type = GetType(typeFullName);
            using (var session = OpenSessionByType(type))
            using (var transaction = session.BeginTransaction())
            {
                dList.ForEach(session.Delete);
                try
                {
                    session.Flush();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    LogExceptionForHib(ex);
                    throw;
                }
            }
        }

        public object Save(string pType, object pObject)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            using (var transaction = session.BeginTransaction())
            {
                session.SaveOrUpdate(pObject);
                try
                {
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    LogExceptionForHib(ex);
                    throw;
                }
                var oId = session.GetIdentifier(pObject);
                return oId;
            }
        }

        public void Delete(string pType, object pObject)
        {
            var type = GetType(pType);
            using (var session = OpenSessionByType(type))
            using (var transaction = session.BeginTransaction())
            {
                session.Delete(pObject);
                try
                {
                    session.Flush();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    LogExceptionForHib(ex);
                    throw;
                }
            }

        }

        public object GetObjectProperty(string pType, object pIId, string pSPropertyName)
        {
            return GetObjectPropertyInternal(pType, pIId, pSPropertyName);
        }

        private object GetObjectPropertyInternal(string pType, object pIId, string pSPropertyName,
            ISession session = null)
        {
            object result;
            var needCleanup = session == null;
            var type = GetType(pType);
            try
            {
                session = session ?? OpenSessionByType(type);
                result = session.Get(type, pIId);
                if (result == null)
                    return null;

                var t = result.GetType();

                var prop = t.GetProperty(pSPropertyName);
                if (prop == null)
                    throw new Exception("Property not found");

                result = prop.GetValue(result, null);
                NHibernateUtil.Initialize(result);
            }
            finally
            {
                if (needCleanup && session != null)
                    session.Close();
            }

            return result;
        }


        private long GetObjectPropertyCount(string pType, object pIId, string pSPropertyName, ISession session = null)
        {
            bool needCleanup = session == null;
            var type = GetType(pType);
            try
            {
                session = session ?? OpenSessionByType(type);
                var result = session.Get(type, pIId);
                if (result == null)
                    return 0;

                var t = result.GetType();

                var prop = t.GetProperty(pSPropertyName);
                if (prop == null)
                    throw new Exception("Property not found");

                result = prop.GetValue(result, null);
                if (result == null)
                    return 0;
                //приводим к коллекции (каунт для нее работает очень быстро)
                var col = result as ICollection;
                if (col != null) return col.Count;
                //если не получилось приводим к IEnumerable и высчитываем кол-во элементов
                var enumerable = result as IEnumerable;
                if (enumerable != null)
                    return enumerable.Cast<object>().Count();

                // NHibernate.NHibernateUtil.Initialize(result);
            }
            finally
            {
                if (needCleanup && session != null)
                    session.Close();
            }

            throw new Exception($"Не удалось привести свойство {pSPropertyName} к коллекции!");

        }


        /// <summary>
        /// Получает кол-во элементов в свойстве-коллекции
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pIId"></param>
        /// <param name="pSPropertyName"></param>
        /// <returns></returns>
        public long GetObjectPropertyCount(string pType, object pIId, string pSPropertyName)
        {
            return GetObjectPropertyCount(pType, pIId, pSPropertyName, null);
        }

#endregion

#region Delete Document

        public void SaveDeleteEntry(string docType, long? docID, int opType, DeletedDataItem data = null)
        {
            SaveDeleteEntryLog(docType, string.Empty, docID, opType, data);
        }

        public void SaveDeleteEntryLog(string docType, string docCaption, long? docID, int opType, DeletedDataItem data = null)
        {
            using (var session = OpenSessionByType(typeof(DeleteDocumentEntry)))
            {
                //транзация для обхода внутр. исключения хибернейта
                var transaction = session.BeginTransaction();
                SaveDeleteEntryLog(docType, docCaption, docID, opType, session, data);
                transaction.Commit();
                session.Flush();
            }
        }

        private void SaveDeleteEntryLog(string docType, string docCaption, long? docID, int opType, ISession session, DeletedDataItem data = null)
        {
            string opName = string.Empty;
            switch (opType)
            {
                case 1:
                    opName = "Удаление";
                    break;
                case 2:
                    opName = "Откат шага";
                    break;
                default:
                    opName = "Отправка в архив";
                    break;
            }
            var del = new DeleteDocumentEntry
            {
                DocumentCaption = docCaption,
                Date = DateTime.Now.Date,
                DocumentID = docID,
                Operation = opName,
                DocumentType = docType,
                RelatedData = data != null ? JsonConvert.SerializeObject(data) : null
            };
            session.Save(del);
        }

        public IList<DeleteDocumentEntry> GetDeletedDocumentsList()
        {
            using (var session = OpenSessionByType(typeof(DeleteDocumentEntry)))
            {
                var list = session.CreateCriteria(typeof(DeleteDocumentEntry)).List<DeleteDocumentEntry>();
                foreach (var item in list)
                    item.Unwrap();
                return list;
            }
        }

        public WebServiceDataError RestoreDocument(long delId)
        {
            var entry = GetByID<DeleteDocumentEntry>(delId);
            if (entry == null || !entry.DocumentID.HasValue) return WebServiceDataError.DocumentDeleteEntryNotFound;
            entry.Unwrap();
            var data = new DeletedData
            {
                Id = entry.DocumentID.Value,
                StringType = entry.DocumentType,
                OptionalEntryId = delId
            };
            if(entry.Data != null)
                data.PreviousStatus = entry.Data.PreviousStatus;

            var list = new List<DeletedData>();
            list.Add(data);
            if (entry.Data != null && entry.Data.DataList != null)
                list.AddRange(entry.Data.DataList);
            foreach (var item in list)
            {
                var result = RestoreDocument(item);
                if (result != WebServiceDataError.None)
                    return result;
            }

            foreach (var item in list)
            {
                if(item.OptionalEntryId.HasValue)
                    DeleteRestoreEntry(item.OptionalEntryId.Value);
            }

            return WebServiceDataError.None;
        }

        private WebServiceDataError RestoreDocument(DeletedData data)
        {
            try
            {
                //var entry = GetByID<DeleteDocumentEntry>(delId);
                //if (entry == null) return WebServiceDataError.DocumentDeleteEntryNotFound;
               // entry.Unwrap();
                var otype = data.StringType;
                var id = data.Id;
                var delId = data.OptionalEntryId ?? 0;
                var prevStatus = data.PreviousStatus;

                var ass = Assembly.GetAssembly(typeof(BaseDataObject));
                if (!otype.StartsWith("PFR_INVEST."))
                    otype = $"{typeof(BaseDataObject).Namespace}.{otype}";
                var type = ass.GetType(otype, false);
                if (type != null && id > 0)
                {
                    //работаем с новым типом
                    var aList = type.GetCustomAttributes(typeof(UseRestoreChildAttribute), false).Select(a => (UseRestoreChildAttribute)a);
                    TransactionWrapper(session =>
                    {
                        UndeleteEntry(type, id, session, prevStatus);

                        foreach (var item in aList)
                        {
                            //значение статуса не вынесено в параметры специально для обхода ограничений связанных с разными типами поля статуса (Int, long...)
                            session.CreateQuery(string.Format(@"update {0} i set i.{2}={3} where i.{1}=:id",
                                item.ChildType.Name,
                                item.ChildIdFieldName, item.ChildStatusField, item.ChildOkStatus))
                                .SetParameter("id", id)
                                .ExecuteUpdate();
                        }

                    });
                    return WebServiceDataError.None;
                }

                switch (data.StringType.ToLower())
                {
                    case "siviewmodel":
                    case "npfviewmodel":
                    case "bankviewmodel":
                    case "bankagentviewmodel":
                        IList<Contragent> list = null;
                        using (var session = DataAccessSystem.OpenSession())
                        {
                            if (string.Compare(otype, "siviewmodel", true) == 0 ||
                                string.Compare(otype, "bankviewmodel", true) == 0 ||
                                string.Compare(otype, "bankagentviewmodel", true) == 0)
                                list =
                                    session.CreateQuery("SELECT le.Contragent FROM LegalEntityHib le WHERE le.ID = :id")
                                        .SetParameter("id", id)
                                        .List<Contragent>();
                            else if (string.Compare(otype, "npfviewmodel", true) == 0)
                                list =
                                    session.CreateQuery(
                                        "SELECT le.Contragent FROM LegalEntityHib le WHERE le.ContragentID = :id")
                                        .SetParameter("id", id)
                                        .List<Contragent>();

                            if (list == null)
                                return WebServiceDataError.None;
                        }
                        foreach (var contr in list)
                        {
                            if (contr.StatusID != -1)
                                continue;
                            contr.StatusID = 1;
                            Save("Contragent", contr);
                        }

                        break;
                    case "contract":
                    case "sicontractviewmodel":
                        IList<PFR_INVEST.DataObjects.Contract> list2;
                        using (var session = OpenSessionByType(typeof(PFR_INVEST.DataObjects.Contract)))
                        {
                            list2 =
                                session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract)).Add(Restrictions.Eq("ID", id)).List<PFR_INVEST.DataObjects.Contract>();
                        }
                        foreach (var obj in list2)
                        {
                            if (obj.Status != -1)
                                continue;
                            obj.Status = 1;
                            Save(typeof(PFR_INVEST.DataObjects.Contract).Name, obj);
                        }
                        break;
                    case "security":
                    case "securityviewmodel":
                        IList<Security> list3;
                        using (var session = OpenSessionByType(typeof(Security)))
                        {
                            list3 =
                                session.CreateCriteria(typeof(Security)).Add(Restrictions.Eq("ID", id)).List<Security>();
                        }
                        foreach (var obj in list3)
                        {
                            if (obj.Status != -1)
                                continue;
                            obj.Status = 0;
                            Save(typeof(Security).Name, obj);
                        }
                        break;
                    case "pfraccountviewmodel":
                        IList<PfrBankAccount> list4;
                        using (var session = OpenSessionByType(typeof(PfrBankAccount)))
                        {
                            list4 =
                                session.CreateCriteria(typeof(PfrBankAccount))
                                    .Add(Restrictions.Eq("ID", id))
                                    .List<PfrBankAccount>();
                        }
                        foreach (var obj in list4)
                        {
                            if (obj.StatusID != -1)
                                continue;
                            obj.StatusID = 0;
                            Save(typeof(PfrBankAccount).Name, obj);
                        }
                        break;
                    case "portfolio":
                    case "portfolioviewmodel":
                        Portfolio item;
                        using (var session = OpenSessionByType(typeof(Portfolio)))
                        {
                            item =
                                session.CreateCriteria(typeof(Portfolio))
                                    .Add(Restrictions.Eq("ID", id))
                                    .UniqueResult<Portfolio>();
                            if (item == null || item.StatusID != -1)
                                break;
                            item.StatusID = 0;
                        }
                        Save(typeof(Portfolio).Name, item);

                        List<PortfolioPFRBankAccount> list5;
                        using (var session = OpenSessionByType(typeof(PortfolioPFRBankAccount)))
                        {
                            list5 =
                                session.CreateCriteria(typeof(PortfolioPFRBankAccount))
                                    .Add(Restrictions.Eq("PortfolioID", id))
                                    .List<PortfolioPFRBankAccount>()
                                    .ToList();
                        }
                        foreach (var pacc in list5)
                        {
                            var item2 =
                                (PfrBankAccount)GetByID(typeof(PfrBankAccount).FullName, pacc.PFRBankAccountID);
                            item2.StatusID = 0;
                            Save(typeof(PfrBankAccount).FullName, item2);
                        }

                        break;
                    case "kbkviewmodel":
                        UndeleteEntry<KBK>(id);
                        break;

                    case "commonppviewmodel":
                        UndeleteEntry<AsgFinTr>(id);
                        break;
                    case "npfcodeviewmodel":
                        UndeleteEntry<NPFErrorCode>(id);
                        break;
                    case "legalentitycourierletterofattorney":
                        UndeleteEntry<LegalEntityCourierLetterOfAttorney>(id);
                        break;
                    case "legalentitycouriercertificate":
                        UndeleteEntry<LegalEntityCourierCertificate>(id);
                        break;
                    case "legalentityidentifier":
                        using (var session = DataAccessSystem.OpenSession())
                        {
                            var c = session.Get<LegalEntityIdentifier>(id);
                            if (c != null)
                            {
                                c.StatusID = 1;
                                session.SaveOrUpdate(c);
                                session.Flush();
                            }
                        }
                        break;
                    case "finregisterviewmodel":
                        UndeleteEntry<Finregister>(id);
                        break;
                    case "pfrbranch":
                    case "pfrbranchviewmodel":
                        UndeleteEntry<PFRBranch>(id);
                        /* SessionWrapper(session =>
                         {
                             session.CreateQuery(@"update PFRContact c set c.StatusID = 1 where c.PFRB_ID =")
                         });*/
                        break;
                    case "pfrcontact":
                    case "pfrcontactviewmodel":
                        UndeleteEntry<PFRContact>(id);
                        break;
                    case "registerviewmodel":
                        UndeleteEntry<Register>(id);
                        break;
                    case "depclaimselectparamsviewmodel":
                        UndeleteEntry<DepClaimSelectParams>(id);
                        break;
                    case "yearplanviewmodel":
                    {
                        //восстанавливаем план
                        if (id == 0 || !DeleteYearPlan(id, true))
                            return WebServiceDataError.DocumentDeleteError;
                    }
                        break;
                    case "spnreturnviewmodel":
                        using (var session = DataAccessSystem.OpenSession())
                        {
                            var c = session.Get<Register>(id);
                            if (c != null)
                            {
                                c.StatusID = 1;
                                session.SaveOrUpdate(c);
                            }
                            var regIdList =
                                session.CreateSQLQuery($"select id from {DATA_SCHEME}.{TABLE_REGISTER} where return_id = {id}")
                                    .List().OfType<long>().ToList();
                            if (regIdList.Any())
                            {
                                //восстанавливаем реестры
                                var str = GenerateStringFromList(regIdList);
                                session.CreateSQLQuery(
                                    $"update {DATA_SCHEME}.{TABLE_REGISTER} set status_id = 1 where id in ({str})").ExecuteUpdate();

                                //восстанавливаем финреестры
                                session.CreateSQLQuery(
                                    $"update {DATA_SCHEME}.{TABLE_FINREGISTER} set status_id = 1 where reg_id in ({str})").ExecuteUpdate();

                                //выбираем идентификаторы восстановленных финреестров
                                var finregIdList = session.CreateSQLQuery(
                                    $"select id from {DATA_SCHEME}.{TABLE_FINREGISTER} where reg_id in ({str})").List().OfType<long>().ToList();
                                var str2 = GenerateStringFromList(finregIdList);

                                //удаляем записи о восстановлении реестров и финреестров временного размещения, которые восстанавливаем автоматически с основным реестром
                                session.CreateSQLQuery(
                                    $"delete from {DATA_SCHEME}.DELETED_DOCUMENTS where DOCUMENT_ID in ({str}) and DOCUMENT_TYPE = '{otype}' and OPERATION = 'Отправка в архив'").ExecuteUpdate();

                                session.CreateSQLQuery(
                                    $"delete from {DATA_SCHEME}.DELETED_DOCUMENTS where DOCUMENT_ID in ({str2}) and DOCUMENT_TYPE = 'FinregisterViewModel' and OPERATION = 'Отправка в архив'").ExecuteUpdate();

                            }

                            session.Flush();
                        }
                        break;
                    default:
                        return WebServiceDataError.UnknownUndeleteType;
                }
                //if (string.IsNullOrEmpty(table)) return;
            }
            catch (Exception ex)
            {
                LogManager.LogException(ex);
                return WebServiceDataError.GeneralException;
            }

            //DeleteRestoreEntry(delId);
            return WebServiceDataError.None;
        }

        private void DeleteRestoreEntry(long? delID)
        {
            if(!delID.HasValue) return;
            DeleteDocumentEntry doc;
            using (var session = OpenSessionByType(typeof(DeleteDocumentEntry)))
            {
                doc =
                    (DeleteDocumentEntry)
                        session.CreateCriteria(typeof(DeleteDocumentEntry))
                            .Add(Restrictions.Eq("ID", delID))
                            .UniqueResult();
                if (doc == null)
                    return;
            }

            using (var session = OpenSessionByType(typeof(DeleteDocumentEntry)))
            using (var transaction = session.BeginTransaction())
            {
                session.Delete(doc);
                session.Flush();
                transaction.Commit();
            }
        }

        /// <summary>
        /// Простое восстановление (разархивирование) сущности путем выставления статус = 1
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идентификатор</param>
        private static void UndeleteEntry<T>(long? id, long? prevStatus = null) where T : IMarkedAsDeleted
        {
            if (!id.HasValue) return;
            using (var session = DataAccessSystem.OpenSession())
            {
                var c = session.Get<T>(id);
                if (c != null)
                {
                    c.StatusID = prevStatus.HasValue ? prevStatus.Value : 1;
                    session.SaveOrUpdate(c);
                    session.Flush();
                }
            }
        }

        private static void UndeleteEntry(Type type, long? id, ISession session, long? prevStatus)
        {
            if (!id.HasValue) return;
            var c = session.Get(type, id) as IMarkedAsDeleted;
            if (c != null)
            {
                c.StatusID = prevStatus.HasValue ? prevStatus.Value : 1;
                session.SaveOrUpdate(c);
                session.Flush();
            }
            else throw new Exception("Ожидался тип, реализующий IMarkedAsDeleted! " + type.FullName);
        }

#endregion

        public List<Contact> GetContactsForUKs()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateQuery(@"SELECT c FROM Contact c
					JOIN c.LegalEntity le
					JOIN le.Contragent cg
					WHERE (cg.TypeName = 'УК' OR cg.TypeName = 'ГУК') 
						AND (cg.StatusID = 1 OR (cg.StatusID = 6 OR cg.StatusID = 2) AND le.CloseDate > :today)
					ORDER BY le.Contragent.Name, c.FIO")
                    .SetParameter("today", DateTime.Today);

                return new List<Contact>(crit.List<Contact>());
            }
        }

#region Schils tab

        public List<SchilsCostsListItem> GetSchilsCostsList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<SchilsCostsListItem>();

                //Пустые бюджеты
                var budgets = session.CreateCriteria(typeof(BudgetHib))
                    .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join).List<BudgetHib>().ToList();

                result.AddRange(budgets.ConvertAll(bg =>
                {
                    return new SchilsCostsListItem
                    {
                        ID = bg.ID,
                        BudgetID = bg.ID,
                        Portfolio = bg.Portfolio.Year,
                        Operation1Type = SchilsCostsListItem.SchilsCostsListItemOpertion1Type.Budget,
                        Operation2Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCreation,
                        Operation3Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCreation,
                        Date = bg.Date,
                        Comment = bg.Comment,
                        Budget = bg.Sum
                    };
                }));

                //Пустые распоряжения
                var directions = session.CreateCriteria(typeof(DirectionHib))
                    .CreateCriteria("Budget", "b", JoinType.InnerJoin)
                    .SetFetchMode("Budget", FetchMode.Join)
                    .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join).List<DirectionHib>().ToList();

                result.AddRange(directions.ConvertAll(dir =>
                {
                    return new SchilsCostsListItem
                    {
                        ID = dir.ID,
                        BudgetID = dir.Budget.ID,
                        DirectionID = dir.ID,
                        DirectionNumber = dir.Number,
                        Portfolio = dir.Budget.Portfolio.Year,
                        Operation1Type = SchilsCostsListItem.SchilsCostsListItemOpertion1Type.Budget,
                        Operation2Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction,
                        Operation3Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction,
                        Number = dir.Number,
                        Date = dir.Date,
                        Comment = dir.Comment
                    };
                }));

                //Корректировки бюджетов
                var corrections = session.CreateCriteria(typeof(BudgetCorrectionHib), "corr")
                    .CreateCriteria("Budget", "b", JoinType.InnerJoin)
                    .SetFetchMode("Budget", FetchMode.Join)
                    .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join)
                    .AddOrder(Order.Asc("b.ID"))
                    .AddOrder(Order.Asc("corr.Date"))
                    .List<BudgetCorrectionHib>().ToList();

                BudgetCorrectionHib last = null;
                foreach (BudgetCorrectionHib corr in corrections)
                {
                    SchilsCostsListItem item = new SchilsCostsListItem
                    {
                        ID = corr.ID,
                        BudgetID = corr.Budget.ID,
                        Portfolio = corr.Budget.Portfolio.Year,
                        Operation1Type = SchilsCostsListItem.SchilsCostsListItemOpertion1Type.Budget,
                        Operation2Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCorrection,
                        Operation3Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.BudgetCorrection,
                        Date = corr.Date,
                        Comment = corr.Comment,
                        Budget =
                            (last == null || last.BudgetID != corr.BudgetID)
                                ? corr.Sum - corr.Budget.Sum
                                : corr.Sum - last.Sum
                    };
                    last = corr;
                    result.Add(item);
                }
                /*
					result.AddRange(corrections.ConvertAll<SchilsCostsListItem>(corr =>
					{
						return new SchilsCostsListItem
						{
							ID = corr.ID,
							BudgetID = corr.Budget.ID,
							Portfolio = corr.Budget.Year.Name,
							Operation1Type = SchilsCostsListItem.SchilsCostsListItemOpertion1Type.SchilsCostsListItemOpertion1TypeBudget,
							Operation2Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.SchilsCostsListItemOpertion2TypeBudgetCorrection,
							Date = corr.Date,
							Comment = corr.Comment,
							Budget = corr.Sum
						};
					}));*/

                //Перечисления + распоряжения
                var transfers = session.CreateCriteria(typeof(TransferHib))
                    .CreateCriteria("Direction", "d", JoinType.InnerJoin)
                    .SetFetchMode("Direction", FetchMode.Join)
                    .CreateCriteria("Budget", "b", JoinType.InnerJoin)
                    .SetFetchMode("Budget", FetchMode.Join)
                    .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join).List<TransferHib>().ToList();

                result.AddRange(transfers.ConvertAll(tr =>
                    new SchilsCostsListItem
                    {
                        ID = tr.ID,
                        BudgetID = tr.Direction.Budget.ID,
                        DirectionID = tr.Direction.ID,
                        ParentDirectionID = tr.Direction.ID,
                        DirectionNumber = tr.Direction.Number,
                        Portfolio = tr.Direction.Budget.Portfolio.Year,
                        Operation1Type = SchilsCostsListItem.SchilsCostsListItemOpertion1Type.Budget,
                        Operation2Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Direction,
                        Operation3Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Transfer,
                        Number = tr.Number,
                        Date = tr.Date,
                        Comment = tr.Comment,
                        Sum = tr.Sum // "каквлотусе"
                                     //Sum = -tr.Sum
                    }));

                //Возвраты + распоряжения
                var returns = session.CreateCriteria(typeof(ReturnHib))
                    .CreateCriteria("Budget", "b", JoinType.InnerJoin)
                    .SetFetchMode("Budget", FetchMode.Join)
                    .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join).List<ReturnHib>().ToList();

                result.AddRange(returns.ConvertAll(ret =>
                {
                    return new SchilsCostsListItem
                    {
                        ID = ret.ID,
                        BudgetID = ret.BudgetID ?? 0,
                        //DirectionID = ret.Direction.ID,
                        //DirectionNumber = ret.Direction.Number,
                        Portfolio = ret.Budget.Portfolio.Year,
                        Operation1Type = SchilsCostsListItem.SchilsCostsListItemOpertion1Type.Budget,
                        Operation2Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Return,
                        Operation3Type = SchilsCostsListItem.SchilsCostsListItemOpertion2Type.Return,
                        Number = ret.Number,
                        Date = ret.Date,
                        Comment = ret.Comment,
                        Sum = -ret.Sum // "каквлотусе"
                                       //Sum = ret.Sum
                    };
                }));

                return result;
            }
        }

        public List<Year> GetYearsListAvailableForBudget()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var c = DetachedCriteria.For<Budget>()
                    .SetProjection(Projections.Property("YearID"));

                return session.CreateCriteria(typeof(Year))
                    .Add(Subqueries.PropertyNotIn("ID", c))
                    .List<Year>().ToList();
            }
        }

        public List<BudgetCorrection> GetCorrectionsForBudget(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery("FROM BudgetCorrectionHib bc WHERE bc.BudgetID = :ID ORDER BY bc.Date ASC")
                        .SetParameter("ID", id)
                        .List<BudgetCorrection>();
                return list.ToList();
            }
        }

        public List<Budget> GetBudgetsListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria(typeof(BudgetHib))
                    .CreateCriteria("Portfolio", "y", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join)
                    .List<Budget>();
                return list.ToList();
            }
        }

        public List<DirectionTransferListItem> GetTransfersListForDirection(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list1 = session.CreateCriteria(typeof(TransferHib), "tr")
                    .Add(Restrictions.Eq("tr.DirectionID", id))
                    .List<Transfer>().ToList()
                    .ConvertAll(tr => new DirectionTransferListItem(tr));

                var result = list1;
                decimal? previousSum = 0;
                foreach (var item in result)
                {

                    item.Total = previousSum + item.Sum;
                    previousSum = item.Total;
                }

                return result;
            }
        }

#endregion

        private string CreateQueryForStatus(string query, string source, StatusFilter sf)
        {
            int cnt = sf?.Filter?.Length ?? 0;
            int idx = 0;
            if (cnt > 0)
            {
                bool firstIteration = true;
                query = string.Concat(query, " AND (");
                foreach (var f in sf.Filter)
                {
                    query = string.Concat(
                        firstIteration ? string.Empty : f.BOP == BOP.And ? " AND " : " OR ",
                        query,
                        string.Format(StatusFilter.FILTER_QUERY, source, f.OP, idx)
                        );
                    firstIteration = false;
                    idx++;
                }
                query = string.Concat(query, ")");
            }
            return query;
        }


        public List<RejectApplication> GetRejectApplications()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    @"SELECT ra.id,ra.regnum,ra.region,ra.regdate,ra.selection,ra.state_type,ra.error_code,ra.from,ra.portfolio,ra.contract_code,ra.fileid,
                             le.FORMALIZEDNAME as FROM_NAME,le2.FORMALIZEDNAME as SEL_NAME FROM PFR_BASIC.NPF_REJECT_APP ra
                                    LEFT JOIN PFR_BASIC.LEGALENTITY_IDENTIFIER leid ON leid.IDENTIFIER = ra.FROM
                                    LEFT JOIN PFR_BASIC.LEGALENTITY le ON le.ID = leid.LE_ID
                                    LEFT JOIN PFR_BASIC.LEGALENTITY_IDENTIFIER leid2 ON leid2.IDENTIFIER = ra.SELECTION
                                    LEFT JOIN PFR_BASIC.LEGALENTITY le2 ON le2.ID = leid2.LE_ID";
                var list = session.CreateSQLQuery(query)
                    .List<object[]>()
                    .Select(a => new RejectApplication(a)).ToList();
                return list;
            }
        }


        public IList<Contragent> GetNPFListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = @"SELECT le.Contragent 
								FROM LegalEntityHib le 
								JOIN le.Contragent c
								JOIN c.Status s
								WHERE LOWER(le.Contragent.TypeName) = :type";
                StatusFilter sf = StatusFilter.NotDeletedFilter;

                query = string.Concat(CreateQueryForStatus(query, "le.Contragent.Status", sf),
                    " ORDER BY le.Contragent.Name");

                var list = session.CreateQuery(query)
                    .SetParameter("type", "нпф")
                    .SetParameter("st0", sf.Filter[0].Identifier)
                    .List<Contragent>();
                return list;
            }
        }

        public IList<Contragent> GetFilteredNPFContragentListHib(StatusFilter filter, long[] excludedStatuses,
            long ownLeid)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //								JOIN le.Contragent c
                //								JOIN c.Status s 

                string query = $@"SELECT le.Contragent 
								FROM LegalEntityHib le 
                                WHERE LOWER(le.Contragent.TypeName) = :type and le.ID != {ownLeid}
                                and le.Contragent.StatusID NOT IN ({string.Join(",", excludedStatuses.Select(i => i.ToString()).ToArray())})";

                StatusFilter sf = StatusFilter.NotDeletedFilter;

                query = string.Concat(CreateQueryForStatus(query, "le.Contragent.Status", sf),
                    " ORDER BY le.Contragent.Name");

                var list = session.CreateQuery(query)
                    .SetParameter("type", "нпф")
                    .SetParameter("st0", sf.Filter[0].Identifier)
                    .List<Contragent>();
                return list;
            }
        }


        public IList<LegalEntity> GetFilteredNPFListLEHib(StatusFilter filter, long[] excludedStatuses)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = "SELECT le, le.Contragent, le.Contragent.Status "
                            + " FROM LegalEntityHib le "
                            + " WHERE LOWER(le.Contragent.TypeName) = :type and "
                            + " le.Contragent.StatusID NOT IN (" +
                            string.Join(",", excludedStatuses.Select(i => i.ToString()).ToArray()) + ")";

                //string.Join(",", p.HiddenPortfolioIDs.Select(id => id.ToString()).ToArray()), ")")

                int cnt1 = filter.Filter.Length;

                query = string.Concat(CreateQueryForStatus(query, "le.Contragent.Status", filter),
                    " ORDER BY le.Contragent.Name");

                var q = session.CreateQuery(query)
                    .SetParameter("type", "нпф");

                int idx = 0;
                if (cnt1 > 0)
                {
                    foreach (var f in filter.Filter)
                    {
                        q.SetParameter($"st{idx}", f.Identifier);
                        idx++;
                    }
                }

                var raw = q.List<object[]>();

                var list = raw.Select(m =>
                {
                    var le = (LegalEntity)m[0];
                    var ca = (Contragent)m[1];
                    var s = (Status)m[2];
                    if (ca != null)
                    {
                        ca.ExtensionData = new Dictionary<string, object>();
                        ca.ExtensionData.Add("Status", s);
                    }
                    if (le != null)
                    {
                        le.ExtensionData = new Dictionary<string, object>();
                        le.ExtensionData.Add("Contragent", ca);
                    }
                    return le;
                }).ToList();

                return list;
            }
        }

        public IList<LegalEntity> GetNPFGarantedListLEHib(StatusFilter filter, long garantType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = $@"SELECT le, le.Contragent, le.Contragent.Status 
							FROM LegalEntityHib le 
							WHERE LOWER(le.Contragent.TypeName) = :type 
							{(garantType == Element.SpecialDictionaryItems.ACBAll.ToLong() ? "" : "AND le.GarantACBID = :garant ")} ";

                int cnt = filter.Filter.Length;
                query = string.Concat(CreateQueryForStatus(query, "le.Contragent.Status", filter),
                    " ORDER BY le.Contragent.Name");

                var q = session.CreateQuery(query)
                    .SetParameter("type", "нпф");
                if (garantType != Element.SpecialDictionaryItems.ACBAll.ToLong())
                    q.SetParameter("garant", garantType.ToString());

                if (cnt > 0)
                {
                    int idx = 0;
                    foreach (var f in filter.Filter)
                    {
                        q.SetParameter($"st{idx}", f.Identifier);
                        idx++;
                    }
                }
                var raw = q.List<object[]>();

                var list = raw.Select(m =>
                {
                    var le = (LegalEntity)m[0];
                    var ca = (Contragent)m[1];
                    var s = (Status)m[2];
                    if (ca != null)
                    {
                        ca.ExtensionData = new Dictionary<string, object>();
                        ca.ExtensionData.Add("Status", s);
                    }
                    if (le != null)
                    {
                        le.ExtensionData = new Dictionary<string, object>();
                        le.ExtensionData.Add("Contragent", ca);
                    }
                    return le;
                }).ToList();

                return list;
            }
        }

        public IList<LegalEntity> GetNPFListLEHib(StatusFilter filter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = "SELECT le, le.Contragent, le.Contragent.Status "
                            + " FROM LegalEntityHib le "
                            + " WHERE LOWER(le.Contragent.TypeName) = :type";

                int cnt = filter?.Filter?.Length ?? 0;
                
                query = string.Concat(CreateQueryForStatus(query, "le.Contragent.Status", filter),
                    " ORDER BY le.Contragent.Name");

                var q = session.CreateQuery(query)
                    .SetParameter("type", "нпф");
                if (cnt > 0)
                {
                    int idx = 0;
                    foreach (var f in filter.Filter)
                    {
                        q.SetParameter($"st{idx}", f.Identifier);
                        idx++;
                    }
                }
                var raw = q.List<object[]>();

                var list = raw.Select(m =>
                {
                    var le = (LegalEntity)m[0];
                    var ca = (Contragent)m[1];
                    var s = (Status)m[2];
                    if (ca != null)
                    {
                        ca.ExtensionData = new Dictionary<string, object>();
                        ca.ExtensionData.Add("Status", s);
                    }
                    if (le != null)
                    {
                        le.ExtensionData = new Dictionary<string, object>();
                        le.ExtensionData.Add("Contragent", ca);
                    }
                    return le;
                }).ToList();

                return list;
            }
        }

        public IList<SimpleObjectValue> GetActiveLegalEntityByType(Contragent.Types type)
        {
            string sType = string.Empty;
            switch (type)
            {
                case Contragent.Types.Bank:
                    sType = "Банк";
                    break;
                case Contragent.Types.BankAgent:
                    sType = "Банк-агент";
                    break;
                case Contragent.Types.GUK:
                    sType = "ГУК";
                    break;
                case Contragent.Types.Npf:
                    sType = "НПФ";
                    break;
                case Contragent.Types.PFR:
                    sType = "ПФР";
                    break;
                case Contragent.Types.SD:
                    sType = "СД";
                    break;
                case Contragent.Types.UK:
                    sType = "УК";
                    break;
            }

            using (var session = DataAccessSystem.OpenSession())
            {
                var criteria = session.CreateCriteria<LegalEntityHib>()
                    .CreateCriteria("Contragent", "c", JoinType.InnerJoin)
                    .Add(Restrictions.Eq(Projections.Property<Contragent>(x => x.TypeName), sType))
                    .AddOrder(Order.Asc("Name"))
                    .CreateCriteria("Status", "s", JoinType.InnerJoin)
                    .Add(
                        Restrictions.Not(
                            Restrictions.Eq(
                                Projections.SqlFunction("lower", NHibernateUtil.String,
                                    Projections.Property<Status>(x => x.Name)), StatusIdentifier.S_DELETED.ToLower())))
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("ID"), "ID")
                        .Add(Projections.Property("FormalizedName"), "FormalizedName")
                        .Add(Projections.Property("INN"), "INN"))
                    .SetResultTransformer(Transformers.AliasToBean<LegalEntity>());

                var list = criteria.List<LegalEntity>().Select(le =>
                {
                    var so = new SimpleObjectValue { ID = le.ID, Name = le.FormalizedName };
                    so.ExtensionData = new Dictionary<string, object>();
                    so.ExtensionData.Add("INN", le.INN);
                    return so;
                }).ToList();
                return list;
            }
        }

        public long GetCountNPFListLEReorgHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var criteria = session.CreateCriteria<LegalEntityHib>()
                    .SetFetchMode("Chiefs", FetchMode.Join)
                    .CreateCriteria("Contragent", "c", JoinType.InnerJoin)
                    .Add(
                        Restrictions.Eq(
                            Projections.SqlFunction("lower", NHibernateUtil.String,
                                Projections.Property<Contragent>(x => x.TypeName)), "нпф"))
                    .SetFetchMode("Reorganizations", FetchMode.Eager)
                    .CreateCriteria("Status", "s", JoinType.InnerJoin)
                    .Add(
                        Restrictions.Not(
                            Restrictions.Eq(
                                Projections.SqlFunction("lower", NHibernateUtil.String,
                                    Projections.Property<Status>(x => x.Name)), StatusIdentifier.S_DELETED.ToLower())))
                    .SetResultTransformer(Transformers.DistinctRootEntity).SetProjection(Projections.RowCountInt64());

                return criteria.UniqueResult<long>();
            }
        }

        public IList<LegalEntity> GetNPFListLEReorgByPageHib(int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var criteria = session.CreateCriteria<LegalEntityHib>()
                    .SetFetchMode("Chiefs", FetchMode.Join)
                    .CreateCriteria("Contragent", "c", JoinType.InnerJoin)

                    .Add(
                        Restrictions.Eq(
                            Projections.SqlFunction("lower", NHibernateUtil.String,
                                Projections.Property<Contragent>(x => x.TypeName)), "нпф"))
                    .AddOrder(Order.Asc("Name"))
#if !WEBCLIENT
                    .SetFetchMode("Reorganizations", FetchMode.Join)
#endif
                    .CreateCriteria("Status", "s", JoinType.InnerJoin)
                    .Add(
                        Restrictions.Not(
                            Restrictions.Eq(
                                Projections.SqlFunction("lower", NHibernateUtil.String,
                                    Projections.Property<Status>(x => x.Name)), StatusIdentifier.S_DELETED.ToLower())))
                    .SetResultTransformer(Transformers.DistinctRootEntity)
                    .SetFirstResult(startIndex)
                    .SetMaxResults(PageSize);

                var result = criteria.List<LegalEntityHib>();

                foreach (var entity in result)
                {
                    if (entity.Contragent != null)
                    {
                        entity.Contragent.ExtensionData = new Dictionary<string, object>();
                        entity.Contragent.ExtensionData.Add("Status", entity.Contragent.Status);
                        entity.Contragent.ExtensionData.Add("Reorganizations",
                            entity.Contragent.Reorganizations.ToList());
                        entity.ExtensionData = new Dictionary<string, object>();
                        entity.ExtensionData.Add("Contragent", entity.Contragent);
                        entity.ExtensionData.Add("Chiefs", entity.Chiefs);
                    }
                }
                return result.Cast<LegalEntity>().ToList();
            }
        }



        public IList<LegalEntity> GetNPFListLEReorgHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var criteria = session.CreateCriteria<LegalEntityHib>()
                    .SetFetchMode("Chiefs", FetchMode.Join)
                    .CreateCriteria("Contragent", "c", JoinType.InnerJoin)

                    .Add(
                        Restrictions.Eq(
                            Projections.SqlFunction("lower", NHibernateUtil.String,
                                Projections.Property<Contragent>(x => x.TypeName)), "нпф"))
                    .AddOrder(Order.Asc("Name"))
                    .SetFetchMode("Reorganizations", FetchMode.Eager)
                    .CreateCriteria("Status", "s", JoinType.InnerJoin)
                    .Add(
                        Restrictions.Not(
                            Restrictions.Eq(
                                Projections.SqlFunction("lower", NHibernateUtil.String,
                                    Projections.Property<Status>(x => x.Name)), StatusIdentifier.S_DELETED.ToLower())))
                    .SetResultTransformer(Transformers.DistinctRootEntity);

                IList<LegalEntityHib> result = criteria.List<LegalEntityHib>();

                foreach (var entity in result)
                {
                    if (entity.Contragent != null)
                    {
                        entity.Contragent.ExtensionData = new Dictionary<string, object>();
                        entity.Contragent.ExtensionData.Add("Status", entity.Contragent.Status);
                        entity.Contragent.ExtensionData.Add("Reorganizations",
                            entity.Contragent.Reorganizations.ToList());
                        entity.ExtensionData = new Dictionary<string, object>();
                        entity.ExtensionData.Add("Contragent", entity.Contragent);
                        entity.ExtensionData.Add("Chiefs", entity.Chiefs);
                    }
                }
                return result.Cast<LegalEntity>().ToList();
            }
        }





        public IList<LegalEntity> GetNPFListLEAccHib(StatusFilter filter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string sQuery = @"
					  SELECT le
					  FROM LegalEntityHib le
					  JOIN le.Contragent c
					  JOIN c.Accounts acc
					  JOIN c.Status s
					  WHERE TRIM(LOWER(c.TypeName)) = :type AND TRIM(LOWER(acc.Kind.Measure)) = :zl {0} 
					  ORDER BY c.Name";
                sQuery = string.Format(sQuery, CreateQueryForStatus(string.Empty, "s", filter));

                var query = session.CreateQuery(sQuery)
                    .SetParameter("type", "нпф")
                    .SetParameter("zl", "зл");

                int cnt = filter.Filter.Length;
                if (cnt > 0)
                {
                    int idx = 0;
                    foreach (var f in filter.Filter)
                    {
                        query.SetParameter($"st{idx}", f.Identifier);
                        idx++;
                    }
                }

                var res = query.List<LegalEntity>();

                return res;
            }
        }

#region Работа с СИ - Перечисления && правопреемники

        public List<ReqTransfer> GetTransfersForSIRegisterHib(long registerID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var lists = session.CreateCriteria(typeof(ReqTransferHib), "t")
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)))
                    .CreateCriteria("TransferList", "l", JoinType.InnerJoin)
                    .SetFetchMode("TransferList", FetchMode.Join);

                lists.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                    .SetFetchMode("TransferRegister", FetchMode.Join)
                    .Add(Restrictions.Eq("ID", registerID));

                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join);

                return lists.List<ReqTransfer>().ToList();
            }
        }

        public List<ReqTransfer> GetTransfersForSIRegistersHib(List<long> registerIDs)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var lists = session.CreateCriteria(typeof(ReqTransferHib), "t")
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)))
                    .CreateCriteria("TransferList", "l", JoinType.InnerJoin)
                    .SetFetchMode("TransferList", FetchMode.Join);

                lists.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                    .SetFetchMode("TransferRegister", FetchMode.Join)
                    .Add(Restrictions.In("ID", registerIDs));

                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join);

                return lists.List<ReqTransfer>().ToList();
            }
        }



        public List<ZLMovementListItem> GetZLMovementsListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateQuery(@"
						select t, l.FormalizedName, c.ContractNumber, o.Name, d.Name
						from SITransfer t
							left join t.Contract c
							left join c.LegalEntity l
							left join t.TransferRegister s							
							left join s.Direction d
							left join s.Operation o
                            where t.StatusID <> -1				
					").List<object[]>();


                //Dictionary<long, List<ReqTransfer>> reqTransfersGroupped = GetListByPropertyCondition(typeof(ReqTransfer).FullName, "TransferListID", null, "neq").Cast<ReqTransfer>().GroupBy(item => item.TransferListID != null ? item.TransferListID.Value : 0).ToDictionary(item => item.Key, item => item.ToList());
                var pL = new List<ListPropertyCondition>();
                pL.Add(ListPropertyCondition.IsNotNull("TransferListID"));
                pL.Add(ListPropertyCondition.NotEqual("StatusID", (long)-1));
                var reqTransferList = GetListByPropertyConditions<ReqTransfer>(pL);

                Dictionary<long, List<ReqTransfer>> reqTransfersGroupped =
                    reqTransferList.Cast<ReqTransfer>()
                        .GroupBy(p => p.TransferListID != null ? p.TransferListID.Value : 0)
                        .ToDictionary(item => item.Key, item => item.ToList());

                List<ZLMovementListItem> list = new List<ZLMovementListItem>();
                foreach (var item in crit.ToList())
                {
                    var t = item[0] as SITransferHib;
                    List<ReqTransfer> transfers = t != null && reqTransfersGroupped.ContainsKey(t.ID)
                        ? reqTransfersGroupped[t.ID]
                        : new List<ReqTransfer>();

                    var fname = item[1] as string;
                    var cn = item[2] as string;
                    var oname = item[3] as string;
                    var dname = item[4] as string;

                    var regs = t == null ? null : transfers.Cast<ReqTransferHib>().ToList();
                    ReqTransferHib max = regs == null || regs.Count == 0
                        ? null
                        : regs.OrderBy(r => r.TransferActDate).ThenBy(r => r.ID).LastOrDefault();
                    var dt = max == null ? null : max.TransferActDate;
                    if (dt == null) continue;
                    var mov = new ZLMovementListItem
                    {
                        ID = t == null ? 0 : t.ID,
                        ZLCountBeforeOpeartion =
                            t == null || !t.InsuredPersonsCountBeforeOperation.HasValue
                                ? 0
                                : t.InsuredPersonsCountBeforeOperation.Value,
                        UKFormalizedName = fname,
                        ContractNumber = cn,
                        OperationDate = dt,
                        Operation = oname,
                        Direction = dname
                    };

                    long zl = t == null || !t.InsuredPersonsCount.HasValue ? 0 : t.InsuredPersonsCount.Value;

                    if (TransferOperationIdentifier.IsAPSIOpertion(mov.Operation))
                    {
                        mov.ZLCountFromUKtoPFR = 0;
                        mov.ZLCountFromPFRToUK = 0;
                        mov.ZLCountToAssignPayments = zl;
                    }
                    else if (TransferDirectionIdentifier.IsFromUKToPFR(mov.Direction))
                    {
                        mov.ZLCountFromUKtoPFR = zl;
                        mov.ZLCountFromPFRToUK = 0;
                    }
                    else
                    {
                        mov.ZLCountFromUKtoPFR = 0;
                        mov.ZLCountFromPFRToUK = zl;
                    }

                    list.Add(mov);
                }
                return list;
            }
        }

        public List<ZLMovementListItem> GetZLMovementsListByContractTypeHib(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateQuery(@"
						select t, l.FormalizedName, c.ContractNumber, o.Name, d.Name, rt
						from
                            ReqTransfer rt     
                            join rt.TransferList t
							left join t.Contract c
							left join c.LegalEntity l
							left join t.TransferRegister s							
							left join s.Direction d
							left join s.Operation o	
                            WHERE c.TypeID=:type
                            and rt.StatusID <> -1
                            and rt.ZLMoveCount is not null
                        ").SetParameter("type", contractType).List<object[]>();


                //Dictionary<long, List<ReqTransfer>> reqTransfersGroupped = GetListByPropertyCondition(typeof(ReqTransfer).FullName, "TransferListID", null, "neq").Cast<ReqTransfer>().GroupBy(item => item.TransferListID != null ? item.TransferListID.Value : 0).ToDictionary(item => item.Key, item => item.ToList());
                var pL = new List<ListPropertyCondition>();
                pL.Add(ListPropertyCondition.IsNotNull("TransferListID"));
                pL.Add(ListPropertyCondition.NotEqual("StatusID", (long)-1));
                var reqTransferList = GetListByPropertyConditions<ReqTransfer>(pL);

                Dictionary<long, List<ReqTransfer>> reqTransfersGroupped =
                    reqTransferList.Cast<ReqTransfer>()
                        .GroupBy(p => p.TransferListID != null ? p.TransferListID.Value : 0)
                        .ToDictionary(item => item.Key, item => item.ToList());

                List<ZLMovementListItem> list = new List<ZLMovementListItem>();
                foreach (var item in crit.ToList())
                {
                    var t = item[0] as SITransferHib;
                    var rt = item[5] as ReqTransferHib;
                    List<ReqTransfer> transfers = t != null && reqTransfersGroupped.ContainsKey(t.ID)
                        ? reqTransfersGroupped[t.ID]
                        : new List<ReqTransfer>();

                    var fname = item[1] as string;
                    var cn = item[2] as string;
                    var oname = item[3] as string;
                    var dname = item[4] as string;

                    var dt = rt == null ? null : rt.TransferActDate;
                    if (dt == null) continue;
                    var mov = new ZLMovementListItem
                    {
                        ID = t == null ? 0 : t.ID,
                        ZLCountBeforeOpeartion = (rt == null) ? 0 : (rt.ZLStartCount ?? 0),
                        UKFormalizedName = fname,
                        ContractNumber = cn,
                        OperationDate = dt,
                        Operation = oname,
                        Direction = dname
                    };

                    long zl = (rt == null) ? 0 : (rt.ZLMoveCount ?? 0);

                    if (TransferOperationIdentifier.IsAPSIOpertion(mov.Operation))
                    {
                        mov.ZLCountFromUKtoPFR = 0;
                        mov.ZLCountFromPFRToUK = 0;
                        mov.ZLCountToAssignPayments = zl;
                    }
                    else if (TransferDirectionIdentifier.IsFromUKToPFR(mov.Direction))
                    {
                        mov.ZLCountFromUKtoPFR = zl;
                        mov.ZLCountFromPFRToUK = 0;
                    }
                    else
                    {
                        mov.ZLCountFromUKtoPFR = 0;
                        mov.ZLCountFromPFRToUK = zl;
                    }

                    list.Add(mov);
                }
                return list;
            }
        }


        public long IsEnoughZLForTransferWizardOnDate(Dictionary<long, int> contractData, DateTime date)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                foreach (var item in contractData)
                {
                    IList<ReqTransferHib> l = session.CreateQuery(@"
						select rt
						from ReqTransfer rt 
                            join rt.TransferList t
							join t.TransferRegister s
							join s.Direction d
							join s.Operation o							
						where 
							t.ContractID = :contractID
                            AND t.StatusID <> -1 
							AND rt.TransferActDate < :date
						ORDER BY rt.TransferActDate DESC, rt.ID DESC
						")
                        .SetParameter("date", date)
                        .SetParameter("contractID", item.Key)
                        .SetMaxResults(1)
                        .List<ReqTransferHib>();
                    long? result;
                    var lastOperation = l.FirstOrDefault();
                    if (lastOperation == null) result = 0;
                    else
                    {
                        bool? isFromUktoPFR = lastOperation.TransferList.TransferRegister.Direction.isFromUKToPFR;
                        result = (isFromUktoPFR.HasValue && isFromUktoPFR.Value
                            ? lastOperation.ZLStartCount - lastOperation.ZLMoveCount
                            : lastOperation.ZLStartCount + lastOperation.ZLMoveCount) ?? 0;
                    }
                    if (result < item.Value) return (long)result;
                }
                return -1;
            }
        }


        /// <summary>
        /// Возвращает последний ReqTransfer до указанной  даты. Поиск осуществляеться 
        /// по максимальной дате акта передачи перевода.
        /// </summary>
        public long GetZLCountForDate(long contractID, DateTime date)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IList<ReqTransferHib> l = session.CreateQuery(@"
						select rt
						from ReqTransfer rt 
                            join rt.TransferList t
							join t.TransferRegister s
							join s.Direction d
							join s.Operation o							
						where 
							t.ContractID = :contractID
                            AND t.StatusID <> -1 
							AND rt.TransferActDate < :date
						ORDER BY rt.TransferActDate DESC, rt.ID DESC
						")
                    .SetParameter("date", date)
                    .SetParameter("contractID", contractID)
                    .SetMaxResults(1)
                    .List<ReqTransferHib>();

                var lastOperation = l.FirstOrDefault();
                if (lastOperation == null) return 0;
                bool? isFromUktoPFR = lastOperation.TransferList.TransferRegister.Direction.isFromUKToPFR;
                var result = isFromUktoPFR.HasValue && isFromUktoPFR.Value
                    ? lastOperation.ZLStartCount - lastOperation.ZLMoveCount
                    : lastOperation.ZLStartCount + lastOperation.ZLMoveCount;
                return result ?? 0;
            }
        }

        public decimal GetSCHAForContractPeriod(long contrID, DateTime start, DateTime end)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return
                    session.CreateSQLQuery(
                        @"select sum(A090) / count(A090) scha from pfr_basic.EDO_ODKF015
                            where ID_CONTRACT = :contr and REPORT_ON_TIME BETWEEN :start and :end")
                        .SetParameter("start", start)
                        .SetParameter("end", end)
                        .SetParameter("contr", contrID)
                        .UniqueResult<decimal>();
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetSIContractsForPeriod(DateTime start, DateTime end)
        {
            
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add(Restrictions.Ge("Status", 0) &&
                         Restrictions.Eq("TypeID", 1));
                        // ((Restrictions.IsNull("ContractEndDate") || Restrictions.Gt("ContractEndDate", end) || Restrictions.Gt("ContractSupAgreementEndDate", end)) && Restrictions.Lt("ContractDate", start)));
                crit.Add(Restrictions.Or(Restrictions.IsNull("DissolutionDate"), Restrictions.Or(Restrictions.Eq("DissolutionDate", DateTime.MinValue), Restrictions.Ge("DissolutionDate", end))));
                return new List<PFR_INVEST.DataObjects.Contract>(crit.List<PFR_INVEST.DataObjects.Contract>());
            }
        
            /*
            using (var session = DataAccessSystem.OpenSession())
            {


                return
                    session.CreateQuery(
                        @"SELECT new YearPlanListItem(tr.ID, le.FormalizedName, c.ContractNumber, (sum(coalesce(req.Sum,0)) + sum(coalesce(req2.Sum,0))),  (sum(coalesce(req.ZLMoveCount,0)) + sum(coalesce(req2.ZLMoveCount,0))), (sum(coalesce(req.InvestmentIncome,0)) + sum(coalesce(req2.InvestmentIncome,0))))
                                                                     FROM SITransferHib tr 
                                                                     JOIN tr.Contract c 
                                                                     JOIN c.LegalEntity le
                                                                     JOIN tr.UKPlans plans
                                                                     LEFT JOIN plans.ReqTransfer req
                                                                     LEFT JOIN plans.ReqTransfer2 req2
                                                                     WHERE tr.TransferRegisterID = :regID
                                                                     AND tr.StatusID <> -1
                                                                     GROUP BY tr.ID, le.FormalizedName, le.ID, c.ContractNumber, tr.InsuredPersonsCount
                                                                     ORDER BY le.FormalizedName")
                        .SetParameter("regID", p_regID)
                        .List<YearPlanListItem>()
                        .ToList();
            }
            */
        }

        public List<YearPlanListItem> GetUKPaymentPlansForYearPlan(long pRegID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {


                return
                    session.CreateQuery(
                        @"SELECT new YearPlanListItem(tr.ID, le.FormalizedName, c.ContractNumber, (sum(coalesce(req.Sum,0)) + sum(coalesce(req2.Sum,0))),  (sum(coalesce(req.ZLMoveCount,0)) + sum(coalesce(req2.ZLMoveCount,0))), (sum(coalesce(req.InvestmentIncome,0)) + sum(coalesce(req2.InvestmentIncome,0))))
                                                                     FROM SITransferHib tr 
                                                                     JOIN tr.Contract c 
                                                                     JOIN c.LegalEntity le
                                                                     JOIN tr.UKPlans plans
                                                                     LEFT JOIN plans.ReqTransfer req
                                                                     LEFT JOIN plans.ReqTransfer2 req2
                                                                     WHERE tr.TransferRegisterID = :regID
                                                                     AND tr.StatusID <> -1
                                                                     GROUP BY tr.ID, le.FormalizedName, le.ID, c.ContractNumber, tr.InsuredPersonsCount
                                                                     ORDER BY le.FormalizedName")
                        .SetParameter("regID", pRegID)
                        .List<YearPlanListItem>()
                        .ToList();
            }
        }

        /// <summary>
        /// Выбирает список УК, основываясь на том есть ли доступные для создания годового плана контракты у УК
        /// </summary>
        /// <param name="pRegID">Идентификатор корневого реестра (годового плана)</param>
        /// <param name="contractType">Тип контракта</param>
        /// <param name="isNew">Вызов из режима Create?</param>
        public List<LegalEntity> GetAvailableUkListForYearPlan(long pRegID, int contractType, bool isNew)
        {
            return SessionWrapper(session =>
            {
                var list = session.CreateQuery(
                    @"select le FROM LegalEntity le
                  inner join le.Contragent ca
                  where (((select count(c.ID) from Contract c where c.LegalEntityID = le.ID and c.Status <> -1 and c.TypeID = :contractType)
                        <>
                        (select count(c.ID) from Contract c 
                         inner join c.TransferLists tr
                         where c.LegalEntityID = le.ID and tr.StatusID <> -1 and tr.TransferRegisterID = :regID and c.TypeID = :contractType and c.Status <> -1 ))
                        )
                        and ca.StatusID <> -1
                  ORDER BY le.FormalizedName
                  ")
                    .SetParameter("regID", pRegID)
                    .SetParameter("contractType", contractType)
                    .List<LegalEntity>()
                    .ToList();
                if (!isNew)
                    list.Insert(0, session.Get<LegalEntity>(pRegID));
                return list;
            }
                );
        }


        /* public List<LongLongTuple> GetUKPaymentContractIdsForYearPlan(long p_regID)
		 {
			 return SessionWrapper(session => session.CreateQuery(
				 @"SELECT le.ID, c.ID
				   FROM SITransferHib tr 
				   JOIN tr.Contract c 
				   JOIN c.LegalEntity le
				   JOIN tr.UKPlans plans
				   LEFT JOIN plans.ReqTransfer req
				   LEFT JOIN plans.ReqTransfer2 req2
				   WHERE tr.TransferRegisterID = :regID
				   AND tr.StatusID <> -1
				   GROUP BY tr.ID, le.FormalizedName, le.ID, c.ContractNumber, tr.InsuredPersonsCount, le.ID, c.ID
				   ORDER BY le.FormalizedName")
				 .SetParameter("regID", p_regID)
				 .List<LongLongTuple>()
				 .ToList());
		 }*/


        public List<UKPaymentListItem> GetUKPaymentsForPlan(long pPlanID)
        {
            return GetUKPaymentsForPlan(pPlanID, null);
        }

        private List<UKPaymentListItem> GetUKPaymentsForPlan(long pPlanID, ISession session)
        {
            bool keepSession = session != null;
            try
            {
                session = session ?? DataAccessSystem.OpenSession();
                var plans = session.CreateCriteria(typeof(SIUKPlanHib), "l")
                    .Add(Restrictions.Eq("TransferListID", pPlanID))
                    .Add(Restrictions.Not(Restrictions.Eq("l.StatusID", (long)-1)));

                plans.CreateCriteria("ReqTransfer", "tr", JoinType.LeftOuterJoin)
                    .SetFetchMode("ReqTransfer", FetchMode.Join);

                plans.CreateCriteria("ReqTransfer2", "tr2", JoinType.LeftOuterJoin)
                    .SetFetchMode("ReqTransfer2", FetchMode.Join);

                plans.CreateCriteria("Month", "m", JoinType.InnerJoin)
                    .SetFetchMode("Month", FetchMode.Join);

                var list = plans.List<SIUKPlanHib>();
                var res = list.ToList().ConvertAll(plan =>
                {
                    var item = new UKPaymentListItem
                    {
                        ID = plan.ID,
                        Month = plan.Month.Name,
                        MonthID = plan.MonthID,
                        TransferListID = plan.TransferListID,
                        PlanSum = plan.PlanSum.GetValueOrDefault(),
                        FactSum = plan.FactSum.GetValueOrDefault()
                    };
                    item.Diff = item.PlanSum - item.FactSum;
                    item.ReqTransferID = plan.ReqTransfer == null ? 0 : plan.ReqTransfer.ID;
                    item.ReqTransfer2ID = plan.ReqTransfer2 == null ? 0 : plan.ReqTransfer2.ID;

                    var rq = plan.ReqTransfer;// ?? plan.ReqTransfer2;
                    if (rq != null)
                    {
                        item.InvestDohod = rq.InvestmentIncome.GetValueOrDefault();
                        item.ZLCount = rq.ZLMoveCount ?? 0;
                        item.Status = rq.TransferStatus;
                        item.ActNum = rq.TransferActNumber;
                        item.ActDate = rq.TransferActDate;
                    }

                    rq = plan.ReqTransfer2;
                    if (rq != null)
                    {
                        item.InvestDohod += rq.InvestmentIncome.GetValueOrDefault();
                        item.ZLCount += rq.ZLMoveCount ?? 0;
                        //item.Status = rq.TransferStatus;
                        //item.ActNum = rq.TransferActNumber;
                        //item.ActDate = rq.TransferActDate;
                    }

                    return item;
                });
                return res;
            }
            finally
            {
                if (session != null && !keepSession)
                    session.Close();
            }
        }

        public List<LegalEntity> GetUKListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                // 2169 
                // Корреспонденция. Удаленные УК стираются из уже заведенных карточек документов
                // Если УК(ГУК,СД) удален из перечня СИ, это не значит, что УК удаляется и из ранее созданных документов с этими УК(ГУК, СД)    
                //              var list = session.CreateQuery("FROM LegalEntityHib le WHERE (le.Contragent.TypeName = :type1 OR le.Contragent.TypeName = :type2) AND le.Contragent.StatusID <> -1 AND le.Contragent.StatusID <> 5")
                var list =
                    session.CreateQuery(
                        "FROM LegalEntityHib le WHERE (le.Contragent.TypeName = :type1 OR le.Contragent.TypeName = :type2) AND le.Contragent.StatusID <> 5")
                        .SetParameter("type1", "УК")
                        .SetParameter("type2", "ГУК")
                        .List<LegalEntity>();
                return new List<LegalEntity>(list);
            }
        }

   /*     public List<LegalEntity> GetUKListByContractTypeHib(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                // 2169 
                // Корреспонденция. Удаленные УК стираются из уже заведенных карточек документов
                // Если УК(ГУК,СД) удален из перечня СИ, это не значит, что УК удаляется и из ранее созданных документов с этими УК(ГУК, СД)    
                //              var list = session.CreateQuery("FROM LegalEntityHib le WHERE (le.Contragent.TypeName = :type1 OR le.Contragent.TypeName = :type2) AND le.Contragent.StatusID <> -1 AND le.Contragent.StatusID <> 5")
                var list =
                    session.CreateQuery("FROM LegalEntityHib le WHERE (le.Contragent.TypeName = :type1 OR le.Contragent.TypeName = :type2) AND le.Contragent.StatusID <> 5"
                                        + " AND (select count(ID) from le.Contracts c where c.Status > 0 and c.TypeID=:contractType )>0")
                        .SetParameter("type1", "УК")
                        .SetParameter("type2", "ГУК")
                        .SetParameter("contractType", contractType)
                        .List<LegalEntity>();
                return new List<LegalEntity>(list);
            }
        }*/

        public List<LegalEntity> GetActiveUKListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        @"FROM LegalEntityHib le 
                            WHERE (le.Contragent.TypeName = :type1 OR le.Contragent.TypeName = :type2) AND (le.Contragent.StatusID = 1 OR le.Contragent.StatusID = 2 AND (le.CloseDate IS NULL OR le.CloseDate > :today)) 
                            ORDER BY le.Contragent.Name")
                        .SetParameter("type1", "УК")
                        .SetParameter("type2", "ГУК")
                        .SetParameter("today", DateTime.Today)
                        .List<LegalEntity>();
                return new List<LegalEntity>(list);
            }
        }



        public List<LegalEntity> GetActiveUKListByTypeContractHibForRegister(int contractType, long registerId, long? leId = null, long? contractId = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //TODO возможно нужно также допускать le с закрытой датой, если leId != null
                var list = session.CreateQuery(string.Format(@"
                    FROM LegalEntityHib le 
                    WHERE (le.Contragent.TypeName = :type1 OR le.Contragent.TypeName = :type2) AND 
                    (le.Contragent.StatusID = 1 OR le.Contragent.StatusID = 2 {1} ) AND 
                    (le.CloseDate IS NULL OR le.CloseDate > :today) AND 
                    (select count(ID) from le.Contracts c where (c.Status >= 0 {0}) and c.TypeID=:contractType and (c.ID not in (select ContractID from SITransfer tr where tr.TransferRegisterID = :regId and tr.StatusID <> -1) {0}) )>0
                    ORDER BY le.Contragent.Name", contractId.HasValue ? " OR c.ID = " + contractId.Value : "", leId.HasValue ? "OR le.ID =" + leId.Value : ""))
                    .SetParameter("type1", "УК")
                    .SetParameter("type2", "ГУК")
                    .SetParameter("today", DateTime.Today)
                    .SetParameter("contractType", contractType)
                    .SetParameter("regId", registerId)
                    .List<LegalEntity>();

                return new List<LegalEntity>(list);
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetAvailableContractsForUkTransfer(int contractType, long registerId, long leId, long? contractId = null)
        {
            return SessionWrapper(session => session.CreateQuery(
                $@"FROM Contract c WHERE c.Status >= 0 and c.TypeID = :cType and c.LegalEntityID = :leId and (c.ID not in (select ContractID from SITransfer tr where tr.TransferRegisterID = :regId and tr.StatusID <> -1) {(contractId
                    .HasValue
                    ? " OR c.ID = " + contractId.Value
                    : "")})")
                .SetParameter("cType", contractType)
                .SetParameter("leId", leId)
                .SetParameter("regId", registerId)
                .List<PFR_INVEST.DataObjects.Contract>().ToList());
        }


        public List<LegalEntity> GetValidUKListByMcProfitAbilityHib(int contractType, long yearId, long quarter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                int year = 2000 + (int)yearId;
                int month = (int)quarter * 3 - 2;
                var dt = new DateTime(year, month, 1).AddDays(-1);


                var id = session.CreateQuery(@"SELECT ContractId 
                    FROM McProfitAbility   
                    WHERE YearId = :yearId AND Quarter = :quarter GROUP BY ContractId ORDER BY ContractId ")
                    .SetParameter("yearId", yearId)
                    .SetParameter("quarter", quarter).List<long>().ToList();

                if (id.Any())
                {

                    return session.CreateQuery(@"SELECT DISTINCT le FROM LegalEntity le 
    JOIN le.Contragent ca 
    JOIN le.Contracts c 
    WHERE   (ca.TypeName='ГУК' OR ca.TypeName='УК') AND
            ( 
                ( le.CloseDate IS NULL AND (ca.StatusID != 6 AND ca.StatusID > 0 ) ) OR 
                 le.CloseDate > :closeDate 
            ) AND 
            c.TypeID= :typeContract AND c.Status >= 0 AND c.ID NOT IN (:id)  ")
                        .SetParameter("closeDate", dt)
                        .SetParameter("typeContract", contractType)
                        .SetParameterList("id", id)
                        .List<LegalEntity>()
                        .ToList();
                }
                return session.CreateQuery(@"SELECT DISTINCT le FROM LegalEntity le 
    JOIN le.Contragent ca 
    JOIN le.Contracts c 
    WHERE   (ca.TypeName='ГУК' OR ca.TypeName='УК') AND
            ( 
                ( le.CloseDate IS NULL AND (ca.StatusID != 6 AND ca.StatusID > 0 ) ) OR 
                 le.CloseDate > :closeDate 
            ) AND 
            c.TypeID= :typeContract AND c.Status >= 0  ")
                    .SetParameter("closeDate", dt)
                    .SetParameter("typeContract", contractType)
                    .List<LegalEntity>()
                    .ToList();
                //var legalEntityList = session.CreateCriteria(typeof(LegalEntity), "le")

                //.CreateCriteria("Contragent", "ca", NHibernate.SqlCommand.JoinType.InnerJoin)
                //.SetFetchMode("Contragent", FetchMode.Lazy)
                //.Add(Restrictions.Or(Restrictions.Eq("ca.TypeName", "ГУК"), Restrictions.Eq("ca.TypeName", "УК")))
                //.Add(Restrictions.Or(
                //Restrictions.And(Restrictions.IsNull("le.CloseDate"),Restrictions.And(Restrictions.Ge("ca.StatusID", 0L),Restrictions.Not(Restrictions.Eq("ca.StatusID",6L)))),
                //Restrictions.Gt("le.CloseDate", dt))
                //)

                //.CreateCriteria("le.Contracts", "c", NHibernate.SqlCommand.JoinType.InnerJoin)
                //.SetFetchMode("le.Contracts", FetchMode.Lazy)
                //.Add(Restrictions.And(Restrictions.Eq("c.TypeID", contractType), Restrictions.Eq("c.Status", 0)))
                //.Add(Restrictions.Not(Restrictions.In("c.ID", id)))
                //.SetResultTransformer(CriteriaSpecification.DistinctRootEntity)
                //.List<LegalEntity>().ToList();
                //return legalEntityList;

            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetValidContractListByMcProfitAbilityHib(int contractType, long legalEntityId, long yearId,
            long quarter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                var id = session.CreateQuery(@"SELECT ContractId 
                    FROM McProfitAbility   
                    WHERE YearId = :yearId AND Quarter = :quarter GROUP BY ContractId ORDER BY ContractId ")
                    .SetParameter("yearId", yearId)
                    .SetParameter("quarter", quarter).List<long>().ToList();
                if (id != null && id.Any())
                {
                    return
                        session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                            .Add(
                                Restrictions.And(
                                    Restrictions.Eq("c.TypeID", contractType), Restrictions.Eq("c.Status", 0)))
                            .Add(Restrictions.Not(Restrictions.In("c.ID", id)))
                            .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                            .Add(Restrictions.Eq("le.ID", legalEntityId))
                            .List<PFR_INVEST.DataObjects.Contract>()
                            .ToList();
                }
                return
                    session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                        .Add(
                            Restrictions.And(
                                Restrictions.Eq("c.TypeID", contractType), Restrictions.Eq("c.Status", 0)))

                        .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                        .Add(Restrictions.Eq("le.ID", legalEntityId))
                        .List<PFR_INVEST.DataObjects.Contract>()
                        .ToList();
            }
        }




        public List<LegalEntity> GetActiveUKListTransferHib(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery($@"SELECT le 
                    FROM Contract c  
                    JOIN c.LegalEntity le
                    WHERE (le.Contragent.TypeName = :type1 OR le.Contragent.TypeName = :type2) AND 
(le.Contragent.StatusID = 1 OR le.Contragent.StatusID = 2 AND
(le.CloseDate IS NULL OR le.CloseDate > :today)) AND
c.TypeID={contractType} AND c.Status <> -1
ORDER BY le.Contragent.Name")
                    .SetParameter("type1", "УК")
                    .SetParameter("type2", "ГУК")
                    .SetParameter("today", DateTime.Today)
                    .List<LegalEntity>().Distinct();
                return new List<LegalEntity>(list);
            }
        }

        public List<LegalEntity> GetActiveNpfListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        "FROM LegalEntityHib le WHERE (le.Contragent.TypeName = :type1) AND (le.Contragent.StatusID = 1 OR le.Contragent.StatusID = 2 AND (le.CloseDate IS NULL OR le.CloseDate > :today)) ORDER BY le.Contragent.Name")
                        .SetParameter("type1", "НПФ")
                        .SetParameter("today", DateTime.Today)
                        .List<LegalEntity>();
                return new List<LegalEntity>(list);
            }
        }

        public List<IncomeSecurityListItem> GetIncomeSecurityList(long? recordID = null, string typeFilter = null, bool withPercents = true)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(IncomesecHib), "IncomesecHib")
                    .AddOrder(Order.Asc("OrderDate"));
                if (recordID.HasValue)
                    crit.Add(Restrictions.Eq("ID", recordID));
                if (!string.IsNullOrEmpty(typeFilter))
                    crit.Add(Restrictions.Eq("IncomeKind", typeFilter));
                crit.CreateCriteria("Portfolio", "Portfolio", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join);
                crit.CreateCriteria("PfrBankAccount", JoinType.InnerJoin)
                    .CreateCriteria("Currency", "Currency", JoinType.LeftOuterJoin)
                    .SetFetchMode("Currency", FetchMode.Join);
                crit.CreateCriteria("Curs", "Curs", JoinType.LeftOuterJoin)
                    .SetFetchMode("Curs", FetchMode.Join);

                var list = crit.List<IncomesecHib>().ToList();

                var res = list.ConvertAll(delegate (IncomesecHib incomesec)
                {
                    decimal total;
                    //string currency;
                    string currency = incomesec.PfrBankAccount.Currency.Name;
                    if (incomesec.Curs == null)
                    {
                        total = incomesec.OrderSum ?? 0;
                        //currency = CurrencyIdentifier.RUBLES;
                    }
                    else
                    {
                        total = (incomesec.OrderSum ?? 0) * (incomesec.Curs.Value ?? 1);
                        //if (incomesec.Curs.Currency != null)
                        //    currency = incomesec.Curs.Currency.Name;
                        //else
                        //    currency = "Н/Д";
                    }
                    return new IncomeSecurityListItem
                    {
                        Comment = incomesec.Comment,
                        ID = incomesec.ID,
                        IncomeKind = incomesec.IncomeKind,
                        Month = DateTools.GetMonthInWordsForDate(incomesec.OrderDate),
                        OrderDate = incomesec.OrderDate,
                        OrderNumber = incomesec.OrderNumber,
                        Portfolio = incomesec.Portfolio.Year,
                        Quarter = DateTools.GetQarterYearInWordsForDate(incomesec.OrderDate),
                        //для рублевых - сумма в рублях - рубли, для валютных - по курсу
                        Total = Math.Round(total, 2, MidpointRounding.AwayFromZero),
                        Year = DateTools.GetYearInWordsForDate(incomesec.OrderDate),
                        //для рублевых - сумма в валюте - null, для валютных - их сумма валюта
                        Sum = CurrencyIdentifier.IsRUB(currency) ? (decimal?)null : incomesec.OrderSum,
                        Currency = currency
                    };
                });

                //Проценты по депозиту
                if (withPercents)
                {
                    var query = session.CreateQuery(@"
                        select 
                        ph.ID,
                        ph.Date,
                        ph.OrderNumber,
                        ph.Comment,
                        p.Year,
                        ph.Sum,
                        ph.OrderDate
                        
                        from 
                        PaymentHistory ph
                        join ph.OfferPfrBankAccountSum s
                        join s.Portfolio p
                        where ph.ForPercent = 1
                        and s.StatusID>0
                        ");



                    foreach (object[] p in query.List<object[]>())
                    {
                        var date = (DateTime)IsDbNull(p[6], p[1]);
                        //Дата ордера. Если там пусто, берем дату операции

                        res.Add(new IncomeSecurityListItem
                        {
                            ID = 0,
                            PaymentHistoryID = (long)p[0],

                            IncomeKind = IncomeSecIdentifier.s_DepositsPercents,
                            OrderNumber = (string)p[2],
                            Comment = (string)p[3],
                            Portfolio = (string)p[4],

                            OrderDate = date,
                            Month = DateTools.GetMonthInWordsForDate(date),
                            Quarter = DateTools.GetQarterYearInWordsForDate(date),
                            Year = DateTools.GetYearInWordsForDate(date),

                            Sum = (decimal)p[5],
                            //для рублевых - сумма в рублях - рубли, для валютных - по курсу
                            Total = (decimal)p[5], //Same as Sum

                            Currency = CurrencyIdentifier.RUBLES
                        });
                    }
                }

                return res;
            }
        }

        private object IsDbNull(object value, object defaultValue)
        {
            if (value == null || value == DBNull.Value)
                return defaultValue;

            return value;
        }

        private const long m_APSIOperationID = 8;

        public SPNOperation GetAPSIOperationHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.Get(typeof(SPNOperation), m_APSIOperationID) as SPNOperation;
            }
            ;
        }

        private const long m_UKtoPFRDirectionID = 1;

        public List<SPNOperation> GetSPNOperationsUKtoPFRListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var dir = session.Get(typeof(SPNDirectionHib), m_UKtoPFRDirectionID) as SPNDirectionHib;
                return new List<SPNOperation>(dir.Operations.Cast<SPNOperation>());
            }
            ;
        }

        private const long m_PFRtoUKDirectionID = 2;

        public List<SPNOperation> GetSPNOperationsPFRtoUKListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var dir = session.Get(typeof(SPNDirectionHib), m_PFRtoUKDirectionID) as SPNDirectionHib;
                return new List<SPNOperation>(dir.Operations.Cast<SPNOperation>());
            }
            ;
        }

        public List<SPNOperation> GetSPNOperationsByTypeListHib(long directionID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var dir = session.Get(typeof(SPNDirectionHib), directionID) as SPNDirectionHib;
                return new List<SPNOperation>(dir.Operations.Cast<SPNOperation>());
            }
            ;
        }



#endregion

#region Print Plans

        public bool IsValidYearForPrintMonthPlan(Document.Types contractType, long pYearID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"
					select count(y.ID)
					from SIUKPlan p
					join p.TransferList l
					join l.TransferRegister r
					join r.CompanyYear y
                    join l.Contract c
					where y.ID=:yID
                    and c.TypeID = {(int) contractType}
                    and p.StatusID <> -1
                    ")
                    .SetParameter("yID", pYearID);

                long count = query.UniqueResult<long>();
                return count > 0;
            }
        }

        public List<SITransfer> GetUKPlansForYearAndOperation(Document.Types contractType, long pYearID,
            long operationId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var lists =
                    session.CreateCriteria(typeof(SITransferHib), "l")
                        .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)));

                if (pYearID > 0)
                {

                    lists.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                        .SetFetchMode("TransferRegister", FetchMode.Join)
                        .Add(Restrictions.Eq("OperationID", operationId))
                        .CreateCriteria("CompanyYear", "y", JoinType.InnerJoin)
                        .SetFetchMode("CompanyYear", FetchMode.Join)
                        .Add(Restrictions.Eq("y.ID", pYearID));
                }
                else
                {
                    lists.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                        .SetFetchMode("TransferRegister", FetchMode.Join)
                        .Add(Restrictions.Eq("OperationID", operationId))
                        .CreateCriteria("CompanyYear", "y", JoinType.InnerJoin)
                        .SetFetchMode("CompanyYear", FetchMode.Join);
                }
                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .Add(Restrictions.Eq("TypeID", (int)contractType))
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .AddOrder(Order.Asc("FormalizedName"));

                var list = lists.List<SITransfer>();

                return new List<SITransfer>(list);
            }
        }

        public List<SITransfer> GetUKPlansForYearAndOperationType(Document.Types contractType, long pYearID, long operationTypeId, long ukId = 0)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var lists =
                    session.CreateCriteria(typeof(SITransferHib), "l")
                        .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)));

                lists.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                        .SetFetchMode("TransferRegister", FetchMode.Join)
                        .Add(Restrictions.Eq("OperationTypeID", operationTypeId))
                        .CreateCriteria("CompanyYear", "y", JoinType.InnerJoin)
                        .SetFetchMode("CompanyYear", FetchMode.Join);
                if (pYearID > 0)
                    lists.Add(Restrictions.Eq("y.ID", pYearID));

                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .Add(Restrictions.Eq("TypeID", (int)contractType))
                    .Add(Restrictions.Not(Restrictions.Eq("Status", -1)))
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .Add(ukId != 0 ? Restrictions.Eq("ID", ukId) : Restrictions.Not(Restrictions.Eq("ID", 0L)))
                    .AddOrder(Order.Asc("FormalizedName"));

                var cSession = session;
                var list = lists.List<SITransfer>().Where(a => GetObjectPropertyCount<SITransfer>(a.ID, "UKPlans", cSession) > 0);

                return new List<SITransfer>(list);
            }
        }

        /// <summary>
        /// Для писем о выплатах СИ/ВР
        /// </summary>
        public Dictionary<long, string> GetUKListForYearAndOperationType(Document.Types contractType, long pYearID, long operationTypeId)
        {
            return SessionWrapper(session =>
            {
                var dic = new Dictionary<long, string>();
                session.CreateQuery($@"select distinct le.ID, le.FormalizedName from SITransferHib l
                                       join l.TransferRegister r
                                       join r.CompanyYear y
                                       join l.Contract c
                                       join c.LegalEntity le
                                         where l.StatusID<>-1 and r.OperationTypeID=:typeid and c.TypeID=:contractType and c.Status<>-1 {
                            (pYearID > 0 ? " and y.ID=:year" : " and y.ID<>:year")
                        }
                                         order by le.FormalizedName
                                        ")
                    .SetParameter("typeid", operationTypeId)
                    .SetParameter("contractType", (int)contractType)
                    .SetParameter("year", pYearID > 0 ? pYearID : 0)
                    .List<object[]>()
                    .ForEach(a => dic.Add((long)a[0], (string)a[1]));

                /*var lists =
                    session.CreateCriteria(typeof(SITransferHib), "l")
                        .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)));

                lists.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                        .SetFetchMode("TransferRegister", FetchMode.Join)
                        .Add(Restrictions.Eq("OperationTypeID", operationTypeId))
                        .CreateCriteria("CompanyYear", "y", JoinType.InnerJoin)
                        .SetFetchMode("CompanyYear", FetchMode.Join);
                if (pYearID > 0)
                    lists.Add(Restrictions.Eq("y.ID", pYearID));

                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .Add(Restrictions.Eq("TypeID", (int)contractType))
                    .Add(Restrictions.Not(Restrictions.Eq("Status", -1)))
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .AddOrder(Order.Asc("FormalizedName"));
                var cSession = session;
                var list = lists.List<SITransfer>().Where(a => GetObjectPropertyCount<SITransfer>(a.ID, "UKPlans", cSession) > 0);
                */
                return dic;
            });
        }

        public List<SIRegister> GetSiRegistersByContractOperationHib(int typeContract, long? operationID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (operationID.HasValue)
                {
                    var query = session.CreateQuery($@"
					select distinct  r from  SITransfer t
                    join t.TransferRegister r 
                    join t.Contract c   
                    where 
                    c.TypeID = {typeContract} 
                    and c.Status >= 0 
                    and r.OperationID={operationID.Value}
                    AND t.StatusID <> -1
                    ");
                    return query.List<SIRegister>().ToList();
                }
                else
                {
                    var query = session.CreateQuery($@"
					select distinct  r from  SITransfer t
                    join t.TransferRegister r 
                    join t.Contract c   
                    where 
                    c.TypeID = {typeContract} 
                    and c.Status >= 0
                    AND t.StatusID <> -1");
                    return query.List<SIRegister>().ToList();
                }

            }
        }

        public List<SIRegister> GetSiRegistersByCondition(int typeContract, long directionID, long operationID,
            List<long> contractNamesIds, bool isArchive)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string queryString = $@"
					select distinct s from  SITransfer st
                join st.TransferRegister s
                join st.Contract c
				join c.LegalEntity
				join st.Transfers t
				where c.TypeID={typeContract} 
                and s.DirectionID={directionID} 
                and s.OperationID={operationID}
                AND st.StatusID <> -1
                ";

                if (contractNamesIds != null && contractNamesIds.Count > 0)
                {
                    string ids = string.Empty;
                    foreach (var id in contractNamesIds)
                    {
                        ids = $" {id},";
                    }

                    queryString += $" and c.ContractNameID in ({ids.TrimEnd(',')}) ";
                }


                if (isArchive)
                {
                    queryString += " and trim(lower(t.Status) <> 'акт подписан' OR t.Status IS NULL";
                }
                else
                {
                    queryString += " and c.Status >= 0 ";
                }

                var query = session.CreateQuery(queryString);

                return query.List<SIRegister>().ToList();

            }
        }

        public List<ExportReportVRListItem> GetDataReportVRApplication1Pfr(List<long> siregIDs)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string str = string.Empty;
                foreach (var item in siregIDs)
                {
                    str += item + ",";
                }

                string queryString = $@"
				select  le.FormalizedName, c.ContractNumber,cn.Name as ContractName, t.Sum
                from  SITransfer st
                join st.TransferRegister s
                join st.Contract c
                join c.ContractName cn
				join c.LegalEntity le
				join st.Transfers t
				where  s.ID in ({str.TrimEnd(',')})
                AND st.StatusID <> -1
                ";


                var list = session.CreateQuery(queryString).List<object[]>();
                List<ExportReportVRListItem> listResult = new List<ExportReportVRListItem>();
                foreach (var item in list)
                {
                    listResult.Add(
                        new ExportReportVRListItem
                        {
                            FormalizedName = item[0] as string,
                            ContractNumber = item[1] as string,
                            ContractName = item[2] as string,
                            Sum = item[3] as decimal?
                        }

                        );

                }


                return listResult;

            }
        }


        public List<SITransfer> GetSITransfersByRegister(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var lists =
                    session.CreateCriteria(typeof(SITransferHib), "l")
                        .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)));

                lists.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)))
                    .SetFetchMode("TransferRegister", FetchMode.Join)
                    .CreateCriteria("CompanyYear", "y", JoinType.InnerJoin)
                    .SetFetchMode("CompanyYear", FetchMode.Join)
                    .Add(Restrictions.Eq("l.TransferRegisterID", id));

                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join);

                var list = lists.List<SITransfer>();

                return new List<SITransfer>(list);
            }
        }

#endregion

#region PlanCorrectionWizard & YearPlanWizard

        public List<Year> GetValidYearListForYearPlanCreation(Document.Types contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"
					from Year year where year.ID not in
					(
						select y.ID
						from SIUKPlan p
						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
                        join l.Contract c
                        where c.TypeID = {(int) contractType}
                        and p.StatusID <> -1
					)
					order by year.Name asc");

                return query.List<Year>().ToList();
            }
        }

        /// <summary>
        /// Список лет для мастера создания годового плана, исключает года по оторым были созданы планы
        /// </summary>
        /// <param name="contractType">Тип контракта</param>
        /// <param name="operationTypeId">Тип операции</param>
        public List<Year> GetYearListForYearPlanCreation(Document.Types contractType, long operationTypeId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"
					from Year year where year.ID not in
					(
						select distinct y.ID
						from SIUKPlan p
						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
                        join l.Contract c
                        where c.TypeID = {(int) contractType}
                        and r.OperationTypeID={operationTypeId} 
                        and p.StatusID <> -1
					)
					order by year.Name asc");

                return query.List<Year>().ToList();
            }
        }


        public List<Year> GetYearListCreateSIUKPlanByOperationType(Document.Types contractType, long operationTypeId, bool includeArchive = true)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string strQuery = string.Format(@"
						select y
						from SIUKPlan p
						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
                        join l.Contract c
                        where c.TypeID = {0} 
                        {2}
                        and c.Status <> -1
                        and r.OperationTypeID={1} 
                        and p.StatusID <> -1
                        group by y.ID, y.Name  
					    order by y.Name asc", (int)contractType, operationTypeId, includeArchive ? "" : $"and y.ID >= {DateTime.Today.Year - 2000 - 5} ");
                var query = session.CreateQuery(strQuery);

                return query.List<Year>().ToList();
            }
        }

        public List<Year> GetYearListCreateSIUKPlan(Document.Types contractType, long? operationID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string strQuery = operationID.HasValue
                    ? $@"
						select y
						from SIUKPlan p
						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
                        join l.Contract c
                        where c.TypeID = {(int) contractType} 
                        and r.OperationID={operationID.Value} 
                        and p.StatusID <> -1
                        group by y.ID, y.Name  
					    order by y.Name asc"
                    : $@"
						select y
						from SIUKPlan p
						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
                        join l.Contract c
                        where c.TypeID = {(int) contractType} 
                        and p.StatusID <> -1
                        group by y.ID, y.Name  
					    order by y.Name asc";
                var query = session.CreateQuery(strQuery);

                return query.List<Year>().ToList();
            }
        }

        /*public List<Month> GetMonthListCreateSIUKPlan(Document.Types contractType, long companyYearID, long operationID)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				string strQuery = string.Format(@"
						select m
						from SIUKPlan p
						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
						join p.Month m 
						join l.Contract c
						where c.TypeID = {0} 
						and r.OperationID={1} 
						and r.CompanyYearID={2} 
						and p.TransferID is null 
						and p.StatusID <> -1
						group by m.ID, m.Name  
						order by m.ID asc", (int)contractType, operationID, companyYearID);
				var query = session.CreateQuery(strQuery);

				return query.List<Month>().ToList();
			}
		}*/

        /// <summary>
        /// Получение списка месяцов для перечисления на месяц по типу операции и содержанию операции
        /// </summary>
        /// <param name="contractType"></param>
        /// <param name="companyYearID"></param>
        /// <param name="operationTypeID"></param>
        /// <param name="operationContentID"></param>
        public List<Month> GetMonthListCreateSIUKPlanByOperationType(Document.Types contractType, long companyYearID,
            long operationTypeID, long operationContentID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //выбираем планы
                //проверяем наличие перечислений у плана, если такие имеются, добираемся до их реестров 
                //и проверяем содержание операции - оно не должно совпадать с поставляемым

                string whereCondition;
                //для выплат правопреемникам не нужно проверять 2 слота перечислений
                if (operationTypeID == (long)Element.SpecialDictionaryItems.PaymentReserve)
                    whereCondition = string.Format(@" and (p.TransferID is null or rqtrreg.OperationID <> {0})
                        and (p.Transfer2ID is null or rqtrreg2.OperationID <> {0})
                        and (p.TransferID is null or p.Transfer2ID is null) ", operationContentID);
                else whereCondition = @" and p.TransferID is null ";

                string strQuery = $@"
						select m
						from SIUKPlan p
                        
                        left join p.ReqTransfer rq
                        left join rq.TransferList rqtr
                        left join rqtr.TransferRegister rqtrreg

                        left join p.ReqTransfer2 rq2
                        left join rq2.TransferList rqtr2
                        left join rqtr2.TransferRegister rqtrreg2

						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
                        join p.Month m 
                        join l.Contract c
                        where c.TypeID = {(int) contractType} 
                        and c.Status <> -1
                        and r.OperationTypeID={operationTypeID} 
                        and r.CompanyYearID={companyYearID} 
                        and p.StatusID <> -1
                        {whereCondition}
                        group by m.ID, m.Name  
					    order by m.ID asc";
                var query = session.CreateQuery(strQuery);

                return query.List<Month>().ToList();
            }
        }

        public List<Year> GetValidYearListForYearPlanCorrection(Document.Types contractType, long operationTypeId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(string.Format(@"
					from Year year where year.ID in
					(
						select y.ID
						from SIUKPlan p
						join p.TransferList l
						join l.TransferRegister r
						join r.CompanyYear y
                        join l.Contract c
                        where c.TypeID = {0}
                        and y.ID >= {2}
                        and c.Status <> -1
                        and r.OperationTypeID = {1}
                        and p.StatusID <> -1
					)
					order by year.Name asc", (int)contractType, operationTypeId, DateTime.Today.Year - 2000 - 5));

                return
                    query.List<Year>()
                        .Where(
                            y => GetValidMonthListForYearPlanCorrection(contractType, y.ID, operationTypeId).Count > 0)
                        .ToList();
            }
        }

        public List<Month> GetValidMonthListForYearPlanCorrection(Document.Types contractType, long pYearID,
            long operationTypeId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"
					from Month month where month.ID in
					(select p.MonthID
					from SIUKPlan p
					join p.TransferList l
					join l.TransferRegister r
					join r.CompanyYear y
                    join l.Contract c
                    where c.TypeID = {(int) contractType}
                    and r.OperationTypeID = {operationTypeId}
					and (p.TransferID is null or p.Transfer2ID is null)
                    and y.ID=:yID
                    and p.StatusID <> -1)
                    ")
                    .SetParameter("yID", pYearID);

                return query.List<Month>().ToList();
            }
        }

        public List<SIUKPlan> GetSIUKPlansByReqID(long transferId)
        {
            return SessionWrapper(session =>
                session.CreateCriteria(typeof(SIUKPlanHib), "p")
                    .Add(SIUKPlanHib.GetCriterionContainsTransferId(transferId))
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", -1L)))
                    .List<SIUKPlan>()
                    .ToList(), true);
        }

        public List<SIUKPlan> GetUKPlansForYearMonthAndOperationForMonthTransferWizard(Document.Types contractType,
            long pMonthID, long pYearID, long operationTypeId, long operationId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                string where;
                //для выплат правопреемников только 1 перечисление, поэтому проверяем если оба нулл
                //для другого типа операции - более детальная првоерка
                if (operationTypeId == (long)Element.SpecialDictionaryItems.PaymentReserve)
                    where = string.Format(@"and ((rt1.ID is null or r1.OperationID <> {0}) and (rt2.ID is null or r2.OperationID <> {0}) )
                                            and (rt1.ID is null or rt2.ID is null) ", operationId);
                else where = "and (rt1.ID is null and rt2.ID is null) ";

                return session.CreateQuery($@"
                       SELECT p from SIUKPlanHib p
                       inner join p.TransferList t
                       inner join t.Contract c
                       inner join c.LegalEntity le
                       inner join t.TransferRegister r
                       inner join r.CompanyYear y
                       left outer join p.ReqTransfer rt1
                       left outer join rt1.TransferList t1
                       left outer join t1.TransferRegister r1
                       left outer join p.ReqTransfer2 rt2
                       left outer join rt2.TransferList t2
                       left outer join t2.TransferRegister r2                       
                       where p.StatusID <> -1 and p.MonthID = {pMonthID} and c.TypeID = {(int) contractType} and y.ID = {pYearID}
                       and r.OperationTypeID = {operationTypeId}
                       {where} 
                       order by le.FormalizedName")
                    .List<SIUKPlan>().ToList();
            }
        }

        public List<SIUKPlan> GetUKPlansForYearMonthAndOperation(Document.Types contractType, long pMonthID,
            long pYearID,
            long operationId, bool forPrint, bool isOperationTypeIdSupplied = false)
        {
            return GetUKPlansForYearMonthAndOperationInternal(contractType, pMonthID, pYearID, operationId, forPrint,
                isOperationTypeIdSupplied);
        }

        private List<SIUKPlan> GetUKPlansForYearMonthAndOperationInternal(Document.Types contractType, long pMonthID,
            long pYearID,
            long operationId, bool forPrint, bool isOperationTypeIdSupplied = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                return session.CreateQuery($@"
                       SELECT p from SIUKPlanHib p
                       inner join p.TransferList t
                       inner join t.Contract c
                       inner join c.LegalEntity le
                       inner join t.TransferRegister r
                       inner join r.CompanyYear y
                       {""}
                       where p.StatusID <> -1 and p.MonthID = {pMonthID} {""} and c.TypeID = {(int) contractType} {
                            (isOperationTypeIdSupplied
                                ? (operationId == 0
                                    ? " and r.OperationTypeID is null "
                                    : ("and r.OperationTypeID = " + operationId))
                                : (operationId == 0 ? " and r.OperationID is null " : ("and r.OperationID = " + operationId)))
                        }  and y.ID = {pYearID}
                       order by le.FormalizedName")
                    .List<SIUKPlan>().ToList();

                /*  var transf = session.CreateCriteria(typeof(SIUKPlanHib), "p")
					.Add(Restrictions.Not(Restrictions.Eq("p.StatusID", (long)-1)));

				if (forPrint)
					transf.CreateCriteria("ReqTransfer", "rt", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
						  .SetFetchMode("rt", FetchMode.Join);

				transf = transf.Add(Restrictions.Eq("p.MonthID", p_MonthID))
				.CreateCriteria("TransferList", "t", NHibernate.SqlCommand.JoinType.InnerJoin)
				.SetFetchMode("TransferList", FetchMode.Join);


				if (!forPrint)
				{
					if (!forMonthTr)
						transf.Add(SIUKPlanHib.GetCriterionAnyTransferIsNull());
					else
					{
			            
					}
				}

				transf.CreateCriteria("Contract", "c", NHibernate.SqlCommand.JoinType.InnerJoin)
					.SetFetchMode("Contract", FetchMode.Join)
					.Add(Restrictions.Eq("TypeID", (int)contractType))
					.CreateCriteria("LegalEntity", "le", NHibernate.SqlCommand.JoinType.InnerJoin)
					.SetFetchMode("LegalEntity", FetchMode.Join)
				.AddOrder(Order.Asc("FormalizedName"));

				transf.CreateCriteria("TransferRegister", "r", NHibernate.SqlCommand.JoinType.InnerJoin)
				.SetFetchMode("TransferRegister", FetchMode.Join)
				.Add(isOperationTypeIdSupplied ?
				(operationId == 0 ? Restrictions.IsNull("r.OperationTypeID") : Restrictions.Eq("r.OperationTypeID", operationId))
				: (operationId == 0 ? Restrictions.IsNull("r.OperationID") : Restrictions.Eq("r.OperationID", operationId)))
				.CreateCriteria("CompanyYear", "y", NHibernate.SqlCommand.JoinType.InnerJoin)
				.SetFetchMode("CompanyYear", FetchMode.Join)
				.Add(Restrictions.Eq("y.ID", p_YearID));

				var list = transf.List<SIUKPlan>();

				return new List<SIUKPlan>(list);*/
            }
        }

        /*public List<SIUKPlan> GetSIUKPlansByReqID(long id)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var transf = session.CreateCriteria(typeof(SIUKPlanHib), "p")
				.Add(Restrictions.Not(Restrictions.Eq("p.StatusID", (long)-1)))
				.Add(Restrictions.Eq("p.TransferID", id))
				.CreateCriteria("TransferList", "t", JoinType.InnerJoin)
				.SetFetchMode("TransferList", FetchMode.Join);

				transf.CreateCriteria("Contract", "c", JoinType.InnerJoin)
				.SetFetchMode("Contract", FetchMode.Join)
				.CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
				.SetFetchMode("LegalEntity", FetchMode.Join);

				transf.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
				.SetFetchMode("TransferRegister", FetchMode.Join)
				.CreateCriteria("CompanyYear", "y", JoinType.InnerJoin)
				.SetFetchMode("CompanyYear", FetchMode.Join);

				var list = transf.List<SIUKPlan>();

				return new List<SIUKPlan>(list);
			}
		}*/

        public List<SIUKPlan> GetSIUKPlansByReqListID(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var transf = session.CreateCriteria(typeof(SIUKPlanHib), "p")
                    .Add(Restrictions.Not(Restrictions.Eq("p.StatusID", (long)-1)))
                    .Add(Restrictions.Eq("p.TransferListID", id))
                    .CreateCriteria("TransferList", "t", JoinType.InnerJoin)
                    .SetFetchMode("TransferList", FetchMode.Join);

                transf.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join);

                transf.CreateCriteria("TransferRegister", "r", JoinType.InnerJoin)
                    .SetFetchMode("TransferRegister", FetchMode.Join)
                    .CreateCriteria("CompanyYear", "y", JoinType.InnerJoin)
                    .SetFetchMode("CompanyYear", FetchMode.Join);

                var list = transf.List<SIUKPlan>();

                return new List<SIUKPlan>(list);
            }
        }

#endregion

#region TransferWizard

        public List<PFR_INVEST.DataObjects.Contract> GetContractsForUKs()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add(Restrictions.Ge("Status", 0) || Restrictions.IsNull("Status"))
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .AddOrder(Order.Asc("FormalizedName"))
                    .CreateCriteria("Contragent", "cg", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join)
                    .Add(Restrictions.Eq("TypeName", "УК") || Restrictions.Eq("TypeName", "ГУК"));

                return new List<PFR_INVEST.DataObjects.Contract>(crit.List<PFR_INVEST.DataObjects.Contract>());
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetContractsWithoutRegister(int contractType, long registerOperationID,
            long registerDirectionID, DateTime registerDate)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@" select distinct c, le from ContractHib c 
				 join fetch c.LegalEntity le
				 left join c.TransferLists tr 
				 left join tr.TransferRegister r
				 where c.Status <> -1 and c.TypeID = :contractType
				")
                    .SetParameter("contractType", contractType)
                    .List<ContractHib>();
                ;


                //Функционал фильтрации отключён http://jira.vs.it.ru/browse/DOKIPIV-213

                //                // при таком условии WHERE выираются лишние записи, через nhibernate без описания дополнительных join в маппинге нужный фильтр не реализовать
                //                // поэтому после выбора всего возможного списка контрактов исключаем те, для которых уже есть перечисления
                //                var list = session.CreateQuery(@" select distinct c, le from ContractHib c 
                //				 join fetch c.LegalEntity le
                //				 left join c.TransferLists tr 
                //				 left join tr.TransferRegister r
                //				 where c.Status <> -1 and c.TypeID = :contractType
                //				 and (r = null or r.OperationID <> :operationID or r.DirectionID <>:directionID or r.RegisterDate <> :registerDate) 
                //				")
                //                .SetParameter<int>("contractType", contractType)
                //                .SetParameter<long>("operationID", registerOperationID)
                //                .SetParameter<long>("directionID", registerDirectionID)
                //                .SetParameter<DateTime>("registerDate", registerDate)
                //                .List<ContractHib>();

                //                var restr = session.CreateQuery(@" select distinct c.ID from ContractHib c
                //                 left join c.TransferLists tr 
                //				 left join tr.TransferRegister r
                //                 where c.Status <> -1 and c.TypeID = :contractType
                //				 and r.OperationID = :operationID and r.DirectionID =:directionID and r.RegisterDate = :registerDate")
                //                .SetParameter<int>("contractType", contractType)
                //                .SetParameter<long>("operationID", registerOperationID)
                //                .SetParameter<long>("directionID", registerDirectionID)
                //                .SetParameter<DateTime>("registerDate", registerDate)
                //                .List<long>();

                //                list = list.Where(x => !restr.Contains(x.ID)).ToList();

                //list.ForEach(c => { c.ExtensionData.Add("LegalEntity", c.LegalEntity); });



                return list.Cast<PFR_INVEST.DataObjects.Contract>().ToList();
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetContractsForUKsByType(int contractType, bool showDissolved = true)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add((Restrictions.Ge("Status", 0) || Restrictions.IsNull("Status")) &&
                         Restrictions.Eq("TypeID", contractType));
                if (!showDissolved)
                    crit.Add(Restrictions.Or(Restrictions.IsNull("DissolutionDate"), Restrictions.Or(Restrictions.Eq("DissolutionDate", DateTime.MinValue), Restrictions.Ge("DissolutionDate", DateTime.Now))));

                var list = crit.CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .AddOrder(Order.Asc("FormalizedName"))
                    .CreateCriteria("Contragent", "cg", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join)
                    .Add(Restrictions.Eq("TypeName", "УК") || Restrictions.Eq("TypeName", "ГУК"))
                    .List<ContractHib>();

                return list.Cast<PFR_INVEST.DataObjects.Contract>().ToList();
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetContractsForUKsByTypeAndName(int contractType, int[] contractNames, bool showDissolved = true)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add(Restrictions.Eq("TypeID", contractType))
                    .Add(Restrictions.Ge("Status", 0));
                if (!showDissolved)
                    crit.Add(Restrictions.Or(Restrictions.IsNull("DissolutionDate"), Restrictions.Ge("DissolutionDate", DateTime.Now)));
                crit = crit.CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .CreateCriteria("Contragent", "ca", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join);

                if (contractNames != null && contractNames.Any())
                {
                    crit.Add(Restrictions.In("c.ContractNameID", contractNames));
                }

                var list = crit.List<ContractHib>();
                list.ForEach(a =>
                {
                    a.ExtensionData = new Dictionary<string, object> { { "leName", a.LegalEntity.ShortName }, { "leFormName", a.LegalEntity.FormalizedName } };
                });
                return list.Cast<PFR_INVEST.DataObjects.Contract>().ToList();
            }
        }


#endregion

        public List<PFR_INVEST.DataObjects.Contract> GetContractsByType(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add((Restrictions.Ge("Status", 0) || Restrictions.IsNull("Status")) &&
                         Restrictions.Eq("TypeID", contractType));
                return new List<PFR_INVEST.DataObjects.Contract>(crit.List<PFR_INVEST.DataObjects.Contract>());
            }
        }

        public PFR_INVEST.DataObjects.Contract GetContractByDogovorNum(string dogovorNum)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var contract = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add((Restrictions.Ge("Status", 0) || Restrictions.IsNull("Status")))
                    .Add(Restrictions.Eq("ContractNumber", dogovorNum)).List<PFR_INVEST.DataObjects.Contract>().FirstOrDefault();
                ;
                return contract;
            }
        }
        

        public List<OldSIName> GetOldSINamesSortedByDate(long legalEntityID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        "FROM OldSINameHib oldName  WHERE oldName.LegalEntityID = :le_id ORDER BY oldName.Date ASC")
                        .SetParameter("le_id", legalEntityID)
                        .List<OldSIName>();
                return new List<OldSIName>(list);
            }
        }

        public List<OldSIName> GetOldSINamesForInvDecl(List<long> leIdList)
        {
            if (leIdList == null || leIdList.Count == 0) return new List<OldSIName>();
            return SessionWrapper(session =>
                session.CreateQuery(
                        $@"FROM OldSINameHib oldName  WHERE oldName.LegalEntityID IN ({GenerateStringFromList(leIdList)}) ORDER BY oldName.Date ASC")
                    .List<OldSIName>().ToList());
        }

        public List<ContrInvest> GetAllContrInvestForInvDecl(List<long> contractIdList)
        {
            if (contractIdList == null || contractIdList.Count == 0) return new List<ContrInvest>();
            return SessionWrapper(session =>
                session.CreateQuery(
                        $@"FROM ContrInvest ci WHERE ci.ContractID IN ({GenerateStringFromList(contractIdList)})")
                .List<ContrInvest>().ToList());
        }

        public List<ContrInvestOBL> GetOBLContrInvestForInvDecl(List<long> ciIdList)
        {
            if (ciIdList == null || ciIdList.Count == 0) return new List<ContrInvestOBL>();
            return SessionWrapper(session =>
                session.CreateQuery(
                        $@"FROM ContrInvestOBL ci WHERE ci.ContrInvestID IN ({GenerateStringFromList(ciIdList)})")
                .List<ContrInvestOBL>().ToList());
        }


        public List<ContrInvestAKC> GetAKCContrInvestForInvDecl(List<long> ciIdList)
        {
            if (ciIdList == null || ciIdList.Count == 0) return new List<ContrInvestAKC>();
            return SessionWrapper(session =>
                session.CreateQuery(
                        $@"FROM ContrInvestAKC ci WHERE ci.ContrInvestID IN ({GenerateStringFromList(ciIdList)})")
                .List<ContrInvestAKC>().ToList());
        }

        public List<ContrInvestPAY> GetPAYContrInvestForInvDecl(List<long> ciIdList)
        {
            if (ciIdList == null || ciIdList.Count == 0) return new List<ContrInvestPAY>();
            return SessionWrapper(session =>
                session.CreateQuery(
                        $@"FROM ContrInvestPAY ci WHERE ci.ContrInvestID IN ({GenerateStringFromList(ciIdList)})")
                .List<ContrInvestPAY>().ToList());
        }

        public List<OldNPFName> GetOldNPFNamesSortedByDate(long legalEntityID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        "FROM OldNPFNameHib oldName  WHERE oldName.LegalEntityID = :le_id ORDER BY oldName.Date DESC")
                        .SetParameter("le_id", legalEntityID)
                        .List<OldNPFName>();
                return new List<OldNPFName>(list);
            }
        }

        public License GetCurrentLicense(long legalEntityID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        "FROM LicenseHib currLic  WHERE currLic.LegalEntityID = :le_id ORDER BY currLic.CloseDate DESC")
                        .SetParameter("le_id", legalEntityID)
                        .List<License>();
                //теоретически не должно быть 0, но все же...
                return list.Count == 0 ? new License() : list.First();
            }
        }

        public Account GetContragentAccountByMeasure(long contragentID, string measure)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var acc =
                    session.CreateQuery(
                        "FROM AccountHib acc WHERE acc.ContragentID = :ca_id AND LOWER(TRIM(acc.Kind.Measure)) = :m")
                        .SetParameter("ca_id", contragentID)
                        .SetParameter("m", measure)
                        .List<Account>().FirstOrDefault();

                return acc;
            }
        }

        public Account GetContragentRoubleAccount(long contragentID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var acc =
                    session.CreateQuery(
                        "FROM AccountHib acc  WHERE acc.ContragentID = :ca_id AND acc.Kind.Measure = 'рубли'")
                        .SetParameter("ca_id", contragentID)
                        .List<Account>().FirstOrDefault();

                return acc;
            }
        }

        public List<Contragent> GetContragentsListHib(string type)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result =
                    session.CreateQuery(
                        "FROM ContragentHib c  WHERE TRIM(LOWER(c.TypeName)) = :type ORDER BY c.Name ASC")
                        .SetParameter("type", type.ToLower().Trim())
                        .List<Contragent>().ToList();
                return result;
            }
        }

        public Account GetPFRAccountByMeasure(string measure)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var acc =
                    session.CreateQuery(
                        "FROM AccountHib acc  WHERE LOWER(acc.Contragent.TypeName) = 'пфр' AND LOWER(TRIM(acc.Kind.Measure)) = :m")
                        .SetParameter("m", measure)
                        .List<Account>().FirstOrDefault();

                return acc;
            }
        }

        public Account GetPFRRoubleAccount()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var acc =
                    session.CreateQuery(
                        "FROM AccountHib acc  WHERE LOWER(acc.Contragent.TypeName) = 'пфр' AND acc.Kind.Measure = 'рубли'")
                        .List<Account>().FirstOrDefault();

                return acc;
            }
        }

        public LegalEntity GetLegalEntityForContragent(long contragentID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var le = session.CreateQuery("FROM LegalEntityHib le WHERE le.ContragentID = :ca_id")
                    .SetParameter("ca_id", contragentID)
                    .List<LegalEntity>().First();

                return le;
            }
        }

        public BankAccount GetCurrentBankAccount(long legalEntityID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        "FROM BankAccountHib ba  WHERE ba.LegalEntityID = :le_id AND ba.CloseDate = NULL")
                        .SetParameter("le_id", legalEntityID)
                        .List<BankAccount>();

                return list.Count == 0 ? new BankAccount() : list.First();
            }
        }

#region Учет ЗЛ

        public List<ERZLListItem> GetERZLListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<ERZLListItem>();

                var erzlList = session.CreateCriteria(typeof(ERZLHib), "erzl")
                    .CreateCriteria("FZ", "fz", JoinType.InnerJoin)
                    .List<ERZLHib>().ToList();

                result.AddRange(erzlList.ConvertAll(erzl =>
                {
                    return new ERZLListItem
                    {
                        ID = erzl.ID,
                        Content = erzl.Content,
                        FZDate = erzl.FZ.Date,
                        FZName = erzl.FZ.Name,
                        FZ_ID = erzl.FZ_ID.Value,
                        Company = erzl.Company,
                        RegNum = erzl.RegNum,
                        Date = erzl.Date
                    };
                }));

                return result;
            }
        }


        public List<NPFforERZLNotifyListItem> GetDeadZLForAllNPF(DateTime date)
        {
            List<NPFforERZLNotifyListItem> result = new List<NPFforERZLNotifyListItem>();
            using (var session = DataAccessSystem.OpenSession())
            {
                string sqlQuery =
                    string.Format(@"SELECT {{erzl.*}}, {{ca.*}}, {{le.*}} FROM (SELECT * FROM PFR_BASIC.ERZLNOTIFY er WHERE er.DATE = :date ) erzl
									JOIN {0}.ACCOUNT credAcc ON erzl.CR_ACC_ID = credAcc.ID
									JOIN {0}.ACCOUNT debdAcc ON erzl.DBT_ACC_ID = debdAcc.ID
									RIGHT OUTER JOIN {0}.CONTRAGENT ca ON ca.ID = credAcc.CONTRAGENTID AND ca.ID = debdAcc.CONTRAGENTID --OR credAcc.CONTRAGENTID IS NULL OR debdAcc.CONTRAGENTID IS NULL
									JOIN {0}.STATUS st ON st.ID = ca.STATUS_ID
									JOIN {0}.LEGALENTITY le ON le.CONTRAGENTID = ca.ID
								WHERE   
									LOWER(ca.TYPE) = '{1}'
									AND LOWER(TRIM(st.STATUS)) like '{2}' ORDER BY ca.NAME",
                        DATA_SCHEME, //0
                        "нпф", //1
                        StatusIdentifier.S_ACTIVE.ToLower(), //2
                        date.ToString("yyyy-mm-dd")); //3

                var list1 = session.CreateSQLQuery(sqlQuery)
                    .AddEntity("erzl", typeof(ERZLNotify))
                    .AddEntity("ca", typeof(Contragent))
                    .AddEntity("le", typeof(LegalEntity))
                    .SetParameter("date", date)
                    .List();

                foreach (object[] a in list1)
                {
                    NPFforERZLNotifyListItem item = new NPFforERZLNotifyListItem();
                    //item.Count = 0;
                    item.ERZLNotify = (ERZLNotify)a[0];
                    item.LegalEntity = (LegalEntityHib)a[2];
                    item.Contragent = (ContragentHib)a[1];
                    item.Count = (item.ERZLNotify == null) ? null : item.ERZLNotify.Count;
                    item.ID = $"{item.ERZLNotify?.ID ?? 0}_{item.Contragent?.ID ?? 0}";
                    if(item.ID == "0_0")
                        continue;
                    result.Add(item);
                }
            }
            return result;
        }

        public List<DeadZLListItem> GetDeadZLListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                var result = new List<DeadZLListItem>();
                var erzlList = session.CreateQuery(@"select en, c
				from ERZLNotify en
				join en.CreditAccount ca
				join ca.Contragent c 
				join c.Status s
				where en.ERZL_ID is NULL and en.Date is not null and 
					LOWER(TRIM(s.Name)) like :st ")
                    .SetParameter("st", StatusIdentifier.S_ACTIVE.ToLower()).List<object[]>().ToList();

                result.AddRange(erzlList.ConvertAll(it =>
                {
                    var erzlNotify = it[0] as ERZLNotifyHib;
                    var contragent = it[1] as ContragentHib;
                    var retVal = new DeadZLListItem
                    {
                        ID = erzlNotify.ID,
                        ERZL_ID = null,
                        ContragentID = contragent == null ? 0 : contragent.ID,
                        NPFName = contragent == null ? null : contragent.Name,
                        Count = erzlNotify.Count,
                        Date = erzlNotify.Date,
                        CregitAccID = erzlNotify.CrAccID,
                        DebetAccID = erzlNotify.DbtAccID
                    };
                    if (contragent != null && contragent.LegalEntities != null && contragent.LegalEntities.Count > 0)
                        retVal.NPFName = contragent.GetFullName();
                    else
                        retVal.NPFName = contragent.Name;
                    return retVal;
                }));

                return result;
            }
        }

        public ERZLNotify GetERZLNotifyHib(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(ERZLNotify), "erzlNotify")
                    .Add(Restrictions.Eq("erzlNotify.ID", id));

                crit.CreateCriteria("CreditAccount", "crAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "crCA", JoinType.InnerJoin);

                crit.CreateCriteria("DebitAccount", "dbtAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "dbtCA", JoinType.InnerJoin);

                return crit.UniqueResult() as ERZLNotify;
            }
        }

        public List<ERZLNotify> GetERZLNotifiesHib(long erzlID, string notifyNum)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(ERZLNotifyHib), "erzlNotify")
                    .Add(Restrictions.Eq("erzlNotify.ERZL_ID", erzlID));

                if (notifyNum != null)
                    crit.Add(Restrictions.Like("erzlNotify.NotifyNum", notifyNum, MatchMode.Start));

                crit.AddOrder(Order.Asc("erzlNotify.NotifyNum"));

                crit.CreateCriteria("CreditAccount", "crAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "crCA", JoinType.InnerJoin);

                crit.CreateCriteria("DebitAccount", "dbtAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "dbtCA", JoinType.InnerJoin);


                return crit.List<ERZLNotify>().ToList();
            }
        }

        public List<ERZLNotifiesListItem> GetERZLNotifiesListByERZL_ID(long erzlID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                var result = new List<ERZLNotifiesListItem>();

                var crit = session.CreateCriteria(typeof(ERZLNotifyHib), "erzlNotify");

                crit.CreateCriteria("CreditAccount", "crAcc", CriteriaSpecification.LeftJoin)
                    .CreateCriteria("Contragent", "crCA", CriteriaSpecification.LeftJoin,
                        !Restrictions.Eq("StatusID", (long)(5)));

                crit.CreateCriteria("DebitAccount", "dbtAcc", CriteriaSpecification.LeftJoin)
                    .CreateCriteria("Contragent", "dbtCA", CriteriaSpecification.LeftJoin,
                        !Restrictions.Eq("StatusID", (long)(5)));

                crit.AddOrder(Order.Asc("erzlNotify.NotifyNum"))
                    .Add(Restrictions.Eq("erzlNotify.ERZL_ID", erzlID));

                var erzlNotifiesList = crit
                    .List<ERZLNotifyHib>().ToList();

                string currNotifyNum;
                ContragentHib currContragent;
                Account currNPFAccount;

                if (erzlNotifiesList != null && erzlNotifiesList.Count > 0)
                {
                    currNotifyNum = (erzlNotifiesList.First().NotifyNum ?? string.Empty).Trim();

                    if (erzlNotifiesList.First().MainNPF == "0")
                    {
                        currNPFAccount = erzlNotifiesList.First().CreditAccount;
                        currContragent = erzlNotifiesList.First().CreditAccount.Contragent;
                    }
                    else
                    {
                        currNPFAccount = erzlNotifiesList.First().DebitAccount;
                        currContragent = erzlNotifiesList.First().DebitAccount.Contragent;
                    }

                }
                else
                    return result;

                long? npf2PFRCount = 0;
                long? npf2OtherCount = 0;

                long? pfr2NPFCount = 0;
                long? other2NPFCount = 0;

                bool isOutEqualInput = true;
                bool isInputEqualOut = true;
                long id = 0;

                foreach (var notify in erzlNotifiesList)
                {
                    id = notify.ID;
                    if (notify.DebitAccount != null &&
                        notify.CreditAccount != null &&
                        notify.DebitAccount.Contragent != null &&
                        notify.CreditAccount.Contragent != null &&
                        notify.DebitAccount.Contragent.TypeName != null &&
                        notify.CreditAccount.Contragent.TypeName != null &&
                        notify.CreditAccount.Contragent.StatusID != -1)
                    {
                        if (notify.NotifyNum != null && notify.NotifyNum.Trim() != currNotifyNum)
                        //    if (notify.NotifyNum.Trim() != currNotifyNum) // prev. code
                        {
                            var newItem = new ERZLNotifiesListItem
                            {
                                ID = notify.ID,
                                ERZL_ID = erzlID,
                                NotifyNum = currNotifyNum,
                                Date = notify.Date,
                                ContragentID = currContragent.ID,
                                NPFName = currContragent.GetFullName(),
                                NPFAccountID = currNPFAccount.ID,
                                NPF2PFRCount = npf2PFRCount,
                                NPF2OTHERCount = npf2OtherCount,
                                PFR2NPFCount = pfr2NPFCount,
                                OTHER2NPFCount = other2NPFCount,
                                IsOutNotEqualInput = !isOutEqualInput,
                                IsInputNotEqualOut = !isInputEqualOut
                            };

                            result.Add(newItem);

                            npf2PFRCount = 0;
                            npf2OtherCount = 0;

                            pfr2NPFCount = 0;
                            other2NPFCount = 0;

                            isOutEqualInput = true;
                            isInputEqualOut = true;


                            currNotifyNum = notify.NotifyNum.Trim();

                            if (notify.MainNPF == "0")
                            {
                                currNPFAccount = notify.CreditAccount;
                                currContragent = notify.CreditAccount.Contragent;
                            }
                            else
                            {
                                currNPFAccount = notify.DebitAccount;
                                currContragent = notify.DebitAccount.Contragent;
                            }
                        }



                        if (notify.MainNPF == "0")
                        {
                            if (notify.DebitAccount.Contragent.TypeName.ToLower().Trim() == "нпф")
                            {
                                npf2OtherCount += notify.Count ?? 0;
                                try
                                {
                                    isOutEqualInput = isOutEqualInput && notify.Count == GetReverseNotifyCount(notify);
                                }
                                catch
                                {
                                    isOutEqualInput = false;
                                }
                            }
                            else
                            {
                                npf2PFRCount += notify.Count ?? 0;
                            }
                        }
                        else
                        {
                            if (notify.CreditAccount.Contragent.TypeName.ToLower().Trim() == "нпф")
                            {
                                other2NPFCount += notify.Count ?? 0;
                                try
                                {
                                    isInputEqualOut = isInputEqualOut && notify.Count == GetReverseNotifyCount(notify);
                                }
                                catch
                                {
                                    isInputEqualOut = false;
                                }
                            }
                            else
                            {
                                pfr2NPFCount += notify.Count ?? 0;
                            }
                        }
                    }
                }

                if (currContragent.StatusID != -1)
                {
                    var lastItem = new ERZLNotifiesListItem
                    {
                        ID = id,
                        ERZL_ID = erzlID,
                        NotifyNum = currNotifyNum,
                        Date = erzlNotifiesList.Last().Date,
                        ContragentID = currContragent.ID,
                        NPFName = currContragent.GetFullName(),
                        NPF2PFRCount = npf2PFRCount,
                        NPF2OTHERCount = npf2OtherCount,
                        PFR2NPFCount = pfr2NPFCount,
                        OTHER2NPFCount = other2NPFCount,
                        IsOutNotEqualInput = !isOutEqualInput,
                        IsInputNotEqualOut = !isInputEqualOut
                    };

                    result.Add(lastItem);
                }
                return result;
            }
        }

        public long? GetReverseNotifyCount(ERZLNotify notify)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(ERZLNotifyHib), "erzlNotify");

                if (!string.IsNullOrEmpty(notify.NotifyNum))
                    crit.Add(
                        Restrictions.Eq("erzlNotify.ERZL_ID", notify.ERZL_ID) &&
                        (!Restrictions.Like("erzlNotify.NotifyNum", notify.NotifyNum, MatchMode.Start)) &&
                        !Restrictions.Eq("erzlNotify.MainNPF", notify.MainNPF) &&
                        Restrictions.Eq("erzlNotify.CrAccID", notify.CrAccID) &&
                        Restrictions.Eq("erzlNotify.DbtAccID", notify.DbtAccID)
                        );
                else
                    crit.Add(
                        Restrictions.Eq("erzlNotify.ERZL_ID", notify.ERZL_ID) &&
                        (Restrictions.IsNotNull("erzlNotify.NotifyNum")) &&
                        !Restrictions.Eq("erzlNotify.MainNPF", notify.MainNPF) &&
                        Restrictions.Eq("erzlNotify.CrAccID", notify.CrAccID) &&
                        Restrictions.Eq("erzlNotify.DbtAccID", notify.DbtAccID)
                        );

                crit.CreateCriteria("CreditAccount", "crAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "crCA", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("TypeName", "НПФ"));
                //.Add(Expression.Sql("lower({alias}.TYPE) like lower(?)", "нпф", NHibernateUtil.String));

                crit.CreateCriteria("DebitAccount", "dbtAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "dbtCA", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("TypeName", "НПФ"));
                //.Add(Expression.Sql("lower({alias}.TYPE) like lower(?)", "нпф", NHibernateUtil.String));

                crit.SetProjection(Projections.Sum("Count"));

                return crit.UniqueResult<long?>();
            }
        }

        public long? GetInputZLCountByNPFAccountID(long npfAccountID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(ERZLNotifyHib), "erzlNotify");

                crit.CreateCriteria("CreditAccount", "crAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "crCA", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("TypeName", "НПФ"));

                crit.Add(Restrictions.Eq("DbtAccID", npfAccountID));

                crit.SetProjection(Projections.Sum("Count"));

                return crit.UniqueResult<long?>();
            }
        }

        public long? GetOutputZLCountByNPFAccountID(long npfAccountID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(ERZLNotifyHib), "erzlNotify");

                crit.CreateCriteria("DebitAccount", "dbtAcc", JoinType.InnerJoin)
                    .CreateCriteria("Contragent", "dbtCA", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("TypeName", "НПФ"));

                crit.Add(Restrictions.Eq("CrAccID", npfAccountID));

                crit.SetProjection(Projections.Sum("Count"));

                return crit.UniqueResult<long?>();
            }
        }

        public List<NPFforERZLNotifyListItem> GetNPFforERZLNotifyListHib(long erzlID, string notifynum, bool fromNPF)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<NPFforERZLNotifyListItem>();

                var crit = session.CreateCriteria(typeof(LegalEntityHib), "le")
                    .CreateCriteria("Contragent", "ca", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("TypeName", "НПФ"))
                    .AddOrder(Order.Asc("ca.Name"));

                crit.CreateCriteria("Accounts", "accs", JoinType.InnerJoin)
                    .CreateCriteria("Kind", "k", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("Measure", "ЗЛ"));

                var leList = new List<LegalEntityHib>(crit.List().Cast<LegalEntityHib>());

                foreach (var le in leList)
                {
                    long zlAccountID = 0;
                    try
                    {
                        zlAccountID = (from acc in le.Contragent.Accounts
                                       where acc.Kind.Measure.ToLower().Trim() == "зл"
                                       select acc.ID).FirstOrDefault();
                    }
                    catch
                    {

                    }

                    ERZLNotify notifyToAdd = null;

                    notifyToAdd = new ERZLNotify
                    {
                        Count = null,
                        CrAccID = fromNPF ? 0 : zlAccountID,
                        DbtAccID = fromNPF ? zlAccountID : 0,
                        ERZL_ID = erzlID,
                        NotifyNum = notifynum.Trim(),
                        MainNPF = fromNPF ? "0" : "1"
                    };
                    long? count = null;

                    var item = new NPFforERZLNotifyListItem
                    {
                        Contragent = le.Contragent,
                        Count = count,
                        ERZLNotify = notifyToAdd,
                        ExistERZLNotifyCount = null
                    };

                    result.Add(item);
                }

                return result;
            }
        }

        public List<NPFforERZLNotifyListItem> GetNPF2NPFListHib(long erzlID, string notifynum, long? mainNPFAccID,
            bool fromNPF)
        {
            notifynum = (notifynum ?? string.Empty).Trim();
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<NPFforERZLNotifyListItem>();

                var crit = session.CreateCriteria(typeof(LegalEntityHib), "le")
                    .CreateCriteria("Contragent", "ca", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join)
                    .Add(Restrictions.Eq("TypeName", "НПФ"))
                    .Add(!Restrictions.Eq("StatusID", ((long)-1)))
                    .AddOrder(Order.Asc("ca.Name"));

                crit.CreateCriteria("Accounts", "accs", JoinType.InnerJoin)
                    .SetFetchMode("Accounts", FetchMode.Join)
                    .CreateCriteria("Kind", "k", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("Measure", "ЗЛ"));

                crit.CreateCriteria("Status", "s", JoinType.InnerJoin)
                    .Add(
                        !Expression.Sql("lower({alias}.STATUS) like (?)", StatusIdentifier.S_REORGANIZED.ToLower(),
                            NHibernateUtil.String));

                var leList = new List<LegalEntityHib>(crit.List().Cast<LegalEntityHib>());

                var erzlNotifiesInErzl = new List<ERZLNotify>(GetERZLNotifiesHib(erzlID, null));
                //List<ERZLNotify> erzlNotifiesInVirtNotify = new List<ERZLNotify>(GetERZLNotifiesHib(erzl_id, notifynum));

                NPFforERZLNotifyListItem item;

                foreach (var le in leList)
                {
                    long zlAccountID = 0;
                    try
                    {
                        zlAccountID = le.Contragent.Accounts.Where(a => a.Kind.Measure.ToLower().Trim() == "зл").Select(a => a.ID).FirstOrDefault();
                    }
                    catch
                    {

                    }


                    long? count = null;
                    ERZLNotify notifyToAdd = null;


                    long? reverseCount = null;
                    ERZLNotify reverseNotify = null;



                    if (mainNPFAccID != null && mainNPFAccID > 0)
                    {
                        reverseNotify = erzlNotifiesInErzl.Find(not =>
                        {
                            if (!string.IsNullOrEmpty(notifynum.Trim()))
                            {
                                return (fromNPF && (not.NotifyNum == null || not.NotifyNum?.Trim() != notifynum) &&
                                        not.MainNPF == "1" && not.CrAccID == mainNPFAccID &&
                                        not.DbtAccID == zlAccountID)
                                        ||
                                       (!fromNPF && (not.NotifyNum == null || not.NotifyNum?.Trim() != notifynum) &&
                                        not.MainNPF == "0" && not.CrAccID == zlAccountID &&
                                        not.DbtAccID == mainNPFAccID);
                            }
                            return (fromNPF && !string.IsNullOrEmpty(not.NotifyNum?.Trim()) && not.MainNPF == "1" &&
                                    not.CrAccID == zlAccountID && not.DbtAccID == mainNPFAccID) ||
                                   (!fromNPF && !string.IsNullOrEmpty(not.NotifyNum?.Trim()) && not.MainNPF == "0" &&
                                    not.CrAccID == mainNPFAccID && not.DbtAccID == zlAccountID);
                        });
                    }

                    if (reverseNotify != null)
                    {
                        reverseCount = reverseNotify.Count;
                    }

                    item = new NPFforERZLNotifyListItem
                    {
                        LegalEntity = le,
                        Contragent = le.Contragent,
                        ContragentZLAccountID = zlAccountID,
                        Count = count,
                        ERZLNotify = notifyToAdd,
                        ExistERZLNotifyCount = reverseCount
                    };

                    result.Add(item);
                }

                return result;
            }
        }

        public RegisterHib GetRegisterByERZLHib(long erzlID, string direction)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                try
                {
                    return session.CreateCriteria(typeof(RegisterHib), "reg")
                        .Add(Restrictions.Eq("reg.ERZL_ID", erzlID))
                        .Add(Restrictions.Eq("reg.StatusID", 1L))
                        .Add(Restrictions.Like("reg.Kind", direction, MatchMode.Start))
                        .UniqueResult<RegisterHib>();
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Concat("Невозможно определить уникальный реестр для ЗЛ с ID=",
                        erzlID.ToString(), "\r\n", ex.Message));
                }
            }
        }

        public Portfolio GetPortfolioByNameHib(string sName)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                try
                {
                    return session.CreateQuery(@"FROM Portfolio p
							WHERE LOWER(TRIM(p.Year)) LIKE :name")
                        .SetParameter("name", sName)
                        .UniqueResult<Portfolio>();
                }
                catch
                {
                    return null;
                }
            }
        }

        public Dictionary<bool, long> /*bool*/ GenerateRegisterByERZL(long erzlID)
        {
            Dictionary<bool, long> dictionary = new Dictionary<bool, long>();
            long regID = 0;
            using (var session = DataAccessSystem.OpenSession())
            {
                try
                {
                    var regCredit = GetRegisterByERZLHib(erzlID, RegisterIdentifier.PFRtoNPF);
                    var regDebit = GetRegisterByERZLHib(erzlID, RegisterIdentifier.NPFtoPFR);
                    var portfolio = GetPortfolioByNameHib("совокупный портфель");

                    var erzl = session.CreateCriteria(typeof(ERZL), "erzl")
                        .Add(Restrictions.Eq("erzl.ID", erzlID)).List<ERZL>().FirstOrDefault();

                    if (regCredit == null)
                    {

                        regCredit = new RegisterHib();
                        regCredit.RegDate = null;
                        regCredit.ERZL_ID = erzlID;
                        regCredit.FZ_ID = erzl.FZ_ID;
                        regCredit.Company = erzl.Company;
                        //RegCredit.PortfolioID = portfolio == null ? null : (long?)portfolio.ID; //20;
                    }

                    if (regDebit == null)
                    {
                        regDebit = new RegisterHib();
                        regDebit.RegDate = null;
                        regDebit.ERZL_ID = erzlID;
                        regDebit.FZ_ID = erzl.FZ_ID;
                        regDebit.Company = erzl.Company;
                        //RegDebit.PortfolioID = portfolio == null ? null : (long?)portfolio.ID; //20;
                    }
                    //long crRegID = 0; // из пфр
                    //long dbtRegID = 0; // в пфр

                    var lstCreditFinRegisters = regCredit.FinregistersList;
                    var lstDebitFinRegisters = regDebit.FinregistersList;

                    var crit = session.CreateCriteria(typeof(ERZLNotifyHib), "erzlNotify")
                        .Add(Restrictions.Eq("erzlNotify.ERZL_ID", erzlID));

                    crit.CreateCriteria("CreditAccount", "crAcc", JoinType.InnerJoin)
                        .CreateCriteria("Contragent", "crCA", JoinType.InnerJoin);

                    crit.CreateCriteria("DebitAccount", "dbtAcc", JoinType.InnerJoin)
                        .CreateCriteria("Contragent", "dbtCA", JoinType.InnerJoin);

                    var notifiesList = new List<ERZLNotifyHib>(crit.List<ERZLNotifyHib>());

                    FinregisterHib fr;

                    if (notifiesList != null && notifiesList.Count > 0)
                    {
                        foreach (var notify in notifiesList)
                        {
                            try
                            {
                                fr = new FinregisterHib();

                                fr.Date = null;
                                fr.CrAccID = notify.CrAccID;
                                fr.DbtAccID = notify.DbtAccID;
                                fr.ZLCount = notify.Count;
                                fr.Status = RegisterIdentifier.FinregisterStatuses.Created;

                                Contragent ca = notify.CreditAccount.Contragent;
                                if (ca != null)
                                {
                                    if (ca.TypeName.Trim().ToLower() == "пфр")
                                    {
                                        if (regCredit.FZ_ID != null)
                                        {
                                            switch (regCredit.FZ_ID.Value)
                                            {
                                                case 1:
                                                    regCredit.Content = "Переход ЗЛ ежегодно (75-ФЗ)";
                                                    //RegCredit.PortfolioID = null;
                                                    break;
                                                case 2:
                                                    regCredit.Content = "Переход ЗЛ ежемесячно (56-ФЗ)";
                                                    break;
                                                case 3:
                                                    regCredit.Content = "Материнский семейный капитал (256-ФЗ)";
                                                    break;
                                                default:
                                                    break;
                                            }
                                            regCredit.Kind = RegisterIdentifier.PFRtoNPF;
                                            try
                                            {
                                                session.SaveOrUpdate(regCredit);
                                                session.Flush();
                                            }
                                            catch
                                            {
                                                regCredit.ID = 0;
                                            }
                                        }

                                        if (regCredit.ID > 0)
                                        {
                                            fr.RegisterID = regCredit.ID;
                                            regID = regCredit.ID;
                                        }
                                    }

                                    var acc = GetContragentAccountByMeasure(ca.ID, "рубли");
                                    if (acc != null)
                                        fr.CrAccID = acc.ID;
                                }

                                ca = notify.DebitAccount.Contragent;
                                if (ca != null)
                                {
                                    if (ca.TypeName.Trim().ToLower() == "пфр")
                                    {
                                        if (regCredit.FZ_ID != null && regCredit.FZ_ID == 1)
                                        {
                                            regDebit.Kind = RegisterIdentifier.NPFtoPFR;
                                            regDebit.Content = "Переход ЗЛ ежегодно (75-ФЗ)";
                                            try
                                            {
                                                session.SaveOrUpdate(regDebit);
                                                session.Flush();
                                            }
                                            catch
                                            {
                                                regDebit.ID = 0;
                                            }
                                        }

                                        if (regDebit.ID > 0)
                                        {
                                            fr.RegisterID = regDebit.ID;
                                            regID = regDebit.ID;
                                        }
                                    }

                                    var acc = GetContragentAccountByMeasure(ca.ID, "рубли");
                                    if (acc != null)
                                        fr.DbtAccID = acc.ID;
                                }

                                // update finreg if exist
                                if (lstCreditFinRegisters != null)
                                {
                                    FinregisterHib _fr;
                                    try
                                    {
                                        _fr =
                                            lstCreditFinRegisters.Single(
                                                f => (f.CrAccID == fr.CrAccID && f.DbtAccID == fr.DbtAccID));
                                    }
                                    catch
                                    {
                                        _fr = null;
                                    }
                                    if (_fr != null)
                                    {
                                        _fr.ZLCount = notify.Count;
                                        session.Save(_fr);
                                        session.Flush();
                                        continue;
                                    }
                                }
                                if (lstDebitFinRegisters != null)
                                {
                                    FinregisterHib _fr;
                                    try
                                    {
                                        _fr =
                                            lstDebitFinRegisters.Single(
                                                f => (f.CrAccID == fr.CrAccID && f.DbtAccID == fr.DbtAccID));
                                    }
                                    catch
                                    {
                                        _fr = null;
                                    }
                                    if (_fr != null)
                                    {
                                        _fr.ZLCount = notify.Count;
                                        session.Save(_fr);
                                        session.Flush();
                                        continue;
                                    }
                                }
                                // otherwise create new finreg
                                if (fr.RegisterID > 0 && fr.ZLCount > 0)
                                    try
                                    {
                                        session.Save(fr);
                                    }
                                    catch
                                    {
                                    }
                            }
                            catch
                            {
                            }
                        }
                    }
                    dictionary.Add(true, regID);
                    return dictionary;
                    //                    return true;
                }
                catch
                {
                    dictionary.Add(false, regID);
                    return dictionary;
                    //                    return false;
                }
            }
        }

        public bool SaveERZLNotifiesList(List<ERZLNotify> list)
        {
            if (list.Count == 0)
                return true;

            foreach (var item in list)
            {
                try
                {
                    Save(typeof(ERZLNotify).FullName, item);
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }

        public bool EraseERZLNotifiesList(List<ERZLNotify> list)
        {
            if (list.Count == 0)
                return true;

            foreach (var item in list)
            {
                try
                {
                    Delete(typeof(ERZLNotify).FullName, item);
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }

#endregion

#region GetFinregisterWithCorrAndNPFList

        public List<FinregisterWithCorrAndNPFListItem> GetFinregisterWithCorrAndNPFList(long? registerID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<FinregisterWithCorrAndNPFListItem>();

                var reg = session.Get<Register>(registerID);

                if (reg != null && reg.Kind != null)
                {
                    var crit = session.CreateCriteria(typeof(FinregisterHib), "fr")
                        .Add(Restrictions.Eq("RegisterID", registerID))
                        .Add(Restrictions.Eq("StatusID", 1L));

                    if (RegisterIdentifier.IsFromNPF(reg.Kind.ToLower()))
                    {
                        crit.CreateCriteria("CreditAccount", "acc", JoinType.InnerJoin)
                            .CreateCriteria("Contragent", "ca", JoinType.InnerJoin);
                    }
                    else
                    {
                        crit.CreateCriteria("DebitAccount", "acc", JoinType.InnerJoin)
                            .CreateCriteria("Contragent", "ca", JoinType.InnerJoin);
                    }

                    //crit.CreateCriteria("CorrList", "corrList", NHibernate.SqlCommand.JoinType.LeftOuterJoin);

                    var list = new List<FinregisterHib>(crit.List().Cast<FinregisterHib>());

                    foreach (var fr in list)
                    {
                        decimal? corrSum = null;

                        var item = new FinregisterWithCorrAndNPFListItem
                        {
                            ID = fr.ID,
                            Finregister = fr,
                            NPFName = RegisterIdentifier.IsFromNPF(reg.Kind.ToLower())
                                ? fr.CreditAccount.Contragent.Name
                                : fr.DebitAccount.Contragent.Name,
                            NPFStatus = RegisterIdentifier.IsFromNPF(reg.Kind.ToLower())
                                ? fr.CreditAccount.Contragent.Status.Name
                                : fr.DebitAccount.Contragent.Status.Name,
                            Count = fr.Count ?? 0,
                            CorrCount = corrSum,
                            CFRCount = fr.CFRCount,
                            ZLCount = fr.ZLCount,
                            IsItemChanged = false
                        };

                        result.Add(item);
                    }
                }

                return result;
            }
        }

        public List<FinregisterWithCorrAndNPFListItem> GetFinregisterWithCorrAndNPFListByPortfolio(long? portfolioID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<FinregisterWithCorrAndNPFListItem>();

                //var reg = session.Get<Register>(register_id);
                var regQuery = session.CreateQuery(@"SELECT rg FROM 
												Register rg												
												JOIN rg.Portfolio p
												WHERE rg.StatusID <> -1 AND (rg.Kind = :kind AND p.ID = :pID)")
                    .SetParameter("kind", RegisterIdentifier.TempAllocation)
                    .SetParameter("pID", portfolioID);
                var regList = regQuery.List().Cast<Register>();

                if (regList != null && regList.Any())
                {
                    var crit = session.CreateCriteria(typeof(FinregisterHib), "fr")
                        .Add(Restrictions.In("RegisterID", regList.Select(x => x.ID).ToArray()))
                        .Add(Restrictions.Eq("StatusID", 1L));

                    //if (RegisterIdentifier.IsNPFtoPFR(reg.Kind.ToLower()))
                    //{
                    //    crit.CreateCriteria("CreditAccount", "acc", NHibernate.SqlCommand.JoinType.InnerJoin)
                    //    .CreateCriteria("Contragent", "ca", NHibernate.SqlCommand.JoinType.InnerJoin);
                    //}
                    //else
                    //{
                    //    crit.CreateCriteria("DebitAccount", "acc", NHibernate.SqlCommand.JoinType.InnerJoin)
                    //    .CreateCriteria("Contragent", "ca", NHibernate.SqlCommand.JoinType.InnerJoin);
                    //}

                    //crit.CreateCriteria("CorrList", "corrList", NHibernate.SqlCommand.JoinType.LeftOuterJoin);

                    var list = new List<FinregisterHib>(crit.List().Cast<FinregisterHib>());

                    FinregisterWithCorrAndNPFListItem item;

                    foreach (var fr in list)
                    {
                        decimal? corrSum = null;


                        item = new FinregisterWithCorrAndNPFListItem
                        {
                            ID = fr.ID,
                            Finregister = fr,
                            //NPFName = RegisterIdentifier.IsNPFtoPFR(reg.Kind.ToLower()) ?
                            //fr.CreditAccount.Contragent.Name :
                            //fr.DebitAccount.Contragent.Name,
                            //NPFStatus = RegisterIdentifier.IsNPFtoPFR(reg.Kind.ToLower()) ?
                            //fr.CreditAccount.Contragent.Status.Name :
                            //fr.DebitAccount.Contragent.Status.Name,
                            Count = fr.Count ?? 0,
                            CFRCount = fr.CFRCount,
                            CorrCount = corrSum,
                            ZLCount = fr.ZLCount,
                            IsItemChanged = false
                        };

                        result.Add(item);
                    }
                }

                return result;
            }
        }

#endregion

#region Работа с ЦБ

        public long GetOrderCounterForPortfolio(long portfolioYearID, string orderType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //выбираем все поручения по типу
                var crit = session.CreateCriteria(typeof(CbOrderHib), "cborder")
                    .Add(Restrictions.Eq("Type", orderType));

                //ограничиваем по году портфеля
                crit.CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("YearID", portfolioYearID));

                var cbordersList = new List<CbOrderHib>(crit.List().Cast<CbOrderHib>().Distinct());
                var ilist = new List<long>();
                ilist.AddRange(cbordersList.Select(o => o.RegNumLong));
                //foreach (var item in cbordersList)
                //{
                //    if (!string.IsNullOrEmpty(item.RegNum))
                //    {
                //        string[] tmp = item.RegNum.Split(new char[] { '/' });
                //        if (tmp.Length == 0) continue;
                //        int itmp;
                //        if (int.TryParse(tmp[tmp.Length - 1], out itmp))
                //            ilist.Add(itmp);
                //    }
                //}

                return ilist.Count == 0 ? 1 : ilist.Max();
            }
        }

        public long GetOrderCounterForPortfolioYear(long? portfolioYearID, string orderType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //выбираем все поручения по типу
                var crit = session.CreateCriteria(typeof(CbOrderHib), "cborder")
                    .Add(Restrictions.Eq("Type", orderType));

                //ограничиваем по году портфеля
                var pfCrit = crit.CreateCriteria("Portfolio", "p", JoinType.InnerJoin);
                if (portfolioYearID.HasValue)
                {
                    pfCrit.Add(Restrictions.Eq("YearID", portfolioYearID));
                }
                else
                {
                    pfCrit.Add(Restrictions.IsNull("YearID"));
                }

                var cbordersList = new List<CbOrderHib>(crit.List().Cast<CbOrderHib>().Distinct());
                var ilist = new List<long>();
                ilist.AddRange(cbordersList.Select(o => o.RegNumLong));

                return ilist.Count == 0 ? 0 : ilist.Max();
            }
        }

#region Private GetPortfolioStatus

        private List<string> GetSecurityIDs()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateSQLQuery("SELECT securityid "
                                                  + " FROM PFR_BASIC.SECURITY").List<string>();
                return list.ToList();
            }
        }

        private List<string> GetPortfolioNames()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateSQLQuery("SELECT year"
                                                  + " FROM PFR_BASIC.PORTFOLIO").List<string>();

                return list.ToList();
            }
        }

        private decimal CalcNkd(long? id, DateTime? dateOrder, DateTime? dateDepo, string currencyName)
        {
            decimal nkd = 0;
            bool currencyIsRubles = CurrencyIdentifier.IsRUB(currencyName);
            var paymentsList = GetPaymentsListHib(id);
            if (dateOrder.HasValue && dateDepo.HasValue && paymentsList != null)
            {
                DateTime dt;
                if (currencyIsRubles)
                    dt = dateOrder.Value;
                else
                    dt = dateDepo.Value;

                var closestPayment = OrderReportsHelper.GetClosestPayment(dt, paymentsList);

                if (closestPayment != null)
                {
                    if (closestPayment.CouponPeriod.HasValue) //stub ClosestPayment.CouponPeriod is NULL
                    {
                        if (currencyIsRubles)
                            nkd = OrderReportsHelper.CalcNKDForRubles(dateOrder.Value, closestPayment);
                        else
                            nkd = OrderReportsHelper.CalcNKDForDollars(dateDepo.Value, closestPayment);
                    }
                    else
                    {
                        nkd = 0;
                        LogManager.Log("Attention! " + "ClosestPayment.CouponPeriod is NULL! " + $" Payment.ID={closestPayment.ID} ");
                    }

                }
            }
            else
                nkd = 0;

            return nkd;
        }

        private decimal GetCurs(long? id)
        {
            if (id == null || !id.HasValue)
                return 1m;
            var curses = GetCurrentCursesHib();
            foreach (var item in curses)
                if (item.CurrencyID == id)
                    return item.Value ?? 1m;
            return 1m;
        }

        private List<CustomReportsListItem> GetCustomSellReportsListHib()
        {
            var result = new List<CustomReportsListItem>();

            using (var session = DataAccessSystem.OpenSession())
            {
                var reports = session.CreateCriteria(typeof(OrdReportHib), "rep")
                    .AddOrder(Order.Asc("DateOrder"))
                    .AddOrder(Order.Asc("DateDEPO"))
                    .CreateCriteria("CbInOrder", JoinType.InnerJoin)
                    .SetFetchMode("CbInOrder", FetchMode.Join);

                reports.CreateCriteria("Security", JoinType.InnerJoin)
                    .SetFetchMode("Security", FetchMode.Join);
                /*
									.CreateCriteria("Currency", "currency", NHibernate.SqlCommand.JoinType.InnerJoin)
									.SetFetchMode("Currency", FetchMode.Join);*/

                reports.CreateCriteria("Order", JoinType.InnerJoin)
                    .SetFetchMode("Order", FetchMode.Join)
                    .Add(Restrictions.Like("Type", OrderTypeIdentifier.SELLING + "%"));

                foreach (var item in reports.List<OrdReportHib>())
                    result.Add(new CustomReportsListItem
                    {
                        Count = item.Count ?? 0,
                        SecurityId = item.CbInOrder.Security.SecurityId
                    });
            }
            return result;
        }

        private List<CustomReportsListItem> GetCustomReportsListHib(bool forSale)
        {
            var result = new List<CustomReportsListItem>();

            using (var session = DataAccessSystem.OpenSession())
            {
                var reports = session.CreateCriteria(typeof(OrdReportHib), "rep")
                    .AddOrder(Order.Asc("DateOrder"))
                    .AddOrder(Order.Asc("DateDEPO"))
                    .CreateCriteria("CbInOrder", JoinType.InnerJoin)
                    .SetFetchMode("CbInOrder", FetchMode.Join);

                reports.CreateCriteria("Security", JoinType.InnerJoin)
                    .SetFetchMode("Security", FetchMode.Join)
                    .Add(Restrictions.Ge("RepaymentDate", DateTime.Now))
                    .CreateCriteria("Currency", "currency", JoinType.InnerJoin)
                    .SetFetchMode("Currency", FetchMode.Join);

                var orders = reports.CreateCriteria("Order", JoinType.InnerJoin)
                    .SetFetchMode("Order", FetchMode.Join);
                if (forSale)
                    orders.Add(Restrictions.Like("Type", OrderTypeIdentifier.SELLING + "%"));
                else
                    orders.Add(Restrictions.Like("Type", OrderTypeIdentifier.BUYING + "%"));

                orders.CreateCriteria("Portfolio", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join);

                foreach (var report in reports.List<OrdReportHib>())
                {
                    //decimal sumNom = 0;
                    //decimal sumWithoutNKD = 0;
                    decimal nkd = 0;
                    decimal nfn = 0;

#region NFN

                    if (report.CbInOrder.Security.NomValue.HasValue && report.Count.HasValue)
                    {
                        nfn = OrderReportsHelper.CalcNFN(report.CbInOrder.Security.Payments.ToList(), report.DateDEPO,
                            report.CbInOrder.Security.NomValue.Value);
                        //if (nfn == 0) nfn = report.CbInOrder.Security.NomValue ?? 0;
                        /*var list = report.CbInOrder.Security.Payments.Where(paym => paym.PaymentDate >= DateTime.Today);
						if (list != null && list.Count() > 0)
							//sumNom = 0m;
							nfn = (list.First().NotRepaymentNom ?? 0m);*/
                        //sumNom = nfn * report.Count.Value;
                    }

#endregion

                    //if (report.Price.HasValue)
                    //    sumWithoutNKD = sumNom * report.Price.Value / 100m;

#region NKD

                    bool currencyIsRubles = CurrencyIdentifier.IsRUB(report.CbInOrder.Security.Currency.Name);
                    var paymentsList = report.CbInOrder.Security.Payments.ToList();
                    if (report.DateOrder.HasValue && report.DateDEPO.HasValue && paymentsList != null)
                    {
                        DateTime dt;
                        if (currencyIsRubles)
                            dt = report.DateOrder.Value;
                        else
                            dt = report.DateDEPO.Value;

                        var closestPayment = OrderReportsHelper.GetClosestPayment(dt, paymentsList);

                        if (closestPayment != null)
                        {

                            if (closestPayment.CouponPeriod.HasValue) //stub ClosestPayment.CouponPeriod is NULL
                            {
                                if (currencyIsRubles)
                                    nkd = OrderReportsHelper.CalcNKDForRubles(report.DateOrder.Value, closestPayment);
                                else
                                    nkd = OrderReportsHelper.CalcNKDForDollars(report.DateDEPO.Value, closestPayment);
                            }
                            else
                            {
                                nkd = 0;
                                LogManager.Log("Attention! " + "ClosestPayment.CouponPeriod is NULL! " + $" Payment.ID={closestPayment.ID} ");
                            }

                        }
                    }
                    else
                    {
                        nkd = 0;
                    }
                    if (nkd < 0)
                        nkd = 0;
                    else
                        nkd = Math.Round(nkd, 2) * report.Count ?? 0;

#endregion

                    result.Add(new CustomReportsListItem
                    {
                        /*RegNum = report.CbInOrder.Order.RegNum,
												SumNom = sumNom,
												SumWithoutNKD = sumWithoutNKD,
												Sum = sumWithoutNKD + nkd,
												*/
                        SecurityName = report.CbInOrder.Security.Name,
                        CurrencyName = report.CbInOrder.Security.Currency.Name,
                        DateOrder = report.DateOrder,
                        DateDEPO = report.DateDEPO,
                        Count = report.Count ?? 0,
                        Price = report.Price ?? 0,
                        Yield = report.Yield ?? 0,
                        ID = report.ID,
                        PortfolioYear = report.CbInOrder.Order.Portfolio.Year,
                        SecurityId = report.CbInOrder.Security.SecurityId,
                        KindName = report.CbInOrder.Security.KindName,
                        NotFadedNominal = nfn,
                        NKD = nkd,
                        FadeDate = report.CbInOrder.Security.RepaymentDate,
                        CurrencyID = report.CbInOrder.Security.CurrencyID
                    });
                }
            }

            return result;
        }

#endregion

        public List<PortfolioStatusListItem> GetPortfolioStatusListHib()
        {
            var sellList = GetCustomReportsListHib(true);
            var buyList = GetCustomReportsListHib(false);
            var cbidList = GetSecurityIDs();
            var pnList = GetPortfolioNames();
            var result = new List<PortfolioStatusListItem>();


            foreach (var id in cbidList)
            {
                if (id == null)
                    continue;


                var idSellList = sellList.FindAll(a => a.SecurityId == id); // && a.FadeDate >= DateTime.Now
                var idBuyList = buyList.FindAll(a => a.SecurityId == id);
                //.OrderBy(a => a.DateOrder.Value).ToList();//first in start
                foreach (var portfolio in pnList)
                {
                    var selectedSellList = idSellList.FindAll(a => a.PortfolioYear == portfolio);
                    var selectedBuyList = idBuyList.FindAll(a => a.PortfolioYear == portfolio);

                    long boughtCount = selectedBuyList.Sum(a => a.Count);
                    long soldCount = selectedSellList.Sum(a => a.Count);
                    long leftCount = boughtCount - soldCount;
                    if (leftCount <= 0)
                        continue;

                    if (soldCount > 0)
                    {
                        long counter = soldCount;
                        for (int i = 0; i < selectedBuyList.Count; i++)
                        {
                            counter -= selectedBuyList[i].Count;
                            if (counter >= 0) //вычисляем дальше
                            {
                                selectedBuyList.RemoveAt(i);
                                if (counter == 0)
                                    break; //чистый конец
                            }
                            else //грязный конец
                            {
                                selectedBuyList[i].Count = Math.Abs(counter);
                                //decimal nkd = CalcNKD(SelectedBuyList[i].ID, SelectedBuyList[i].DateOrder, SelectedBuyList[i].DateDEPO, SelectedBuyList[i].CurrencyName);
                                //if (nkd < 0)
                                //    nkd = 0;
                                //else
                                //    nkd = Math.Round(nkd, 2) * SelectedBuyList[i].Count;
                                break;
                            }
                        }
                    }

                    decimal commonSumWonkd = 0;
                    decimal commonSumNkd = 0;
                    decimal commonNkdSum = 0;

                    //получили список оставшихся акций SelectedBuyList
                    foreach (var sample in selectedBuyList)
                    {
                        //var sample = SelectedBuyList[0];
                        var sumWonkd = (sample.NotFadedNominal * sample.Count) * sample.Price / 100m;
                        //SelectedBuyList.Sum(a => ((a.NotFadedNominal * a.Count) * a.Price / 100m));
                        commonSumWonkd += sumWonkd;
                        var sumNkd = sumWonkd + sample.NKD;
                        //SelectedBuyList.Sum(a => ((a.NotFadedNominal * a.Count) * a.Price / 100m) + a.NKD);
                        commonSumNkd += sumNkd;
                        commonNkdSum += sample.NKD;
                    }
                    var midwealth = commonSumNkd == 0
                        ? 0m
                        : selectedBuyList.Sum(a => (((a.NotFadedNominal * a.Count) * a.Price / 100m) + a.NKD) * a.Yield) /
                          commonSumNkd;
                    var rsum = CurrencyIdentifier.IsRUB(selectedBuyList[0].CurrencyName)
                        ? commonSumNkd
                        : commonSumNkd * GetCurs(selectedBuyList[0].CurrencyID);


                    var item = new PortfolioStatusListItem
                    {
                        PortfolioYear = selectedBuyList[0].PortfolioYear,
                        SecurityID = id,
                        CurrencyName = selectedBuyList[0].CurrencyName,
                        Kind = selectedBuyList[0].KindName,
                        Name = selectedBuyList[0].SecurityName,
                        Count = leftCount,
                        VolumeByNominal = selectedBuyList.Sum(a => a.NotFadedNominal * a.Count),
                        SummNKD = commonSumNkd,
                        NKD = commonNkdSum,
                        SummWithoutNKD = commonSumWonkd,
                        NominalPercent = 0,
                        MidWealth = midwealth,
                        MidPrice = selectedBuyList.Sum(a => a.Price * a.Count) / leftCount,
                        FadeDate = selectedBuyList[0].FadeDate,
                        SumRoubles = rsum
                    };
                    result.Add(item);

                }
            }
            return result;
        }


        public long GetCountOrdersListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(CbOrderHib), "cborder");
                crit.CreateCriteria("Portfolio", "portf", JoinType.InnerJoin);

                crit.CreateCriteria("CbInOrderList", "cbinord", JoinType.LeftOuterJoin)
                    .CreateCriteria("Security", "sec", JoinType.LeftOuterJoin);

                return
                    crit.SetResultTransformer(Transformers.DistinctRootEntity)
                        .SetProjection(Projections.RowCountInt64())
                        .UniqueResult<long>();
                //return session.CreateCriteria(typeof(CbOrderHib), "cborder")
                //    .CreateCriteria("Portfolio", "portf", NHibernate.SqlCommand.JoinType.InnerJoin)
                //    .CreateCriteria("CbInOrderList", "cbinord", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                //    .CreateCriteria("Security", "sec", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                //    .SetResultTransformer(Transformers.DistinctRootEntity).SetProjection(Projections.RowCount()).UniqueResult<long>();

            }
        }


        public List<OrdersListItem> GetOrdersListByPageHib(int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<OrdersListItem>();

                var crit = session.CreateCriteria(typeof(CbOrderHib), "cborder");
                crit.CreateCriteria("Portfolio", "portf", JoinType.InnerJoin);

                crit.CreateCriteria("CbInOrderList", "cbinord", JoinType.LeftOuterJoin)
                    .CreateCriteria("Security", "sec", JoinType.LeftOuterJoin)
                    .SetResultTransformer(Transformers.DistinctRootEntity)
                    .SetFirstResult(startIndex).SetMaxResults(PageSize);

                var orderList = new List<CbOrderHib>(crit.List().Cast<CbOrderHib>());

                OrdersListItem item;

                foreach (var order in orderList)
                {
                    if (order.CbInOrderList != null && order.CbInOrderList.Count > 0)
                    {
                        foreach (var cbinorder in order.CbInOrderList)
                        {
                            item = new OrdersListItem
                            {
                                OrderID = order.ID,
                                PortfolioYear = order.Portfolio != null
                                    ? order.Portfolio.Year
                                    : null,
                                RegDate = order.RegDate,
                                MonthYear = new DateTime(order.RegDate.Value.Year, order.RegDate.Value.Month, 1),
                                //String.Format("{0} {1}", DateTools.GetMonthInWordsForDate(order.RegDate), DateTools.GetYearInWordsForDate(order.RegDate)),
                                RegNum = order.RegNum,
                                Status = order.Status,
                                Term = order.Term,
                                NoncompReq = order.NoncompReq,
                                Type = order.Type != null
                                    ? order.Type.Trim()
                                    : null,
                                Place = order.Place,

                                ID = cbinorder.ID,

                                Issue = cbinorder.Security != null ? cbinorder.Security.Name : null,
                                Count = cbinorder.Count,
                                Sum =
                                    cbinorder.MaxNomValue != null && cbinorder.MaxNomValue != 0
                                        ? cbinorder.MaxNomValue
                                        : cbinorder.MinValueNKD != null && cbinorder.MinValueNKD != 0
                                            ? cbinorder.MinValueNKD
                                            : cbinorder.SumMoney != null && cbinorder.SumMoney != 0
                                                ? cbinorder.SumMoney
                                                : null,
                                Value = cbinorder.NomValue ?? null,
                                Price = cbinorder.MaxPrice != null && cbinorder.MaxPrice != 0
                                    ? cbinorder.MaxPrice
                                    : cbinorder.MinPrice != null && cbinorder.MinPrice != 0
                                        ? cbinorder.MinPrice
                                        : cbinorder.MaxPriceBay != null && cbinorder.MaxPriceBay != 0
                                            ? cbinorder.MaxPriceBay
                                            : cbinorder.MinPriceSell != null && cbinorder.MinPriceSell != 0
                                                ? cbinorder.MinPriceSell
                                                : cbinorder.FixPriceBay != null && cbinorder.FixPriceBay != 0
                                                    ? cbinorder.FixPriceBay
                                                    : null,
                                IsSecurityListEmpty = false,
                                IsReadOnly = order.IsReadOnly
                            };

                            result.Add(item);
                        }
                    }
                    else
                    {
                        item = new OrdersListItem
                        {
                            OrderID = order.ID,
                            PortfolioYear = order.Portfolio != null
                                ? order.Portfolio.Year
                                : null,
                            RegDate = order.RegDate,
                            MonthYear = new DateTime(order.RegDate.Value.Year, order.RegDate.Value.Month, 1),
                            //String.Format("{0} {1}", DateTools.GetMonthInWordsForDate(order.RegDate), DateTools.GetYearInWordsForDate(order.RegDate)),
                            RegNum = order.RegNum,
                            Status = order.Status,
                            Term = order.Term,
                            NoncompReq = order.NoncompReq,
                            Type = order.Type != null
                                ? order.Type.Trim()
                                : null,
                            Place = order.Place,
                            IsSecurityListEmpty = true
                        };

                        result.Add(item);
                    }
                }

                return result;
            }
        }

        public List<OrdersListItem> GetOrdersListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var result = new List<OrdersListItem>();

                var crit = session.CreateCriteria(typeof(CbOrderHib), "cborder");
                crit.CreateCriteria("Portfolio", "portf", JoinType.InnerJoin);

                crit.CreateCriteria("CbInOrderList", "cbinord", JoinType.LeftOuterJoin)
                    .CreateCriteria("Security", "sec", JoinType.LeftOuterJoin);

                //string s = crit.ToSql();


                var orderList = new List<CbOrderHib>(crit.List().Cast<CbOrderHib>().Distinct());


                OrdersListItem item;

                foreach (var order in orderList)
                {
                    if (order.CbInOrderList != null && order.CbInOrderList.Count > 0)
                    {
                        foreach (var cbinorder in order.CbInOrderList)
                        {
                            item = new OrdersListItem
                            {
                                OrderID = order.ID,
                                PortfolioYear = order.Portfolio != null
                                    ? order.Portfolio.Year
                                    : null,
                                RegDate = order.RegDate,
                                MonthYear = new DateTime(order.RegDate.Value.Year, order.RegDate.Value.Month, 1),
                                //String.Format("{0} {1}", DateTools.GetMonthInWordsForDate(order.RegDate), DateTools.GetYearInWordsForDate(order.RegDate)),
                                RegNum = order.RegNum,
                                Status = order.Status,
                                Term = order.Term,
                                NoncompReq = order.NoncompReq,
                                Type = order.Type != null
                                    ? order.Type.Trim()
                                    : null,
                                Place = order.Place,

                                ID = cbinorder.ID,

                                Issue = cbinorder.Security != null ? cbinorder.Security.Name : null,
                                Count = cbinorder.Count,
                                Sum =
                                    cbinorder.MaxNomValue != null && cbinorder.MaxNomValue != 0
                                        ? cbinorder.MaxNomValue
                                        : cbinorder.MinValueNKD != null && cbinorder.MinValueNKD != 0
                                            ? cbinorder.MinValueNKD
                                            : cbinorder.SumMoney != null && cbinorder.SumMoney != 0
                                                ? cbinorder.SumMoney
                                                : null,
                                Value = cbinorder.NomValue ?? null,
                                Price = cbinorder.MaxPrice != null && cbinorder.MaxPrice != 0
                                    ? cbinorder.MaxPrice
                                    : cbinorder.MinPrice != null && cbinorder.MinPrice != 0
                                        ? cbinorder.MinPrice
                                        : cbinorder.MaxPriceBay != null && cbinorder.MaxPriceBay != 0
                                            ? cbinorder.MaxPriceBay
                                            : cbinorder.MinPriceSell != null && cbinorder.MinPriceSell != 0
                                                ? cbinorder.MinPriceSell
                                                : cbinorder.FixPriceBay != null && cbinorder.FixPriceBay != 0
                                                    ? cbinorder.FixPriceBay
                                                    : null,
                                IsSecurityListEmpty = false
                            };

                            result.Add(item);
                        }
                    }
                    else
                    {
                        item = new OrdersListItem
                        {
                            OrderID = order.ID,
                            PortfolioYear = order.Portfolio != null
                                ? order.Portfolio.Year
                                : null,
                            RegDate = order.RegDate,
                            MonthYear = new DateTime(order.RegDate.Value.Year, order.RegDate.Value.Month, 1),
                            //String.Format("{0} {1}", DateTools.GetMonthInWordsForDate(order.RegDate), DateTools.GetYearInWordsForDate(order.RegDate)),
                            RegNum = order.RegNum,
                            Status = order.Status,
                            Term = order.Term,
                            NoncompReq = order.NoncompReq,
                            Type = order.Type != null
                                ? order.Type.Trim()
                                : null,
                            Place = order.Place,
                            IsSecurityListEmpty = true
                        };

                        result.Add(item);
                    }
                }

                return result;
            }
        }

        public List<LegalEntity> GetBanksListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("SELECT le "
                                               + " FROM LegalEntityHib le "
                                               + " JOIN le.Contragent ca "
                                               + " WHERE TRIM(LOWER(ca.TypeName)) = :type ORDER BY ca.Name")
                    .SetParameter("type", "банк-агент")
                    .List<LegalEntity>();

                return new List<LegalEntity>(list.Cast<LegalEntity>());
            }
        }

        public List<AccBankType> GetAccBankTypes()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("FROM AccBankTypeHib name")
                    .List<AccBankType>();
                return new List<AccBankType>(list);
            }
        }

        public List<Currency> GetCurrencyListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("FROM CurrencyHib name")
                    .List<Currency>();
                return new List<Currency>(list);
            }
        }


        public List<PfrBankAccount> GetPFRBankAccounts(long pBankAgentID, long pPortfolioID, string pType,
            bool includeDeleted)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("SELECT bAcc FROM PfrBankAccountHib bAcc "
                                               + "JOIN bAcc.PFRBankAccountToPortfolios pf_ba "
                                               + "JOIN bAcc.AccountType bAccType "
                                               + "WHERE bAcc.LegalEntityBankID = :p_BA_ID "
                                               + (includeDeleted ? string.Empty : "AND bAcc.StatusID <> -1 ")
                                               + (includeDeleted ? string.Empty : "AND pf_ba.StatusID <> -1 ")
                                               + "AND LOWER(TRIM(bAccType.Name)) = :p_ACC_TYPE "
                                               + "AND pf_ba.PortfolioID = :p_PF_ID"
                    )
                    .SetParameter("p_BA_ID", pBankAgentID)
                    .SetParameter("p_ACC_TYPE", pType.ToLower())
                    .SetParameter("p_PF_ID", pPortfolioID)
                    .List<PfrBankAccount>();

                return list.ToList();
            }
        }

        public List<CbInOrderListItem> GetCbInOrderList(long? orderID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(CbInOrderHib), "cbinorder")
                    .Add(Restrictions.Eq("OrderID", orderID));

                crit.CreateCriteria("Security", "sec", JoinType.InnerJoin)
                    .CreateCriteria("Currency", "currency", JoinType.InnerJoin);

                var cbinordersList = new List<CbInOrderHib>(crit.List().Cast<CbInOrderHib>());

                return cbinordersList.Select(cbinorder => new CbInOrderListItem
                {
                    CbInOrder = cbinorder,
                    Security = cbinorder.Security,
                    Currency = cbinorder.Security != null ? cbinorder.Security.Currency : null
                }).ToList();
            }
        }

        public List<SecuritiesListItem> GetSecuritiesListHib(long? currencyID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(SecurityHib), "sec")
                    .Add(Restrictions.Ge("Status", 0));

                if (currencyID != null && currencyID > 0)
                    crit.Add(Restrictions.Eq("CurrencyID", currencyID));

                crit.CreateCriteria("Kind", "kind", JoinType.InnerJoin);

                crit.CreateCriteria("Currency", "currency", JoinType.InnerJoin);

                var secList = new List<SecurityHib>(crit.List().Cast<SecurityHib>());

                return secList.Select(sec => new SecuritiesListItem
                {
                    Security = sec,
                    Currency = sec.Currency,
                    SecurityKind = sec.Kind
                }).ToList();
            }
        }

        public decimal? GetRateForDateHib(long? currencyID, DateTime? date)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(CursHib), "curs")
                    .Add(Restrictions.Eq("CurrencyID", currencyID))
                    .Add(Restrictions.Eq("Date", date))
                    .AddOrder(Order.Desc("Date"));

                var result = new List<CursHib>(crit.List().Cast<CursHib>());

                return result.Count > 0 ? result.First().Value : null;
            }
        }

        public List<Security> GetSecuritiesListForOrderHib(long? orderID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
									from Security security where security.ID in
									(
										select cbinorder.SecurityID
										from CbInOrderHib cbinorder
										where cbinorder.OrderID = :orderID
									)
									join security.Currency c");
                query.SetParameter("orderID", orderID);
                return query.List<Security>().ToList();
            }
        }

        public List<Security> GetAllSecuritiesListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
					from Security security where security.Status = :status
					join security.Currency c");
                query.SetParameter("status", 0);
                return query.List<Security>().ToList();
            }
        }

        public List<Payment> GetPaymentsListHib(long? securityID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(Payment))
                    .Add(Restrictions.Eq("SecurityId", securityID));

                return new List<Payment>(crit.List<Payment>());
            }
        }

        public List<OrdReport> GetOrdReportsBySecurityPortfolioAndType(long securityID, long portfolioID, string type)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"SELECT rep FROM OrdReport rep
				   JOIN rep.CbInOrder cbinord
				   JOIN cbinord.Order ord
				   WHERE cbinord.SecurityID = :p_sec_id
				   AND ord.PortfolioID = :p_pf_id
				   AND LOWER(TRIM(ord.Type)) = :p_ord_type
				   ORDER BY rep.DateOrder");

                query.SetParameter("p_sec_id", securityID)
                    .SetParameter("p_pf_id", portfolioID)
                    .SetParameter("p_ord_type", type);

                return query.List<OrdReport>().ToList();
            }
        }



        private ICriteria GetBuyOrSaleReportsListFilteredQuery(ISession session, bool forSale, long? orderID,
            DateTime? from, DateTime? to)
        {
            var reports = session.CreateCriteria(typeof(OrdReportHib), "rep")
                .CreateCriteria("CbInOrder", JoinType.InnerJoin)
                .SetFetchMode("CbInOrder", FetchMode.Select);

            reports.CreateCriteria("Security", "sec", JoinType.InnerJoin)
                .SetFetchMode("Security", FetchMode.Select)
                .CreateCriteria("Currency", "currency", JoinType.InnerJoin)
                .SetFetchMode("Currency", FetchMode.Select);

            var orders = reports.CreateCriteria("Order", JoinType.InnerJoin)
                .SetFetchMode("Order", FetchMode.Select);


            var saleType = forSale ? OrderTypeIdentifier.SELLING + "%" : OrderTypeIdentifier.BUYING + "%";
            orders.Add(Restrictions.Like("Type", saleType));

            if (orderID.HasValue)
                orders.Add(Restrictions.Eq("ID", orderID.Value));

            orders.CreateCriteria("Portfolio", JoinType.InnerJoin)
                .SetFetchMode("Portfolio", FetchMode.Select);
            orders.Add(Restrictions.Eq("rep.IsUnlinked", 0));
            if (from.HasValue)
                orders.Add(Restrictions.Ge("rep.DateOrder", from.Value));
            if (to.HasValue)
                orders.Add(Restrictions.Le("rep.DateOrder", to.Value));
            return orders;
        }

        public long GetCountBuyOrSaleReportsListFilteredHib(bool forSale, long? orderID, DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var orders = GetBuyOrSaleReportsListFilteredQuery(session, forSale, orderID, from, to);
                return orders.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
            }
        }


        public List<TempAllocationListItem> GetTempAllocationBuyOrSaleListFilteredByPageHib(bool forSale, long? orderID,
            int startIndex, DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var orders = GetBuyOrSaleReportsListFilteredQuery(session, forSale, orderID, from, to);

                var list =
                    GetListResultBuyOrSaleReportsListItem(
                        orders.SetFirstResult(startIndex).SetMaxResults(PageSize).List<OrdReportHib>());

                return list.ConvertAll(
                    x =>
                    {
                        //int buyMult = OrderTypeIdentifier.IsBuyOrder(x.OrderType) ? -1 : 1;                     
                        decimal? sum = x.Sum;
                        return new TempAllocationListItem
                        {
                            ID = x.ID,
                            PortfolioYear = x.PortfolioYear,
                            Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB,
                            //OrderType = x.OrderType,
                            Year = x.DateOrder == null ? null : (int?)x.DateOrder.Value.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(x.DateOrder),
                            Quarter = DateTools.GetQarterYearInWordsForDate(x.DateOrder),
                            Month = DateTools.GetMonthInWordsForDate(x.DateOrder),
                            Date = x.DateOrder,
                            OrderRegNum = x.RegNum,
                            CurrencyName = x.CurrencyName,
                            CurrSumm = x.CurrencyIsRubles ? null : sum,
                            RubSumm = x.CurrencyIsRubles ? sum : x.RubSum,
                            RubNKD = x.RubNKD,
                            RubWithoutNKD = x.CurrencyIsRubles ? x.SumWithoutNKD : x.SumWithoutNKD * (x.Rate ?? 1),
                            Comment = x.Comment,
                            DateDI = x.DateDI,
                            OperationType =
                                forSale
                                    ? TempAllocationListItem.enOperationType.PaperSell
                                    : TempAllocationListItem.enOperationType.PaperBuy
                        };
                    }
                    );
            }

        }

        public long GetCountBuyOrSaleReportsListHib(bool isSale, long? orderID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reports = session.CreateCriteria(typeof(OrdReportHib), "rep")
                    .CreateCriteria("CbInOrder", JoinType.InnerJoin)
                    .SetFetchMode("CbInOrder", FetchMode.Select);

                reports.CreateCriteria("Security", "sec", JoinType.InnerJoin)
                    .SetFetchMode("Security", FetchMode.Select)
                    .CreateCriteria("Currency", "currency", JoinType.InnerJoin)
                    .SetFetchMode("Currency", FetchMode.Select);

                var orders = reports.CreateCriteria("Order", JoinType.InnerJoin)
                    .SetFetchMode("Order", FetchMode.Select);


                var saleType = isSale ? OrderTypeIdentifier.SELLING + "%" : OrderTypeIdentifier.BUYING + "%";
                orders.Add(Restrictions.Like("Type", saleType));

                if (orderID.HasValue)
                    orders.Add(Restrictions.Eq("ID", orderID.Value));

                orders.CreateCriteria("Portfolio", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Select);

                return reports.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
            }
        }

        private List<BuyOrSaleReportsListItem> GetListResultBuyOrSaleReportsListItem(IList<OrdReportHib> list)
        {
            var result = new List<BuyOrSaleReportsListItem>();
            var secIds = list.Select(item => item.CbInOrder.Security.ID).Cast<object>().ToArray();

            var payments = GetListByPropertyCondition(typeof(Payment).FullName,
                "SecurityId", secIds, "in")
                .Cast<Payment>()
                .GroupBy(item => item.SecurityId ?? 0)
                .ToDictionary(item => item.Key, item => item.ToList());

            foreach (var report in list)
            {
                var paymentsList = new List<Payment>();
                if (payments.ContainsKey(report.CbInOrder.Security.ID))
                    paymentsList = payments[report.CbInOrder.Security.ID];

                bool currencyIsRubles = CurrencyIdentifier.IsRUB(report.CbInOrder.Security.Currency.Name);
                DateTime? dt = currencyIsRubles ? report.DateOrder : report.DateDEPO;

                decimal sumNom = 0;
                decimal sumWithoutNkd = 0;
                decimal nkd = 0;
                decimal? rate = 1;

                decimal nfn = 0;
                if (report.CbInOrder.Security.NomValue.HasValue && report.Count.HasValue)
                {
                    nfn = OrderReportsHelper.CalcNFN(paymentsList, dt, report.CbInOrder.Security.NomValue.Value);
                }

                sumNom = ((report.OneNomValue ?? nfn) * report.Count) ?? 0;

                if (report.Price.HasValue)
                    sumWithoutNkd = Math.Round(sumNom * report.Price.Value / 100m, 2); //Округляем как в лотусе

                if (report.DateOrder.HasValue && report.DateDEPO.HasValue && paymentsList != null)
                {
                    if (!currencyIsRubles)
                    {
                        var c = report.CbInOrder.Security.Currency.Curses.FirstOrDefault(curs => curs.Date.Value.Date == dt);
                        rate = c == null ? 1 : c.Value;
                    }
                }

                if (report.OneNKD != null)
                    nkd = Math.Round((report.OneNKD * report.Count) ?? 0, 2, MidpointRounding.AwayFromZero);
                //Округление как в лотусе
                else
                    nkd = report.NKD ?? 0;

                if (nkd < 0)
                    nkd = 0;


                result.Add(new BuyOrSaleReportsListItem
                {
                    ID = report.ID,
                    PortfolioYear = report.CbInOrder.Order.Portfolio.Year,
                    RegNum = report.CbInOrder.Order.RegNum,
                    SecurityID = report.CbInOrder.Security.ID,
                    SecurityName = report.CbInOrder.Security.Name,
                    CurrencyName = report.CbInOrder.Security.Currency.Name,
                    DateOrder = report.DateOrder,
                    DateDEPO = report.DateDEPO,
                    Count = report.Count ?? 0,
                    Price = report.Price ?? 0,
                    Yield = report.Yield ?? 0,
                    RubNKD = nkd * (rate ?? 1),
                    SumNom = sumNom,
                    SumWithoutNKD = report.CustomSumWithoutNKD ?? sumWithoutNkd,
                    Sum = report.CustomSumm ?? Math.Round(sumWithoutNkd + nkd, 2, MidpointRounding.AwayFromZero),
                    RubSum =
                        report.CustomSummRUR ??
                        (Math.Round(sumWithoutNkd + nkd, 2, MidpointRounding.AwayFromZero) * (rate ?? 1)),
                    OrderType = report.CbInOrder.Order.Type,
                    CurrencyIsRubles = currencyIsRubles,
                    Rate = rate,
                    Comment = report.CbInOrder.Order.Comment,
                    DateDI = report.CbInOrder.DateDI
                });
            }
            return result;
        }

        public List<BuyOrSaleReportsListItem> GetBuyOrSaleReportsListByPageHib(bool isSale, long? orderID,
            int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reports = session.CreateCriteria(typeof(OrdReportHib), "rep")
                    .AddOrder(Order.Asc("DateOrder"))
                    .AddOrder(Order.Asc("DateDEPO"))
                    .CreateCriteria("CbInOrder", JoinType.InnerJoin)
                    .SetFetchMode("CbInOrder", FetchMode.Select);

                reports.CreateCriteria("Security", "sec", JoinType.InnerJoin)
                    .SetFetchMode("Security", FetchMode.Select)
                    .CreateCriteria("Currency", "currency", JoinType.InnerJoin)
                    .SetFetchMode("Currency", FetchMode.Select);

                var orders = reports.CreateCriteria("Order", JoinType.InnerJoin)
                    .SetFetchMode("Order", FetchMode.Select);

                string saleType = isSale ? OrderTypeIdentifier.SELLING + "%" : OrderTypeIdentifier.BUYING + "%";
                orders.Add(Restrictions.Like("Type", saleType));

                if (orderID.HasValue)
                    orders.Add(Restrictions.Eq("ID", orderID.Value));

                orders.CreateCriteria("Portfolio", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Select);


                return
                    GetListResultBuyOrSaleReportsListItem(
                        reports.SetFirstResult(startIndex).SetMaxResults(PageSize).List<OrdReportHib>());
            }
        }

        public List<BuyOrSaleReportsListItem> GetBuyOrSaleReportsListHib(bool isSale, long? orderID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reports = session.CreateCriteria(typeof(OrdReportHib), "rep")
                    .AddOrder(Order.Asc("DateOrder"))
                    .AddOrder(Order.Asc("DateDEPO"))
                    .CreateCriteria("CbInOrder", JoinType.InnerJoin)
                    .SetFetchMode("CbInOrder", FetchMode.Select);

                reports.CreateCriteria("Security", "sec", JoinType.InnerJoin)
                    .SetFetchMode("Security", FetchMode.Select)
                    .CreateCriteria("Currency", "currency", JoinType.InnerJoin)
                    .SetFetchMode("Currency", FetchMode.Select);

                var orders = reports.CreateCriteria("Order", JoinType.InnerJoin)
                    .SetFetchMode("Order", FetchMode.Select);


                string saleType = isSale ? OrderTypeIdentifier.SELLING + "%" : OrderTypeIdentifier.BUYING + "%";
                orders.Add(Restrictions.Like("Type", saleType));

                if (orderID.HasValue)
                    orders.Add(Restrictions.Eq("ID", orderID.Value));

                orders.CreateCriteria("Portfolio", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Select);

                return GetListResultBuyOrSaleReportsListItem(reports.List<OrdReportHib>());
            }


        }

        public long[] GetOrdReportsIDsForRecalc()
        {
            using (var session = DataAccessSystem.OpenSession())
                return session.CreateQuery("SELECT r.ID FROM OrdReportHib r").List().Cast<long>().ToArray();
        }

        public void RecalcNKDForReport(long reportID)
        {
            using (ISession session = DataAccessSystem.OpenSession())
            {
                var report = session.Get<OrdReportHib>(reportID);

                if (report.CbInOrder == null)
                    return;
                if (report.CbInOrder.Security == null)
                    return;

                bool currencyIsRubles = CurrencyIdentifier.IsRUB(report.CbInOrder.Security.Currency.Name);
                var paymentsList = report.CbInOrder.Security.Payments.ToList();
                if (report.DateOrder.HasValue && report.DateDEPO.HasValue && paymentsList != null)
                {
                    var dt = currencyIsRubles ? report.DateOrder.Value : report.DateDEPO.Value;
                    var closestPayment = OrderReportsHelper.GetClosestPayment(dt, paymentsList);

                    decimal nkdPerOneObligation = 0;
                    if (closestPayment != null)
                    {

                        if (closestPayment.CouponPeriod.HasValue) //stub ClosestPayment.CouponPeriod is NULL
                        {
                            nkdPerOneObligation = (currencyIsRubles
                                ? OrderReportsHelper.CalcNKDForRubles(dt, closestPayment)
                                : OrderReportsHelper.CalcNKDForDollars(dt, closestPayment));
                        }
                        else
                        {
                            nkdPerOneObligation = 0;
                            LogManager.Log("Attention! " + "ClosestPayment.CouponPeriod is NULL! " + $" Payment.ID={closestPayment.ID} ");
                        }




                    }
                    if (nkdPerOneObligation < 0)
                        report.NKD = 0;
                    else
                        report.NKD = Math.Round(nkdPerOneObligation, 2) * report.Count ?? 0;

                    /* var nfn = OrderReportsHelper.CalcNFN(PaymentsList, dt, report.CbInOrder.Security.NomValue);
					 var SumNom = nfn * report.Count;
					 var SumWithoutNKD = SumNom * report.Price / 100m;

					 report.SumBuy = SumWithoutNKD;*/
                    Save(typeof(OrdReportHib).FullName, report);
                }
            }
        }

#endregion

        public IList<PaymentHistory> GetPaymentHistoryList(long[] depclaimOfferIds)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return
                    session.CreateCriteria(typeof(PaymentHistory).FullName)
                        .Add(Restrictions.In("OfferPfrBankAccountSumId", depclaimOfferIds))
                        .List()
                        .Cast<PaymentHistory>()
                        .ToList();
            }
        }

        public void DeleteDepositPayment(long paymentID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var payment = session.Get<PaymentHistoryHib>(paymentID);

                    var dep = payment.OfferPfrBankAccountSum.Offer.Deposits;
                    session.Delete(payment);
                    session.Flush();
                    foreach (var d in dep)
                    {
                        UpdateDepositStatus(session, d.ID);
                    }

                    transaction.Commit();
                }
            }
        }

        [Obsolete("Replaced by GetDepositsListByPropertyHib2")]
        public List<DepositListItem> GetDepositsListByPropertyHib(string propertyName, object status)
        {
            IList dL = status == null
                ? GetList(typeof(Deposit).FullName)
                : GetListByProperty(typeof(Deposit).FullName, propertyName, status);
            var res = new List<DepositListItem>();

            foreach (Deposit d in dL)
            {
                DepClaimOffer dco = GetByID<DepClaimOffer>(d.DepClaimOfferId.Value);
                DepClaim2 dc2 = GetByID<DepClaim2>(dco.DepClaimID);
                Element e = d.CommentID == null ? null : GetByID<Element>(d.CommentID);

                var x = new DepositListItem(d, dco, dc2, e);
                res.Add(x);
            }

            return res;
        }

        public List<DepositListItem> GetDepositsListByPropertyHib2(string propertyName, object status)
        {
            return SessionWrapper(session =>
            {
                var res = new List<DepositListItem>();
                var results = session.CreateQuery($@"select d.ID, d.Status, d.ReturnDateActual, dco.Number, dc2, e.Name, 
                                                     (select sum(coalesce(pas.PlannedSum, 0)) from OfferPfrBankAccountSum pas where pas.OfferId = dco.ID)
                                                     from Deposit d
                                                     join d.DepClaimOffer dco
                                                     join dco.DepClaim dc2
                                                     left join d.Comment e
                                                     {(status == null ? "" : $" where d.{propertyName} = {status} ")}")
                    .List<object[]>()
                    .ToList();
                results.ForEach(a =>
                {
                    res.Add(new DepositListItem((long)a[0], (long?)a[1], (DateTime?)a[2], (long)a[3], (DepClaim2)a[4], (string)a[5], (decimal?)a[6]));
                });
                return res;
            });

            /*  IList dL = status == null
                  ? GetList(typeof(Deposit).FullName)
                  : GetListByProperty(typeof(Deposit).FullName, propertyName, status);
              var res = new List<DepositListItem>();

              foreach (Deposit d in dL)
              {
                  DepClaimOffer dco = GetByID<DepClaimOffer>(d.DepClaimOfferId.Value);
                  DepClaim2 dc2 = GetByID<DepClaim2>(dco.DepClaimID);
                  Element e = d.CommentID == null ? null : GetByID<Element>(d.CommentID);

                  var x = new DepositListItem(d, dco, dc2, e);
                  res.Add(x);
              }

              return res;*/
        }

        internal decimal GetDepositsSumDiffForROPS()
        {
            return SessionWrapper(session =>
            {
                decimal sum = 0;
                var passedList = new[] { new { Id = 0L, IsReturn = false, IsPercent = false } }.ToList();
                session.CreateQuery(@"
                            select 	
	                            s.Sum as LocationVolume,
                                ph.ForPercent as IsReturn,
                                s.ID as summID,
                                ph.Sum as ReturnVolume
                            from d in Deposit
                            join d.DepClaimOffer o
                            join o.OfferPfrBankAccountSum s
                            join s.Portfolio p
                            left join s.PaymentHistory ph
                            where s.StatusID>0 and p.TypeID = :typeId
                            ")
                    .SetParameter("typeId", (long)Portfolio.Types.ROPS)
                    .List<object[]>()
                    .ForEach(a =>
                    {
                        bool? isReturn = ((byte?)a[1]).HasValue ? (bool?)(((byte?)a[1]).Value == 0) : null;
                        var summId = (long)a[2];
                        var sumPlus = (decimal?)a[0]; //LocationVolume
                        var sumMinus = (decimal?)a[3]; //ReturnVolume

                        //добавление записи без возврата
                        if (!isReturn.HasValue)
                        {
                            passedList.Add(new { Id = summId, IsReturn = false, IsPercent = false });
                            sum += sumPlus ?? 0;
                        }
                        //если есть записи возврата процентов или тела
                        else
                        {
                            //Добавлеяем размещение, если ещё не было.
                            var ret = passedList.FirstOrDefault(r => r.Id == summId && !r.IsReturn && !r.IsPercent);
                            //при наличии возвратов в выборе может быть 2 записи
                            if (ret == null)
                            {
                                passedList.Add(new { Id = summId, IsReturn = false, IsPercent = false });
                                sum += sumPlus ?? 0;
                            }

                            // добавление записи возврат суммы
                            if (isReturn.Value) //не процент, т.к. true при 0
                            {
                                passedList.Add(new { Id = summId, IsReturn = true, IsPercent = false });
                                sum -= sumMinus ?? 0;
                            }
                        }
                    });

                //Balance_Extra
                {
                    session.CreateQuery(@"
                            select
                                x.Amount,
                                x.OperationName
                            from x in BalanceExtra
                            join x.Portfolio p
                            where x.OperationName in (:types) and p.TypeID = :typeId")
                        .SetParameterList("types",
                            new List<string>
                            {
                                BalanceExtraIdentifier.DEPOSIT_SETLE,
                                BalanceExtraIdentifier.DEPOSIT_RETURN
                            })
                        .SetParameter("typeId", (long)Portfolio.Types.ROPS)
                        .List<object[]>()
                        .ForEach(p =>
                        {
                            var summ = (decimal?)p[0];
                            switch ((string)p[1])
                            {
                                case BalanceExtraIdentifier.DEPOSIT_SETLE:
                                    sum += summ ?? 0;
                                    break;
                                case BalanceExtraIdentifier.DEPOSIT_RETURN:
                                    sum -= summ ?? 0;
                                    break;
                            }
                        });
                }
                return sum;
            });
        }

        public List<DepositListItem> GetDepositsListByPortfolioHib()
        {
            try
            {
                var res2 = new List<DepositListItem>();
                using (var session = DataAccessSystem.OpenSession())
                {
                    {
                        var query = session.CreateQuery(@"
                            select 
	                            d.ID as ID,		
	                            p.ID as PortfolioID,	
	                            p.Year as PortfolioName, 
	                            ba.AccountNumber as AccNum,		
	                            d.Status as Status, 
	                            dc2.SettleDate as LocationDate,		
	                            b.FormalizedName as BankName,	
	                            o.Number as RegNum,		
	                            s.Sum as LocationVolume,
	                            dc2.Rate as DepositYield, 
	                            dc2.ReturnDate as ReturnDate, 
	                            c.Name as Comment,

                            dc2.Payment, dc2.Amount, ph.ForPercent, s.ID, ph.Sum

                            from d in Deposit
                            join d.DepClaimOffer o
                            join o.DepClaim dc2 
                            join o.OfferPfrBankAccountSum s
                            join s.Portfolio p
                            left join d.Comment c
                            join s.PfrBankAccount ba
                            left join ba.LegalEntity b
                            left join s.PaymentHistory ph
                            where s.StatusID>0
                            ");

                        foreach (var p in query.List<object[]>())
                        {
                            var x = new DepositListItem
                            {
                                ID = (long)p[0],
                                PortfolioID = (long?)p[1],
                                PortfolioName = (string)p[2],
                                AccNum = (string)p[3],
                                Status = (long?)p[4],

                                LocationDate = (DateTime?)p[5],
                                BankName = (string)p[6],
                                RegNum = ((long)p[7]).ToString(),
                                LocationVolume = (decimal?)p[8],

                                DepositYield = (decimal?)p[9], //dc2.Rate as DepositYield
                                ReturnDate = (DateTime?)p[10], //dc2.ReturnDate as ReturnDate
                                Comment = (string)p[11], //c.Name as Comment
                                SummID = (long)p[15], //s.ID
                                ReturnSum = (decimal?)p[16] //ph.Sum
                            };

                            decimal dc2Payment = (decimal)p[12];
                            decimal dc2Amount = (decimal)p[13];

                            bool? isReturn = ((byte?)p[14]).HasValue ? (bool?)(((byte?)p[14]).Value == 0) : null;



                            x.StatusName = x.Status == null
                                ? string.Empty
                                : DepositIdentifier.GetDepositStatusText((long)x.Status);
                            x.InterestVolume = dc2Amount == 0 ? 0 : (dc2Payment / dc2Amount) * x.LocationVolume;
                            // Важно!!! порядок операций не меняем, иначе возможно переполнение
                            x.InterestVolume = Math.Round(x.InterestVolume.Value, 2);

                            x.TermLocation = x.ReturnDate.Value.Date.Subtract(x.LocationDate.Value.Date).Days;

                            //добавление записи без возврата
                            if (!isReturn.HasValue)
                            {
                                var depo = x.Copy();
                                depo.IsReturn = false;
                                depo.IsPercent = false;
                                res2.Add(depo);
                            }
                            //если есть записи возврата процентов или тела
                            else
                            {
                                //Добавлеяем размещение, если ещё не было.
                                var ret = res2.FirstOrDefault(r => r.SummID == x.SummID && !r.IsReturn && !r.IsPercent);
                                //при наличии возвратов в выборе может быть 2 записи
                                if (ret == null)
                                {
                                    var depo = x.Copy();
                                    depo.IsReturn = false;
                                    depo.IsPercent = false;
                                    res2.Add(depo);
                                }

                                // добавление записи возврат суммы или процента
                                x.IsReturn = true;
                                x.IsPercent = !isReturn.Value;
                                res2.Add(x);
                            }
                        }
                    }

                    //Balance_Extra
                    {
                        var query = session.CreateQuery(@"
                            select
                            x.ID, p.ID, p.Year, ba.AccountNumber,
                            x.OperationDate, b.FormalizedName, x.Amount,
                            x.ReturnDate, x.Comment, x.RegNumber,
                            x.OperationName

                            from x in BalanceExtra
                            join x.Portfolio p
                            join x.PfrBankAccount ba
                            left join ba.LegalEntity b
                            where  x.OperationName in (:types) ")
                            .SetParameterList("types",
                                new List<string>
                                {
                                    BalanceExtraIdentifier.DEPOSIT_SETLE,
                                    BalanceExtraIdentifier.DEPOSIT_RETURN
                                });

                        foreach (var p in query.List<object[]>())
                        {
                            var x = new DepositListItem
                            {
                                ID = (long)p[0],
                                PortfolioID = (long?)p[1],
                                PortfolioName = (string)p[2],
                                AccNum = (string)p[3],
                                Status = 2, //Returned

                                LocationDate = (DateTime?)p[4],
                                BankName = (string)p[5],
                                RegNum = Convert.ToString(p[9]),
                                LocationVolume = (decimal?)p[6],
                                ReturnSum = (decimal?)p[6],

                                DepositYield = 0,
                                ReturnDate = (DateTime?)p[7],
                                Comment = (string)p[8],
                                IsExtra = true
                            };

                            var type = (string)p[10];
                            switch (type)
                            {
                                case BalanceExtraIdentifier.DEPOSIT_SETLE:
                                    x.IsReturn = false;
                                    break;
                                case BalanceExtraIdentifier.DEPOSIT_RETURN:
                                    x.IsReturn = true;
                                    x.ReturnDate = x.LocationDate;
                                    // x.LocationDate = null;
                                    break;
                            }

                            x.StatusName = x.Status == null
                                ? string.Empty
                                : DepositIdentifier.GetDepositStatusText((long)x.Status);
                            x.InterestVolume = null;

                            if (x.ReturnDate.HasValue)
                                x.TermLocation = x.ReturnDate.Value.Date.Subtract(x.LocationDate.Value.Date).Days;
                            res2.Add(x);
                        }
                    }
                }

                return res2;
            }
            catch (Exception ex)
            {
                LogExceptionForHib(ex);
                throw;
            }
        }

        public List<AllocationRequestListItem> GetAllocationRequestListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return
                    session.CreateCriteria(typeof(AllocationRequestListItem).FullName)
                        .List()
                        .Cast<AllocationRequestListItem>()
                        .ToList();
            }
        }

#region GetTempAllocationListHib

        public List<TempAllocationListItem> GetTempAllocationSellPaperListByPageHib(int startIndex)
        {
            return GetBuyOrSaleReportsListByPageHib(true, null, startIndex).ConvertAll(
                x =>
                {
                    decimal? sum = OrderTypeIdentifier.IsBuyOrder(x.OrderType) ? -x.Sum : x.Sum;
                    return new TempAllocationListItem
                    {
                        ID = x.ID,
                        PortfolioYear = x.PortfolioYear,
                        Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB,
                        //OrderType = x.OrderType,
                        Year = x.DateOrder == null ? null : (int?)x.DateOrder.Value.Year,
                        HalfYear = DateTools.GetHalfYearInWordsForDate(x.DateOrder),
                        Quarter = DateTools.GetQarterYearInWordsForDate(x.DateOrder),
                        Month = DateTools.GetMonthInWordsForDate(x.DateOrder),
                        Date = x.DateOrder,
                        OrderRegNum = x.RegNum,
                        CurrencyName = x.CurrencyName,
                        CurrSumm = x.CurrencyIsRubles ? null : sum,
                        RubSumm = x.CurrencyIsRubles ? sum : x.RubSum,
                        Comment = x.Comment,
                        DateDI = x.DateDI,
                        OperationType = TempAllocationListItem.enOperationType.PaperSell
                    };
                }
                );

        }

        public List<TempAllocationListItem> GetTempAllocationBuyPaperListByPageHib(int startIndex)
        {
            return GetBuyOrSaleReportsListByPageHib(false, null, startIndex).ConvertAll(
                x =>
                {
                    decimal? sum = OrderTypeIdentifier.IsBuyOrder(x.OrderType) ? -x.Sum : x.Sum;
                    return new TempAllocationListItem
                    {
                        ID = x.ID,
                        PortfolioYear = x.PortfolioYear,
                        Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB,
                        //OrderType = x.OrderType,
                        Year =
                            x.DateOrder == null ? null : (int?)x.DateOrder.Value.Year,
                        HalfYear = DateTools.GetHalfYearInWordsForDate(x.DateOrder),
                        Quarter = DateTools.GetQarterYearInWordsForDate(x.DateOrder),
                        Month = DateTools.GetMonthInWordsForDate(x.DateOrder),
                        Date = x.DateOrder,
                        OrderRegNum = x.RegNum,
                        CurrencyName = x.CurrencyName,
                        CurrSumm = x.CurrencyIsRubles ? null : sum,
                        RubSumm = x.CurrencyIsRubles ? sum : x.RubSum,
                        Comment = x.Comment,
                        DateDI = x.DateDI,
                        OperationType =
                            TempAllocationListItem.enOperationType.PaperBuy
                    };
                });
        }


        public long GetCountTempAllocateDepositListFilteredHib(DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateQuery(@"
                            select count(*) 
                            from d in Deposit
                            join d.DepClaimOffer o
                            join o.DepClaim dc2 
                            join o.OfferPfrBankAccountSum s
                            where s.StatusID > 0 AND (:from is null OR dc2.SettleDate >= :from) AND (:to is null OR dc2.SettleDate <= :to)
                            ")
                    .SetParameter("from", from)
                    .SetParameter("to", to)
                    .UniqueResult<long>();
            }
        }


        public List<TempAllocationListItem> GetTempAllocateDepositListFilteredByPageHib(int startIndex, DateTime? from,
            DateTime? to)
        {
            //Allocate Deposit
            var deposits = new List<Deposit2>();
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
                        select d.ID, d.Status, p.Year, dc2.SettleDate, dc2.ReturnDate, o.Number, s.Sum, d.DateDI
                        from d in Deposit
                        join d.DepClaimOffer o
                        join o.DepClaim dc2 
                        join o.OfferPfrBankAccountSum s
                        join s.Portfolio p
                        where s.StatusID>0 AND (:from is null OR dc2.SettleDate >= :from) AND (:to is null OR dc2.SettleDate <= :to)
                        order by d.ID")
                    .SetParameter("from", from)
                    .SetParameter("to", to);
                deposits.AddRange(query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>().Select(p =>
                {
                    var sIndex = p.Length == 9 ? 1 : 0;
                    return new Deposit2
                    {
                        ID = (long) p[sIndex + 0],
                        Status = (long?) p[sIndex + 1],
                        Year = (string) p[sIndex + 2],
                        LocationDate = (DateTime?) p[sIndex + 3],
                        ReturnDate = (DateTime?) p[sIndex + 4],
                        RegNum = p[sIndex + 5].ToString(),
                        LocationVolume = (decimal?) p[sIndex + 6],
                        DateDI = (DateTime?) p[sIndex + 7]
                    };
                }));
            }

            //Allocate Deposit
            return deposits.ConvertAll(
                d =>
                {
                    //const bool isAllocated = true;
                    var date = d.LocationDate;
                    return new TempAllocationListItem
                    {
                        ID = d.ID,
                        PortfolioYear = d.Year,
                        Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,

                        Year = date == null ? null : (int?)date.Value.Year,
                        HalfYear = DateTools.GetHalfYearInWordsForDate(date),
                        Quarter = DateTools.GetQarterYearInWordsForDate(date),
                        Month = DateTools.GetMonthInWordsForDate(date),
                        Date = date,
                        OrderRegNum = (d.RegNum ?? "").ToString(),
                        CurrencyName = CurrencyIdentifier.RUBLES,
                        CurrSumm = null,
                        RubSumm = d.LocationVolume,
                        DateDI = d.DateDI,
                        OperationType = TempAllocationListItem.enOperationType.DepositAllocate
                    };
                }
                );
        }


        public long GetCountTempAllocateDepositListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateQuery(@"
                    select count(*) 
                    from d in Deposit
                    join d.DepClaimOffer o
                    join o.DepClaim dc2 
                    join o.OfferPfrBankAccountSum s
                    where s.StatusID>0
                    ").UniqueResult<long>();
            }
        }

        public List<TempAllocationListItem> GetTempAllocateDepositListByPageHib(int startIndex)
        {
            //Allocate Deposit
            var deposits = new List<Deposit2>();
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
                    select d.ID, d.Status, p.Year, dc2.SettleDate, dc2.ReturnDate, o.Number, s.Sum, d.DateDI
                    from d in Deposit
                    join d.DepClaimOffer o
                    join o.DepClaim dc2 
                    join o.OfferPfrBankAccountSum s
                    join s.Portfolio p
                    where s.StatusID>0
                    order by d.ID");

                deposits.AddRange(query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>().Select(p => new Deposit2
                {
                    ID = (long)p[0],
                    Status = (long?)p[1],
                    Year = (string)p[2],
                    LocationDate = (DateTime?)p[3],
                    ReturnDate = (DateTime?)p[4],
                    RegNum = p[5].ToString(),
                    LocationVolume = (decimal?)p[6],
                    DateDI = (DateTime?)p[7]
                }));
            }

            //Allocate Deposit
            return deposits.ConvertAll(
                d =>
                {
                    const bool isAllocated = true;
                    DateTime? date = isAllocated ? d.LocationDate : d.ReturnDate;
                    return new TempAllocationListItem
                    {
                        ID = d.ID,
                        PortfolioYear = d.Year,
                        Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,
                        //OrderType = isAllocated ? DepositIdentifier.Placed : DepositIdentifier.Returned,
                        Year = date == null ? null : (int?)date.Value.Year,
                        HalfYear = DateTools.GetHalfYearInWordsForDate(date),
                        Quarter = DateTools.GetQarterYearInWordsForDate(date),
                        Month = DateTools.GetMonthInWordsForDate(date),
                        Date = date,
                        OrderRegNum = (d.RegNum ?? "").ToString(),
                        CurrencyName = CurrencyIdentifier.RUBLES,
                        CurrSumm = null,
                        RubSumm = isAllocated ? -d.LocationVolume : d.LocationVolume,
                        DateDI = d.DateDI,
                        OperationType = TempAllocationListItem.enOperationType.DepositAllocate
                    };
                }
                );
        }



        public long GetCountTempAllocateReturnDepositListFilteredHib(DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateQuery(@"
                    select count(*) 
                    from d in Deposit
                    join d.DepClaimOffer o
                    join o.DepClaim dc2 
                    join o.OfferPfrBankAccountSum s
                    join s.PaymentHistory ph
                    where s.StatusID>0
                    and ph.ForPercent = 0 AND (:from is null OR ph.OrderDate >= :from) AND (:to is null OR ph.OrderDate <= :to)
                    ").SetParameter("from", from)
                    .SetParameter("to", to)
                    .UniqueResult<long>();
            }
        }


        public List<TempAllocationListItem> GetTempAllocateReturnDepositListFilteredHib(int startIndex, DateTime? from,
            DateTime? to)
        {
            //Return Deposit
            var rL = new List<Deposit2>();
            using (var session = DataAccessSystem.OpenSession())
            {
                // ph.Date -> ph.OrderDate
                // Дата ДИ -> Дата
                // в соотв. с задачей http://jira.dob.datateh.ru/browse/DOKIPIV-1325
                var query = session.CreateQuery(@"
                            select d.ID, d.Status, p.Year, dc2.SettleDate, dc2.ReturnDate, o.Number, ph.Sum, ph.Date
                            from d in Deposit
                            join d.DepClaimOffer o
                            join o.DepClaim dc2 
                            join o.OfferPfrBankAccountSum s
                            join s.Portfolio p
                            join s.PaymentHistory ph
                            where s.StatusID>0
                            and ph.ForPercent = 0 AND (:from is null OR ph.OrderDate >= :from) AND (:to is null OR ph.OrderDate <= :to)
                            order by d.ID")
                    .SetParameter("from", from)
                    .SetParameter("to", to);

                rL.AddRange(query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>().Select(p => new Deposit2
                {
                    ID = (long)p[0],
                    Status = (long?)p[1],
                    Year = (string)p[2],
                    LocationDate = (DateTime?)p[3],
                    ReturnDate = (DateTime?)p[4],
                    RegNum = p[5].ToString(),
                    LocationVolume = (decimal?)p[6],
                    DateDI = (DateTime?)p[7]
                }));
            }

            //Return Deposit
            return
                rL.ConvertAll(
                    d =>
                    {
                        const bool isAllocated = false;
                        var date = isAllocated ? d.LocationDate : d.ReturnDate;
                        return new TempAllocationListItem
                        {
                            ID = d.ID,
                            PortfolioYear = d.Year,
                            Kind =
                                TempAllocationListItem
                                    .TEMP_ALLOCATION_KIND_DEPOSIT,
                            //OrderType =
                            //    isAllocated
                            //        ? DepositIdentifier.Placed
                            //        : DepositIdentifier.Returned,
                            Year = date == null ? null : (int?)date.Value.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(date),
                            Quarter = DateTools.GetQarterYearInWordsForDate(date),
                            Month = DateTools.GetMonthInWordsForDate(date),
                            Date = date,
                            OrderRegNum = (d.RegNum ?? "").ToString(),
                            CurrencyName = CurrencyIdentifier.RUBLES,
                            CurrSumm = null,
                            RubSumm = d.LocationVolume,
                            DateDI = d.DateDI,
                            OperationType = TempAllocationListItem.enOperationType.DepositReturn
                        };
                    });
        }

        public long GetCountTempAllocateReturnDepositListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateQuery(@"
                    select count(*) 
                    from d in Deposit
                    join d.DepClaimOffer o
                    join o.DepClaim dc2 
                    join o.OfferPfrBankAccountSum s
                    join s.Portfolio p
                    join s.PaymentHistory ph
                    where s.StatusID>0
                    and ph.ForPercent = 0
                    ").UniqueResult<long>();
            }
        }

        public List<TempAllocationListItem> GetTempAllocateReturnDepositListHib(int startIndex)
        {
            //Return Deposit
            var rL = new List<Deposit2>();
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
                    select d.ID, d.Status, p.Year, dc2.SettleDate, ph.Date, o.Number, ph.Sum, ph.Date
                    from d in Deposit
                    join d.DepClaimOffer o
                    join o.DepClaim dc2 
                    join o.OfferPfrBankAccountSum s
                    join s.Portfolio p
                    join s.PaymentHistory ph
                    where s.StatusID>0
                    and ph.ForPercent = 0
                    order by d.ID");

                rL.AddRange(query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>().Select(p => new Deposit2
                {
                    ID = (long)p[0],
                    Status = (long?)p[1],
                    Year = (string)p[2],
                    LocationDate = (DateTime?)p[3],
                    ReturnDate = (DateTime?)p[4],
                    RegNum = p[5].ToString(),
                    LocationVolume = (decimal?)p[6],
                    DateDI = (DateTime?)p[7]
                }));
            }

            //Return Deposit
            return
                rL.ConvertAll(
                    d =>
                    {
                        bool isAllocated = false;
                        DateTime? date = isAllocated ? d.LocationDate : d.ReturnDate;
                        return new TempAllocationListItem
                        {
                            ID = d.ID,
                            PortfolioYear = d.Year,
                            Kind =
                                TempAllocationListItem
                                    .TEMP_ALLOCATION_KIND_DEPOSIT,
                            //OrderType =
                            //    isAllocated
                            //        ? DepositIdentifier.Placed
                            //        : DepositIdentifier.Returned,
                            Year = date == null ? null : (int?)date.Value.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(date),
                            Quarter = DateTools.GetQarterYearInWordsForDate(date),
                            Month = DateTools.GetMonthInWordsForDate(date),
                            Date = date,
                            OrderRegNum = (d.RegNum ?? "").ToString(),
                            CurrencyName = CurrencyIdentifier.RUBLES,
                            CurrSumm = null,
                            RubSumm =
                                isAllocated ? -d.LocationVolume : d.LocationVolume,
                            DateDI = d.DateDI,
                            OperationType =
                                TempAllocationListItem.enOperationType
                                    .DepositReturn
                        };
                    });
        }


        public long GetCountTempAllocateBalanceExtraListFilteredHib(bool isAllocate, DateTime? from, DateTime? to)
        {
            //Balance_Extra
            using (var session = DataAccessSystem.OpenSession())
            {
                return
                    session.CreateQuery(@"select count(*) from x in BalanceExtra
                        WHERE  x.OperationName IN (:operations) AND x.OperationDate is not null AND (:from is null OR x.OperationDate >= :from) AND (:to is null OR x.OperationDate <= :to) ")
                        .SetParameter("from", from)
                        .SetParameter("to", to)
                        .SetParameterList("operations",
                            new List<string>
                            {
                                BalanceExtraIdentifier.CB_BUY,
                                BalanceExtraIdentifier.CB_SELL,
                                BalanceExtraIdentifier.DEPOSIT_SETLE,
                                BalanceExtraIdentifier.DEPOSIT_RETURN
                            })
                        .UniqueResult<long>();
            }
        }


        public List<TempAllocationListItem> GetTempAllocateBalanceExtraListFilteredHib(bool isAllocate, int startIndex,
            DateTime? from, DateTime? to)
        {
            var result = new List<TempAllocationListItem>();
            //Balance_Extra
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
                    select x, p.Year, x.OperationDate, x.ReturnDate, x.Amount, x.RegNumber, x.OperationName, x.Comment,
                            x.CurrencyID, x.VAmount

                    from x in BalanceExtra
                    join x.Portfolio p
                      WHERE   x.OperationName IN (:operations) AND x.OperationDate is not null AND (:from is null OR x.OperationDate >= :from) AND (:to is null OR x.OperationDate <= :to) order by x.ID
                        ")
                    .SetParameter("from", from)
                    .SetParameter("to", to)
                    .SetParameterList("operations",
                        new List<string>
                        {
                            BalanceExtraIdentifier.CB_BUY,
                            BalanceExtraIdentifier.CB_SELL,
                            BalanceExtraIdentifier.DEPOSIT_SETLE,
                            BalanceExtraIdentifier.DEPOSIT_RETURN
                        });

                foreach (var p in query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>())
                {
#region  //Allocate

                    {
                        //Allocate
                        BalanceExtra bal = (BalanceExtra)p[0];

                        DateTime? d = bal.OperationDate;
                        var opName = bal.OperationName;

                        var x = new TempAllocationListItem
                        {
                            ID = bal.ID,
                            PortfolioYear = (string)p[1],
                            Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB,
                            //OrderType = "покупка",
                            Year = d == null ? null : (int?)d.Value.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(d),
                            Quarter = DateTools.GetQarterYearInWordsForDate(d),
                            Month = DateTools.GetMonthInWordsForDate(d),
                            Date = d,
                            OrderRegNum = bal.RegNumber,
                            CurrencyName =
                                (bal.CurrencyID == 1) ? CurrencyIdentifier.RUBLES : CurrencyIdentifier.DOLLARS,
                            CurrSumm = (bal.CurrencyID == 1) ? null : bal.VAmount,
                            RubSumm = bal.Amount,
                            DateDI = bal.DiDate,
                            OperationType = TempAllocationListItem.enOperationType.DepositAllocate,
                            Comment = bal.Comment,
                            IsExtra = true
                        };

                        //DateTime? d = (DateTime?)p[2];
                        //var opName = Convert.ToString(p[6]);
                        //var comment = Convert.ToString(p[7]);
                        //var curr = Convert.ToInt32(p[8]);

                        //var x = new TempAllocationListItem()
                        //{
                        //    ID = (long)p[0],
                        //    PortfolioYear = (string)p[1],
                        //    Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB,
                        //    //OrderType = "покупка",
                        //    Year = d == null ? null : (int?)d.Value.Year,
                        //    HalfYear = DateTools.GetHalfYearInWordsForDate(d),
                        //    Quarter = DateTools.GetQarterYearInWordsForDate(d),
                        //    Month = DateTools.GetMonthInWordsForDate(d),
                        //    Date = d,
                        //    OrderRegNum = Convert.ToString(p[5]),
                        //    CurrencyName = (curr == 1) ? CurrencyIdentifier.RUBLES : CurrencyIdentifier.DOLLARS,
                        //    CurrSumm = (curr == 1) ? null : (decimal?)p[9],
                        //    RubSumm = (decimal)p[4],
                        //    DateDI = d,
                        //    OperationType = TempAllocationListItem.enOperationType.DepositAllocate,
                        //    Comment = comment,
                        //    IsExtra = true
                        //};
                        switch (opName)
                        {
                            case BalanceExtraIdentifier.CB_BUY:
                                x.Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB;
                                x.OperationType = TempAllocationListItem.enOperationType.PaperBuy;
                                break;
                            case BalanceExtraIdentifier.CB_SELL:
                                x.Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB;
                                x.OperationType = TempAllocationListItem.enOperationType.PaperSell;
                                break;
                            case BalanceExtraIdentifier.DEPOSIT_SETLE:
                                x.Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT;
                                x.OperationType = TempAllocationListItem.enOperationType.DepositAllocate;
                                break;
                            case BalanceExtraIdentifier.DEPOSIT_RETURN:
                                x.Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT;
                                x.OperationType = TempAllocationListItem.enOperationType.DepositReturn;
                                break;
                        }
                        result.Add(x);
                    }
                }

#endregion

            }

            return result;
        }


        public long GetCountTempAllocateBalanceExtraListHib()
        {
            //Balance_Extra
            using (var session = DataAccessSystem.OpenSession())
            {
                return
                    session.CreateQuery(@"select count(*) from x in BalanceExtra join x.Portfolio p ")
                        .UniqueResult<long>();
            }
        }


        public List<TempAllocationListItem> GetTempAllocateBalanceExtraListHib(int startIndex)
        {
            List<TempAllocationListItem> result = new List<TempAllocationListItem>();
            //Balance_Extra
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
                        select x.ID, p.Year, x.OperationDate, x.ReturnDate, x.Amount, x.RegNumber
                        from x in BalanceExtra
                        join x.Portfolio p
                        order by x.ID");

                foreach (var p in query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>())
                {
#region  //Allocate

                    {
                        //Allocate
                        var d = (DateTime?)p[2];
                        var x = new TempAllocationListItem
                        {
                            ID = (long)p[0],
                            PortfolioYear = (string)p[1],
                            Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,
                            //OrderType = DepositIdentifier.Returned,
                            Year = d?.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(d),
                            Quarter = DateTools.GetQarterYearInWordsForDate(d),
                            Month = DateTools.GetMonthInWordsForDate(d),
                            Date = d,
                            OrderRegNum = Convert.ToString(p[5]),
                            CurrencyName = CurrencyIdentifier.RUBLES,
                            CurrSumm = null,
                            RubSumm = -(decimal)p[4],
                            DateDI = d,
                            OperationType = TempAllocationListItem.enOperationType.DepositAllocate,
                            IsExtra = true
                        };
                        result.Add(x);
                    }

#endregion

#region //Return

                    {

                        var d = (DateTime?)p[3];
                        var x = new TempAllocationListItem
                        {
                            ID = (long)p[0],
                            PortfolioYear = (string)p[1],
                            Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,
                            //OrderType = DepositIdentifier.Returned,
                            Year = d?.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(d),
                            Quarter = DateTools.GetQarterYearInWordsForDate(d),
                            Month = DateTools.GetMonthInWordsForDate(d),
                            Date = d,
                            OrderRegNum = Convert.ToString(p[5]),
                            CurrencyName = CurrencyIdentifier.RUBLES,
                            CurrSumm = null,
                            RubSumm = (decimal)p[4],
                            DateDI = d,
                            OperationType = TempAllocationListItem.enOperationType.DepositReturn,
                            IsExtra = true
                        };
                        result.Add(x);
                    }

#endregion
                }
            }
            return result;
        }


        public List<TempAllocationListItem> GetTempAllocationListHib()
        {
            //Sell paper
            List<TempAllocationListItem> result = GetBuyOrSaleReportsListHib(true, null)
                .ConvertAll(
                    x =>
                    {
                        decimal? sum = OrderTypeIdentifier.IsBuyOrder(x.OrderType) ? -x.Sum : x.Sum;
                        return new TempAllocationListItem
                        {
                            ID = x.ID,
                            PortfolioYear = x.PortfolioYear,
                            Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB,
                            //OrderType = x.OrderType,
                            Year = x.DateOrder == null ? null : (int?)x.DateOrder.Value.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(x.DateOrder),
                            Quarter = DateTools.GetQarterYearInWordsForDate(x.DateOrder),
                            Month = DateTools.GetMonthInWordsForDate(x.DateOrder),
                            Date = x.DateOrder,
                            OrderRegNum = x.RegNum,
                            CurrencyName = x.CurrencyName,
                            CurrSumm = x.CurrencyIsRubles ? null : sum,
                            RubSumm = x.CurrencyIsRubles ? sum : x.RubSum,
                            Comment = x.Comment,
                            DateDI = x.DateDI,
                            OperationType = TempAllocationListItem.enOperationType.PaperSell
                        };
                    }
                );

            //Buy Paper
            result.AddRange(GetBuyOrSaleReportsListHib(false, null).ConvertAll(
                x =>
                {
                    decimal? sum = OrderTypeIdentifier.IsBuyOrder(x.OrderType) ? -x.Sum : x.Sum;
                    return new TempAllocationListItem
                    {
                        ID = x.ID,
                        PortfolioYear = x.PortfolioYear,
                        Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_CB,
                        //OrderType = x.OrderType,
                        Year = x.DateOrder == null ? null : (int?)x.DateOrder.Value.Year,
                        HalfYear = DateTools.GetHalfYearInWordsForDate(x.DateOrder),
                        Quarter = DateTools.GetQarterYearInWordsForDate(x.DateOrder),
                        Month = DateTools.GetMonthInWordsForDate(x.DateOrder),
                        Date = x.DateOrder,
                        OrderRegNum = x.RegNum,
                        CurrencyName = x.CurrencyName,
                        CurrSumm = x.CurrencyIsRubles ? null : sum,
                        RubSumm = x.CurrencyIsRubles ? sum : x.RubSum,
                        Comment = x.Comment,
                        DateDI = x.DateDI,
                        OperationType = TempAllocationListItem.enOperationType.PaperBuy
                    };
                }
                ));


            //Allocate Deposit
            List<Deposit2> deposits = new List<Deposit2>();
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
select d.ID, d.Status, p.Year, dc2.SettleDate, dc2.ReturnDate, o.Number, s.Sum, d.DateDI
from d in Deposit
join d.DepClaimOffer o
join o.DepClaim dc2 
join o.OfferPfrBankAccountSum s
join s.Portfolio p
where s.StatusID>0
");

                foreach (object[] p in query.List<object[]>())
                {
                    deposits.Add(new Deposit2
                    {
                        ID = (long)p[0],
                        Status = (long?)p[1],
                        Year = (string)p[2],
                        LocationDate = (DateTime?)p[3],
                        ReturnDate = (DateTime?)p[4],
                        RegNum = p[5].ToString(),
                        LocationVolume = (decimal?)p[6],
                        DateDI = (DateTime?)p[7]
                    });
                }
            }

            //Allocate Deposit
            result.AddRange(deposits.ConvertAll(
                d =>
                {
                    bool isAllocated = true;
                    DateTime? date = isAllocated ? d.LocationDate : d.ReturnDate;
                    return new TempAllocationListItem
                    {
                        ID = d.ID,
                        PortfolioYear = d.Year,
                        Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,
                        //OrderType = isAllocated ? DepositIdentifier.Placed : DepositIdentifier.Returned,
                        Year = date == null ? null : (int?)date.Value.Year,
                        HalfYear = DateTools.GetHalfYearInWordsForDate(date),
                        Quarter = DateTools.GetQarterYearInWordsForDate(date),
                        Month = DateTools.GetMonthInWordsForDate(date),
                        Date = date,
                        OrderRegNum = (d.RegNum ?? "").ToString(),
                        CurrencyName = CurrencyIdentifier.RUBLES,
                        CurrSumm = null,
                        RubSumm = isAllocated ? -d.LocationVolume : d.LocationVolume,
                        DateDI = d.DateDI,
                        OperationType = TempAllocationListItem.enOperationType.DepositAllocate
                    };
                }
                ));


            //Return Deposit
            List<Deposit2> rL = new List<Deposit2>();
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
select d.ID, d.Status, p.Year, dc2.SettleDate, ph.Date, o.Number, ph.Sum, ph.Date
from d in Deposit
join d.DepClaimOffer o
join o.DepClaim dc2 
join o.OfferPfrBankAccountSum s
join s.Portfolio p
join s.PaymentHistory ph
where s.StatusID>0
and ph.ForPercent = 0
");

                foreach (object[] p in query.List<object[]>())
                {
                    rL.Add(new Deposit2
                    {
                        ID = (long)p[0],
                        Status = (long?)p[1],
                        Year = (string)p[2],
                        LocationDate = (DateTime?)p[3],
                        ReturnDate = (DateTime?)p[4],
                        RegNum = p[5].ToString(),
                        LocationVolume = (decimal?)p[6],
                        DateDI = (DateTime?)p[7]
                    });
                }
            }

            //Return Deposit
            result.AddRange(rL.ConvertAll(
                d =>
                {
                    bool isAllocated = false;
                    DateTime? date = isAllocated ? d.LocationDate : d.ReturnDate;
                    return new TempAllocationListItem
                    {
                        ID = d.ID,
                        PortfolioYear = d.Year,
                        Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,
                        //OrderType = isAllocated ? DepositIdentifier.Placed : DepositIdentifier.Returned,
                        Year = date == null ? null : (int?)date.Value.Year,
                        HalfYear = DateTools.GetHalfYearInWordsForDate(date),
                        Quarter = DateTools.GetQarterYearInWordsForDate(date),
                        Month = DateTools.GetMonthInWordsForDate(date),
                        Date = date,
                        OrderRegNum = (d.RegNum ?? "").ToString(),
                        CurrencyName = CurrencyIdentifier.RUBLES,
                        CurrSumm = null,
                        RubSumm = isAllocated ? -d.LocationVolume : d.LocationVolume,
                        DateDI = d.DateDI,
                        OperationType = TempAllocationListItem.enOperationType.DepositReturn
                    };
                }
                ));


            //Balance_Extra
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
select x.ID, p.Year, x.OperationDate, x.ReturnDate, x.Amount, x.RegNumber

from x in BalanceExtra
join x.Portfolio p
");

                foreach (object[] p in query.List<object[]>())
                {
                    {
                        //Allocate
                        DateTime? d = (DateTime?)p[2];
                        var x = new TempAllocationListItem
                        {
                            ID = (long)p[0],
                            PortfolioYear = (string)p[1],
                            Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,
                            //OrderType = DepositIdentifier.Returned,
                            Year = d == null ? null : (int?)d.Value.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(d),
                            Quarter = DateTools.GetQarterYearInWordsForDate(d),
                            Month = DateTools.GetMonthInWordsForDate(d),
                            Date = d,
                            OrderRegNum = Convert.ToString(p[5]),
                            CurrencyName = CurrencyIdentifier.RUBLES,
                            CurrSumm = null,
                            RubSumm = -(decimal)p[4],
                            DateDI = d,
                            OperationType = TempAllocationListItem.enOperationType.DepositAllocate,
                            IsExtra = true
                        };
                        result.Add(x);
                    }


                    {
                        //Return
                        DateTime? d = (DateTime?)p[3];
                        var x = new TempAllocationListItem
                        {
                            ID = (long)p[0],
                            PortfolioYear = (string)p[1],
                            Kind = TempAllocationListItem.TEMP_ALLOCATION_KIND_DEPOSIT,
                            //OrderType = DepositIdentifier.Returned,
                            Year = d == null ? null : (int?)d.Value.Year,
                            HalfYear = DateTools.GetHalfYearInWordsForDate(d),
                            Quarter = DateTools.GetQarterYearInWordsForDate(d),
                            Month = DateTools.GetMonthInWordsForDate(d),
                            Date = d,
                            OrderRegNum = Convert.ToString(p[5]),
                            CurrencyName = CurrencyIdentifier.RUBLES,
                            CurrSumm = null,
                            RubSumm = (decimal)p[4],
                            DateDI = d,
                            OperationType = TempAllocationListItem.enOperationType.DepositReturn,
                            IsExtra = true
                        };
                        result.Add(x);
                    }
                }
            }


            return result;
        }

#endregion

        public DepositInfo GetDepositInfoHib(long depositId)
        {
            DepositInfo res;

            using (var session = DataAccessSystem.OpenSession())
            {
                {
                    IQuery query = session.CreateQuery(@"
                        select 
                        d.ID, o.ID, dc2.FirmName, b.PFRAGR, b.PFRAGRDATE, dc2.FirmID, b.CorrAcc, o.Date, o.Number, 
                        dc2.Amount, dc2.Rate, dc2.Payment, dc2.SettleDate, dc2.ReturnDate, d.ReturnDateActual, d.Status, c.Name

                        from d in Deposit
                        join d.DepClaimOffer o
                        join o.DepClaim dc2
                        join dc2.Bank b
                        left join d.Comment c
                        where d.ID = :id
                        ").SetParameter("id", depositId);

                    object[] p = query.UniqueResult<object[]>();
                    if (p == null)
                        return null;

                    res = new DepositInfo
                    {
                        DepositID = (long)p[0],
                        DepClaimOfferID = (long)p[1],
                        Dc2_FirmName = (string)p[2],
                        Bank_PfrAgr = (string)p[3],
                        Bank_PfrAgrDate = (DateTime?)p[4],
                        Dc2_FirmID = (string)p[5],
                        Bank_CorrAcc = (string)p[6],
                        Dco_Date = (DateTime)p[7],
                        Dco_Number = (long)p[8],

                        Dc2_Amount = (decimal)p[9],
                        Dc2_Rate = (decimal)p[10],
                        Dc2_Payment = (decimal)p[11],
                        CurrencyName = "Российский рубль",
                        Dc2_SettleDate = (DateTime)p[12],
                        Dc2_ReturnDate = (DateTime)p[13],
                        D_ReturnDateActual = (DateTime?)p[14],
                        Status = (long?)p[15],
                        Comment = (string)p[16]
                    };
                }

                //Select Allocation
                {
                    IQuery query = session.CreateQuery(@"
select 
d.ID, dc2.SettleDate, dc2.SettleDate, p.Year, p.Type, y.Name, ba.AccountNumber, s.Sum

from d in Deposit
join d.DepClaimOffer o
join o.DepClaim dc2
join o.OfferPfrBankAccountSum s
join s.Portfolio p
join s.PfrBankAccount ba
left join p.RealYear y
where d.ID = :id
and s.StatusID>0
").SetParameter("id", depositId);


                    var xL = res.DepositAllocationInfoList = new List<DepositAllocationInfo>();
                    foreach (object[] p in query.List<object[]>())
                    {
                        xL.Add(new DepositAllocationInfo
                        {
                            Date = (DateTime)p[1],
                            ActualDate = (DateTime?)p[2],
                            PortfolioName = (string)p[3],
                            PortfolioType = (string)p[4],
                            Year = (string)p[5],
                            AccountNumber = (string)p[6],
                            Amount = (decimal)p[7]
                        });
                    }
                }
                //select Return
                {
                    IQuery query = session.CreateQuery(@"
select 
d.ID, dc2.ReturnDate, ph.OrderDate, p.Year, p.Type, y.Name, ba.AccountNumber, ph.Sum

from d in Deposit
join d.DepClaimOffer o
join o.DepClaim dc2
join o.OfferPfrBankAccountSum s
join s.Portfolio p
join s.PfrBankAccount ba
left join p.RealYear y
join s.PaymentHistory ph
where d.ID = :id
and ph.ForPercent = 0
and s.StatusID>0
").SetParameter("id", depositId);


                    var xL = res.DepositReturnInfoList = new List<DepositAllocationInfo>();
                    foreach (object[] p in query.List<object[]>())
                    {
                        xL.Add(new DepositAllocationInfo
                        {
                            Date = (DateTime)p[1],
                            ActualDate = (DateTime?)p[2],
                            PortfolioName = (string)p[3],
                            PortfolioType = (string)p[4],
                            Year = (string)p[5],
                            AccountNumber = (string)p[6],
                            Amount = (decimal)p[7]
                        });
                    }
                }

                //select percent
                {
                    IQuery query = session.CreateQuery(@"
select 
d.ID, dc2.ReturnDate, ph.OrderDate, p.Year, p.Type, y.Name, ba.AccountNumber, ph.Sum

from d in Deposit
join d.DepClaimOffer o
join o.DepClaim dc2
join o.OfferPfrBankAccountSum s
join s.Portfolio p
join s.PfrBankAccount ba
left join p.RealYear y
join s.PaymentHistory ph
where d.ID = :id
and ph.ForPercent = 1
and s.StatusID>0
").SetParameter("id", depositId);


                    var xL = res.DepositPercentInfoList = new List<DepositAllocationInfo>();
                    foreach (object[] p in query.List<object[]>())
                    {
                        xL.Add(new DepositAllocationInfo
                        {
                            Date = (DateTime)p[1],
                            ActualDate = (DateTime?)p[2],
                            PortfolioName = (string)p[3],
                            PortfolioType = (string)p[4],
                            Year = (string)p[5],
                            AccountNumber = (string)p[6],
                            Amount = (decimal)p[7]
                        });
                    }
                }
            }

            return res;
        }

        public List<CorrespondenceListItemNew> GetNpfDocumentsListHib(Document.Statuses status, long? documentID = null, long? attachID = null)
        {
            return GetDocumentsListHib(status, Document.Types.Npf, documentID, attachID);
        }

        public List<CorrespondenceListItemNew> GetSIDocumentsListHib(Document.Statuses status, long? documentID = null, long? attachID = null)
        {
            return GetDocumentsListHib(status, Document.Types.SI, documentID, attachID);
        }

        public List<CorrespondenceListItemNew> GetVRDocumentsListHib(Document.Statuses status, long? documentID = null, long? attachID = null)
        {
            return GetDocumentsListHib(status, Document.Types.VR, documentID, attachID);
        }


        private List<CorrespondenceListItemNew> GetDocumentsListHib(bool? executed, Document.Types type)
        {
            Document.Statuses status = Document.Statuses.All;
            if (executed.HasValue)
            {
                //Корреспонденция или Архив
                if (executed.Value)
                    //Архив
                    status = Document.Statuses.Executed;
                else
                    //Корреспонденция
                    status = Document.Statuses.Active;
            }
            else
            {
                //Контроль
                status = Document.Statuses.Control;
            }

            return GetDocumentsListHib(status, type);
        }



        private List<CorrespondenceListItemNew> GetDocumentsListHib(Document.Statuses status, Document.Types type, long? documentID = null, long? attachID = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = @"	SELECT 
d.ID AS ID, 
d.IncomingDate AS IncomingDate, 
d.IncomingNumber AS IncomingNumber, 
d.OutgoingDate AS OutgoingDate, 
d.OutgoingNumber AS OutgoingNumber, 

le.FormalizedName AS Sender, 
dc.Name AS DocumentClass, 
CASE WHEN (d.TypeID = 2 OR d.TypeID = 3) THEN d.ExecutorName ELSE NULL END AS Executor, 
coalesce(ai.Name, d.AdditionalInfoText) AS AdditionalDocumentInfo, 

d.ControlDate AS ControlDate, 
d.ExecutionDate AS ExecutionDate,
coalesce(att.ExecutorName, (select f.LastName from Person f where f.ID = att.ExecutorID) ) AS AttachExecutorName, 

att.Additional AS AttachAdditional,
att.ControlDate AS AttachControlDate, 
att.ExecutionDate AS AttachExecutionDate, 
ac.Name AS AttachClassific, 
att.ID AS AttachId,

fio

FROM Document d 
join d.LegalEntity le
join d.DocumentClass dc
left outer join d.AdditionalInfo ai
left outer join d.Executor fio
left join d.AttachesActive att
left outer join att.AttachClassific ac
WHERE 
    (not (d.StatusID = -1)) 
and (d.TypeID = (:docTypeId)) 
and (:documentID is null OR d.ID = :documentID)
and (:attachID is null OR att.ID = :attachID)
";

                switch (status)
                {
                    //Корреспонденция
                    case Document.Statuses.Active:
                        query += " AND d.ExecutionDate is null ";
                        break;
                    //Архив
                    case Document.Statuses.Executed:
                        query += " AND d.ExecutionDate is not null ";
                        break;
                    //Контроль
                    case Document.Statuses.Control:
                        query += " AND d.ExecutionDate is null AND d.ControlDate is not null";
                        break;
                }

                var crit = session.CreateQuery(query);
                crit.SetParameter("docTypeId", (int)type);
                crit.SetParameter("documentID", documentID);
                crit.SetParameter("attachID", attachID);

                var list = crit.List<object[]>();
                var docInfoList = new List<CorrespondenceListItemNewInfo>();

                foreach (var p in list)
                {
                    var executor = p[7];
                    var fio = (Person) p[17];

                    var executorName = (string)executor ?? "";
                    if (executor == null && fio != null)
                    {
                        executorName = fio.FIOShort;
                    }

                    var x = new CorrespondenceListItemNewInfo
                    {
                        ID = (long)p[0],
                        IncomingDate = (DateTime?)p[1],
                        IncomingNumber = (string)p[2],
                        OutgoingDate = (DateTime?)p[3],
                        OutgoingNumber = (string)p[4],
                        Sender = (string)p[5],
                        DocumentClass = (string)p[6],
                        Executor = executorName,
                        AdditionalDocumentInfo = (string)p[8],
                        ControlDate = (DateTime?)p[9],
                        ExecutionDate = (DateTime?)p[10],
                        AttachExecutorName = (string)p[11],
                        AttachAdditional = (string)p[12],
                        AttachControlDate = (DateTime?)p[13],
                        AttachExecutionDate = (DateTime?)p[14],
                        AttachClassific = (string)p[15],
                        AttachId = (long?)p[16]
                    };
                    docInfoList.Add(x);
                }

                var attachGroups = docInfoList.GroupBy(x => x.ID);
                var items = new List<CorrespondenceListItemNew>();

                foreach (var group in attachGroups)
                {
                    var f = group.First();
                    var item = new CorrespondenceListItemNew
                    {
                        ID = f.ID,
                        IncomingDate = f.IncomingDate,
                        IncomingNumber = string.IsNullOrEmpty(f.IncomingNumber) ? string.Empty : f.IncomingNumber.Trim(),
                        Month =
                            f.IncomingDate.HasValue
                                ? DateTools.GetMonthInWordsForDate(f.IncomingDate.Value.Month)
                                : string.Empty,
                        OutgoingDate = f.OutgoingDate,
                        OutgoingNumber = string.IsNullOrEmpty(f.OutgoingNumber) ? string.Empty : f.OutgoingNumber.Trim(),
                        Sender = f.Sender,
                        Year = f.IncomingDate.HasValue ? f.IncomingDate.Value.Year : (int?)null,
                        DocumentClass = f.DocumentClass,
                        Executor =
                            string.IsNullOrEmpty(f.Executor) ? CorrespondenceComments.ExecutorNotAssigned : f.Executor,
                        AdditionalDocumentInfo = f.AdditionalDocumentInfo,
                        ControlDate = f.ControlDate,
                        ExecutionDate = f.ExecutionDate,
                        IsExecutionExpired =
                            f.ControlDate.HasValue && DateTime.Today > f.ControlDate &&
                            (f.ExecutionDate == null || f.ExecutionDate != null && f.ExecutionDate > f.ControlDate),
                        IsAttach = false
                    };
                    items.Add(item);

                    foreach (var attach in group)
                    {
                        if (attach.AttachId.HasValue)
                        {
                            var subitem = new CorrespondenceListItemNew
                            {
                                ID = attach.AttachId.Value,
                                IncomingDate = item.IncomingDate,
                                IncomingNumber = item.IncomingNumber,
                                Month =
                                    item.IncomingDate.HasValue
                                        ? DateTools.GetMonthInWordsForDate(item.IncomingDate.Value.Month)
                                        : string.Empty,
                                Year = item.IncomingDate.HasValue ? item.IncomingDate.Value.Year : (int?)null,
                                Sender = item.Sender,
                                DocumentClass = !string.IsNullOrEmpty(attach.AttachClassific)
                                    ? $"{attach.AttachClassific} ({CorrespondenceComments.Attachment})"
                                    : string.Empty,
                                Executor =
                                    string.IsNullOrEmpty(attach.AttachExecutorName)
                                        ? CorrespondenceComments.ExecutorNotAssigned
                                        : attach.AttachExecutorName,
                                AdditionalDocumentInfo = attach.AttachAdditional,
                                ControlDate = attach.AttachControlDate,
                                ExecutionDate = attach.AttachExecutionDate,
                                IsExecutionExpired =
                                    attach.ControlDate.HasValue && DateTime.Today > attach.ControlDate &&
                                    (attach.ExecutionDate == null ||
                                     attach.ExecutionDate != null && attach.ExecutionDate > attach.ControlDate),
                                IsAttach = true
                            };

                            items.Add(subitem);
                        }
                    }
                }

                return items;
            }
        }

        public List<Document> GetDocumentsBypropertiesListHib(Document.Types type, string incomingNumber,
            DateTime? dateTime)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (dateTime == null)
                {
                    return
                        session.CreateQuery(
                            "select d from Document d where lower(d.IncomingNumber)= :incomingNumber and d.TypeID= :type and d.StatusID<>-1")
                            .SetParameter("incomingNumber", incomingNumber.ToLower())
                            .SetParameter("type", (int)type)
                            .List<Document>()
                            .ToList();
                }
                return
                    session.CreateQuery(
                            $@"select d from Document d 
                               where lower(d.IncomingNumber)= :incomingNumber and d.TypeID= :type and 
                               {(IsDB2 ? "YEAR(" : "EXTRACT(YEAR FROM ")}d.IncomingDate) = :ytd and d.StatusID<>-1")
                        .SetParameter("incomingNumber", incomingNumber.ToLower())
                        .SetParameter("type", (int)type)
                        .SetParameter("ytd", dateTime.Value.Year)
                        .List<Document>()
                        .ToList();
            }
        }




#region Get list

        /// <summary>
        /// Запрос для перечислений УК раздела Бэк-Офис
        /// </summary>			
        private ICriteria GetUKTransfersListQuery(ISession session, bool ppEntered, bool includePpList = true)
        {
            var reqs = session.CreateCriteria(typeof(ReqTransferHib), "rt")
                .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)))
                .Add(Restrictions.Not(Restrictions.Eq("Unlinked", 1)))
                // Отключаем показ всех записей мигрированых из лотуса из разделов отличных от Бэк-Офис
                .Add(
                    Restrictions.Not(Restrictions.Like("TransferStatus", TransferStatusIdentifier.sDemandGenerated,
                        MatchMode.Anywhere)))
                .Add(
                    Restrictions.Not(Restrictions.Like("TransferStatus", TransferStatusIdentifier.sBeginState,
                        MatchMode.Anywhere)));

            if (ppEntered) //Все перечисления с введенной датой платежки
            {
                reqs.Add(Restrictions.IsNotNull("SPNDebitDate"));
                //reqs.Add(Expression.Eq("Unlinked", 0)); // Показываем все операции
            }
            else
            {
                //Все перечисления с не введенной датой платежки и в статусе 
                //Требование отправлено в УК (в ПФР)
                //Заявка оформлена (из ПФР)
                //Такой гемор для фильтрации записей с невалидным статусом 
                //но не введенной датой платежки
                reqs.Add(
                    Restrictions.Like("TransferStatus", TransferStatusIdentifier.sDemandSentInUK, MatchMode.Anywhere) ||
                    Restrictions.Like("TransferStatus", TransferStatusIdentifier.sClaimCompleted, MatchMode.Anywhere));
                reqs.Add(Restrictions.IsNull("PaymentOrderDate"));
            }

            reqs.CreateCriteria("PfrBankAccount", "ba", JoinType.LeftOuterJoin)
                .SetFetchMode("PfrBankAccount", FetchMode.Join);

            reqs.CreateCriteria("Portfolio", "p", JoinType.LeftOuterJoin)
                .SetFetchMode("Portfolio", FetchMode.Join);

            var lists =
                reqs.CreateCriteria("TransferList", "tl", JoinType.InnerJoin)
                    .SetFetchMode("TransferList", FetchMode.Join);

            lists.CreateCriteria("Contract", "c", JoinType.LeftOuterJoin)
                .SetFetchMode("Contract", FetchMode.Join)
                .CreateCriteria("LegalEntity", "le", JoinType.LeftOuterJoin)
                .SetFetchMode("LegalEntity", FetchMode.Join);

            var regs =
                lists.CreateCriteria("TransferRegister", "tr", JoinType.InnerJoin)
                    .SetFetchMode("TransferRegister", FetchMode.Join);

            regs.CreateCriteria("Direction", "d", JoinType.InnerJoin)
                .SetFetchMode("Direction", FetchMode.Join);

            regs.CreateCriteria("Operation", "op", JoinType.InnerJoin)
                .SetFetchMode("Operation", FetchMode.Join);

            //TODO не всем нужен полный список ПП, тем более дублирует записи
            /*if (forUkArch)
		    {
		        DetachedCriteria exampleSubquery = DetachedCriteria.For<AsgFinTr>()
                    .Add(Restrictions.Eq("StatusID", -1))
		            .SetProjection(Property.ForName("ID"));
                    

		        regs.CreateCriteria("rt.CommonPPList", "pp", JoinType.LeftOuterJoin)
		            .SetFetchMode("rt.CommonPPList", FetchMode.Join)
                    .AddOrder(Order.Desc("DraftPayDate"))
		            .SetFirstResult(1);
		    }
		    else
		    {*/
            if (includePpList)
            {
                regs.CreateCriteria("rt.CommonPPList", "pp", JoinType.LeftOuterJoin)
                    .SetFetchMode("rt.CommonPPList", FetchMode.Join);
                regs.CreateCriteria("pp.Portfolio", "ppp", JoinType.LeftOuterJoin)
                    .SetFetchMode("pp.Portfolio", FetchMode.Join);
                regs.CreateCriteria("pp.PFRBankAccount", "ppba", JoinType.LeftOuterJoin)
                    .SetFetchMode("pp.PFRBankAccount", FetchMode.Join);
            }
            //var pp = regs.SetFetchMode("rt.CommonPPList", FetchMode.Join);

            //pp.SetFetchMode("rt.CommonPPList.Portfolio", FetchMode.Join);
            //pp.SetFetchMode("rt.CommonPPList.PFRBankAccount", FetchMode.Join);

            return reqs;

        }

        public long GetCountUKTransfersListArchiveFiltered(DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = GetUKTransfersListQuery(session, true, false);

                if (from.HasValue)
                    reqs.Add(Restrictions.Ge("SPNDebitDate", from.Value));
                if (to.HasValue)
                    reqs.Add(Restrictions.Le("SPNDebitDate", to.Value));
                var count = reqs.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
                return count;
            }

        }

        public List<ReqTransfer> GetUKTransfersListArchiveFilteredByPage(DateTime? from, DateTime? to, int startIngex)
        {
            ///
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = GetUKTransfersListQuery(session, true);

                if (from.HasValue)
                    reqs.Add(Restrictions.Ge("SPNDebitDate", from.Value));
                if (to.HasValue)
                    reqs.Add(Restrictions.Le("SPNDebitDate", to.Value));

                return reqs.SetFirstResult(startIngex).SetMaxResults(PageSize).List<ReqTransfer>().ToList();
            }
        }

        private UKListItemHib WrapReqTransfer(ReqTransferHib rt, bool calcCommonPP = false)
        {
            return new UKListItemHib
            {
                ReqTransfer = rt,
                SPNDirectionName =
                    rt.TransferList.TransferRegister.Direction != null
                        ? rt.TransferList.TransferRegister.Direction.Name
                        : null,
                SPNOperationName =
                    rt.TransferList.TransferRegister.Operation != null
                        ? rt.TransferList.TransferRegister.Operation.Name
                        : null,
                FormalizedName =
                    rt.TransferList.Contract != null && rt.TransferList.Contract.LegalEntity != null
                        ? rt.TransferList.Contract.LegalEntity.FormalizedName
                        : null,
                ContractNumber = rt.TransferList.Contract != null ? rt.TransferList.Contract.ContractNumber : null,
                Portfolio = rt.Portfolio == null ? null : rt.Portfolio.Year,
                PfrBankAccount = rt.PfrBankAccount == null ? null : rt.PfrBankAccount.AccountNumber,
                IsFromUKToPFR =
                    rt.TransferList.TransferRegister.Direction != null
                        ? rt.TransferList.TransferRegister.Direction.isFromUKToPFR
                        : null,
                SIRegisterID = rt.TransferList.TransferRegister.ID,
                RegisterDate = rt.TransferList.TransferRegister.RegisterDate,
                RegisterKind = rt.TransferList.TransferRegister.RegisterKind,
                RegisterNumber = rt.TransferList.TransferRegister.RegisterNumber,
                CommonPPSum = calcCommonPP ? rt.CommonPPList.Sum(p => p.DraftAmount) : rt.Sum
            };
        }

        /// <summary>
        /// Вывод данных в архив на основе привязанных платежных поручений
        /// </summary>
        private UKListItemHib WrapArchiveReqTransfer(ReqTransferHib rt)
        {
            var lastPP = rt.CommonPPList.OrderBy(p => p.SPNDate).LastOrDefault();

            return new UKListItemHib
            {
                ReqTransfer = rt,
                SPNDirectionName =
                    rt.TransferList.TransferRegister.Direction != null
                        ? rt.TransferList.TransferRegister.Direction.Name
                        : null,
                SPNOperationName =
                    rt.TransferList.TransferRegister.Operation != null
                        ? rt.TransferList.TransferRegister.Operation.Name
                        : null,
                FormalizedName =
                    rt.TransferList.Contract != null && rt.TransferList.Contract.LegalEntity != null
                        ? rt.TransferList.Contract.LegalEntity.FormalizedName
                        : null,
                ContractNumber = rt.TransferList.Contract != null ? rt.TransferList.Contract.ContractNumber : null,
                Portfolio =
                    (lastPP != null && lastPP.Portfolio != null)
                        ? lastPP.Portfolio.Year
                        : rt.Portfolio != null ? rt.Portfolio.Year : null,
                PfrBankAccount =
                    (lastPP != null && lastPP.PFRBankAccount != null)
                        ? lastPP.PFRBankAccount.AccountNumber
                        : rt.PfrBankAccount != null ? rt.PfrBankAccount.AccountNumber : null,
                IsFromUKToPFR =
                    rt.TransferList.TransferRegister.Direction != null
                        ? rt.TransferList.TransferRegister.Direction.isFromUKToPFR
                        : null,
                SIRegisterID = rt.TransferList.TransferRegister.ID,
                RegisterDate = rt.TransferList.TransferRegister.RegisterDate,
                RegisterKind = rt.TransferList.TransferRegister.RegisterKind,
                RegisterNumber = rt.TransferList.TransferRegister.RegisterNumber,
                CommonPPNumber = lastPP != null ? lastPP.PayNumber : rt.PaymentOrderNumber,
                CommonPPDate = lastPP != null ? lastPP.PayDate : rt.PaymentOrderDate,
                CommonPPKBK = lastPP != null ? lastPP.KBKID : rt.KBKID
                //CommonPPSum = x.DraftAmount,
            };
        }

        public List<UKListItemHib> GetUKTransfersListHibByPageArchiveFiltered(DateTime? from, DateTime? to,
            int startIngex, long? recordID = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = GetUKTransfersListQuery(session, true);

                if (from.HasValue)
                    reqs.Add(Restrictions.Ge("SPNDebitDate", from.Value));
                if (to.HasValue)
                    reqs.Add(Restrictions.Le("SPNDebitDate", to.Value));

                if (recordID.HasValue)
                    reqs.Add(Restrictions.Eq("ID", recordID));

                var reqTransferHib = reqs.SetFirstResult(startIngex).SetMaxResults(PageSize).List<ReqTransferHib>();

                //отсеиваем дубликаты из трешового запроса (дублируются по п/п)
                //берем первую запись из группировки по ID перечисления
                //var tmp = reqTransferHib.Cast<ReqTransfer>().ToList();
                var uniqueList = reqTransferHib.GroupBy(a => a.ID).Select(a => a.ToList().First()).ToList();

                var uKListItemHib = new List<UKListItemHib>();
                uniqueList.ForEach(rt => uKListItemHib.Add(WrapArchiveReqTransfer(rt)));
                return uKListItemHib;
            }
        }



        public long GetCountUKTransfersList(bool ppEntered)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = GetUKTransfersListQuery(session, ppEntered, false);

                var count = reqs.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
                return count;
                //return reqs.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
            }
        }


        public List<ReqTransfer> GetUKTransfersListByPage(bool ppEntered, int startIngex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = GetUKTransfersListQuery(session, ppEntered);

                return reqs.SetFirstResult(startIngex).SetMaxResults(PageSize).List<ReqTransfer>().ToList();
            }
        }



        public List<UKListItemHib> GetUKTransfersListHibByPage(bool ppEntered, int startIngex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = GetUKTransfersListQuery(session, ppEntered);

                var reqTransferHib = reqs.SetFirstResult(startIngex).SetMaxResults(PageSize).List<ReqTransferHib>();
                var uniqueList = reqTransferHib.GroupBy(a => a.ID).Select(a => a.ToList().First()).ToList();
                var uKListItemHib = new List<UKListItemHib>();
                uniqueList.ForEach(rt => uKListItemHib.Add(WrapReqTransfer(rt, true)));

                return uKListItemHib;
            }
        }

        public List<UKListItemHib> GetUKTransfersListHibByID(bool ppEntered, long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = GetUKTransfersListQuery(session, ppEntered);
                reqs.Add(Restrictions.Eq("ID", id));

                var reqTransferHib = reqs.List<ReqTransferHib>();
                var uniqueList = reqTransferHib.GroupBy(a => a.ID).Select(a => a.ToList().First()).ToList();
                var uKListItemHib = new List<UKListItemHib>();
                uniqueList.ForEach(rt => uKListItemHib.Add(WrapReqTransfer(rt, true)));

                return uKListItemHib;
            }
        }

        public List<ReqTransfer> GetUKTransfersList(bool ppEntered)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = session.CreateCriteria(typeof(ReqTransferHib))
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)))
                    .Add(
                        Restrictions.Not(Restrictions.Like("TransferStatus", TransferStatusIdentifier.sDemandGenerated,
                            MatchMode.Anywhere)))
                    .Add(
                        Restrictions.Not(Restrictions.Like("TransferStatus", TransferStatusIdentifier.sBeginState,
                            MatchMode.Anywhere)));

                if (ppEntered)
                    //Все перечисления с введенной датой платежки
                    reqs.Add(Restrictions.IsNotNull("PaymentOrderDate"));
                else
                {
                    //Все перечисления с не введенной датой платежки и в статусе 
                    //Требование отправлено в УК (в ПФР)
                    //Заявка оформлена (из ПФР)
                    //Такой гемор для фильтрации записей с невалидным статусом 
                    //но не введенной датой платежки
                    reqs.Add(
                        Restrictions.Like("TransferStatus", TransferStatusIdentifier.sDemandSentInUK, MatchMode.Anywhere) ||
                        Restrictions.Like("TransferStatus", TransferStatusIdentifier.sClaimCompleted, MatchMode.Anywhere));
                    reqs.Add(Restrictions.IsNull("PaymentOrderDate"));
                }

                reqs.CreateCriteria("PfrBankAccount", "ba", JoinType.LeftOuterJoin)
                    .SetFetchMode("PfrBankAccount", FetchMode.Join);

                reqs.CreateCriteria("Portfolio", "p", JoinType.LeftOuterJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join);

                var lists = reqs.CreateCriteria("TransferList", "tl", JoinType.InnerJoin)
                    .SetFetchMode("TransferList", FetchMode.Join);

                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join);

                var regs = lists.CreateCriteria("TransferRegister", "tr", JoinType.InnerJoin)
                    .SetFetchMode("TransferRegister", FetchMode.Join);

                regs.CreateCriteria("Direction", "d", JoinType.InnerJoin)
                    .SetFetchMode("Direction", FetchMode.Join);

                regs.CreateCriteria("Operation", "op", JoinType.InnerJoin)
                    .SetFetchMode("Operation", FetchMode.Join);

                return reqs.List<ReqTransfer>().ToList();
            }
        }

        public List<PPTransferListItem> GetUKPPTransfersByTransferID(long transferID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = session.CreateCriteria(typeof(ReqTransferHib), "rt")
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)));

                var lists =
                    reqs.CreateCriteria("TransferList", "tl", JoinType.InnerJoin)
                        .SetFetchMode("TransferList", FetchMode.Join);

                lists.Add(Restrictions.Eq("ID", transferID));

                var pp = reqs.SetFetchMode("rt.CommonPPList", FetchMode.Join);

                return reqs.List<ReqTransferHib>().Select(x => x.MapToPPTransfer()).ToList();
            }
        }

        public List<ReqTransfer> GetTransfersListBySITR(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var reqs = session.CreateCriteria(typeof(ReqTransferHib))
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)));

                reqs.CreateCriteria("PfrBankAccount", "ba", JoinType.LeftOuterJoin)
                    .SetFetchMode("PfrBankAccount", FetchMode.Join);

                reqs.CreateCriteria("Portfolio", "p", JoinType.LeftOuterJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join);

                var lists = reqs.CreateCriteria("TransferList", "tl", JoinType.InnerJoin)
                    .SetFetchMode("TransferList", FetchMode.Join)
                    .Add(Restrictions.Eq("ID", id));

                lists.CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join);

                lists.CreateCriteria("TransferRegister", "tr", JoinType.InnerJoin)
                    .SetFetchMode("TransferRegister", FetchMode.Join)
                    .CreateCriteria("Direction", "d", JoinType.InnerJoin)
                    .SetFetchMode("Direction", FetchMode.Join);

                return reqs.List<ReqTransfer>().ToList();
            }
        }

        /// <summary>
        /// Запрос для архива перечислений НПФ для раздела Бэк-Офис
        /// </summary>		
        private ICriteria GetTransferToNPFArchiveListFilteredQuery(ISession session, DateTime? from, DateTime? to)
        {
            var finregisters = session.CreateCriteria(typeof(AsgFinTrHib), "AsgFinTr")
                .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                .SetFetchMode("Portfolio", FetchMode.Join)
                .CreateCriteria("AsgFinTr.Finregister", "fr", JoinType.InnerJoin)
                .SetFetchMode("AsgFinTr.Finregister", FetchMode.Join)
                ;

            finregisters.CreateCriteria("CreditAccount", "ca", JoinType.InnerJoin)
                .SetFetchMode("CreditAccount", FetchMode.Join)
                .CreateCriteria("Contragent", "c", JoinType.InnerJoin)
                .SetFetchMode("Contragent", FetchMode.Join);

            finregisters.CreateCriteria("DebitAccount", "da", JoinType.InnerJoin)
                .SetFetchMode("DebitAccount", FetchMode.Join)
                .CreateCriteria("Contragent", "c2", JoinType.InnerJoin)
                .SetFetchMode("Contragent", FetchMode.Join);

            finregisters.CreateCriteria("Register", "r", JoinType.InnerJoin)
                .SetFetchMode("Register", FetchMode.Join)
                ;

            if (from.HasValue)
                finregisters.Add(Restrictions.Ge("AsgFinTr.DraftDate", from.Value));
            if (to.HasValue)
                finregisters.Add(Restrictions.Le("AsgFinTr.DraftDate", to.Value));

            finregisters.Add(Restrictions.Eq("AsgFinTr.Unlinked", 0));
            finregisters.Add(Restrictions.Not(Restrictions.Eq("AsgFinTr.StatusID", (long)-1)));

            return finregisters;
        }

        public long GetCountTransferToNPFArchiveListFiltered(DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = GetTransferToNPFArchiveListFilteredQuery(session, from, to);
                return query.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
            }
        }


        public List<TransferFromOrToNPFListItem> GetTransferToNPFArchiveListFilteredByPage(int startIngex,
            DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = GetTransferToNPFArchiveListFilteredQuery(session, from, to);
                return query
                    .AddOrder(Order.Asc("AsgFinTr.DraftPayDate"))
                    .SetFirstResult(startIngex)
                    .SetMaxResults(PageSize).List<AsgFinTrHib>().ToList()
                    .ConvertAll
                    (
                        fintr =>
                        {
                            var item = new TransferFromOrToNPFListItem
                            {
                                AsgFinTrID = fintr.ID,
                                FinregisterID = fintr.FinregisterID ?? 0,
                                Portfolio = fintr.Portfolio.Year,
                                Year = fintr.DraftPayDate.HasValue ? fintr.DraftPayDate.Value.Year : (int?)null,
                                HalfYear = DateTools.GetHalfYearInWordsForDate(fintr.DraftPayDate),
                                QuarterYear = DateTools.GetQarterYearInWordsForDate(fintr.DraftPayDate),
                                Month = DateTools.GetMonthInWordsForDate(fintr.DraftPayDate),
                                PaymentOrderDate = fintr.DraftDate,
                                DraftPayDate = fintr.DraftPayDate,
                                //Comment = fintr.Finregister.Register.Comment,
                                Comment = fintr.Comment,
                                Content = fintr.Finregister.Register.Content
                            };
                            if (RegisterIdentifier.IsToNPF(fintr.Finregister.Register.Kind))
                            {
                                item.NPFShortName = fintr.Finregister.DebitAccount.Contragent.Name;
                                item.RegisterKind = RegisterIdentifier.PFRtoNPF;
                                item.ToNPF = fintr.DraftAmount;
                                item.FromNPF = null;
                            }
                            else
                            {
                                item.NPFShortName = fintr.Finregister.CreditAccount.Contragent.Name;
                                item.RegisterKind = RegisterIdentifier.NPFtoPFR;
                                item.ToNPF = null;
                                item.FromNPF = fintr.DraftAmount;
                            }
                            return item;
                        });
            }
        }


        public long GetCountTransferToNPFArchiveList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var finregisters =
                    session.CreateCriteria(typeof(AsgFinTrHib))
                        .CreateCriteria("Finregister", "fr", JoinType.InnerJoin)
                        .SetFetchMode("Finregister", FetchMode.Join);

                finregisters.CreateCriteria("CreditAccount", "ca", JoinType.InnerJoin)
                    .SetFetchMode("CreditAccount", FetchMode.Join)
                    .CreateCriteria("Contragent", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join);

                finregisters.CreateCriteria("DebitAccount", "da", JoinType.InnerJoin)
                    .SetFetchMode("DebitAccount", FetchMode.Join)
                    .CreateCriteria("Contragent", "c2", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join);

                finregisters.CreateCriteria("Register", "r", JoinType.InnerJoin)
                    .SetFetchMode("Register", FetchMode.Join)
                    .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join);

                finregisters.Add(Restrictions.Not(Restrictions.Eq("AsgFinTr.StatusID", (long)-1)));
                return finregisters.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
            }
        }


        public List<Attach> GetLinkedDocumentsListHib(long documentID)
        {
            if (documentID < 0)
                return null;

            using (var session = DataAccessSystem.OpenSession())
            {
                var attaches = session.CreateCriteria<Attach>()
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)))
                    .Add(Restrictions.Eq("DocumentID", documentID))
                    .List<Attach>();

                return attaches.ToList();
            }
        }

        public List<F040DetailsListItem> GetF040List()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //string query = "select new F040DetailsListItem(det, edo.Year, edo.Month, c.ContractNumber, le.FormalizedName, le.OldName "
                //                       + ",(select count(d1.ID) from F040Detail d1 GROUP BY d1.EdoID HAVING d1.EdoID=edo.ID)) "
                //+ " from F040Detail det "
                //+ " join det.Document edo "
                //+ " left join edo.Contract c "
                //+ " left join c.LegalEntity le "
                //+ " where ( det.Exchange not in (:emptys) or det.Broker not in (:emptys)) ";
                string query = "select det, edo.Year, edo.Month, c.ContractNumber, le.FormalizedName, le.OldName "
                               + " from F040Detail det "
                               + " join det.Document edo "
                               + " left join edo.Contract c "
                               + " left join c.LegalEntity le "
                               + " where ( det.Exchange not in (:emptys) or det.Broker not in (:emptys)) ";

                var hql = session.CreateQuery(query)
                    .SetParameterList("emptys", "|-".Split('|'));

                List<F040DetailsListItem> list = new List<F040DetailsListItem>();
                foreach (var item in hql.List<object[]>())
                {
                    var f040 = item[0] as F040Detail;
                    var year = item[1] as string;
                    var month = item[2] as string;
                    var cn = item[3] as string;
                    var fn = item[4] as string;
                    var on = item[5] as string;
                    list.Add(new F040DetailsListItem(f040, year, month, cn, fn, on, null));
                }

                return list;
            }
        }

        public List<F040DetailsListItem> GetF040ListByTypeContract(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //string query = "select new F040DetailsListItem(det, edo.Year, edo.Month, c.ContractNumber, le.FormalizedName, le.OldName "
                //                       + ",(select count(d1.ID) from F040Detail d1 GROUP BY d1.EdoID HAVING d1.EdoID=edo.ID)) "
                //+ " from F040Detail det "
                //+ " join det.Document edo "
                //+ " left join edo.Contract c "
                //+ " left join c.LegalEntity le "
                //+ " where ( det.Exchange not in (:emptys) or det.Broker not in (:emptys)) ";
                string query =
                    "select det, edo.Year, edo.Month, c.ContractNumber, le.FormalizedName, le.OldName " + " from F040Detail det " + " join det.Document edo " +
                    " left join edo.Contract c " + " left join c.LegalEntity le " + $" where c.TypeID={contractType} ";

                var hql = session.CreateQuery(query);
                //.SetParameterList("emptys", "|-".Split('|'));

                List<F040DetailsListItem> list = new List<F040DetailsListItem>();
                foreach (var item in hql.List<object[]>())
                {
                    var f040 = item[0] as F040Detail;
                    var year = item[1] as string;
                    var month = item[2] as string;
                    var cn = item[3] as string;
                    var fn = item[4] as string;
                    var on = item[5] as string;
                    list.Add(new F040DetailsListItem(f040, year, month, cn, fn, on, null));
                }

                return list;
            }
        }

        public long GetF040DetailsCount(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = "select count(*)" + " from EdoOdkF040 edo " + " left join edo.Details det  " + " left join edo.Contract c " + " left join c.LegalEntity le " +
                               $" where c.TypeID={contractType}  ";
                return session.CreateQuery(query).UniqueResult<long>();
            }
        }

        public IList<F040DetailsListItem> GetF040DetailsByPage(int contractType, int startIndex)
        {
            var lastCulture = Thread.CurrentThread.CurrentCulture;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

                using (var session = DataAccessSystem.OpenSession())
                {
                    string query =
                        $@"select det, edo.Year, edo.Month, c.ContractNumber, le.FormalizedName, le.OldName,
                           edo.CustomBuyCount, edo.CustomBuyAmount, edo.CustomSellCount, edo.CustomSellAmount 
                           from EdoOdkF040 edo 
                           left join edo.Details det  
                           left join edo.Contract c 
                           left join c.LegalEntity le 
                           where c.TypeID={contractType}   
                           order by edo.ID";

                    var data = session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize).List();

                    var list = new List<F040DetailsListItem>();
                    foreach (object[] item in data)
                    {
                        var f040 = item[0] as F040Detail;
                        var year = item[1] as string;
                        var month = item[2] as string;
                        var cn = item[3] as string;
                        var fn = item[4] as string;
                        var on = item[5] as string;
                        var det = new F040DetailsListItem(f040, year, month, cn, fn, on, null)
                        {
                            BuyCount = item[6] as long?,
                            BuyAmount = item[7] as decimal?,
                            SellCount = item[8] as long?,
                            SellAmount = item[9] as decimal?
                        };
                        list.Add(det);
                    }

                    return list;
                }

            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = lastCulture;
            }

        }

        public long GetTotalZLSummForSIRegister(long id)
        {
            var result = SessionWrapper(session => session.CreateQuery(@"SELECT coalesce(SUM(r.ZLMoveCount),0) FROM ReqTransfer r 
                                             INNER JOIN r.TransferList tr
                                             INNER JOIN tr.TransferRegister reg
                                             WHERE reg.ID = :id and r.ZLMoveCount is not null and r.StatusID <> -1").SetParameter("id", id).UniqueResult<long?>());
            return result ?? 0;
            /*string q = string.Format("SELECT SUM({3}) FROM {0}.{1} WHERE {0}.{1}.SI_REG_ID={2}",
				DATA_SCHEME, TABLE_SI_TRANSFER, id, "ZLCOUNT");

			var cmd = new DBCommandLogging(q, connection);
			cmd.Prepare();
			var reader = cmd.ExecuteReader();

			decimal retValue = (reader.Read()) ? ReadDecimalValue(reader, 0, 0) : 0;

			reader.Close();
			reader.Dispose();
			cmd.Cancel();
			cmd.Dispose();

			return retValue;*/
        }


        public IList<F040DetailsListItemLite> GetF040DetailsByPageLite(int contractType, int startIndex)
        {
            var lastCulture = Thread.CurrentThread.CurrentCulture;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

                using (var session = DataAccessSystem.OpenSession())
                {
                    //string query = string.Format("select det, "
                    string query =
                        $@"select det.ID, det.EdoID, det.Broker, det.Exchange, det.BuyCount, det.SellCount, det.BuyAmount, det.SellAmount, det.DetailID, det.DetailText, 
                              edo.Year, edo.Month, c.ContractNumber, le.FormalizedName, le.OldName 
                              from F040Detail det 
                              join det.Document edo 
                              left join edo.Contract c 
                              left join c.LegalEntity le 
                              where c.TypeID={contractType} 
                              order by det.ID";

                    var data = session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize).List();

                    var list = new List<F040DetailsListItemLite>();
                    foreach (object[] item in data)
                    {
                        //var f040 = new F040DetailLite(item[0] as F040Detail);
                        var f040 = new F040DetailLite(
                            (long)item[0],
                            item[1] as long?,
                            item[2] as string,
                            item[3] as string,
                            item[4] as decimal?,
                            item[5] as decimal?,
                            item[6] as decimal?,
                            item[7] as decimal?,
                            item[8] as long?,
                            item[9] as string
                            );

                        var year = item[10] as string;
                        var month = item[11] as string;
                        var cn = item[12] as string;
                        var fn = item[13] as string;
                        var on = item[14] as string;
                        list.Add(new F040DetailsListItemLite(f040, year, month, cn, fn, on, null));
                    }

                    return list;
                }

            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = lastCulture;
            }

        }

#region F050


        public long GetF050ReportCount(int contractType, int year = 0)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string queryCount = string.Format($@"select count(*)
												from EdoOdkF050 doc												 
												join doc.Contract contract
												left join contract.LegalEntity le
												where doc.ReportDate != null and contract.TypeID = :typeID 
                                                    and (:year = 0 OR {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}doc.ReportDate) = :year)"
                    , contractType);


                long total = session.CreateQuery(queryCount)
                    .SetParameter("typeID", contractType)
                    .SetParameter("year", year)
                    .UniqueResult<long>();

                return total;
            }
        }


        public IList<F050ReportListItem> GetF050ReportByPage(int contractType, int startIndex, int year = 0)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    $@"select doc.ID as ID, doc.ReportDate as ReportDate, contract.ContractNumber as ContractNumber, le.FormalizedName as UKName, le.OldName as UKOldName,
                                                      cast(doc.ItogoCB1 as long)  as CBCount, doc.ItogoCB2 as CBAmount
												from EdoOdkF050 doc
												join doc.Contract contract
												left join contract.LegalEntity le                                                
												where doc.ReportDate != null and contract.TypeID= :typeID 
                                                    and (:year = 0 OR {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}doc.ReportDate) = :year)
                                                order by doc.ID";




                var data = session.CreateQuery(query)
                    .SetParameter("typeID", contractType)
                    .SetParameter("year", year)
                    .SetFirstResult(startIndex).SetMaxResults(PageSize)
                    .SetResultTransformer(Transformers.AliasToBean<F050ReportListItem>())
                    .List<F050ReportListItem>();
                return data;
            }
        }


        public long GetF050CBInfoCount(int contractType, int year = 0)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string queryCount =
                    $@"select count(*)
												from F050CBInfo cb 
												join cb.Document doc 
												join doc.Contract contract
												left join contract.LegalEntity le
												where doc.ReportDate != null and contract.TypeID={contractType}";

                if (year > 0)
                    queryCount = $"{queryCount} and  {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}doc.ReportDate)={year} ";


                long total = session.CreateQuery(queryCount).UniqueResult<long>();

                return total;
            }
        }

        public IList<F050CBInfoListItem> GetF050CBInfoByPage(int contractType, int startIndex, int year = 0)
        {
            var lastCulture = Thread.CurrentThread.CurrentCulture;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

                using (var session = DataAccessSystem.OpenSession())
                {
                    string query =
                        $@"select cb, doc.ReportDate, contract.ContractNumber, le.FormalizedName, le.OldName 
							from F050CBInfo cb 
							join cb.Document doc 
							join doc.Contract contract
							left join contract.LegalEntity le
							where doc.ReportDate != null and contract.TypeID={contractType} 
                            order by cb.ID";


                    if (year > 0)
                        query = $"{query} and  {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}doc.ReportDate)={year} ";
                    query += " order by cb.ID";


                    var data = session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize).List();

                    var list = new List<F050CBInfoListItem>();
                    foreach (object[] item in data)
                    {
                        var cb = item[0] as F050CBInfo;
                        var dt = item[1] as DateTime?;
                        var sCn = item[2] as string;
                        var sFn = item[3] as string;
                        var sOn = item[4] as string;
                        list.Add(new F050CBInfoListItem(cb, dt, sCn, sFn, sOn, null));
                    }

                    return list;
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = lastCulture;
            }
        }

        public IList<F050CBInfoListItemLite> GetF050CBInfoByPageLite(int contractType, int startIndex, int year = 0)
        {
            var lastCulture = Thread.CurrentThread.CurrentCulture;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

                using (var session = DataAccessSystem.OpenSession())
                {
                    //string query = string.Format(@"select cb, doc.ReportDate, contract.ContractNumber, le.FormalizedName, le.OldName 
                    string query =
                        $@"select cb.ID, cb.EdoID,  cb.TypeID,  cb.Type,  cb.Name,  cb.Num,  cb.Count,  cb.Price, 
                                                doc.ReportDate, contract.ContractNumber, le.FormalizedName, le.OldName 
												from F050CBInfo cb 
												join cb.Document doc 
												join doc.Contract contract
												left join contract.LegalEntity le
												where doc.ReportDate != null and contract.TypeID={contractType} order by cb.ID";

                    if (year > 0)
                        query = $"{query} and  {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}(doc.ReportDate)={year}  ";
                    query += " order by cb.ID";


                    var data = session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize).List();

                    var list = new List<F050CBInfoListItemLite>();
                    foreach (object[] item in data)
                    {
                        var cb = new F050CBInfoLite(
                            (long)item[0],
                            item[1] as long?,
                            item[2] as int?,
                            item[3] as string,
                            item[4] as string,
                            item[5] as string,
                            item[6] as decimal?,
                            item[7] as decimal?
                            );
                        var dt = item[8] as DateTime?;
                        var sCn = item[9] as string;
                        var sFn = item[10] as string;
                        var sOn = item[11] as string;
                        list.Add(new F050CBInfoListItemLite(cb, dt, sCn, sFn, sOn, null));

                    }

                    return list;
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = lastCulture;
            }
        }



#endregion F050



        public List<F060DetailsListItem> GetF60ListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //                var query = session.CreateQuery(@"select f60, contract, year, le,
                //                                                (select count(_f60.ID) from EdoOdkF060 _f60 GROUP BY _f60.YearId, _f60.Quartal HAVING _f60.Quarter=f60.Quarter)
                //												from EdoOdkF060 f60 
                //												join f60.Contract contract 
                //												join f60.Year year
                //												left join contract.LegalEntity le");

                var query = session.CreateQuery(@"select f60, contract, year, le
												from EdoOdkF060 f60 
												join f60.Contract contract 
												join f60.Year year
												left join contract.LegalEntity le");

                List<F060DetailsListItem> list = new List<F060DetailsListItem>();

                foreach (var item in query.List<object[]>())
                {
                    var f60 = item[0] as EdoOdkF060;
                    var cn = item[1] as PFR_INVEST.DataObjects.Contract;
                    var yr = item[2] as Year;
                    var le = item[3] as LegalEntity;
                    list.Add(new F060DetailsListItem(f60, yr, cn, le, null));
                }
                return list;
            }
        }


        public List<F060DetailsListItem> GetF60ListByTypeContractHib(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //                var query = session.CreateQuery(@"select f60, contract, year, le,
                //                                                (select count(_f60.ID) from EdoOdkF060 _f60 GROUP BY _f60.YearId, _f60.Quartal HAVING _f60.Quarter=f60.Quarter)
                //												from EdoOdkF060 f60 
                //												join f60.Contract contract 
                //												join f60.Year year
                //												left join contract.LegalEntity le");

                var query = session.CreateQuery($@"select f60, contract, year, le
												from EdoOdkF060 f60 
												join f60.Contract contract 
												join f60.Year year
												left join contract.LegalEntity le 
                                                where contract.TypeID={contractType}");

                List<F060DetailsListItem> list = new List<F060DetailsListItem>();

                foreach (var item in query.List<object[]>())
                {
                    var f60 = item[0] as EdoOdkF060;
                    var cn = item[1] as PFR_INVEST.DataObjects.Contract;
                    var yr = item[2] as Year;
                    var le = item[3] as LegalEntity;
                    list.Add(new F060DetailsListItem(f60, yr, cn, le, null));
                }
                return list;
            }
        }



        public List<F070DetailsListItem> GetF70ListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //                var query = session.CreateQuery(@"select f70, contract, year, le,
                //                                                (select count(_f70.ID) from EdoOdkF070 _f70 GROUP BY _f70.YearId, _f70.Quartal HAVING _f70.Quarter=f70.Quarter)
                //												from EdoOdkF070 f70 
                //												join f70.Contract contract 
                //												join f70.Year year
                //												left join contract.LegalEntity le");

                var query = session.CreateQuery(@"select f70, contract, year, le
												from EdoOdkF070 f70 
												join f70.Contract contract 
												join f70.Year year
												left join contract.LegalEntity le");

                List<F070DetailsListItem> list = new List<F070DetailsListItem>();

                foreach (var item in query.List<object[]>())
                {
                    var f70 = item[0] as EdoOdkF070;
                    var cn = item[1] as PFR_INVEST.DataObjects.Contract;
                    var yr = item[2] as Year;
                    var le = item[3] as LegalEntity;
                    list.Add(new F070DetailsListItem(f70, yr, cn, le, null));
                }
                return list;
            }
        }


        public long GetCountF70ListByTypeContractHib(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                return session.CreateQuery($@"select count (*) 
												from EdoOdkF070 f70 
												join f70.Contract contract 
												join f70.Year year
												left join contract.LegalEntity le 
                                                where contract.TypeID={contractType}").UniqueResult<long>();

            }
        }


        public List<F070DetailsListItem> GetF70ListByTypeContractHib(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //                var query = session.CreateQuery(@"select f70, contract, year, le,
                //                                                (select count(_f70.ID) from EdoOdkF070 _f70 GROUP BY _f70.YearId, _f70.Quartal HAVING _f70.Quarter=f70.Quarter)
                //												from EdoOdkF070 f70 
                //												join f70.Contract contract 
                //												join f70.Year year
                //												left join contract.LegalEntity le");

                var query = session.CreateQuery($@"select f70, contract, year, le
												from EdoOdkF070 f70 
												join f70.Contract contract 
												join f70.Year year
												left join contract.LegalEntity le 
                                                where contract.TypeID={contractType}");

                List<F070DetailsListItem> list = new List<F070DetailsListItem>();

                foreach (var item in query.List<object[]>())
                {
                    var f70 = item[0] as EdoOdkF070;
                    var cn = item[1] as PFR_INVEST.DataObjects.Contract;
                    var yr = item[2] as Year;
                    var le = item[3] as LegalEntity;
                    list.Add(new F070DetailsListItem(f70, yr, cn, le, null));
                }
                return list;
            }
        }

        public List<F070DetailsListItem> GetF70ListByTypeContractPageHib(int contractType, int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                var query = session.CreateQuery(
                    $@"select f70, contract, year, le
												from EdoOdkF070 f70 
												join f70.Contract contract 
												join f70.Year year
												left join contract.LegalEntity le 
                                                where contract.TypeID={contractType} order by f70.ID");

                var list = new List<F070DetailsListItem>();
                var listResult = query.SetFirstResult(startIndex).SetMaxResults(PageSize).List();
                foreach (object[] item in listResult)
                {
                    var f70 = item[0] as EdoOdkF070;
                    var cn = item[1] as PFR_INVEST.DataObjects.Contract;
                    var yr = item[2] as Year;
                    var le = item[3] as LegalEntity;
                    list.Add(new F070DetailsListItem(f70, yr, cn, le, null));
                }
                return list;
            }
        }

        public int GetCountSelectDateDepClaimSelectParams(DateTime date)
        {
            try
            {
                return SessionWrapper(session => session.CreateSQLQuery($@"SELECT COUNT(ID) FROM {DATA_SCHEME}.DEPCLAIMSELECTPARAMS 
                 WHERE {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}SELECT_DATE)={date.Year} AND 
                       {(IsDB2 ? "MONTH(" : "EXTRACT( MONTH FROM ")}SELECT_DATE)={date.Month} AND 
                       {(IsDB2 ? "DAY(" : "EXTRACT( DAY FROM ")}SELECT_DATE)={date.Day} AND STATUS > -1 ").UniqueResult<int>());
            }
            catch (Exception e)
            {
                LogManager.LogException(e);
                return -1;
            }
        }

        public int GetMaxAuctionNumDepClaimSelectParams(int year)
        {
            try
            {
                //http://jira.dob.datateh.ru/browse/DOKIPIV-460 убрали проверку статуса, т.к. атоматически предлагался номер уже существующий (удаленный)
                return
                    SessionWrapper(
                        session =>
                            session.CreateSQLQuery(
                                $@"SELECT MAX(AUCTIONNUM) FROM {DATA_SCHEME}.DEPCLAIMSELECTPARAMS 
                    WHERE {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}SELECT_DATE)={year} ")
                                .UniqueResult<int>());
            }
            catch (Exception ex)
            {
                LogManager.LogException(ex);
                return -1;
            }
        }


        public List<F080DetailsListItem> GetF80ListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"select f80, d.ReportOnDate
												from F80Deal f80
												join f80.Document d");

                return (from item in query.List<object[]>() let f80 = item[0] as F80Deal let d = item[1] as DateTime? select new F080DetailsListItem(f80, d, null)).ToList();
            }
        }

        public List<F080DetailsListItem> GetF80ListByContractTypeHib(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"select f80, d.ReportOnDate
												from F80Deal f80
												join f80.Document d where f80.ContractType={contractType}");

                return (from item in query.List<object[]>() let f80 = item[0] as F80Deal let d = item[1] as DateTime? select new F080DetailsListItem(f80, d, null)).ToList();
            }
        }



        public IList<Curs> GetCurrentCursesHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var subCreateria = DetachedCriteria.For<Curs>("cc");
                //var proj_lower = Projections.SqlFunction("lower", NHibernateUtil.String, Projections.Property("cr.Name"));

                subCreateria
                    .SetProjection(Property.ForName("Date").Max())
                    .Add(Property.ForName("CurrencyID").EqProperty("cr.ID"));
                //.Add(Expression.Like(proj_lower, "рубли", MatchMode.Anywhere));

                return session.CreateCriteria(typeof(Curs))
                    .CreateAlias("Currency", "cr", JoinType.InnerJoin)
                    .Add(Subqueries.PropertyIn("Date", subCreateria))
                    //                    .CreateQuery(@"
                    //                    select c from Curs c 
                    //                    join c.Currency cr").IsIn(
                    //                    where c.Date IN (
                    //                        select MAX(cc.Date) from Curs cc where cc.CurrencyID = cr.ID || LOWER(cr.Name) LIKE '%рубли%'
                    //                    )
                    //")
                    //")
                    .List<Curs>();
            }
        }


        public IList<Curs> GetOnDateCurrentCursesHibForLine30(DateTime thisDate, int cursType)
        {
            /*return SessionWrapper(session => session.CreateQuery(string.Format(@"SELECT с FROM Curs c 
											 JOIN c.Currency cr
											 WHERE c.Date in (SELECT max(c1.Date) as y0_ FROM Curs c1 WHERE (c1.CurrencyID = cr.ID and (lower(cr.Name) like '{0}' or c1.Date <= :date)))
											 and c.CurrencyID = :cursType and c.Value is not null", "%рубли%")
				)
				.SetParameter("date", thisDate)
				.SetParameter("cursType", cursType)
				.List<Curs>());
			*/
            using (var session = DataAccessSystem.OpenSession())
            {
                var subCreateria = DetachedCriteria.For<Curs>("cc");
                var projLower = Projections.SqlFunction("lower", NHibernateUtil.String, Projections.Property("cr.Name"));
                var left = Restrictions.Or(Restrictions.Like(projLower, "рубли", MatchMode.Anywhere),
                    Property.ForName("Date").Le(thisDate));

                subCreateria
                    .SetProjection(Property.ForName("Date").Max())
                    .Add(Restrictions.And(Property.ForName("CurrencyID").EqProperty("cr.ID"), left));

                return session.CreateCriteria(typeof(Curs))
                    .CreateAlias("Currency", "cr", JoinType.InnerJoin)
                    .Add(Subqueries.PropertyIn("Date", subCreateria))
                    .Add(Restrictions.And(Property.ForName("CurrencyID").Eq((long)cursType), Property.ForName("Value").IsNotNull()))
                    .List<Curs>();
            }
        }

        public IList<Curs> GetOnDateCurrentCursesHib(DateTime thisDate)
        {
            return SessionWrapper(session => session.CreateQuery(@"
                                             SELECT c FROM Curs c 
											 INNER JOIN c.Currency cr
											 WHERE c.Date in (
                                SELECT max(c1.Date)  
                                  FROM Curs c1
                                  inner join c1.Currency cr1
                                    WHERE lower(cr1.Name) like '%рубли%' or c1.Date <= :date)")
				.SetParameter("date", thisDate)
				.List<Curs>());
			
            /*using (var session = DataAccessSystem.OpenSession())
            {
                var subCreateria = DetachedCriteria.For<Curs>("cc");
                var projLower = Projections.SqlFunction("lower", NHibernateUtil.String, Projections.Property("cr.Name"));
                var left = Restrictions.Or(Restrictions.Like(projLower, "рубли", MatchMode.Anywhere),
                    Property.ForName("Date").Le(thisDate));

                subCreateria
                    .SetProjection(Property.ForName("Date").Max())
                    .Add(Restrictions.And(Property.ForName("CurrencyID").EqProperty("cr.ID"), left));

                return session.CreateCriteria(typeof(Curs))
                    .CreateAlias("Currency", "cr", JoinType.InnerJoin)
                    .Add(Subqueries.PropertyIn("Date", subCreateria))
                    .List<Curs>();
            }*/
        }

        public List<RatesListItem> GetRatesListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var criteria = session.CreateCriteria(typeof(CursHib), "curs")
                    .AddOrder(Order.Asc("Date"))
                    .CreateCriteria("Currency", "curr", JoinType.InnerJoin)
                    .SetFetchMode("Currency", FetchMode.Join);

                var months = GetList(typeof(Month).FullName).Cast<Month>();

                return criteria.List<CursHib>().ToList().ConvertAll(
                    curs => new RatesListItem
                    {
                        ID = curs.ID,
                        Currency = curs.Currency.Name,
                        Date = curs.Date,
                        Year = curs.Date.Value.Year,
                        Rate = curs.Value,
                        Month = (from month in months
                                 where month.ID == curs.Date.Value.Month
                                 select month.Name).First()
                    }
                    );
            }
        }

        /// <summary>
        /// Возвращает список "пропущенных курсов".
        /// </summary>
        /// <returns></returns>
        public List<RatesListItem> GetMissedRatesListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //списко валют кроме рублей упорядоченых по идентификатору
                IList<Currency> currencies = session.CreateCriteria(typeof(Currency), "cur")
                    .AddOrder(Order.Asc("ID"))
                    .Add(Restrictions.Not(Restrictions.Eq("ID", (long)1 /*Рубли*/)))
                    .List<Currency>();

                //список курсов упорядоченых по дате и валюте (кроме рублей)
                IList<Curs> curses = session.CreateCriteria(typeof(Curs), "curs")
                    .AddOrder(Order.Asc("Date"))
                    .AddOrder(Order.Asc("CurrencyID"))
                    .SetProjection( // выбираем только уникальные комбинации даты и валюты (во избежании корявых данных)
                        Projections.Distinct(
                            Projections.ProjectionList()
                                .Add(Projections.Alias(Projections.Property("CurrencyID"), "CurrencyID"))
                                .Add(Projections.Alias(Projections.Property("Date"), "Date"))
                            )
                    )
                    .Add(Restrictions.Not(Restrictions.Eq("CurrencyID", (long)1 /*Рубли*/))) //кроме рублей
                    .SetResultTransformer(new AliasToBeanResultTransformer(typeof(Curs)))
                    .List<Curs>();

                if (curses.Count == 0 || currencies.Count == 0) return null;

                DateTime date = curses.First().Date.Value;
                DateTime today = DateTime.Today;

                IEnumerator<Currency> currency = currencies.GetEnumerator();
                IEnumerator<Curs> curs = curses.GetEnumerator();
                curs.MoveNext();
                CultureInfo ruCultureInfo = CultureInfo.GetCultureInfo("ru-RU");
                List<RatesListItem> result = new List<RatesListItem>();
                while (date <= today) //перебираем все даты начиная с самой мало до сегоднешнего дня
                {
                    while (currency.MoveNext()) //перебираем все валюты
                    {
                        //перебираем все элементы по порядку, так как колекция отсортирована
                        //не совпадающие элементы добавляем в колекцию "пропущеных куросв"
                        if (curs.Current != null && date == curs.Current.Date &&
                            currency.Current.ID == curs.Current.CurrencyID)
                            curs.MoveNext();
                        else
                        {
                            //для автоматического заполнения пропущенных дат раскоментить строку ниже
                            //Save("Curs", new Curs() { CurrencyID = currency.Current.ID, Value = 30, Date = date });

                            result.Add(new RatesListItem
                            {
                                ID = 0,
                                Currency = currency.Current.Name,
                                Date = date,
                                Year = date.Year,
                                Rate = null,
                                Month = ruCultureInfo.DateTimeFormat.MonthNames[date.Month - 1]
                            });
                        }


                    }
                    currency.Reset();
                    date = date.AddDays(1);
                }
                return result;
            }
        }

#endregion

        public IList<F050CBInfo> GetCBInfosListForF50(long edoId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria<F050CBInfo>()
                    .Add(Restrictions.Eq("EdoID", edoId))
                    .AddOrder(Order.Asc("TypeID"))
                    .List<F050CBInfo>();

                return list;
            }
        }

        public IList<F040Detail> GetF040Details(long edoId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var dict = session.CreateCriteria<DetailsKind>().List<DetailsKind>()
                    .ToDictionary(item => item.ID, item => item.Name);

                //var list = session.CreateQuery("select det from F040Detail det where EdoID = :id and ( Exchange not in (:emptys) or Broker not in (:emptys)) ")
                //.SetParameter("id", EdoId)
                //.SetParameterList("emptys", "|-".Split('|')).List<F040Detail>();
                var list = session.CreateQuery("select det from F040Detail det where EdoID = :id ")
                    .SetParameter("id", edoId).List<F040Detail>();


                foreach (var item in list)
                {
                    if (item.DetailID.HasValue)
                        item.DetailText = dict[item.DetailID.Value];
                }
                return list;
            }
        }

        public IList<F040Rrz> GetF040Rrzs(long edoId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var dict = session.CreateCriteria<RrzKind>().List<RrzKind>()
                    .ToDictionary(item => item.ID, item => item.Name);

                //var list = session.CreateQuery("select det from F040Rrz det where EdoID = :id and ( SellCount > :zerro or BuyCount > :zerro) ")
                //.SetParameter("id", EdoId)
                //.SetParameter("zerro", 0M).List<F040Rrz>();
                var list = session.CreateQuery("select det from F040Rrz det where EdoID = :id ")
                    .SetParameter("id", edoId).List<F040Rrz>();

                foreach (var item in list)
                {
                    if (item.RrzID.HasValue)
                        item.RrzText = dict[item.RrzID.Value];
                }
                return list;
            }
        }

#region F010-F015

        private IList<NetWealthListItem> NetWealthsListGet(string query, string tableName)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(query).List<object[]>();
                return (from object[] item in list
                        select
                            new NetWealthListItem((long)item[0], item[1] as DateTime?, item[2] as decimal?, item[3] as long?, item[4] as string, item[5] as string, item[6] as string)
                            {
                                Table = tableName
                            }).ToList();
            }
        }

        private IList<NetWealthListItem> NetWealthsListGetByPage(string query, int startIndex, string tableName)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize).List();
                return (from object[] item in list
                    select
                        new NetWealthListItem((long) item[0], item[1] as DateTime?, item[2] as decimal?, item[3] as long?, item[4] as string, item[5] as string, item[6] as string)
                        {
                            Table = tableName
                        }).ToList();
            }
        }

        private IList<NetWealthListItem> NetWealthsListGetByPage(IQuery query, int startIndex, string tableName)
        {
            var list = query.SetFirstResult(startIndex).SetMaxResults(PageSize).List();
            return (from object[] item in list
                    select
                        new NetWealthListItem((long)item[0], item[1] as DateTime?, item[2] as decimal?, item[3] as long?, item[4] as string, item[5] as string, item[6] as string)
                        {
                            Table = tableName
                        }).ToList();
        }

        public long GetCountNetWealthsSumF010(DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = "select count(*) "
                               + " from EdoOdkF010 edo "
                               + " left join edo.Contract c "
                               + " left join c.LegalEntity le"
                               + " where edo.IsTotalReport = 1 "
                               + " and (:from is null OR edo.ReportOnDate >= :from)  "
                               + " and (:to is null OR edo.ReportOnDate <= :to)";

                return
                    session.CreateQuery(query)
                        .SetParameter("from", periodStart)
                        .SetParameter("to", periodEnd)
                        .UniqueResult<long>();


            }
        }

        public IList<NetWealthListItem> GetNetWealthsSumF010ListByOnlyPage(int startIndex, DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                const string hql = @"select edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                      from EdoOdkF010 edo  
                      left join edo.Contract c  
                      left join c.LegalEntity le 
                      where  edo.IsTotalReport = 1 
                      and (:from is null OR edo.ReportOnDate >= :from)  
                      and (:to is null OR edo.ReportOnDate <= :to)
                      order by edo.ID";
                var query = session.CreateQuery(hql)
                    .SetParameter("from", periodStart)
                    .SetParameter("to", periodEnd);


                return NetWealthsListGetByPage(query, startIndex, "EDO_ODKF010");
            }
        }

        public long GetCountNetWealthsF010()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = "select count(*) "
                               + " from EdoOdkF010 edo "
                               + " left join edo.Contract c "
                               + " left join c.LegalEntity le";
                return session.CreateQuery(query).UniqueResult<long>();
            }

        }

        public IList<NetWealthListItem> GetNetWealthsF010List()
        {
            //string query = "select new NetWealthListItem(edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName) "
            //+ " from EdoOdkF010 edo "
            //+ " left join edo.Contract c "
            //+ " left join c.LegalEntity le";
            string query = "select edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName "
                           + " from EdoOdkF010 edo "
                           + " left join edo.Contract c "
                           + " left join c.LegalEntity le";

            return NetWealthsListGet(query, "EDO_ODKF010");
        }

        public IList<NetWealthListItem> GetNetWealthsF010ListByTypeContract(int contractType)
        {
            //string query = "select new NetWealthListItem(edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName) "
            //+ " from EdoOdkF010 edo "
            //+ " left join edo.Contract c "
            //+ " left join c.LegalEntity le";
            string query =
                "select edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName " + " from EdoOdkF010 edo " + " left join edo.Contract c " +
                " left join c.LegalEntity le" + $" where c.TypeID={contractType}";

            return NetWealthsListGet(query, "EDO_ODKF010");
        }


        public long GetCountNetWealthsF010ListByTypeContract(int contractType, DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (periodStart.HasValue && periodEnd.HasValue)
                {
                    string query =
                        string.Format(
                            "select count(*)" + " from EdoOdkF010 edo " + " left join edo.Contract c "
                            + " left join c.LegalEntity le" + " where c.TypeID=:typeID "
                            + " and (edo.ReportOnDate >= :from  and edo.ReportOnDate <= :to)");


                    return session.CreateQuery(query)
                        .SetParameter("typeID", contractType)
                        .SetParameter("from", periodStart)
                        .SetParameter("to", periodEnd).UniqueResult<long>();
                }
                else
                {
                    string query =
                        "select count(*)" + " from EdoOdkF010 edo " + " left join edo.Contract c " + " left join c.LegalEntity le" + $" where c.TypeID={contractType}";
                    return session.CreateQuery(query).UniqueResult<long>();

                }

            }
        }



        public IList<NetWealthListItem> GetNetWealthsF010ListByPage(int contractType, int startIndex,
            DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            string query;
            if (periodStart.HasValue && periodEnd.HasValue)
            {
                query =
                    $@"select edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                          from EdoOdkF010 edo 
                          left join edo.Contract c 
                          left join c.LegalEntity le
                          where c.TypeID={contractType}
                          and  (edo.ReportOnDate  BETWEEN '{periodStart
                        .Value.ToString("yyyy-MM-dd")}' AND '{periodEnd.Value.ToString("yyyy-MM-dd")}')
                          order by edo.ID";
            }
            else
            {
                query =
                    $@"select edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                          from EdoOdkF010 edo 
                          left join edo.Contract c 
                          left join c.LegalEntity le
                          where c.TypeID={contractType}
                          order by edo.ID";
            }

            return NetWealthsListGetByPage(query, startIndex, "EDO_ODKF010");
        }

        public IList<NetWealthListItem> GetNetWealthsF010ListByOnlyPage(int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                const string query = @"select edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                                    from EdoOdkF010 edo 
                                    left join edo.Contract c 
                                    left join c.LegalEntity le 
                                    order by edo.ID";
                return NetWealthsListGetByPage(query, startIndex, "EDO_ODKF010");
            }
        }

        public IList<NetWealthListItem> GetNetWealthsF010ListNullContract()
        {
            //var list = session.CreateQuery("select new NetWealthListItem(edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName) "
            //+ " from EdoOdkF010 edo "
            //+ " left join edo.Contract c "
            //+ " left join c.LegalEntity le "
            //+ " where c is null"
            //+ " and le.FormalizedName is not null").List<NetWealthListItem>();
            const string query =
                "select edo.ID, edo.ReportOnDate, edo.ItogoScha1, c.ID, c.ContractNumber, le.FormalizedName, le.OldName "
                + " from EdoOdkF010 edo " + " left join edo.Contract c " + " left join c.LegalEntity le "
                + " where c is null" + " and le.FormalizedName is not null";

            return NetWealthsListGet(query, "EDO_ODKF010");
        }

        public IList<NetWealthListItem> GetNetWealthsF015List()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = "select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName "
                               + " from EdoOdkF015 edo "
                               + " left join edo.Contract c "
                               + " left join c.LegalEntity le"
                               + " where le.FormalizedName is not null";
                return NetWealthsListGet(query, "EDO_ODKF015");
            }
        }

        public IList<NetWealthListItem> GetNetWealthsF015ListByTypeContract(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    "select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName " + " from EdoOdkF015 edo " + " left join edo.Contract c " +
                    " left join c.LegalEntity le" + $" where le.FormalizedName is not null and c.TypeID={contractType}";
                return NetWealthsListGet(query, "EDO_ODKF015");
            }
        }



        public long GetCountNetWealthsF015ListByTypeContract(int contractType, DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (periodStart.HasValue && periodEnd.HasValue)
                {
                    const string query = @"select count(*) 
                                        from EdoOdkF015 edo 
                                        left join edo.Contract c 
                                        left join c.LegalEntity le
                                        where c.TypeID=:typeID 
                                        and (edo.ReportOnDate >= :from  and edo.ReportOnDate <= :to)";

                    return session.CreateQuery(query)
                        .SetParameter("typeID", contractType)
                        .SetParameter("from", periodStart)
                        .SetParameter("to", periodEnd).UniqueResult<long>();
                }
                else
                {
                    string query =
                        $@"select count(*) 
                            from EdoOdkF015 edo 
                            left join edo.Contract c 
                            left join c.LegalEntity le
                            where le.FormalizedName is not null and c.TypeID={contractType}";
                    return session.CreateQuery(query).UniqueResult<long>();
                }

            }
        }



        public IList<NetWealthListItem> GetNetWealthsF015ListByPage(int contractType, int startIndex,
            DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            string query;
            if (periodStart.HasValue && periodEnd.HasValue)
            {
                query =
                    $@"select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                              from EdoOdkF015 edo 
                              left join edo.Contract c 
                              left join c.LegalEntity le
                              where le.FormalizedName is not null and c.TypeID={contractType} 
                              and  (edo.ReportOnDate  BETWEEN '{periodStart
                        .Value.ToString("yyyy-MM-dd")}' AND '{periodEnd.Value.ToString("yyyy-MM-dd")}')
                              order by edo.ID";

            }
            else
            {
                query =
                    $@"select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                        from EdoOdkF015 edo  
                        left join edo.Contract c 
                        left join c.LegalEntity le
                        where le.FormalizedName is not null and c.TypeID={contractType}
                        order by edo.ID";

            }


            return NetWealthsListGetByPage(query, startIndex, "EDO_ODKF015");
        }

        public IList<int> GetNetWealthsF015YearsList(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query =
                    $@"select distinct edo.ReportOnDate 
                          from EdoOdkF015 edo left join edo.Contract c left join c.LegalEntity le
                          where le.FormalizedName is not null and c.TypeID={contractType}";

                var list = session.CreateQuery(query).List<DateTime?>();
                return list.Select(x => x.Value.Year).Distinct().ToList();
            }
        }

        public IList<int> GetNetWealthsF010YearsList(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query =
                    "select distinct edo.ReportOnDate " + " from EdoOdkF010 edo " + " left join edo.Contract c " + " left join c.LegalEntity le" +
                    $" where le.FormalizedName is not null and c.TypeID={contractType}";

                var list = session.CreateQuery(query).List<DateTime?>();
                return list.Select(x => x.Value.Year).Distinct().ToList();
            }
        }

#endregion

#region F012, F014, F016


        public long GetCountNetWealthsSumF012(DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                string query = "select count(*) "
                               + " from EdoOdkF012 edo "
                               + " left join edo.Contract c "
                               + " left join c.LegalEntity le"
                               + " where edo.IsTotalReport = 1"
                               + " and (:from is null OR edo.ReportOnDate >= :from)  "
                               + " and (:to is null OR edo.ReportOnDate <= :to)";

                return
                    session.CreateQuery(query)
                        .SetParameter("from", periodStart)
                        .SetParameter("to", periodEnd)
                        .UniqueResult<long>();


            }
        }


        public IList<NetWealthListItem> GetNetWealthsSumF012ListByPage(int startIndex, DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {

            using (var session = DataAccessSystem.OpenSession())
            {
                const string hql = @"select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                               from EdoOdkF012 edo  left join edo.Contract c  left join c.LegalEntity le
                               where edo.IsTotalReport = 1
                               and (:from is null OR edo.ReportOnDate >= :from)  
                               and (:to is null OR edo.ReportOnDate <= :to)
                               order by edo.ID";
                var query = session.CreateQuery(hql)
                    .SetParameter("from", periodStart)
                    .SetParameter("to", periodEnd);
                
                return NetWealthsListGetByPage(query, startIndex, "EDO_ODKF012");
            }
        }


        public long GetCountNetWealthsF012(DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (periodStart.HasValue && periodEnd.HasValue)
                {
                    string query = "select count(*) "
                                   + " from EdoOdkF012 edo "
                                   + " left join edo.Contract c "
                                   + " left join c.LegalEntity le"
                                   + " where le.FormalizedName is not null"
                                   + " and (edo.ReportOnDate >= :from  and edo.ReportOnDate <= :to)";

                    return
                        session.CreateQuery(query)
                            .SetParameter("from", periodStart)
                            .SetParameter("to", periodEnd)
                            .UniqueResult<long>();
                }
                else
                {
                    string query = "select count(*) "
                                   + " from EdoOdkF012 edo "
                                   + " left join edo.Contract c "
                                   + " left join c.LegalEntity le"
                                   + " where le.FormalizedName is not null";
                    return session.CreateQuery(query).UniqueResult<long>();
                }
            }
        }

        public IList<NetWealthListItem> GetNetWealthsF012List()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = "select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName "
                               + " from EdoOdkF012 edo "
                               + " left join edo.Contract c "
                               + " left join c.LegalEntity le"
                               + " where le.FormalizedName is not null";
                return NetWealthsListGet(query, "EDO_ODKF012");
            }
        }


        public IList<NetWealthListItem> GetNetWealthsF012ListByPage(int startIndex, DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {

            string query;
            if (periodStart.HasValue && periodEnd.HasValue)
            {
                query =
                    $@"select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                              from EdoOdkF012 edo  left join edo.Contract c  left join c.LegalEntity le
                              where le.FormalizedName is not null
                              and  (edo.ReportOnDate  BETWEEN '{periodStart
                        .Value.ToString("yyyy-MM-dd")}' AND '{periodEnd.Value.ToString("yyyy-MM-dd")}')
                              order by edo.ID";
            }
            else
            {
                query = @"select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                              from EdoOdkF012 edo  left join edo.Contract c  left join c.LegalEntity le
                              where le.FormalizedName is not null
                              order by edo.ID";
            }

            return NetWealthsListGetByPage(query, startIndex, "EDO_ODKF012");
        }

        public IList<NetWealthListItem> GetNetWealthsF012ListByTypeContract(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    $@"select edo.ID, edo.ReportOnDate, edo.A090, c.ID, c.ContractNumber, le.FormalizedName, le.OldName 
                          from EdoOdkF012 edo 
                          left join edo.Contract c 
                          left join c.LegalEntity le
                          where le.FormalizedName is not null and c.TypeID={contractType}";
                return NetWealthsListGet(query, "EDO_ODKF012");
            }
        }

        public long GetCountNetWealthsF014(DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (periodStart.HasValue && periodEnd.HasValue)
                {
                    string query =
                        "select count(*) from EdoOdkF014 edo where (edo.ReportOnDate >= :from  and edo.ReportOnDate <= :to) ";
                    return session.CreateQuery(query)
                        .SetParameter("from", periodStart)
                        .SetParameter("to", periodEnd).UniqueResult<long>();
                }
                else
                {
                    string query = "select count(*) from EdoOdkF014 edo ";
                    return session.CreateQuery(query).UniqueResult<long>();

                }
            }
        }

        public IList<NetWealthListItem> GetNetWealthsF014List()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = "select edo.ID, edo.ReportOnDate, edo.A090 from EdoOdkF014 edo ";
                var list = session.CreateQuery(query);
                List<NetWealthListItem> items = new List<NetWealthListItem>();
                foreach (var item in list.List<object[]>())
                {
                    long did = (long)item[0];
                    var rdt = item[1] as DateTime?;
                    var a90 = item[2] as decimal?;
                    NetWealthListItem i = new NetWealthListItem(did, rdt, a90);
                    items.Add(i);
                    i.Table = "EDO_ODKF014";
                }
                return items;
            }
        }

        public IList<NetWealthListItem> GetNetWealthsF014ListByPage(int startIndex, DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {

            using (var session = DataAccessSystem.OpenSession())
            {
                string query;
                if (periodStart.HasValue && periodEnd.HasValue)
                {
                    query =
                        $@"select edo.ID, edo.ReportOnDate, edo.A090 from EdoOdkF014 edo 
                                           where  (edo.ReportOnDate  BETWEEN '{periodStart
                            .Value.ToString("yyyy-MM-dd")}' AND '{periodEnd.Value.ToString("yyyy-MM-dd")}') order by edo.ID";
                }
                else
                    query = "select edo.ID, edo.ReportOnDate, edo.A090 from EdoOdkF014 edo order by edo.ID";

                var list = session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize);
                var items = new List<NetWealthListItem>();
                foreach (var item in list.List<object[]>())
                {
                    long did = (long)item[0];
                    var rdt = item[1] as DateTime?;
                    var a90 = item[2] as decimal?;
                    NetWealthListItem i = new NetWealthListItem(did, rdt, a90);
                    items.Add(i);
                    i.Table = "EDO_ODKF014";
                }
                return items;
            }
        }

        public long GetCountNetWealthsF016(DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (periodStart.HasValue && periodEnd.HasValue)
                {
                    const string query = "select count(*) from EdoOdkF016 edo where (edo.ReportOnDate >= :from  and edo.ReportOnDate <= :to) ";
                    return session.CreateQuery(query)
                        .SetParameter("from", periodStart)
                        .SetParameter("to", periodEnd).UniqueResult<long>();
                }
                else
                {
                    const string query = "select count(*) from EdoOdkF016 edo ";
                    return session.CreateQuery(query).UniqueResult<long>();
                }
            }
        }


        public IList<NetWealthListItem> GetNetWealthsF016List()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                const string query = "select edo.ID, edo.ReportOnDate, edo.A090 from EdoOdkF016 edo ";
                var list = session.CreateQuery(query).List<object[]>();
                return list.Select(item => new NetWealthListItem((long)item[0], item[1] as DateTime?, item[2] as decimal?) { Table = "EDO_ODKF016" }).ToList();
            }

        }

        public IList<NetWealthListItem> GetNetWealthsF016ListByPage(int startIndex, DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {

            using (var session = DataAccessSystem.OpenSession())
            {
                string query;
                if (periodStart.HasValue && periodEnd.HasValue)
                    query =
                        $@"select edo.ID, edo.ReportOnDate, edo.A090 from EdoOdkF016 edo where  (edo.ReportOnDate  BETWEEN '{periodStart.Value.ToString("yyyy-MM-dd")}' AND '{periodEnd
                            .Value.ToString("yyyy-MM-dd")}') order by edo.ID";
                else
                    query = "select edo.ID, edo.ReportOnDate, edo.A090 from EdoOdkF016 edo order by edo.ID";

                var list = session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>();
                return list.Select(item => new NetWealthListItem((long) item[0], item[1] as DateTime?, item[2] as decimal?) {Table = "EDO_ODKF016"}).ToList();
            }
        }

#endregion

#region MarketCost


        private static IList<MarketCostListItem> MarketCostListGet(string query, MarketCostIdentifier.Forms type)
        {
            return SessionWrapper(session =>
            {
                var list = session.CreateQuery(query).List<object[]>();
                return
                    list.Select(
                        item =>
                            new MarketCostListItem((long) item[0], item[1] as DateTime?,
                                Convert.ToDecimal(item[2] ?? 0m), (string) item[3], (string) item[4], (string) item[5])
                            {
                                FormType = type
                            }).ToList();
            });
        }

        private IList<MarketCostListItem> MarketCostListGetByPage(IQuery query, MarketCostIdentifier.Forms type,
            int startIndex)
        {
            var list = query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>();
            return
                list.Select(
                    item =>
                        new MarketCostListItem((long) item[0], item[1] as DateTime?, Convert.ToDecimal(item[2] ?? 0m),
                            (string) item[3], (string) item[4], (string) item[5]) {FormType = type}).ToList();
        }

        private IList<MarketCostListItem> MarketCostListGetByPage(string query, MarketCostIdentifier.Forms type,
            int startIndex)
        {
            return SessionWrapper(session =>
            {
                var list =
                    session.CreateQuery(query).SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>();
                return
                    list.Select(
                        item =>
                            new MarketCostListItem((long) item[0], item[1] as DateTime?,
                                Convert.ToDecimal(item[2] ?? 0m), (string) item[3], (string) item[4], (string) item[5])
                            {
                                FormType = type
                            }).ToList();
            });
        }

#region F020


        public long GetCountMarketCostF020Filtered(int contractType, DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    $@"select count(*) from EdoOdkF020 edo 
                           left join edo.Contract c 
                           left join c.LegalEntity le
                           where le.FormalizedName is not null and c.TypeID={contractType} 
                           AND (:from is null OR edo.ReportOnDate >= :from) 
                           AND (:to is null OR edo.ReportOnDate <= :to) ";
                return session.CreateQuery(query)
                    .SetParameter("from", from)
                    .SetParameter("to", to)
                    .UniqueResult<long>();
            }
        }


        public IList<MarketCostListItem> GetMarketCostF020ListFilteredByPage(int contractType, int startIndex,
            DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    $@"select edo.ID, edo.ReportOnDate, edo.Sumactiv1, c.ContractNumber, le.FormalizedName, le.OldName
                          from EdoOdkF020 edo
                          left join edo.Contract c
                          left join c.LegalEntity le
                          where le.FormalizedName is not null and c.TypeID={contractType}
                          AND (:from is null OR edo.ReportOnDate >= :from) 
                          AND (:to is null OR edo.ReportOnDate <= :to) 
                          order by edo.ID";
                var crit = session.CreateQuery(query)
                    .SetParameter("from", from)
                    .SetParameter("to", to);
                return MarketCostListGetByPage(crit, 0, startIndex);
            }
        }


        public IList<MarketCostListItem> GetMarketCostF020List()
        {
            const string query = @"select edo.ID, edo.ReportOnDate, edo.Sumactiv1, c.ContractNumber, le.FormalizedName, le.OldName 
                                   from EdoOdkF020 edo 
                                   left join edo.Contract c 
                                   left join c.LegalEntity le
                                   where le.FormalizedName is not null";
            return MarketCostListGet(query, MarketCostIdentifier.Forms.F20);
        }

        public IList<MarketCostListItem> GetMarketCostF020ListByTypeContract(int contractType)
        {
            string query =
                $@"select edo.ID, edo.ReportOnDate, edo.Sumactiv1, c.ContractNumber, le.FormalizedName, le.OldName
                      from EdoOdkF020 edo 
                      left join edo.Contract c 
                      left join c.LegalEntity le
                      where le.FormalizedName is not null and c.TypeID={contractType}";
            return MarketCostListGet(query, MarketCostIdentifier.Forms.F20);
        }

        public long GetCountMarketCostF020(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query =
                    @"select count(*) from EdoOdkF020 edo left join edo.Contract c 
                          left join c.LegalEntity le" + $" where le.FormalizedName is not null and c.TypeID={contractType}";
                return session.CreateQuery(query).UniqueResult<long>();
            }
        }

        public IList<MarketCostListItem> GetMarketCostF020ListByPage(int contractType, int startIndex)
        {
            var query =
                $@"select edo.ID, edo.ReportOnDate, edo.Sumactiv1, c.ContractNumber, le.FormalizedName, le.OldName 
                      from EdoOdkF020 edo 
                      left join edo.Contract c 
                      left join c.LegalEntity le
                      where le.FormalizedName is not null and c.TypeID={contractType} order by edo.ID";
            return MarketCostListGetByPage(query, 0, startIndex);
        }


        public IList<MarketCostListItem> GetMarketCostF020ListNullContract(DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {
            var query = @"select edo.ID, edo.ReportOnDate, edo.Sumactiv1, c.ContractNumber, le.FormalizedName, le.OldName 
                             from EdoOdkF020 edo 
                             left join edo.Contract c 
                             left join c.LegalEntity le 
                             where c is null";

            query += periodStart.HasValue
                ? $" and edo.ReportOnDate >= '{periodStart.Value.ToShortDateString()}' "
                : string.Empty;
            query += periodEnd.HasValue
                ? $" and edo.ReportOnDate <= '{periodEnd.Value.ToShortDateString()}' "
                : string.Empty;

            return MarketCostListGet(query, MarketCostIdentifier.Forms.F20);
        }

#endregion

#region F025




        public long GetCountMarketCostF025Filtered(int contractType, DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    $@"select count(*) from EdoOdkF025 edo  left join edo.Contract c 
                          left join c.LegalEntity le
                          where le.FormalizedName is not null and c.TypeID={contractType}
                          AND (:from is null OR edo.ReportOnDate >= :from) 
                          AND (:to is null OR edo.ReportOnDate <= :to) ";
                return session.CreateQuery(query)
                    .SetParameter("from", from)
                    .SetParameter("to", to)
                    .UniqueResult<long>();
            }
        }


        public IList<MarketCostListItem> GetMarketCostF025ListFilteredByPage(int contractType, int startIndex,
            DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    $@"select edo.ID, edo.ReportOnDate, edo.TotalAmount, c.ContractNumber, le.FormalizedName, le.OldName 
                        from EdoOdkF025 edo 
                        left join edo.Contract c 
                        left join c.LegalEntity le
                        where le.FormalizedName is not null and c.TypeID={contractType}
                        AND (:from is null OR edo.ReportOnDate >= :from) 
                        AND (:to is null OR edo.ReportOnDate <= :to) 
                        order by edo.ID";
                var crit = session.CreateQuery(query)
                    .SetParameter("from", from)
                    .SetParameter("to", to);
                return MarketCostListGetByPage(crit, MarketCostIdentifier.Forms.F25, startIndex);
            }
        }

        public IList<MarketCostListItem> GetMarketCostF025ListNullContract(DateTime? periodStart = null,
            DateTime? periodEnd = null)
        {
            string query = "select edo.ID, edo.ReportOnDate, edo.TotalAmount, c.ContractNumber, le.FormalizedName, le.OldName "
                           + " from EdoOdkF025 edo "
                           + " left join edo.Contract c "
                           + " left join c.LegalEntity le"
                           + " where c is null";

            query += periodStart.HasValue
                ? $" and edo.ReportOnDate >= '{periodStart.Value.ToShortDateString()}' "
                : string.Empty;
            query += periodEnd.HasValue
                ? $" and edo.ReportOnDate <= '{periodEnd.Value.ToShortDateString()}' "
                : string.Empty;

            return MarketCostListGet(query, MarketCostIdentifier.Forms.F25);
        }

        public long GetCountMarketCostF025(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query =
                    $@"select count(*) from EdoOdkF025 edo  left join edo.Contract c
                        left join c.LegalEntity le where le.FormalizedName is not null and c.TypeID={contractType}";
                return session.CreateQuery(query).UniqueResult<long>();
            }
        }

        public IList<MarketCostListItem> GetMarketCostF025ListByPage(int contractType, int startIndex)
        {
            string query =
                $@"select edo.ID, edo.ReportOnDate, edo.TotalAmount, c.ContractNumber, le.FormalizedName, le.OldName 
                      from EdoOdkF025 edo 
                      left join edo.Contract c 
                      left join c.LegalEntity le
                      where le.FormalizedName is not null and c.TypeID={contractType} order by edo.ID";
            return MarketCostListGetByPage(query, MarketCostIdentifier.Forms.F25, startIndex);
        }

#endregion

#region F022

        public IList<MarketCostListItem> GetMarketCostF022List()
        {
            const string query = @"select edo.ID, edo.ReportOnDate, edo.TotalAmount, c.ContractNumber, le.FormalizedName, le.OldName 
                             from EdoOdkF022 edo 
                             left join edo.Contract c 
                             left join c.LegalEntity le
                             where le.FormalizedName is not null";
            return MarketCostListGet(query, MarketCostIdentifier.Forms.F22);
        }

        public IList<MarketCostListItem> GetMarketCostF022ListByTypeContract(int contractType)
        {
            string query =
                $@"select edo.ID, edo.ReportOnDate, edo.TotalAmount, c.ContractNumber, le.FormalizedName, le.OldName 
                      from EdoOdkF022 edo 
                      left join edo.Contract c 
                      left join c.LegalEntity le
                      where le.FormalizedName is not null and c.TypeID={contractType}";
            return MarketCostListGet(query, MarketCostIdentifier.Forms.F22);
        }

        public IList<MarketCostListItem> GetMarketCostF022ListByIds(IList<long> ids)
        {
            const string query = @"select edo.ID, edo.ReportOnDate, edo.TotalAmount, c.ContractNumber, le.FormalizedName, le.OldName 
                             from EdoOdkF022 edo 
                             left join edo.Contract c 
                             left join c.LegalEntity le
                             where edo.ID in (:ids)";
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(query).SetParameterList("ids", ids.ToArray());
                var items = new List<MarketCostListItem>();
                foreach (var item in list.List<object[]>())
                {
                    var did = (long)item[0];
                    var rdt = item[1] as DateTime?;
                    var d = item[2] as decimal?;
                    var cn = item[3] as string;
                    var fn = item[4] as string;
                    var on = item[5] as string;
                    var i = new MarketCostListItem(did, rdt, d, cn, fn, on);
                    items.Add(i);
                    i.FormType = MarketCostIdentifier.Forms.F22;
                }
                return items;
            }
        }

#endregion


        public IList<MarketCostListItem> GetMarketCostF024List(DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            string query = "select edo.ID, edo.ReportOnDate, edo.TotalAmount from EdoOdkF024 edo ";
            query += " where (:periodStart is null OR edo.ReportOnDate >= :periodStart)";
            query += " and (:periodEnd is null OR edo.ReportOnDate <= :periodEnd) ";
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(query);
                list.SetParameter("periodStart", periodStart?.Date);
                list.SetParameter("periodEnd", periodEnd?.Date);
                var items = new List<MarketCostListItem>();
                foreach (var item in list.List<object[]>())
                {
                    var did = (long)item[0];
                    var rdt = item[1] as DateTime?;
                    var d = item[2] as decimal?;
                    MarketCostListItem i = new MarketCostListItem(did, rdt, d);
                    items.Add(i);
                    i.FormType = MarketCostIdentifier.Forms.F24;
                }
                return items;
            }
        }

        public IList<MarketCostListItem> GetMarketCostF026List(DateTime? periodStart = null, DateTime? periodEnd = null)
        {
            string query = "select edo.ID, edo.ReportOnDate, edo.TotalAmount from EdoOdkF026 edo ";
            query += " where (:periodStart is null OR edo.ReportOnDate >= :periodStart)";
            query += " and (:periodEnd is null OR edo.ReportOnDate <= :periodEnd) order by edo.ID ";
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(query);
                list.SetParameter("periodStart", periodStart?.Date);
                list.SetParameter("periodEnd", periodEnd?.Date);
                var items = new List<MarketCostListItem>();
                foreach (var item in list.List<object[]>())
                {
                    var did = (long)item[0];
                    var rdt = item[1] as DateTime?;
                    var d = item[2] as decimal?;
                    MarketCostListItem i = new MarketCostListItem(did, rdt, d);
                    items.Add(i);
                    i.FormType = MarketCostIdentifier.Forms.F26;
                }
                return items;
            }
        }


#endregion

        public IList<LegalEntity> GetFullBankListForDeposit()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("SELECT le "
                                               + " FROM LegalEntityHib le "
                                               + " JOIN le.Contragent ca "
                                               + " WHERE TRIM(LOWER(ca.TypeName)) = :type ORDER BY ca.Name")
                    .SetString("type", "банк")
                    .List<LegalEntity>();
                return list;
            }
        }

        public IList<LegalEntity> GetBankListForDeposit(long? stockId = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(
                    $@"SELECT le 
                        FROM LegalEntityHib le 
                        JOIN le.Contragent ca 
                        {(stockId.HasValue ? " JOIN le.BankStockCodes sc " : "")}
                        WHERE {(stockId.HasValue ? (" sc.StockID = " + stockId.Value + " AND ") : "")} ca.StatusID <> -1 AND TRIM(LOWER(ca.TypeName)) = :type ORDER BY ca.Name")
                    .SetString("type", "банк")
                    .List<LegalEntity>();
                return list;
            }
        }

        public IList<BankItem> GetBankItemListForDeposit()
        {
            List<BankItem> results = new List<BankItem>();
            string sql = "select le, l.Number" +
                         " from LegalEntityHib le " +
                         " join le.Contragent ca " +
                         " left join le.Licensies l " +
                         //" left join le.MultiplierRatings mrl " +
                         //" left join mrl.MultiplierRating mr " +
                         " where ca.StatusID <> -1 AND TRIM(LOWER(ca.TypeName)) = :type" +
                         " AND l.ID in " +
                         " (select max(l_i.ID) from License l_i group by l_i.LegalEntityID) " +
                         //" AND mrl.ID in " +
                         //" () " +
                         " ORDER BY ca.Name ";

            string maxMultSql =
                "select max(mr_i.Multiplier), le_r_i.LegalEntityID from LegalEntityRating le_r_i join le_r_i.MultiplierRating mr_i group by le_r_i.LegalEntityID";

            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(sql).SetString("type", "банк");
                var sqlResult = query.List<object[]>();

                if (sqlResult.Count > 0)
                {
                    results = sqlResult.Select(i =>
                    {
                        BankItem item = new BankItem();
                        item.Entity = i[0] as LegalEntity;
                        item.RegistrationNum = i[1] != null ? i[1].ToString() : "";
                        item.Entity.Limit4Money = 0;
                        //if (item.Entity != null)
                        //{
                        //    item.Entity.Limit4Money = i[2] as decimal?;
                        //}

                        return item;
                    }).ToList();
                }

                query = session.CreateQuery(maxMultSql);

                sqlResult = query.List<object[]>();

                if (sqlResult.Count > 0)
                {
                    sqlResult.ForEach(i =>
                    {
                        var item = results.FirstOrDefault(x => x.ID == i[1] as long?);
                        if (item != null && item.Entity.Money != null)
                        {
                            var d = i[0] as decimal?;
                            if (d != null)
                            {
                                item.Entity.Limit4Money = d * item.Entity.Money;
                            }
                        }
                    });
                }
            }

            return results;
        }

        public IList<BankConclusion> GetBankConclusionList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("SELECT bc "
                                               + " FROM BankConclusionHib bc "
                    //+ " JOIN bc.BanksConclusion bsc "
                    //+ " WHERE ORDER BY bc.ImportDate desc"
                    )
                    .List<BankConclusion>();
                return list;
            }
        }

        public IList<LegalEntity> GetBankAgentListForDeposit()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("SELECT le "
                                               + " FROM LegalEntityHib le "
                                               + " JOIN le.Contragent ca "
                                               +
                                               " WHERE ca.StatusID <> -1 AND TRIM(LOWER(ca.TypeName)) = :type ORDER BY ca.Name")
                    .SetString("type", "банк-агент")
                    .List<LegalEntity>();
                return list;
            }
        }

        public IList<LegalEntity> GetFullBankAgentListForDeposit()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("SELECT le "
                                               + " FROM LegalEntityHib le "
                                               + " JOIN le.Contragent ca "
                                               + " WHERE TRIM(LOWER(ca.TypeName)) = :type ORDER BY ca.Name")
                    .SetString("type", "банк-агент")
                    .List<LegalEntity>();
                return list;
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetContractsListForLegalEntityHib(long leID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add(Restrictions.Eq("LegalEntityID", leID))
                    .Add(!Restrictions.Eq("Status", -1));

                return crit.List<PFR_INVEST.DataObjects.Contract>().ToList();
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetContractsListForLegalEntityHibByContractType(long leID, Document.Types contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add(Restrictions.Eq("LegalEntityID", leID))
                    .Add(!Restrictions.Eq("Status", -1))
                    .Add(Restrictions.Eq("TypeID", (int)contractType));

                return crit.List<PFR_INVEST.DataObjects.Contract>().ToList();
            }
        }

        public List<PFR_INVEST.DataObjects.Contract> GetContractsListForInvDecl(List<long> idList, Document.Types contractType, bool showEnded)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFR_INVEST.DataObjects.Contract), "c")
                    .Add(Restrictions.In("LegalEntityID", idList))
                    .Add(!Restrictions.Eq("Status", -1))
                    .Add(Restrictions.Eq("TypeID", (int)contractType));
                if (!showEnded)
                    crit.Add(Restrictions.IsNull("DissolutionDate"));

                return crit.List<PFR_INVEST.DataObjects.Contract>().ToList();
            }
        }


        public List<PFR_INVEST.DataObjects.Contract> GetGukConractsByContractName(string contractName)
        {
            var results = new List<PFR_INVEST.DataObjects.Contract>();
            string sql =
                string.Format(@"select con.REGNUM, con.FULLNAME, con.STATUS, con.REGDATE, con.REGCLOSE, con.OPERATESUMM, con.ID from {0}.CONTRACT con 
				join {0}.CONTRACTNAME cname on con.CONT_N_ID=cname.ID 
				join {0}.LEGALENTITY le on con.LE_ID=le.ID 
				join {0}.CONTRAGENT cagent on  cagent.ID=le.CONTRAGENTID 
				where con.STATUS=0 and con.TYPEID=3 and 
				cname.NAME= :cntrname and cagent.TYPE='ГУК'", DATA_SCHEME);

            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateSQLQuery(sql);
                var sqlResult = query.SetParameter("cntrname", contractName)
                    .List<object[]>();

                if (sqlResult.Count > 0)
                {
                    results = sqlResult.Select(item =>
                    {
                        PFR_INVEST.DataObjects.Contract contract = new PFR_INVEST.DataObjects.Contract();
                        contract.ContractNumber = item[0] != null ? item[0].ToString() : "";
                        contract.FullName = item[1] != null ? item[1].ToString() : "";
                        contract.Status = (int)item[2];
                        contract.ContractDate = (DateTime?)item[3];
                        contract.ContractEndDate = (DateTime?)item[4];
                        contract.OperateSum = (decimal?)item[5];
                        contract.ID = (long)item[6];
                        return contract;
                    }
                        ).ToList();
                }
            }
            return results;

        }

        public List<PFRContact> GetPFRContactsListHib(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFRContact), "c")
                    .Add(Restrictions.Eq("PFRB_ID", id))
                    .Add(Restrictions.Eq("StatusID", 1L));


                return crit.List<PFRContact>().ToList();
            }
        }

        public List<Person> GetPersonListHib(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(Person), "c")
                    .Add(Restrictions.Eq("DivisionId", id))
                    .Add(Restrictions.Not(Restrictions.Eq("StatusID", (long)-1)));

                return crit.List<Person>().ToList();
            }
        }

        public PFRContact GetPFRContactHib(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(PFRContact), "c")
                    .Add(Restrictions.Eq("ID", id));

                return (PFRContact)crit.UniqueResult();
            }
        }

        public Person GetPersonHib(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(Person), "c")
                    .Add(Restrictions.Eq("ID", id));

                return (Person)crit.UniqueResult();
            }
        }

        public FEDO GetFedoHib(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(FEDO), "c")
                    .Add(Restrictions.Eq("ID", id));

                return crit.UniqueResult() as FEDO;
            }
        }

        public List<FEDO> GetFedoListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(FEDO), "c");

                return crit.List<FEDO>().ToList();
            }
        }

        public List<SIListItem> GetSIListHibForInvDecl(bool withContacts, bool forSI)
        {
            const string queryString = @"
								SELECT DISTINCT le.ID as LegalEntityID, contragent.Name as ContragentName, contragent.StatusID as ContragentStatusId, 
                                       le.FormalizedName as FormalizedName
								FROM LegalEntity le
								JOIN le.Contragent contragent
                                JOIN le.Contracts cs
								WHERE cs.TypeID=:type AND contragent.StatusID>=0";

            var result = SessionWrapper(session => session.CreateQuery(queryString)
                    .SetParameter("type", forSI ? (int)Document.Types.SI : (int)Document.Types.VR)
                    .SetResultTransformer(Transformers.AliasToBean(typeof(SIListItem)))
                    .List<SIListItem>().ToList());
            return result;
        }

        public List<SIListItem> GetSIListHib(bool withContacts)
        {

            string queryString = @"
								SELECT le.ID as LegalEntityID, contragent.Name as ContragentName, contragent.StatusID as ContragentStatusId, 
                                       le.FormalizedName as FormalizedName, le.FullName as FullName, 
									   le.HeadFullName as HeadFullName, le.LegalAddress as LegalAddress,
									   le.PostAddress as PostalAddress, le.Phone as Phone, le.EAddress as EMail, le.CloseDate as CloseDate
									   {0}
								FROM LegalEntity le
								LEFT JOIN le.Contragent contragent
								{1}
								WHERE le.ContragentID in
								(
									SELECT c.ID FROM Contragent c
									WHERE (
                                        	LOWER(TRIM(c.TypeName)) = 'ук' OR 
											LOWER(TRIM(c.TypeName)) = 'сд' OR 
											LOWER(TRIM(c.TypeName)) = 'гук'OR
											LOWER(TRIM(c.TypeName)) = 'управляющая компания' OR 
											LOWER(TRIM(c.TypeName)) = 'специализированный депозитарий' OR 
											LOWER(TRIM(c.TypeName)) = 'генеральная управляющая компания'
							) AND c.StatusID <> -1 AND c.StatusID <> -2
								)";

            queryString = string.Format(queryString,
                withContacts
                    ? ", contact.FIO as ContactFIO, contact.Phone as ContactTel, contact.Email as ContactEMail, contact.ID as ContactID"
                    : "", withContacts ? "LEFT JOIN le.Contacts contact" : "");

            List<SIListItem> result;
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(queryString).SetResultTransformer(Transformers.AliasToBean(typeof(SIListItem)));
                result = query.List<SIListItem>().ToList();
            }
            return result;
        }

        public DocFileBody GetDocFileBodyForDocument(long pID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria<DocFileBody>()
                    .Add(Restrictions.Eq("DocumentID", pID));
                try
                {
                    return crit.List<DocFileBody>().FirstOrDefault();
                }
                catch
                {
                    return null;
                }
            }
        }

        public long SaveDocFileBodyForDocument(long lID, DocFileBody file, byte[] body)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    file.DocumentID = lID;
                    var files = session.CreateCriteria<DocFileBody>()
                        .Add(Restrictions.Eq("DocumentID", lID))
                        .Add(Restrictions.Not(Restrictions.Eq("ID", file.ID))).List<DocFileBody>();
                    foreach (var f in files)
                    {
                        session.Delete(f);
                    }

                    file.FileContentId = SaveFileContent(session, file.FileContentId, body);
                    session.SaveOrUpdate(file);

                    transaction.Commit();
                    return file.ID;
                }
            }
        }

        public long SaveFileContent(long? fileContentId, byte[] data)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return SaveFileContent(session, fileContentId, data);
            }
        }

        private long SaveFileContent(ISession session, long? fileContentId, byte[] data)
        {
            if (fileContentId.HasValue)
            {
                var file = session.Get<FileContent>(fileContentId.Value);
                if (file != null)
                    session.Delete(file);
            }

            FileContent fileContent = new FileContent();
            fileContent.FileContentData = data;
            return (long)session.Save(fileContent);
        }

        public List<SIContractListItem> GetSIContractsListHib()
        {
            return GetContractsListHib(Document.Types.SI);
        }

        public List<SIContractListItem> GetVRContractsListHib()
        {
            return GetContractsListHib(Document.Types.VR);
        }

        private List<SIContractListItem> GetContractsListHib(Document.Types contractType)
        {
            int contractTypeID = (int)contractType;
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(SupAgreementHib), "sa")
                    .AddOrder(Order.Asc("RegDate"))
                    .CreateCriteria("Contract", "c", JoinType.InnerJoin)
                    .SetFetchMode("Contract", FetchMode.Join)
                    .Add(Restrictions.Ge("Status", 0) | Restrictions.IsNull("Status"))
                    .Add(Restrictions.Eq("TypeID", contractTypeID));

                crit.CreateCriteria("RewardCosts", "rc", JoinType.LeftOuterJoin)
                    .SetFetchMode("RewardCosts", FetchMode.Join);

                crit.CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .CreateCriteria("Contragent", "ca", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join)
                    .Add(Restrictions.Eq("TypeName", "УК") | Restrictions.Eq("TypeName", "ГУК") |
                         Restrictions.Eq("TypeName", "СД"))
                    .Add(!Restrictions.Eq("StatusID", (long)-1));

                var retVal = crit.List<SupAgreementHib>().ToList().ConvertAll(sa =>
                {
                    var item = new SIContractListItem
                    {
                        ID = sa.ID,
                        ContractID = sa.Contract.ID,
                        SIName = sa.Contract.LegalEntity.FormalizedName,
                        SupAgreementKind = sa.Kind,
                        SupAgreementDate = sa.RegDate,
                        DissolutionDate = sa.Contract.DissolutionDate,
                        SupAgreementNumber = sa.Number
                    };

                    FillSIContractListItem(item, sa.Contract);
                    return item;
                });

                var empty = session.CreateQuery(@"SELECT c FROM SupAgreementHib sa RIGHT OUTER JOIN sa.Contract c 
												WHERE sa is NULL AND c.LegalEntity.Contragent.TypeName IN (:types) AND c.TypeID = (:contractTypeID) AND (c.Status >= 0 OR c.Status IS NULL)")
                    .SetParameterList("types", "УК|ГУК|СД".Split('|'))
                    .SetParameter("contractTypeID", contractTypeID)
                    .List<ContractHib>().Select(c =>
                    {
                        var item = new SIContractListItem
                        {
                            ContractID = c.ID,
                            SIName = c.LegalEntity.FormalizedName,
                            DissolutionDate = c.DissolutionDate
                        };

                        FillSIContractListItem(item, c);
                        return item;
                    });
                retVal.AddRange(empty);
                return retVal;
            }
        }

        private void FillSIContractListItem(SIContractListItem item, ContractHib contract)
        {
            string contractPortfolioNumber = string.Empty;
            if (!string.IsNullOrEmpty(contract.PortfolioName))
                contractPortfolioNumber = $"{contract.ContractNumber} - {contract.PortfolioName}";
            else
                contractPortfolioNumber = contract.ContractNumber;

            if (contract.ContractDate.HasValue)
                contractPortfolioNumber += $" ({contract.ContractDate.Value:dd.MM.yyyy})";

            RewardCostHib rc;
            if (contract.RewardCosts.Count > 0)
                rc = contract.RewardCosts[0];
            else
                rc = null;
            if (rc != null)
            {
                contractPortfolioNumber +=
                    $"\tПредельный размер расхода: {rc.MaxCost.GetValueOrDefault() / 100:P}\tРазмер вознаграждения: {rc.AmountReward.GetValueOrDefault() / 100:P}";
            }
            item.ContractPortfolioNumber = contractPortfolioNumber;

            switch (contract.LegalEntity.Contragent.TypeName.Trim())
            {
                case "УК":
                    item.ContragentType = "Управляющая компания";
                    break;
                case "ГУК":
                    item.ContragentType = "Государственная управляющая компания";
                    break;
                case "СД":
                    item.ContragentType = "Специализированный депозитарий";
                    break;
                default:
                    break;
            }
            item.LegalEntityID = contract.LegalEntityID == null ? 0 : (long)contract.LegalEntityID;
        }

        public IList<Rating> GetRatingsListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria<Rating>()
                    .List<Rating>().Where(r => r.StatusID == 1).ToList();
                return list;
            }
        }

        public IList<AgencyRatingListItem> GetAgencyRatingsListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("select new AgencyRatingListItem(mr, r, ra) "
                                               + " from MultiplierRating mr "
                                               + " join mr.RatingAgency ra "
                                               + " join mr.Rating r ").List<AgencyRatingListItem>();
                return list;
                //var list = session.CreateCriteria<Rating>()
                //.List<Rating>().Where(r => r.StatusID == 1).ToList();
                //return list;
            }
        }

        public IList<MultiplierRating> GetMultiplierRatingByLegalEntityID(long leID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("select mr " + " from LegalEntityRating le_r " + " join le_r.MultiplierRating mr " + $" where le_r.LegalEntityID = {leID} ")
                    .List<MultiplierRating>();
                return list;
            }
        }

        public IList<BankAccount> GetOldBankAccounts(long legalEntityId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria<BankAccount>()
                    .Add(Restrictions.Eq("LegalEntityID", legalEntityId))
                    .Add(Restrictions.IsNotNull("CloseDate"))
                    .List<BankAccount>();
                return list;
            }
        }

        public IList<BankAccount> GetBankAccountListByTypeHib(string[] type)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria(typeof(BankAccount), "ba")
                    .CreateCriteria("LegalEntity", "le", JoinType.InnerJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Join)
                    .CreateCriteria("Contragent", "ca", JoinType.InnerJoin)
                    .SetFetchMode("Contragent", FetchMode.Join);
                if (type.Length > 0)
                    foreach (string s in type)
                        if (!string.IsNullOrEmpty(s))
                            list.Add(Restrictions.Eq("TypeName", s));
                return list.List<BankAccount>();
            }
        }

        public List<RatingAgency> GetRatingAgenciesList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateCriteria<RatingAgency>().List<RatingAgency>().Where(ra => ra.StatusID == 1).ToList();
                return list;
            }
        }

        public int GetAuctionNextLocalNum(DateTime auctionDate)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("select a from DepClaimSelectParams a where SelectDate = :date")
                    .SetParameter("date", auctionDate).List<DepClaimSelectParams>();
                if (!list.Any())
                    return 1;
                return list.Max(a => a.LocalNum) + 1;
            }
        }

        public List<DepClaimSelectParams> GetDepClaimSelectParamsList()
        {
            return
                GetListByPropertyCondition(typeof(DepClaimSelectParams).FullName, "StatusID", (long)-1, "neq")
                    .Cast<DepClaimSelectParams>()
                    .ToList();
        }

        public List<DepClaimMaxListItem> GetDepClaimMaxList(int type)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var claimList =
                    session.CreateCriteria<DepClaimMaxListItem>()
                        .List<DepClaimMaxListItem>()
                        .Where(x => x.DepclaimType == type)
                        .ToList();
                var auctionList = session.CreateCriteria<DepClaimSelectParams>().List<DepClaimSelectParams>().ToList();
                int repKey = type == (int)DepClaimMaxListItem.Types.Common ? 1 : 2;
                //var lastRepository = this.GetLastRepositoryLinks().Where(x => x.RepositoryKey == repKey);
                foreach (var x in claimList)
                {
                    var auction = auctionList.Where(p => p.ID == x.DepclaimselectparamsId).SingleOrDefault();
                    x.SelectDate = auction.SelectDate;
                    x.StockName = auction.StockName;
                }
                return claimList;
            }
        }

        public IList<PortfolioFullListItem> GetPortfoliosByType(Portfolio.Types[] types = null, bool exclude = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {

                var query = session.CreateQuery("select new PortfolioFullListItem(p, bank, bacc, at, l, y) "
                                               + " from PortfolioPFRBankAccount l "
                                               + " join l.Portfolio p "
                                               + " left join p.RealYear y "
                                               + " join l.PfrBankAccount bacc "
                                               + " join bacc.LegalEntity bank "
                                               + " join bacc.AccountType at "
                                               + " join bank.Contragent ct "
                                               + " where "
                                               + (exclude ? " (p.TypeID not in (:types)) "
                                                    : ((types == null || types.Length == 0) ? " 1=1 " : " (p.TypeID in (:types)) "))
                                               + " and ct.StatusID <> -1 and bacc.StatusID <> -1 and l.StatusID <> -1 ");
                if (types != null && types.Length > 0)
                    query.SetParameterList("types", types.Select(t => (long)t).ToList());
                var list = query.List<PortfolioFullListItem>();

                return list;
            }
        }

        public IList<PortfolioFullListItem> GetFullPortfoliosListByType(Portfolio.Types? type = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                long? typeID = (long?)type;

                var list = session.CreateQuery(@"select new PortfolioFullListItem(p, bank, bacc, at, l, y) 
				 from Portfolio p 
                left join p.PortfolioToPFRBankAccounts l 
				left join p.RealYear y 
				left join l.PfrBankAccount bacc 
				left join bacc.LegalEntity bank 
				left join bacc.AccountType at 
				left join bank.Contragent ct 
                where ((:type is null) OR (p.TypeID = (:type))) and (ct.StatusID <> -1 or ct.StatusID = null) and (bacc.StatusID <> -1 or bacc.StatusID = null) and (l.StatusID <> -1 or l.StatusID = null) and p.StatusID <> -1")
                    .SetParameter("type", typeID)
                    .List<PortfolioFullListItem>();

                return list;
            }
        }

        public IList<PortfolioFullListItem> GetPortfoliosFiltered(PortfolioIdentifier.PortfolioPBAParams p)
        {

            string sAnd = " AND ";
            string sOr = " OR ";
            string query = "select distinct new PortfolioFullListItem(p, bank, bacc, at, l) "
                           + " from PortfolioPFRBankAccount l "
                           + " join l.Portfolio p "
                           + " join l.PfrBankAccount bacc "
                           + " join bacc.LegalEntity bank "
                           + " join bacc.AccountType at "
                           + " join bacc.Currency cr "
                           + " join bank.Contragent ct "
                           + " where";

            StringBuilder sb = new StringBuilder();
            sb.Append(" ct.StatusID <> -1 and bacc.StatusID <> -1 and l.StatusID <> -1");

            if (p.PortfolioID != null)
                sb.Append(string.Concat(sAnd, "l.PortfolioID = :pfID"));

            if (p.HiddenPortfolioIDs != null && p.HiddenPortfolioIDs.Length > 0)
                sb.Append(string.Concat(sAnd, "l.PortfolioID NOT IN (",
                    string.Join(",", p.HiddenPortfolioIDs.Select(id => id.ToString()).ToArray()), ")"));

            if (p.PfrBankAccountID != null)
                sb.Append(string.Concat(sAnd, "l.PfrBankAccountID = :pbaID"));

            if (p.HiddenPfrBankAccountID != null && p.HiddenPfrBankAccountID.Length > 0)
                sb.Append(string.Concat(sAnd, "bacc.ID NOT IN (",
                    string.Join(",", p.HiddenPfrBankAccountID.Select(id => id.ToString()).ToArray()), ")"));



            if (p.PortfolioTypeFilter != null && p.PortfolioTypeFilter.PortfolioTypeID.Length > 0)
            {
                sb.Append(string.Concat(sAnd, "("));

                if (p.PortfolioTypeFilter.Exclude == false)
                {
                    sb.Append("p.TypeID IN (:pt)");
                }
                else
                {
                    sb.Append("p.TypeID NOT IN (:pt)");
                }

                sb.Append(")");
            }

            if (p.AccountType != null && p.AccountType.Length > 0)
            {
                sb.Append(string.Concat(sAnd, "("));
                for (int i = 0; i < p.AccountType.Length; i++)
                {
                    if (i > 0)
                        sb.Append(sOr);
                    sb.Append($"lower(trim(at.Name)) = :at{i}");
                }
                sb.Append(")");
            }

            if (p.ContragentContains != null && p.ContragentContains.Length > 0)
            {
                sb.Append(string.Concat(sAnd, "("));
                for (int i = 0; i < p.ContragentContains.Length; i++)
                {
                    if (i > 0)
                        sb.Append(sOr);
                    sb.Append($"lower(trim(ct.Name)) LIKE :ctn{i}");
                }
                sb.Append(")");
            }

            if (p.ContragentTypeContains != null && p.ContragentTypeContains.Length > 0)
            {
                sb.Append(string.Concat(sAnd, "("));
                for (int i = 0; i < p.ContragentTypeContains.Length; i++)
                {
                    if (i > 0)
                        sb.Append(sOr);
                    sb.Append($"lower(trim(ct.TypeName)) LIKE :ctt{i}");
                }
                sb.Append(")");
            }

            if (p.CurrencyName != null && p.CurrencyName.Length > 0)
            {
                sb.Append(string.Concat(sAnd, "("));
                for (int i = 0; i < p.CurrencyName.Length; i++)
                {
                    if (i > 0)
                        sb.Append(sOr);
                    sb.Append($"lower(TRIM(cr.Name)) LIKE :crn{i}");
                }
                sb.Append(")");
            }

            if (p.LegalEntityFormalizedNameContains != null && p.LegalEntityFormalizedNameContains.Length > 0)
            {
                sb.Append(string.Concat(sAnd, "("));
                for (int i = 0; i < p.LegalEntityFormalizedNameContains.Length; i++)
                {
                    if (i > 0)
                        sb.Append(sOr);
                    sb.Append($"lower(trim(bank.FormalizedName)) LIKE :lefn{i}");
                }
                sb.Append(")");
            }

            if (p.BankPortfolioContainAccountType != null && p.BankPortfolioContainAccountType.Length > 0)
            {
                string querySub = "select count(l1) "
                                  + " from PortfolioPFRBankAccount l1 "
                                  + " join l1.Portfolio p1 "
                                  + " join l1.PfrBankAccount bacc1 "
                                  + " join bacc1.LegalEntity bank1 "
                                  + " join bacc1.AccountType at1 "
                                  + " join bacc1.Currency cr1 "
                                  + " join bank1.Contragent ct1 "
                                  + " where  ct1.StatusID <> -1 and bacc1.StatusID <> -1 and l1.StatusID <> -1 ";
                StringBuilder sb1 = new StringBuilder();
                if (p.CurrencyName != null && p.CurrencyName.Length > 0)
                {


                    sb1.Append(string.Concat(sAnd, "("));
                    for (int i = 0; i < p.CurrencyName.Length; i++)
                    {
                        if (i > 0)
                            sb1.Append(sOr);
                        sb1.Append($"lower(TRIM(cr1.Name)) LIKE '{p.CurrencyName[i].ToLower()}'");
                    }
                    sb1.Append(") ");
                }

                sb1.Append(string.Concat(sAnd,
                    $"lower(trim(at1.Name)) = '{p.BankPortfolioContainAccountType}'"));

                sb1.Append(string.Concat(sAnd, " l1.PortfolioID = l.PortfolioID "));

                sb1.Append(string.Concat(sAnd, " bank1.ID = bank.ID "));

                query = string.Concat(
                    query,
                    $" ({string.Concat(querySub, sb1.ToString())}) > 0 AND ");
            }

            sb.Append(" order by bacc.CurrencyID desc ");

            using (var session = DataAccessSystem.OpenSession())
            {
                var q = session.CreateQuery(string.Concat(query, sb.ToString()));

                if (p.PortfolioID != null)
                    q.SetInt64("pfID", (long)p.PortfolioID);

                if (p.PfrBankAccountID != null)
                    q.SetInt64("pbaID", (long)p.PfrBankAccountID);

                if (p.PortfolioTypeFilter != null && p.PortfolioTypeFilter.PortfolioTypeID.Length > 0)
                {
                    var listPt = p.PortfolioTypeFilter.PortfolioTypeID.Where(id => id > 0).ToList();
                    q.SetParameterList("pt", listPt);
                }

                if (p.AccountType != null && p.AccountType.Length > 0)
                {
                    for (int i = 0; i < p.AccountType.Length; i++)
                        q.SetString($"at{i}", p.AccountType[i]);
                }

                if (p.ContragentContains != null && p.ContragentContains.Length > 0)
                {
                    for (int i = 0; i < p.ContragentContains.Length; i++)
                        q.SetString($"ctn{i}", p.ContragentContains[i]);
                }

                if (p.ContragentTypeContains != null && p.ContragentTypeContains.Length > 0)
                {
                    for (int i = 0; i < p.ContragentTypeContains.Length; i++)
                        q.SetString($"ctt{i}", p.ContragentTypeContains[i]);
                }

                if (p.CurrencyName != null && p.CurrencyName.Length > 0)
                {
                    for (int i = 0; i < p.CurrencyName.Length; i++)
                        q.SetString($"crn{i}", p.CurrencyName[i].ToLower());
                }

                if (p.LegalEntityFormalizedNameContains != null && p.LegalEntityFormalizedNameContains.Length > 0)
                {
                    for (int i = 0; i < p.LegalEntityFormalizedNameContains.Length; i++)
                        q.SetString($"lefn{i}", p.LegalEntityFormalizedNameContains[i]);
                }

                var list = q.List<PortfolioFullListItem>();

                if (p.HiddenPortfolioAccountLinks != null && p.HiddenPortfolioAccountLinks.Length > 0)
                {
                    list = (from l in list
                            join hpa in p.HiddenPortfolioAccountLinks
                                on new { pID = l.Portfolio.ID, aID = l.PfrBankAccount.ID } equals
                                new { pID = hpa.PortfolioID, aID = hpa.PfrBankAccountID }
                                into nhpa
                            from hpa in nhpa.DefaultIfEmpty()
                            where hpa == null
                            select l).ToList();
                }

                return list;
            }
        }

        public PortfolioFullListItem GetPortfolioFull(long pfrBankAccId, long portfolioId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("select new PortfolioFullListItem(p, bank, bacc, at, l) "
                                               + " from PortfolioPFRBankAccount l "
                                               + " join l.Portfolio p "
                                               + " join l.PfrBankAccount bacc "
                                               + " join bacc.LegalEntity bank "
                                               + " join bacc.AccountType at "
                                               + " where bacc.ID = (:baccId) and p.ID = (:pId)")
                    .SetInt64("baccId", pfrBankAccId)
                    .SetInt64("pId", portfolioId).List<PortfolioFullListItem>();

                var acc = list.FirstOrDefault();

                //если нет такой записи, возвращаем сгенерированую
                if (acc == null)
                {
                    var p = session.Get<PortfolioHib>(portfolioId);
                    var ba = session.Get<PfrBankAccountHib>(pfrBankAccId);

                    if (p != null && ba != null)
                    {
                        var le = session.Get<LegalEntityHib>(ba.LegalEntity.ID);
                        var at = session.Get<AccBankType>(ba.AccountType.ID);
                        acc = new PortfolioFullListItem(p, le, ba, at,
                            new PortfolioPFRBankAccount
                            {
                                ID = 0,
                                PortfolioID = portfolioId,
                                PFRBankAccountID = pfrBankAccId
                            });
                    }
                }

                return acc;
            }
        }

        public PortfolioFullListItem GetPortfolioFullByPfBaccID(long portfolioPfrBankAccountID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("select new PortfolioFullListItem(p, bank, bacc, at, l) "
                                               + " from PortfolioPFRBankAccount l "
                                               + " join l.Portfolio p "
                                               + " join l.PfrBankAccount bacc "
                                               + " join bacc.LegalEntity bank "
                                               + " join bacc.AccountType at "
                                               + " where l.ID = (:lId) ")
                    .SetInt64("lId", portfolioPfrBankAccountID).List<PortfolioFullListItem>();
                return list.FirstOrDefault();
            }
        }


        public PortfolioFullListItem GetPortfolioFullForBank(long portfolioId, long bankId, long accId = 0)
        {
            if (accId == 0)
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    var list = session.CreateQuery("select new PortfolioFullListItem(p, bank, bacc, at, l) "
                                                   + " from PortfolioPFRBankAccount l "
                                                   + " join l.Portfolio p "
                                                   + " join l.PfrBankAccount bacc "
                                                   + " join bacc.LegalEntity bank "
                                                   + " join bacc.AccountType at "
                                                   + " where bank.ID = (:bankId) and p.ID = (:pId)")
                        .SetInt64("bankId", bankId)
                        .SetInt64("pId", portfolioId).List<PortfolioFullListItem>();
                    return list.FirstOrDefault();
                }
            }
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("select new PortfolioFullListItem(p, bank, bacc, at, l) "
                                               + " from PortfolioPFRBankAccount l "
                                               + " join l.Portfolio p "
                                               + " join l.PfrBankAccount bacc "
                                               + " join bacc.LegalEntity bank "
                                               + " join bacc.AccountType at "
                                               + " where bank.ID = (:bankId) and p.ID = (:pId) and bacc.ID = (:aId)")
                    .SetInt64("bankId", bankId)
                    .SetInt64("aId", accId)
                    .SetInt64("pId", portfolioId).List<PortfolioFullListItem>();
                return list.FirstOrDefault();
            }
        }

        private string GetPortfolioTypeFilterString(string portfolioAlias, string parameterName, PortfolioTypeFilter filter)
        {
            if (filter.Exclude)
                return $" {portfolioAlias}.TypeID not in(:{parameterName}) ";
            if (filter.Types != null && filter.Types.Count() > 0)
                return $" {portfolioAlias}.TypeID in(:{parameterName}) ";
            return " 1=1 ";
        }

        private void SetPortfolioTypeFilterParameter(IQuery query, string parameterName, PortfolioTypeFilter filter)
        {
            if (filter.Types != null && filter.Types.Count() > 0)
                query.SetParameterList(parameterName, filter.Types.Select(t => (long)t).ToList());
        }


        public long GetCountAddSPNFullList(PortfolioTypeFilter filter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery("select count(*) from AddSPN due"
                                           + " join due.Portfolio p "
                                           + " where " + GetPortfolioTypeFilterString("p", "types", filter));
                SetPortfolioTypeFilterParameter(query, "types", filter);
                return query.UniqueResult<long>();

            }
        }

        public IList<AddSPN> GetAddSPNFullListByPage(PortfolioTypeFilter filter, int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"select due, p from AddSPN due
                                                   join due.Portfolio p 
                                                   where {GetPortfolioTypeFilterString("p", "types", filter)}
                                                   order by p.ID");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetFirstResult(startIndex)
                    .SetMaxResults(PageSize)
                    .List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as AddSPN;
                    i.ExtensionData = new Dictionary<string, object> { { "Portfolio", item[1] } };
                    return i;
                }).ToList();

                return list;
            }
        }

        public IList<AddSPN> GetAddSPNFullListByID(PortfolioTypeFilter filter, long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery("select due, p from AddSPN due"
                                                  + " join due.Portfolio p "
                                                  + " where " + GetPortfolioTypeFilterString("p", "types", filter)
                                                  + " AND due.ID = :id");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetParameter("id", id)
                            .List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as AddSPN;
                    i.ExtensionData = new Dictionary<string, object> { { "Portfolio", item[1] } };
                    return i;
                }).ToList();

                return list;
            }
        }


        public IList<DopSPN> GetDopSPNList(long pfrBankAccId, long portfolioId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var rawList = session.CreateQuery("select due, p from DopSPN due"
                                                  + " join due.Portfolio p "
                                                  +
                                                  " where due.QuarterPfrBankAccountID = (:baccId) and due.PortfolioID = (:pfId)")
                    .SetParameter("baccId", pfrBankAccId)
                    .SetParameter("pfId", portfolioId)
                    .List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as DopSPN;
                    i.ExtensionData = new Dictionary<string, object>();
                    i.ExtensionData.Add("Portfolio", item[1]);
                    return i;
                }).ToList();

                return list;
            }
        }

        public IList<AddSPN> GetAddSPNList(long pfrBankAccId, long portfolioId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var rawList = session.CreateQuery("select due, p from AddSPN due"
                                                  + " join due.Portfolio p "
                                                  +
                                                  " where due.PFRBankAccountID = (:baccId) and due.PortfolioID = (:pfId)")
                    .SetParameter("baccId", pfrBankAccId)
                    .SetParameter("pfId", portfolioId)
                    .List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as AddSPN;
                    i.ExtensionData = new Dictionary<string, object> {{"Portfolio", item[1]}};
                    return i;
                }).ToList();

                return list;
            }
        }

        public long GetCountDopSPNFullList(PortfolioTypeFilter filter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query =
                    session.CreateQuery(
                        $@"select count(*) from DopSPN due
                           join due.Portfolio p 
                           where {GetPortfolioTypeFilterString("p", "types", filter)}");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                return query.UniqueResult<long>();
            }
        }

        public IList<DopSPN> GetDopSPNFullListByPage(PortfolioTypeFilter filter, int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"select due, p from DopSPN due
                                                  join due.Portfolio p 
                                                  where { GetPortfolioTypeFilterString("p", "types", filter)}
                                                  order by p.ID");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as DopSPN;
                    i.ExtensionData = new Dictionary<string, object> {{"Portfolio", item[1]}};
                    return i;
                }).ToList();

                return list;
            }
        }

        public IList<DopSPN> GetDopSPNFullListByID(PortfolioTypeFilter filter, long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery("select due, p from DopSPN due"
                                                  + " join due.Portfolio p "
                                                  + " where " + GetPortfolioTypeFilterString("p", "types", filter)
                                                  + " AND due.ID = :id");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetParameter("id", id).List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as DopSPN;
                    i.ExtensionData = new Dictionary<string, object> { { "Portfolio", item[1] } };
                    return i;
                }).ToList();

                return list;
            }
        }



        public long GetCountApsFullList(PortfolioTypeFilter filter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query =
                    session.CreateQuery(
                        "select count(*) from Aps due"
                        + " join due.Portfolio p " + " join due.SPortfolio sp "
                        + " where " + GetPortfolioTypeFilterString("p", "types", filter)
                            + " or " + GetPortfolioTypeFilterString("sp", "types", filter));
                SetPortfolioTypeFilterParameter(query, "types", filter);
                return query.UniqueResult<long>();


            }
        }

        public IList<Aps> GetApsFullListByPage(PortfolioTypeFilter filter, int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"select due, p, sp from Aps due
                                                  join due.Portfolio p 
                                                  join due.SPortfolio sp 
                                                  where {GetPortfolioTypeFilterString("p", "types", filter)}
                                                      or {GetPortfolioTypeFilterString("sp", "types", filter)}
                                                  order by due.ID");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as Aps;
                    i.ExtensionData = new Dictionary<string, object> {{"Portfolio", item[1]}, {"SPortfolio", item[2]}};
                    return i;
                }).ToList();

                return list;
            }
        }

        public IList<Aps> GetApsFullListByID(PortfolioTypeFilter filter, long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery("select due, p, sp from Aps due"
                                                  + " join due.Portfolio p "
                                                  + " join due.SPortfolio sp "
                                                  + " where (" + GetPortfolioTypeFilterString("p", "types", filter)
                                                     + " or " + GetPortfolioTypeFilterString("sp", "types", filter)
                                                  + ") and due.ID = :id");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetParameter("id", id).List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as Aps;
                    i.ExtensionData = new Dictionary<string, object> { { "Portfolio", item[1] }, { "SPortfolio", item[2] } };
                    return i;
                }).ToList();

                return list;
            }

        }



        public long GetCountDopApsFullList(PortfolioTypeFilter filter)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery("select count(*) from DopAps due"
                                           + " join due.Portfolio p "
                                           + " join due.SPortfolio sp "
                                           + " where " + GetPortfolioTypeFilterString("p", "types", filter)
                                              + " or " + GetPortfolioTypeFilterString("sp", "types", filter));
                SetPortfolioTypeFilterParameter(query, "types", filter);
                return query.UniqueResult<long>();

            }

        }

        public IList<DopAps> GetDopApsFullListByPage(PortfolioTypeFilter filter, int startIndex)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"select due, p, sp from DopAps due
                                                   join due.Portfolio p 
                                                   join due.SPortfolio sp 
                                                   where {GetPortfolioTypeFilterString("p", "types", filter)}
                                                       or {GetPortfolioTypeFilterString("sp", "types", filter)}
                                                    order by due.ID");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetFirstResult(startIndex).SetMaxResults(PageSize).List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as DopAps;
                    i.ExtensionData = new Dictionary<string, object>();
                    i.ExtensionData.Add("Portfolio", item[1]);
                    i.ExtensionData.Add("SPortfolio", item[2]);
                    return i;
                }).ToList();

                return list;
            }
        }

        public IList<DopAps> GetDopApsFullListByID(PortfolioTypeFilter filter, long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery("select due, p, sp from DopAps due"
                                                  + " join due.Portfolio p "
                                                  + " join due.SPortfolio sp "
                                                  + " where (" + GetPortfolioTypeFilterString("p", "types", filter)
                                                        + " or " + GetPortfolioTypeFilterString("sp", "types", filter)
                                                  + ") and due.ID = :id ");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var rawList = query.SetParameter("id", id).List<object[]>();
                var list = rawList.Select(item =>
                {
                    var i = item[0] as DopAps;
                    i.ExtensionData = new Dictionary<string, object> { { "Portfolio", item[1] }, { "SPortfolio", item[2] } };
                    return i;
                }).ToList();

                return list;
            }
        }



        public decimal GetAddSPNSumByPeriod(PortfolioTypeFilter filter, DateTime targetDate)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery("select sum(due.Summ) from AddSPN due"
                                                 + " join due.Portfolio p "
                                                 +
                                                 " where " + GetPortfolioTypeFilterString("p", "types", filter)
                                                 + " and due.MonthID = (:month) and due.YearID = (:year)");
                SetPortfolioTypeFilterParameter(query, "types", filter);
                var result = query.SetParameter("month", targetDate.Month)
                    .SetParameter("year", targetDate.Year - 2000)
                    .UniqueResult<decimal>();

                return result;
            }
        }

        public List<TemplateListItem> GetTemplatesList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //var result = new List<TemplateListItem>();

                //var crit = session.CreateCriteria<TemplateHib>()
                //.CreateCriteria("Department", "d", NHibernate.SqlCommand.JoinType.InnerJoin)
                //.SetFetchMode("Department", FetchMode.Join);

                //foreach (var t in crit.List<TemplateHib>())
                //{
                //    var item = new TemplateListItem()
                //                        {
                //                            ID = t.ID,
                //                            Department = t.Department.Name,
                //                            Date = t.Date,
                //                            DepartmentID = t.DepartmentID,
                //                            FileName = t.FileName,
                //                            UNID = t.UNID
                //                        };
                //    result.Add(((TemplateListItem)item));
                //}

                var result = new List<TemplateListItem>();

                var crit = session.CreateCriteria<TemplateHib>()
                    .CreateCriteria("Department", "d", JoinType.InnerJoin)
                    .SetFetchMode("Department", FetchMode.Join)
                    .SetProjection(Projections.Property("ID"),
                        Projections.Property("d.Name"),
                        Projections.Property("Date"),
                        Projections.Property("DepartmentID"),
                        Projections.Property("FileName"),
                        Projections.Property("UNID"))
                    .List();

                foreach (object[] t in crit)
                {
                    var item = new TemplateListItem
                    {
                        ID = (long)t[0],
                        Department = (string)t[1],
                        Date = (DateTime)t[2],
                        DepartmentID = (long)t[3],
                        FileName = (string)t[4],
                        UNID = (int)t[5]
                    };
                    result.Add(item);
                }

                return result;
            }
        }

        public Template GetTemplateByName(string templateFileName)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria<TemplateHib>()
                    .Add(Restrictions.Eq("FileName", templateFileName));

                return crit.UniqueResult<Template>();
            }
        }

        public List<OfferPfrBankAccountSum> GetOfferPfrBankAccountSumForOffer(long offerId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(OfferPfrBankAccountSum), "offsum").CreateCriteria(
                    "offsum.Portfolio", JoinType.InnerJoin).
                    SetFetchMode("offsum.Portfolio", FetchMode.Eager).CreateCriteria("offsum.PfrBankAccount",
                        JoinType.InnerJoin)
                    .SetFetchMode("offsum.PfrBankAccount", FetchMode.Eager).CreateCriteria("offsum.Portfolio.RealYear",
                        JoinType.LeftOuterJoin)
                    .SetFetchMode("offsum.Portfolio.RealYear", FetchMode.Eager).CreateCriteria("offsum.Offer",
                        JoinType.InnerJoin).
                    Add(Restrictions.Eq("offsum.Offer.ID", offerId)).Add(
                        Restrictions.Not(Restrictions.Eq("offsum.StatusID", (long)-1)));

                var list = crit.List<OfferPfrBankAccountSumHib>().ToList();
                var portfolioTypes = GetElementByType(Element.Types.PortfolioType);
                foreach (var item in list)
                {
                    item.AccountNum = item.PfrBankAccount.AccountNumber;
                    item.Name = item.Portfolio.Year;
                    item.Year = item.Portfolio.GetRealYearName();
                    item.TypeValue = item.Portfolio.TypeID;
                    var p = portfolioTypes.FirstOrDefault(a => a.ID == item.Portfolio.TypeID);
                    item.TypeText = p == null ? "" : p.Name;
                }

                return list.Cast<OfferPfrBankAccountSum>().ToList();
            }
        }

        public IList<OfferPfrBankAccountSumListItem> GetOfferPfrBankAccountSumForOffers(long[] offerIds)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(OfferPfrBankAccountSum), "offsum").CreateCriteria(
                    "offsum.Portfolio", JoinType.InnerJoin).
                    SetFetchMode("offsum.Portfolio", FetchMode.Eager).CreateCriteria("offsum.PfrBankAccount",
                        JoinType.InnerJoin)
                    .SetFetchMode("offsum.PfrBankAccount", FetchMode.Eager).CreateCriteria("offsum.Portfolio.RealYear",
                        JoinType.LeftOuterJoin)
                    .SetFetchMode("offsum.Portfolio.RealYear", FetchMode.Eager)
                    .CreateCriteria("offsum.Offer", "offer", JoinType.InnerJoin)
                    .SetFetchMode("offsum.Offer", FetchMode.Eager).
                    CreateCriteria("offsum.PfrBankAccount.LegalEntity", JoinType.InnerJoin)
                    .SetFetchMode("offsum.PfrBankAccount.LegalEntity", FetchMode.Eager).
                    CreateCriteria("offsum.Offer.DepClaim", JoinType.InnerJoin)
                    .SetFetchMode("offsum.Offer.DepClaim", FetchMode.Eager).
                    CreateCriteria("offsum.Offer.Auction", JoinType.InnerJoin)
                    .SetFetchMode("offsum.Offer.Auction", FetchMode.Eager).
                    Add(Restrictions.In("offsum.Offer.ID", offerIds)).
                    Add(Restrictions.Not(Restrictions.Eq("offsum.StatusID", (long)-1)))
                    .Add(Restrictions.Eq("offer.DepositsGenerated", (byte)0));

                var list = crit.List<OfferPfrBankAccountSumHib>().ToList();
                var resultList = new List<OfferPfrBankAccountSumListItem>(list.Count);
                var portfolioTypes = GetElementByType(Element.Types.PortfolioType);
                foreach (OfferPfrBankAccountSumHib item in list)
                {
                    item.AccountNum = item.PfrBankAccount.AccountNumber;
                    item.Name = item.Portfolio.Year;
                    item.Year = item.Portfolio.GetRealYearName();
                    item.TypeValue = item.Portfolio.TypeID;
                    var p = portfolioTypes.FirstOrDefault(a => a.ID == item.Portfolio.TypeID);
                    item.TypeText = p == null ? "" : p.Name;

                    resultList.Add(new OfferPfrBankAccountSumListItem(item, item.PfrBankAccount.LegalEntity.ShortName,
                        item.Offer.DepClaim.SettleDate, item.Offer.Number, item.Offer.Auction.AuctionNum));
                }

                return resultList;
            }
        }

        public ActionResult GenerateDeposits(List<OfferPfrBankAccountSum> offerPfrBankAccountSums)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        var oIdL =
                            offerPfrBankAccountSums.Select(item => item.OfferId).Distinct().Cast<object>().ToArray();
                        var offers =
                            GetListByPropertyCondition(typeof(DepClaimOffer).FullName, "ID", oIdL, "in")
                                .Cast<DepClaimOfferHib>()
                                .ToList();

                        foreach (DepClaimOfferHib offer in offers)
                        {
                            //offer.IsDepositsGenerated = true;
                            //session.Update(offer);
                            offer.SetDepositsGenerated(session, true);

                            var deposit = new Deposit();
                            deposit.Status = 1;
                            deposit.DateDI = DateTime.Now.Date;
                            deposit.DepClaimOfferId = offer.ID;
                            session.Save(deposit);
                        }

                        foreach (long id in offers.Select(p => p.AuctionID).Distinct())
                            UpdateAuctionStatusInternal(session, id);

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionForHib(ex);
                throw;
            }

            return ActionResult.Success();
        }

        public List<PortfolioPFRBankAccountListItem> GetAccountsForPorPortfolio(long portfolioID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateQuery(@"
					SELECT DISTINCT acc.ID as ID, acc.AccountNumber as AccountNumber, bank.FormalizedName as BankName, type.Name as Type, ac.Name as AssignKindName, ac.ID as AssignKindID
					FROM PortfolioPFRBankAccountHib pfbacc
					JOIN pfbacc.PfrBankAccount acc
					JOIN acc.LegalEntity bank
					JOIN acc.AccountType type
                    JOIN pfbacc.AssignKind ac
					WHERE pfbacc.Portfolio.ID = :PortfolioID AND acc.StatusID != -1 AND pfbacc.StatusID !=-1
				")
                    .SetParameter("PortfolioID", portfolioID)
                    .SetResultTransformer(Transformers.AliasToBean<PortfolioPFRBankAccountListItem>());
                return crit.List<PortfolioPFRBankAccountListItem>().ToList();
            }
        }

        public List<PortfolioPFRBankAccountListItem> GetAccountsForPorPortfolioByIDs(long[] ds)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateQuery(@"
					SELECT DISTINCT acc.ID as ID, acc.AccountNumber as AccountNumber, bank.FormalizedName as BankName, type.Name as Type
					FROM PfrBankAccount acc
					JOIN acc.AccountType type
					JOIN acc.LegalEntity bank
					JOIN acc.Currency curr
					JOIN bank.Contragent cag
					WHERE acc.ID IN (:IDs) AND cag.StatusID != -1 AND acc.StatusID != -1
				")
                    .SetParameterList("IDs", ds)
                    .SetResultTransformer(Transformers.AliasToBean<PortfolioPFRBankAccountListItem>());
                return crit.List<PortfolioPFRBankAccountListItem>().ToList();
            }
        }



        /*public void UpdateNPFStatus(long? id, long value)
		{
			if (!id.HasValue) return;
			using (var session = OpenSessionByType(typeof(Contragent)))
			{
				session.CreateQuery(string.Format("update Contragent set StatusID = {0} where ID = {1}", value, id))
					.ExecuteUpdate();
			}
		}*/

#region DBFImport

        public long ImportBDate(IList<BDate> list)
        {
            long imported = 0;
            foreach (var data in list)
            {
                if (ImportBDate(data))
                    imported++;

            }
            return imported;
        }

        private bool ImportBDate(BDate data)
        {
            //TRADEDATE. SEC_ID, BOARDID
            using (var session = DataAccessSystem.OpenSession())
            {

                var items = session.CreateQuery(@"SELECT d.ID FROM pfr_basic.BDate d
                                                        join pfr_basic.Board b on b.id = d.boardId
                                                        join pfr_basic.Security s on s.id = d.sec_id
														WHERE d.TradeDate = :date
														AND COALESCE(trim(s.SecurityID),'') = :secId
														AND COALESCE(trim(b.BoardID),'')  = :boardId")
                    .SetParameter("date", data.TradeDate)
                    .SetParameter("secId", data.SecurityIdText ?? string.Empty)
                    .SetParameter("boardId", data.BoardIdText ?? string.Empty).List();

                if (items.Count == 0)
                {
                    session.SaveOrUpdate(data);
                    return true;
                }
                return false;
            }
        }

        public long ImportTDate(IList<TDate> list)
        {
            long imported = 0;
            foreach (var data in list)
            {
                if (ImportTDate(data))
                    imported++;

            }
            return imported;
        }

        private bool ImportTDate(TDate data)
        {
            //TRADEDATE, SEC_ID, TIME
            using (var session = DataAccessSystem.OpenSession())
            {

                var items = session.CreateQuery(@"SELECT d.ID FROM TDate d 
														WHERE d.TradeDate = :date
														AND COALESCE(trim(d.SecurityID),'') = :secId
														AND d.Time = :time")
                    .SetParameter("date", data.TradeDate)
                    .SetParameter("secId", data.SecurityID ?? string.Empty)
                    .SetParameter("time", data.Time).List();

                if (items.Count == 0)
                {
                    session.SaveOrUpdate(data);
                    return true;
                }
                return false;
            }
        }

        public long ImportINDate(IList<INDate> list)
        {
            long imported = 0;
            foreach (var data in list)
            {
                if (ImportINDate(data))
                    imported++;

            }
            return imported;
        }


        private bool ImportINDate(INDate data)
        {
            //TRADEDATE
            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateCriteria<INDate>()
                    .Add(Restrictions.Eq("TradeDate", data.TradeDate)).List<INDate>();

                if (items.Count == 0)
                {
                    session.SaveOrUpdate(data);
                    return true;
                }
                return false;
            }
        }

#endregion

#region СВ ДСВ ДК и уточнения

        public IList<AddSPN> GetBaseDocumentRecordsNum(long portfolioID, long periodID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(@"FROM AddSPN s WHERE s.PortfolioID=:PFID AND s.MonthID=:PID AND s.Kind = 1")
                        .SetParameter("PFID", portfolioID).SetParameter("PID", periodID).List<AddSPN>();
                return list;
            }
        }

        public List<DopSPNListItem> GetDopSPNListForDue(long addSpnid)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                decimal startSum = session.CreateQuery("SELECT a.Summ FROM AddSPN a WHERE a.ID = :AddSPNID")
                    .SetParameter("AddSPNID", addSpnid)
                    .UniqueResult<decimal>();

                // 1 Уточнение за месяц
                // 2 Уточнение пени и штрафов за месяц
                List<DopSPNListItem> dops =
                    session.CreateQuery(
                        "SELECT dop.ID as ID, 'Уточнение' as DocKind, dop.Date as Date, dop.MSumm as MSumm, dop.ChSumm as Diff FROM DopSPN dop WHERE dop.AddSpnID = :AddSPNID and dop.DocKind in (:kinds)")
                        .SetParameter("AddSPNID", addSpnid)
                        .SetParameterList("kinds", new long[] { 1, 2 })
                        .SetResultTransformer(Transformers.AliasToBean<DopSPNListItem>())
                        .List<DopSPNListItem>().ToList();

                var summ = startSum;
                dops.ForEach(d =>
                {
                    d.Diff = d.Diff;
                    //d.Diff = d.MSumm - summ;
                    summ = d.MSumm;
                });

                //Документы казначейства исключены из уточнений

                //                DopSPNListItem kdoc = session.CreateQuery(@"
                //						SELECT kdoc.ID as ID, 'Документ казначейства' as DocKind, kdoc.DocDate as Date,
                //						(
                //							CASE
                //								WHEN 
                //								( 	
                //									SELECT TRIM(LOWER(p.Type)) FROM Portfolio p WHERE p.ID in (SELECT a.PortfolioID FROM AddSPN a WHERE a.ID = :AddSPNID) 
                //								) = :prtfolioType
                //								THEN 
                //								(
                //									COALESCE(kdoc.Summ1, 0) + COALESCE(kdoc.Other1, 0) + COALESCE(kdoc.Summ2 ,0) + COALESCE(kdoc.Other2, 0) + 
                //									COALESCE(kdoc.Summ3, 0) + COALESCE(kdoc.Other3, 0) + COALESCE(kdoc.Summ4, 0) + COALESCE(kdoc.Other4, 0) + 
                //									COALESCE(kdoc.Fix, 0)
                //								)
                //								ELSE
                //								(
                //									kdoc.Dsv
                //								)
                //							END
                //						) as MSumm FROM KDoc kdoc WHERE kdoc.AddSpnID = :AddSPNID OR kdoc.AsnDsvID = :AddSPNID")
                //                    .SetParameter("prtfolioType", PortfolioIdentifier.SPN.ToLower())
                //                    .SetParameter("AddSPNID", AddSPNID)
                //                    .SetResultTransformer(Transformers.AliasToBean<DopSPNListItem>())
                //                    .List<DopSPNListItem>().FirstOrDefault();

                //                if (kdoc != null)
                //                {
                //                    if (dops.Count > 0)
                //                        kdoc.Diff = kdoc.MSumm - dops.Last().MSumm;
                //                    else
                //                        kdoc.Diff = kdoc.MSumm - startSum;
                //                    dops.Add(kdoc);
                //                }

                return dops;
            }
        }

        public List<Month> GetValidMothsListForAddPSNWithPortfolioIDAndMonth(long? pID, long? mID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"FROM Month m WHERE m.ID NOT IN 
												(
													SELECT DISTINCT a.MonthID 
													FROM AddSPN a 
													WHERE a.PortfolioID = :pID AND a.Kind = 1 AND a.MonthID IS NOT NULL
												) 
												OR m.ID = :mID
												ORDER BY m.ID")
                    //                                                ORDER BY m.ID")
                    .SetParameter("mID", mID)
                    .SetParameter("pID", pID);
                return query.List<Month>().ToList();
            }
        }


        public bool IsDateClosedByKDoc(long portfolioID, DateTime date)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //Если просто проверить дату, ищем ближайший ДК с таким портфелем
                var list = session.CreateQuery("FROM KDoc doc WHERE doc.CheckDate > :date AND doc.PortfolioID = :pfID")
                    .SetParameter("pfID", portfolioID)
                    .SetParameter("date", date)
                    .SetMaxResults(1)
                    .List<KDoc>();
                return list.Any();
            }
        }

        public bool IsAddSPNClosedByKDoc(long addSpnid)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var addSpn = session.Get<AddSPN>(addSpnid);
                if (addSpn == null || !addSpn.YearID.HasValue || !addSpn.MonthID.HasValue)
                    return false;

                var portfolioID = addSpn.PortfolioID.Value;
                var date = new DateTime((int)addSpn.YearID.Value + 2000, (int)addSpn.MonthID.Value, 1);

                //Если просто проверить дату, ищем ближайший ДК с таким портфелем
                var list = session.CreateQuery("FROM KDoc doc WHERE doc.CheckDate > :date AND doc.PortfolioID = :pfID")
                    .SetParameter("pfID", portfolioID)
                    .SetParameter("date", date)
                    .SetMaxResults(1)
                    .List<KDoc>();
                return list.Any();
            }
        }

        public bool IsKDocClosed(long checkedKDocID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var doc = session.Get<KDoc>(checkedKDocID);
                if (doc == null)
                    return true;

                //Годовой. не может блокироватся другими ДК
                var query = doc.IsYearDocument
                    ? "FROM KDoc doc WHERE doc.CheckDate > :date AND doc.ID <> :docID "
                    // Годровой может быть закрыт только следующим
                    : "FROM KDoc doc WHERE doc.CheckDate >= :date AND doc.ID <> :docID ";
                // Месячный может быть закрыт годовым с той же датой
                var list = session.CreateQuery(query)
                    .SetParameter("docID", doc.ID)
                    .SetParameter("date", doc.CheckDate.Value)
                    .SetMaxResults(1)
                    .List<KDoc>();
                return list.Where(d => d.ID != doc.ID).Any();
            }
        }

        public ActionResult CheckNewKDoc(KDoc doc)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (doc == null)
                    return ActionResult.Error(string.Empty);

                //Отчетная дата должна быть уникальна среди ДК с таким же Отчетным периодом
                var list =
                    session.CreateQuery("FROM KDoc doc WHERE doc.CheckDate = :date AND doc.ReportPeriodID = :period")
                        .SetParameter("date", doc.CheckDate)
                        .SetParameter("period", doc.ReportPeriodID)
                        .SetMaxResults(1).List<KDoc>();
                if (list.Any())
                    return
                        ActionResult.Error(
                            "Невозможно сохранить документ казначейства. В системе уже существует документ на указаную отчётную дату ({0:dd-MM-yyyy}).",
                            doc.CheckDate);

                //Должен быть ДК за предыдущий период
                var prev = session.CreateQuery("FROM KDoc doc WHERE doc.CheckDate <= :date ORDER BY doc.CheckDate DESC")
                    .SetParameter("date", doc.CheckDate)
                    .SetMaxResults(1).List<KDoc>().FirstOrDefault();
                if (prev != null)
                {
                    if (doc.IsYearDocument && doc.CheckDate != prev.CheckDate)
                        return
                            ActionResult.Error(
                                "Невозможно сохранить документ казначейства. В системе отсутствует документ казначейства за предыдущий период.");
                    if (doc.IsMonthDocument && doc.CheckDate.Value != prev.CheckDate.Value.AddMonths(1))
                        return
                            ActionResult.Error(
                                "Невозможно сохранить документ казначейства. В системе отсутствует документ казначейства за предыдущий период.");
                }

                //Должны быть СВ за период и портфель, для которого сохраняют ДК				
                //Берётся год первичного поступления и месяц из периода				
                var query = session.CreateQuery(
                    "FROM AddSPN spn WHERE spn.Kind = 1 AND spn.YearID = :year AND spn.MonthID = :month AND spn.PortfolioID = :pfID")
                    .SetParameter("year", doc.CheckDate.Value.AddDays(-1).Year % 100)
                    .SetParameter("month", doc.CheckDate.Value.AddDays(-1).Month)
                    .SetParameter("pfID", doc.PortfolioID);

                var spns = query.SetMaxResults(1).List<AddSPN>();
                if (!spns.Any())
                    return
                        ActionResult.Error(
                            "Невозможно сохранить документ казначейства. В системе отсутствуют записи страховых взносов за отчётный период документа.",
                            doc.CheckDate);


                return ActionResult.Success();
            }
        }


#endregion

#region Import UK

        //Async Service
        //public IAsyncResult BeginImportUK026(EdoOdkF026Hib import, AsyncCallback callback, object state)
        //{
        //    ImportF026UKAsyncResult asyncResult =
        //       new ImportF026UKAsyncResult(import, callback, state);

        //    ThreadPool.QueueUserWorkItem(new WaitCallback
        //        (CallbackImportF026), asyncResult);
        //    return asyncResult;

        //}

        //public ActionResult EndImportUK026(IAsyncResult ar)
        //{
        //    ActionResult actoinResult = null;
        //    if (ar != null)
        //    {
        //        using (ImportF026UKAsyncResult asyncResult = ar as ImportF026UKAsyncResult)
        //        {
        //            if (asyncResult == null)
        //                throw new ArgumentNullException("IAsyncResult parameter is null.");

        //            asyncResult.AsyncWait.WaitOne();

        //            actoinResult = asyncResult.Result;
        //        }
        //    }
        //    return actoinResult;
        //}

        //private void CallbackImportF026(object state)
        //{
        //    ImportF026UKAsyncResult asyncResult = state as ImportF026UKAsyncResult;
        //    try
        //    {
        //        asyncResult.Result = ImportUKF026(asyncResult.Input);
        //    }
        //    finally
        //    {
        //        asyncResult.Complete();
        //    }
        //}

        public ActionResult ImportUKF015(EdoOdkF015 import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {
                            import.RegNum = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };

                            session.SaveOrUpdate(import);

                            edoLog.DocId = import.ID;
                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                .Add(Restrictions.Eq("DocId", import.ID)).List<EDOLog>();

                            foreach (var repeatLog in repeatLogs)
                            {
                                if (repeatLog.ExeptId == null)
                                {
                                    repeatLog.ExeptId = 143;
                                    repeatLog.DocId = null;
                                    session.SaveOrUpdate(repeatLog);
                                }

                            }

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF025 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF016(EdoOdkF016 import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        import.RegNum = GetRegNumEDOLog();
                        import.RegDate = DateTime.Now.Date;
                        edoLog.LoadDate = DateTime.Now.Date;
                        edoLog.LoadTime = DateTime.Now.TimeOfDay;
                        edoLog.DocRegNum = import.RegNum;
                        edoLog.RegDate = DateTime.Now.Date;

                        byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                        EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };

                        session.SaveOrUpdate(import);

                        edoLog.DocId = import.ID;
                        edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                        var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                            .Add(Restrictions.Eq("DocId", import.ID)).List<EDOLog>();

                        foreach (var repeatLog in repeatLogs)
                        {
                            if (repeatLog.ExeptId == null)
                            {
                                repeatLog.ExeptId = 143;
                                repeatLog.DocId = null;
                                session.SaveOrUpdate(repeatLog);
                            }

                        }

                        session.SaveOrUpdate(edoLog);
                        transaction.Commit();

                    }



                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF025(EdoOdkF025Hib import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {


                            long idDeleteItem = import.ID;

                            import.ID = 0;
                            import.RegNum = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };

                            EdoOdkF025 f025 = new EdoOdkF025
                            {
                                Group1 = import.Group1,
                                Group2 = import.Group2,
                                Group3 = import.Group3,
                                Group4 = import.Group4,
                                Group5 = import.Group5,
                                Group6 = import.Group6,
                                Group7 = import.Group7,
                                Group8 = import.Group8,
                                Group81 = import.Group81,
                                Group9 = import.Group9,
                                Group10 = import.Group10,
                                Group11 = import.Group11,
                                Group12 = import.Group12,
                                Group13 = import.Group13,
                                Subgroup1 = import.Subgroup1,
                                Subgroup2 = import.Subgroup2,
                                Subgroup3 = import.Subgroup3,
                                INN = import.INN,
                                KPP = import.KPP,
                                ManageName = import.ManageName,
                                Name = import.Name,
                                Post = import.Post,
                                RegDate = import.RegDate,
                                RegNum = import.RegNum,
                                ReportDate = import.ReportDate,
                                ReportOnDate = import.ReportOnDate,
                                ReportTime = import.ReportTime,
                                TotalAmount = import.TotalAmount,
                                ContractId = import.ContractId
                            };
                            session.SaveOrUpdate(f025);
                            import.ID = (long)session.GetIdentifier(f025);

                            foreach (var group in import.F025Groups1)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            foreach (var group in import.F025Groups2)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            foreach (var group in import.F025Groups3)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }

                            foreach (var group in import.F025Groups4)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }

                            foreach (var group in import.F025Groups5)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            foreach (var group in import.F025Groups6)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F025Groups7)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F025Groups8)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F025Groups81)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }

                            foreach (var group in import.F025Groups9)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }
                            foreach (var group in import.F025Groups10)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }
                            foreach (var group in import.F025Groups11)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }
                            foreach (var group in import.F025Groups12)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }

                            foreach (var group in import.F025SubGroups1)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }
                            foreach (var group in import.F025SubGroups2)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }
                            foreach (var group in import.F025SubGroups3)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);
                            }

                            edoLog.DocId = import.ID;
                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);
                            session.SaveOrUpdate(edoLog);

                            if (idDeleteItem > 0)
                            {
                                session.Delete(new EdoOdkF025 { ID = idDeleteItem });

                                var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                    .Add(Restrictions.Eq("DocId", idDeleteItem)).List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }

                                }

                            }

                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF025 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF026(EdoOdkF026Hib import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {


                            long idDeleteItem = import.ID;

                            import.ID = 0;
                            import.RegNum = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };


                            EdoOdkF026 f026 = new EdoOdkF026
                            {
                                Group1 = import.Group1,
                                Group2 = import.Group2,
                                Group3 = import.Group3,
                                Group4 = import.Group4,
                                Group5 = import.Group5,
                                Group6 = import.Group6,
                                Group7 = import.Group7,
                                Group8 = import.Group8,
                                Group81 = import.Group81,
                                Group9 = import.Group9,
                                Group10 = import.Group10,
                                Group11 = import.Group11,
                                Group12 = import.Group12,
                                Group13 = import.Group13,
                                Subgroup1 = import.Subgroup1,
                                Subgroup2 = import.Subgroup2,
                                Subgroup3 = import.Subgroup3,
                                INN = import.INN,
                                KPP = import.KPP,
                                ManageName = import.ManageName,
                                Name = import.Name,
                                Post = import.Post,
                                RegDate = import.RegDate,
                                RegNum = import.RegNum,
                                ReportDate = import.ReportDate,
                                ReportOnDate = import.ReportOnDate,
                                ReportTime = import.ReportTime,
                                TotalAmount = import.TotalAmount

                            };
                            session.SaveOrUpdate(f026);
                            import.ID = (long)session.GetIdentifier(f026);
                            foreach (var group in import.F026Groups1)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            foreach (var group in import.F026Groups2)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }


                            foreach (var group in import.F026Groups3)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);


                            }

                            foreach (var group in import.F026Groups4)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            foreach (var group in import.F026Groups5)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            foreach (var group in import.F026Groups6)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026Groups7)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026Groups8)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026Groups81)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }


                            foreach (var group in import.F026Groups9)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026Groups10)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026Groups11)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026Groups12)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            foreach (var group in import.F026SubGroups1)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026SubGroups2)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }
                            foreach (var group in import.F026SubGroups3)
                            {
                                group.EdoID = import.ID;
                                session.SaveOrUpdate(group);

                            }

                            edoLog.DocId = import.ID;

                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);
                            session.SaveOrUpdate(edoLog);

                            if (idDeleteItem > 0)
                            {
                                session.Delete(new EdoOdkF026 { ID = idDeleteItem });

                                var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                    .Add(Restrictions.Eq("DocId", idDeleteItem)).List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }

                                }
                            }

                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF026 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF040(EdoOdkF040Hib import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {
                            import.RegNum = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            long idDeleteItem = import.ID;
                            import.ID = 0;
                            EdoOdkF040 f40 = new EdoOdkF040
                            {
                                ApName = import.ApName,
                                ApPost = import.ApPost,
                                ContractId = import.ContractId,
                                IdOndate = import.IdOndate,
                                IdYear = import.IdYear,
                                RegNum = import.RegNum,
                                RegDate = import.RegDate,
                                RegNumberOut = import.RegNumberOut,
                                ReportType = import.ReportType,
                                Writedate = import.Writedate,
                                Portfolio = import.Portfolio
                            };

                            session.SaveOrUpdate(f40);
                            import.ID = f40.ID;
                            foreach (var rrz in import.RRZs)
                            {
                                rrz.EdoID = f40.ID;
                                session.SaveOrUpdate(rrz);
                            }
                            foreach (var detail in import.Details)
                            {
                                detail.EdoID = f40.ID;
                                session.SaveOrUpdate(detail);
                            }



                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;


                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };



                            edoLog.DocId = import.ID;
                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            if (idDeleteItem > 0)
                            {
                                session.Delete(new EdoOdkF040 { ID = idDeleteItem });

                                var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                    .Add(Restrictions.Eq("DocId", idDeleteItem)).List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }

                                }
                            }

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF025 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF050(EdoOdkF050Hib import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {
                            import.RegNum = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            long idDeleteItem = import.ID;
                            import.ID = 0;
                            EdoOdkF050 f50 = new EdoOdkF050
                            {
                                ReportDate = import.ReportDate,
                                RegDate = DateTime.Now.Date,
                                ResPerson = import.ResPerson,
                                ContractId = import.ContractId,
                                Post = import.Post,
                                AkciiRAO1 = import.AkciiRAO1,
                                AkciiRAO2 = import.AkciiRAO2,
                                Ipotech1 = import.Ipotech1,
                                Ipotech2 = import.Ipotech2,
                                GCBSubRF1 = import.GCBSubRF1,
                                GCBSubRF2 = import.GCBSubRF2,
                                GCB1 = import.GCB1,
                                GCB2 = import.GCB2,
                                ObligaciiMO1 = import.ObligaciiMO1,
                                ObligaciiMO2 = import.ObligaciiMO2,
                                ObligaciiRHO1 = import.ObligaciiRHO1,
                                ObligaciiRHO2 = import.ObligaciiRHO2,
                                Pai1 = import.Pai1,
                                Pai2 = import.Pai2,
                                ObligMFO1 = import.ObligMFO1,
                                ObligMFO2 = import.ObligMFO2,
                                ItogoCB1 = import.ItogoCB1,
                                ItogoCB2 = import.ItogoCB2,
                                RegNum = import.RegNum,
                                RegNumOut = import.RegNumOut,
                                Portfolio = import.Portfolio
                            };
                            session.SaveOrUpdate(f50);
                            import.ID = f50.ID;
                            foreach (var info in import.F050CBInfos)
                            {
                                info.EdoID = f50.ID;
                                session.SaveOrUpdate(info);
                            }

                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;


                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };



                            edoLog.DocId = import.ID;
                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            if (idDeleteItem > 0)
                            {
                                session.Delete(new EdoOdkF050 { ID = idDeleteItem });

                                var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                    .Add(Restrictions.Eq("DocId", idDeleteItem)).List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }

                                }
                            }

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF025 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF060(EdoOdkF060 import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {
                            import.RegNumber = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            long idDeleteItem = import.ID;
                            import.ID = 0;

                            session.SaveOrUpdate(import);

                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNumber;
                            edoLog.RegDate = DateTime.Now.Date;
                            edoLog.DocId = import.ID;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };

                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            if (idDeleteItem > 0)
                            {
                                session.Delete(new EdoOdkF060 { ID = idDeleteItem });

                                var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                    .Add(Restrictions.Eq("DocId", idDeleteItem)).List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }

                                }
                            }

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF025 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF070(EdoOdkF070 import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {

                        using (var transaction = session.BeginTransaction())
                        {
                            import.RegNumber = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            long idDeleteItem = import.ID;
                            import.ID = 0;

                            session.SaveOrUpdate(import);

                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNumber;
                            edoLog.RegDate = DateTime.Now.Date;
                            edoLog.DocId = import.ID;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };

                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            if (idDeleteItem > 0)
                            {
                                session.Delete(new EdoOdkF070 { ID = idDeleteItem });

                                var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                    .Add(Restrictions.Eq("DocId", idDeleteItem)).List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }

                                }
                            }

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF080(EdoOdkF080Hib import, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {
                            import.RegNum = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            long idDeleteItem = import.ID;
                            import.ID = 0;
                            EdoOdkF080 f80 = new EdoOdkF080
                            {
                                RegDate = DateTime.Now.Date,
                                ContractId = import.ContractId,
                                InvestCaseName = import.InvestCaseName,
                                ReportOnDate = import.ReportOnDate,
                                SdPerson = import.SdPerson,
                                UkPerson = import.UkPerson,
                                RegNum = import.RegNum
                            };
                            session.SaveOrUpdate(f80);
                            import.ID = f80.ID;
                            foreach (var info in import.Deals)
                            {
                                info.EdoID = f80.ID;
                                session.SaveOrUpdate(info);
                            }

                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;
                            edoLog.DocId = import.ID;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };


                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            if (idDeleteItem > 0)
                            {
                                session.Delete(new EdoOdkF080 { ID = idDeleteItem });

                                var repeatLogs = session.CreateCriteria(typeof(EDOLog))
                                    .Add(Restrictions.Eq("DocId", idDeleteItem)).List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }

                                }
                            }

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF025 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public List<EdoOdkF140ListItem> GetReportViolationsByType(int contractType)
        {
            return GetEdoOdkF140ListItems(contractType);
        }

        public List<EdoOdkF140ListItem> GetEdoOdkF140ListItems(int? contractType = null)
        {
            return SessionWrapper(session =>
            {
                return session.CreateSQLQuery($@"
                    select odk.ID, le.SHORTNAME, odk.DOGOVORNUMBER, odk.STATUS, odk.DOCNUMBER, 
                        odk.VIOLATIONDATE, cat.INFIRM1, cat.INFIRM2, odk.STOPDATE, 
                        odk.REALSTOPDATE, odk.STOPNUMBER
                    from pfr_basic.edo_odkf140 odk
                    left join pfr_basic.category_f140 cat ON cat.ID=odk.CATEGORY_ID
                    left join pfr_basic.contract c ON c.ID=odk.id_contract
                    left join pfr_basic.legalentity le ON le.ID=c.LE_ID
                    {(!contractType.HasValue ? null : $@"WHERE c.TYPEID = {contractType} ")}")
                    .List<object[]>()
                    .Select(a =>
                        new EdoOdkF140ListItem
                        {
                            ID = (long)a[0],
                            UKName = (string)a[1],
                            ContractNumber = (string)a[2],
                            Status = (string)a[3],
                            DocNumber = (string)a[4],
                            ViolationDate = (DateTime?)a[5],
                            Infirm1 = (string)a[6],
                            Infirm2 = (string)a[7],
                            StopDate = (DateTime?)a[8],
                            RealStopDate = (DateTime?)a[9],
                            StopNumber = (string)a[10]
                        }).ToList();
            });
        }

        public ActionResult ImportUKF140(EdoOdkF140 import, EDOLog edoLog, string xmlBody, int reportTypeID)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        // LogMessageForHib("ImportUKF026 start: " + DateTime.Now.ToString());

                        using (var transaction = session.BeginTransaction())
                        {
                            //сохраним EdoOdkF140 import
                            import.RegNum = GetRegNumEDOLog();
                            import.RegDate = DateTime.Now.Date;
                            session.SaveOrUpdate(import);
                            //заполним журнал
                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = import.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;
                            edoLog.DocId = import.ID;

                            //получим документ
                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };
                            //сохраним журнал и документ
                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);
                            session.SaveOrUpdate(edoLog);

                            //запишем ссылку на документ в EDO
                            long id = 0;
                            switch (reportTypeID)
                            {
                                case 1:
                                case 3:
                                    long.TryParse(import.LinkReport, out id);
                                    import.LinkReport = edoLog.ID.ToString();
                                    break;
                                case 2:
                                case 4:
                                    long.TryParse(import.LinkStopReport, out id);
                                    import.LinkStopReport = edoLog.ID.ToString();
                                    break;
                                case 5:
                                    long.TryParse(import.LinkNoStopReport, out id);
                                    import.LinkNoStopReport = edoLog.ID.ToString();
                                    break;

                            }

                            if (id > 0)
                            {
                                var repeatLogs =
                                    session.CreateCriteria(typeof(EDOLog))
                                        .Add(Restrictions.Eq("ID", id))
                                        .List<EDOLog>();

                                foreach (var repeatLog in repeatLogs)
                                {
                                    if (repeatLog.ExeptId == null)
                                    {
                                        repeatLog.ExeptId = 143;
                                        repeatLog.DocId = null;
                                        session.SaveOrUpdate(repeatLog);
                                    }
                                }
                            }

                            //внесем ссылки
                            session.SaveOrUpdate(import);

                            transaction.Commit();

                        }

                    }
                    finally
                    {
                        //LogMessageForHib("ImportUKF025 end: " + DateTime.Now.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                // LogMessageForHib("Error - " + ex.Message);
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();
        }

        public ActionResult ImportUKF401(EdoOdkF401402 requirement, List<F401402UK> details, EDOLog edoLog,
            string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        using (var transaction = session.BeginTransaction())
                        {
                            requirement.RegNum = GetRegNumEDOLog();
                            requirement.RegDate = DateTime.Now.Date;

                            session.SaveOrUpdate(requirement);

                            foreach (var inf in details)
                            {
                                inf.EdoID = requirement.ID;
                                session.SaveOrUpdate(inf);
                            }

                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = requirement.RegNum;
                            edoLog.RegDate = DateTime.Now.Date;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };

                            edoLog.DocId = requirement.ID;
                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {
                    }
                }

            }
            catch (Exception ex)
            {
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();





        }

        public ActionResult ImportUKF402(List<F401402UK> details, EDOLog edoLog, string xmlBody)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {

                    try
                    {
                        using (var transaction = session.BeginTransaction())
                        {

                            foreach (var inf in details)
                            {

                                session.SaveOrUpdate(inf);
                            }

                            edoLog.LoadDate = DateTime.Now.Date;
                            edoLog.LoadTime = DateTime.Now.TimeOfDay;
                            edoLog.DocRegNum = GetRegNumEDOLog();
                            edoLog.RegDate = DateTime.Now.Date;

                            byte[] bytes = Encoding.UTF8.GetBytes(xmlBody);
                            EdoXmlBody edoXmlBody = new EdoXmlBody { Body = bytes };

                            edoLog.DocBodyId = (long)session.Save(edoXmlBody);

                            session.SaveOrUpdate(edoLog);
                            transaction.Commit();

                        }

                    }
                    finally
                    {
                    }
                }

            }
            catch (Exception ex)
            {
                return ActionResult.Error(ex.Message);
            }

            return ActionResult.Success();





        }











#endregion

#region EDO_LOG + F401402

        public string GetRegNumEDOLog()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string queryStr =
                    @"select DOC_REGNUM from PFR_BASIC.EDO_LOG where  ID=(select MAX(ID) from PFR_BASIC.EDO_LOG where DOC_REGNUM is not null)";
                var query = session.CreateSQLQuery(queryStr);
                string lastRegNum = query.List<string>().FirstOrDefault();
                string result = $"{DateTime.Now.ToString("yy")}-22/";
                var split = new string[0];
                if (!string.IsNullOrEmpty(lastRegNum))
                    split = lastRegNum.Split('/');
                string num = string.Empty;
                if (split.Count() >= 2)
                    num = split[1];
                int n;
                if (string.IsNullOrEmpty(num) || !int.TryParse(num, out n))
                {
                    result += $"{1:000000}";
                }
                else
                {
                    result += $"{++n:000000}";
                }
                return result;
            }
        }


        public List<EDOLogListItem> GetSuccessLoadedODKByForm2(DateTime? dateFrom, DateTime? dateTo, string form, int quartal, long year, string dogovorNum)
        {
            if (string.IsNullOrEmpty(form))
                throw new InvalidDataException("Параметр form не может быть пустым!");
            string select = "";
            switch (form.ToUpper())
            {
                case "EDO_ODKF060":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F060.REGNUMBEROUT as REGNUMBEROUT, F060.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F060 ON EDOLOG.DOC_ID = F060.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}' AND F060.QUARTAL={3} AND F060.ID_YEAR={4}", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF060.ToLower(), quartal, year);
                    break;
                case "EDO_ODKF070":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F070.REGNUMBEROUT as REGNUMBEROUT, F070.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F070 ON EDOLOG.DOC_ID = F070.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}' AND F070.QUARTAL={3} AND F070.ID_YEAR={4}", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF070.ToLower(), quartal, year);
                    break;
                
                default:
                    throw new Exception("Неизвестная форма " + form);
            }

            var queryStr = string.Format(@"SELECT LOG.ID, LOG.LOADDATE, LOG.REGDATE, LOG.DOCFORM, LOG.DOCID, LOG.DOCBODYID, CAST(NULL as VARCHAR(1)) as EXC, LOG.REGNUMBEROUT, LOG.REGNUM as DOCREGNUM, CONTRACT.REGNUM as CONTRACTNUM, LE.FORMALIZEDNAME, LOG.FILENAME as FILENAME, LOG.LTIME
                    FROM ({1}) AS LOG
					LEFT JOIN {0}.{2} CONTRACT ON CONTRACT.ID = LOG.CONTRACTID
					LEFT JOIN {0}.{3} LE ON LE.ID = CONTRACT.LE_ID
					WHERE LOG.REGDATE IS NOT NULL
					{4} {5}
					AND LOWER(LOG.DOCFORM) = :form {6} ",
                    DATA_SCHEME,
                    select,
                    TABLE_CONTRACT,
                    TABLE_LEGALENTITY,
                    dateFrom.HasValue ? "AND LOG.REGDATE >= :dateFrom" : "",
                    dateTo.HasValue ? "AND LOG.REGDATE <= :dateTo" : "",
                    " AND LOWER(CONTRACT.REGNUM) = :dogovorNum");

            return SessionWrapper(session =>
            {
                List<EDOLogListItem> result;
                var query = session.CreateSQLQuery(queryStr);
                if (dateFrom.HasValue)
                    query.SetParameter("dateFrom", dateFrom.Value.Date); //.ToString(DATE_FORMAT_STRING)
                if (dateTo.HasValue)
                    query.SetParameter("dateTo", dateTo.Value.Date);
                query.SetParameter("form", form.ToLower());
                if (!string.IsNullOrEmpty(dogovorNum))
                    query.SetParameter("dogovorNum", dogovorNum.ToLower());

                result = query.List<object[]>().Select(raw =>
                {
                    var item = new EDOLogListItem
                    {
                        ID = (long)raw[0],
                        LoadDate = raw[1] == null ? null : (DateTime?)Convert.ToDateTime(raw[1]),
                        RegDate = raw[2] == null ? null : (DateTime?)Convert.ToDateTime(raw[2]),
                        DocForm = raw[3] as string,
                        DocID = raw[4] as long?,
                        DocBodyId = raw[5] as long?,
                        Exception = raw[6] as string,
                        DocRegNumberOut = raw[7] as string,
                        DocRegNum = raw[8] as string,
                        ContractNum = raw[9] as string,
                        UKName = raw[10] as string,
                        Filename = raw[11] as string
                    };

                    var time = raw[12] as string;

                    if (item.LoadDate.HasValue && !string.IsNullOrEmpty(time))
                    {
                        item.LoadDate =
                            item.LoadDate.Value.AddHours(
                                time.ParseTime().Value.Hour);
                        item.LoadDate =
                            item.LoadDate.Value.AddMinutes(
                                time.ParseTime().Value.Minute);
                        item.LoadDate =
                            item.LoadDate.Value.AddSeconds(
                                time.ParseTime().Value.Second);
                    }

                    return item;
                }).ToList();
                return result;
            });
        }

        public List<EDOLogListItem> GetSuccessLoadedODKByForm(DateTime? dateFrom, DateTime? dateTo, string form, string docRegNumberOut = null)
        {
            if (string.IsNullOrEmpty(form))
                throw new InvalidDataException("Параметр form не может быть пустым!");
            string select = "";

            switch (form.ToUpper())
            {
                case "EDO_ODKF010":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F010.REGNUMBEROUT as REGNUMBEROUT, F010.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F010 ON EDOLOG.DOC_ID = F010.ID
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF010.ToLower());
                    break;
                case "EDO_ODKF015":
                case "EDO_ODKF012":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F015.REGNUMBEROUT as REGNUMBEROUT, F015.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F015 ON EDOLOG.DOC_ID = F015.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}' OR LOWER(EDOLOG.DOC_FORM) = '{3}' ", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF015.ToLower(), TABLE_EDO_ODKF012.ToLower());
                    break;
                case "EDO_ODKF016":
                case "EDO_ODKF014":
                    select = string.Format(@" SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F016.REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F016 ON EDOLOG.DOC_ID = F016.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}' OR LOWER(EDOLOG.DOC_FORM) = '{3}' ", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF016.ToLower(), TABLE_EDO_ODKF014.ToLower());
                    break;
                case "EDO_ODKF020":
                    select = string.Format(@" SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F020.REGNUMBEROUT as REGNUMBEROUT, F020.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F020 ON EDOLOG.DOC_ID = F020.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF020.ToLower());
                    break;
                case "EDO_ODKF025":
                case "EDO_ODKF022":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, F025.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F025 ON EDOLOG.DOC_ID = F025.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'  OR LOWER(EDOLOG.DOC_FORM) = '{3}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF025.ToLower(), TABLE_EDO_ODKF022.ToLower());
                    break;
                case "EDO_ODKF040":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F040.REGNUMBEROUT as REGNUMBEROUT, F040.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F040 ON EDOLOG.DOC_ID = F040.ID
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF040.ToLower());
                    break;
                case "EDO_ODKF050":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F050.REGNUMBEROUT as REGNUMBEROUT, F050.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F050 ON EDOLOG.DOC_ID = F050.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF050.ToLower());
                    break;
                case "EDO_ODKF060":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F060.REGNUMBEROUT as REGNUMBEROUT, F060.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F060 ON EDOLOG.DOC_ID = F060.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF060.ToLower());
                    break;
                case "EDO_ODKF070":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F070.REGNUMBEROUT as REGNUMBEROUT, F070.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F070 ON EDOLOG.DOC_ID = F070.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF070.ToLower());
                    break;
                case "EDO_ODKF080":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, F080.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F080 ON EDOLOG.DOC_ID = F080.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF080.ToLower());
                    break;
                case "EDO_ODKF140":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F140.REGNUMBEROUT as REGNUMBEROUT, F140.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					INNER JOIN {0}.{2} F140 ON EDOLOG.DOC_ID = F140.ID
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}'", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF140.ToLower());
                    break;
                case "EDO_ODKF401402":
                case "EDO_ODKF401":
                case "EDO_ODKF402":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					WHERE (LOWER(EDOLOG.DOC_FORM) = 'edo_odkf401' OR LOWER(EDOLOG.DOC_FORM) = 'edo_odkf402') AND EDOLOG.DOC_ID IN (SELECT ID FROM {0}.{2})", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF401402);
                    break;
                case "EDO_ODKF026":
                case "EDO_ODKF024":
                    select = string.Format(@"SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{1} as EDOLOG
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}' OR LOWER(EDOLOG.DOC_FORM) = '{3}' AND EDOLOG.DOC_ID IN (SELECT ID FROM {0}.{2}) ", DATA_SCHEME, TABLE_EDO_LOG, TABLE_EDO_ODKF026.ToLower(), TABLE_EDO_ODKF024.ToLower());
                    break;

                default:
                    throw new Exception("Неизвестная форма " + form);
            }
            var queryStr = string.Format(@"SELECT LOG.ID, LOG.LOADDATE, LOG.REGDATE, LOG.DOCFORM, LOG.DOCID, LOG.DOCBODYID, CAST(NULL as VARCHAR(1)) as EXC, LOG.REGNUMBEROUT, LOG.REGNUM as DOCREGNUM, CONTRACT.REGNUM as CONTRACTNUM, LE.FORMALIZEDNAME, LOG.FILENAME as FILENAME, LOG.LTIME
                    FROM ({1}) AS LOG
					LEFT JOIN {0}.{2} CONTRACT ON CONTRACT.ID = LOG.CONTRACTID
					LEFT JOIN {0}.{3} LE ON LE.ID = CONTRACT.LE_ID
					WHERE LOG.REGDATE IS NOT NULL
					{4} {5}
					AND LOWER(LOG.DOCFORM) = :form {6} ",
                    DATA_SCHEME,
                    select,
                    TABLE_CONTRACT,
                    TABLE_LEGALENTITY,
                    dateFrom.HasValue ? "AND LOG.REGDATE >= :dateFrom" : "",
                    dateTo.HasValue ? "AND LOG.REGDATE <= :dateTo" : "",
                    string.IsNullOrEmpty(docRegNumberOut) ? "" : " AND LOG.REGNUMBEROUT = :regnumout");

            return SessionWrapper(session =>
            {
                List<EDOLogListItem> result;
                var query = session.CreateSQLQuery(queryStr);
                if (dateFrom.HasValue)
                    query.SetParameter("dateFrom", dateFrom.Value.Date); //.ToString(DATE_FORMAT_STRING)
                if (dateTo.HasValue)
                    query.SetParameter("dateTo", dateTo.Value.Date);
                query.SetParameter("form", form.ToLower());
                if (!string.IsNullOrEmpty(docRegNumberOut))
                    query.SetParameter("regnumout", docRegNumberOut);

                result = query.List<object[]>().Select(raw =>
                {
                    var item = new EDOLogListItem
                    {
                        ID = (long)raw[0],
                        LoadDate = raw[1] == null ? null : (DateTime?)Convert.ToDateTime(raw[1]),
                        RegDate = raw[2] == null ? null : (DateTime?)Convert.ToDateTime(raw[2]),
                        DocForm = raw[3] as string,
                        DocID = raw[4] as long?,
                        DocBodyId = raw[5] as long?,
                        Exception = raw[6] as string,
                        DocRegNumberOut = raw[7] as string,
                        DocRegNum = raw[8] as string,
                        ContractNum = raw[9] as string,
                        UKName = raw[10] as string,
                        Filename = raw[11] as string
                    };

                    var time = raw[12] as string;

                    if (item.LoadDate.HasValue && !string.IsNullOrEmpty(time))
                    {
                        item.LoadDate =
                            item.LoadDate.Value.AddHours(
                                time.ParseTime().Value.Hour);
                        item.LoadDate =
                            item.LoadDate.Value.AddMinutes(
                                time.ParseTime().Value.Minute);
                        item.LoadDate =
                            item.LoadDate.Value.AddSeconds(
                                time.ParseTime().Value.Second);
                    }

                    return item;
                }).ToList();
                return result;
            });
        }



        public List<EDOLogListItem> GetLoadedODKLogList(bool success, DateTime? dateFrom, DateTime? dateTo, string form)
        {
            List<EDOLogListItem> result;
            using (var session = DataAccessSystem.OpenSession())
            {
                string queryStr;
                if (success)
                {
                    queryStr = string.Format(@"

                    SELECT LOG.ID, LOG.LOADDATE, LOG.REGDATE, LOG.DOCFORM, LOG.DOCID, LOG.DOCBODYID, CAST(NULL as VARCHAR(1)) as EXC, LOG.REGNUMBEROUT, LOG.REGNUM as DOCREGNUM, CONTRACT.REGNUM as CONTRACTNUM, LE.FORMALIZEDNAME, LOG.FILENAME as FILENAME, LOG.LTIME
					FROM
					(
					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F010.REGNUMBEROUT as REGNUMBEROUT, F010.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{1} F010 ON EDOLOG.DOC_ID = F010.ID
					WHERE LOWER(EDOLOG.DOC_FORM) = '{1}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F015.REGNUMBEROUT as REGNUMBEROUT, F015.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{2} F015 ON EDOLOG.DOC_ID = F015.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{2}' OR LOWER(EDOLOG.DOC_FORM) = '{21}' 

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F016.REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{3} F016 ON EDOLOG.DOC_ID = F016.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{3}' OR LOWER(EDOLOG.DOC_FORM) = '{22}' 

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F020.REGNUMBEROUT as REGNUMBEROUT, F020.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{4} F020 ON EDOLOG.DOC_ID = F020.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{4}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, F025.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{5} F025 ON EDOLOG.DOC_ID = F025.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{5}'  OR LOWER(EDOLOG.DOC_FORM) = '{20}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					WHERE (LOWER(EDOLOG.DOC_FORM) = '{6}' OR LOWER(EDOLOG.DOC_FORM) = '{23}' ) AND EDOLOG.DOC_ID IN (SELECT ID FROM {0}.{6})

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F040.REGNUMBEROUT as REGNUMBEROUT, F040.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{7} F040 ON EDOLOG.DOC_ID = F040.ID
					WHERE LOWER(EDOLOG.DOC_FORM) = '{7}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F050.REGNUMBEROUT as REGNUMBEROUT, F050.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{8} F050 ON EDOLOG.DOC_ID = F050.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{8}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F060.REGNUMBEROUT as REGNUMBEROUT, F060.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{9} F060 ON EDOLOG.DOC_ID = F060.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{9}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F070.REGNUMBEROUT as REGNUMBEROUT, F070.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{10} F070 ON EDOLOG.DOC_ID = F070.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{10}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, F080.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{11} F080 ON EDOLOG.DOC_ID = F080.ID 
					WHERE LOWER(EDOLOG.DOC_FORM) = '{11}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F140.REGNUMBEROUT as REGNUMBEROUT, F140.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{12} F140 ON EDOLOG.DOC_ID = F140.ID
					WHERE LOWER(EDOLOG.DOC_FORM) = '{12}'

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					WHERE (LOWER(EDOLOG.DOC_FORM) = 'edo_odkf401' OR LOWER(EDOLOG.DOC_FORM) = 'edo_odkf402') AND EDOLOG.DOC_ID IN (SELECT ID FROM {0}.{13})
					) AS LOG
					LEFT JOIN {0}.{15} CONTRACT ON CONTRACT.ID = LOG.CONTRACTID
					LEFT JOIN {0}.{16} LE ON LE.ID = CONTRACT.LE_ID
					WHERE LOG.REGDATE IS NOT NULL
					{17} {18}
					{19}",
                        DATA_SCHEME,
                        TABLE_EDO_ODKF010.ToLower(),
                        TABLE_EDO_ODKF015.ToLower(),
                        TABLE_EDO_ODKF016.ToLower(),
                        TABLE_EDO_ODKF020.ToLower(),
                        TABLE_EDO_ODKF025.ToLower(),
                        TABLE_EDO_ODKF026.ToLower(),
                        TABLE_EDO_ODKF040.ToLower(),
                        TABLE_EDO_ODKF050.ToLower(),
                        TABLE_EDO_ODKF060.ToLower(),
                        TABLE_EDO_ODKF070.ToLower(),
                        TABLE_EDO_ODKF080.ToLower(),
                        TABLE_EDO_ODKF140.ToLower(),
                        TABLE_EDO_ODKF401402.ToLower(),
                        TABLE_EDO_LOG.ToLower(),
                        TABLE_CONTRACT,
                        TABLE_LEGALENTITY,
                        dateFrom.HasValue ? "AND LOG.REGDATE >= :dateFrom" : "",
                        dateTo.HasValue ? "AND LOG.REGDATE <= :dateTo" : "",
                        form == null ? "" : "AND LOWER(LOG.DOCFORM) = LOWER(:form)",
                        TABLE_EDO_ODKF022,
                        TABLE_EDO_ODKF012,//21
                        TABLE_EDO_ODKF014,
                        TABLE_EDO_ODKF024);
                }
                else
                {
                    queryStr = string.Format(@"
					SELECT LOG.ID, LOG.LOAD_DATE, CAST(NULL as DATE), LOG.DOC_FORM, CAST(NULL as BIGINT), LOG.DOC_BODY_ID, EXC.NAME, CAST(NULL as VARCHAR(1)), CAST(NULL as VARCHAR(1)), CAST(NULL as VARCHAR(1)), CAST(NULL as VARCHAR(1)), LOG.FILENAME as FILENAME, CAST(LOG.LOAD_TIME as VARCHAR(10))
					FROM {0}.{1} as LOG
					LEFT JOIN {0}.{2} EXC ON EXC.ID = LOG.EXEPT_ID
					WHERE LOG.EXEPT_ID IS NOT NULL
					{3} {4}
					{5}",
                        DATA_SCHEME,
                        TABLE_EDO_LOG,
                        TABLE_EDO_EXEPTION,
                        dateFrom.HasValue ? "AND LOG.LOAD_DATE >= :dateFrom" : "",
                        dateTo.HasValue ? "AND LOG.LOAD_DATE <= :dateTo" : "",
                        form == null ? "" : "AND LOWER(LOG.DOC_FORM) = :form");
                }

                var query = session.CreateSQLQuery(queryStr);
                if (dateFrom.HasValue)
                    query.SetParameter("dateFrom", dateFrom.Value.Date); //.ToString(DATE_FORMAT_STRING)
                if (dateTo.HasValue)
                    query.SetParameter("dateTo", dateTo.Value.Date);
                if (form != null)
                    query.SetString("form", form.ToLower());
                result = query.List().Cast<object[]>().ToList().ConvertAll(delegate (object[] raw)
                {
                    var item = new EDOLogListItem
                    {
                        ID = (long)raw[0],
                        LoadDate = raw[1] == null ? null : (DateTime?)Convert.ToDateTime(raw[1]),
                        RegDate = raw[2] == null ? null : (DateTime?)Convert.ToDateTime(raw[2]),
                        DocForm = raw[3] as string,
                        DocID = raw[4] as long?,
                        DocBodyId = raw[5] as long?,
                        Exception = raw[6] as string,
                        DocRegNumberOut = raw[7] as string,
                        DocRegNum = raw[8] as string,
                        ContractNum = raw[9] as string,
                        UKName = raw[10] as string,
                        Filename = raw[11] as string
                    };

                    var time = raw[12] as string;

                    if (item.LoadDate.HasValue && !string.IsNullOrEmpty(time))
                    {
                        item.LoadDate =
                            item.LoadDate.Value.AddHours(
                                time.ParseTime().Value.Hour);
                        item.LoadDate =
                            item.LoadDate.Value.AddMinutes(
                                time.ParseTime().Value.Minute);
                        item.LoadDate =
                            item.LoadDate.Value.AddSeconds(
                                time.ParseTime().Value.Second);
                    }

                    return item;
                });
            }
            return result;
        }

        public List<EDOLogListItem> GetLoadedSuccessODKLogListByTypeContract(DateTime? dateFrom, DateTime? dateTo,
            string form, int contractType)
        {
            List<EDOLogListItem> result;
            using (var session = DataAccessSystem.OpenSession())
            {
                string queryStr;


                queryStr = string.Format(@"
					SELECT LOG.ID, LOG.LOADDATE, LOG.REGDATE, LOG.DOCFORM, LOG.DOCID, LOG.DOCBODYID, CAST(NULL as VARCHAR(1)) as EXC, LOG.REGNUMBEROUT, LOG.REGNUM as DOCREGNUM, CONTRACT.REGNUM as CONTRACTNUM, LE.FORMALIZEDNAME, LOG.FILENAME as FILENAME, LOG.LTIME
					FROM
					(
					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F010.REGNUMBEROUT as REGNUMBEROUT, F010.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{1} F010 ON EDOLOG.DOC_ID = F010.ID
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{1}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F015.REGNUMBEROUT as REGNUMBEROUT, F015.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{2} F015 ON EDOLOG.DOC_ID = F015.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{2}')  OR UPPER(EDOLOG.DOC_FORM) = UPPER('{22}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F016.REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{3} F016 ON EDOLOG.DOC_ID = F016.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{3}') OR UPPER(EDOLOG.DOC_FORM) = UPPER('{23}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F020.REGNUMBEROUT as REGNUMBEROUT, F020.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{4} F020 ON EDOLOG.DOC_ID = F020.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{4}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, F025.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{5} F025 ON EDOLOG.DOC_ID = F025.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{5}') OR UPPER(EDOLOG.DOC_FORM) = UPPER('{21}') 
					
					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					WHERE (UPPER(EDOLOG.DOC_FORM) = UPPER('{6}') OR UPPER(EDOLOG.DOC_FORM) = UPPER('{24}'))  AND EDOLOG.DOC_ID IN (SELECT ID FROM {0}.{6})

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F040.REGNUMBEROUT as REGNUMBEROUT, F040.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{7} F040 ON EDOLOG.DOC_ID = F040.ID
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{7}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F050.REGNUMBEROUT as REGNUMBEROUT, F050.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{8} F050 ON EDOLOG.DOC_ID = F050.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{8}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F060.REGNUMBEROUT as REGNUMBEROUT, F060.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{9} F060 ON EDOLOG.DOC_ID = F060.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{9}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F070.REGNUMBEROUT as REGNUMBEROUT, F070.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{10} F070 ON EDOLOG.DOC_ID = F070.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{10}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, F080.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{11} F080 ON EDOLOG.DOC_ID = F080.ID 
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{11}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, F140.REGNUMBEROUT as REGNUMBEROUT, F140.ID_CONTRACT as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					INNER JOIN {0}.{12} F140 ON EDOLOG.DOC_ID = F140.ID
					WHERE UPPER(EDOLOG.DOC_FORM) = UPPER('{12}')

					UNION ALL

					SELECT EDOLOG.ID as ID, EDOLOG.LOAD_DATE AS LOADDATE, EDOLOG.DOC_FORM as DOCFORM, EDOLOG.DOC_BODY_ID as DOCBODYID, EDOLOG.DOC_REGDATE as REGDATE, EDOLOG.DOC_REGNUM as REGNUM, EDOLOG.DOC_REGNUMBEROUT as REGNUMBEROUT, CAST(NULL as BIGINT) as CONTRACTID, EDOLOG.DOC_ID as DOCID, EDOLOG.FILENAME as FILENAME, CAST(EDOLOG.LOAD_TIME as VARCHAR(10)) as LTIME
						FROM {0}.{14} as EDOLOG
					WHERE (UPPER(EDOLOG.DOC_FORM) = 'EDO_ODKF401' OR UPPER(EDOLOG.DOC_FORM) = 'EDO_ODKF402') AND EDOLOG.DOC_ID IN (SELECT ID FROM {0}.{13})
					) AS LOG
					LEFT JOIN {0}.{15} CONTRACT ON CONTRACT.ID = LOG.CONTRACTID
					LEFT JOIN {0}.{16} LE ON LE.ID = CONTRACT.LE_ID
					WHERE LOG.REGDATE IS NOT NULL AND (CONTRACT.TYPEID={20} OR  CONTRACT.TYPEID IS NULL) 
					{17} {18}
					{19}",
                    DATA_SCHEME,
                    TABLE_EDO_ODKF010,
                    TABLE_EDO_ODKF015,
                    TABLE_EDO_ODKF016,
                    TABLE_EDO_ODKF020,
                    TABLE_EDO_ODKF025,
                    TABLE_EDO_ODKF026,
                    TABLE_EDO_ODKF040,
                    TABLE_EDO_ODKF050,
                    TABLE_EDO_ODKF060,
                    TABLE_EDO_ODKF070,
                    TABLE_EDO_ODKF080,
                    TABLE_EDO_ODKF140,
                    TABLE_EDO_ODKF401402,
                    TABLE_EDO_LOG,
                    TABLE_CONTRACT,
                    TABLE_LEGALENTITY,
                    dateFrom.HasValue ? "AND LOG.REGDATE >= :dateFrom" : "",
                    dateTo.HasValue ? "AND LOG.REGDATE <= :dateTo" : "",
                    form == null ? "" : "AND UPPER(LOG.DOCFORM) = UPPER(:form)",
                    contractType,
                    TABLE_EDO_ODKF022,
                    TABLE_EDO_ODKF012,
                    TABLE_EDO_ODKF014,
                    TABLE_EDO_ODKF024);


                var query = session.CreateSQLQuery(queryStr);
                if (dateFrom.HasValue)
                    query.SetParameter("dateFrom", dateFrom.Value.Date);
                if (dateTo.HasValue)
                    query.SetParameter("dateTo", dateTo.Value.Date);
                if (form != null)
                    query.SetParameter("form", form);
                result = query.List().Cast<object[]>().ToList().ConvertAll(delegate (object[] raw)
                {
                    var item = new EDOLogListItem
                    {
                        ID = (long)raw[0],
                        LoadDate = raw[1] as DateTime?,
                        RegDate = raw[2] as DateTime?,
                        DocForm = raw[3] as string,
                        DocID = raw[4] as long?,
                        DocBodyId = raw[5] as long?,
                        Exception = raw[6] as string,
                        DocRegNumberOut = raw[7] as string,
                        DocRegNum = raw[8] as string,
                        ContractNum = raw[9] as string,
                        UKName = raw[10] as string,
                        Filename = raw[11] as string
                    };

                    var time = raw[12] as string;

                    if (item.LoadDate.HasValue && !string.IsNullOrEmpty(time))
                    {
                        item.LoadDate =
                            item.LoadDate.Value.AddHours(
                                time.ParseTime().Value.Hour);
                        item.LoadDate =
                            item.LoadDate.Value.AddMinutes(
                                time.ParseTime().Value.Minute);
                        item.LoadDate =
                            item.LoadDate.Value.AddSeconds(
                                time.ParseTime().Value.Second);
                    }

                    return item;
                });
            }
            return result;
        }





        public long GetCountF401402Filtered(Document.Types contractType, DateTime? from, DateTime? to)
        {
            var start = from.HasValue ? new DateTime?(from.Value.Date) : null;
            var end = to.HasValue ? new DateTime?(to.Value.Date) : null;
            return SessionWrapper(session =>
            {
                string hql =
                    @"
					SELECT  COUNT(*) 
					FROM F401402UK F
					JOIN F.Document EDOF401402
                    JOIN F.Contract C
                    WHERE C.TypeID = :contractTypeID 
                            AND (:from is null OR EDOF401402.FromDate >= :from)
                            AND (:to is null OR EDOF401402.FromDate <= :to)";
                var query = session.CreateQuery(hql)
                    .SetParameter("contractTypeID", (int)contractType)
                    .SetParameter("from", start)
                    .SetParameter("to", end);
                return query.UniqueResult<long>();
            });
        }


        public List<F401402UKListItem> GetF401402ListFilteredByPage(Document.Types contractType, int startIndex,
            DateTime? from, DateTime? to)
        {
            return SessionWrapper(session =>
            {
                var start = from.HasValue ? new DateTime?(from.Value.Date) : null;
                var end = to.HasValue ? new DateTime?(to.Value.Date) : null;

                const string hql = @"
					SELECT F.ID as ID, F.EdoID as EdoID, F.DogSDNum as DogSDNum, F.Itogo as Itogo, F.PayFromTransh as PayFromTransh, 
					F.ControlRegnum as ControlRegnum, F.ControlGetDate as ControlGetDate, F.ControlSetDate as ControlSetDate, F.ControlFactDate as ControlFactDate, F.ControlSendDate as ControlSendDate,
					le.FormalizedName as UKName, C.ContractNumber as ContractNumber, F.StatusID as StatusId, st.Name as StatusName, EDOF401402.FromDate as FromDate
					FROM F401402UKHib F
					JOIN F.Document EDOF401402
                    JOIN F.Contract C
                    JOIN C.LegalEntity le
                    JOIN F.FStatus st
                    WHERE C.TypeID = :contractTypeID 
                            AND (:from is null OR EDOF401402.FromDate >= :from)
                            AND (:to is null OR EDOF401402.FromDate <= :to)
                    order by F.ID";
                var query = session.CreateQuery(hql)
                    .SetParameter("contractTypeID", (int)contractType)
                    .SetParameter("from", start)
                    .SetParameter("to", end)
                    .SetFirstResult(startIndex).SetMaxResults(PageSize)
                    .SetResultTransformer(Transformers.AliasToBean<F401402UKListItem>());
                return query.List<F401402UKListItem>().ToList();
            });
        }

        public long GetCountF401402(Document.Types contractType)
        {
            return SessionWrapper(session =>
            {
                var query = session.CreateQuery(@"
					SELECT COUNT(*) 
					FROM F401402UKHib F
					JOIN F.Document EDOF401402
                    JOIN F.Contract C
                    WHERE C.TypeID = :contractTypeID")
                    .SetParameter("contractTypeID", (int)contractType);
                return query.UniqueResult<long>();
            });
        }

        public List<F401402UKListItem> GetF401402ListByPage(Document.Types contractType, int startIndex)
        {
            return SessionWrapper(session =>
            {
                var query = session.CreateQuery(@"
					SELECT F.ID as ID, F.EdoID as EdoID, F.DogSDNum as DogSDNum, F.Itogo as Itogo, F.PayFromTransh as PayFromTransh, 
					F.ControlRegnum as ControlRegnum, F.ControlGetDate as ControlGetDate, F.ControlSetDate as ControlSetDate, F.ControlFactDate as ControlFactDate, F.ControlSendDate as ControlSendDate,
					le.FormalizedName as UKName, C.ContractNumber as ContractNumber, F.StatusID as StatusId, st.Name as StatusName, EDOF401402.FromDate as FromDate
					FROM F401402UKHib F
					JOIN F.Document EDOF401402
                    JOIN F.Contract C
                    JOIN C.LegalEntity le
                    JOIN F.FStatus st
                    WHERE C.TypeID = :contractTypeID
                    order by F.ID")
                    .SetParameter("contractTypeID", (int)contractType)
                    .SetFirstResult(startIndex)
                    .SetMaxResults(PageSize)
                    .SetResultTransformer(Transformers.AliasToBean<F401402UKListItem>());
                return query.List<F401402UKListItem>().ToList();
            });
        }

        public List<F401402UKListItem> GetF401402ListByID(Document.Types contractType, long id)
        {
            using (ISession session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
					SELECT F.ID as ID, F.EdoID as EdoID, F.DogSDNum as DogSDNum, F.Itogo as Itogo, F.PayFromTransh as PayFromTransh, 
					F.ControlRegnum as ControlRegnum, F.ControlGetDate as ControlGetDate, F.ControlSetDate as ControlSetDate, F.ControlFactDate as ControlFactDate, F.ControlSendDate as ControlSendDate,
					F.UKName as UKName, F.ContractNumber as ContractNumber, F.StatusID as StatusId, F.StatusName as StatusName, EDOF401402.FromDate as FromDate
					FROM F401402UKHib F
					JOIN F.Document EDOF401402
                    JOIN F.Contract C
                    WHERE C.TypeID = :contractTypeID AND F.ID = :f401402ukID")
                    .SetParameter("contractTypeID", (int)contractType).SetParameter("f401402ukID", id)
                    .SetResultTransformer(Transformers.AliasToBean<F401402UKListItem>());
                return query.List<F401402UKListItem>().ToList();
            }
        }


        public PFR_INVEST.DataObjects.Contract GetSDContractOnDate(DateTime date)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("SELECT c FROM Contract c "
                                               + " WHERE LOWER(c.LegalEntity.Contragent.TypeName) = :type "
                                               + "		AND (c.DissolutionDate IS NULL OR c.DissolutionDate > :date) "
                                               + "		AND c.ContractDate < :date").SetString("type", "сд")
                    .SetParameter("date", date)
                    .List<PFR_INVEST.DataObjects.Contract>();

                return list.FirstOrDefault();
            }
        }

        public long? GetOriginalXmlIDForEdoOdkF401402(long edoOdkF401402Id, string regNumberOut)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var log = session.CreateCriteria<EDOLog>()
                    .Add(Restrictions.Or(Restrictions.Eq("DocTable", "EDO_ODKF401"),
                        Restrictions.Eq("DocTable", "EDO_ODKF402")))
                    //.Add(Restrictions.Eq("DocRegNumberOut", regNumberOut))
                    .Add(Restrictions.Eq("DocId", edoOdkF401402Id))
                    .List<EDOLog>().OrderByDescending(a => a.DocTable).FirstOrDefault();

                return log != null ? log.DocBodyId : null;

                /*var Log = session.CreateQuery("SELECT c FROM EDOLog c "
					+ " WHERE c.DocId = :type ")
					.SetInt64("type", EdoOdkF401402Id)
					.List<EDOLog>().FirstOrDefault();
				return Log != null ? Log.DocBodyId : null;*/
            }
        }

#endregion

#region Страхование СИ


        public List<InsuranceListItem> GetInsuranceList(bool archive)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string sqlQuery = $@"
					SELECT 
						insDoc.ID,
						insDoc.CLOSE_DATE as CloseDate, 
						insDoc.SUM as Sum,
						le.FORMALIZEDNAME as LegalEntity, 
						contract.REGNUM as RegNum, 
						CASE WHEN siReg.DIR_SPN_ID = 1 THEN reqTr.STARTSUM - reqTr.SUM + reqTr.INVESTDOHOD ELSE reqTr.STARTSUM + reqTr.SUM END TotalInmanagement
					FROM PFR_BASIC.INSURANCE_DOC insDoc 
					INNER JOIN PFR_BASIC.CONTRACT contract ON insDoc.CONTRACT_ID = contract.ID
					INNER JOIN PFR_BASIC.LEGALENTITY le ON contract.LE_ID = le.ID
					LEFT JOIN PFR_BASIC.SI_TRANSFER siTr ON  contract.ID = siTr.CONTR_ID AND siTr.STATUS_ID <> -1
					LEFT JOIN PFR_BASIC.SI_REGISTER siReg ON  siReg.ID = siTr.SI_REG_ID AND siReg.STATUS_ID <> -1
					LEFT JOIN PFR_BASIC.REQ_TRANSFER reqTr ON  siTr.ID = reqTr.SI_TR_ID AND reqTr.STATUS_ID <> -1
					LEFT JOIN 
						(
						SELECT 
							siTr2.CONTR_ID as CONTRACT_ID,
							MAX(reqTr2.ID) as REQ_TRANSFER_ID
						FROM PFR_BASIC.SI_TRANSFER siTr2
						INNER JOIN PFR_BASIC.REQ_TRANSFER reqTr2 ON siTr2.ID = reqTr2.SI_TR_ID AND reqTr2.STATUS_ID <> -1
						INNER JOIN ( 
							SELECT 
								siTr1.CONTR_ID as CONTRACT_ID, 
								MAX(reqTr1.ACT_DATE) as DATE
							FROM PFR_BASIC.SI_TRANSFER siTr1
							INNER JOIN PFR_BASIC.REQ_TRANSFER reqTr1 ON  siTr1.ID = reqTr1.SI_TR_ID AND reqTr1.STATUS_ID <> -1
							GROUP BY siTr1.CONTR_ID

							) maxDate ON maxDate.CONTRACT_ID = siTr2.CONTR_ID AND reqTr2.ACT_DATE = maxDate.DATE
                        WHERE siTr2.STATUS_ID <> -1
						GROUP BY siTr2.CONTR_ID
						) lastTr ON contract.ID = lastTr.CONTRACT_ID AND reqTr.ID = lastTr.REQ_TRANSFER_ID
						WHERE insDoc.CLOSE_DATE {(archive ? "<" : ">=")} '{DateTime.Today.ToString(DATE_FORMAT_STRING)}'";

                var query = session.CreateSQLQuery(sqlQuery)
                    .AddScalar("ID", NHibernateUtil.Int64)
                    .AddScalar("CloseDate", NHibernateUtil.Date)
                    .AddScalar("Sum", NHibernateUtil.Decimal)
                    .AddScalar("LegalEntity", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String)
                    .AddScalar("TotalInmanagement", NHibernateUtil.Decimal)
                    .SetResultTransformer(Transformers.AliasToBean<InsuranceListItem>());
                var result = query.List<InsuranceListItem>().ToList();

                //300 а не 300000000 потому что decimal в DB2 и .Net не дружат и nHibernate генерит запрос в результате которого decimal(19,5) 300,00000 превращается в 300000000
                result.ForEach(delegate (InsuranceListItem item)
                {
                    item.Sum = FixDecimal(item.Sum, 4) ?? 0;
                    item.TotalInmanagement = FixDecimal(item.TotalInmanagement, 4) ?? 0;
                });
                return result;
            }
        }

        public List<InsuranceListItem> GetInsuranceListByType(bool archive, int type)
        {
            using (var session = DataAccessSystem.OpenSession())
            {


                string sqlQuery = $@"
					SELECT 
	                    insDoc.ID,
	                    insDoc.CLOSE_DATE as CloseDate, 
	                    insDoc.SUM as Sum,
	                    le.FORMALIZEDNAME as LegalEntity, 
	                    contract.REGNUM as RegNum, 	                    
	                    coalesce(contract.OPERATESUMM,0) AS TotalInmanagement	                    
                    FROM PFR_BASIC.INSURANCE_DOC insDoc 
                    INNER JOIN PFR_BASIC.CONTRACT contract ON insDoc.CONTRACT_ID = contract.ID
                    INNER JOIN PFR_BASIC.LEGALENTITY le ON contract.LE_ID = le.ID 
					
                    WHERE coalesce(insDoc.CLOSE_DATE, '2199-01-01') {(archive ? "<" : ">=")} '{
                        DateTime.Today.ToString(DATE_FORMAT_STRING)
                    }' and contract.TYPEID={type} and contract.STATUS <> -1";

                var query = session.CreateSQLQuery(sqlQuery)
                    .AddScalar("ID", NHibernateUtil.Int64)
                    .AddScalar("CloseDate", NHibernateUtil.Date)
                    .AddScalar("Sum", NHibernateUtil.Decimal)
                    .AddScalar("LegalEntity", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String)
                    .AddScalar("TotalInmanagement", NHibernateUtil.Decimal)
                    .SetResultTransformer(Transformers.AliasToBean<InsuranceListItem>());
                List<InsuranceListItem> result = query.List<InsuranceListItem>().ToList();

                //300 а не 300000000 потому что decimal в DB2 и .Net не дружат и nHibernate генерит запрос в результате которого decimal(19,5) 300,00000 превращается в 300000000
                result.ForEach(delegate (InsuranceListItem item)
                {
                    item.Sum = FixDecimal(item.Sum, 4) ?? 0;
                    item.TotalInmanagement = FixDecimal(item.TotalInmanagement, 4) ?? 0;
                });
                return result;
            }
        }


#endregion

#region Доходность

        public List<McProfitAbility> GetMCProfitHistoryList(long yearID, long quarterID, long contractID, long mcid)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
					FROM McProfitAbility MC 
					WHERE MC.ContractId = :cid AND
						  MC.YearId = :yid AND
						  MC.Quarter = :quarter AND 
						  MC.ID <> :mcid
				").SetInt64("cid", contractID)
                    .SetInt64("yid", yearID)
                    .SetInt64("quarter", quarterID)
                    .SetInt64("mcid", mcid);

                return query.List<McProfitAbility>().ToList();
            }
        }

        public List<McProfitAbility> GetMcProfitAbilityListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
					FROM McProfitAbility MC 
					WHERE MC.ID IN (SELECT MAX(MC2.ID) FROM McProfitAbility MC2 GROUP BY MC2.Quarter, MC2.ContractId, MC2.YearId)
				");

                return query.List<McProfitAbility>().ToList();
            }
        }

        public List<McProfitAbility> GetMcProfitAbilityListHibByContractType(int contractType)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery($@"
					FROM McProfitAbility MC 
					WHERE MC.ID IN (SELECT MAX(MC2.ID) FROM McProfitAbility MC2 GROUP BY MC2.Quarter, MC2.ContractId, MC2.YearId)
				 AND MC.Contract.TypeID={contractType}");

                return query.List<McProfitAbility>().ToList();
            }
        }

        /// <summary>
        /// Удаляет "Доходность" вместе с историйе ие изменения.
        /// </summary>
        /// <param name="yearID">Год</param>
        /// <param name="QuartalID">Квартал</param>
        /// <param name="LegalEntityID">Идентификатор контракта.</param>
        /// <returns>Количество удаленных записей.</returns>
        public int DeleteMcProfit(long yearID, long quarterID, long contractID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
					DELETE
					FROM McProfitAbility MC 
					WHERE MC.ContractId = :cid AND
						  MC.YearId = :yid AND
						  MC.Quarter = :quarter
				").SetInt64("cid", contractID)
                    .SetInt64("yid", yearID)
                    .SetInt64("quarter", quarterID);
                return query.ExecuteUpdate();
            }
        }

#endregion

#region собственные средства

        public List<OwnedFounds> GetOwnedFoundsHistoryList(long yearID, long quartalID, long legalEntityID, long ofid)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
					FROM OwnedFounds f 
					WHERE f.LegalEntityID = :lid AND
						  f.YearID = :yid AND
						  f.Quartal = :q AND 
						  f.ID <> :ofid
				").SetInt64("lid", legalEntityID)
                    .SetInt64("yid", yearID)
                    .SetInt64("q", quartalID)
                    .SetInt64("ofid", ofid);

                return query.List<OwnedFounds>().ToList();
            }
        }


        public List<OwnFundsListItem> GetOwnedFoundsListHibPrevious()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
					FROM OwnedFoundsHib f 
					WHERE f.ID IN (SELECT MAX(f2.ID) FROM OwnedFounds f2 GROUP BY f2.Quartal, f2.LegalEntityID, f2.YearID)
				");

                return query.List<OwnedFoundsHib>().ToList().ConvertAll(i =>
                {
                    return new OwnFundsListItem
                    {
                        ID = i.ID,
                        Capital = i.Capital,
                        Quartal = i.Quartal,
                        Name = i.LegalEntity == null ? string.Empty : i.LegalEntity.FormalizedName,
                        Year = i.Year == null ? string.Empty : i.Year.Name
                    };
                });
            }
        }

        public List<OwnFundsListItem> GetOwnedFoundsListHib()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var ids = session.CreateSQLQuery($@"
					select MAX(ownedfound1_.ID) from {DATA_SCHEME}.OWNEDFOUNDS ownedfound1_ group by ownedfound1_.QUARTAL , ownedfound1_.LE_ID , ownedfound1_.Y_ID
				").List();

                var crit = session.CreateCriteria(typeof(OwnedFounds).FullName, "ownf").Add(Restrictions.In("ID", ids));
                crit.CreateCriteria("LegalEntity", "l", JoinType.LeftOuterJoin)
                    .SetFetchMode("LegalEntity", FetchMode.Select);
                crit.CreateCriteria("Year", "y", JoinType.LeftOuterJoin).SetFetchMode("Year", FetchMode.Select);

                var list = crit.List<OwnedFoundsHib>();

                return list.ToList().ConvertAll(i => new OwnFundsListItem
                {
                    ID = i.ID,
                    Capital = i.Capital,
                    Quartal = i.Quartal,
                    Name = i.LegalEntity == null ? string.Empty : i.LegalEntity.FormalizedName,
                    Year = i.Year == null ? string.Empty : i.Year.Name
                });
            }
        }


        /// <summary>
        /// Удаляет "Собственные средства" вместе с историйе их изменения.
        /// </summary>
        /// <param name="yearID">Год</param>
        /// <param name="quartalID">Квартал</param>
        /// <param name="legalEntityID">Идентификатор управляющей компании.</param>
        /// <returns>Количество удаленных записей.</returns>
        public int DeleteOwnedFounds(long yearID, long quartalID, long legalEntityID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query = session.CreateQuery(@"
					DELETE
					FROM OwnedFounds f 
					WHERE f.LegalEntityID = :lid AND
						  f.YearID = :yid AND
						  f.Quartal = :q
				").SetInt64("lid", legalEntityID)
                    .SetInt64("yid", yearID)
                    .SetInt64("q", quartalID);
                return query.ExecuteUpdate();
            }
        }

        public List<DepClaimMaxListItem> GetAllocationRequestMaxSumPlacingList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria<DepClaimMaxListItem>().List<DepClaimMaxListItem>().ToList();
                return list;
            }

        }

#endregion

#region Аукционы

        public ActionResult AuctionImportDepClaim2(long auctionID, IList<DepClaim2> claims, bool forceUpdate)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        var a = session.Get<DepClaimSelectParamsHib>(auctionID);
                        if (!a.CanImportDepClaim2())
                            return
                                ActionResult.Error(
                                    "Аукцион находится в статусе, в котором не разрешено импортировать заявки");

                        if (a.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimImported && !forceUpdate)
                            return ActionResult.Error("У аукциона уже существуют заявки");

                        a.DepClaim2List.Clear();

                        claims.ForEach(c =>
                        {
                            c.AuctionDate = a.SelectDate;
                            c.Status = DepClaim2.Statuses.New;
                            a.DepClaim2List.Add(c);
                        });

                        a.AuctionStatus = DepClaimSelectParams.Statuses.DepClaimImported;
                        session.SaveOrUpdate(a);
                        transaction.Commit();
                        return ActionResult.Success();
                    }
                }
            }
            catch
            {
                return ActionResult.Error("Ошибка при импорте заявок");
            }
            //return false;
        }

        private class ClaimArgumentException : ArgumentException
        {
            public ClaimArgumentException(string message)
                : base(message)
            {
            }
        }


        /// <summary>
        /// Импорт выписки из реестра заявок, подлежащих удовлетворению
        /// </summary>
        /// <param name="auctionID"></param>
        /// <param name="claims"></param>
        /// <param name="forceUpdate"></param>
        /// <returns></returns>
        public ActionResult AuctionImportDepClaim2Confirm(long auctionID, IList<DepClaim2> claims, bool forceUpdate)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    var a = session.Get<DepClaimSelectParamsHib>(auctionID);

                    if (
                        !a.DepClaim2List.All(
                            c =>
                                c.Status == DepClaim2.Statuses.New || c.Status == DepClaim2.Statuses.Confirm ||
                                c.Status == DepClaim2.Statuses.NotConfirm))
                        return
                            ActionResult.Error(
                                "Аукцион находится в статусе, в котором не разрешено импортировать заявки");


                    //проверка на случай если мы делаем повторный импорт. 
                    var cL = a.DepClaim2List.Where(p => p.Status == DepClaim2.Statuses.Confirm);
                    //Эти записи уже импортировались ранее
                    foreach (var c in cL)
                    {
                        if (claims.All(e => e.RecNum != c.RecNum)
                            &&
                            !claims.Any(e => e.FirmID == c.FirmID && e.Rate == c.Rate && e.SettleDate == c.SettleDate))
                        // Запись импортировалась ранее, но сейчас ее нет, недопустимо
                        {
                            return
                                ActionResult.Error(
                                    "При импорте обнаружены ранее импортированные заявки, для который отсутствуют соответствующие заявки в текущей выписке");
                        }
                    }

                    using (var transaction = session.BeginTransaction())
                    {
                        a.DepClaim2List.ForEach(c => c.Status = DepClaim2.Statuses.NotConfirm);
                        if (a.StockId == (long)Stock.StockID.SPVB)
                        {
                            claims.ForEach(c => c.RecNum = -1);//Убираем сравнение по коду для СПВБ
                        }

                        foreach (var c in claims)
                        {
                            //Ищем по уникальному коду заявки
                            //Если кода в базе нету - ищем по параметрам среди заявок без номера
                            var claim = a.DepClaim2List.FirstOrDefault(e => e.RecNum == c.RecNum)
                                        ??
                                        a.DepClaim2List.FirstOrDefault(
                                            e =>
                                                e.RecNum == 0 && e.FirmID == c.FirmID && e.Rate == c.Rate &&
                                                e.SettleDate == c.SettleDate);

                            if (claim == null)
                            {
                                string s =
                                    string.Format(
                                        "Импортируемые заявки не соответствуют заявкам, существующим в базе.\nДля номера заявки {3} реестра заявок, подлежащих удовлетворению не найден соответствующий номер заявки из реестра заявок.\nПоля: Идентификатор участника (FIRMID2 = '{0}'), Процентная ставка (RATE = '{1}'), Дата размещения (SETTLEDATE = '{2:yyyy-MM-dd}').",
                                        c.FirmID, c.Rate, c.SettleDate, c.RecNum);
                                throw new ClaimArgumentException(s);
                            }

                            claim.Amount = c.Amount;
                            claim.Payment = c.Payment;
                            claim.Status = DepClaim2.Statuses.Confirm;
                            claim.Rate = c.Rate;
                            //claim.Term = c.Term;
                            claim.Part2Sum = c.Part2Sum;
                            claim.ConfirmRepositoryID = c.ConfirmRepositoryID;
                        }

                        a.AuctionStatus = DepClaimSelectParams.Statuses.DepClaimConfirmImported;
                        session.SaveOrUpdate(a);
                        transaction.Commit();

                        LogManager.Log("Completed");
                        return ActionResult.Success();
                    }

                }
            }
            catch (ClaimArgumentException ex)
            {
                return ActionResult.Error(ex.Message);
            }
            catch (Exception ex)
            {
                LogExceptionForHib(ex);
                return ActionResult.Error("Ошибка при импорте заявок");
            }
            //return false;
        }

        protected bool CanGenerateOfferts(DepClaimSelectParamsHib auction)
        {
            if (auction.AuctionStatus == DepClaimSelectParams.Statuses.DepClaimConfirmImported)
                return true;

            return auction.AuctionStatus == DepClaimSelectParams.Statuses.OfferCreated &&
                   auction.DepClaimOfferList.All(item => item.Status == DepClaimOffer.Statuses.NotSigned);
        }

        public ActionResult DepClaimOfferCreate(long auctionID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var auction = session.Get<DepClaimSelectParamsHib>(auctionID);

                    if (auction == null)
                        return ActionResult.Error("Указаного аукциона не существует");
                    if (!CanGenerateOfferts(auction))
                        //auction.AuctionStatus != DepClaimSelectParams.Statuses.DepClaimConfirmImported)
                        return
                            ActionResult.Error("Аукцион находится в статусе, для которого не доступна генерация оферт");

                    var confirmed =
                        auction.DepClaim2List.Where(dc => dc.Status == DepClaim2.Statuses.Confirm)
                            .OrderByDescending(x => x.Rate)
                            .ToList();

                    //Номер договора банковского депозита = оферты. Оферты нумеруются сквозной нумерацией с начала каждого года. Номер оферте присваивается во время генерации оферт.
                    var year = auction.SelectDate.Year;
                    if (confirmed.Count > 0)
                    {
                        var number =
                            session.CreateQuery($@"select max(Number) from DepClaimOffer where {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}Date) = :year")
                                .SetParameter("year", year)
                                .UniqueResult<long>();
                        auction.DepClaimOfferList.Clear();
                        for (int i = 0; i < confirmed.Count; i++)
                        //foreach (var dc in confirmed)
                        {
                            DepClaim2 dc = confirmed[i];
                            number++;
                            var offer = new DepClaimOfferHib
                            {
                                AuctionID = auction.ID,
                                BankID = dc.BankID,
                                DepClaimID = dc.ID,
                                Date = auction.SelectDate,
                                Status = DepClaimOffer.Statuses.NotSigned,
                                Number = number
                            };
                            auction.DepClaimOfferList.Add(offer);
                            dc.Status = DepClaim2.Statuses.ConfirmOfferCreated;
                        }
                    }

                    auction.AuctionStatus = DepClaimSelectParams.Statuses.OfferCreated;
                    session.SaveOrUpdate(auction);
                    transaction.Commit();

                    return ActionResult.Success();
                }
            }
            //return false;
        }

        public List<DepClaimOfferListItem> GetDepClaimOfferList()
        {
            var retVal = new List<DepClaimOfferListItem>();
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria(typeof(DepClaimOfferHib), "a")
                    .CreateCriteria("DepClaim", "c", JoinType.InnerJoin)
                    .SetFetchMode("DepClaim", FetchMode.Join)
                    .SetFetchMode("Auction", FetchMode.Join)
                    .SetFetchMode("Bank", FetchMode.Join)
                    .AddOrder(Order.Asc("a.ID"))
                    .List<DepClaimOfferHib>();

                retVal.AddRange(list.Select(offer =>
                    new DepClaimOfferListItem { Offer = offer, DepClaim = offer.DepClaim, Auction = offer.Auction }));
            }
            return retVal;
        }


        public DepClaimOfferListItem GetDepClaimOfferById(long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var offers = session.CreateCriteria(typeof(DepClaimOfferHib), "a")
                    .CreateCriteria("DepClaim", "c", JoinType.InnerJoin)
                    .SetFetchMode("DepClaim", FetchMode.Join)
                    .SetFetchMode("Auction", FetchMode.Join)
                    .SetFetchMode("Bank", FetchMode.Join)
                    .AddOrder(Order.Asc("a.ID"))
                    .List<DepClaimOfferHib>();

                var offer = offers.FirstOrDefault(a => a.ID == id);
                return offer == null
                    ? null
                    : new DepClaimOfferListItem { Offer = offer, DepClaim = offer.DepClaim, Auction = offer.Auction };
            }

        }

        //        public List<AuctionRepositoryLinkItem> GetLastRepositoryLinks()
        //        {
        //            List<AuctionRepositoryLinkItem> results = new List<AuctionRepositoryLinkItem>();
        //            String sql = @"SELECT rep.ID AS repID, rep.KEY AS repKey, auc.ID AS aucID FROM REPOSITORY_IMPEXP_FILE rep
        //LEFT JOIN REPOSITORY_IMPEXP_FILE newrep ON 
        //	rep.AUCTION_DATE = newrep.AUCTION_DATE AND rep.AUCTION_LOCALNUM = newrep.AUCTION_LOCALNUM AND rep.KEY = newrep.KEY
        //	AND (newrep.DDATE > rep.DDATE OR (newrep.DDATE = rep.DDATE AND newrep.TTIME > rep.TTIME))
        //INNER JOIN DEPCLAIMSELECTPARAMS auc ON rep.AUCTION_DATE = auc.SELECT_DATE AND rep.AUCTION_LOCALNUM = auc.LOCALNUM
        //WHERE newrep.ID IS NULL
        //";

        //            using (var session = DataAccessSystem.OpenSession())
        //            {
        //                IQuery query = session.CreateSQLQuery(sql);
        //                var sqlResult = query.List<object[]>();

        //                if (sqlResult.Count > 0)
        //                {
        //                    results = sqlResult.Select(item =>
        //                    {
        //                        AuctionRepositoryLinkItem link = new AuctionRepositoryLinkItem();
        //                        link.RepositoryID = (long)item[0];
        //                        link.RepositoryKey = (int)item[1];
        //                        link.AuctionID = (long)item[2];
        //                        return link;
        //                    }
        //                    ).ToList();
        //                }
        //            }
        //            return results;
        //        }

        public List<PfrBranchReport> GetPfrBranchReportsByTypeAndPeriod(int year, int? month, long typeId)
        {
            List<PfrBranchReport> retVal;
            using (var session = DataAccessSystem.OpenSession())
            {
                var x = session.CreateCriteria<PfrBranchReport>("r")
                    .Add(Restrictions.Not(Restrictions.Eq("r.StatusID", (long)-1)))
                    .Add(Restrictions.Eq("r.ReportTypeID", typeId)).Add(Restrictions.Eq("r.PeriodYear", year));

                if (month != null)
                    x = x.Add(Restrictions.Eq("r.PeriodMonth", month));

                retVal = x.List<PfrBranchReport>().ToList();
            }

            return retVal;
        }

        public List<PfrBranchReportInsuredPerson> GetPfrReportInsuredList(int year, int? month, long typeId)
        {
            List<PfrBranchReportInsuredPerson> retVal;
            using (var session = DataAccessSystem.OpenSession())
            {
                var x = session.CreateCriteria<PfrBranchReportInsuredPersonHib>("ip")
                    .CreateCriteria("Report", "r", JoinType.InnerJoin)
                    .SetFetchMode("ip.Report", FetchMode.Eager)
                    .Add(Restrictions.Not(Restrictions.Eq("r.StatusID", (long)-1)))
                    .Add(Restrictions.Eq("r.ReportTypeID", typeId)).Add(Restrictions.Eq("r.PeriodYear", year));

                if (month != null)
                    x = x.Add(Restrictions.Eq("r.PeriodMonth", month));

                retVal = x.List<PfrBranchReportInsuredPerson>().ToList();
            }

            return retVal;
        }

        public List<DepClaimOfferListItem> GetDepClaimOfferListByAuctionId(long auctionID)
        {
            var retVal = new List<DepClaimOfferListItem>();
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria<DepClaimOfferHib>()
                    .Add(Restrictions.Eq("AuctionID", auctionID))
                    .SetFetchMode("DepClaim", FetchMode.Join)
                    .SetFetchMode("Auction", FetchMode.Join)
                    .SetFetchMode("Bank", FetchMode.Join)
                    .List<DepClaimOfferHib>();

                retVal.AddRange(list.Select(offer =>
                    new DepClaimOfferListItem { Offer = offer, DepClaim = offer.DepClaim, Auction = offer.Auction }));
            }
            return retVal;
        }

        public List<DepClaimOfferListItem> GetDepClaimOfferListFiltered(long auctionID,
            ICollection<DepClaimOffer.Statuses> statuses)
        {
            var retVal = new List<DepClaimOfferListItem>();
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria<DepClaimOfferHib>()
                    .Add(Restrictions.Eq("AuctionID", auctionID))
                    .Add(Restrictions.InG("StatusID", statuses))
                    .SetFetchMode("DepClaim", FetchMode.Join)
                    .SetFetchMode("Auction", FetchMode.Join)
                    .SetFetchMode("Bank", FetchMode.Join)
                    .List<DepClaimOfferHib>();

                retVal.AddRange(list.Select(offer =>
                    new DepClaimOfferListItem { Offer = offer, DepClaim = offer.DepClaim, Auction = offer.Auction }));
            }
            return retVal;
        }

        public List<DepClaimOfferListItem> GetDepClaimOfferListBySettleDate(
            ICollection<DepClaimOffer.Statuses> statuses, DateTime date)
        {
            var retVal = new List<DepClaimOfferListItem>();
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateCriteria<DepClaimOfferHib>("offer")
                    .CreateCriteria("offer.DepClaim", "dep", JoinType.InnerJoin)
                    .SetFetchMode("offer.DepClaim", FetchMode.Join)
                    .CreateCriteria("offer.Auction", "auc", JoinType.InnerJoin)
                    .SetFetchMode("offer.Auction", FetchMode.Join)
                    .CreateCriteria("offer.Bank", "b", JoinType.InnerJoin)
                    .SetFetchMode("Bank", FetchMode.Join)
                    .Add(Restrictions.Eq("dep.SettleDate", date))
                    .Add(Restrictions.In("offer.StatusID", statuses.ToArray()))
                    .Add(Restrictions.Eq("offer.DepositsGenerated", (byte)0))
                    .List<DepClaimOfferHib>();

                retVal.AddRange(list.Select(offer =>
                    new DepClaimOfferListItem
                    {
                        Offer = offer,
                        DepClaim = offer.DepClaim,
                        Auction = (DepClaimSelectParams)offer.Auction
                    }));
            }
            return retVal;
        }

        public List<long> GetFailedDepClaimOffersAucIds()
        {
            return SessionWrapper(session =>
            {
                return session.CreateCriteria<DepClaimOfferHib>("offer")
                    .Add(Restrictions.In("offer.StatusID", new[] {(int) DepClaimOffer.Statuses.Signed, (int) DepClaimOffer.Statuses.NotSigned}))
                    .Add(Restrictions.Eq("offer.DepositsGenerated", (byte) 0))
                    .List<DepClaimOfferHib>()
                    .Select(offer => offer.AuctionID)
                    .Distinct()
                    .ToList();
            });
        }

        public List<DepClaimOfferListItem> GetAcceptedDepClaimOffers()
        {
            return SessionWrapper(session =>
            {
                return session.CreateQuery(@"from DepClaimOfferHib o 
                                             join fetch o.DepClaim dep
                                             join fetch o.Auction a
                                             join fetch o.Bank b
                                             where o.StatusID=:status and o.DepositsGenerated=0")
                    .SetParameter("status", (int) DepClaimOffer.Statuses.Accepted)
                    .List<DepClaimOfferHib>()
                    .Select(offer =>
                        new DepClaimOfferListItem
                        {
                            Offer = offer,
                            DepClaim = offer.DepClaim,
                            Auction = (DepClaimSelectParams) offer.Auction
                        })
                    .ToList();
            });
        }


        public ActionResult CheckLicenseUnique(long bankID, string licenseNumber, string caType = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (licenseNumber == null)
                    return ActionResult.Success();

                caType = caType ?? session.CreateQuery(@"
                    SELECT ca.TypeName
                    FROM LegalEntity le
                    JOIN le.Contragent ca
                    WHERE le.ID = :Id
                ").SetParameter("Id", bankID)
                    .UniqueResult<string>();

                //Проверка номера до '/'
                licenseNumber = licenseNumber.Split('/').First();


                var count = session.CreateQuery(@"
					SELECT le.FormalizedName
					FROM License l
					JOIN l.LegalEntity le
					JOIN le.Contragent ca
					WHERE " +
                                                (caType != null ? "ca.TypeName = :type AND " : string.Empty) +
                                                @"le.ID <> :id AND 
                    l.Number = :num
				")
                    .SetParameter("type", caType)
                    .SetParameter("id", bankID)
                    .SetParameter("num", licenseNumber)
                    .List<string>().Count;

                if (count > 0)
                    return ActionResult.Error("Лицензия с таким номером уже существует");

                //Проверка номера для номеров с '/' базе              


                var mask = licenseNumber + "/%";
                count = session.CreateQuery(@"
					SELECT le.FormalizedName
					FROM License l
					JOIN l.LegalEntity le
					JOIN le.Contragent ca
					WHERE " +
                                            (caType != null ? "ca.TypeName = :type AND " : string.Empty) +
                                            @"le.ID <> :id AND 
                    l.Number like :num
				")
                    .SetParameter("type", caType)
                    .SetParameter("id", bankID)
                    .SetParameter("num", mask)
                    .List<string>().Count;

                if (count > 0)
                    return ActionResult.Error("Лицензия с таким номером уже существует");

            }

            return ActionResult.Success();
        }

        public bool ImportKO761(long importId, DateTime importDate, IList<BankConclusion> conclusions, bool forceUpdate, long elId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    //delete from pfr_basic.KOSTATUSIMPORT r join pfr_basic.koimport i on i.ID = r.IMPORT_ID where i.DATE=:mydate and i.EL_ID=:elid
                    session.CreateQuery("delete from BankConclusion r where r.ImportID in (select i.ID from BanksConclusion i where i.ImportDate=:mydate and i.ElementID=:elid)")
                        .SetParameter("mydate", importDate.Date)
                        .SetParameter("elid", elId)
                        .ExecuteUpdate();
                    session.CreateQuery("delete from BanksConclusion i where i.ImportDate=:mydate and i.ElementID=:elid")
                        .SetParameter("mydate", importDate.Date)
                        .SetParameter("elid", elId)
                        .ExecuteUpdate();

                    var importKo = new BanksConclusionHib
                                   {
                                       ElementID = elId,
                                       ImportDate = importDate
                                   };

                    session.SaveOrUpdate(importKo);

                    if (importKo.BanksList == null)
                        importKo.BanksList = new List<BankConclusion>();

                    importKo.BanksList.Clear();

                    conclusions.ForEach(c =>
                    {
                        c.ImportID = importKo.ID;
                        importKo.BanksList.Add(c);
                    });

                    session.SaveOrUpdate(importKo);
                    transaction.Commit();

                    return true;
                }
            }

        }

#endregion Аукционы

        

#region NPF

        public ActionResult SaveNPFContacts(long npfID, IList<LegalEntityCourier> contacts)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        var npf = session.Get<LegalEntityHib>(npfID);
                        if (npf == null)
                            return ActionResult.Error("Указаный НПФ не существует");

                        //Mark all as deleted
                        npf.Couriers.Where(c => c.Type == LegalEntityCourier.Types.Contact).ToList()
                            .ForEach(c => c.StatusID = -1);

                        foreach (var contact in contacts)
                        {
                            contact.LegalEntityID = npf.ID;
                            contact.Type = LegalEntityCourier.Types.Contact;

                            var old = npf.Couriers.FirstOrDefault(c => c.ID == contact.ID);
                            if (old != null)
                                session.Save(contact);
                            else
                                npf.Couriers.Add(contact);
                        }

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                return ActionResult.Error(ex.Message);
            }
            return ActionResult.Success();
        }

        public IList<LegalEntityIdentifier> GetExistingNPFIdentifiers()
        {
            return GetExistingIdentifiers();
        }

        public IList<LegalEntityIdentifier> GetNPFIdentifiers(long npfID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = $@"SELECT lei
								FROM LegalEntityHib le 
								JOIN le.Contragent c
                                JOIN le.Identifiers lei
								WHERE LOWER(le.Contragent.TypeName) = 'нпф' and le.ID = {npfID} 
                                ORDER BY lei.SetDate DESC";

                var list = session.CreateQuery(query)
                    .List<LegalEntityIdentifier>();
                return list;
            }

        }

        public IList<LegalEntityIdentifier> GetExistingUKIdentifiers()
        {
            return GetExistingIdentifiers();
        }

        public IList<LegalEntityIdentifier> GetExistingIdentifiers()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = @"SELECT lei
								FROM LegalEntityHib le 
								JOIN le.Contragent c
                                JOIN le.Identifiers lei
								WHERE LOWER(le.Contragent.TypeName) = 'нпф' or LOWER(le.Contragent.TypeName) = 'ук' or LOWER(le.Contragent.TypeName) = 'гук' 
								ORDER BY lei.SetDate DESC";

                var list = session.CreateQuery(query)
                    .List<LegalEntityIdentifier>();
                list.Where(x => x.AdditionInfo == Guid.Empty).ForEach(x => x.AdditionInfo = Guid.NewGuid());
                return list;
            }
        }

        public List<LegalEntityIdentifier> GetUKIdentifiers(long ukID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                string query = $@"SELECT lei
								FROM LegalEntityHib le 
								JOIN le.Contragent c
                                JOIN le.Identifiers lei
								WHERE (LOWER(le.Contragent.TypeName) = 'ук' or LOWER(le.Contragent.TypeName) = 'гук') and le.ID = {ukID} and lei.StatusID != -1 ORDER BY lei.SetDate DESC";

                var list = session.CreateQuery(query)
                    .List<LegalEntityIdentifier>().ToList();
                list.Where(x => x.AdditionInfo == Guid.Empty).ForEach(x => x.AdditionInfo = Guid.NewGuid());
                return list;
            }
        }

        public ActionResult SaveNPFIdentifiers(long npfID, IList<LegalEntityIdentifier> identifiers)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        var npf = session.Get<LegalEntityHib>(npfID);
                        if (npf == null)
                            return ActionResult.Error("Указаный НПФ не существует");

                        foreach (var identifier in identifiers)
                        {
                            identifier.LegalEntityID = npf.ID;

                            session.Save(identifier);
                        }

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                return ActionResult.Error(ex.Message);
            }
            return ActionResult.Success();
        }

#endregion


        public PfrBranchReportImportResponse ImportPfrBranchReport(PfrBranchReportImportRequest parameters)
        {
            var res = new PfrBranchReportImportResponse();

            try
            {

                using (var session = DataAccessSystem.OpenSession())
                {
                    var xL = parameters.Items.ToList();

                    List<PfrBranchReportImportContentBase> dL = null;

                    using (var transaction = session.BeginTransaction())
                    {

                        if (parameters.IsForceUpdateDuplicate)
                        {
                            dL = new List<PfrBranchReportImportContentBase>();

                            ImportContentExtensionServer.DeleteDuplicate(parameters.Items, session);
                        }
                        else
                        {

                            //detect duplicate imports
                            dL = ImportContentExtensionServer.GetDuplicate(parameters.Items, session);

                            //Remove duplicates from import
                            foreach (var d in dL)
                                xL.Remove(d);
                        }


                        //todo: validate data

                        foreach (var x in xL)
                            ImportContentExtensionServer.Save(x, session);

                        try
                        {
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            LogExceptionForHib(ex);
                            throw;
                        }
                    }

                    if (dL.Count == 0)
                    {
                        res.Status = PfrBranchReportImportResponse.enStatus.OK;
                        res.ErrorMessage = "OK";
                    }
                    else
                    {
                        res.Status = PfrBranchReportImportResponse.enStatus.Duplicate;
                        res.ErrorMessage = "Обнаружены уже импортированные записи";
                        res.DuplicateItemKeys = (from x in dL select x.ReportKey).ToArray();
                    }
                }

            }
            catch (Exception ex)
            {
                res.Status = PfrBranchReportImportResponse.enStatus.Error;
                res.ErrorMessage = ex.Message;
                res.StackTrace = ex.StackTrace;
            }

            return res;
        }


        public List<Element> GetElementByType(Element.Types type)
        {
            return GetListByProperty(typeof(Element).FullName, "Key", (long)type).Cast<Element>()
                .OrderBy(e => e.Order)
                .ThenBy(e => e.Name).ToList();
        }

        public bool IsOfferNumberUsed(int auctionYear, long offerId, long offerNum)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //var query = session.QueryOver<DepClaimOffer>().Where(Restrictions.Eq("AuctionID", auctionId)).Where(Restrictions.Eq("Number", offerId));
                //return query.RowCount() > 0;
                var count =
                    session.CreateQuery(
                        $"select count(Number) from DepClaimOffer where {(IsDB2 ? "YEAR(" : "EXTRACT( YEAR FROM ")}Date) = :year and Number = :number and ID<>:id")
                        .SetParameter("year", auctionYear).SetParameter("number", offerNum)
                        .SetParameter("id", offerId).UniqueResult<long>();

                return count > 0;
            }
        }


        public void UpdateAuctionStatus(long auctionId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    if (UpdateAuctionStatusInternal(session, auctionId))
                        transaction.Commit();
                }
            }
        }

        private bool UpdateAuctionStatusInternal(ISession session, long auctionId)
        {
            var a = session.Get<DepClaimSelectParamsHib>(auctionId);
            if (a == null) return false;
            if (a.AuctionStatus == DepClaimSelectParams.Statuses.OfferCreated &&
                a.DepClaimOfferList.All(o => o.Status == DepClaimOffer.Statuses.Signed))
                a.SetAuctionStatus(session, DepClaimSelectParams.Statuses.OfferSigned);
            //a.AuctionStatus = DepClaimSelectParams.Statuses.OfferSigned;

            if ((a.AuctionStatus == DepClaimSelectParams.Statuses.OfferSigned ||
                 a.AuctionStatus == DepClaimSelectParams.Statuses.OfferCreated) &&
                a.DepClaimOfferList.All(
                    o => o.Status == DepClaimOffer.Statuses.Accepted || o.Status == DepClaimOffer.Statuses.NotAccepted))
                //a.AuctionStatus = DepClaimSelectParams.Statuses.OfferAcceptedOrNo;
                a.SetAuctionStatus(session, DepClaimSelectParams.Statuses.OfferAcceptedOrNo);

            if (a.AuctionStatus == DepClaimSelectParams.Statuses.OfferAcceptedOrNo &&
                a.DepClaimOfferList.All(o => o.IsDepositsGenerated || o.Status == DepClaimOffer.Statuses.NotAccepted))
                //a.AuctionStatus = DepClaimSelectParams.Statuses.DepositSettled;
                a.SetAuctionStatus(session, DepClaimSelectParams.Statuses.DepositSettled);

            return true;
        }

        public int GetDepClaim2Count(long auctionID, bool withConfirmationOnly)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.QueryOver<DepClaim2>().Where(Restrictions.Eq("AuctionID", auctionID));
                if (withConfirmationOnly)
                {
                    var confirmationStatusIds = new[]
                    {
                        (int) DepClaim2.Statuses.Confirm, (int) DepClaim2.Statuses.ConfirmNotAccepted,
                        (int) DepClaim2.Statuses.ConfirmOfferCreated
                    };
                    query.Where(Restrictions.In("StatusID", confirmationStatusIds));
                }
                return query.RowCount();
            }
        }


       /* public IList<DepositReturnListItem> GetDepositReturnListForQuarter(long pid, int year, int quarter)
        {
            var res = new List<DepositReturnListItem>();
            var fromMonth = DateTools.GetFirstMonthOfQuarterForBO(quarter);
            var toMonth = DateTools.GetLastMonthOfQuarterForBO(quarter);
            var toYear = toMonth == 1 ? (year + 1) : year;

            var from = new DateTime(year, fromMonth, 1);
            var to = new DateTime(toYear, toMonth, 1);

            using (var session = DataAccessSystem.OpenSession())
            {
                var iL = new List<long>();

                {
                    var query = session.CreateQuery($@"
                        select 
                        d.ID, 
                        o.Number,
                        d.Status,
                        c.Name,

                        b.FormalizedName,
                        dc2.SettleDate, 
                        dc2.ReturnDate,

                        p.Year,
                        p.Type,
                        ba.AccountNumber,

                        dc2.Amount,
                        dc2.Payment,


                        s.ID,
                        s.Sum,
                        d.DepClaimOfferId,
                        p.ID,
                        s.PlannedSum,
                        auc.AuctionNum



                        from d in Deposit
                        join d.DepClaimOffer o
                        join o.DepClaim dc2
                        join dc2.Auction auc
                        left join d.Comment c
                        join o.OfferPfrBankAccountSum s
                        join s.Portfolio p
                        join s.PfrBankAccount ba
                        join ba.LegalEntity b
                        where d.Status in(1,3)
                        and dc2.ReturnDate >= :from and dc2.ReturnDate <= :to
                        and p.ID=:pid 
                        and s.StatusID>0
                        ");
                    query.SetParameter("from", from);
                    query.SetParameter("to", to);
                    query.SetParameter("pid", pid);

                    foreach (var p in query.List<object[]>())
                    {
                        {
                            var x = new DepositReturnListItem()
                            {
                                IsSelected = true,
                                IsPercent = false,

                                DepositID = (long)p[0],
                                OfferNumber = (long)p[1],
                                Status = (long)p[2],
                                Comment = (string)p[3],

                                BankName = (string)p[4],
                                SettleDate = (DateTime)p[5],
                                ReturnDate = (DateTime)p[6],

                                PortfolioName = (string)p[7],
                                PortfolioType = (string)p[8],
                                AccountNum = (string)p[9],

                                AmountTotal = (decimal)p[10],
                                AmountPercent = (decimal)p[11],
                                SumID = (long)p[12],
                                AmountSum = (decimal)p[13],
                                AmountPercPlanned = (decimal?)p[16],
                                //AmountTotalPlanned = (decimal?)p[14],
                                PaymentDate = (DateTime)p[6],
                                AmountPaid = 0,
                                AuctionNum = (int)p[17]
                            };
                            res.Add(x);

                            //if (x.Status == 3)//Если возвращены только проценты, то статус 1
                            iL.Add(x.SumID);
                        }
                        {
                            var x = new DepositReturnListItem()
                            {
                                IsSelected = true,
                                IsPercent = true,

                                DepositID = (long)p[0],
                                OfferNumber = (long)p[1],
                                Status = (long)p[2],
                                Comment = (string)p[3],

                                BankName = (string)p[4],
                                SettleDate = (DateTime)p[5],
                                ReturnDate = (DateTime)p[6],

                                PortfolioName = (string)p[7],
                                PortfolioType = (string)p[8],
                                AccountNum = (string)p[9],

                                AmountTotal = (decimal)p[10],
                                AmountPercent = (decimal)p[11],
                                SumID = (long)p[12],
                                AmountSum = (decimal)p[13],
                                AmountPercPlanned = (decimal?)p[16],
                                PaymentDate = (DateTime)p[6],
                                AmountPaid = 0,
                                AuctionNum = (int)p[17]
                            };
                            res.Add(x);
                        }

                    }
                }

                // получаем сумму уже произведенных выплат
                if (iL.Count != 0)
                {
                    var query = session.CreateQuery(@"
                        select ph
                        from ph in PaymentHistory
                        where ph.OfferPfrBankAccountSumId in(:idL)
                        ");
                    query.SetParameterList("idL", iL);

                    var pL = query.List<PaymentHistory>();

                    foreach (var p in pL)
                    {
                        //Find corresponding record
                        var x = res.Single(y => y.SumID == p.OfferPfrBankAccountSumId && y.IsPercent == p.IsForPercent);

                        x.AmountPaid += p.Sum;
                    }
                }
            }


            //группируем проценты по депозитам (AmountSumPercent)
            var gL = from x in res
                     where x.IsPercent
                     //We need it only for Percent records
                     group x by x.DepositID
                         into g
                     select new { Items = g };
            //распределяем оставшуюся сумму процентов - по портфелям в процентном соотношении
            foreach (var g in gL)
            {
                DistributePercent(g.Items);
            }

            res.ForEach(d => d.SetAmmount());
            foreach (var x in res)
            {
                //устанавливаем фактическую сумму равной плановой (для отображения в возврате депозитов в частности)
                x.AmountActual = x.Amount;
                //устанавливаем плановую сумму процентов по умолчанию, если она отсутствует
                if (!x.AmountPercPlanned.HasValue && x.IsPercent)
                    x.AmountPercPlanned = x.Amount;
            }


            return res;
        }*/

        public IList<DepositReturnListItem> GetDepositReturnList(DateTime? date)
        {
            var res = new List<DepositReturnListItem>();

            using (var session = DataAccessSystem.OpenSession())
            {
                var iL = new List<long>();

                {
                    var query = session.CreateQuery($@"
                        select 
                        d.ID, 
                        o.Number,
                        d.Status,
                        c.Name,

                        b.FormalizedName,
                        dc2.SettleDate, 
                        dc2.ReturnDate,

                        p.Year,
                        p.Type,
                        ba.AccountNumber,

                        dc2.Amount,
                        dc2.Payment,


                        s.ID,
                        s.Sum,
                        d.DepClaimOfferId,
                        p.ID,
                        s.PlannedSum,
                        auc.AuctionNum



                        from d in Deposit
                        join d.DepClaimOffer o
                        join o.DepClaim dc2
                        join dc2.Auction auc
                        left join d.Comment c
                        join o.OfferPfrBankAccountSum s
                        join s.Portfolio p
                        join s.PfrBankAccount ba
                        join ba.LegalEntity b
                        where d.Status in(1,3)
                        {(date.HasValue ? "and dc2.ReturnDate = :date" : "")}
                        and s.StatusID>0
                        ");
                    if(date.HasValue)
                        query.SetParameter("date", date);

                    foreach (var p in query.List<object[]>())
                    {
                        {
                            var x = new DepositReturnListItem
                            {
                                IsSelected = true,
                                IsPercent = false,

                                DepositID = (long)p[0],
                                OfferNumber = (long)p[1],
                                Status = (long)p[2],
                                Comment = (string)p[3],

                                BankName = (string)p[4],
                                SettleDate = (DateTime)p[5],
                                ReturnDate = (DateTime)p[6],

                                PortfolioName = (string)p[7],
                                PortfolioType = (string)p[8],
                                AccountNum = (string)p[9],

                                AmountTotal = (decimal)p[10],
                                AmountPercent = (decimal)p[11],
                                SumID = (long)p[12],
                                AmountSum = (decimal)p[13],
                                AmountPercPlanned = (decimal?)p[16],
                                //AmountTotalPlanned = (decimal?)p[14],
                                PaymentDate = (DateTime)p[6],
                                AmountPaid = 0,
                                AuctionNum = (int)p[17]
                            };
                            res.Add(x);

                            //if (x.Status == 3)//Если возвращены только проценты, то статус 1
                            iL.Add(x.SumID);
                        }
                        {
                            var x = new DepositReturnListItem
                            {
                                IsSelected = true,
                                IsPercent = true,

                                DepositID = (long)p[0],
                                OfferNumber = (long)p[1],
                                Status = (long)p[2],
                                Comment = (string)p[3],

                                BankName = (string)p[4],
                                SettleDate = (DateTime)p[5],
                                ReturnDate = (DateTime)p[6],

                                PortfolioName = (string)p[7],
                                PortfolioType = (string)p[8],
                                AccountNum = (string)p[9],

                                AmountTotal = (decimal)p[10],
                                AmountPercent = (decimal)p[11],
                                SumID = (long)p[12],
                                AmountSum = (decimal)p[13],
                                AmountPercPlanned = (decimal?)p[16],
                                PaymentDate = (DateTime)p[6],
                                AmountPaid = 0,
                                AuctionNum = (int)p[17]
                            };
                            res.Add(x);
                        }

                    }
                }

                // получаем сумму уже произведенных выплат
                if (iL.Count != 0)
                {
                    var query = session.CreateQuery(@"
                        select ph
                        from ph in PaymentHistory
                        where ph.OfferPfrBankAccountSumId in(:idL)
                        ");
                    query.SetParameterList("idL", iL);

                    var pL = query.List<PaymentHistory>();

                    foreach (var p in pL)
                    {
                        //Find corresponding record
                        var x = res.Single(y => y.SumID == p.OfferPfrBankAccountSumId && y.IsPercent == p.IsForPercent);

                        x.AmountPaid += p.Sum;
                    }
                }
            }


            //группируем проценты по депозитам (AmountSumPercent)
            var gL = from x in res
                     where x.IsPercent
                     //We need it only for Percent records
                     group x by x.DepositID
                         into g
                     select new { Items = g };
            //распределяем оставшуюся сумму процентов - по портфелям в процентном соотношении
            foreach (var g in gL)
            {
                DistributePercent(g.Items);
            }

            res.ForEach(d => d.SetAmmount());
            foreach (var x in res)
            {
                //устанавливаем фактическую сумму равной плановой (для отображения в возврате депозитов в частности)
                x.AmountActual = x.Amount;
                //устанавливаем плановую сумму процентов по умолчанию, если она отсутствует
                if (!x.AmountPercPlanned.HasValue && x.IsPercent)
                    x.AmountPercPlanned = x.Amount;
            }


            return res;
        }

        /// <summary>
        /// У нас сумма процентов по каждой отдельной сумме не хранится, вычисляем.
        /// </summary>
        /// <param name="xL"></param>
        private void DistributePercent(IGrouping<long, DepositReturnListItem> xL)
        {
            if (!xL.Any())
                return;
            decimal? totalPlannedSum = xL.Sum(a => a.AmountPercPlanned ?? 0);
            totalPlannedSum = totalPlannedSum == 0 ? null : totalPlannedSum;
            var f = xL.First();

            decimal sum = totalPlannedSum ?? f.AmountPercent; //All sum of percents
            decimal sumPaid = xL.Sum(p => p.AmountPaid);

            //Если нет процентов для выплат - выставояем всем 0
            if (sum == 0 || sum == sumPaid)
            {
                foreach (var x in xL)
                {
                    x.SetAmountSumPercent(0);
                }
                return;
            }

            decimal changeSum = 0;

            sum = sum - sumPaid;

            foreach (var x in xL)
            {
                decimal k = x.AmountSum / x.AmountTotal; //сумма разбики по портф / единую сумму 

                decimal n = Math.Round(k * sum, 2); //Распределяем невыплаченые остатки
                changeSum += x.SetAmountSumPercent(n);

                sum -= n;
            }
            sum += changeSum;

            // Distributed too much or not distributed some money, set payment to first possible account
            if (sum != 0)
            {
                foreach (var x in xL)
                {
                    decimal n = sum + x.AmountSumPercent; //<0
                    sum = x.SetAmountSumPercent(n);
                    if (sum == 0)
                        continue;
                }

                //if (sum != 0) //Something is wrong in calc. Just for any case
                //	throw new InvalidOperationException("Ошибка распределения суммы по портфелям.");
            }


        }

        public bool DepositReturn(List<DepositReturnListItem> xL)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        //обновляем показатели плановых сумм
                        foreach (var a in xL.Where(a => a.IsSelected && a.IsPercent && a.SumID != 0))
                        {
                            session.CreateQuery("update OfferPfrBankAccountSum s set s.PlannedSum=:sum where s.ID=:id and s.StatusID<>-1")
                                .SetParameter("sum", a.AmountPercPlanned)
                                .SetParameter("id", a.SumID)
                                .ExecuteUpdate();
                        }

                        //сохраняем историю платежей для всех выбранных записей с ненулевой суммой
                        foreach (var x in xL.Where(p => p.IsSelected && p.AmountActual > 0))
                        {
                            var p = new PaymentHistory
                            {
                                Sum = x.AmountActual,
                                Date = DateTime.Now, // Дата ДИ
                                OrderDate = x.PaymentDate,
                                IsForPercent = x.IsPercent,
                                OfferPfrBankAccountSumId = x.SumID
                            };
                            session.SaveOrUpdate(p);
                        }

                        //Всем записям обновляем статусы депозитов
                        foreach (var depositID in xL.Where(p => p.IsSelected).Select(p => p.DepositID).Distinct())
                        {
                            UpdateDepositStatus(session, depositID);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        LogExceptionForHib(ex);
                        throw;
                    }
                }
            }
            return true;
        }


        private void UpdateDepositStatus(ISession session, long depositID, decimal? planSum = null)
        {
            var dep = session.Get<DepositHib>(depositID);

            var dc = dep.DepClaimOffer.DepClaim;

            var payments = dep.DepClaimOffer.OfferPfrBankAccountSum.SelectMany(s => s.PaymentHistory).ToList();
            //выбираем плановую сумму процентов (должна быть уже 100% указана на данном этапе) путем сложения сумм по портфелям
            var plannedPercSum = dep.DepClaimOffer.OfferPfrBankAccountSum.Select(s => s.PlannedSum ?? 0).Sum();

            var payBody = payments.Where(p => p.IsForPercent == false).Sum(p => p.Sum);
            var shouldPay = payments.Any(p => !p.IsForPercent);
            var payPercent = payments.Where(p => p.IsForPercent).Sum(p => p.Sum);
            var shouldPayPerc = payments.Any(p => p.IsForPercent);// || (dep.DepClaimOffer.OfferPfrBankAccountSum.Any() && plannedPercSum == 0);

            bool isBodyPaid = false;
            bool isPercentPaid = false;

            bool isBodyPaidPartial = false;
            bool isPercentPaidPartial = false;

            if (shouldPay)
            {
                isBodyPaid = dc.Amount == payBody;
                isBodyPaidPartial = dc.Amount > payBody;
            }
            if (shouldPayPerc)
            {
                //учитываем плановую сумму по процентам
                //http://jira.vs.it.ru/browse/DOKIPIV-890
                isPercentPaid = plannedPercSum == payPercent;
                isPercentPaidPartial = plannedPercSum > payPercent;
                //isPercentPaid = dc.Payment == payPercent;
                //isPercentPaidPartial = dc.Payment > payPercent;
            }

            long? oldStatus = dep.Status;
            long? oldComment = dep.CommentID;

            if (isBodyPaid)
            {
                if (isPercentPaid) //Returned all
                {
                    //PAYMENT_HISTORY.DATE операции, после которой депозиту был присвоен статус "Возвращен
                    dep.ReturnDateActual = GetMaxPaymentDate(depositID, session);
                    dep.Status = (long)Deposit.Statuses.Returned;
                    dep.CommentID = GetElementID(30, null); // null;
                }
                else if (isPercentPaidPartial)
                {
                    dep.Status = (long)Deposit.Statuses.PartiallyReturned;
                    dep.CommentID = GetElementID(30, 4); // 25;
                }
                else
                {
                    dep.Status = (long)Deposit.Statuses.PartiallyReturned;
                    dep.CommentID = GetElementID(30, 5); //26;
                }
            }
            else if (isBodyPaidPartial)
            {
                dep.Status = (long)Deposit.Statuses.PartiallyReturned;

                if (isPercentPaid)
                {
                    dep.CommentID = GetElementID(30, 1); //22;
                }
                else if (isPercentPaidPartial)
                {
                    dep.CommentID = GetElementID(30, 2); //23;
                }
                else
                {
                    dep.CommentID = GetElementID(30, 3); //24;
                }
            }
            else //Проплат по телу депозита не было
            {
                if (isPercentPaid)
                {
                    dep.Status = (long)Deposit.Statuses.PartiallyReturned;
                    dep.CommentID = GetElementID(30, 7);
                }
                else if (isPercentPaidPartial)
                {
                    dep.Status = (long)Deposit.Statuses.PartiallyReturned;
                    dep.CommentID = GetElementID(30, 6);
                }
                else
                {
                    //Никаких проплат не было
                    dep.CommentID = null;
                    dep.Status = (long)Deposit.Statuses.Placed;
                }
            }

            if (dep.Status != oldStatus || dep.CommentID != oldComment)
                session.SaveOrUpdate(dep);
        }





        public DateTime? GetMaxPaymentDate(long depositID, ISession session)
        {
            var query = session.CreateQuery(@"
select max(ph.OrderDate)
from
Deposit d
join d.DepClaimOffer o
join o.OfferPfrBankAccountSum s
join s.PaymentHistory ph
where d.ID = :id
and s.StatusID>0
").SetParameter("id", depositID);

            return query.UniqueResult<DateTime?>();
        }

        private DateTime? Max(DateTime a, DateTime b)
        {
            if (a > b) return a;
            return b;
        }

        private long? GetElementID(long key, int? order)
        {
            if (order == null)
                return null;

            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery("FROM Element e WHERE e.Key =:key and e.Order = :order")
                    .SetParameter("key", key)
                    .SetParameter("order", order.Value)
                    .List<Element>();

                if (list.Count == 0)
                    return null;

                return list[0].ID;
            }
        }

        public EdoOdkF140 GetEdoOdkF140ByReportData(long contractId, long categoryId, DateTime? theDate)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = session.CreateCriteria(typeof(EdoOdkF140), "e")
                    .Add(Restrictions.Eq("ContractID", contractId))
                    .Add(Restrictions.Eq("CategoryId", categoryId));
                if (theDate.HasValue)
                    crit.Add(Restrictions.Eq("TheDate", theDate));
                return crit.List<EdoOdkF140>().FirstOrDefault();
            }
        }

        private class PlanItem
        {
            public readonly SITransfer Plan;
            public List<UKPaymentListItem> UkPaymentList;
            public IList<ReqTransfer> ReqTransferList;

            public PlanItem(SITransfer plan)
            {
                Plan = plan;
            }
        }

        /// <summary>
        /// Удаление записи годового плана
        /// </summary>
        /// <param name="registerId">Id реестра</param>
        public bool DeleteYearPlan(long registerId, bool undelete)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var status = undelete ? 1L : -1L;

                    var propList = new List<ListPropertyCondition>
                    {
                        ListPropertyCondition.Equal("TransferRegisterID", registerId),
                        ListPropertyCondition.NotEqual("StatusID", status)
                    };
                    var planList = GetListByPropertyConditions<SITransfer>(propList, session)
                        .Cast<SITransfer>()
                        .Select(p => new PlanItem(p))
                        .ToList();

                    foreach (var plan in planList)
                    {
                        plan.UkPaymentList = GetUKPaymentsForPlan(plan.Plan.ID, session);
                        var xL = new List<ListPropertyCondition>
                        {
                            ListPropertyCondition.Equal("TransferListID", plan.Plan.ID),
                            ListPropertyCondition.NotEqual("StatusID", status)
                        };
                        plan.ReqTransferList = GetListByPropertyConditions<ReqTransfer>(xL, session)
                            .Cast<ReqTransfer>()
                            .ToList();
                    }

                    //ПРОВЕРКА
                    if (!undelete && planList.Any()) //Плана нет, можно удалять
                    {
                        foreach (var p in planList)
                        {
                            if (p.ReqTransferList != null && p.ReqTransferList.Count > 0)
                                //Есть перечисление, не удаляем
                                return false;

                            if (p.UkPaymentList == null) continue;
                            if (p.UkPaymentList.Any(u => u.ReqTransferID != 0 || u.ReqTransfer2ID != 0))
                            {
                                return false;
                            }
                        }
                    }

                    //УДАЛЕНИЕ
                    foreach (var p in planList)
                    {
                        if (p == null) continue;

                        if (p.UkPaymentList != null)
                        {
                            if(undelete)
                                RestoreEntities<SIUKPlan>(p.UkPaymentList.Select(a => a.ID), session);
                            else DeleteEntities<SIUKPlan>(p.UkPaymentList.Select(a => a.ID), session);
                        }

                        if (p.ReqTransferList != null)
                        {
                            if(undelete)
                                RestoreEntities<ReqTransfer>(p.ReqTransferList.Select(a => a.ID), session);
                            else DeleteEntities<ReqTransfer>(p.ReqTransferList.Select(a => a.ID), session);
                        }

                        if(undelete)
                            RestoreEntity<SITransfer>(p.Plan.ID, session);
                        else DeleteEntity<SITransfer>(p.Plan.ID, session);
                    }

                    if(undelete)
                        RestoreEntity<SIRegister>(registerId, session);
                    else DeleteEntity<SIRegister>(registerId, session);

                    transaction.Commit();
                    return true;
                }
            }
        }


        /// <summary>
        /// Сохранения результата мастера перечислений (годовой план, перечисления и т.д.)
        /// </summary>
        /// <param name="register">Реестр</param>
        /// <param name="listSITransfers">Списки</param>
        /// <param name="listReqTransfers">Перечисления</param>
        /// <param name="isFinVypl">Тип перечисления: финансовые выплаты или выплаты правопреемникам</param>
        /// <param name="yearId">Год</param>
        /// <param name="monthId">Месяц</param>
        /// <param name="createSITransfers">Создание перечислений</param>
        public long SaveTransferWizardResult(SIRegister register, List<SITransfer> listSITransfers,
            List<ReqTransfer> listReqTransfers, bool isFinVypl, long yearId, long monthId, bool createSITransfers)
        {
            try
            {
                using (var session = DataAccessSystem.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {

                        register.StatusID = 1;
                        session.SaveOrUpdate(register);
                        //register.ID = (long)session.GetIdentifier(register);

                        foreach (var siTransfer in listSITransfers)
                        {
                            var fakeId = siTransfer.ID;
                            siTransfer.ID = 0; //обнуляем свой id
                            siTransfer.TransferRegisterID = register.ID; //id родителя
                            siTransfer.StatusID = 1;
                            /*var siTr = new SITransfer();
							siTr.ContractID = siTransfer.ContractID;
							siTr.InsuredPersonsCount = siTransfer.InsuredPersonsCount;
							siTr.InsuredPersonsCountBeforeOperation = siTransfer.InsuredPersonsCountBeforeOperation;
							siTr.PlannedTransferSum = siTransfer.PlannedTransferSum;
							siTr.StatusID = 1;
							siTr.TransferRegisterID = register.ID;*/
                            session.SaveOrUpdate(siTransfer);
                            //siTransfer.ID = (long)session.GetIdentifier(siTransfer);

                            if (!createSITransfers)
                            {
                                //создаем план перечисления, если выплаты правопреемникам
                                //оптимизация по http://jira.vs.it.ru/browse/DOKIPIV-473
                                //должно быть немного быстрее через Hib класс
                                var sums = CurrencyTools.DivideSum(siTransfer.PlannedTransferSum.Value, 12);
                                // session.SetBatchSize(12);
                                for (var i = 0; i < 12; i++)
                                {
                                    session.SaveOrUpdate(new SIUKPlan
                                    {
                                        StatusID = 1,
                                        TransferListID = siTransfer.ID,
                                        PlanSum = sums[i],
                                        MonthID = i + 1
                                    });
                                }

                                // session.SetBatchSize(1);
                            }
                            else
                                foreach (var reqTransfer in listReqTransfers.Where(a => a.TransferListID == fakeId))
                                {
                                    reqTransfer.ID = 0; //обнуляем свой id
                                    reqTransfer.TransferListID = siTransfer.ID; //id родителя
                                    reqTransfer.ID = (long)session.Save(reqTransfer);

                                    if (isFinVypl)
                                    {
                                        //ортимизация по http://jira.vs.it.ru/browse/DOKIPIV-473
                                        session.CreateSQLQuery(
                                            @"UPDATE PFR_BASIC.SI_UKPLAN PLAN SET FACTSUM = :factSum, REQ_TR_ID = :reqTrId 
                                                                WHERE ID IN 
                                                                (SELECT PL.ID FROM PFR_BASIC.SI_UKPLAN PL
                                                                    JOIN PFR_BASIC.SI_TRANSFER TR ON PL.SI_TR_ID = TR.ID
	                                                                JOIN PFR_BASIC.SI_REGISTER REG ON TR.SI_REG_ID = REG.ID 
                                                                    WHERE PL.STATUS_ID <> -1 
                                                                        AND REG.COMP_Y_ID = :yearID
                                                                        AND REG.DIR_SPN_ID = :directionId
                                                                        AND REG.OPERATION_ID = :operationId
                                                                        AND TR.CONTR_ID = :contractID
                                                                        AND PL.MONTH_ID = :monthID
                                                                        )")
                                            .SetParameter("factSum", reqTransfer.Sum)
                                            .SetParameter("reqTrId", reqTransfer.ID)
                                            .SetParameter("yearID", yearId)
                                            .SetParameter("contractID", siTransfer.ContractID.Value)
                                            .SetParameter("monthID", monthId)
                                            .SetParameter("directionId", register.DirectionID.Value)
                                            .SetParameter("operationId", register.OperationID.Value)
                                            .ExecuteUpdate();
                                        /* plan.FactSum = reqTransfer.Sum;
										 plan.TransferID = reqTransfer.ID; //id родителя
										 session.SaveOrUpdate(plan);*/
                                    }
                                }
                        }

                        transaction.Commit();
                    }
                }
                return register.ID;
            }
            catch (Exception ex)
            {
                LogManager.LogException(ex, null);
                return 0;
            }
        }





        public IList<BankDepositInfoItem> GetBankDepositInfoListByBankIds(long[] legalEntityIDs)
        {
            string[] query;
            return GetBankDepositInfoListByBankIds(legalEntityIDs, out query);
        }

        public string[] GetBankDepositInfoQuery(long[] legalEntityIDs)
        {
            string[] query;
            GetBankDepositInfoListByBankIds(legalEntityIDs, out query, true);
            return query;
        }

        private IList<BankDepositInfoItem> GetBankDepositInfoListByBankIds(long[] legalEntityIDs, out string[] querySQL,
            bool buildQueryOnly = false)
        {
            List<BankDepositInfoItem> res = new List<BankDepositInfoItem>();
            List<string> tQuery = new List<string>();

            using (var session = DataAccessSystem.OpenSession())
            {
                List<long> iL = new List<long>();

                IQuery query = session.CreateQuery(@"
select 

d.ID,
o.ID, 
o.Number,

o.Date,
dc2.SettleDate, 
dc2.Amount,
dc2.Rate,

dc2.ReturnDate,
d.Status,
dc2.BankID

from d in Deposit
join d.DepClaimOffer o
join o.DepClaim dc2
left join d.Comment c
where d.Status in(1,3)
and dc2.BankID in(:ids)

order by dc2.SettleDate desc, d.ID desc
");
                query.SetParameterList("ids", legalEntityIDs.ToList());

                var sql = GetSqlByHql(query, session);
                sql += $"\r\nParameters: ({string.Join(", ", legalEntityIDs.Select(x => x.ToString()).ToArray())})";
                tQuery.Add(sql);

                foreach (object[] p in query.List<object[]>())
                {
                    {
                        BankDepositInfoItem x = new BankDepositInfoItem
                        {
                            DepositID = (long)p[0],
                            OfferID = (long)p[1],
                            Number = (long)p[2],

                            Date = (DateTime)p[3],
                            SettleDate = (DateTime)p[4],
                            Amount = (decimal)p[5],
                            Rate = (decimal)p[6],

                            ReturnDate = (DateTime)p[7],
                            Status = (long)p[8],

                            ReturnVolume = 0, //will get later

                            BankID = (long)p[9]
                        };
                        res.Add(x);

                        iL.Add(x.OfferID);
                    }
                }

                // Get payment values from payment history
                if (iL.Count != 0)
                {
                    var _query = session.CreateQuery(@"
                        select 
                        ph.Sum,
                        s.OfferId
                        
                        from ph in PaymentHistory
                        join ph.OfferPfrBankAccountSum s
                        where s.OfferId in(:idL)
                        and ph.ForPercent = 0
                        and s.StatusID>0
                        ");
                    _query.SetParameterList("idL", iL);

                    var hql = GetSqlByHql(_query, session);
                    hql += $"\r\nParameters: ({string.Join(", ", iL.Select(x => x.ToString()).ToArray())})";
                    tQuery.Add(hql);
                    if (!buildQueryOnly)
                    {
                        foreach (var p in _query.List<object[]>())
                        {
                            decimal sum = (decimal)p[0];
                            long offerID = (long)p[1];

                            var x = res.First(y => y.OfferID == offerID);
                            //В случее багов, возможно наличие 2 депозитов у оферты.
                            x.ReturnVolume += sum;
                        }
                    }
                }
            }

            querySQL = tQuery.ToArray();
            //foreach (var x in res)
            //    x.AmountActual = x.Amount;

            return res;
        }

        public RepositoryImpExpFile GetLastBankDepositImpExpData(DateTime auctionDate, int auctionLocalNum, int key,
            int status, int impExp)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var res = session.CreateQuery(@"
                select rep 
                    from RepositoryImpExpFile rep
                    where 
                    rep.AuctionDate = :auctionDate
                    and rep.AuctionLocalNum = :auctionLocalNum
                    and rep.Key = :key
                    and rep.Status = :status
                    and rep.ImpExp = :impExp
                    order by rep.DDate desc, rep.TTime desc
                    ")
                    .SetParameter("auctionDate", auctionDate)
                    .SetParameter("auctionLocalNum", auctionLocalNum)
                    .SetParameter("key", key)
                    .SetParameter("status", status)
                    .SetParameter("impExp", impExp)
                    .List<RepositoryImpExpFile>()
                    .FirstOrDefault();

                return res;
            }
        }

        public IList<BankDepositInfoItem> GetBankDepositInfoList(long legalEntityID)
        {
            var res = GetBankDepositInfoListByBankIds(new [] { legalEntityID });
            return res;
        }

        public List<LegalEntity> GetBankListByStockCode(string stockCode)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
                select le 
                     from BankStockCode bc
	                    join bc.Bank le
                     join le.Contragent c
                     where 
                     bc.StockCode = :stockCode
                     and c.StatusID <> -1").SetParameter("stockCode", stockCode);

                return query.List<LegalEntity>().ToList();
            }
        }


        public PaymentHistoryItem GetPaymentHistoryItem(long id)
        {
            var res = new PaymentHistoryItem {Payment = GetByID<PaymentHistory>(id)};

            //select payment

            if (res.Payment == null)
                return null;

            //Select other data

            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
                select 
ba.AccountNumber,
b.FormalizedName,
p.Year

    from PaymentHistory ph
join ph.OfferPfrBankAccountSum s
join s.PfrBankAccount ba
join ba.LegalEntity b
join s.Portfolio p

where 
ph.ID = :id
and s.StatusID>0
").SetParameter("id", id);

                var p = query.UniqueResult<object[]>();

                res.AccountNumber = (string)p[0];
                res.BankName = (string)p[1];
                res.PortfolioName = (string)p[2];
            }

            return res;
        }

        public List<ModelContactRecord> GetModelContactRecords()
        {
            var lastCulture = Thread.CurrentThread.CurrentCulture;

            try
            {

                List<ModelContactRecord> list = new List<ModelContactRecord>();

                using (var session = DataAccessSystem.OpenSession())
                {
                    var query = session.CreateQuery(@"SELECT c.ID ,c.Email,c.FIO ,le.FormalizedName   FROM Contact c
					JOIN c.LegalEntity le
					JOIN le.Contragent cg
					WHERE (cg.TypeName = 'УК' OR cg.TypeName = 'ГУК') 
						AND (cg.StatusID = 1 OR (cg.StatusID = 6 OR cg.StatusID = 2) AND le.CloseDate > :today)
					ORDER BY le.Contragent.Name, c.FIO")
                        .SetParameter("today", DateTime.Today);

                    list.AddRange(
                        query.List<object[]>()
                            .Select(
                                p =>
                                    new ModelContactRecord
                                    {
                                        ContactID = (long)p[0],
                                        Email = p[1].ToString(),
                                        FIO = p[2].ToString(),
                                        UkName = p[3].ToString()
                                    }));

                }

                return list;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = lastCulture;
            }

        }

#region Costs

        private static ICriteria GetCostsListFilteredQuery(ISession session, DateTime? from, DateTime? to)
        {
            var crit = session.CreateCriteria<CostHib>("Cost")
                .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                .SetFetchMode("Portfolio", FetchMode.Join)
                .CreateCriteria("Cost.Currency", "c", JoinType.InnerJoin)
                .SetFetchMode("Cost.Currency", FetchMode.Join);
            if (from.HasValue)
                crit.Add(Restrictions.Ge("Cost.PaymentDate", from.Value));
            if (to.HasValue)
                crit.Add(Restrictions.Le("Cost.PaymentDate", to.Value));
            return crit;
        }

        public long GetCountCostsListFiltered(DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = GetCostsListFilteredQuery(session, from, to);
                return crit.SetProjection(Projections.RowCountInt64()).UniqueResult<long>();
            }
        }

        public IList<Cost> GetCostsListFilteredByPage(int startIndex, DateTime? from, DateTime? to)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var crit = GetCostsListFilteredQuery(session, from, to);
                return crit.SetFirstResult(startIndex).SetMaxResults(PageSize).SetMaxResults(PageSize).List<Cost>();
            }
        }

#endregion Costs

        public List<PaymentAssignment> GetPayAssignmentsForNpf()
        {
            var result = SessionWrapper(session => session.CreateQuery(@"select pa from PaymentAssignmentHib pa
                                        left join fetch pa.Element el
                                        where pa.PartID = :appPart and ((pa.OperationContentID=el.ID and el.Key=:key) or (pa.OperationContentID=0 and el.ID is null)) ")
               .SetParameter("appPart", (long)Element.SpecialDictionaryItems.AppPartNPF)
               .SetParameter("key", 61)
               .List<object>());
            return result.Cast<PaymentAssignment>().ToList();
        }

        public List<PaymentAssignmentHib> GePayAssignmentsForNpfWithElementName()
        {
            var result = SessionWrapper(session => session.CreateQuery(@"select pa, el.Name from PaymentAssignmentHib pa
                                        left join fetch pa.Element el
                                        where pa.PartID = :appPart and ((pa.OperationContentID=el.ID and el.Key=:key) or (pa.OperationContentID=0 and el.ID is null)) ")
                .SetParameter("appPart", (long)Element.SpecialDictionaryItems.AppPartNPF)
                .SetParameter("key", 61)
                .List<object[]>().Select(a =>
                {
                    var p = a[0] as PaymentAssignmentHib;
                    p.ElementName = (string)a[1];
                    return p;
                }));
            return result.Cast<PaymentAssignmentHib>().ToList();
        }

        public string GePayAssignmentForNpf(string operation, long dirId)
        {
            return SessionWrapper(session =>
            {
                return session.CreateQuery(@"select pa.Name from PaymentAssignment pa
                                        join pa.Element el
                                        where pa.PartID = :appPart and pa.OperationContentID=el.ID and pa.DirectionID=:dirId and el.Name=:op and el.Key=:key")
                     .SetParameter("appPart", (long)Element.SpecialDictionaryItems.AppPartNPF)
                     .SetParameter("dirId", dirId)
                     .SetParameter("op", operation)
                     .SetParameter("key", 61)
                     .UniqueResult<string>();
            });
        }

        public List<OrdReport> GetReportByOrderType(string orderType, int? limit)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                IQuery query;
                if (string.Equals(OrderTypeIdentifier.RELAYING, orderType))
                {
                    query =
                        session.CreateQuery(
                            "select rep from OrdReport rep inner join rep.CbInOrder cb inner join cb.Order o  "
                            + " where o.Type = :orderType").SetParameter("orderType", orderType);
                }
                else
                {
                    query =
                        session.CreateQuery(
                            "select rep from OrdReport rep inner join rep.CbInOrder cb inner join cb.Order o  "
                            + " where o.ParentId is not null");
                }

                if (limit.HasValue)
                {
                    query.SetMaxResults(limit.Value);
                }

                //session.CreateCriteria<OrdReport>("OrdReport")
                //                   .CreateCriteria("CbInOrder", "cb", NHibernate.SqlCommand.JoinType.InnerJoin)
                //                   .SetFetchMode("CbInOrder", FetchMode.Join)
                //                   .CreateCriteria("cb.Order", "o", NHibernate.SqlCommand.JoinType.InnerJoin)
                //                   .SetFetchMode("cb.Order", FetchMode.Join);


                return query.List<OrdReport>().ToList();
            }
        }

#region BlUserSettings

        public IList<BlUserSetting> GetBlUserSettings()
        {
            //this.DomainUser
            using (var session = DataAccessSystem.OpenSession())
            {
                //Remove expired
                session.CreateQuery("DELETE BlUserSetting o WHERE o.ExpireDate < :date")
                    .SetParameter("date", DateTime.Now)
                    .ExecuteUpdate();

                //Get settings
                var list =
                    session.CreateCriteria<BlUserSettingHib>()
                        .Add(Restrictions.Eq("UserLogin", DomainUser))
                        .List<BlUserSetting>();
                return list;
            }
        }


        public void SaveBlUserSetting(BlUserSetting setting)
        {
            TransactionWrapper(session =>
            {
                setting.UserLogin = DomainUser;
                setting.UpdateDate = DateTime.Now;
                var exists = session.CreateCriteria<BlUserSettingHib>()
                    .Add(Restrictions.Eq("UserLogin", DomainUser))
                    .Add(Restrictions.Eq("Key", setting.Key))
                    .UniqueResult<BlUserSetting>();
                session.Evict(exists);
                if (exists != null)
                    setting.ID = exists.ID;

                session.SaveOrUpdate(setting);
            });
        }

#endregion BlUserSettings

        public IList<LoginMessageItem> GetLoginMessageItems()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var messages = new List<LoginMessageItem>();

                var dt = DateTime.Now;

                //LetterOfAttorneyExpireDate 0-7 days				
                var list1 =
                    session.CreateQuery(
                        "SELECT lea.ID, lec.FirstName, lec.LastName, lea.ExpireDate, lec.LegalEntityName  from LegalEntityCourier lec JOIN lec.LetterOfAttorneys lea where lec.StatusID <> -1 AND lec.TypeID = :typeId AND lea.ExpireDate between :from and :to ORDER BY lea.ExpireDate desc ")
                        .SetParameter<long>("typeId", 3) //Person = 3
                        .SetParameter("from", dt.AddDays(0))
                        .SetParameter("to", dt.AddDays(7)).List<object[]>();

                messages.AddRange(list1.Select(n => new LoginMessageItem
                {
                    Key = $"LoginMessage.ExpireAtt.7d.{(long) n[0]}",
                    Title = "Доверенные лица, срок доверенности которых истекает в течение недели",
                    Message =
                        string.Format("{0} {1} (\"{3}\", {2})", (string)n[1], (string)n[2],
                            (n[3] as DateTime?).Value.ToString("dd.MM.yyyy"), n[4] as string),
                    Order = 1,
                    refID = (long)n[0]
                }));

                //LetterOfAttorneyExpireDate 7-14 days
                list1 =
                    session.CreateQuery(
                        "SELECT lea.ID, lec.FirstName, lec.LastName, lea.ExpireDate, lec.LegalEntityName  from LegalEntityCourier lec JOIN lec.LetterOfAttorneys lea where lec.StatusID <> -1 AND lec.TypeID = :typeId AND lea.ExpireDate between :from and :to ORDER BY lea.ExpireDate desc ")
                        .SetParameter<long>("typeId", 3) //Person = 3
                        .SetParameter("from", dt.AddDays(7))
                        .SetParameter("to", dt.AddDays(14)).List<object[]>();
                messages.AddRange(list1.Select(n => new LoginMessageItem
                {
                    Key = $"LoginMessage.ExpireAtt.7d.{(long) n[0]}",
                    Title = "Доверенные лица, срок доверенности которых истекает в течение двух недель",
                    Message =
                        string.Format("{0} {1} (\"{3}\", {2})", (string)n[1], (string)n[2],
                            (n[3] as DateTime?).Value.ToString("dd.MM.yyyy"), n[4] as string),
                    Order = 2,
                    refID = (long)n[0]
                }));


                //SignatureExpireDate 0-7 days
                list1 =
                    session.CreateQuery(
                        "SELECT cert.ID, lec.FirstName, lec.LastName, cert.ExpireDate, lec.LegalEntityName  from LegalEntityCourier lec JOIN lec.Certificates cert  where lec.StatusID <> -1 AND lec.TypeID = :typeId AND cert.ExpireDate between :from and :to")
                        .SetParameter<long>("typeId", 3) //Person = 3
                        .SetParameter("from", dt.AddDays(0))
                        .SetParameter("to", dt.AddDays(7)).List<object[]>();
                messages.AddRange(list1.Select(n => new LoginMessageItem
                {
                    Key = $"LoginMessage.ExpireSig.7d.{(long) n[0]}",
                    Title = "Доверенные лица, срок сертификата ЭЦП которых истекает в течение недели",
                    Message =
                        string.Format("{0} {1} (\"{3}\", {2})", (string)n[1], (string)n[2],
                            (n[3] as DateTime?).Value.ToString("dd.MM.yyyy"), n[4] as string),
                    Order = 3,
                    refID = (long)n[0]
                }));

                //SignatureExpireDate 7-14 days
                list1 =
                    session.CreateQuery(
                        "SELECT cert.ID, lec.FirstName, lec.LastName, cert.ExpireDate, lec.LegalEntityName  from LegalEntityCourier lec JOIN lec.Certificates cert  where lec.StatusID <> -1 AND lec.TypeID = :typeId AND cert.ExpireDate between :from and :to")
                        .SetParameter<long>("typeId", 3) //Person = 3
                        .SetParameter("from", dt.AddDays(7))
                        .SetParameter("to", dt.AddDays(14)).List<object[]>();
                messages.AddRange(list1.Select(n => new LoginMessageItem
                {
                    Key = $"LoginMessage.ExpireSig.7d.{(long) n[0]}",
                    Title = "Доверенные лица, срок сертификата ЭЦП которых истекает в течение двух недель",
                    Message =
                        string.Format("{0} {1} (\"{3}\", {2})", (string)n[1], (string)n[2],
                            (n[3] as DateTime?).Value.ToString("dd.MM.yyyy"), n[4] as string),
                    Order = 4,
                    refID = (long)n[0]
                }));

                

                var settings = GetBlUserSettings();

                messages.ForEach(m =>
                {
                    var ignore = settings.FirstOrDefault(s => s.Key == m.Key + ".Ignore");
                    m.Ignore = ignore != null;
                });



                return messages;
            }
        }

        public void SetIgnoredLoginMessages(IList<string> mesagesKeys)
        {
            foreach (var key in mesagesKeys)
            {
                SaveBlUserSetting(new BlUserSetting
                {
                    Key = key + ".Ignore",
                    Value = "True",
                    ExpireDate = DateTime.Now.AddDays(20)
                });
            }
            ;
        }

        private static readonly Mutex ROPSMutex = new Mutex();


        public LoginMessageItem GetROPSLimitMessage(bool ignoreHidden = false, bool updateNow = false)
        {
            try
            {
                ROPSMutex.WaitOne();
                var rops = new ROPSLimitNotifyTask();
                if (updateNow)
                    rops.Process();

                if (rops.Message == null)
                    return null;

                var settings = GetBlUserSettings();
                bool ignore = rops.Message != null &&
                              settings.FirstOrDefault(s => s.Key == rops.Message.Key + ".Ignore") != null;

                if (!ignoreHidden && ignore)
                    return null;
                else
                    return rops.Message;
            }
            finally
            {
                ROPSMutex.ReleaseMutex();
            }
        }



        public bool CheckSIRegisterDuplicate(SIRegister register)
        {
            if (register == null)
                return false;
            using (ISession session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@"
									select r from SIRegister r
									where ID <> :id and RegisterKind = :kind and RegisterNumber = :number and RegisterDate = :date
									")
                    .SetParameter("id", register.ID)
                    .SetParameter("kind", register.RegisterKind)
                    .SetParameter("number", register.RegisterNumber)
                    .SetParameter("date", register.RegisterDate)
                    .List<SIRegister>();
                return list.Any();
            }
        }

        public bool UpdateTransferByActSigned(PFR_INVEST.DataObjects.Contract contract, ReqTransfer transfer,
            bool isTransferFromPfrtoUK, TransferStatusIdentifier.ActionTransfer action)
        {


            try
            {

                if (!TransferStatusIdentifier.IsStatusActSigned(transfer.TransferStatus))
                {
                    return false;
                }

                using (var session = DataAccessSystem.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        switch (action)
                        {
                            case TransferStatusIdentifier.ActionTransfer.DeleteTransfer:
                                //он удаляется в модели карточки
                                break;
                            case TransferStatusIdentifier.ActionTransfer.EnterTransferActData:
                                //статус уже выставлен в модели до прихода
                                //session.SaveOrUpdate(transfer);
                                break;
                            case TransferStatusIdentifier.ActionTransfer.RollbackStatus:
                                transfer.SumBeforeOperation = null;
                                transfer.TransferActDate = null;
                                transfer.TransferActNumber = string.Empty;
                                transfer.TransferStatus = isTransferFromPfrtoUK
                                    ? TransferStatusIdentifier.sSPNTransferred
                                    : TransferStatusIdentifier.sDemandReceivedUK;
                                session.SaveOrUpdate(transfer);
                                break;
                        }
                        session.Flush();


                        // Пересчитываем сумму на начало для каждого перечисления. Перечисление уже добавлено/изменено/удалено в базе
                        // получение списка всех перечислений по договору						
                        UpdateReqTrAndContractByActSigned(contract.ID, session);

                        session.Flush();
                        transaction.Commit();
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void UpdateFinregisterIsTransferred(long? ntParentId, int value)
        {
            if(!ntParentId.HasValue) return;
            TransactionWrapper(session => { session.CreateSQLQuery($"update pfr_basic.finregister set is_transferred={value} where id={ntParentId.Value}").ExecuteUpdate(); });
        }

        private decimal GetUKDiff(ReqTransferHib transfer)
        {
            var diff = transfer.Sum ?? 0;
            var dir = transfer.TransferList.TransferRegister.DirectionID;
            bool isTransferFromPfrtoUK = TransferDirectionIdentifier.IsFromPFRToUK(dir);
            if (!isTransferFromPfrtoUK)
            {
                diff = (transfer.InvestmentIncome ?? 0) - diff;
            }
            return diff;
        }

        public long SaveKDoc(KDoc doc)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    if (doc == null) return 0;

                    var kDocSumm = doc.TotalSumm;
                    var kDocPenny = doc.TotalPenny;

                    //Вычисляем разницу текущего ДК и предыдущего
                    KDoc prev = null;
                    if (doc.IsMonthDocument && doc.CheckDate.Value.Month == 2)
                    {
                        prev = null; //Для первого месячного нет предыдущего
                    }
                    else
                    {
                        prev =
                            session.CreateQuery("FROM KDoc doc WHERE doc.CheckDate <= :date ORDER BY doc.CheckDate desc")
                                .SetParameter("date", doc.CheckDate)
                                .SetMaxResults(1)
                                .List<KDoc>()
                                .FirstOrDefault();
                    }
                    if (prev != null)
                    {
                        kDocSumm -= prev.TotalSumm;
                        kDocPenny -= prev.TotalPenny;
                    }


                    var pfAcc = session.Get<PortfolioPFRBankAccountHib>(doc.PortfolioPFRBankAccountID);

                    if (doc.CheckDate?.Date < new DateTime(2017,2,1).Date && doc.DocType == (int)KDoc.DocVersion.Latest)
                        doc.DocType = (int)KDoc.DocVersion.Version3;

                    //Сохраняем сам документ
                    session.SaveOrUpdate(doc);


                    if (doc.IsYearDocument)
                    {
                        //Годовой - просто квартальное уточнение с разницей предидущего (декабрьського) и годового. СВ считать не нужно ;)

#region Year

                        if (kDocSumm != 0)
                        {
                            DopSPN dop = new DopSPN
                            {
                                KDocID = doc.ID,
                                PortfolioID = pfAcc.PortfolioID,
                                LegalEntityID = pfAcc.PfrBankAccount.LegalEntityBankID,
                                QuarterPfrBankAccountID = pfAcc.PFRBankAccountID,
                                ChSumm = kDocSumm,
                                DocKind = (long)DopSPN.Kinds.DueQuarter,
                                Quarter = 4,
                                OperationDate = DateTime.Now,
                                Date = doc.DocDate,
                                PeriodID = 12,

                                RegNum = doc.RegNum,
                                Comment = "Годовой документ казначейства " + doc.RegNum
                            };

                            session.Save(dop);
                        }
                        if (kDocPenny != 0)
                        {
                            DopSPN dop = new DopSPN
                            {
                                KDocID = doc.ID,
                                PortfolioID = pfAcc.PortfolioID,
                                LegalEntityID = pfAcc.PfrBankAccount.LegalEntityBankID,
                                QuarterPfrBankAccountID = pfAcc.PFRBankAccountID,
                                ChSumm = kDocPenny,
                                DocKind = (long)DopSPN.Kinds.PennyQuarter,
                                Quarter = 4,
                                OperationDate = DateTime.Now,
                                Date = doc.DocDate,
                                PeriodID = 12,
                                RegNum = doc.RegNum,
                                Comment = "Годовой документ казначейства " + doc.RegNum
                            };

                            session.Save(dop);
                        }

#endregion
                    }

                    else if (doc.IsMonthDocument)
                    {
#region Month

                        var yearID = doc.CheckDate.Value.AddDays(-1).Year % 100;
                        var monthID = doc.CheckDate.Value.AddDays(-1).Month;

                        var totalSumm = GetSumForKDoc(session, yearID, monthID, doc.PortfolioID.Value, 1);
                        //1- Страховые взносы
                        var totalPenny = GetSumForKDoc(session, yearID, monthID, doc.PortfolioID.Value, 2);
                        //2- Пени иштрафы

                        //Уточнение СВ
                        if (totalSumm != kDocSumm)
                        {
                            var last =
                                session.CreateQuery(
                                    "SELECT spn FROM AddSPN spn WHERE spn.YearID = :year AND spn.MonthID = :month AND spn.PortfolioID = :portfolioID ORDER BY spn.NewDate DESC")
                                    .SetParameter("portfolioID", doc.PortfolioID)
                                    .SetParameter<long>("year", yearID)
                                    .SetParameter<long>("month", monthID)
                                    .SetMaxResults(1).List<AddSPN>().FirstOrDefault();

                            DopSPN dop = new DopSPN
                            {
                                KDocID = doc.ID,
                                AddSpnID = last.ID,
                                PortfolioID = last.PortfolioID,
                                ChSumm = kDocSumm - totalSumm,
                                DocKind = (long)DopSPN.Kinds.DueMonth,
                                OperationDate = DateTime.Now,
                                Date = doc.DocDate,
                                PeriodID = last.MonthID,
                                QuarterPfrBankAccountID = last.PFRBankAccountID,
                                RegNum = doc.RegNum,
                                Comment = "Документ казначейства " + doc.RegNum
                            };

                            session.Save(dop);
                        }
                        //Пенни
                        if (totalPenny != kDocPenny)
                        {

                            var spn = new AddSPN
                            {
                                KDocID = doc.ID,
                                Kind = (int)AddSPN.Kinds.Penny,
                                SummNakop = kDocPenny - totalPenny,
                                Summ = kDocPenny - totalPenny,
                                PFRBankAccountID = pfAcc.PFRBankAccountID,
                                PortfolioID = pfAcc.PortfolioID,
                                MonthID = monthID,
                                YearID = yearID,
                                OperationDate = DateTime.Now,
                                NewDate = doc.DocDate,
                                RegNum = doc.RegNum,
                                Comment = "Документ казначейства " + doc.RegNum

                            };

                            session.Save(spn);
                        }

#endregion
                    }

                    transaction.Commit();
                    return doc.ID;
                }
            }

        }

        public void DeleteKDoc(KDoc doc)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    //Удаление связаных с ДК уточнений
                    session.CreateQuery("DELETE DopSPN o WHERE o.KDocID = :id")
                        .SetParameter("id", doc.ID)
                        .ExecuteUpdate();

                    //Удаление связаных с ДК пени
                    session.CreateQuery("DELETE AddSPN o WHERE o.KDocID = :id")
                        .SetParameter("id", doc.ID)
                        .ExecuteUpdate();

                    session.Delete(doc);
                    transaction.Commit();
                }
            }
        }

        private decimal GetSumForKDoc(ISession session, int yearID, int monthID, long portfolioID, int aspKind)
        {
            var sumIncome = session.CreateQuery(@"SELECT sum(spn.Summ) FROM AddSPN spn 
																WHERE spn.PortfolioID = :portfolioID AND spn.YearID = :year AND spn.MonthID = :month
																	AND spn.Kind = :kind AND spn.KDocID is null")
                .SetParameter<long?>("portfolioID", portfolioID)
                .SetParameter("kind", aspKind)
                .SetParameter<long>("year", yearID)
                .SetParameter<long>("month", monthID)
                .UniqueResult<decimal>();

            var sumPrecise = session.CreateQuery(@"SELECT sum(dop.ChSumm) FROM  DopSPN dop JOIN dop.AddSPN spn
																WHERE spn.PortfolioID = :portfolioID AND spn.YearID = :year AND spn.MonthID = :month
																	AND spn.Kind =  :kind AND spn.KDocID is null")
                .SetParameter<long?>("portfolioID", portfolioID)
                .SetParameter("kind", aspKind)
                .SetParameter<long>("year", yearID)
                .SetParameter<long>("month", monthID)
                .UniqueResult<decimal>();

            var total = sumIncome + sumPrecise;

            return total;
        }

#region PaymentOrder

        public long GetPaymentOrderCount(DateTime? periodStart, DateTime? periodEnd)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"
						SELECT count(po.ID) 
						FROM PaymentOrder po
						WHERE po.StatusID <> -1
														{(periodStart.HasValue ? " AND po.DraftDate >= :start" : "")}{(periodEnd.HasValue ? " AND po.DraftDate < :end" : "")}");
                if (periodStart.HasValue)
                    query.SetParameter("start", periodStart.Value.Date);
                if (periodEnd.HasValue)
                    query.SetParameter("end", periodEnd.Value.Date.AddDays(1));
                return query.UniqueResult<long>();
            }
        }


        public List<PaymentOrderListItem> GetPaymentOrderByPage(long startIndex, DateTime? periodStart, DateTime? periodEnd)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query =
                    session.CreateQuery(
                        $@"SELECT new PaymentOrderListItem(po, po.Comment) FROM 
												PaymentOrder po
												WHERE po.StatusID <> -1
														{(periodStart
                            .HasValue
                            ? " AND po.DraftDate >= :start"
                            : "")}{(periodEnd.HasValue ? " AND po.DraftDate < :end" : "")}
												ORDER BY po.ID");
                if (periodStart.HasValue)
                    query.SetParameter("start", periodStart.Value.Date);
                if (periodEnd.HasValue)
                    query.SetParameter("end", periodEnd.Value.Date.AddDays(1));
                return query.SetFirstResult((int)startIndex).SetMaxResults(PageSize)
                    .List<PaymentOrderListItem>().ToList();
            }
        }


        public PaymentOrderListItem GetPaymentOrderByID(long id, DateTime? periodStart, DateTime? periodEnd)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                periodEnd = periodEnd?.AddDays(1);

                var list = session.CreateQuery(@"SELECT new PaymentOrderListItem(po, po.Comment) FROM 
												PaymentOrder po
												WHERE po.StatusID <> -1
														AND (:start is null OR po.DraftDate >= :start) AND  (:end is null OR po.DraftDate < :end)
														AND po.ID = :id
												ORDER BY po.ID") //rt.StatusID <> -1 AND 
                    .SetParameter("start", periodStart)
                    .SetParameter("end", periodEnd)
                    .SetParameter("id", id)
                    .SetMaxResults(1)
                    .List<PaymentOrderListItem>();
                return list.FirstOrDefault();
            }
        }

#endregion

#region CommonPP

        public long GetCommonPPCount(DateTime? periodStart, DateTime? periodEnd, bool isOpfr)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery($@"SELECT count(rt.ID) FROM 
												AsgFinTr rt												
												JOIN rt.Portfolio p
												WHERE (rt.Unlinked is null OR rt.Unlinked = 0) AND rt.StatusID <> -1 AND rt.ContragentTypeID in (:list)
														{(periodStart.HasValue ? " AND rt.DraftPayDate >= :start" : "")}{(periodEnd.HasValue ? " AND rt.DraftPayDate <= :end" : "")}");

                query.SetParameterList("list", isOpfr ? new[] { (long)AsgFinTr.ContragentTypeEnum.OPFR } :
                    new[] { (long)AsgFinTr.ContragentTypeEnum.None, (long)AsgFinTr.ContragentTypeEnum.UK, (long)AsgFinTr.ContragentTypeEnum.NPF });

                if (periodStart.HasValue)
                    query.SetParameter("start", periodStart);
                if (periodEnd.HasValue)
                    query.SetParameter("end", periodEnd);
                return query.UniqueResult<long>();
            }

        }


        public List<PPListItem> GetCommonPPByPage(long startIndex, DateTime? periodStart, DateTime? periodEnd, bool isOpfr)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query =
                    session.CreateQuery(
                        $@"SELECT new PPListItem(pp, p.Year, p.StatusID, pd.PaymentDetails) FROM 
												AsgFinTr pp												
												left JOIN pp.Portfolio p
                                                left join pp.PaymentDetail pd 
												WHERE (pp.Unlinked is null OR pp.Unlinked = 0) AND pp.StatusID <> -1 AND pp.ContragentTypeID in (:list)
														{(periodStart
                            .HasValue
                            ? " AND pp.DraftPayDate >= :start"
                            : "")}{(periodEnd.HasValue ? " AND pp.DraftPayDate <= :end" : "")}
												ORDER BY pp.ID");
                query.SetParameterList("list", isOpfr ? new[] { (long)AsgFinTr.ContragentTypeEnum.OPFR } :
                    new[] {(long) AsgFinTr.ContragentTypeEnum.None, (long) AsgFinTr.ContragentTypeEnum.UK, (long) AsgFinTr.ContragentTypeEnum.NPF});
                
                if (periodStart.HasValue)
                    query.SetParameter("start", periodStart);
                if (periodEnd.HasValue)
                    query.SetParameter("end", periodEnd);
                return query.SetFirstResult((int)startIndex).SetMaxResults(PageSize)
                    .List<PPListItem>().ToList();
            }
        }


        public PPListItem GetCommonPPByID(long id, DateTime? periodStart, DateTime? periodEnd)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@"SELECT new PPListItem(rt, p.Year, p.StatusID, pd.PaymentDetails) FROM 
												AsgFinTr rt												
												JOIN rt.Portfolio p
                                                left join rt.PaymentDetail pd 
												WHERE (rt.Unlinked is null OR rt.Unlinked = 0) AND rt.StatusID <> -1
														AND (:start is null OR rt.DraftPayDate >= :start) AND  (:end is null OR rt.DraftPayDate <= :end)
														AND rt.ID = :id
												ORDER BY rt.ID") //rt.StatusID <> -1 AND 
                    .SetParameter("start", periodStart)
                    .SetParameter("end", periodEnd)
                    .SetParameter("id", id)
                    .SetMaxResults(1)
                    .List<PPListItem>();
                return list.FirstOrDefault();
            }
        }

        public CommonRegisterListItem GetCommonRegister(AsgFinTr.Sections? section, long id)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                List<CommonRegisterListItem> list = new List<CommonRegisterListItem>();
                switch (section)
                {

                    case AsgFinTr.Sections.BackOffice:
                        list =
                            session.CreateQuery(@"select new CommonRegisterListItem(r.ID, r.RegDate, r.RegNum) 
														from OpfrRegister r 
														join r.Kind k
														where r.ID = :id and r.StatusID <> -1")
                                .SetParameter("id", id)
                                .List<CommonRegisterListItem>().ToList();
                        break;

                    case AsgFinTr.Sections.NPF:
                        list =
                            session.CreateQuery(@"select new CommonRegisterListItem(r.ID, r.RegDate, r.RegNum, ad.Name) 
														from Register r 
														left join r.ApproveDoc ad 
														where r.ID = :id and r.StatusID <> -1")
                                .SetParameter("id", id)
                                .List<CommonRegisterListItem>().ToList();
                        break;
                    case AsgFinTr.Sections.SI:
                    case AsgFinTr.Sections.VR:
                        var contractType = (section == AsgFinTr.Sections.SI)
                            ? (int)Document.Types.SI
                            : (int)Document.Types.VR;
                        list =
                            session.CreateQuery(
                                @"select new CommonRegisterListItem(r.ID, r.RegisterDate, r.RegisterNumber, r.RegisterKind) 
													from SIRegister r
													where r.ID = :id")
                                .SetParameter("id", id)
                                .List<CommonRegisterListItem>().ToList();
                        break;
                }
                return list.FirstOrDefault();
            }
        }

        /// <summary>
        /// Выборка реестров для указания в п/п
        /// </summary>
        /// <param name="section">Раздел ПТК</param>
        /// <param name="direction">Направление</param>
        public List<CommonRegisterListItem> GetCommonRegisterListForPP(AsgFinTr.Sections section, long direction)
        {
            var list = new List<CommonRegisterListItem>();
            using (var session = DataAccessSystem.OpenSession())
            {
                string queryString = "";

                if (section == AsgFinTr.Sections.BackOffice)
                {
                    //TODO condition
                    var opfrDirection = direction;
                    var rlist = session.CreateQuery($@"from OpfrRegister r
                                              where r.KindID=:kind and StatusID<>-1")
                        .SetParameter("kind", opfrDirection == (long) AsgFinTr.Directions.FromPFR ? (long)Element.SpecialDictionaryItems.OpfrDirectionFromPfr : (long)Element.SpecialDictionaryItems.OpfrDirectionToPfr)
                        .List<OpfrRegister>()
                        .Select(a => new CommonRegisterListItem(a.ID, a.RegDate, a.RegNum, null))
                        .ToList();
                    return rlist;
                }

                var ppDirection = (AsgFinTr.Directions) direction;
                //используем стандартный запрос, если условия не совпали
                if ((section != AsgFinTr.Sections.NPF && section != AsgFinTr.Sections.SI && section != AsgFinTr.Sections.VR) || ppDirection != AsgFinTr.Directions.FromPFR)
                    return GetCommonRegisterList(section, direction, null);
                switch (section)
                {
                    case AsgFinTr.Sections.NPF:
                        //используем запрос согласно http://jira.vs.it.ru/browse/DOKIPIV-669
                        queryString =
                                @"select new CommonRegisterListItem(r.ID, r.RegDate, r.RegNum, ad.Name) 
									    from Register r 
									    left join r.ApproveDoc ad 
									    where r.StatusID <> -1 and r.TrancheDate is not null
                                                AND (select count(f.ID) from Finregister f where f.RegisterID = r.ID and f.Status = :fStatus and f.StatusID <>-1) > 0
											    AND lower(r.Kind) = lower(:direction)
									    group by r.ID, r.RegDate, r.RegNum, ad.Name";
                        list = session.CreateQuery(queryString)
                            .SetParameter("direction", RegisterIdentifier.PFRtoNPF)
                            .SetParameter("fStatus", RegisterIdentifier.FinregisterStatuses.Created)
                            .List<CommonRegisterListItem>()
                            .OrderBy(a => a.Name)
                            .ToList();
                        break;
                    case AsgFinTr.Sections.SI:
                    case AsgFinTr.Sections.VR:
                        var contractType = (section == AsgFinTr.Sections.SI)
                            ? (int)Document.Types.SI
                            : (int)Document.Types.VR;
                        list = session.CreateQuery(
                                @"select new CommonRegisterListItem(r.ID, r.RegisterDate, r.RegisterNumber, r.RegisterKind) 
													from SIRegister r 
													join r.TransferLists st 
													join st.Transfers rt 
													join st.Contract c
													where rt.StatusID <> -1 AND rt.Unlinked <> 1 AND rt.PaymentOrderDate is null AND st.StatusID <> -1
															AND rt.TransferStatus in (:statuses) AND c.TypeID = :type
															AND r.DirectionID in (:directions)
													group by r.ID, r.RegisterDate, r.RegisterNumber, r.RegisterKind")
                                .SetParameterList("directions", new List<long>
                                    {
                                        (long) SIRegister.Directions.ToUK,
                                        (long) SIRegister.Directions.ToGUK
                                    })
                                .SetParameterList("statuses",
                                    new List<string>
                                    {
                                        TransferStatusIdentifier.sSPNTransferred,
                                        TransferStatusIdentifier.sClaimCompleted
                                    })
                                .SetParameter("type", contractType)
                                .List<CommonRegisterListItem>()
                                .OrderBy(a => a.Name)
                                .ToList();
                        break;
                }
            }
            list.ForEach(m => m.Section = section);
            return list;
        }

        public List<CommonRegisterListItem> GetCommonRegisterList(AsgFinTr.Sections section,
            long direction, string content = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = new List<CommonRegisterListItem>();
                switch (section)
                {
                    case AsgFinTr.Sections.BackOffice:
                        var opfrDirection = direction;
                        var rlist = session.CreateSQLQuery($@"select distinct r.ID, r.REGDATE, r.REGNUM from PFR_BASIC.OPFR_REGISTER r
                                                              JOIN PFR_BASIC.OPFR_TRANSFER tr ON tr.OPFR_REGISTER_ID = r.ID

                                                              LEFT JOIN (select count(pp.ID) as cnt, sum(pp.DRAFT_AMOUNT) as sum, l.OPFR_TRANSFER_ID as TID, l.asg_fin_tr_id as PPID
                                                                         from PFR_BASIC.ASG_FIN_TR pp
                                                                         JOIN PFR_BASIC.LINK_PP_OPFRTRANSFER l ON l.asg_fin_tr_id = pp.ID
                                                                           where pp.STATUS_ID<>-1
                                                                        GROUP BY l.OPFR_TRANSFER_ID, l.asg_fin_tr_id) c ON c.TID = tr.ID

                                                              LEFT JOIN (select sum(tr2.SUM) as sum, l.asg_fin_tr_id as ID
                                                                         from PFR_BASIC.REQ_TRANSFER tr2
                                                                         JOIN PFR_BASIC.LINK_PP_OPFRTRANSFER l ON l.OPFR_TRANSFER_ID = tr2.ID
                                                                           WHERE tr2.STATUS_ID<>-1
                                                                        GROUP BY l.asg_fin_tr_id) tri ON tri.ID = c.PPID


                                                            WHERE (c.cnt is NULL or (c.cnt>0 and c.sum<>tr.SUM) or (c.cnt=1 and c.sum < tr.SUM and tri.sum<>c.sum))
                                                            and r.STATUS_ID<>-1 and tr.STATUS_ID<>-1 and r.KIND_ID=:kind")
                            .SetParameter("kind", opfrDirection == (long)AsgFinTr.Directions.FromPFR ? (long)Element.SpecialDictionaryItems.OpfrDirectionFromPfr : (long)Element.SpecialDictionaryItems.OpfrDirectionToPfr)
                            .List<object[]>()
                            .Select(a => new CommonRegisterListItem((long)a[0], (DateTime?)a[1], (string)a[2], null))
                            .ToList();
                        return rlist;
                        break;
                    case AsgFinTr.Sections.NPF:

                        string queryString;

                        if (string.IsNullOrEmpty(content))
                        {
                            queryString =
                                @"SELECT
                                                  r.ID     ,
                                                  r.regdate,
                                                  r.regnum ,
                                                  ad.name
                                                FROM pfr_basic.register r
                                                  LEFT OUTER JOIN pfr_basic.approvedoc ad ON r.appd_id = ad.ID
                                                  INNER JOIN pfr_basic.finregister fr ON r.ID = fr.reg_id
                                                  LEFT JOIN(SELECT sum(asg.draft_amount) as ppsum, asg.fr_id as fr_id
                                                                                  FROM pfr_basic.asg_fin_tr asg
                                                                                  WHERE asg.status_id <> -1
                                                                                  GROUP BY asg.fr_id) as pp ON pp.fr_id = fr.ID
                                                WHERE
                                                  r.status_id <> -1 AND
                                                  fr.status_id <> -1 AND
                                                  fr.status not in (:statuses) AND
                                                  ((lower(r.kind) = :toNPF AND r.tranchedate IS NOT NULL) OR
                                                   (lower(r.kind) = :fromNPF AND lower(fr.status) = :statusIssued)) AND
                                                  (fr.count <> pp.ppsum OR pp.ppsum IS NULL)
                                                  AND lower(r.kind) = :direction
                                                GROUP BY r.ID, r.regdate, r.regnum, ad.name; ";
                            list = session.CreateSQLQuery(queryString)
                                .SetParameter("toNPF", RegisterIdentifier.PFRtoNPF.ToLower())
                                .SetParameter("fromNPF", RegisterIdentifier.NPFtoPFR.ToLower())
                                .SetParameter("direction",
                                    (AsgFinTr.Directions)direction == AsgFinTr.Directions.FromPFR
                                        ? RegisterIdentifier.PFRtoNPF.ToLower()
                                        : RegisterIdentifier.NPFtoPFR.ToLower())
                                .SetParameterList("statuses", new object[] { RegisterIdentifier.FinregisterStatuses.Transferred.ToLower(), RegisterIdentifier.FinregisterStatuses.NotTransferred.ToLower() })
                                .SetParameter("statusIssued", RegisterIdentifier.FinregisterStatuses.Issued.ToLower())
                                .List<object[]>()
                                .Select(a => new CommonRegisterListItem((long)a[0], (DateTime?)a[1], (string)a[2], (string)a[3]))
                                .OrderBy(a => a.Name).ToList();
                        }
                        else
                        {
                            string contragentFilter = " or ca.TYPE = 'ПФР' ";
                            /*switch (section)
							{
								case AsgFinTr.Sections.NPF:
									contragentFilter = " or contragent4_.TYPE = 'ПФР' ";
							}*/
                            var limit1Statement = IsDB2 ? "" : "LIMIT 1";

                            //выдернуто из хибера и проапдечено, т.к. хибер не поддерживает лимит результата выборки в подзапросах
                            queryString =
                                string.Format(
                                    @"SELECT DISTINCT
                                          r.ID,
                                          r.REGDATE,
                                          r.REGNUM,
                                          ad.NAME,
                                          count(fr.ID)
                                        FROM PFR_BASIC.REGISTER r
                                          LEFT OUTER JOIN PFR_BASIC.APPROVEDOC ad
                                            ON r.APPD_ID = ad.ID
                                          LEFT OUTER JOIN PFR_BASIC.FINREGISTER fr
                                            ON r.ID = fr.REG_ID AND fr.STATUS_ID <> -1
                                          LEFT OUTER JOIN PFR_BASIC.ACCOUNT acc
                                            ON fr.{0} = acc.ID
                                          LEFT JOIN (SELECT frx.ID as ID, frx.reg_id as REG_ID
                                                     FROM PFR_BASIC.FINREGISTER frx
                                                     WHERE frx.STATUS_ID <> -1
                                                     GROUP BY frx.ID, frx.REG_ID
                                                     {2}) as frc ON frc.REG_ID = r.ID
                                          LEFT JOIN (SELECT ca.ID as ID
                                                     FROM PFR_BASIC.CONTRAGENT ca
                                                     WHERE ca.STATUS_ID = -1 {1}
                                                     {2}) as cal ON cal.ID = acc.CONTRAGENTID
                                        WHERE r.STATUS_ID <> -1 AND
                                              (frc.ID IS NULL OR cal.ID is NULL) AND
                                              fr.STATUS not in (:statuses)
                                              AND lower(r.KIND) = lower(:direction) AND
                                              TRIM(lower(r.CONTENT)) = TRIM(lower(:content))
                                        GROUP BY r.ID, r.REGDATE, r.REGNUM, ad.NAME",
                                    (AsgFinTr.Directions)direction == AsgFinTr.Directions.FromPFR ? "DBT_ACC_ID" : "CR_ACC_ID",
                                    contragentFilter, limit1Statement);

                            list = session.CreateSQLQuery(queryString)
                                .SetParameter("content", content)
                                .SetParameterList("statuses", new object[] { RegisterIdentifier.FinregisterStatuses.Transferred.ToLower(), RegisterIdentifier.FinregisterStatuses.NotTransferred.ToLower() })
                                .SetParameter("direction",
                                    (AsgFinTr.Directions)direction == AsgFinTr.Directions.FromPFR
                                        ? RegisterIdentifier.PFRtoNPF
                                        : RegisterIdentifier.NPFtoPFR)
                                .List().Cast<object[]>().ToList().ConvertAll(a =>
                                    new CommonRegisterListItem((long)a[0], (DateTime?)a[1], (string)a[2],
                                        (string)a[3])
                                ).ToList();
                        }
                        break;
                    case AsgFinTr.Sections.SI:
                    case AsgFinTr.Sections.VR:
                        var contractType = (section == AsgFinTr.Sections.SI)
                            ? (int)Document.Types.SI
                            : (int)Document.Types.VR;
                        list =
                            session.CreateQuery(
                                @"select new CommonRegisterListItem(r.ID, r.RegisterDate, r.RegisterNumber, r.RegisterKind) 
													from SIRegister r 
													join r.TransferLists st 
													join st.Transfers rt 
													join st.Contract c
													where rt.StatusID <> -1 AND rt.Unlinked <> 1 AND rt.PaymentOrderDate is null
															AND  rt.TransferStatus in (:statuses) AND c.TypeID = :type
															AND r.DirectionID in (:directions)
													group by r.ID, r.RegisterDate, r.RegisterNumber, r.RegisterKind")
                                .SetParameterList("directions", (AsgFinTr.Directions)direction == AsgFinTr.Directions.FromPFR
                                    ? new List<long>
                                    {
                                        (long) SIRegister.Directions.ToUK,
                                        (long) SIRegister.Directions.ToGUK
                                    }
                                    : new List<long>
                                    {
                                        (long) SIRegister.Directions.FromUK,
                                        (long) SIRegister.Directions.FromGUK
                                    })
                                .SetParameterList("statuses",
                                    new List<string>
                                    {
                                        TransferStatusIdentifier.sDemandSentInUK,
                                        TransferStatusIdentifier.sClaimCompleted
                                    })
                                .SetParameter("type", contractType)
                                .List<CommonRegisterListItem>().ToList();
                        break;
                }

                list.ForEach(m => m.Section = section);
                return list;
            }
        }

        public List<FinregisterListItem> GetFinregisterForCommonPP(long registerID, bool onlyValidNames = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                //http://jira.vs.it.ru/browse/DOKIPIV-392 добавлен фильтр для записей по направлению Возврат из НПФ, брать только финреестры в статусе Оформлен
                var list = session.CreateQuery($@"select new FinregisterListItem(fr, r, {(onlyValidNames ? "lec.FormalizedName" : "acc.Name")}, {
                            (onlyValidNames ? "led.FormalizedName" : "adc.Name")
                        },
                                                                    coalesce((select count(*) from AsgFinTr pp where pp.FinregisterID = fr.ID AND pp.StatusID <> -1), 0)) 
														from Register r 
														left join r.ApproveDoc ad 
														join r.FinregistersList fr
														join fr.CreditAccount ac
														join fr.DebitAccount ad
														join ac.Contragent acc
														join ad.Contragent adc
														join acc.LegalEntities lec
														join adc.LegalEntities led

														where r.StatusID <> -1 and fr.StatusID <> -1 and fr.Status <> :status AND fr.Status <> :status2 
                                                            AND ((trim(lower(r.Kind)) = :dirFromNpf AND trim(lower(fr.Status)) = :statusIssued AND fr.Count <> coalesce((select sum(pp.DraftAmount) from AsgFinTr pp where pp.FinregisterID = fr.ID AND pp.StatusID <> -1), 0)) 
                                                                OR (trim(lower(r.Kind)) = :dirToNpf and  trim(lower(fr.Status)) = :statusCreated and r.TrancheDate is not null) )
															AND r.ID = :id")
                    .SetParameter("status", RegisterIdentifier.FinregisterStatuses.Transferred.ToLower())
                    .SetParameter("status2", RegisterIdentifier.FinregisterStatuses.NotTransferred.ToLower())
                    .SetParameter("dirFromNpf", RegisterIdentifier.NPFtoPFR.ToLower())
                    .SetParameter("statusIssued", RegisterIdentifier.FinregisterStatuses.Issued.ToLower())
                    .SetParameter("statusCreated", RegisterIdentifier.FinregisterStatuses.Created.ToLower())
                    .SetParameter("dirToNpf", RegisterIdentifier.PFRtoNPF.ToLower())

                    .SetParameter("id", registerID)
                    .List<FinregisterListItem>().ToList();
                return list;
            }
        }

        /// <summary>
        /// Получить перечисления СИ / ВР
        /// </summary>
        /// <param name="registerID">ID реестра</param>
        /// <param name="type">Тип: СИ/ВР</param>
        public List<ReqTransferListItem> GetReqTransferForCommonPP(long registerID, Document.Types type)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (type == Document.Types.OPFR)
                {
                    var lst = session.CreateSQLQuery($@"select tr.iD, coalesce(c.cnt,0), b.NAME, tr.SUM from PFR_BASIC.OPFR_REGISTER r
                                                              JOIN PFR_BASIC.OPFR_TRANSFER tr ON tr.OPFR_REGISTER_ID = r.ID
                                                              left join pfr_basic.pfrbranch b on b.id = tr.pfrbranch_id

                                                              LEFT JOIN (select count(pp.ID) as cnt, sum(pp.DRAFT_AMOUNT) as sum, l.OPFR_TRANSFER_ID as TID, l.asg_fin_tr_id as PPID
                                                                         from PFR_BASIC.ASG_FIN_TR pp
                                                                         JOIN PFR_BASIC.LINK_PP_OPFRTRANSFER l ON l.asg_fin_tr_id = pp.ID
                                                                           where pp.STATUS_ID<>-1
                                                                        GROUP BY l.OPFR_TRANSFER_ID, l.asg_fin_tr_id) c ON c.TID = tr.ID

                                                              LEFT JOIN (select sum(tr2.SUM) as sum, l.asg_fin_tr_id as ID
                                                                         from PFR_BASIC.OPFR_TRANSFER tr2
                                                                         JOIN PFR_BASIC.LINK_PP_OPFRTRANSFER l ON l.OPFR_TRANSFER_ID = tr2.ID
                                                                           WHERE tr2.STATUS_ID<>-1
                                                                        GROUP BY l.asg_fin_tr_id) tri ON tri.ID = c.PPID


                                                            WHERE (c.cnt is NULL or (c.cnt>0 and c.sum<>tr.SUM) or (c.cnt=1 and c.sum < tr.SUM and tri.sum<>c.sum))
                                                            and r.STATUS_ID<>-1 and tr.STATUS_ID<>-1 and r.ID=:id")
                        .SetParameter("id", registerID)
                        .List<object[]>()
                        .Select(a => new ReqTransferListItem
                            {
                                ID = Convert.ToInt64(a[0]),
                                HasPP = Convert.ToInt32(a[1]) != 0,
                                UKName = (string)a[2],
                                Sum = NormalizePlainSQLDecimal(Convert.ToDecimal(a[3]))
                            })
                        .ToList();

                    return lst;
                }


                var list =
                    session.CreateQuery(
                        @"select new ReqTransferListItem(rt, c.ContractNumber, le.FormalizedName, r.DirectionID, 
                            coalesce((select count(*) from AsgFinTr pp where pp.ReqTransferID = rt.ID AND pp.StatusID <> -1), 0)) 
													from SIRegister r 
													join r.TransferLists st 
													join st.Transfers rt 
													join st.Contract c
													join c.LegalEntity le
													where rt.StatusID <> -1 
                                                        AND ((r.DirectionID in (:directions) and rt.TransferStatus in (:okStatuses)) OR
														    (rt.Sum <> coalesce((select sum(pp.DraftAmount) from AsgFinTr pp where pp.ReqTransferID = rt.ID AND pp.StatusID <> -1), 0)
														    AND rt.TransferStatus not in (:statuses)))
                                                        AND c.TypeID = :type AND r.ID = :id
													")
                        .SetParameterList("statuses", new List<string>
                        {
                            TransferStatusIdentifier.sActSigned,
                            TransferStatusIdentifier.sSPNTransferred,
                            //Исключаем статусы, которые уже не нужно показывать 
                            TransferStatusIdentifier.sBeginState,
                            TransferStatusIdentifier.sDemandGenerated
                            //Исключаем статусы, которые еще не нужно показывать
                        })
                        .SetParameterList("directions", new List<long>
                            {
                                (long) SIRegister.Directions.ToUK,
                                (long) SIRegister.Directions.ToGUK
                            })
                        .SetParameterList("okStatuses",
                            new List<string>
                            {
                                TransferStatusIdentifier.sSPNTransferred,
                                TransferStatusIdentifier.sClaimCompleted
                            })
                        .SetParameter("type", (int)type)
                        .SetParameter("id", registerID)
                        .List<ReqTransferListItem>()
                        .ToList();

                return list;
            }
        }


        public FinregisterListItem GetFinregisterForCommonPPByID(long finregisterID)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@"select new FinregisterListItem(fr, r, acc.Name, adc.Name) 
														from Register r 
														left join r.ApproveDoc ad 
														join r.FinregistersList fr
														join fr.CreditAccount ac
														join fr.DebitAccount ad
														join ac.Contragent acc
														join ad.Contragent adc
														where fr.ID = :id and fr.StatusID <> -1")
                    .SetParameter("id", finregisterID)
                    .SetMaxResults(1)
                    .List<FinregisterListItem>().ToList();
                return list.FirstOrDefault();
            }
        }

        public void DeleteOpfrPPLink(long transferId, long ppId)
        {
            TransactionWrapper(session =>
            {
                session.CreateQuery(@"delete from LinkPPOPFRTransfer l where l.PPID=:ppid and l.TransferID=:trid")
                    .SetParameter("ppid", ppId)
                    .SetParameter("trid", transferId)
                    .ExecuteUpdate();
            });
        }

        public ReqTransferListItem GetReqTransferForCommonPPByID(long reqTransferID, bool isOpfr)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (isOpfr)
                {
                    var list =
                        session.CreateQuery(
                            @"select new ReqTransferListItem(rt.ID, b.Name) 
													                    from OpfrRegister r 
													                    join r.Transfers rt 
                                                                        join rt.Branch b
													                    where rt.ID = :id
													                    ")
                            .SetParameter("id", reqTransferID)
                            .SetMaxResults(1)
                            .List<ReqTransferListItem>().ToList();
                    return list.FirstOrDefault();
                }
                else
                {
                    var list =
                        session.CreateQuery(
                                @"select new ReqTransferListItem(rt, c.ContractNumber, le.FormalizedName, r.DirectionID) 
													from SIRegister r 
													join r.TransferLists st 
													join st.Transfers rt 
													join st.Contract c
													join c.LegalEntity le
													where rt.ID = :id
													")
                            .SetParameter("id", reqTransferID)
                            .SetMaxResults(1)
                            .List<ReqTransferListItem>().ToList();
                    return list.FirstOrDefault();
                }
            }
        }


        private long? GetLegalEntityIdFromFinregister(long finregisterID)
        {
            return SessionWrapper(session =>
            {
                var list = session.CreateQuery(@"select case when trim(lower(r.Kind)) = :dirFromNpf then lec.ID else led.ID end
														from Register r 
														join r.FinregistersList fr
														join fr.CreditAccount ac
														join fr.DebitAccount ad
														join ac.Contragent acc
														join ad.Contragent adc
														join acc.LegalEntities lec
														join adc.LegalEntities led
														where fr.ID = :id and fr.StatusID<>-1 and r.StatusID<>-1")
                    .SetParameter("dirFromNpf", RegisterIdentifier.NPFtoPFR.ToLower())
                    .SetParameter("id", finregisterID)
                    .SetMaxResults(1)
                    .List<long>().ToList();
                return list.Count == 0 ? null : (long?)list.First();
            });
        }

        private List<long?> GetLegalEntityAndContractIdFromReqTransfer(long reqTransferID)
        {
            return SessionWrapper(session =>
            {
                var result = session.CreateQuery(@"select c.ID, le.ID
													from SIRegister r 
													join r.TransferLists st 
													join st.Transfers rt 
													join st.Contract c
													join c.LegalEntity le
													where rt.StatusID <> -1 and rt.ID = :id
													")
                    .SetParameter("id", reqTransferID)
                    .SetMaxResults(1)
                    .List<object[]>().FirstOrDefault();
                return result == null ? new List<long?> { null, null } : new List<long?> { (long?)result[0], (long?)result[1] };
            });
        }

        public long SaveCommonPP(AsgFinTr payment)
        {
            decimal? sum = 0;

            if (payment.ReqTransferID.HasValue)
            {
                var comPP = GetListByProperty<AsgFinTr>("ReqTransferID", payment.ReqTransferID.Value).Cast<AsgFinTr>()
                    .Where(pp => !pp.IsDeleted()).ToList(); //Не забываем отбрасывть удалённые
                                                            //суммируем все, кроме сохраняемого, а затем прибавляем к сумме сохраняемое п/п иначе получится неверная сумма всех п/п для проверки условия перевода статуса
                                                            //при правке п/п
                sum = comPP.Where(a => a.ID != payment.ID).Sum(p => p.DraftAmount);
                sum += payment.DraftAmount;
                //if (comPP.All(x => x.ID != payment.ID)) sum += payment.DraftAmount;
            }
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        if (payment.FinregisterID != null) // && payment.SectionElID == (long) AsgFinTr.Sections.NPF
                            payment.InnLegalEntityID = GetLegalEntityIdFromFinregister(payment.FinregisterID.Value);
                        else if (payment.ReqTransferID != null)
                        {
                            var result = GetLegalEntityAndContractIdFromReqTransfer(payment.ReqTransferID.Value);
                            payment.InnLegalEntityID = result[1];
                            payment.InnContractID = result[0];
                        }
                        else
                        {
                            payment.InnLegalEntityID = null;
                            payment.InnContractID = null;
                        }                    
                        SaveEntity(payment, session);
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogException(ex);
                        throw ex;
                    }
                    session.Flush();

                    //Обновляем статус связаного перечисления
                    if (payment.ReqTransferID.HasValue)
                    {
                        payment.ContragentTypeID = (long) AsgFinTr.ContragentTypeEnum.UK;
                        var tr = session.Get<ReqTransferHib>(payment.ReqTransferID.Value);

                        if (sum == tr.Sum && !TransferStatusIdentifier.IsStatusSPNTransfered(tr.TransferStatus) &&
                            !TransferStatusIdentifier.IsStatusActSigned(tr.TransferStatus))
                        {
                            tr.TransferStatus = TransferStatusIdentifier.sSPNTransferred;
                            tr.SPNDebitDate = payment.SPNDate;
                            SaveEntity(tr, session);
                            session.Flush();
                        }
                    }
                    //Обновляем статус связаного финреестра
                    else if (payment.FinregisterID.HasValue)
                    {                        
                        var fr = session.Get<FinregisterHib>(payment.FinregisterID.Value);
                        var sumFr = fr.DraftsList.Sum(p => p.DraftAmount);
                        if (fr.Count == sumFr)
                        {
                            //Пока никаких специфических действий не требуется
                        }
                    }

                    switch (payment.SectionElID)
                    {
                        case (long)AsgFinTr.Sections.NPF:
                            payment.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.NPF;
                            break;
                        case (long)AsgFinTr.Sections.SI:
                        case (long)AsgFinTr.Sections.VR:
                            payment.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.UK;
                            break;
                        case (long)AsgFinTr.Sections.BackOffice:
                            payment.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.OPFR;
                            break;
                    }

                    SaveEntity(payment, session);

                    try
                    {
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogException(ex);
                        throw ex;
                    }
                    return payment.ID;
                }
            }
        }

        public List<AsgFinTr> GetUnlinkedCommonPPList(AsgFinTr.Sections section, AsgFinTr.Directions direction)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                switch (section)
                {
                    case AsgFinTr.Sections.BackOffice:
                        var idList = session.CreateSQLQuery($@"select distinct pp.ID from PFR_BASIC.ASG_FIN_TR pp
                                                               
                                                              LEFT JOIN (select count(tr.ID) as cnt, sum(tr.SUM) as sum, l.OPFR_TRANSFER_ID as TID, l.asg_fin_tr_id as PPID
                                                                         from PFR_BASIC.OPFR_TRANSFER tr
                                                                         JOIN PFR_BASIC.LINK_PP_OPFRTRANSFER l ON l.OPFR_TRANSFER_ID = tr.ID
                                                                           where tr.STATUS_ID<>-1
                                                                        GROUP BY l.OPFR_TRANSFER_ID, l.asg_fin_tr_id) c ON c.PPID = pp.ID

                                                              LEFT JOIN (select sum(pp2.DRAFT_AMOUNT) as sum, l.OPFR_TRANSFER_ID as ID
                                                                         from PFR_BASIC.ASG_FIN_TR pp2
                                                                         JOIN PFR_BASIC.LINK_PP_OPFRTRANSFER l ON l.asg_fin_tr_id = pp2.ID
                                                                           WHERE pp2.STATUS_ID<>-1
                                                                        GROUP BY l.OPFR_TRANSFER_ID) tri ON tri.ID = c.TID


                                                            WHERE (c.cnt is NULL or (c.cnt>0 and c.sum<>pp.DRAFT_AMOUNT) or (c.cnt=1 and c.sum < pp.DRAFT_AMOUNT and tri.sum<>c.sum))
                                                            and pp.STATUS_ID<>-1 and pp.link_el_id = :link AND pp.REQ_TR_ID is null AND pp.FR_ID is null AND pp.direction_el_id = :direction
                                                            and (pp.contragent_type in (:clist))")
                            .SetParameter("link", (long)AsgFinTr.Links.NoLink)
                            .SetParameter("direction", (long)direction)
                            .SetParameterList("clist", new [] { (long)AsgFinTr.ContragentTypeEnum.OPFR})
                            .List<long>()
                            .ToList();
                        return idList.Count > 0 ? session.CreateQuery("from AsgFinTr pp where pp.ID in (:list)")
                            .SetParameterList("list", idList)
                            .List<AsgFinTr>()
                            .ToList() : new List<AsgFinTr>();
                        break;
                    default:
                        var list = session.CreateQuery(@"SELECT pp FROM AsgFinTr pp 
                                                    LEFT JOIN pp.Portfolio pf
												WHERE 
													pp.LinkElID = :link AND pp.ReqTransferID is null AND pp.FinregisterID is null AND pp.DirectionElID = :direction
                                                    AND (pf is null OR pf.StatusID > -1 AND pf.StatusID < 2)
													AND pp.StatusID <> -1 and pp.ContragentTypeID<>:cid")
                            .SetParameter("link", (long)AsgFinTr.Links.NoLink)
                            .SetParameter("direction", (long)direction)
                            .SetParameter("cid", (long)AsgFinTr.ContragentTypeEnum.OPFR)
                            .List<AsgFinTr>();
                        return list.ToList();
                }
            }
        }

        public List<Finregister> GetFinregListForCommonPPSplit(long id, bool isReturn)
        {
            return SessionWrapper(session =>
            {
                return session.CreateQuery(@"FROM Finregister fr 
                                             WHERE fr.Status = :status and fr.StatusID<>-1 and fr.RegisterID=:id and (select count(*) from AsgFinTr pp where pp.FinregisterID=fr.ID and pp.StatusID<>-1)=0")
                    .SetParameter("id", id)
                    .SetParameter("status", isReturn ? RegisterIdentifier.FinregisterStatuses.Issued : RegisterIdentifier.FinregisterStatuses.Created)
                    .List<Finregister>().ToList();
            });
        }

        public List<ReqTransfer> GetReqTransferListForCommonPPSplit(long id)
        {
            return SessionWrapper(session =>
            {
                return session.CreateQuery(@"select rq FROM ReqTransfer rq 
                                                 join rq.TransferList tl
                                                 join tl.TransferRegister r
                                                 WHERE rq.StatusID<>-1 and r.StatusID<>-1 and tl.StatusID<>-1 and r.ID=:id and 
                                                    rq.TransferStatus in (:status) and
                                                    (select count(*) from AsgFinTr pp where pp.ReqTransferID=rq.ID and pp.StatusID<>-1)=0")
                    .SetParameter("id", id)
                    .SetParameterList("status", new[] { TransferStatusIdentifier.sDemandSentInUK, TransferStatusIdentifier.sClaimCompleted })
                    .List<ReqTransfer>().ToList();
            });
        }

        public List<ReqTransferPartialListItem> GetReqTransferPartialListForCommonPPSplit(long? regid)
        {
            // return new List<ReqTransferPartialListItem>();
            return SessionWrapper(session =>
            {
                return session.CreateSQLQuery(string.Format(@"select rq.ID as rid, c.LE_ID, le.FORMALIZEDNAME, CAST((rq.SUM-pp.summ) as DECIMAL(19,4)) as sum, c.ID as cid
                                                from {0}.REQ_TRANSFER rq
                                                join {0}.SI_TRANSFER tl on tl.ID = rq.SI_TR_ID
                                                join {0}.CONTRACT c on c.ID = tl.CONTR_ID
                                                join {0}.LEGALENTITY le on le.ID = c.LE_ID
                                                join {0}.SI_REGISTER r on r.ID = tl.SI_REG_ID
                                                join (select count(1) as cnt, sum(coalesce(DRAFT_AMOUNT, 0)) as summ, DIRECTION_EL_ID as dir, REQ_TR_ID as rq_id 
		                                                from {0}.ASG_FIN_TR
		                                                where STATUS_ID<>-1
		                                                GROUP BY REQ_TR_ID, DIRECTION_EL_ID) pp ON pp.rq_id=rq.ID
                                                where rq.STATUS_ID<>-1 and r.STATUS_ID<>-1 and tl.STATUS_ID<>-1 
                                                    and r.ID = :id
	                                                and pp.cnt = 1 and rq.SUM - pp.summ > 0 and pp.dir = :dir ", DATA_SCHEME))
                    .SetParameter("id", regid)
                    .SetParameter("dir", (long)AsgFinTr.Directions.ToPFR)
                    .List<object[]>().Select(a => new ReqTransferPartialListItem
                    {
                        ReqTransferID = (long)a[0],
                        LegalEntityID = (long)a[1],
                        LegalEntityName = (string)a[2],
                        SumDiff = (decimal)a[3] / 10000,
                        ContractID = (long)a[4]
                    }).ToList();
            });
        }



#endregion CommonPP





        public List<ReqTransferWithStatsListItem> GetReqTransfersWithStatsFromUkPlanId(long ukPlanId)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        string.Format(@"SELECT new ReqTransferWithStatsListItem(req, op.Name, req.TransferStatus)  FROM ReqTransfer req
                                                left join req.TransferList tr
                                                left join tr.TransferRegister r
                                                left join r.Operation op
												WHERE req.ID in (select p.Transfer2ID from SIUKPlan p where p.ID = {0} and p.Transfer2ID is not null) or req.ID in (select p.TransferID from SIUKPlan p where p.ID = {0} and p.TransferID is not null) 
                                                    and req.StatusID <> -1", ukPlanId))
                        .List<ReqTransferWithStatsListItem>();
                return list.ToList();
            }
        }

        public List<PfrBranchReportInsuredPersonListItem> GetPfrReportInsuredPersonList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                // t.ID <> 1 - Исключаем столбец "Всего принято заявлений" (при группировке по региону будет суммироваться)
                var list =
                    session.CreateQuery(@"SELECT ip, t, r, b, f
                                        FROM PfrBranchReportInsuredPerson ip
                                            LEFT JOIN ip.Type t
                                            LEFT JOIN ip.Report r
                                            LEFT JOIN r.PfrBranch b
                                            LEFT JOIN b.FEDO f
                                        WHERE t.ID <> 1 AND r.StatusID <> -1")
                        .List<object[]>();
                var results = new List<PfrBranchReportInsuredPersonListItem>();
                list.ForEach(a => results.Add(new PfrBranchReportInsuredPersonListItem((PfrBranchReportInsuredPerson)a[0], (PfrBranchReportInsuredPersonType)a[1], (PfrBranchReport)a[2], (PFRBranch)a[3], (FEDO)a[4])));

                return results;
            }
        }

        public List<PfrBranchReportNpfNoticeListItem> GetPfrReportNpfNoticeList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(@"SELECT nn, le, r, b, f
                                        FROM PfrBranchReportNpfNotice nn
                                            LEFT JOIN nn.Npf le
                                            LEFT JOIN nn.Report r
                                            LEFT JOIN r.PfrBranch b
                                            LEFT JOIN b.FEDO f
                                        WHERE r.StatusID <> -1")
                        .List<object[]>();
                var results = new List<PfrBranchReportNpfNoticeListItem>();
                list.ForEach(a => results.Add(new PfrBranchReportNpfNoticeListItem((PfrBranchReportNpfNotice)a[0], (LegalEntity)a[1], (PfrBranchReport)a[2], (PFRBranch)a[3], (FEDO)a[4])));

                return results;
            }
        }

        public List<PfrBranchReportAssigneePaymentListItem> GetPfrReportAssigneePaymentList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(@"SELECT ap, r, b, f
                                        FROM PfrBranchReportAssigneePayment ap
                                            LEFT JOIN ap.Report r
                                            LEFT JOIN r.PfrBranch b
                                            LEFT JOIN b.FEDO f
                                        WHERE r.StatusID <> -1")
                        .List<object[]>();
                var results = new List<PfrBranchReportAssigneePaymentListItem>();
                list.ForEach(a => results.Add(new PfrBranchReportAssigneePaymentListItem((PfrBranchReportAssigneePayment)a[0], (PfrBranchReport)a[1], (PFRBranch)a[2], (FEDO)a[3])));

                return results;
            }
        }

        public List<PfrBranchReportCashExpendituresListItem> GetPfrReportCashExpendituresList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(@"SELECT ce, r, b, f
                                        FROM PfrBranchReportCashExpenditures ce
                                            LEFT JOIN ce.Report r
                                            LEFT JOIN r.PfrBranch b
                                            LEFT JOIN b.FEDO f
                                        WHERE r.StatusID <> -1")
                        .List<object[]>();
                var results = new List<PfrBranchReportCashExpendituresListItem>();
                list.ForEach(a => results.Add(new PfrBranchReportCashExpendituresListItem((PfrBranchReportCashExpenditures)a[0], (PfrBranchReport)a[1], (PFRBranch)a[2], (FEDO)a[3])));

                return results;
            }
        }

        public List<PfrBranchReportApplication741ListItem> GetPfrReportApplication741List()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(@"SELECT ap, r, b, f
                                        FROM PfrBranchReportApplication741 ap
                                            LEFT JOIN ap.Report r
                                            LEFT JOIN r.PfrBranch b
                                            LEFT JOIN b.FEDO f
                                        WHERE r.StatusID <> -1")
                        .List<object[]>();
                var results = new List<PfrBranchReportApplication741ListItem>();
                list.ForEach(a => results.Add(new PfrBranchReportApplication741ListItem((PfrBranchReportApplication741)a[0], (PfrBranchReport)a[1], (PFRBranch)a[2], (FEDO)a[3])));

                return results;
            }
        }

        public List<PfrBranchReportDeliveryCostListItem> GetPfrReportDeliveryCostList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(@"SELECT dc, r, b, f
                                        FROM PfrBranchReportDeliveryCost dc
                                            LEFT JOIN dc.Report r
                                            LEFT JOIN r.PfrBranch b
                                            LEFT JOIN b.FEDO f
                                        WHERE r.StatusID <> -1")
                        .List<object[]>();
                var results = new List<PfrBranchReportDeliveryCostListItem>();
                list.ForEach(a => results.Add(new PfrBranchReportDeliveryCostListItem((PfrBranchReportDeliveryCost)a[0], (PfrBranchReport)a[1], (PFRBranch)a[2], (FEDO)a[3])));

                return results;
            }
        }

        public List<CBPaymentDetailListItem> GetCBPaymentDetailList()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(@"SELECT item, p, pd
                                        FROM CBPaymentDetail item
                                            LEFT JOIN item.Portfolio p
                                            LEFT JOIN item.PaymentDetail pd
                                        WHERE item.StatusID <> -1")
                        .List<object[]>();
                var results = new List<CBPaymentDetailListItem>();
                list.ForEach(a => results.Add(new CBPaymentDetailListItem((CBPaymentDetail)a[0], (Portfolio)a[1], (PaymentDetail)a[2])));

                return results;
            }
        }


#region Analyze

        //public List<ActivesKind> GetActivesKind()
        //{
        //    using (var session = DataAccessSystem.OpenSession())
        //    {
        //        var list =
        //            session.CreateQuery(@"SELECT item, p, pd
        //                                FROM CBPaymentDetail item
        //                                    LEFT JOIN item.Portfolio p
        //                                    LEFT JOIN item.PaymentDetail pd
        //                                WHERE item.StatusID <> -1")
        //                .List<object[]>();
        //        var results = new List<CBPaymentDetailListItem>();
        //        list.ForEach(a => results.Add(new CBPaymentDetailListItem((CBPaymentDetail)a[0], (Portfolio)a[1], (PaymentDetail)a[2])));

        //        return results;
        //    }
        //}

#endregion






#region ПРОЧИЕ ОПТИМИЗАЦИИ

        public bool ImportOwnCapital(DateTime reportDate, IList<OwnCapitalHistory> items, bool forceUpdate)
        {
            return TransactionWrapper(session =>
            {
                var history = GetListByPropertyConditions<OwnCapitalHistory>(ListPropertyCondition.Equal("ReportDate", reportDate))
                    .Cast<OwnCapitalHistory>().ToList();
               /* var banks =
                    GetListByPropertyCondition(typeof(LegalEntity).FullName, "ID",
                            history.Select(h => h.LegalEntityID).Cast<object>().ToArray(), "in")
                        .Cast<LegalEntityHib>()
                        .ToList();*/

                if (history.Any())
                {
                    foreach (var item in history)
                    {
                        session.Delete(item);
                    }
                }

               /* if (banks.Any())
                {
                    banks.ForEach(b =>
                    {
                        var cap =
                            items.Where(
                                    i =>
                                        i.LegalEntityID == b.ID && b.MoneyDate.HasValue &&
                                        i.ReportDate >= b.MoneyDate.Value)
                                .OrderByDescending(c => c.ReportDate)
                                .FirstOrDefault();
                        if (cap != null)
                        {
                            b.Money = cap.Summ;
                            b.MoneyDate = cap.ReportDate;
                            SaveEntity(b, session);
                        }
                    });
                }*/

                items?.ForEach(i =>
                {
                    SaveEntity(i, session);
                });

                if (items != null)
                {
                    var banks2 =
                        GetListByPropertyCondition(typeof(LegalEntity).FullName, "ID",
                                items?.Select(h => h.LegalEntityID).Cast<object>().ToArray(), "in")
                            .Cast<LegalEntityHib>()
                            .ToList();
                    banks2.ForEach(b => FillInOwnCapital(b, session));
                }
                return true;
            });

        }

        /// <summary>
        /// Пересчет собственного капитала банка
        /// </summary>
        /// <param name="bankId"></param>
        public object[] FillInOwnCapital(long bankId)
        {
            return TransactionWrapper(session =>
            {
                var bank = session.CreateQuery(@"from LegalEntity where ID=:value")
                    .SetParameter("value", bankId).UniqueResult<LegalEntity>();
                return FillInOwnCapital(bank, session);
            });
        }


        /// <summary>
        /// Пересчет собственного капитала банка
        /// </summary>
        /// <param name="bank">Банк</param>
        private object[] FillInOwnCapital(LegalEntity bank, ISession session)
        {
            var cap = GetListByProperty<OwnCapitalHistory>("LegalEntityID", bank.ID, session)
                .Cast<OwnCapitalHistory>()
                .OrderByDescending(c => c.ReportDate)
                .FirstOrDefault();
            if (cap != null)
            {
                if (bank.Money != cap.Summ || bank.MoneyDate != cap.ReportDate)
                {
                    bank.Money = cap.Summ;
                    bank.MoneyDate = cap.ReportDate;
                    SaveEntity(bank, session);
                }
            }
            else
            {
                if (bank.Money != null || bank.MoneyDate != null)
                {
                    bank.Money = null;
                    bank.MoneyDate = null;
                    SaveEntity(bank, session);
                }
            }
            return new object[] { bank.Money, bank.MoneyDate};
        }

        /// <summary>
        /// Обновить данные по дате и номеру заявки для Оферт по ID аукциона
        /// </summary>
        /// <param name="id">ID аукциона</param>
        /// <param name="claimNumber">Номер заявки</param>
        /// <param name="claimDate">Дата заявки</param>
        public void UpdateOfferClaimData(long id, string claimNumber, DateTime? claimDate)
        {
            SessionWrapper(session =>
            {
                session.CreateQuery(@"update DepClaimOffer d set d.ClaimNumber=:claimNumber, d.ClaimDate=:claimDate where d.AuctionID=:auctionId")
                    .SetParameter("claimNumber", claimNumber)
                    .SetParameter("claimDate", claimDate)
                    .SetParameter("auctionId", id)
                    .ExecuteUpdate();
            });
        }

        /// <summary>
        /// Выборка назначения платежа для отчетов
        /// </summary>
        /// <param name="appPart">Раздел ПТК</param>
        /// <param name="direction">Направление</param>
        /// <param name="opId">Идентификатор операции</param>
        /// <returns></returns>
		public PaymentAssignment GetPayAssForReport(long appPart, long direction, long? opId = null)
        {
            return TransactionWrapper(session =>
                session.CreateQuery($@"from PaymentAssignment p where p.PartID=:partId and p.DirectionID=:dirId {
                            (opId.HasValue ? (" and (p.OperationContentID=" + opId.Value + " or p.OperationContentID=0) ") : "")
                        } order by p.OperationContentID desc")
                    .SetParameter("partId", appPart)
                    .SetParameter("dirId", direction)
                    .SetMaxResults(1)
                    .UniqueResult<PaymentAssignment>());
        }

        public long SaveMonthTransferWizardResults(SIRegister register, List<MonthTransferWizardResultListItem> contracts)
        {
            return TransactionWrapper(session =>
            {
                register.ID = SaveEntity(register);

                contracts.ForEach(cr =>
                {
                    if (cr.FactSum == null || !(cr.FactSum > 0)) return;

                    var transferList = new SITransfer
                    {
                        ContractID = cr.ContractID,
                        TransferRegisterID = register.ID,
                        InsuredPersonsCount = cr.QuantityZL,
                        StatusID = 1
                    };
                    transferList.ID = SaveEntity(transferList, session);

                    var reqTransfer = new ReqTransfer
                    {
                        TransferListID = transferList.ID,
                        TransferStatus = TransferStatusIdentifier.sBeginState,
                        Date = DateTime.Today,
                        Sum = cr.FactSum.GetValueOrDefault(),
                        InvestmentIncome = cr.InvestmentIncome,
                        ZLMoveCount = cr.QuantityZL,
                        StatusID = 1
                    };
                    reqTransfer.ID = SaveEntity(reqTransfer, session);

                    cr.UKPlan.SetTransferId(reqTransfer.ID);
                    cr.UKPlan.AddFactSum(cr.FactSum);
                    SaveEntity(cr.UKPlan, session);
                });

                return register.ID;
            });
        }


        public bool IsUKPlansHasReqTransfers(List<long> transferIdList)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var idStr = GenerateStringFromList(transferIdList);
                var count =
                    session.CreateQuery(
                            $"select count(*) from SIUKPlan p where p.TransferListID in ({idStr}) and p.StatusID <> -1 and (p.TransferID is not null or p.Transfer2ID is not null)")
                        .UniqueResult<long>();
                return count > 0;
            }
        }

        public void UpdateReqTrAndContractByActSigned(long contractId, bool unlinked)
        {
            TransactionWrapper(session => UpdateReqTrAndContractByActSigned(contractId, session, unlinked == false? null : (bool?)true));
        }

        /// <summary>
        /// Пересчитываем сумму на начало для каждого перечисления. Перечисление уже добавлено/изменено/удалено в базе
        /// получение списка всех перечислений по договору	
        /// </summary>
        /// <param name="contractId">ID контракта</param>
        /// <param name="session">Сессия</param>
        private void UpdateReqTrAndContractByActSigned(long contractId, ISession session, bool? unlinked= null)
		{
			var query = session.CreateQuery($@"SELECT rt 
						FROM ReqTransfer rt 
						JOIN fetch rt.TransferList tl 
						JOIN tl.Contract c 
						WHERE 
							rt.StatusID > -1 AND tl.StatusID >-1 
							AND LOWER(TRIM(rt.TransferStatus)) =:transferStatus 
							AND c.ID = :contractID {(unlinked == null ? "" : " and rt.Unlinked=1 ")}")
					   .SetParameter("transferStatus", TransferStatusIdentifier.sActSigned.Trim().ToLower())
					   .SetParameter("contractID", contractId);
			var reqTransferHibs =
				query.List<ReqTransferHib>().OrderBy(tr => tr.TransferActDate).ThenBy(tr => tr.ID).ToList();

			decimal start = 0;
			foreach (var rt in reqTransferHibs)
			{
				//Сохраняем изменения в базу, только если нужно
				if (rt.SumBeforeOperation != start)
				{
					rt.SumBeforeOperation = start;
                    session.CreateQuery("update ReqTransfer rt set rt.SumBeforeOperation=:value where rt.ID=:id")
                        .SetParameter("value", rt.SumBeforeOperation)
                        .SetParameter("id", rt.ID)
                        .ExecuteUpdate();
				}
				start += GetUKDiff(rt);
			}

			session.CreateSQLQuery($"UPDATE {DATA_SCHEME}.CONTRACT SET OPERATESUMM = :sum WHERE ID = :id")
				.SetParameter("sum", start)
				.SetParameter("id", contractId)
				.ExecuteUpdate();
		}

		private void ClearLinkReqTrForAsgFinTr(string idStr, ISession session)
		{
			session.CreateSQLQuery($"update {DATA_SCHEME}.{TABLE_ASG_FIN_TR} set REQ_TR_ID = null, LINKEDREGISTER_ID = null, LINK_EL_ID = 100001 where REQ_TR_ID in ({idStr})")
								.ExecuteUpdate();
		}

		public List<KDoc> GetKDocsForPeriod(long periodId)
		{
			return SessionWrapper(session => session.CreateQuery("from KDoc k where k.ReportPeriodID = :reportId")
				.SetParameter("reportId", periodId)
				.List<KDoc>()
				.ToList());
		}


#endregion


#region ВЫБОРКА ДАННЫХ

        public LoginMessageItem GetCBReportForApproveMessage()
        {
            return SessionWrapper(session =>
            {
                var statusInProcess = (int) BOReportForm1.ReportStatus.InProcess;
                var login = DomainUser?.Trim().ToLower();
                var list = session.CreateQuery(
                    @"from BOReportForm1 br where br.ReportType=0 and ((br.IsApproved1=:status and trim(lower(br.ApproverLogin1))=:login) or (br.IsApproved2=:status and trim(lower(br.ApproverLogin2))=:login)) and br.Status=1")
                    .SetParameter("status", statusInProcess)
                    .SetParameter("login", login)
                    .List<BOReportForm1>();
                var message = new StringBuilder();
                var list2 = list.OrderBy(a=> a.Year).ThenBy(a=> a.Quartal);
                list2.ForEach(a =>
                {
                    if (a.OKUD == BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Two))
                    {
                        bool insert = false;
                        if (!string.IsNullOrEmpty(a.ApproverLogin1) && a.IsApproved1 == statusInProcess && string.Equals(a.ApproverLogin1, login, StringComparison.InvariantCultureIgnoreCase))
                        {
                            message.Append(
                                $"Пожалуйста согласуйте 2 форму отчета в Банк России (Перечисления УК) за {DateTools.GetQuarterRomeName(a.Quartal)} квартал {a.Year} года!");
                            insert = true;
                        }
                        if (!string.IsNullOrEmpty(a.ApproverLogin2) && a.IsApproved2 == statusInProcess && string.Equals(a.ApproverLogin2, login, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (insert) message.Append("\n");
                            message.Append(
                                $"Пожалуйста согласуйте 2 форму отчета в Банк России (Перечисления НПФ) за {DateTools.GetQuarterRomeName(a.Quartal)} квартал {a.Year} года!");
                        }
                    }
                    else
                        message.Append(
                            $"Пожалуйста согласуйте {(a.OKUD == BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.One) ? "1" : "3")} форму отчета в Банк России за {DateTools.GetQuarterRomeName(a.Quartal)} квартал {a.Year} года!");
                    message.Append("\n");
                });


                return list.Count > 0 ? new LoginMessageItem { Title = "Отчет в БР", Message = message.ToString(), refID = list2.FirstOrDefault()?.ID ?? 0 } : null;
            });
        }

        public void RemoveOpfrTransferLink(long ppId, long transferId)
        {
            TransactionWrapper(session =>
            {
                session.CreateQuery(@"delete from LinkPPOPFRTransfer where PPID=:ppid and TransferID=:trid")
                    .SetParameter("ppid", ppId)
                    .SetParameter("trid", transferId)
                    .ExecuteUpdate();
            });
        }

        public OpfrTransferListItem GetOpfrTransferListItem(long transferId)
        {
            return SessionWrapper(session => session.CreateQuery(@"select new OpfrTransferListItem(tr, c.Name, b.Name, r.RegNum, r.RegDate)
                                                from OpfrTransferHib tr
                                                join tr.Register r
                                                left join tr.Cost c
                                                left join tr.Branch b
                                                where tr.StatusID<>-1 and tr.ID=:id")
                .SetParameter("id", transferId)
                .UniqueResult<OpfrTransferListItem>());
        }

        public IList<OpfrTransferListItem> GetOpfrTransferListItemsByPP(long id)
        {
            return SessionWrapper(session =>
            {

                var ids = session.CreateSQLQuery(@"select l.opfr_transfer_id from pfr_basic.link_pp_opfrtransfer l
                                         where l.asg_fin_tr_id = :id")
                    .SetParameter("id", id)
                    .List<long>();

                var result = session.CreateQuery(@"select new OpfrTransferListItem(tr, c.Name, b.Name, r.RegNum, r.RegDate)
                                                from OpfrTransfer tr
                                                left join tr.Cost c
                                                left join tr.Branch b
                                                left join tr.Register r
                                                where tr.StatusID<>-1 and tr.ID in (:idlist)")
                    .SetParameterList("idlist", ids.ToArray())
                    .List<OpfrTransferListItem>();


                return result;
            });
        }

        public IList<OpfrTransferListItem> GetOpfrTransferListItems(long registerId)
        {
            return SessionWrapper(session =>
            {
                var result = session.CreateQuery(@"select new OpfrTransferListItem(tr, c.Name, b.Name, r.RegNum, r.RegDate)
                                                from OpfrTransfer tr
                                                left join tr.Cost c
                                                left join tr.Branch b
                                                left join tr.Register r
                                                where tr.StatusID<>-1 and tr.OpfrRegisterID=:id")
                    .SetParameter("id", registerId)
                    .List<OpfrTransferListItem>();

                var ids = result.Select(a => a.ID);

                var col = session.CreateSQLQuery(@"select l.opfr_transfer_id, pp.draft_amount from pfr_basic.link_pp_opfrtransfer l
                                         inner join pfr_basic.asg_fin_tr pp ON pp.ID = l.asg_fin_tr_id
                                         where l.opfr_transfer_id in (:list)")
                    .SetParameterList("list", ids.ToArray())
                    .List<object[]>().Select(a => new KeyValuePair<long, decimal?>((long) a[0], FixDecimal((decimal) a[1], 4)));

                col.GroupBy(pair => pair.Key).ForEach(g =>
                {
                    var tr = result.FirstOrDefault(a => a.ID == g.Key);
                    if (tr != null)
                        tr.PPSum = g.Sum(a => a.Value ?? 0);
                });

                return result;
            });
        }

        public long GetOpfrRegisterListItemCount(DateTime? from, DateTime? to)
        {
            from = from ?? DateTime.MinValue.Date;
            to = to ?? DateTime.MaxValue.Date;
            return SessionWrapper(session =>
            {
                return Convert.ToInt64(session.CreateSQLQuery(@"select count(*) " + OpfrRegisterQueryString)
                                                .SetParameter("from", from.Value)
                                                .SetParameter("to", to.Value)
                                                .UniqueResult<object>());
            });
        }

        private const string OpfrRegisterQueryString = @"from pfr_basic.opfr_register r
                                                left join pfr_basic.opfr_transfer tr on tr.opfr_register_id = r.id and tr.status_id<>-1
                                                left join pfr_basic.opfr_costs c on c.id = tr.opfr_costs_id
                                                left join pfr_basic.pfrbranch b on b.id = tr.pfrbranch_id
                                                left join (select el.ID as id, el.name as KindName from pfr_basic.element el) i1 on i1.id = r.kind_id
                                                where r.status_id<>-1 and r.regdate >= :from and r.regdate <= :to";

        public List<OpfrRegisterListItem> GetOpfrRegisterListItemPage(int index, DateTime? from, DateTime? to)
        {
            from = from ?? DateTime.MinValue.Date;
            to = to ?? DateTime.MaxValue.Date;
            return SessionWrapper(session =>
            {
                var result =  session.CreateSQLQuery(@"select r.ID as rid, tr.ID as trid, i1.KindName, r.regnum, r.regdate, b.name as bname, r.year_id, r.month_id, c.name as cname, CAST(tr.sum as DECIMAL(19,4)), r.tranchenum, r.tranchedate
                                                " + OpfrRegisterQueryString)
                                                .SetParameter("from", from.Value)
                                                .SetParameter("to", to.Value)
                                                .List<object[]>().Select(SelectOpfrRegisterListItem).ToList();
                if (result.Any())
                {
                    var ids = result.Select(a => a.TransferID);

                    var col = session.CreateSQLQuery(@"select l.opfr_transfer_id, pp.draft_amount from pfr_basic.link_pp_opfrtransfer l
                                         inner join pfr_basic.asg_fin_tr pp ON pp.ID = l.asg_fin_tr_id
                                         where l.opfr_transfer_id in (:list)")
                        .SetParameterList("list", ids.ToArray())
                        .List<object[]>().Select(a => new KeyValuePair<long, decimal?>((long) a[0], FixDecimal((decimal) a[1], 4)));

                    col.GroupBy(pair => pair.Key).ForEach(g =>
                    {
                        var tr = result.FirstOrDefault(a => a.TransferID == g.Key);
                        if (tr != null)
                            tr.PPSum = g.Sum(a => a.Value ?? 0);
                    });
                }
                return result;
            });
        }

        public OpfrRegisterListItem GetOpfrRegisterListItem(long id)
        {
            return SessionWrapper(session =>
            {
                return session.CreateSQLQuery(@"select r.ID as rid, tr.ID as trid, i1.KindName, r.regnum, r.regdate, b.name as bname, r.year_id, r.month_id, c.name as cname, CAST(tr.sum as DECIMAL(19,4)), r.tranchenum, r.tranchedate
                                                " + OpfrRegisterQueryString + " and r.ID=:id")
                                                .SetParameter("from", DateTime.MinValue.Date)
                                                .SetParameter("to", DateTime.MaxValue.Date)
                                                .SetParameter("id", id)
                                                .List<object[]>().Select(SelectOpfrRegisterListItem).ToList().FirstOrDefault();
            });
        }

        public List<OpfrRegisterListItem> GetOpfrRegisterListItems(long id)
        {
            return SessionWrapper(session =>
            {
                var result = session.CreateSQLQuery(@"select r.ID as rid, tr.ID as trid, i1.KindName, r.regnum, r.regdate, b.name as bname, r.year_id, r.month_id, c.name as cname, tr.sum, r.tranchenum, r.tranchedate
                                                " + OpfrRegisterQueryString + " and r.ID=:id")
                                                .SetParameter("from", DateTime.MinValue.Date)
                                                .SetParameter("to", DateTime.MaxValue.Date)
                                                .SetParameter("id", id)
                                                .List<object[]>().Select(SelectOpfrRegisterListItem).ToList();

                if (result.Any())
                {
                    var ids = result.Select(a => a.TransferID).ToList();
                    /*var nulled = ids.Where(a => !a.HasValue);
                    var nulledList = new List<OpfrRegisterListItem>();
                    if (nulled.Any())
                    {
                        result.Where(a=> nulled.Contains(a.ID)).ForEach(a=> nulledList.Add(a));
                    }
                    */
                    ids.RemoveAll(a => !a.HasValue);
                    if (ids.Count == 0) return result;

                    var col = session.CreateSQLQuery(@"select l.opfr_transfer_id, pp.draft_amount from pfr_basic.link_pp_opfrtransfer l
                                         inner join pfr_basic.asg_fin_tr pp ON pp.ID = l.asg_fin_tr_id
                                         where l.opfr_transfer_id in (:list)")
                        .SetParameterList("list", ids.ToArray())
                        .List<object[]>().Select(a => new KeyValuePair<long, decimal?>((long) a[0], FixDecimal((decimal) a[1], 4)));

                    col.GroupBy(pair => pair.Key).ForEach(g =>
                    {
                        var tr = result.FirstOrDefault(a => a.TransferID == g.Key);
                        if (tr != null)
                            tr.PPSum = g.Sum(a => a.Value ?? 0);
                    });
                }
                return result;
            });
        }


        public OpfrRegisterListItem GetOpfrRegisterListItemByTransferID(long id)
        {
            return SessionWrapper(session =>
            {
                var result = session.CreateSQLQuery(@"select r.ID as rid, tr.ID as trid, i1.KindName, r.regnum, r.regdate, b.name as bname, r.year_id, r.month_id, c.name as cname, tr.sum, r.tranchenum, r.tranchedate
                                                " + OpfrRegisterQueryString + " and tr.ID=:id")
                                                .SetParameter("from", DateTime.MinValue.Date)
                                                .SetParameter("to", DateTime.MaxValue.Date)
                                                .SetParameter("id", id)
                                                .List<object[]>().Select(SelectOpfrRegisterListItem).ToList().FirstOrDefault();
                if (result != null)
                {

                    var col = session.CreateSQLQuery(@"select l.opfr_transfer_id, pp.draft_amount from pfr_basic.link_pp_opfrtransfer l
                                         inner join pfr_basic.asg_fin_tr pp ON pp.ID = l.asg_fin_tr_id
                                         where l.opfr_transfer_id in (:list)")
                        .SetParameter("list", result.TransferID)
                        .List<object[]>().Select(a => new KeyValuePair<long, decimal?>((long) a[0], FixDecimal((decimal) a[1], 4)));

                    result.PPSum = col.Sum(a => (decimal) a.Value);
                }
                return result;
            });
        }

        private static OpfrRegisterListItem SelectOpfrRegisterListItem(object[] a)
        {
            return new OpfrRegisterListItem((long)a[0], (long?)a[1], (string)a[2],
                                                        (string)a[3], Convert.ToDateTime(a[4]), (string)a[5], (long)a[6],
                                                        (long)a[7], (string)a[8], (decimal)FixDecimal(Convert.ToDecimal(a[9]),4),(string)a[10], a[11] == null ? null : (DateTime?)Convert.ToDateTime(a[11]));
        }

        public List<long> GetUsedContragentIdListForRegister(long regId)
		{
			//var finregisters = Register.GetFinregistersList(); 
			//var usedAcc = finregisters.Where(a => a.StatusID != -1).Select(fr => IsNPFtoPFR ? fr.CrAccID : fr.DbtAccID).ToList();
			//return SessionWrapper(session => session.CreateSQLQuery(string.Format("select CONTRAGENTID from ACCOUNT where ID in ({0}) ", GenerateStringFromList(accountIdList)))
			return SessionWrapper(session => session.CreateSQLQuery(@"
                select CONTRAGENTID from pfr_basic.ACCOUNT where ID in (
                    select (case when r.KIND = :kind then fr.CR_ACC_ID else fr.DBT_ACC_ID end) 
	                from pfr_basic.finregister fr
	                inner join pfr_basic.register r on r.ID = fr.reg_id 
	                where fr.reg_id = :regId and fr.status_id <> -1)")
				.SetParameter("regId", regId)
				.SetParameter("kind", RegisterIdentifier.NPFtoPFR)
				.List<long>().ToList());
		}

		/// <summary>
		/// Возвращает список банков для отображения в гриде справочника
		/// </summary>
		public IList<BankListItem> GetCommonBankListItems()
		{
			var results = new List<BankListItem>();
			const string sql = @"select le.ID, le.ShortName, le.FormalizedName, le.LegalAddress, le.Phone, l.Number, le.PFRAGRSTAT, le.Money
                        from LegalEntityHib le
                        join le.Contragent ca
                        left join le.Licensies l
                        where ca.StatusID <> -1 AND TRIM(LOWER(ca.TypeName)) = :type
                        AND l.ID in
                        (select max(l_i.ID) from License l_i group by l_i.LegalEntityID)
                        ORDER BY ca.Name";

			SessionWrapper(session =>
			{
				var sqlResult = session.CreateQuery(sql)
					.SetString("type", "банк")
					.List<object[]>();

				if (sqlResult.Count > 0)
				{
					results = sqlResult.Select(i => new BankListItem
					{
						ID = Convert.ToInt64(i[0]),
						ShortName = i[1] == null ? "" : i[1].ToString(),
						FormalizedName = i[2] == null ? "" : i[2].ToString(),
						LegalAddress = i[3] == null ? "" : i[3].ToString(),
						Phone = i[4] == null ? "" : i[4].ToString(),
						RegistrationNum = i[5] != null ? i[5].ToString() : "",
						PFRAGRSTAT = i[6] != null ? i[6].ToString() : "",
						Money = i[7] == null ? null : (decimal?)Convert.ToDecimal(i[7])
					}).ToList();
				}
			});

			return results;
		}

#endregion


#region ОПЕРАЦИИ СОХРАНЕНИЯ СУЩНОСТЕЙ

        public void OpfrRegisterUpdateRequest(OpfrRegister register)
        {
            TransactionWrapper(session =>
            {
                SaveEntity(register, session);
                session.CreateQuery("update OpfrTransfer set Status=:status where OpfrRegisterID=:id")
                .SetParameter("status", (long)Element.SpecialDictionaryItems.OpfrTransferStatusIssued)
                .SetParameter("id", register.ID)
                .ExecuteUpdate();
            });
        }

        public void UpdateOpfrTransfersPortfolio(long[] idList, long? importPortfolioID)
        {
            SessionWrapper(session =>
            {
                session.CreateQuery(@"update OpfrTransfer set PortfolioID=:id where id in (:list)")
                    .SetParameter("id", importPortfolioID)
                    .SetParameterList("list", idList)
                    .ExecuteUpdate();
            });
        }

        public void BatchUpdatePP(List<AsgFinTr> list, bool isTwoPP, List<long> singleIdList)
        {
            if (list == null || list.Count == 0) return;
	        TransactionWrapper(session =>
	        {
                list.ForEach(item =>
                {
                    SaveEntity(item, session);
                });


	            var tmpList = list.Select(a => a.ReqTransferID).Where(a=> a.HasValue);
                //выбираем только перечисления, к которым было привязано два п/п, если нужное направление
                //или ID перечисления присутствует в списке на допривязку 2го п/п
                //иначе выбираем все
	            var reqTrIdList = tmpList.Where(a => !isTwoPP || tmpList.Count(b=> b == a) == 2 || singleIdList.Contains(a.Value)).Distinct().ToArray();
                //переводим статус перечислений
	            if (reqTrIdList.Length > 0)
	            {
                    session.CreateQuery(@"update ReqTransfer rq set rq.TransferStatus=:status, rq.SPNDebitDate=:date where rq.ID in (:list)")
	                    .SetParameter("status", TransferStatusIdentifier.sSPNTransferred)
	                    .SetParameter("date", list.Last().SPNDate)
	                    .SetParameterList("list", reqTrIdList)
	                    .ExecuteUpdate();
	            }
	        });
	    }

		/// <summary>
		/// Перевод группы финреестров в статус Финреестр передан
		/// </summary>
		/// <param name="finDate"></param>
		/// <param name="finMoveType"></param>
		/// <param name="frIdList">Список идентификаторов финреестров для перевода</param>
		/// <param name="isNpfToPfr">Направление</param>
		public void SetFinregisterStateTransfered(DateTime? finDate, string finMoveType, List<long> frIdList, bool isNpfToPfr)
		{
			SessionWrapper(session =>
			{
				if (isNpfToPfr)
				{
                    session.CreateSQLQuery(string.Format(@"update {0}.finregister SET STATUS=:status, IS_TRANSFERRED=1, FINNUM=
                                            (select leid.IDENTIFIER
                                                from {0}.Finregister fi
												LEFT join {0}.ACCOUNT ac ON ac.ID = fi.{1}
												LEFT join {0}.CONTRAGENT contr ON contr.ID = ac.CONTRAGENTID
                                                LEFT join {0}.LEGALENTITY le ON le.CONTRAGENTID = contr.ID
                                                LEFT join {0}.LEGALENTITY_IDENTIFIER leid ON leid.LE_ID=le.ID
                                                where fi.ID = {0}.finregister.ID and leid.STATUS_ID<>-1
                                                order by leid.SETDATE asc
                                                FETCH FIRST 1 ROWS ONLY
                                            ) where ID in (:idlist)", DATA_SCHEME, "CR_ACC_ID" ))
						.SetParameter("status", RegisterIdentifier.FinregisterStatuses.Transferred)
						.SetParameterList("idlist", frIdList)
						.ExecuteUpdate();
				}
				else
					session.CreateSQLQuery(string.Format(@"update {0}.finregister set FINDATE=:date, FINMOVETYPE=:type, IS_TRANSFERRED=1, STATUS=:status, FINNUM=
                                            (select leid.IDENTIFIER
                                                from {0}.Finregister fi
												LEFT join {0}.ACCOUNT ac ON ac.ID = fi.{1}
												LEFT join {0}.CONTRAGENT contr ON contr.ID = ac.CONTRAGENTID
                                                LEFT join {0}.LEGALENTITY le ON le.CONTRAGENTID = contr.ID
                                                LEFT join {0}.LEGALENTITY_IDENTIFIER leid ON leid.LE_ID=le.ID
                                                where fi.ID = {0}.finregister.ID  and leid.STATUS_ID<>-1
                                                order by leid.SETDATE asc
                                                FETCH FIRST 1 ROWS ONLY
                                            )
                                        where ID in (:idlist)", DATA_SCHEME, "DBT_ACC_ID"))
						.SetParameter("date", finDate)
						.SetParameter("type", finMoveType)
						.SetParameter("status", RegisterIdentifier.FinregisterStatuses.Transferred)
						.SetParameterList("idlist", frIdList)
						.ExecuteUpdate();
				frIdList.ForEach(a => SaveJornalLog(JournalEventType.ARCHIVE, "Финреестр", a, "Finregister"));
			});
		}

	    public string GetLeIdentifierCodeForFinregister(long id, bool isNpfToPfr)
	    {
	        return SessionWrapper(session => session.CreateSQLQuery(string.Format(@"select leid.IDENTIFIER
                                                from {0}.Finregister fi
												LEFT join {0}.ACCOUNT ac ON ac.ID = fi.{1}
												LEFT join {0}.CONTRAGENT contr ON contr.ID = ac.CONTRAGENTID
                                                LEFT join {0}.LEGALENTITY le ON le.CONTRAGENTID = contr.ID
                                                LEFT join {0}.LEGALENTITY_IDENTIFIER leid ON leid.LE_ID=le.ID
                                                where fi.ID = :frId and leid.STATUS_ID<>-1
                                                order by leid.SETDATE asc
                                                FETCH FIRST 1 ROWS ONLY", DATA_SCHEME, isNpfToPfr ? "CR_ACC_ID" : "DBT_ACC_ID"))
	            .SetParameter("frId", id)
	            .UniqueResult<string>());
	    }

		/// <summary>
		/// Перевод группы финреестров в статус Финреестр оформлен
		/// </summary>
		/// <param name="frIdList"></param>
		/// <param name="isNpfToPfr"></param>
		public void SetFinregisterStateIssued(List<long> frIdList, bool isNpfToPfr)
		{
			SessionWrapper(session =>
			{
				session.CreateQuery(@"update Finregister fr SET fr.Status=:status where fr.ID in (:idlist)")
					.SetParameter("status", RegisterIdentifier.FinregisterStatuses.Issued)
					.SetParameterList("idlist", frIdList)
					.ExecuteUpdate();
				frIdList.ForEach(a => SaveJornalLogWithAdditionalInfo(JournalEventType.CHANGE_STATE, "Финреестр", a, "Finregister", "Перевод в статус Финреестр оформлен"));

			});
		}

		public long SaveAps(Aps aps, List<DopAps> dopAps)
		{
			return TransactionWrapper(session =>
			{
				var result = SaveEntity(aps, session);
				dopAps.ForEach(a =>
				{
					a.PFRBankAccountID = aps.PFRBankAccountID;
					a.PortfolioID = aps.PortfolioID;
					a.SPFRBankAccountID = aps.SPFRBankAccountID;
					a.SPortfolioID = aps.SPortfolioID;
				});
				SaveEntities(dopAps, session);
				return result;
			});
		}

		/// <summary>
		/// Сохранение страховых взносов
		/// </summary>
		/// <param name="addSpn">СВ</param>
		/// <param name="dopSPNList">Список уточнений</param>
		public long SaveDue(AddSPN addSpn, List<DopSPNListItem> dopSPNList)
		{
			return TransactionWrapper(session =>
			{
				var result = SaveEntity(addSpn, session);
				if (dopSPNList != null)
					dopSPNList.ForEach(item =>
					{
						if (item.IsKDoc)
						{
							//Обновляем существующий ДК
							var tmp = session.Get<KDoc>(item.ID);
							tmp.PortfolioID = addSpn.PortfolioID;
							SaveEntity(tmp, session);
						}
						else
						{
							var tmp = session.Get<DopSPN>(item.ID);
							tmp.PeriodID = addSpn.MonthID;
							tmp.PortfolioID = addSpn.PortfolioID;
							SaveEntity(tmp, session);
						}
					});
				return result;
			});
		}

		/// <summary>
		/// Сохранение финреестров для реестра Возврата СПН
		/// </summary>
		/// <param name="finregisters"></param>
		/// <returns></returns>
		public List<long> SaveReturnFinregisters(List<FinregisterWithCorrAndNPFListItem> finregisters)
		{
			return TransactionWrapper(session =>
			{
				var list = new List<long>();
				finregisters.ForEach(a =>
				{
					if (a.Finregister is FinregisterListItem)
					{
						var fr = new Finregister();
						a.Finregister.CopyTo(fr);
						list.Add(SaveEntity(fr, session));
					}
					else list.Add(SaveEntity(a.Finregister, session));
				});
				return list;
			});
		}

		/// <summary>
		/// Сохранение контракта СИ
		/// </summary>
		/// <param name="data">Данные</param>
		/// <param name="isNew">Сохранение нового эемента?</param>
		public SaveSIContractResultItem SaveSIContract(SaveSIContractItem data, bool isNew)
		{
			return TransactionWrapper(session =>
			{
				var result = new SaveSIContractResultItem();

				if (data.BankAccount != null)
					data.Contract.BankAccountID = result.BankAccountId = SaveEntity(data.BankAccount, session);
				data.ContrInvest.ContractID = data.RewardCost.ContractID = result.ContractId = SaveEntity(data.Contract, session);

				result.ContrInvestId = SaveEntity(data.ContrInvest, session);
				data.ContrInvestAKCList.ForEach(a => a.ContrInvestID = result.ContrInvestId);
				data.ContrInvestOBLList.ForEach(a => a.ContrInvestID = result.ContrInvestId);
				data.ContrInvestPAYList.ForEach(a => a.ContrInvestID = result.ContrInvestId);

				SaveEntities(data.ContrInvestAKCList, session);
				SaveEntities(data.ContrInvestOBLList, session);
				SaveEntities(data.ContrInvestPAYList, session);

				//TODO UpdateContragent перенести в отдельный вызов

				result.RewardCostId = SaveEntity(data.RewardCost, session);

				return result;
			});
		}

		/// <summary>
		/// Сохранение  субъекта СИ
		/// </summary>
		/// <param name="data">Объект данных</param>
		public SaveSIResultItem SaveSI(SaveSIItem data)
		{
			return TransactionWrapper(session =>
			{
				var result = new SaveSIResultItem();

				data.LegalEntity.ContragentID = result.ContragentId = SaveEntity(data.Contragent, session);
				result.LegalEntityId = data.LegalEntity.ID = SaveEntity(data.LegalEntity, session);

				data.Licenses.ForEach(a => a.LegalEntityID = result.LegalEntityId);
				switch (data.Licenses.Count)
				{
					case 1:
						result.LicUkId = SaveEntity(data.Licenses.First(), session);
						break;
					case 2:
						result.LicSdId = SaveEntity(data.Licenses.First(), session);
						result.LicDdId = SaveEntity(data.Licenses.Last(), session);
						break;
					//в случае редактирования, без разницы какие Id возвращать
					case 3:
						SaveEntities(data.Licenses, session);
						break;
				}

				if (data.Contracts.Any())
					SaveEntities(data.Contracts, session);

				if (data.Contacts.Any())
				{
					data.Contacts.ForEach(a => a.LegalEntityID = result.LegalEntityId);
					SaveEntities(data.Contacts, session);
				}

				return result;
			});
		}

		/// <summary>
		/// Сохранение объекта банка
		/// </summary>
		/// <param name="ca">Контрагент</param>
		/// <param name="bank">Банк</param>
		/// <param name="license">Лицензия</param>
		/// <param name="headList">Список упол. лиц</param>
		/// <param name="delHeadList">На удаление</param>
		/// <param name="ratingList">Список рейтингов</param>
		/// <param name="delRatingList">На удаление</param>
		/// <param name="stockCodeList">Список биржевых кодов</param>
		/// <param name="delStockCodeList">На удаление</param>
		/// <param name="personListChanged">Список контактов</param>
		public LongLongLongTuple SaveBank(Contragent ca,
			LegalEntity bank,
			License license,
			List<LegalEntityHead> headList,
			List<long> delHeadList,
			List<LegalEntityRating> ratingList,
			List<long> delRatingList,
			List<BankStockCode> stockCodeList,
			List<long> delStockCodeList,
			List<LegalEntityCourier> personListChanged)
		{
			return TransactionWrapper(session =>
			{
				var result = new LongLongLongTuple();
				result.Value = ca.ID = SaveEntity(ca, session);
				bank.ContragentID = ca.ID;
				result.Key = bank.ID = SaveEntity(bank, session);
				license.LegalEntityID = bank.ID;
				result.Value2 = license.ID = SaveEntity(license, session);

				DeleteEntities<LegalEntityHead>(delHeadList, session);
				headList.ForEach(h => h.LegalEntityID = bank.ID);
				SaveEntities(headList, session);

				DeleteEntities<LegalEntityRating>(delRatingList, session);
				ratingList.ForEach(h => h.LegalEntityID = bank.ID);
				SaveEntities(ratingList, session);

				DeleteEntities<BankStockCode>(delStockCodeList, session);
				stockCodeList.ForEach(h => h.BankID = bank.ID);
				SaveEntities(stockCodeList, session);

				personListChanged.ForEach(x =>
				{
					x.LegalEntityID = bank.ID;
					x.LetterOfAttorneyExpireAlert = x.SignatureExpireAlert = (int)LegalEntityCourier.AlertTypes.NotAlerted;
					var cid = x.ID = SaveEntity(x, session);

					if (x.LetterOfAttorneyListChanged != null)
					{
						foreach (var y in x.LetterOfAttorneyListChanged)
						{
							y.LegalEntityCourierID = cid;
							y.ID = SaveEntity(y, session);
						}

						x.LetterOfAttorneyListChanged = null;
					}
					if (x.LetterOfAttorneyListDeleted != null)
					{
						x.LetterOfAttorneyListDeleted.ForEach(l =>
						{
							if (l.ID > 0)
							{
								DeleteEntity<LegalEntityCourierLetterOfAttorney>(l.ID, session);
								SaveDeleteEntryLog(typeof(LegalEntityCourierLetterOfAttorney).Name, $"Доверенность '{l.Number}'", l.ID, 1, session);
							}
						}
						);
						x.LetterOfAttorneyListDeleted = null;
					}

					if (x.CertificatesListChanged != null)
					{
						foreach (var y in x.CertificatesListChanged)
						{
							y.LegalEntityCourierID = cid;
							y.ID = SaveEntity(y, session);
						}

						x.CertificatesListChanged = null;
					}
					if (x.CertificatesListDeleted != null)
					{
						x.CertificatesListDeleted.ForEach(l =>
						{
							if (l.ID > 0)
							{
								DeleteEntity<LegalEntityCourierCertificate>(l.ID, session);
								SaveDeleteEntryLog(typeof(LegalEntityCourierCertificate).Name, $"Сертификат '{l.Signature}'", l.ID, 1, session);
							}
						});

						x.CertificatesListDeleted = null;
					}
				});

				return result;
			});
		}

		/// <summary>
		/// Сохранения плана выплат на год
		/// </summary>
		/// <param name="transfer">План</param>
		/// <param name="paymentsList">Список выплат</param>
		public long SaveUKPaymentPlan(SITransfer transfer, List<LongDecimalTuple> paymentsList)
		{
			return TransactionWrapper(session =>
			{
				bool isNew = transfer.ID == 0;
				var id = SaveEntity(transfer);
				paymentsList.ForEach(a =>
				{
					if (isNew)
					{
						SaveEntity(new SIUKPlan { TransferListID = id, PlanSum = a.Value, MonthID = a.Key }, session);
					}
					else
						session.CreateSQLQuery($"UPDATE {DATA_SCHEME}.{TABLE_SI_UKPLAN} set PLANSUM = :sum WHERE ID = :id")
							.SetParameter("sum", a.Value)
							.SetParameter("id", a.Key)
							.ExecuteUpdate();
				});
				return id;
			});
		}

		/// <summary>
		/// Сохранение списка перечислений
		/// </summary>
		/// <param name="siTransfer">Список перечислений SITransfer</param>
		/// <param name="reqList">Список перечислений</param>
		public long SaveTransfer(SITransfer siTransfer, List<ReqTransfer> reqList)
		{
			return TransactionWrapper(session =>
			{
				var id = SaveEntity(siTransfer, session);

				reqList.ForEach(a => a.TransferListID = id);
				SaveEntities(reqList, session);

				UpdateSITransferZLCount(id, session);

				return id;
			});
		}

		/// <summary>
		/// Сохранения перечисления СИ/ВР
		/// </summary>
		/// <param name="req">Перечисление</param>
		public long SaveReqTransfer(ReqTransfer req)
		{
			return TransactionWrapper(session => SaveReqTransferInternal(req, true, session));
		}

		private long SaveReqTransferInternal(ReqTransfer req, bool updateTransfer, ISession session)
		{
			var res = SaveEntity(req);
			if (updateTransfer && req.TransferListID.HasValue)
				UpdateSITransferZLCount(req.TransferListID.Value, session);
			return res;
		}

		private void UpdateSITransferZLCount(long transferId, ISession session)
		{
			session.CreateQuery(
				@"update SITransfer tr set tr.InsuredPersonsCount = (select sum(r.ZLMoveCount) from ReqTransfer r where r.TransferListID = :id and r.StatusID <> -1) where tr.ID = :id and tr.StatusID <> -1")
				.SetParameter("id", transferId)
				.ExecuteUpdate();
		}

		public WebServiceDataError SaveAndCleanDocument(Document document, List<long> removeAttachIds = null, long? docFileIdToRemove = null, long? docFileContentIdToRemove = null)
		{
			return TransactionWrapper(session =>
			{
				SaveEntity(document, session);
				if (removeAttachIds != null && removeAttachIds.Count > 0)
					DeleteEntities<Attach>(removeAttachIds, session);
				try
				{
					if (docFileIdToRemove.HasValue)
						DeleteEntity<DocFileBody>(docFileIdToRemove.Value, session);
					if (docFileContentIdToRemove.HasValue)
						DeleteEntity<FileContent>(docFileContentIdToRemove.Value, session);
				}
				catch
				{
					return WebServiceDataError.DocumentDeleteError;
				}

				return WebServiceDataError.None;
			});
		}

		public long SaveBankAccount(BankAccount bankAccount)
		{
		    return TransactionWrapper(session =>
		    {
		        //При редактировании делаем копию старого значения в архив
		        if (bankAccount.ID > 0)
		        {
		            var ba = session.Get<BankAccount>(bankAccount.ID);
		            session.Evict(ba);
		            ba.ID = 0;
		            ba.CloseDate = DateTime.Now;
		            SaveEntity(ba, session);
		            session.Flush();
		        }

		        //Закрываем все открытые счета, кроме нового
		        if (bankAccount.LegalEntityID > 0)
		        {
		            session.CreateQuery(@"update BankAccount ba set ba.CloseDate = :date
													where ba.LegalEntityID = :leID and ba.ID <> :baID and ba.CloseDate is null ")
		                .SetParameter("leID", bankAccount.LegalEntityID)
		                .SetParameter("baID", bankAccount.ID)
                        .SetParameter("date", DateTime.Now.Date)
                        .ExecuteUpdate();
		        }

		        if (bankAccount.PfrBranchID > 0)
		        {
		            session.CreateQuery(@"update BankAccount ba set ba.CloseDate = :date
													    where ba.PfrBranchID = :bID and ba.ID <> :baID and ba.CloseDate is null ")
		                .SetParameter("bID", bankAccount.PfrBranchID)
		                .SetParameter("baID", bankAccount.ID)
		                .SetParameter("date", DateTime.Now.Date)
		                .ExecuteUpdate();
		        }

		        // сохраняем счёт
		        var id = SaveEntity<BankAccount, long>(bankAccount, session);
		        session.Flush();
		        return id;
		    });
		}

#endregion


#region ОПЕРАЦИИ УДАЛЕНИЯ СУЩНОСТЕЙ

#region HELPERS

		private bool IsFinregisterHasPP(long id, ISession session)
		{
			return GetObjectPropertyCount<Finregister>(id, "DraftsList", session) != 0;
		}

		private void DeleteFinregisterInternal(long id, ISession session)
		{
			// по аналогии с перечислениями, платежки не удаляются
			//foreach (var item in GetListByProperty<AsgFinTr>("FinregisterID", id))
			//    DeleteEntity(item, session);
			session.CreateSQLQuery(
			        $"UPDATE {DATA_SCHEME}.{TABLE_ASG_FIN_TR} SET FR_ID = NULL, LINKEDREGISTER_ID = null, LINK_EL_ID = 100001 WHERE FR_ID = {id}")
				.ExecuteUpdate();

			session.CreateSQLQuery(
				string.Format(
					@"update {0}.{1}
                      SET STATUS = :newStatus
                      where exists (select 1 from {0}.{2} where ID = {0}.{1}.REG_ID and KIND = :dir) and STATUS = :oldStatus and ID = :id
                     ",
					DATA_SCHEME,
					TABLE_FINREGISTER,
					TABLE_REGISTER))
					.SetParameter("newStatus", RegisterIdentifier.FinregisterStatuses.Created)
					.SetParameter("dir", RegisterIdentifier.PFRtoNPF)
					.SetParameter("oldStatus", RegisterIdentifier.FinregisterStatuses.Issued)
					.SetParameter("id", id)
				.ExecuteUpdate();

			DeleteEntity<Finregister>(id, session);
		}

		private void RemoveReqTransferFromUKPlan(long reqTransferId, ISession session)
		{
			RemoveReqTransferFromUKPlan(new List<long> { reqTransferId }, session);
		}

		private void RemoveReqTransferFromUKPlan(List<long> reqTransferId, ISession session)
		{
			if (reqTransferId == null || reqTransferId.Count == 0) return;
			var idStr = GenerateStringFromList(reqTransferId);
		    if (IsPostgreSQL)
		    {
		        var list = session.CreateSQLQuery(string.Format(@"
	                    SELECT PL.ID, PL.REQ_TR_ID AS RT1, PL.REQ_TR_ID2 as RT2, TR1.SUM as TR1SUM, TR2.SUM as TR2SUM
	                    FROM PFR_BASIC.SI_UKPLAN PL
	                    LEFT JOIN PFR_BASIC.REQ_TRANSFER TR1 ON TR1.ID = PL.REQ_TR_ID
	                    LEFT JOIN PFR_BASIC.REQ_TRANSFER TR2 ON TR2.ID = PL.REQ_TR_ID2
	                    WHERE (PL.REQ_TR_ID IN ({0}) or PL.REQ_TR_ID2 IN ({0})) and PL.STATUS_ID <> -1", idStr)).List<object[]>()
		            .Select(a => new {ID = a[0] == null ? 0 : (long)a[0], RQ1 = a[1] == null ? 0 : (long)a[1], RQ2 = a[2] == null ? 0 : (long)a[2], SUM1 = Convert.ToDecimal(a[3] ?? 0), SUM2 = Convert.ToDecimal(a[4] ?? 0)})
		            .ToList();

		        if (list.Any())
		        {
		            list.Where(a => reqTransferId.Contains(a.RQ1)).ForEach(a => session
		                .CreateSQLQuery(@"UPDATE PFR_BASIC.SI_UKPLAN SET REQ_TR_ID = NULL, FACTSUM = coalesce(:sum,0) WHERE ID = :id")
		                .SetParameter("sum", a.SUM2)
		                .SetParameter("id", a.ID)
		                .ExecuteUpdate());

		            list.Where(a => reqTransferId.Contains(a.RQ2)).ForEach(a => session
		                .CreateSQLQuery(@"UPDATE PFR_BASIC.SI_UKPLAN SET REQ_TR_ID2 = NULL, FACTSUM = coalesce(:sum,0) WHERE ID = :id")
		                .SetParameter("sum", a.SUM1)
		                .SetParameter("id", a.ID)
		                .ExecuteUpdate());
		        }
		    }
            else
		    	session.CreateSQLQuery(string.Format(@"
                        MERGE INTO PFR_BASIC.SI_UKPLAN p
                        USING(
                            SELECT PL.ID, PL.REQ_TR_ID AS RT1, PL.REQ_TR_ID2 as RT2, TR1.SUM as TR1SUM, TR2.SUM as TR2SUM
                            FROM PFR_BASIC.SI_UKPLAN PL
                            LEFT JOIN PFR_BASIC.REQ_TRANSFER TR1 ON TR1.ID = PL.REQ_TR_ID
                            LEFT JOIN PFR_BASIC.REQ_TRANSFER TR2 ON TR2.ID = PL.REQ_TR_ID2
                            WHERE (PL.REQ_TR_ID IN ({0}) or PL.REQ_TR_ID2 IN ({0})) and PL.STATUS_ID <> -1 
                        ) AS src
                        ON src.ID = p.ID
                        WHEN MATCHED AND src.RT1 IN ({0}) THEN 
                            UPDATE SET p.REQ_TR_ID = NULL, p.FACTSUM = coalesce(TR2SUM,0)
                        WHEN MATCHED AND src.RT2 IN ({0}) THEN
                            UPDATE SET p.REQ_TR_ID2 = NULL, p.FACTSUM = coalesce(TR1SUM,0)", idStr))
                        .ExecuteUpdate();
		}

		private List<ReqTransfer> GetReqTransfersForSITransfer(long siTransferId, ISession session)
		{
			return session.CreateQuery(@"SELECT rt 
						                FROM ReqTransfer rt 
                                        WHERE rt.StatusID <> -1 and rt.TransferListID = :id")
						.SetParameter("id", siTransferId)
						.List<ReqTransfer>()
						.ToList();
		}

#endregion

		public void DeleteAPS(long id)
		{
			TransactionWrapper(session =>
			{
				DeleteDopAPSList(GetIdListByProperty<DopAps>("ApsID", id, session));
				DeleteEntity<Aps>(id, session);
			});
			//DeleteCard(id, TABLE_APS);
		}

		public void DeleteDopAPSList(List<long> ids)
		{
			TransactionWrapper(session => DeleteEntities<DopAps>(ids, session));

			/*foreach (var id in ids)
				DeleteCard(id, TABLE_DOPAPS);*/
		}

		/// <summary>
		/// Удаление СВ
		/// </summary>
		/// <param name="addSpnId">Идентификатор СВ</param>
		public void DeleteDue(long addSpnId)
		{
			TransactionWrapper(session =>
			{
				var idList = GetIdListByProperty<DopSPN>("AddSpnID", addSpnId, session);
				if (idList.Any())
					DeleteEntities<DopSPN>(idList, session);
				DeleteEntity<AddSPN>(addSpnId, session);
			});
		}

		/// <summary>
		/// Удаление уточнения СВ
		/// </summary>
		/// <param name="dopSpnId">Идентификатор уточнения СВ</param>
		/// <param name="isKDoc">Это документ казначейства?</param>
		public void DeleteDopSPN(long dopSpnId, bool isKDoc)
		{
			TransactionWrapper(session =>
			{
				if (isKDoc) DeleteEntity<KDoc>(dopSpnId, session);
				else DeleteEntity<DopSPN>(dopSpnId, session);
			});
		}

		/// <summary>
		/// Удаление направления СЧИЛС БО
		/// </summary>
		/// <param name="dirId">Идентификатор направления</param>
		public void DeleteDirectionSchils(long dirId)
		{
			TransactionWrapper(session =>
			{
				var transferIdList = GetIdListByProperty<Transfer>("DirectionID", dirId, session);
				DeleteEntities<Transfer>(transferIdList, session);
				DeleteEntity<Direction>(dirId, session);
			});
		}

		/// <summary>
		/// Удаление бюджета СЧИЛС БО
		/// </summary>
		/// <param name="budgetId">Идентификатор бюджета</param>
		public void DeleteBudgetSchils(long budgetId)
		{
			TransactionWrapper(session =>
			{
				var directionIdList = GetIdListByProperty<Direction>("BudgetID", budgetId, session);
				var transferIdList = GetIdListByPropertyIn<Transfer, long>("DirectionID", directionIdList, session);
				DeleteEntities<Transfer>(transferIdList, session);
				DeleteEntities<Direction>(directionIdList, session);
				var returnIdList = GetIdListByProperty<Return>("BudgetID", budgetId, session);
				DeleteEntities<Return>(returnIdList, session);
				var bcIdList = GetIdListByProperty<BudgetCorrection>("BudgetID", budgetId, session);
				DeleteEntities<BudgetCorrection>(bcIdList, session);

				DeleteEntity<Budget>(budgetId, session);
			});
		}

		/// <summary>
		/// Удаление документа (общего)
		/// </summary>
		/// <param name="documentId">Идентификатор</param>
		/// <param name="attachIdList">Спсиок идентификаторов вложений</param>
		/// <param name="fileContentIdList">Список идентификаторов контента</param>
		public void DeleteDocument(long documentId, List<long> attachIdList, List<long> fileContentIdList)
		{
			TransactionWrapper(session =>
			{
				var docs = GetListByProperty<DocFileBody>("DocumentID", documentId, session)
					.Cast<DocFileBody>().ToList();
				var dfbIdList = docs.Select(a => a.ID);
				DeleteEntities<DocFileBody>(dfbIdList, session);
				var fcList = docs.Where(a => a.FileContentId.HasValue).Select(a => a.FileContentId.Value);
				DeleteEntities<FileContent>(fcList, session);

				DeleteEntities<Attach>(attachIdList, session);
				DeleteEntities<FileContent>(fileContentIdList, session);

				DeleteEntity<Document>(documentId, session);
			});
		}

		/// <summary>
		/// Удаления карточки субъекта СИ
		/// </summary>
		/// <param name="contragentId"></param>
		/// <param name="legalEntityId"></param>
		public void DeleteSI(long contragentId, long legalEntityId)
		{
			TransactionWrapper(session =>
			{
				DeleteEntity<Contragent>(contragentId, session);
				var idList = GetContractsListForLegalEntityHib(legalEntityId).Select(a => a.ID).ToList();
				DeleteEntities<PFR_INVEST.DataObjects.Contract>(idList, session);
			});
		}

		/// <summary>
		/// Удаление контракта
		/// На момент создания - простое - требуется для общего запуска из разных мест
		/// </summary>
		/// <param name="contractId">Идентификатор контракта</param>
		public void DeleteContract(long contractId)
		{
			//при модификации глянуть в DeleteSI(), там удаление контрактов батчем для быстроты, т.к. оно пока примитивное
			TransactionWrapper(session => DeleteEntity<PFR_INVEST.DataObjects.Contract>(contractId, session));
		}

		public void DeleteUKPaymentsPlan(long planId)
		{
			TransactionWrapper(session =>
			{
				session.CreateQuery("update SIUKPlan p set p.StatusID = -1 where p.TransferListID = :id")
					.SetParameter("id", planId)
					.ExecuteUpdate();

				//TODO возможно, следует удалять всю ветку перечисления, т.к. под перечисление создается новая ветка?
				session.CreateQuery("update ReqTransfer p set p.StatusID = -1 where p.TransferListID = :id")
					.SetParameter("id", planId)
					.ExecuteUpdate();
				DeleteEntity<SITransfer>(planId, session);
			});
		}

		public void DeleteDocFileBodyWithContent(long docFileBodyId, long? fileContentId = null)
		{
			TransactionWrapper(session =>
			{
				DeleteEntity<DocFileBody>(docFileBodyId, session);
				if (fileContentId != null)
					DeleteEntity<FileContent>(fileContentId.Value, session);
			});
		}

		/// <summary>
		/// Удаление перечисления ReqTransfer
		/// </summary>
		/// <param name="reqTransferId">ID перечисления</param>
		/// <param name="siTransferId">ID списка перечислений</param>
		/// <param name="contractId">ID контракта</param>
		/// <param name="isActSigned">Подписан ли акт</param>
		public void DeleteReqTransfer(long reqTransferId, long? siTransferId, long contractId, bool isActSigned)
		{
			TransactionWrapper(session =>
			{
				DeleteReqTransferInternal(reqTransferId, contractId, isActSigned, session);
				if (siTransferId.HasValue)
					UpdateSITransferZLCount(siTransferId.Value, session);
				return true;
			});
		}

		private void DeleteReqTransferInternal(long reqTransferId, long contractId, bool isActSigned, ISession session)
		{
			RemoveReqTransferFromUKPlan(reqTransferId, session);

			ClearLinkReqTrForAsgFinTr(reqTransferId.ToString(), session);

			DeleteEntity<ReqTransfer>(reqTransferId, session);

			if (isActSigned)
				UpdateReqTrAndContractByActSigned(contractId, session);
		}

		private void DeleteReqTransferManyInternal(List<ReqTransferDeleteListItem> reqTransferList, ISession session)
		{
			if (reqTransferList == null || reqTransferList.Count == 0) return;
			var idList = reqTransferList.Select(a => a.ReqTransferId).ToList();
			var idStr = GenerateStringFromList(idList);

			RemoveReqTransferFromUKPlan(idList, session);

			ClearLinkReqTrForAsgFinTr(idStr, session);
			DeleteEntities<ReqTransfer>(idList, session);
			//после удаления перечисления??? ДА, т.к. пересчет сумм на начало операций
			if (reqTransferList.Any(a => a.IsActSigned))
				UpdateReqTrAndContractByActSigned(reqTransferList.First().ContractId, session);
		}


		/// <summary>
		/// Удаление реестра НПФ
		/// </summary>
		/// <param name="id">Идентификатор</param>
		public WebServiceDataError DeleteRegister(long id)
		{
			return TransactionWrapper(session =>
			{
				var result = DeleteRegisterInternal(id, session);
				return result;
			});
		}


		private WebServiceDataError DeleteRegisterInternal(long id, ISession session, bool isRecursve = false)
		{
			var frList = GetListByProperty<Finregister>("RegisterID", id, session)
				.OfType<Finregister>()
				.Where(a => a.StatusID != -1)
				.ToList();
			//проверка на наличие финреестров в статусе отличном от начального
			if (!frList.All(a => RegisterIdentifier.FinregisterStatuses.IsCreated(a.Status)))
				return WebServiceDataError.RegisterHasFinregWithInvStatus;
			//проверка на наличие финреестров с п/п
			if (frList.Any(a => IsFinregisterHasPP(a.ID, session)))
				return WebServiceDataError.FinregisterHasPP;

			frList.ForEach(a =>
			{
				DeleteFinregisterInternal(a.ID, session);
				SaveDeleteEntryLog("FinregisterViewModel", "Финреестр удален с реестром " + id, a.ID,
										DocOperation.Archive, session);
			});
			DeleteEntity<Register>(id, session);

			//не ищем более 1 вложения реестров для возврата СПН
			if (!isRecursve)
			{
				var regIdList =
					session.CreateSQLQuery($"select id from {DATA_SCHEME}.{TABLE_REGISTER} where return_id = {id}")
						.List().OfType<long>().ToList(); if (regIdList.Any())
				{
					regIdList.ForEach(a => DeleteRegisterInternal(a, session, true));
				}
			}
			return WebServiceDataError.None;
		}

		/// <summary>
		/// Удаление финреестра НПФ
		/// </summary>
		/// <param name="id">Идентификатор</param>
		/// <param name="checkFailConditions">Проверять условия удаления</param>
		/// <param name="logData">Опционально. Данные для логирования, если указаны, то будет вызов операции логирования</param>
		/// <param name="checkIsCreated">делать ли проверку на статус - финреестр Создан</param>
		/// <returns></returns>
		public WebServiceDataError DeleteFinregister(long id, bool checkFailConditions = false, string logData = null, bool checkIsCreated = false)
		{
			var dataError = TransactionWrapper(session =>
			{
				if (checkFailConditions)
				{
					var fr = GetByID<Finregister>(id);
					//проверка на статус ФИнреестр не передан и наличие дочерних финреестров
					if (RegisterIdentifier.FinregisterStatuses.IsNotTransferred(fr.Status)
						&& GetListByPropertyConditionsCount<Finregister>(new List<ListPropertyCondition>
					    {
		                    ListPropertyCondition.Equal("ParentFinregisterID", fr.ID),
		                    ListPropertyCondition.NotEqual("StatusID", -1L)
		                }) != 0)
						return WebServiceDataError.FinregisterHasChildren;
					//проверка на наличие п/п - ОТМЕНЯЕТСЯ. Разрешаем удалять, п/п теряют связь.
					//if (IsFinregisterHasPP(fr.ID, session))
					//    return WebServiceDataError.FinregisterHasPP;
				}
				if (checkIsCreated)
				{
					if (!RegisterIdentifier.FinregisterStatuses.IsCreated(GetByID<Finregister>(id).Status))
						return WebServiceDataError.FinregisterNotInCreatedStatus;
				}

				DeleteFinregisterInternal(id, session);
				return WebServiceDataError.None;
			});

			if (logData != null)
				SaveDetailedJornalLog(JournalEventType.DELETE, "Финреестр", id, logData, null, typeof(Finregister).Name);

			return dataError;
		}

		/// <summary>
		/// Удаление Списка Перечислений СИ
		/// </summary>
		/// <param name="reqTransferList">Список данных перечислений</param>
		/// <param name="transferListId">Индентификатор списка перечислений</param>
		public void DeleteTransfer(List<ReqTransferDeleteListItem> reqTransferList, long transferListId)
		{
			TransactionWrapper(session =>
			{
				DeleteTransferSingleInternal(reqTransferList, transferListId, session);
			});
		}

        public List<AccOpListItem> GetTransfersForChfr(long portfolioID, int selectedYear, int selectedQuarter, int mode)
        {
            return SessionWrapper(session =>
            {
                var nList = new List<AccOpListItem>();
                switch (mode)
                {
                    case 3:
                        var lst1 = GetTransfersForChfr(portfolioID, selectedYear, selectedQuarter, @"
                        select a.ID, coalesce(a.Summ,0), coalesce(r.Value,1), a.RegNum, a.NewDate
                            from AccOperation a
                            join a.Currency c
                            join a.Portfolio p
                            left join c.Curses r
                        where (r.Date is null or r.Date=a.NewDate) and p.StatusID>=0 and p.ID = :pid
                            and a.PortfolioID<>a.SourcePortfolioID and a.AddToCHFR=1
                        order by r.ID desc", false);
                        lst1.ForEach(
                            a => nList.Add(new AccOpListItem
                            {
                                ID = (long)a[0],
                                Summ = Convert.ToDecimal(a[1]),
                                Curs = Convert.ToDecimal(a[2]),
                                RegNum = (string)a[3],
                                RegDate = (DateTime?)a[4]
                            }));
                        nList = nList.GroupBy(a => a.ID).Select(a => a.First()).ToList();
                        return nList;
                    case 2: //возврат депозита
                        var lst2 = GetTransfersForChfr(portfolioID, selectedYear, selectedQuarter, $@"
                            SELECT 
                                d.ID, 
                                ph.SUM AS VSUMM,
                                CASE
                                    WHEN (b.CURR_ID IS NULL OR b.CURR_ID = 1) THEN 1 ELSE
                                        COALESCE(
                                            (SELECT r.ID
                                            FROM PFR_BASIC.CURS r
                                            WHERE b.CURR_ID = r.CURR_ID
                                            AND dc2.SettleDate>=r.DATE
                                            ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
                                        ,1)
                                END AS CURS

                               FROM PFR_BASIC.DEPOSIT d
                                   inner join PFR_BASIC.DEPCLAIM_OFFER o ON o.ID = d.DEPCLAIM_OFFER_ID
                                   INNER JOIN PFR_BASIC.DEPCLAIM2 dc2 ON o.DEPCLAIM_ID=dc2.ID
                                   INNER JOIN PFR_BASIC.OFFER_PFRBANKACCOUNT_SUM s ON s.OFFER_ID = o.ID AND s.STATUS>0
                                   INNER JOIN PFR_BASIC.PORTFOLIO p ON s.PORTFOLIO_ID=p.ID
                                   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
                                   LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON s.PFRBANKACCOUNT_ID=b.ID
                                   INNER JOIN PFR_BASIC.PAYMENT_HISTORY ph ON ph.OFFER_PFRBANKACCOUNT_SUM_ID = s.ID
                               where
                                   ph.FOR_PERCENT = 0 and p.ID=:pid", true);
                        lst2.ForEach(
                            a => nList.Add(new AccOpListItem
                            {
                                ID = (long) a[0],
                                Summ = Convert.ToDecimal(a[1]) / (IsDB2 && a[1] is decimal ? 100m : 1m),
                                Curs = Convert.ToDecimal(a[2]) / (IsDB2 && a[2] is decimal ? 100m : 1m)
                            }));
                        return nList;
                    case 1: //проценты по депозитам
                        var lst3 = GetTransfersForChfr(portfolioID, selectedYear, selectedQuarter, $@"
                            SELECT 
                                d.ID, 
                                ph.SUM AS VSUMM,
                                CASE
                                    WHEN (b.CURR_ID IS NULL OR b.CURR_ID = 1) THEN 1 ELSE
                                        COALESCE(
                                            (SELECT r.ID
                                            FROM PFR_BASIC.CURS r
                                            WHERE b.CURR_ID = r.CURR_ID
                                            AND dc2.SettleDate>=r.DATE
                                            ORDER BY r.DATE DESC FETCH FIRST 1 ROWS ONLY)
                                        ,1)
                                END AS CURS

                               FROM PFR_BASIC.DEPOSIT d
                                   inner join PFR_BASIC.DEPCLAIM_OFFER o ON o.ID = d.DEPCLAIM_OFFER_ID
                                   INNER JOIN PFR_BASIC.DEPCLAIM2 dc2 ON o.DEPCLAIM_ID=dc2.ID
                                   INNER JOIN PFR_BASIC.OFFER_PFRBANKACCOUNT_SUM s ON s.OFFER_ID = o.ID AND s.STATUS>0
                                   INNER JOIN PFR_BASIC.PORTFOLIO p ON s.PORTFOLIO_ID=p.ID
                                   LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID
                                   LEFT JOIN PFR_BASIC.PFRBANKACCOUNT b ON s.PFRBANKACCOUNT_ID=b.ID
                                   INNER JOIN PFR_BASIC.PAYMENT_HISTORY ph ON ph.OFFER_PFRBANKACCOUNT_SUM_ID = s.ID
                               where
                                   ph.FOR_PERCENT = 1 and p.ID=:pid", true);
                        lst3.ForEach(
                            a => nList.Add(new AccOpListItem
                            {
                                ID = (long) a[0],
                                Summ = Convert.ToDecimal(a[1]) / (IsDB2 && a[1] is decimal ? 100m : 1m),
                                Curs = Convert.ToDecimal(a[2]) / (IsDB2 && a[2] is decimal ? 100m : 1m)
                            }));
                        return nList;
                    default:
                        throw new Exception("Unknown mode!");
                }
            });
        }

        private List<object[]> GetTransfersForChfr(long portfolioID, int selectedYear, int selectedQuarter, string query, bool plainSql)
        {
         //   var fromMonth = DateTools.GetFirstMonthOfQuarterForBO(selectedQuarter);
          //  var toMonth = DateTools.GetLastMonthOfQuarterForBO(selectedQuarter);
          //  var toYear = toMonth == 1 ? (selectedYear + 1) : selectedYear;

         //   var from = new DateTime(selectedYear, fromMonth, 1);
         //   var to = new DateTime(toYear, toMonth, 1);

            return SessionWrapper(session =>
            {
                if (plainSql)
                {
                    return session.CreateSQLQuery(query)
                   // .SetParameter("sdate", from)
                   // .SetParameter("edate", to)
                    .SetParameter("pid", portfolioID)
                    .List<object[]>().ToList();
                }
                return session.CreateQuery(query)
                    // .SetParameter("sdate", from)
                    // .SetParameter("edate", to)
                    .SetParameter("pid", portfolioID)
                    .List<object[]>().ToList();
            });
        }


        private void DeleteTransferManyInternal(List<SITransferDeleteListItem> transferListIds, ISession session)
		{
			foreach (var item in transferListIds)
			{
				var sitId = item.SITransferId;
				var reqTransferList = GetReqTransfersForSITransfer(item.SITransferId, session).Select(a => new ReqTransferDeleteListItem
				{
					ReqTransferId = a.ID,
					ContractId = sitId,
					IsActSigned = TransferStatusIdentifier.IsStatusActSigned(a.TransferStatus)
				}).ToList();
				DeleteReqTransferManyInternal(reqTransferList, session);
			}
			//получаем список ID сп. перечислений
			var stIds = transferListIds.Select(a => a.SITransferId).ToList();
			//получаем строковый эквивалент списка ID
			var idStr = GenerateStringFromList(stIds);
			//delete SIUKPLANs
			session.CreateSQLQuery($"update {DATA_SCHEME}.{TABLE_SI_UKPLAN} set status_id = -1 where SI_TR_ID IN ({idStr})")
				.ExecuteUpdate();
			//delete SItransfers
			DeleteEntities<SITransfer>(stIds, session);
		}

		private void DeleteTransferSingleInternal(List<ReqTransferDeleteListItem> reqTransferList, long transferListId, ISession session)
		{
			DeleteReqTransferManyInternal(reqTransferList, session);
			/*foreach (var item in reqTransferList)
			{
				DeleteReqTransferInternal(item.ReqTransferId, item.ContractId, item.IsActSigned, session);
			}*/
			//delete SIUKPLANs
			session.CreateSQLQuery($"update {DATA_SCHEME}.{TABLE_SI_UKPLAN} set status_id = -1 where SI_TR_ID = {transferListId}")
				.ExecuteUpdate();
			//delete SItransfer
			DeleteEntity<SITransfer>(transferListId, session);
		}



		/// <summary>
		/// Удаление записи реестра СИ/ВР под транзакцией
		/// </summary>
		/// <param name="list">Коллекция списков перечислений</param>
		public void DeleteSIVRRegister(List<SITransfer> list)
		{
			if (!list.Any()) return;

			TransactionWrapper(session =>
			{
				DeleteTransferManyInternal(list.Select(a => new SITransferDeleteListItem
				{
					SITransferId = a.ID,
					ContractId = a.ContractID
				}).ToList(), session);

				/*foreach (var item in list)
				{
					//выборка перечислений для списка пеерчислений
					var reqTransfers = GetReqTransfersForSITransfer(item.ID, session);

					var trList = item;
					//удаление списка перечислений
					DeleteTransferSingleInternal(
						reqTransfers.Select(
							a => new ReqTransferDeleteListItem()
							{
								ReqTransferId = a.ID,
								ContractId = trList.ID,
								IsActSigned = TransferStatusIdentifier.IsStatusActSigned(a.TransferStatus)
							}).ToList(), trList.ID, session);
				}
				*/
				//delete register
				DeleteEntity<SIRegister>(list.First().TransferRegisterID.Value, session);
			});
		}
#endregion

        public void UpdateAuctionWizardStep(long auctionID, int auctionStep)
        {
            TransactionWrapper(session => session.CreateQuery("update DepClaimSelectParams d set d.Step=:step where d.ID=:id")
                .SetParameter("step", auctionStep)
                .SetParameter("id", auctionID)
                .ExecuteUpdate());
        }

        

    }
}




