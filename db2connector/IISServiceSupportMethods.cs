﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Linq;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;
#if WEBCLIENT
using NHibernate.Util;
#endif

namespace db2connector
{
    public partial class IISServiceHib
    {
        #region Auxilary methods

        /// <summary>
        /// Генерирует строку значений из списка, с указанным разделителем
        /// </summary>
        /// <typeparam name="T">Тип значений</typeparam>
        /// <param name="list">Список значений</param>
        /// <param name="delimiter">Разделитель</param>
        /// <param name="isString"></param>
        /// <returns></returns>
        internal static string GenerateStringFromList<T>(IEnumerable<T> list, string delimiter = ",", bool isString = false)
        {
            if (list == null || !list.Any()) return string.Empty;
            var sb = new StringBuilder();
            list.ForEach(a =>
            {
                sb.Append(isString ? $"'{a}'{delimiter}" : $"{a}{delimiter}" );;
            });
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
        #endregion

        #region Entity operations
        /// <summary>
        /// Удаление сущности с учетом интерфейса IMarkedAsDeleted
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="entity">Сущность</param>
        /// <param name="session">Сессия</param>
        internal static void DeleteEntity<T>(T entity, ISession session)
        {
            if (entity == null) return;
            var toDelete = entity as IMarkedAsDeleted;
            if (toDelete != null)
            {
                toDelete.StatusID = -1;
                session.Update(toDelete);
            }
            else session.Delete(entity);
        }

        /// <summary>
        /// Удаление сущности по Id с учетом интерфейса IMarkedAsDeleted
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идентификатор</param>
        /// <param name="session">Сессия</param>
        internal static void DeleteEntity<T>(long id, ISession session)
        {
            DeleteEntities<T>(new[] { id }, session);
        }

        /// <summary>
        /// Удаление списка сущностей по Id с учетом интерфейса IMarkedAsDeleted
        /// </summary> <typeparam name="T">Тип сущности</typeparam>
        /// <param name="idList">Список идентификаторов</param>
        /// <param name="session">Сессия</param>
        internal static void DeleteEntities<T>(IEnumerable<long> idList, ISession session)
        {
            if (idList == null || !idList.Any()) return;
            var tableName = DataAccessSystem.GetClassTableName(typeof(T));

            bool correctStatus = DataContainerFacade.WrongStatusTypesList.ContainsKey(typeof (T));


            if (typeof(IMarkedAsDeleted).IsAssignableFrom(typeof(T)))
            {
                //update status
                session.CreateSQLQuery(string.Format(@"update pfr_basic.{0} set {2} = -1 where ID in ({1})", tableName, GenerateStringFromList(idList), correctStatus ? "status" : "status_id"))
                    .ExecuteUpdate();
            }
            else
            {
                session.CreateSQLQuery($@"delete from pfr_basic.{tableName} where ID in ({GenerateStringFromList(idList)})")
                    .ExecuteUpdate();
            }

			//применяем изменения в базу
			//session.Flush();
        }

        internal static void RestoreEntity<T>(long id, ISession session, int statusValue = 1)
        {
            RestoreEntities<T>(new List<long> {id}, session, statusValue);
        }
        internal static void RestoreEntities<T>(IEnumerable<long> idList, ISession session, int statusValue = 1)
        {
            if (idList == null || !idList.Any()) return;
            var tableName = DataAccessSystem.GetClassTableName(typeof(T));

            bool correctStatus = DataContainerFacade.WrongStatusTypesList.ContainsKey(typeof (T));


            if (typeof(IMarkedAsDeleted).IsAssignableFrom(typeof(T)))
            {
                //update status
                session.CreateSQLQuery(string.Format(@"update pfr_basic.{0} set {2} = {3} where ID in ({1})", tableName, GenerateStringFromList(idList), correctStatus ? "status" : "status_id", statusValue))
                    .ExecuteUpdate();
            }
        }

        /// <summary>
        /// Сохранение списка сущностей в рамках сессии
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="list">Список объектов</param>
        /// <param name="session">Сессия</param>
        internal void SaveEntities<T>(IEnumerable<T> list, ISession session)
        {
            list.ForEach(a=> SaveOnlyEntity(a, session));
        }

        internal T GetEntity<T>(long id)
        {
            return (T)GetByID(typeof(T).FullName, id);
        }

        /// <summary>
        /// Сохранение сущности в БД через хибер с выставлением статуса (порт с факада)
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <typeparam name="TId">Тип идентификатора</typeparam>
        /// <param name="entity">Сущность</param>
        /// <param name="session">Опционально. Работа в рамках существующей сессии</param>
        internal TId SaveEntity<T, TId>(T entity, ISession session = null)
        {
            var asDeleted = entity as IMarkedAsDeleted;
            if (asDeleted != null && asDeleted.StatusID == 0)
            {                
                asDeleted.StatusID = 1;
                var type = entity.GetType();
                if (DataContainerFacade.WrongStatusTypesList.ContainsKey(type))
                    asDeleted.StatusID = DataContainerFacade.WrongStatusTypesList[type].DefaultActiveStatus;
            }

            var o = entity as BaseDataObject;
            if (o != null)
                o.ExtensionData = null;//Зачищаем

            return session == null
                ? (TId) Save(typeof (T).FullName, entity)
                : (TId) Save(entity, session);
        }

        /// <summary>
        /// Сохранение сущности в БД через хибер с выставлением статуса (порт с факада)
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="entity">Сущность</param>
        /// <param name="session">Опционально. Работа в рамках существующей сессии</param>
        internal long SaveEntity<T>(T entity, ISession session = null)
        {
            return SaveEntity<T, long>(entity, session);
        }

        internal long SaveEntityTransact<T>(T entity, ISession session)
        {
            using (var transaction = session.BeginTransaction())
            {
                var result = SaveEntity<T, long>(entity, session);
                transaction.Commit();
                return result;
            }
        }

        internal void SaveOnlyEntity<T>(T entity, ISession session = null)
        {
            var asDeleted = entity as IMarkedAsDeleted;
            if (asDeleted != null && asDeleted.StatusID == 0)
            {
                asDeleted.StatusID = 1;
                var type = entity.GetType();
                if (DataContainerFacade.WrongStatusTypesList.ContainsKey(type))
                    asDeleted.StatusID = DataContainerFacade.WrongStatusTypesList[type].DefaultActiveStatus;            
            }

            var o = entity as BaseDataObject;
            if (o != null)
                o.ExtensionData = null;//Зачищаем

            if(session == null)
                Save(typeof(T).FullName, entity);
            else SaveOnly(entity, session);
        }

        #endregion

        #region Internal facade extensions

        /// <summary>
        /// Метод сохранения сущности в существующей сессии для участия в сложных запросах
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <param name="session">Сессия</param>
        internal object Save(object entity, ISession session)
        {
            session.SaveOrUpdate(entity);
            return session.GetIdentifier(entity);
        }

        internal void SaveOnly(object entity, ISession session)
        {
            session.SaveOrUpdate(entity);
        }

        /// <summary>
        /// Получить кол-во записей по указанному свойству
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="propertyName">Название свойства</param>
        /// <param name="value">Значение свойства</param>
        /// <param name="session">Опционально. Существующая сессия, в рамках которой провести запрос</param>
        internal long GetListByPropertyCount<T>(string propertyName, object value, ISession session = null)
        {
            return GetListByPropertyCount(typeof(T).FullName, propertyName, value, session);
        }

        /// <summary>
        /// Получить кол-во записей по указанному свойству
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="entity">Сущность</param>
        /// <param name="propertyName">Название свойства</param>
        /// <param name="value">Значение свойства</param>
        /// <param name="session">Опционально. Существующая сессия, в рамках которой провести запрос</param>
        internal long GetListByPropertyCount<T>(T entity, string propertyName, object value, ISession session = null)
        {
            return GetListByPropertyCount(typeof(T).FullName, propertyName, value, session);
        }

        /// <summary>
        /// Получает кол-во элементов в свойстве-коллекции
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="id">Идентификатор</param>
        /// <param name="propertyName">Наименование свойства</param>
        /// <param name="session">Опционально. Существующая сессия, в рамках которой провести запрос</param>
        /// <returns></returns>
        internal long GetObjectPropertyCount<T>(object id, string propertyName, ISession session = null)
        {
            return GetObjectPropertyCount(typeof (T).FullName, id, propertyName, session);
        }

        /// <summary>
        /// Получает список сущностей по значению свойства
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="propertyName">Наименование свойства</param>
        /// <param name="value">Значение свойства</param>
        /// <param name="session">Опционально. Существующая сессия, в рамках которой провести запрос</param>
        internal IList GetListByProperty<T>(string propertyName, object value, ISession session)
        {
            return GetListByProperty(typeof(T).FullName, propertyName, value, session);
        }

        /// <summary>
        /// Получает список идентификаторов по значению свойства сущностей указанного типа 
        /// (тип сущности должен быть описан в маппинге хибера)
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <param name="propertyName">Наименование свойства</param>
        /// <param name="value">Значение свойства</param>
        /// <param name="session">Сессия</param>
        internal List<long> GetIdListByProperty<T>(string propertyName, object value, ISession session)
        {
            return session.CreateQuery($@"select tab.ID from {typeof(T).Name} tab where tab.{propertyName} = :param")
                .SetParameter("param", value)
                .List<long>()
                .ToList();
        }

        /// <summary>
        /// Получает список идентификаторов по значению свойства, соотв. списку значений
        /// (тип сущности должен быть описан в маппинге хибера)
        /// </summary>
        /// <typeparam name="T">Тип сущности</typeparam>
        /// <typeparam name="TProp">Тип значения свойства</typeparam>
        /// <param name="propertyName">Наименование свойства</param>
        /// <param name="value">Список значений свойства</param>
        /// <param name="session">Сессия</param>
        internal List<long> GetIdListByPropertyIn<T, TProp>(string propertyName, IEnumerable<TProp> value, ISession session)
        {
            if(value == null || !value.Any()) return new List<long>();
            return session.CreateQuery($@"select tab.ID from {typeof(T).Name} tab where tab.{propertyName} in ({GenerateStringFromList(value)})")
                .List<long>()
                .ToList();
        }


        /// <summary>
        /// Получает значение свойства сущности
        /// </summary>
        /// <typeparam name="TType">Тип сущности</typeparam>
        /// <typeparam name="TReturnType">Тип свойства (возвращаемого значения)</typeparam>
        /// <param name="id">Идентификатор</param>
        /// <param name="propertyName">Наименование свойства</param>
        /// <param name="session">Опционально. Существующая сессия, в рамках которой провести запрос</param>
        internal TReturnType GetObjectProperty<TType, TReturnType>(object id, string propertyName, ISession session = null)
        {
            return (TReturnType)GetObjectPropertyInternal(typeof(TType).FullName, id, propertyName, session);
        }

        internal IList GetListByPropertyConditions<T>(ISession session, params ListPropertyCondition[] propertyConditionList)
        {
            return GetPropertyConditionsCriteria(session.CreateCriteria(typeof(T)), propertyConditionList.ToList()).List();
        }

        internal IList GetListByPropertyConditions<T>(params ListPropertyCondition[] propertyConditionList)
        {
            return GetListByPropertyConditions(typeof(T).FullName, propertyConditionList.ToList());
        }

        #endregion

        /// <summary>
        /// Универсальная обертка под сессию с транзакцией
        /// </summary>
        /// <typeparam name="T">Тип возвращаемых данных</typeparam>
        /// <param name="method">Метод для вызова</param>
        internal static T TransactionWrapper<T>(Func<ISession, T> method)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var result = method(session);
                    transaction.Commit();
                    return result;
                }
            }
        }

        internal static T TransactionWrapper<T>(Func<ISession, T> method, IsolationLevel isolation)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction(isolation))
                {
                    var result = method(session);
                    transaction.Commit();
                    return result;
                }
            }
        }

        /// <summary>
        /// Обертка, отменяющая транзакцию, если метод возвращает false
        /// </summary>
        /// <param name="method"></param>
        internal static void TransactionWrapperSafe (Func<ISession, bool> method)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var result = method(session);
                    if(result)
                        transaction.Commit();
                }
            }
        }

        internal static void TransactionWrapper(Action<ISession> method)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    method(session);
                    transaction.Commit();
                }
            }
        }

        internal static void TransactionWrapper(Action<ISession> method, IsolationLevel isolation)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                using (var transaction = session.BeginTransaction(isolation))
                {
                    method(session);
                    transaction.Commit();
                }
            }
        }

        internal static T SessionWrapper<T>(Func<ISession, T> method, bool clearSession = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (clearSession)
                    session.Clear();
                return method(session);
            }
        }

        internal static void SessionWrapper(Action<ISession> method, bool clearSession = false)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                if (clearSession)
                    session.Clear();
               method(session);
            }
        }

        internal static T StatelessSessionWrapper<T>(Func<IStatelessSession, T> method)
        {
            using (var session = DataAccessSystem.OpenStatelessSession())
            {
                return method(session);
            }
        }

    }
}
