﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Persister.Entity;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Proxy.Tools;

namespace db2connector
{
    /// <summary>
    /// Часть сервиса - реализация журналирования
    /// </summary>
    public partial class IISServiceHib
    {
        public string DomainUser { get; set; }
        public string OsUser { get; set; }
        public string HostName { get; set; }

        private static readonly Type[] JournalTypes = { typeof(Audit), typeof(AuditEvent), typeof(AuditInterface), typeof(AuditUI) };

        public string GetCurrentUserLogin()
        {
            return DomainUser;
        }

        public void SysLog(string userName, string eventName, string resultName, string detailMessage, string hostName, LogSeverity severity = LogSeverity.Info)
        {
            LogManager.LogLeefSysLogEx((IISService) this, userName, eventName, resultName, detailMessage, hostName, severity);
        }

        /// <summary>
        /// Отслеживание дисконнекта пользователя с целью логирования отключения клиента
        /// </summary>
        protected override void TraceClientDisconnect()
        {
            try
            {
                OperationContext.Current.Channel.Faulted += Channel_Faulted;
                OperationContext.Current.Channel.Closed += Channel_Closed;
                //OperationContext.Current.Channel.Close();
            }
            catch
            {
                // ignored
            }
        }

        private void Channel_Closed(object sender, EventArgs e)
        {
            lock (Locker)
            {
                var sessionId = OperationContext.Current == null ? (string.IsNullOrEmpty(LastKnownSessionId) ? null : LastKnownSessionId) : OperationContext.Current.SessionId;
                if (sessionId != null)
                {
                    if (!SessionsDataList.ContainsKey(sessionId) || SessionsDataList[sessionId].IsCloseOpLogged) return;
                    SaveJornalLog(JournalEventType.LOGOUT, "По закрытию соединения", 0, string.Empty);
                    SessionsDataList[sessionId].IsCloseOpLogged = true;
                }
            }
        }

        private void Channel_Faulted(object sender, EventArgs e)
        {
            lock (Locker)
            {
                var sessionId = OperationContext.Current == null ? (string.IsNullOrEmpty(LastKnownSessionId) ? null : LastKnownSessionId) : OperationContext.Current.SessionId;
                if (sessionId != null)
                {
                    if (!SessionsDataList.ContainsKey(sessionId) || SessionsDataList[sessionId].IsCloseOpLogged) return;
                    SaveJornalLog(JournalEventType.LOGOUT, "По ошибке соединения", 0, string.Empty);
                    SessionsDataList[sessionId].IsCloseOpLogged = true;
                }
            }

        }

        /// <summary>
        /// Метод для выбора сессии в зависимости от типа (типы аудита хранятся в другой базе). Нужен для универсальных методов.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static ISession OpenSessionByType(Type type)
        {
            return JournalTypes.Contains(type) ? DataAccessSystem.OpenSessionJournal() : DataAccessSystem.OpenSession();
        }

        public bool SetCurrentUserInformation(string domainUser, string osUser, string hostName)
        {
            DomainUser = domainUser;
            OsUser = osUser;
            HostName = hostName;
            return true;
        }

        public void SaveJornalLogWithAdditionalInfo(JournalEventType eventCode, string objectName, long? documentId, string objectTypeName, string additionalInfo)
        {
            SaveToJornalLog(eventCode, objectName, documentId, objectTypeName, additionalInfo);
        }

        public override void SaveJornalLog(JournalEventType eventCode, string objectName, long? documentId, string objectTypeName)
        {
            SaveToJornalLog(eventCode, objectName, documentId, objectTypeName, null);
        }

        private void SaveToJornalLog(JournalEventType eventCode, string objectName, long? documentId, string objectTypeName, string additionalInfo)
        {
            try
            {
                if (!DataAccessSystem.HasJournal) return;
                using (var session = OpenSessionByType(typeof(Audit)))
                {
                    //транзация для обхода внутр. исключения хибернейта
                    var transaction = session.BeginTransaction();

                    var audit = new Audit
                    {
                        UserID = DomainUser,
                        OsID = OsUser,
                        HostName = HostName,
                        EventCode = eventCode.GetDescription(),
                        ObjectName = objectName,
                        ObjectType = objectTypeName,
                        AdditionalInfo = additionalInfo,
                        DocumentID = documentId,
                        EventDate = DateTime.Now.Date,
                        EventTime = DateTime.Now.TimeOfDay
                    };
                    session.SaveOrUpdate(audit);
                    transaction.Commit();	
                    session.Flush();
                }
            }
            catch(Exception ex)
            {
                LogManager.LogException(ex);
            }
        }

        /// <summary>
        /// Сохранение детального события по действию с карточкой объекта
        /// </summary>
        /// <param name="eventCode">Тип события</param>
        /// <param name="objectName">Сообщение</param>
        /// <param name="documentId">ID документа</param>
        /// <param name="objectTypeName">Наименование типа модели</param>
        /// <param name="additionalInfo">Дополнительная текстовая информация</param>
        /// <param name="entityType">Тип объекта карточки для получения доп. информации на стороне сервиса</param>
        /// <param name="entityDesc"></param>       
        public void SaveDetailedJornalLog(JournalEventType eventCode, string objectName, long? documentId, string objectTypeName, string additionalInfo, string entityType, string entityDesc = null)
        {
            if (entityType != null)
            {
                var itype = GetDataObjectType(entityType);
                var meta = DataAccessSystem.GetClassMetadataSafe(itype) as AbstractEntityPersister;
                if(meta != null)
                {
                    var identifierName = meta.HasIdentifierProperty ? meta.IdentifierPropertyName : "";
                    var idText = (documentId == null || documentId == 0) ? "" : $"Идентификатор({identifierName})={documentId}";
                    additionalInfo += $"Таблица: {meta.TableName} {idText}";
                }
            }
            if(!string.IsNullOrEmpty(entityDesc))
                additionalInfo += $"{(string.IsNullOrEmpty(additionalInfo) ? "" : " \n")}{entityDesc}";
            SaveToJornalLog(eventCode, objectName, documentId, objectTypeName, additionalInfo);
        }

        //TODO возможно есть более простой способ выбрать тип объекта по имени, но GetType требует полное имя с неймспейсом
        private Type GetDataObjectType(string entityType)
        {
           var assembly = typeof(Finregister).Assembly;
           var nameSpace = typeof(Finregister).Namespace;
           var itype = assembly.GetType(nameSpace + "." + entityType);
           if (itype != null) return itype;
           nameSpace = typeof(DepClaimMaxListItem).Namespace;
           itype = assembly.GetType(nameSpace + "." + entityType);
           return itype;
        }


        public IList<string> GetJournalEventObjectList()
        {
            if (!DataAccessSystem.HasJournal) return new List<string>();
            using (var session = OpenSessionByType(typeof(Audit)))
            {
                return session.CreateQuery("select distinct a.ObjectName from Audit a").List<string>();
            }
        }

        public IList<Audit> GetJournalLog(JournalFilter filter)
        {
            //throw new Exception();
            if (!DataAccessSystem.HasJournal) return new List<Audit>();
            using (var session = OpenSessionByType(typeof(Audit)))
            {
                filter = filter ?? new JournalFilter();
                var criteria = session.CreateCriteria<Audit>();

                if (!string.IsNullOrEmpty(filter.UserID))
                    criteria.Add(Restrictions.InsensitiveLike("UserID", $"%{filter.UserID}%"));
                if (!string.IsNullOrEmpty(filter.OsID))
                    criteria.Add(Restrictions.InsensitiveLike("OsID", $"%{filter.OsID}%"));
                if (!string.IsNullOrEmpty(filter.HostName))
                    criteria.Add(Restrictions.InsensitiveLike("HostName", $"%{filter.HostName}%"));
                if (!string.IsNullOrEmpty(filter.ObjectName))
                    criteria.Add(Restrictions.Eq("ObjectName", filter.ObjectName));
                if (!string.IsNullOrEmpty(filter.EventType))
                    criteria.Add(Restrictions.Eq("EventCode", filter.EventType));

                if (filter.DateFrom.HasValue)
                    criteria.Add(Restrictions.Or(Restrictions.Gt("EventDate", filter.DateFrom.Value.Date),
                        Restrictions.And(Restrictions.Eq("EventDate", filter.DateFrom.Value.Date),
                            Restrictions.Gt("EventTime", filter.DateFrom.Value.TimeOfDay))));
                if (filter.DateTo.HasValue)
                    criteria.Add(Restrictions.Or(Restrictions.Lt("EventDate", filter.DateTo.Value.Date),
                        Restrictions.And(Restrictions.Eq("EventDate", filter.DateTo.Value.Date),
                            Restrictions.Lt("EventTime", filter.DateTo.Value.TimeOfDay))));

                return criteria.List<Audit>();
            }
        }
    }
}
