using System.Configuration;
using System.Reflection;
using System.Web.Configuration;

namespace PFR_INVEST.SettingsService.ProvidersBase
{
    public class UserSettingsProviderService
    {
        private static UserSettingsProviderBase _provider;
        private static UserSettingsProviderCollection _providers;

        private static readonly object SyncLock = new object();

        public static string LoadUserSettings(string domain, string userName)
        {
            LoadProviders();
            return _provider.LoadUserSettings(domain, userName);
        }

        public static void SaveUserSettings(string domain, string userName, string settings)
        {
            LoadProviders();
            _provider.SaveUserSettings(domain, userName, settings);
        }

        private static void LoadProviders()
        {
            if (_provider != null) return;

            lock (SyncLock)
            {
                if (_provider != null) return;

                var section = (UserSettingsProviderServiceSection)
                              WebConfigurationManager.GetSection("system.web/userSettingsService");

                _providers = new UserSettingsProviderCollection();

                foreach (var providerSettings in section.Providers)
                {
                    var providerType = Assembly.GetExecutingAssembly().GetType(((ProviderSettings)providerSettings).Type);
                    if (providerType == null) continue;

                    var provider =
                        ProvidersHelper.InstantiateProvider(((ProviderSettings)providerSettings), providerType) as
                        UserSettingsProviderBase;
                    if (provider == null) continue;

                    provider.InitializeCustomProperties(((ProviderSettings)providerSettings).Parameters);
                    _providers.Add(provider);
                }

                _provider = _providers[section.DefaultProvider];
            }
        }
    }
}