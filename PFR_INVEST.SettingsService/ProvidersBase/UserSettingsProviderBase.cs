using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Linq;
using System.Reflection;

namespace PFR_INVEST.SettingsService.ProvidersBase
{
    public abstract class UserSettingsProviderBase : ProviderBase
    {
        public abstract void SaveUserSettings(string domain, string userName, string settings);

        public abstract string LoadUserSettings(string domain, string userName);

        public void InitializeCustomProperties(NameValueCollection parameters)
        {
            var providerProperties = GetType().GetProperties();
            foreach (var parameterKey in parameters.AllKeys)
            {
                var property = FindProperty(providerProperties, parameterKey);
                if (property == null) continue;
                property.SetValue(this, parameters[parameterKey], null);
            }
        }

        private static PropertyInfo FindProperty(IEnumerable<PropertyInfo> providerProperties, string parameterKey)
        {
            return providerProperties.Where(
                p =>
                    {
                        var attributes =
                            p.GetCustomAttributes(typeof (ConfigurationPropertyAttribute), true);
                        if (attributes.Count() == 0) return false;

                        var customAttributes = attributes.Cast<ConfigurationPropertyAttribute>();
                        return customAttributes.Any(c => c.Name == parameterKey);
                    }).FirstOrDefault();
        }
    }
}