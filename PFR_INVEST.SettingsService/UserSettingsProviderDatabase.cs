using System;
using System.Configuration;
using PFR_INVEST.SettingsService.ProvidersBase;

namespace PFR_INVEST.SettingsService
{
    public class UserSettingsProviderDatabase : UserSettingsProviderBase
    {
        [StringValidator(MinLength = 1)]
        [ConfigurationProperty("connectionString", Options = ConfigurationPropertyOptions.IsRequired)]
        public string ConnectionString { get; set; }

        public override void SaveUserSettings(string domain, string userName, string settings)
        {
            throw new NotImplementedException();
        }

        public override string LoadUserSettings(string domain, string userName)
        {
            throw new NotImplementedException();
        }
    }
}