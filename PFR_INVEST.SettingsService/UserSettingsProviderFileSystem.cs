using System.Configuration;
using System.IO;
using PFR_INVEST.SettingsService.ProvidersBase;
using System;

namespace PFR_INVEST.SettingsService
{
    public class UserSettingsProviderFileSystem : UserSettingsProviderBase
    {
        [StringValidator(MinLength = 1)]
        [ConfigurationProperty("rootDirectory", Options = ConfigurationPropertyOptions.IsRequired)]
        public string RootDirectory { get; set; }

        public override void SaveUserSettings(string domain, string userName, string settings)
        {			
			var filePath = Path.Combine(RootDirectory, domain);
            var fileName = Path.Combine(filePath, string.Format("{0}.settings", userName));

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            File.WriteAllText(fileName, settings);
        }

        public override string LoadUserSettings(string domain, string userName)
        {
            var filePath = Path.Combine(RootDirectory, domain);
            var fileName = Path.Combine(filePath, string.Format("{0}.settings", userName));

            if (File.Exists(fileName))
                return File.ReadAllText(fileName);
            return null;
        }
    }
}