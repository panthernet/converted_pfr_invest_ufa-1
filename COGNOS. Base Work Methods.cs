#region 1. Автоматическое обновление страницы промпта при выборе радиокнопок

1. Добавляем HTML блок после группы радиокнопок
2. Прописываем скрипт:

<script language="javascript">
//ищем все элементы input
var inputs = document.getElementsByTagName("input");
var list_box = new Array();

//выбираем только radio
var radio_buttons = new Array();
j=0;
for(i=0;i<inputs.length;i++)
{
    if(inputs[i].type=='radio')
    {
        radio_buttons[j] = inputs[i];
        j++;
    }   
}
//устанавливаем функцию по клику - СКОЛЬКО ТОЧЕК СТОЛЬКО И ЗАПИСЕЙ!
radio_buttons[0].setAttribute("onclick",function(){promptAction('reprompt');});
radio_buttons[1].setAttribute("onclick",function(){promptAction('reprompt');});

</script>

3. Индекс элемента radio_buttons используем в зависимости от количества радиокнопок

#endregion

#region 2. Автоматическое обновление страницы промпта при выборе комбобокса

1. Добавляем HTML блок после комбобокса
2. Прописываем скрипт:

<script language="javascript">
var f = getFormWarpRequest();
var list = f._oLstChoiceslstType;
list.onchange= function (){promptAction('reprompt')};
</script>

#endregion

#region 3. Удаление дефолтового значения комбобокса

1. Добавляем HTML блок после комбобокса
2. Прописываем скрипт:

<script language="javascript">
var f = getFormWarpRequest();
var list = f._oLstChoiceslstType;
list.remove(0);
list.removeAttribute("hasLabel"); //УБИРАЕТ ОБЯЗАТЕЛЬНОСТЬ ВВОДА! И красную маркировку при невыбранном значении
</script>

#endregion

#region 4. Проблемы с пересечением таблиц в FrameworkManager
1. ПКМ на модель, выбрать Show object dependencies...
2. Проверить какие имеются relations
3. ПКМ на модель, Create -> Relation создать новую связь таблиц по ID, например как ADDSPN связана с ADDSPN_TYPE через DOCKIND<->ID. Что дает возможность использовать поля
  из ADDSPN_TYPE в модели ADDSPN без конфликта пересечений таблиц
#endregion

#region RDLC обнуление цифрр в суммах длиннойболее 15 знаков
//https://blogs.msdn.microsoft.com/nav/2014/03/25/formatted-decimal-values-dropping-symbols-in-rdlc/
//Нужно написать свою функцию форматирования и использовать Expression в ячейках
#endregion

#region RDLC - формат сумм в ячейчах для экспорта Word
//Нужно принудительно выставлять форматирование значения в выраджении и убирать форматирование поля ввода (Default)
//=Code.FormatDecimalString(Fields!IncomeSpnLeft.Value,"#,0.00;-#,0.00")
//Public Function FormatDecimalString(ByVal Amount as Decimal, ByVal FormatString as String) as String
//  Dim CultureInfo as New System.Globalization.CultureInfo("ru-RU")
//  Return Amount.ToString(FormatString, CultureInfo)
//End Function
#endregion