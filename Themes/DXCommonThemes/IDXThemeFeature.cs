﻿using System;

namespace DXCommonThemes
{
    public interface IDXThemeFeature : IDisposable
    {
        void Start();
        void Stop();
    }
}
