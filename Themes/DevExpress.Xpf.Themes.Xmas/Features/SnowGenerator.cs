﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using DXCommonThemes;

namespace DevExpress.Xpf.Xmas.Features
{
    public class SnowGenerator : IDXThemeFeature
    {
        private bool _isRunning;
        private DispatcherTimer _timer;
        private Canvas _layoutRoot;
        public int Interval { get; set; }
        readonly Random _random = new Random(Guid.NewGuid().GetHashCode());

        void IDXThemeFeature.Start()
        {
            try
            {
                if (_isRunning) return;
                _isRunning = true;
                _timer = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(Interval)};
                _timer.Tick += (s, arg) => Snow();
                _timer.Start();
            }
            catch
            {
                // ignored
            }
        }


        void IDXThemeFeature.Stop()
        {
            try
            {
                _isRunning = false;
                if (_timer != null)
                    _timer.Stop();
                if (_layoutRoot != null)
                    _layoutRoot.Children.Clear();
            }
            catch
            {
                // ignored
            }
        }

        public SnowGenerator(Canvas layoutRoot, int interval = 300)
        {
            if(layoutRoot == null)
                throw new Exception("Invalid layout root specified! NULL");
            Interval = interval;
            _layoutRoot = layoutRoot;
        }


        private void Snow()
        {
            try
            {
                var width = (int)_layoutRoot.ActualWidth;
                if (width == 0) return;
                var x = _random.Next(-50, width);
                var y = -100;
                var s = _random.Next(5, 15)*.1;
                var r = _random.Next(0, 270);
                var grp = new TransformGroup();
                grp.Children.Add(new ScaleTransform(s, s));
                grp.Children.Add(new RotateTransform(r));
                var flake = new Snowflake
                {
                    RenderTransform = grp,
                };
                _layoutRoot.Children.Add(flake);
                Canvas.SetLeft(flake, x);
                Canvas.SetTop(flake, y);

                var d = TimeSpan.FromSeconds(_random.Next(6, 10));

                x = _random.Next(50, width - 50);
                var ax = new DoubleAnimation {To = x, Duration = d};
                Storyboard.SetTarget(ax, flake);
                Storyboard.SetTargetProperty(ax, new PropertyPath("(Canvas.Left)"));

                y = (int) (_layoutRoot.ActualHeight + 100 + 100);
                var ay = new DoubleAnimation {To = y, Duration = d};
                Storyboard.SetTarget(ay, flake);
                Storyboard.SetTargetProperty(ay, new PropertyPath("(Canvas.Top)"));

                r += _random.Next(90, 360);
                var ar = new DoubleAnimation {To = r, Duration = d};
                Storyboard.SetTarget(ar, flake);
                Storyboard.SetTargetProperty(ar, new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[1].(RotateTransform.Angle)"));

                var story = new Storyboard();
                story.Completed += (sender, e) => _layoutRoot.Children.Remove(flake);
                story.Children.Add(ax);
                story.Children.Add(ay);
                story.Children.Add(ar);
                story.Begin();
            }
            catch
            {
                // ignored
            }
        }

        public void Dispose()
        {
            _layoutRoot = null;
        }
    }
}
