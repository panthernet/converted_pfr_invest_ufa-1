﻿using log4net;

namespace PFR_INVEST.Auth.ECASA
{
    public static class Logs
    {
        internal static readonly ILog Log = LogManager.GetLogger("Auth");

    }
}
