﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.Auth.ServerData
{
    public interface IAuthConnector
    {
        ServiceAuthMsg Init();

        ServiceAuthMsg ChangeCurrentUserPassword(string userName, string oldPassword, string password);

        object GetToken();

        bool IsValidRequest(object token);

        List<User> GetGroupChildUsers(string value1);

        List<Role> GetRoles(string userName = null);

        bool IsAdmin();

        User AddUserToGroup(string userName, string groupName);

        User RemoveUserFromGroup(string userName, string groupName);

        List<User> GetUsersNotInGroup(string groupName);

        bool IsUserInRole(string userName, string role);
        
        User IsUserExist(string dn);

        User GetUserInfo(string userName);
    }
}
