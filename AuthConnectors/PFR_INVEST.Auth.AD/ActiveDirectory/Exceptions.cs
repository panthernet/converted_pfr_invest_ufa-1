﻿using System;

namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    public class ADInitException : Exception
    {
        public Exception Ex { get; set; }
    }
}
