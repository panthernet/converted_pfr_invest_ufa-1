﻿using System;
using System.DirectoryServices;
using System.Runtime.Serialization;
using PFR_INVEST.Auth.AD.ActiveDirectory.Extensions;

namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public class ADUser
    {
        public ADUser()
        {
            AccountControlFlags = ADUserAccountControlFlags.NORMAL_ACCOUNT |
                                  ADUserAccountControlFlags.DONT_EXPIRE_PASSWORD;
        }

        [DataMember]
        public Guid Guid { get; set; }
        [DataMember]
        public string Sid { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SamName { get; set; }
        [DataMember]
        public ADUserAccountControlFlags AccountControlFlags { get; set; }
        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Initials { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string CN { get; set; }

        [DataMember]
        public string StreetAddress { get; set; }
        [DataMember]
        public string PostOfficeBox { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string MobilePhone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string EMail { get; set; }
        [DataMember]
        public string WwwHomePage { get; set; }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Department { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string Manager { get; set; }
        [DataMember]
        public long PrimaryGroupID { get; set; }

        [DataMember]
        public ADGroup PrimaryGroup { get; set; }
        [DataMember]
        public string DN { get; set; }

        public static ADUser CreateFromEntry(DirectoryEntry userEntry, IActiveDirectory owner)
        {
            var user = new ADUser
               {
                   Guid = new Guid((byte[]) userEntry.Properties["objectGUID"].Value),
                   Sid = DirectoryEntryHelper.ConvertByteToStringSid((byte[]) userEntry.Properties["objectSid"].Value),
                   SamName = userEntry.GetStringValue("sAMAccountName"),
                   AccountControlFlags =
                       (ADUserAccountControlFlags)
                       Convert.ToUInt32(userEntry.Properties["userAccountControl"].Value),

                   CN = userEntry.GetStringValue("cn"),
                   Name = userEntry.GetStringValue("name"),
                   
                   FirstName = userEntry.GetStringValue("givenName"),
                   LastName = userEntry.GetStringValue("sn"),
                   Initials = userEntry.GetStringValue("initials"),
                   Description = userEntry.GetStringValue("description"),
                   
                   StreetAddress = userEntry.GetStringValue("streetAddress"),
                   PostOfficeBox = userEntry.GetStringValue("postOfficeBox"),
                   ZipCode = userEntry.GetStringValue("postalCode"),
                   City = userEntry.GetStringValue("l"),
                   State = userEntry.GetStringValue("st"),
                   CountryCode = userEntry.GetStringValue("c"),

                   Phone = userEntry.GetStringValue("telephoneNumber"),
                   MobilePhone = userEntry.GetStringValue("mobile"),
                   Fax = userEntry.GetStringValue("facsimileTelephoneNumber"),
                   EMail = userEntry.GetStringValue("mail"),
                   WwwHomePage = userEntry.GetStringValue("wwwHomePage"),

                   Title = userEntry.GetStringValue("title"),
                   Department = userEntry.GetStringValue("department"),
                   Company = userEntry.GetStringValue("company"),
                   Manager = userEntry.GetStringValue("manager"),
                   PrimaryGroupID = Convert.ToInt64(userEntry.Properties["primaryGroupID"].Value),
                   DN = userEntry.GetStringValue("distinguishedName")
               };

            user.PrimaryGroup = owner.GetGroupInfoByToken(user.PrimaryGroupID);

            return user;
        }

        public static DirectorySearcher FillDirectorySearcher(DirectorySearcher directorySearcher)
        {
            directorySearcher.PropertiesToLoad.Add("userAccountControl");

            directorySearcher.PropertiesToLoad.Add("objectGUID");
            directorySearcher.PropertiesToLoad.Add("objectSid");
            directorySearcher.PropertiesToLoad.Add("sAMAccountName");
            directorySearcher.PropertiesToLoad.Add("cn");
            directorySearcher.PropertiesToLoad.Add("name");

            directorySearcher.PropertiesToLoad.Add("givenName");
            directorySearcher.PropertiesToLoad.Add("sn");
            directorySearcher.PropertiesToLoad.Add("initials");
            directorySearcher.PropertiesToLoad.Add("description");

            directorySearcher.PropertiesToLoad.Add("streetAddress");
            directorySearcher.PropertiesToLoad.Add("postOfficeBox");
            directorySearcher.PropertiesToLoad.Add("postalCode");
            directorySearcher.PropertiesToLoad.Add("c");
            directorySearcher.PropertiesToLoad.Add("st");
            directorySearcher.PropertiesToLoad.Add("l");

            directorySearcher.PropertiesToLoad.Add("telephoneNumber");
            directorySearcher.PropertiesToLoad.Add("mobile");
            directorySearcher.PropertiesToLoad.Add("facsimileTelephoneNumber");
            directorySearcher.PropertiesToLoad.Add("mail");
            directorySearcher.PropertiesToLoad.Add("wwwHomePage");

            directorySearcher.PropertiesToLoad.Add("title");
            directorySearcher.PropertiesToLoad.Add("department");
            directorySearcher.PropertiesToLoad.Add("company");
            directorySearcher.PropertiesToLoad.Add("manager");
            directorySearcher.PropertiesToLoad.Add("primaryGroupID");
            directorySearcher.PropertiesToLoad.Add("distinguishedName");

            return directorySearcher;
        }
    }
}