namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    public interface IActiveDirectory
    {
        ADGroup GetGroupInfoByToken(long groupToken);
    }
}