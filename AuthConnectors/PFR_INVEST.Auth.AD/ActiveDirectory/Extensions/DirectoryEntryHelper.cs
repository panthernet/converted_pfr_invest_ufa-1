using System;
using System.DirectoryServices;
using System.Text;

namespace PFR_INVEST.Auth.AD.ActiveDirectory.Extensions
{
    public static class DirectoryEntryHelper
    {
        public static string GetStringValue(this DirectoryEntry entry, string propertyName)
        {
            return entry.Properties.Contains(propertyName)
                       ? entry.Properties[propertyName].Value as string
                       : string.Empty;
        }

        public static string ConvertByteToStringSid(byte[] sidBytes)
        {
            if (sidBytes == null) return null;

            var strSid = new StringBuilder();
            strSid.Append(@"S-");

            // Add SID revision.
            strSid.Append(sidBytes[0].ToString());
            // Next six bytes are SID authority value.
            if (sidBytes[6] != 0 || sidBytes[5] != 0)
            {
                strSid.Append(@"-");
                string strAuth = String.Format
                    (@"0x{0:2x}{1:2x}{2:2x}{3:2x}{4:2x}{5:2x}",
                     (Int16)sidBytes[1],
                     (Int16)sidBytes[2],
                     (Int16)sidBytes[3],
                     (Int16)sidBytes[4],
                     (Int16)sidBytes[5],
                     (Int16)sidBytes[6]);
                strSid.Append(strAuth);
            }
            else
            {
                Int64 iVal =
                    (sidBytes[1]) +
                    (sidBytes[2] << 8) +
                    (sidBytes[3] << 16) +
                    (sidBytes[4] << 24);
                strSid.Append(@"-");
                strSid.Append(iVal.ToString());
            }

            // Get sub authority count...
            int iSubCount = Convert.ToInt32(sidBytes[7]);
            for (int i = 0; i < iSubCount; i++)
            {
                int idxAuth = 8 + i * 4;
                UInt32 iSubAuth =
                    BitConverter.ToUInt32(sidBytes, idxAuth);
                strSid.Append(@"-");
                strSid.Append(iSubAuth.ToString());
            }

            return strSid.ToString();
        }
    }
}