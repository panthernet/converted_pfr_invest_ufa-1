﻿namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    public enum ADPasswordSetResult
    {
        Success = 0,
        SuccessNoLogin = 1,
        Fail = 2,
        FailPwdPolicy = 3,
        FailPwdWrong = 4
    }
}
