using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Runtime.InteropServices;

namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    public class ActiveDirectory : IDisposable, IActiveDirectory
    {
        public ActiveDirectory(ActiveDirectoryConnectionContext context)
        {
            Context = context;
        }

        public ActiveDirectoryConnectionContext Context { get; private set; }

        private DirectoryEntry _ldapEntry;
        private DirectoryEntry LdapEntry
        {
            get
            {
                if (_ldapEntry == null)
                {
                    _ldapEntry = new DirectoryEntry(Context.GetActiveDirectoryConnectionString());

                    if (!string.IsNullOrEmpty(Context.UserName))
                    {
                        _ldapEntry.AuthenticationType = AuthenticationTypes.Secure;
                        _ldapEntry.Username = Context.UserName;
                        _ldapEntry.Password = Context.Password;
                    }
                }
                return _ldapEntry;
            }
        }


        public ADValidationResult Validate()
        {
            Logs.AD.Info("AD validate");

            ADValidationResult authenticated = ADValidationResult.GeneralError;
            try
            {
                var native = LdapEntry.NativeObject;
                authenticated = ADValidationResult.Success;
                Logs.AD.Info("AD validate SUCESS");
            }
            catch (DirectoryServicesCOMException ex)
            {
                //LogManager.getSingleton().LogAD("AAAA: " + ex.ExtendedErrorMessage);
                // ������� ������� ����� ��� ������
                foreach (ADValidationResult item in Enum.GetValues(typeof (ADValidationResult)))
                {
                    //LogManager.getSingleton().LogAD(string.Format("AD: check vItem={0}({1})", item, (int)item));
                    if (ex.ExtendedErrorMessage.Contains(string.Format("{0}, ",(int)item)))
                    {
                        //LogManager.getSingleton().LogAD("AD: found error id ="+(int)item );
                        return item;
                    }
                }
                return ADValidationResult.InvalidCredentials;
            }
            catch (COMException ex)
            {
                Logs.AD.Error("AD validate FAIL(COM)", ex);

                throw new ADInitException();
            }
            catch (Exception ex)
            {
                Logs.AD.Error("AD validate FAIL", ex);
            }
            return authenticated;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            if (_ldapEntry != null)
                _ldapEntry.Close();
        }

        internal DirectoryEntry GetUserEntry(string paramName, string paramValue, bool dokipGroup = false)
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = string.Format(
                    "(&(objectCategory=person)({0}={1}))", paramName, paramValue)
            };
            DirectoryEntry result;
            using (directorySearcher = ADUser.FillDirectorySearcher(directorySearcher))
            {
                directorySearcher.ReferralChasing = ReferralChasingOption.All;
                var searchResult = directorySearcher.FindOne();
                result = searchResult == null ? null : searchResult.GetDirectoryEntry();
            }
            return result;
        }

        internal DirectoryEntry GetUserEntryBySamName(DirectoryEntry root, string paramValue)
        {
            var directorySearcher = new DirectorySearcher(root)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = string.Format(
                    "(&(objectCategory=person)(sAMAccountName={0}))", paramValue)
            };
            DirectoryEntry result;
            using (directorySearcher = ADUser.FillDirectorySearcher(directorySearcher))
            {
                directorySearcher.ReferralChasing = ReferralChasingOption.All;
                var searchResult = directorySearcher.FindOne();
                result = searchResult == null ? null : searchResult.GetDirectoryEntry();
            }
            return result;
        }

        private ADUser GetUserInfoInternal(string paramName, string paramValue, bool dokipGroup = false)
        {
            using (var userEntry = GetUserEntry(paramName, paramValue, dokipGroup))
            {
                return userEntry == null ? null : ADUser.CreateFromEntry(userEntry, this);
            }
        }

        public ADPasswordSetResult ChangeUserPassword(string userName, string oldPassword, string password)
        {
            try
            {
                //LogManager.getSingleton().LogAD("AD pwd ch: query user " + userName);
                using (var root = new DirectoryEntry(Context.GetActiveDirectoryConnectionString()))//GetUserEntry("name", userName))
                {
                    var node = this.GetUserEntryBySamName(root, userName);
                    node.Invoke("ChangePassword", new object[] { oldPassword, password });
                    //���������� ��������� ���
                    node.CommitChanges();
                    Logs.AD.Info("AD pwd ch: commited pwd = success!");
                    return ADPasswordSetResult.Success;
                }
            }
            catch (Exception ex)
            {
                Logs.AD.Error("AD pwd ch: FAIL!", ex);
                if (ex.InnerException != null && ex.InnerException.Message.Contains("0x800708C5")) return ADPasswordSetResult.FailPwdPolicy;
                if (ex.InnerException != null && ex.InnerException.Message.Contains("0x80070056")) return ADPasswordSetResult.FailPwdWrong;
                return ADPasswordSetResult.Fail;
            }
        }


        public ADPasswordSetResult SetUserPassword(string userName, string password)
        {
            try
            {
               // LogManager.getSingleton().LogAD("AD pwd set: query user "+ userName);
                using (var node = new DirectoryEntry(Context.GetActiveDirectoryConnectionString(userName)))//GetUserEntry("name", userName))
                {
                    //���������� ������������� ����� ������
                    //node.Properties["pwdLastSet"].Value = -1;
                    //���������� ��������� ���
                    //node.Properties["LockOutTime"].Value = 0;
                    node.Invoke("SetPassword", new object[] { password });
                    node.CommitChanges();
                    Logs.AD.Info("AD pwd set: commited pwd = success!");
                    return ADPasswordSetResult.Success;
                }
            }
            catch (Exception ex)
            {
                Logs.AD.Error("AD pwd set: FAIL!", ex);
                return ADPasswordSetResult.Fail;
            }
        }

        public ADUser GetUserInfoCustom(string fio)
        {
            return GetUserInfoInternal("name", fio, true);
        }

        public ADUser GetUserInfo(string samUserName)
        {
            return GetUserInfoInternal("sAMAccountName", samUserName);
        }

        public ADUser GetUserInfoByName(string userName)
        {
            return GetUserInfoInternal("name", userName);
        }

        public void AddUser(ADUser user)
        {
            if (user == null) return;

            using (var newUser = LdapEntry.Children.Add(string.Format("CN={0}", user.Name), "user"))
            {
                newUser.Properties["sAMAccountName"].Value = user.SamName;
                newUser.CommitChanges();

                newUser.Properties["userAccountControl"].Value = user.AccountControlFlags;
                newUser.Invoke("SetPassword", user.Password);
                newUser.CommitChanges();
            }
        }

        public void RemoveUser(ADUser userInfo)
        {
            if (userInfo == null) return;

            using (var userEntry = GetUserEntry("sAMAccountName", userInfo.SamName))
            {
                if (userEntry == null) return;
                LdapEntry.Children.Remove(userEntry);
            }
        }

        public ADGroup[] GetGroupInfos()
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = "(&(objectCategory=group))"
            };

            directorySearcher = ADGroup.FillDirectorySearcher(directorySearcher);
            directorySearcher.ReferralChasing = ReferralChasingOption.All;
            var searchResult = directorySearcher.FindAll();
            var result2 = (from SearchResult result in searchResult
                          select ADGroup.CreateFromEntry(result.GetDirectoryEntry())).ToArray();
            directorySearcher.Dispose();
            return result2;
        }

        private DirectoryEntry GetGroupEntry(string samName)
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = string.Format(
                    "(&(objectCategory=group)(sAMAccountName={0}))", samName)
            };

            directorySearcher = ADUser.FillDirectorySearcher(directorySearcher);
            directorySearcher.ReferralChasing = ReferralChasingOption.All;
            var searchResult = directorySearcher.FindOne();
            var result = searchResult == null ? null : searchResult.GetDirectoryEntry();
            directorySearcher.Dispose();
            return result;
        }

        public ADGroup GetGroupInfoByToken(long groupToken)
        {
            if (groupToken == 0) return null;

            var groups = GetGroupInfos();
            return groups.FirstOrDefault(adGroup => adGroup.PrimaryGroupToken == groupToken);
        }

        public ADGroup GetGroupInfo(string groupName)
        {
            var groups = GetGroupInfos();
            return groups.FirstOrDefault(g => g.SamName == groupName);
        }

        public ADUser[] GetGroupChildUsers(string groupName)
        {
            return GetGroupChildUsers(GetGroupInfo(groupName));
        }

        public ADUser[] GetGroupChildUsers(ADGroup group)
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = string.Format(
                    "(&(objectCategory=person)(memberOf={0}))", @group.DN)
            };

            var searchResult = directorySearcher.FindAll();
            var result2 = (from SearchResult result in searchResult
                           select ADUser.CreateFromEntry(result.GetDirectoryEntry(), this)).ToArray();
            directorySearcher.Dispose();
            return result2;
        }

        public ADGroup[] GetUserGroups(ADUser user)
        {
            var groups = new List<ADGroup>();

            var userEntry = GetUserEntry("sAMAccountName", user.SamName);
            if (userEntry == null) return groups.ToArray();

            var groupsDn =
                (from object groupDn in userEntry.Properties["memberOf"]
                 select groupDn.ToString()).ToList();

            return GetGroupInfos().Where(g => groupsDn.Contains(g.DN)).ToArray();
        }

        public ADGroup[] GetUserGroups(string userName)
        {
            return GetUserGroups(new ADUser { SamName = userName });
        }

        public void AddUserToGroup(string userName, string groupName)
        {
            AddUserToGroup(GetUserInfo(userName), groupName);
        }

        public void AddUserToGroup(ADUser user, string groupName)
        {
            using (var groupEntry = GetGroupEntry(groupName))
            {
                if (groupEntry == null) return;

                groupEntry.Properties["member"].Add(user.DN);
                groupEntry.CommitChanges();
                groupEntry.Close();
            }
        }

        public void AddUserToGroup(ADUser user, ADGroup group)
        {
            AddUserToGroup(user, group.SamName);
        }

        public void RemoveUserFromGroup(string userName, string groupName)
        {
            RemoveUserFromGroup(GetUserInfo(userName), groupName);
        }

        public void RemoveUserFromGroup(ADUser user, string groupName)
        {
            using (var groupEntry = GetGroupEntry(groupName))
            {
                if (groupEntry == null) return;

                groupEntry.Properties["member"].Remove(user.DN);
                groupEntry.CommitChanges();
                groupEntry.Close();
            }
        }

        public void RemoveUserFromGroup(ADUser user, ADGroup group)
        {
            RemoveUserFromGroup(user, group.SamName);
        }

        public ADUser[] GetUsers()
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = "(&(objectCategory=person))"
            };


            directorySearcher = ADGroup.FillDirectorySearcher(directorySearcher);
            directorySearcher.ReferralChasing = ReferralChasingOption.All;
            var searchResult = directorySearcher.FindAll();
            var result2 = (from SearchResult result in searchResult
                           select ADUser.CreateFromEntry(result.GetDirectoryEntry(), this)).ToArray();
            directorySearcher.Dispose();
            return result2;
        }

        public ADUser[] GetUsersNotInGroup(string groupSamName)
        {
            var group = GetGroupInfo(groupSamName);
            return @group == null ? GetUsers() : GetUsersNotInGroup(@group);
        }

        public ADUser[] GetUsersNotInGroup(ADGroup group)
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = string.Format(
                    "(&(objectCategory=person)(!memberOf={0}))", @group.DN)
            };

            var searchResult = directorySearcher.FindAll();
            var result2 = (from SearchResult result in searchResult
                           select ADUser.CreateFromEntry(result.GetDirectoryEntry(), this)).ToArray();
            directorySearcher.Dispose();
            return result2;
        }

        public bool IsUserInRole(string userName, string role)
        {
            var group = GetGroupInfo(role);
            return @group != null && IsUserInRole(userName, @group);
        }

        public ADUser IsUserExist(string dn)
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = "(&(objectCategory=person))"
            };


            var searchResult = directorySearcher.FindAll();
            var users = (from SearchResult result in searchResult
                              select ADUser.CreateFromEntry(result.GetDirectoryEntry(), this)).ToArray();
            directorySearcher.Dispose();

            return users.FirstOrDefault(user => user.DN.ToLower() == dn.ToLower());
        }

        public bool IsUserInRole(string userName, ADGroup role)
        {
            var directorySearcher = new DirectorySearcher(LdapEntry)
            {
                ReferralChasing = ReferralChasingOption.All,
                Filter = string.Format(
                    "(&(objectCategory=person)(memberOf={0}))", role.DN)
            };


            var searchResult = directorySearcher.FindAll();

            ADUser[] users = (from SearchResult result in searchResult
                              select ADUser.CreateFromEntry(result.GetDirectoryEntry(), this)).ToArray();
            directorySearcher.Dispose();

            Logs.AD.Info("rolename: " + role.Name);
            Logs.AD.Info("Name: " + userName);

            foreach (var user in users)
            {
                Logs.AD.Info("username: " + user.Name);
                if (user.SamName.ToLower() == userName.ToLower())
                    return true;
            }

            Logs.AD.Info("FALSE");

            return false;
        }
    }
}