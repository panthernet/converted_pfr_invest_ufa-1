﻿using System;
using System.Diagnostics;
using System.IO;


namespace PFR_INVEST.Proxy.Tools
{
    class LogManager
    {
        private static LogManager singleton;

        public static LogManager getSingleton()
        {
            return singleton ?? (singleton = new LogManager());
        }

        private LogManager()
        {

        }

        private static readonly object locker = new object();

        public void log(string message)
        {
            lock (locker)
            {
                /*message = message.Replace("'", "#");
                message = message.Replace("\"", "@");*/

				//string logLine = String.Format("{0:G}: {1}.", DateTime.Now, message);

				//if (!Directory.Exists("C:/Logs"))
				//    Directory.CreateDirectory("C:/Logs");
				//var SW = File.AppendText("C:/Logs/Log.txt");
				//SW.WriteLine(logLine);
				//SW.Close();
            }
        }

        public void logEx(Exception ex)
        {
            try
            {

                //Writes error information to the log file including name of the file, line number & error message description

                var stackTrace = new StackTrace(ex, true);

                string fileNames = stackTrace.GetFrame((stackTrace.FrameCount - 1)).GetFileName();

                //fileNames = fileNames.Substring(fileNames.LastIndexOf(MediaTypeNames.Application.ProductName));

                Int32 lineNumber = stackTrace.GetFrame((stackTrace.FrameCount - 1)).GetFileLineNumber();

                var methodBase = stackTrace.GetFrame((stackTrace.FrameCount - 1)).GetMethod();    //These two lines are respnsible to find out name of the method

                String methodName = methodBase.Name;

                log(String.Format("Error in {0}, Method name is {1}, at line number {2} ,Error Message,{3}", fileNames, methodName, lineNumber, ex.Message));

            }
            catch (Exception genEx)
            {
                log(genEx.Message);
            }


            log(String.Format("Error catched :{0} \r\n stack trace: {1}", ex.Message, ex.StackTrace));

            if (ex.InnerException != null)
                logEx(ex.InnerException);
        }


    }
}
