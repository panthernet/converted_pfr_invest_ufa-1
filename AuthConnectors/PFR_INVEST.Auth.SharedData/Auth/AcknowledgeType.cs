﻿using System.Runtime.Serialization;

namespace PFR_INVEST.Auth.SharedData.Auth
{
    /// <summary>
    /// Перечисление возможных типов результата обработки запроса.
    /// </summary>
    //[DataContract]
    [DataContract(Namespace = "http://pfr.ru")]
    public enum AcknowledgeType
    {
        /// <summary>
        /// Тип запроса, закончившего неудачной обработкой.
        /// </summary>
        [EnumMember]
        Failure = -1,

        /// <summary>
        /// Тип успешно обработанного запроса.
        /// </summary>
        [EnumMember]
        Success = 1,

        /// <summary>
        /// Некорректная точка доступа.
        /// </summary>
        [EnumMember]
        InvalidAccessToken = 2,
    }
}