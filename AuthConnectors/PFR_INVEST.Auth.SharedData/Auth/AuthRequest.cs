﻿using System.Runtime.Serialization;

namespace PFR_INVEST.Auth.SharedData.Auth
{
    [DataContract]
    //[DataContract]
    public class AuthRequest
    {
        [DataMember]
        public string AccessToken { get; set; }

        [DataMember]
        public object Value1 { get; set; }

        [DataMember]
        public object Value2 { get; set; }
    }
}
