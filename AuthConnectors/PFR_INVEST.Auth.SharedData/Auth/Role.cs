﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.Auth.SharedData.Auth
{
    /// <summary>
    /// Роль пользователей
    /// </summary>
    [DataContract]
    public class Role
    {
        /// <summary>
        /// Название
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [DataMember]
        public string ShowName { get; set; }


        public void SetShowName()
        {
            string switchname = Name;
            switch (switchname)
            {
                case "PTK_DOKIP_USERS":
                    ShowName = "Пользователи ПТК ДОКИП";
                    break;
                case "PTK_DOKIP_ADMINISTRATORS":
                    ShowName = "Администраторы ПТК ДОКИП";
                    break;

                case "PTK_DOKIP_OSRP_VIEWERS":
                    ShowName = "Обозреватели ОСРП";
                    break;
                case "PTK_DOKIP_OSRP_WORKERS":
                    ShowName = "Сотрудники ОСРП";
                    break;
                case "PTK_DOKIP_OSRP_DIRECTORY_EDITORS":
                    ShowName = "Редакторы справочников ОСРП";
                    break;
                case "PTK_DOKIP_OSRP_MANAGERS":
                    ShowName = "Руководитель ОСРП";
                    break;

                case "PTK_DOKIP_OKIP_VIEWERS":
                    ShowName = "Обозреватели ОКИП";
                    break;
                case "PTK_DOKIP_OKIP_WORKERS":
                    ShowName = "Сотрудники ОКИП";
                    break;
                case "PTK_DOKIP_OKIP_DIRECTORY_EDITORS":
                    ShowName = "Редакторы справочников ОКИП";
                    break;
                case "PTK_DOKIP_OKIP_MANAGERS":
                    ShowName = "Руководитель ОКИП";
                    break;

                case "PTK_DOKIP_OFPR_VIEWERS":
                    ShowName = "Обозреватели ОФПР";
                    break;
                case "PTK_DOKIP_OFPR_WORKERS": 
                    ShowName = "Сотрудники ОФПР";
                    break;
                case "PTK_DOKIP_OFPR_DIRECTORY_EDITORS":
                    ShowName = "Редакторы справочников ОФПР";
                    break;
                case "PTK_DOKIP_OFPR_MANAGERS":
                    ShowName = "Руководитель ОФПР";
                    break;

                case "PTK_DOKIP_OARRS_VIEWERS":
                    ShowName = "Обозреватели ОАРРС";
                    break;
                case "PTK_DOKIP_OARRS_WORKERS":
                    ShowName = "Сотрудники ОАРРС";
                    break;
                case "PTK_DOKIP_OARRS_DIRECTORY_EDITORS":
                    ShowName = "Редакторы справочников ОАРРС";
                    break;
                case "PTK_DOKIP_OARRS_MANAGERS":
                    ShowName = "Руководитель ОАРРС";
                    break;

                case "PTK_DOKIP_OVSI_VIEWERS":
                    ShowName = "Обозреватели ОВСИ";
                    break;
                case "PTK_DOKIP_OVSI_WORKERS":
                    ShowName = "Сотрудники ОВСИ";
                    break;
                case "PTK_DOKIP_OVSI_DIRECTORY_EDITORS":
                    ShowName = "Редакторы справочников ОВСИ";
                    break;
                case "PTK_DOKIP_OVSI_MANAGERS":
                    ShowName = "Руководитель ОВСИ";
                    break;

                case "PTK_DOKIP_OUFV_VIEWERS":
                    ShowName = "Обозреватели ОУиФВ";
                    break;
                case "PTK_DOKIP_OUFV_WORKERS":
                    ShowName = "Сотрудники ОУиФВ";
                    break;
                case "PTK_DOKIP_OUFV_DIRECTORY_EDITORS":
                    ShowName = "Редакторы справочников ОУиФВ";
                    break;
                case "PTK_DOKIP_OUFV_MANAGERS":
                    ShowName = "Руководитель ОУиФВ";
                    break;
                default: ShowName = ""; break;
            }
        }


        /// <summary>
        /// Список пользователей которым назначена данная роль
        /// </summary>
        [DataMember]
        public List<User> Users { get; set; }

        public Role()
        {
        }

        public Role(string name)
        {
            Name = name;
        }

        public Role(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}
