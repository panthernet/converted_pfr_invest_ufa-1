﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.Auth.ClientData
{
    public class AuthProvider
    {
        public UserSecurity CurrentUserSecurity;

        public const string DB_SYSTEM_GROUP = "PTK_DOKIP_USERS";

        private string _mAccessToken;

        private string AccessToken
        {
            get
            {
                if (string.IsNullOrEmpty(_mAccessToken))
                    _mAccessToken = AuthServiceSystem.Client.GetToken().ToString();
                return _mAccessToken;
            }
        }

        public string UserName
        {
            get;
            private set;
        }

        public AuthProvider(string pUserName)
        {
            if (pUserName == null)
            {
                pUserName = AuthServiceSystem.Client.GetCurrentUserLogin();
            }

            CurrentUserSecurity = new UserSecurity();
            _mAccessToken = null;
            UserName = pUserName;
            RefreshUserSecurity();
        }

        #region Получение данных

        /// <summary>
        /// Возвращает список пользователей не добавленных в систему
        /// </summary>
        public List<User> GetAuthUser()
        {
            return GetUsersNotInRole(new Role(DB_SYSTEM_GROUP));
        }

        /// <summary>
        /// Возвращает список пользователей системы
        /// </summary>
        public List<User> GetAuthUsers()
        {
            return GetAuthUsers(new Role(DB_SYSTEM_GROUP));
        }

        /// <summary>
        /// Возвращает список пользователей которым назначена указанная роль
        /// </summary>
        /// <param name="source">Роль</param>
        /// <returns>Список пользователей</returns>
        public List<User> GetAuthUsers(Role source)
        {
            //var result = new List<ADUser>();
            var response = AuthServiceSystem.Client.GetGroupsChildUser(new AuthRequest
            {
                AccessToken = AccessToken,
                Value1 = source.Name
            });
            if (response.Acknowledge == AcknowledgeType.Success)
                return (List<User>)response.Result;
            return new List<User>();
        }

        /// <summary>
        /// Возвращает список ролей системы
        /// </summary>
        /// <returns>Список ролей</returns>
        public List<Role> GetRoles(string login = null)
        {
            var response = AuthServiceSystem.Client.GetRoles(new AuthRequest { AccessToken = AccessToken, Value1 = login });
            if (response.Acknowledge == AcknowledgeType.Success)
                return (List<Role>)response.Result;
            return new List<Role>();
        }

        public List<Role> GetRoles(User user)
        {
            return GetRoles(user.Login);
        }


        #endregion

        /// <summary>
        /// Добавить пользователя из Active Directory в список пользователей системы
        /// </summary>
        /// <param name="user">Пользователь</param>
        public bool AddAuthUser(User user, out string message)
        {
            // NOTE: заменено на добавление пользователю конкретной роли
            var response = AuthServiceSystem.Client.AddUserToGroup(new AuthRequest
            {
                AccessToken = AccessToken,
                Value2 = DB_SYSTEM_GROUP,
                Value1 = user.Login
            });
            message = response.Message;
            return response.Acknowledge == AcknowledgeType.Success;
        }


        /// <summary>
        /// Добавить пользователя к роли
        /// </summary>
        public bool AddToRole(Role role, User user, out string message)
        {
            var response = AuthServiceSystem.Client.AddUserToGroup(new AuthRequest
            {
                AccessToken = AccessToken,
                Value2 = role.Name,
                Value1 = user.Login
            });
            message = response.Message;
            return response.Acknowledge == AcknowledgeType.Success;
        }

        /// <summary>
        /// Добавить пользователя к роли
        /// </summary>
        public bool AddToRole(Role role, User user)
        {
            string message;
            return AddToRole(role, user, out message);           
        }

        /// <summary>
        /// Удалить пользователя из роли
        /// </summary>
        public bool RemoveFromRole(Role role, User user ,out string message)
        {
            var response = AuthServiceSystem.Client.RemoveUserFromGroup(new AuthRequest
            {
                AccessToken = AccessToken,
                Value2 = role.Name,
                Value1 = user.Login
            });
            message = response.Message;
            return response.Acknowledge == AcknowledgeType.Success;
        }

        /// <summary>
        /// Удалить пользователя из роли
        /// </summary>
        public bool RemoveFromRole(Role role, User user)
        {
            string message = null;
            return RemoveFromRole(role, user, out message);
        }

        public bool RemoveAuthUser(User user, out string message)
        {
            var response = AuthServiceSystem.Client.RemoveUserFromGroup(new AuthRequest
            {
                AccessToken = AccessToken,
                Value1 = user.Login,
                Value2 = DB_SYSTEM_GROUP
            });
            message = response.Message;
            return response.Acknowledge == AcknowledgeType.Success;
        }

        public List<User> GetUsersNotInRole(Role role)
        {
            var response = AuthServiceSystem.Client.GetUsersNotInGroup(new AuthRequest
            {
                AccessToken = AccessToken,
                Value1 = role.Name
            });
            if (response.Acknowledge == AcknowledgeType.Success)
                return (List<User>) response.Result;
            return new List<User>();
        }

        public bool IsCurrentUserInRole(string role)
        {
            return AuthServiceSystem.Client.IsUserInRole(UserName, role);
        }

        public bool IsCurrentUserInAtLeastOneRole(string[] roles)
        {
            return AuthServiceSystem.Client.IsUserInAtLeastOneRole(UserName, roles);
        }

        public void RefreshUserSecurity()
        {
            List<Role> roles;
            try
            {
                roles = GetRoles(UserName);
            }
            catch(Exception ex)
            {
                return;
            }

            if ((roles == null) || (roles.Count < 1))
                return;

            CurrentUserSecurity.ClearRoles();

            foreach (var role in roles)
            {
                var curRole = UserSecurity.GetRoleByADName(role.Name);
                if (curRole != DOKIP_ROLE_TYPE.None)
                    CurrentUserSecurity.AddRole(curRole);
            }
        }

        public User GetUserInfo(string userName)
        {
            var response = AuthServiceSystem.Client.GetUserInfo(
                new AuthRequest
                {
                    AccessToken = AccessToken,
                    Value1 = userName
                });

            if (response.Acknowledge == AcknowledgeType.Success)
            {
                if (response.Result == null)
                { 
                    return new User
                    {
                        Name = string.Join(" ", new[] { userName, "", "" }),
                        Login = userName,
                    };
                }

                return (User)response.Result;
            }
            return null;
        }
    }
}