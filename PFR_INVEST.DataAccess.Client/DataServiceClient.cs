﻿using System.Linq;
using db2connector;

namespace PFR_INVEST.DataAccess.Client
{
    public class DataServiceClient : System.ServiceModel.ClientBase<IDB2Connector>
    {
        internal DataServiceClient()
        {

        }

        internal DataServiceClient(string endpointConfigurationName)
            : base(endpointConfigurationName)
        {

        }

        public IDB2Connector GetChannel()
        {
            return base.Channel;
        }
    }
}
