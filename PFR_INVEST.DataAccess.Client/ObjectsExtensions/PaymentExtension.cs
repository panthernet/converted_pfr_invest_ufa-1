﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions
{
	public static class PaymentExtension
	{
		public static Security GetSecurity(this Payment payment)
		{
			if (payment != null && payment.SecurityId.HasValue)
				return DataContainerFacade.GetExtensionData<Security>(payment, "Security", delegate()
				{
					return DataContainerFacade.GetByID<Security, long>(payment.SecurityId);
				});
			else 
				return null;
		}
	}
}
