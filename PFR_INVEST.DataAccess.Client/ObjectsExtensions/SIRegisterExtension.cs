﻿using System.Collections.Generic;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class SIRegisterExtension
    {
        public static SPNOperation GetOperation(this SIRegister p_reg)
        {
            return DataContainerFacade.GetExtensionData(p_reg, "Operation", 
                () => DataContainerFacade.GetByID<SPNOperation, long>(p_reg.OperationID));
        }

        public static Element GetOperationType(this SIRegister p_reg)
        {
            return DataContainerFacade.GetExtensionData(p_reg, "OperationType", 
                () => DataContainerFacade.GetByID<Element, long>(p_reg.OperationTypeID));
        }

        public static SPNDirection GetDirection(this SIRegister p_reg)
        {
            return DataContainerFacade.GetExtensionData(p_reg, "Direction", () 
                => DataContainerFacade.GetByID<SPNDirection, long>(p_reg.DirectionID));
        }

        public static Year GetCompanyYear(this SIRegister p_reg)
        {
            return DataContainerFacade.GetExtensionData(p_reg, "CompanyYear", 
                () => DataContainerFacade.GetByID<Year, long>(p_reg.CompanyYearID));
        }

        public static Month GetCompanyMonth(this SIRegister p_reg)
        {
            return DataContainerFacade.GetExtensionData(p_reg, "CompanyMonth", 
                () => DataContainerFacade.GetByID<Month, long>(p_reg.CompanyMonthID));
        }

       public static IList<SITransfer> GetListSITransfer(this SIRegister p_reg)
        {
            //return DataContainerFacade.GetListByProperty<SITransfer>("TransferRegisterID", p_reg.ID);

           var pL = new List<ListPropertyCondition> 
           {
               ListPropertyCondition.Equal("TransferRegisterID", p_reg.ID), 
               ListPropertyCondition.NotEqual("StatusID", (long) -1)
           };
           return DataContainerFacade.GetListByPropertyConditions<SITransfer>(pL);
        }
    }
}
