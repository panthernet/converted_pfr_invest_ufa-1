﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions
{
    public static class F140Extension
    {


        public static EDOLog GetEdoLog(this EdoOdkF140 edo)
        {

            if (edo != null)
            {
                return DataContainerFacade.GetListByProperty<EDOLog>("EdoID", edo.ID).FirstOrDefault(l => !l.ExeptId.HasValue); 

            }
            return null;
        }

        public static long GetCategoryUsage(this CategoryF140 category) 
        {
            if (category == null || category.ID <= 0)
                return 0;
            return DataContainerFacade.GetExtensionData<long>(category, "CategoryUsage",
                () => DataContainerFacade.GetListByPropertyCount<EdoOdkF140>("CategoryId", category.ID));       
        }
    }


}
