﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class AddSPNExtension
    {
        public static Portfolio GetPortfolio(this AddSPN p_addspn)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(p_addspn, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(p_addspn.PortfolioID);
            });
        }

        public static LegalEntity GetLegalEntity(this AddSPN p_addspn)
        {
            return DataContainerFacade.GetExtensionData<LegalEntity>(p_addspn, "LegalEntity", delegate()
            {
                return DataContainerFacade.GetByID<LegalEntity, long>(p_addspn.LegalEntityID);
            });
        }

        public static Month GetMonth(this AddSPN p_addspn)
        {
            return DataContainerFacade.GetExtensionData<Month>(p_addspn, "Month", delegate()
            {
                return DataContainerFacade.GetByID<Month, long>(p_addspn.MonthID);
            });
        }

        public static PfrBankAccount GetPFRBankAccount(this AddSPN p_addspn)
        {
            return DataContainerFacade.GetExtensionData<PfrBankAccount>(p_addspn, "PFRBankAccount", delegate()
            {
                return DataContainerFacade.GetByID<PfrBankAccount, long>(p_addspn.PFRBankAccountID);
            });
        }

		public static IList<DopSPN> GetDopSPNList(this AddSPN p_addspn)
		{
			if (p_addspn != null)
			{
				//return DataContainerFacade.GetExtensionData<IList<DopSPN>>(p_addspn, "DopSPNList", delegate()
				//{
					return DataContainerFacade.GetListByProperty<DopSPN>("AddSpnID", p_addspn.ID);
				//});
			}
			else 
				return new List<DopSPN>();
		}
    }
}
