﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class PortfolioPFRBankAccountExtension
    {
        public static Portfolio GetPortfolio(this PortfolioPFRBankAccount pb_acc)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(pb_acc, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(pb_acc.PortfolioID);
            });
        }

        public static PfrBankAccount GetPfrBankAccount(this PortfolioPFRBankAccount pb_acc)
        {
            return DataContainerFacade.GetExtensionData<PfrBankAccount>(pb_acc, "PfrBankAccount", delegate()
            {
                return DataContainerFacade.GetByID<PfrBankAccount, long>(pb_acc.PFRBankAccountID);
            });
        }
    }
}
