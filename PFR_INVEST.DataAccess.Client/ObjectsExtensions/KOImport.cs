﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    using PFR_INVEST.Constants.Identifiers;

    public static class KOImportExtension
    {
        public static BanksConclusion GetBanksConclusion(this BanksConclusion p_ko, DateTime p_importDate)
        {
            if (p_ko != null)
                return DataContainerFacade.GetExtensionData<BanksConclusion>(p_ko, "ImportDate", delegate()
                {
                    return DataContainerFacade.GetListByProperty<BanksConclusion>("ImportDate", p_importDate).FirstOrDefault();
                });
            else
                return null;
        }
               
    }
}
