﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class ReturnExtension
    {
        public static Portfolio GetPortfolio(this Return pb_ret)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(pb_ret, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(pb_ret.PortfolioID);
            });
        }

        public static PfrBankAccount GetPfrBankAccount(this Return pb_ret)
        {
            return DataContainerFacade.GetExtensionData<PfrBankAccount>(pb_ret, "PfrBankAccount", delegate()
            {
                return DataContainerFacade.GetByID<PfrBankAccount, long>(pb_ret.PFRBankAccountID);
            });
        }
    }
}
