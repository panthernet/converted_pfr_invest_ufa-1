﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class ReorganizationExtension
    {
        public static Contragent GetReceiver(this Reorganization p_reorg)
        {
            return DataContainerFacade.GetExtensionData<Contragent>(p_reorg, "Receiver", 
                () => DataContainerFacade.GetByID<Contragent, long>(p_reorg.ReceiverContragentID));
        } 
    }
}
