﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class ContactExtension
    {
        public static LegalEntity GetLegalEntity(this Contact p_contact)
        {
            return DataContainerFacade.GetExtensionData<LegalEntity>(p_contact, "LegalEntity", delegate()
            {
                return DataContainerFacade.GetByID<LegalEntity, long>(p_contact.LegalEntityID);
            });
        }
    }
}
