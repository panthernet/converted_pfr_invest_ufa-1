﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class OrdReportExtension
    {
        public static CbInOrder GetCbInOrder(this OrdReport p_ord_report)
        {
            return DataContainerFacade.GetExtensionData<CbInOrder>(p_ord_report, "CbInOrder", delegate()
            {
                return DataContainerFacade.GetByID<CbInOrder, long>(p_ord_report.CbInOrderID);
            });
        }
    }
}
