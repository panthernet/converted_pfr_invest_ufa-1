﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class AccountExtension
    {
        public static Contragent GetContragent(this Account p_acc)
        {
            return DataContainerFacade.GetExtensionData<Contragent>(p_acc, "Contragent", delegate()
            {
                return DataContainerFacade.GetByID<Contragent, long>(p_acc.ContragentID);
            });
        }

        public static AccountKind GetAccountKind(this Account p_acc)
        {
            return DataContainerFacade.GetExtensionData<AccountKind>(p_acc, "Kind", delegate()
            {
                return DataContainerFacade.GetByID<AccountKind, long>(p_acc.ContragentID);
            });
        }
    }
}
