﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions
{
	public static class F050CBInfoExtension
	{
		public static EdoOdkF050 GetDocument(this F050CBInfo value)
		{
			if (value != null && value.EdoID.HasValue)
			{
				return DataContainerFacade.GetExtensionData<EdoOdkF050>(value, "Document", delegate()
				{
					return DataContainerFacade.GetByID<EdoOdkF050, long>(value.EdoID.Value);
				});
			}
			else
				return null;
		}

        public static IList<F050CBInfo> GetDetails(this F050ReportListItem value)
        {
            if (value != null )
            {
                return DataContainerFacade.GetExtensionData<IList<F050CBInfo>>(value, "Details", delegate()
                {
                    return DataContainerFacade.GetListByProperty<F050CBInfo>("EdoID", value.ID);
                });
            }
            else
                return null;
        }
	}
}
