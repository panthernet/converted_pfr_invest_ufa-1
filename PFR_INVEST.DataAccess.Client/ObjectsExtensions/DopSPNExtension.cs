﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client
{
    public static class DopSPNExtension
    {
        public static Portfolio GetPortfolio(this DopSPN p_spn)
        {
            if (p_spn != null)
                return DataContainerFacade.GetExtensionData<Portfolio>(p_spn, "Portfolio", delegate()
                {
                    return DataContainerFacade.GetByID<Portfolio, long>(p_spn.PortfolioID);
                });
            else
                return null;
        }

        public static LegalEntity GetLegalEntity(this DopSPN p_spn)
        {
            if (p_spn != null)
                return DataContainerFacade.GetExtensionData<LegalEntity>(p_spn, "LegalEntity", delegate()
                {
                    return DataContainerFacade.GetByID<LegalEntity, long>(p_spn.LegalEntityID);
                });
            else
                return null;
        }

        public static Month GetMonth(this DopSPN p_spn)
        {
            if (p_spn != null)
                return DataContainerFacade.GetExtensionData<Month>(p_spn, "Month", delegate()
                {
                    return DataContainerFacade.GetByID<Month, long>(p_spn.PeriodID);
                });
            else
                return null;
        }


    }
}
