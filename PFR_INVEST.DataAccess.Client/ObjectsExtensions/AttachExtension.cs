﻿using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class AttachExtension
    {
        public static Document GetDocument(this Attach p_att)
        {
            return DataContainerFacade.GetExtensionData<Document>(p_att, "Document", delegate()
            {
                return DataContainerFacade.GetByID<Document, long>(p_att.DocumentID);
            });
        }

        public static Person GetExecutor(this Attach p_att)
        {
			return DataContainerFacade.GetExtensionData<Person>(p_att, "Person", delegate()
            {
				return DataContainerFacade.GetByID<Person, long>(p_att.ExecutorID);
            });
        }

        public static AttachClassific GetAttachClassific(this Attach p_att)
        {
            return DataContainerFacade.GetExtensionData<AttachClassific>(p_att, "AttachClassific", delegate()
            {
                return DataContainerFacade.GetByID<AttachClassific, long>(p_att.AttachClassificID);
            });
        }
    }
}
