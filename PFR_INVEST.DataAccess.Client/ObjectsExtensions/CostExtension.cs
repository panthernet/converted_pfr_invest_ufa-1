﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Collections;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
	public static class CostExtension
	{
        public static Currency GetCurrency(this Cost p_c)
        {
            return DataContainerFacade.GetExtensionData<Currency>(p_c, "Currency", delegate()
            {
                return DataContainerFacade.GetByID<Currency, long>(p_c.CurrencyID);
            });
        }

        public static PfrBankAccount GetPfrBankAccount(this Cost p_c)
        {
            return DataContainerFacade.GetExtensionData<PfrBankAccount>(p_c, "PfrBankAccount", delegate()
            {
                return DataContainerFacade.GetByID<PfrBankAccount, long>(p_c.AccountID);
            });
        }

        public static Portfolio GetPortfolio(this Cost p_c)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(p_c, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(p_c.PortfolioID);
            });
        }

        public static LegalEntity GetLegalEntity(this Cost p_c)
        {
            return DataContainerFacade.GetExtensionData<LegalEntity>(p_c, "LegalEntity", delegate()
            {
                return DataContainerFacade.GetByID<LegalEntity, long>(p_c.BankAccountID);
            });
        }
    }
}
