﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
	public static class CbInOrderExtension
	{
		public static CbOrder GetOrder(this CbInOrder p_cbinorder)
		{
			if (p_cbinorder != null && p_cbinorder.OrderID.HasValue)
				return DataContainerFacade.GetExtensionData<CbOrder>(p_cbinorder, "CbOrder", delegate()
				{
					return DataContainerFacade.GetByID<CbOrder, long>(p_cbinorder.OrderID);
				});
			else
				return null;
		}

		public static Security GetSecurity(this CbInOrder p_cbinorder)
		{
			if (p_cbinorder != null && p_cbinorder.SecurityID.HasValue)
				return DataContainerFacade.GetExtensionData<Security>(p_cbinorder, "Security", delegate()
				{
					return DataContainerFacade.GetByID<Security, long>(p_cbinorder.SecurityID);
				});
			else
				return null;
		}
	}
}
