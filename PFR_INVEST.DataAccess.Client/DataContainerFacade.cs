﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.DataObjects.XMLModel.Auction;
using db2connector;

namespace PFR_INVEST.DataAccess.Client
{
	public static class DataContainerFacade
	{
#region WrongStatusType
		public class WrongStatusType
		{
            /// <summary>
            /// Свойство StatusID биндится к свойству Status (это у большинства исключений)
            /// </summary>
            public bool IsMasked { get; set; }
			/// <summary>
			/// Значение активного статуса по умолчанию
			/// </summary>
			public long DefaultActiveStatus { get; private set; }
			/// <summary>
			/// Тип сущности
			/// </summary>
			public Type EntityType { get; private set; }
			/// <summary>
			/// True - если поле в БД имеет тип BIGINT, иначе INT
			/// </summary>
			public bool IsLongId { get; private set; }

			public WrongStatusType(Type entityType, long defaultStatus, bool isLongId, bool isMasked = true)
			{
			    IsMasked = isMasked;
				EntityType = entityType;
				DefaultActiveStatus = defaultStatus;
				IsLongId = isLongId;
			}
		}

		/// <summary>
		/// Коллекция типов, с полем статус Status вместо StatusID. Требуется для корректной отработки авто запросов на удаление-сохранение как в факаде, так и на сервисе
		/// Long - указывает дефолтное значение активного статуса
		/// WrongStatusType - указывает параметры сущности
		/// </summary>
		public static readonly Dictionary<Type, WrongStatusType> WrongStatusTypesList = new Dictionary<Type, WrongStatusType>
        {
            {typeof (Contract), new WrongStatusType(typeof (Contract), 0, false, false)},
            {typeof (Attach), new WrongStatusType(typeof (Attach), 1, true)},
            {typeof (Document), new WrongStatusType(typeof (Document), 1, true)},
            {typeof (FileContent), new WrongStatusType(typeof (FileContent), 1, true)},
            {typeof (DelayedPaymentClaim), new WrongStatusType(typeof (DelayedPaymentClaim), 1, true)},
            {typeof (DepClaimSelectParams), new WrongStatusType(typeof (DepClaimSelectParams), 1, true)},
            {typeof (Deposit), new WrongStatusType(typeof (Deposit), 1, false, false)},
            {typeof (Division), new WrongStatusType(typeof (Division), 1, true)},
            {typeof (OfferPfrBankAccountSum), new WrongStatusType(typeof (OfferPfrBankAccountSum), 1, true)},
            {typeof (PaymentDetail), new WrongStatusType(typeof (PaymentDetail), 1, false)},
            {typeof (Person), new WrongStatusType(typeof (Person), 1, true)},
            {typeof (Rating), new WrongStatusType(typeof (Rating), 1, true)},
            {typeof (RatingAgency), new WrongStatusType(typeof (RatingAgency), 1, true)},
            {typeof (Security), new WrongStatusType(typeof (Security), 0, false, false)},
        };
#endregion

		/// <summary>
		/// GetByID version for long ID type
		/// </summary>
		/// <typeparam name="TObj"></typeparam>
		/// <param name="p_id"></param>
		/// <returns></returns>
		public static TObj GetByID<TObj>(long? p_id)
		{
			return GetByID<TObj, long>(p_id);
		}

		public static TObj GetByID<TObj, TID>(TID? p_id) where TID : struct
		{
			if (p_id.HasValue == false)
				return default(TObj);
			return (TObj)(ServiceSystem.Client.GetByID(typeof(TObj).FullName, p_id));
		}

		/// <summary>
		/// Save version for long ID type
		/// </summary>
		/// <typeparam name="TObj"></typeparam>
		/// <param name="p_obj"></param>
		/// <returns></returns>
		public static long Save<TObj>(TObj p_obj)
		{
			ClearExtensionData(p_obj);
			return Save<TObj, long>(p_obj);
		}

		private static void ClearExtensionData(object p_obj)
		{
			var o = p_obj as BaseDataObject;
			if (o != null)
				o.ExtensionData = null;//Зачищаем
		}

		/// <summary>
		/// Сохранение списка сущностей
		/// </summary>
		/// <typeparam name="TObj">Тип сущности</typeparam>
		/// <typeparam name="TID">Тип идентификатора сущности</typeparam>
		/// <param name="list">Список сущностей</param>
		/// <param name="returnIds">Возвращать идентификаторы сущностей (например, если добавляются новые)</param>
		public static IEnumerable<TID> BatchSave<TObj, TID>(List<TObj> list, bool returnIds = false)
		{
			//Если статус не выставлен - выставляем активный
			list.ForEach(a =>
			{
				var markedAsDeleted = a as IMarkedAsDeleted;
				if (markedAsDeleted != null && markedAsDeleted.StatusID == 0)
				{
					markedAsDeleted.StatusID = 1;
					var type = a.GetType();
					if (WrongStatusTypesList.ContainsKey(type))
					{
						markedAsDeleted.StatusID = WrongStatusTypesList[type].DefaultActiveStatus;
					}
				}
			});

			return ServiceSystem.Client.BatchSave(typeof(TObj).FullName, list.Cast<object>().ToList(), returnIds).Cast<TID>();
		}

		public static TID Save<TObj, TID>(TObj pObj)
		{
			//Если статус не выставлен - выставляем активный
			var markedAsDeleted = pObj as IMarkedAsDeleted;
			if (markedAsDeleted != null && markedAsDeleted.StatusID == 0)
			{
				markedAsDeleted.StatusID = 1;
				var type = pObj.GetType();
				if (WrongStatusTypesList.ContainsKey(type))
				{
					markedAsDeleted.StatusID = WrongStatusTypesList[type].DefaultActiveStatus;
				}
			}


			return (TID)(ServiceSystem.Client.Save(typeof(TObj).FullName, pObj));
		}

		public static void Delete<TObj>(TObj pObj)
		{
			if (pObj is IMarkedAsDeleted)
			{
				((IMarkedAsDeleted)pObj).StatusID = -1;
				Save(pObj);
			}
			else
			{
				ClearExtensionData(pObj);
				ServiceSystem.Client.Delete(typeof(TObj).FullName, pObj);
			}
		}

		public static void BatchDelete<TObj>(List<TObj> list)
		{
			if (!list.Any()) return;
			var sList = new List<object>();
			var dList = new List<object>();
			var markedAsDeleted = list.First() is IMarkedAsDeleted;
			list.ForEach(a =>
			{
				if (markedAsDeleted)
				{
					((IMarkedAsDeleted)a).StatusID = -1;
					sList.Add(a);
				}
				else dList.Add(a);
			});
			if (sList.Any())
				ServiceSystem.Client.BatchSave(typeof(TObj).FullName, sList);
			if (dList.Any())
				ServiceSystem.Client.BatchDelete(typeof(TObj).FullName, dList);
		}

		/// <summary>
		/// Удаление сущности по ID, менее производительная версия удаления
		/// </summary>
		/// <typeparam name="TObj"></typeparam>
		/// <param name="p_ID"></param>
		public static void Delete<TObj>(long p_ID)
		{
			var obj = GetByID<TObj>(p_ID);
			if (obj == null)
				return;
			Delete(obj);
		}

		public static IList<TObj> GetListByProperty<TObj>(string propertyName, object value, bool hideDeleted = false)
		{
		    var type = typeof (TObj);
			if (type.GetInterfaces().Any(item => item.FullName == typeof(IMarkedAsDeleted).FullName))
			{
				var pc = new List<ListPropertyCondition> { ListPropertyCondition.Equal(propertyName, value) };
                bool isWrong = WrongStatusTypesList.ContainsKey(type) && !WrongStatusTypesList[type].IsMasked;
				//НЕ УДАЛЯТЬ (object)!!! РЕШАРПЕР НЕ ПРАВ!!!
				if (hideDeleted) pc.Add(ListPropertyCondition.NotEqual(isWrong ? "Status" : "StatusID", isWrong ? (object)-1 : (object)-1L));
				return GetListByPropertyConditions<TObj>(pc).ToList();
			}

			var aList = ServiceSystem.Client.GetListByProperty(typeof(TObj).FullName, propertyName, value);
			return aList == null ? null : new List<TObj>(aList.Cast<TObj>());
		}

		public static long GetListByPropertyCount<TObj>(string propertyName, object value, bool hideDeleted = false)
		{
			if (!hideDeleted)
				return ServiceSystem.Client.GetListByPropertyCount(typeof(TObj).FullName, propertyName, value);

            var type = typeof(TObj);
            bool isWrong = WrongStatusTypesList.ContainsKey(type) && !WrongStatusTypesList[type].IsMasked;

			return GetListByPropertyConditionsCount<TObj>(new List<ListPropertyCondition>()
            {
                ListPropertyCondition.Equal(propertyName, value),
                //НЕ УДАЛЯТЬ (object)!!! РЕШАРПЕР НЕ ПРАВ!!!
                ListPropertyCondition.NotEqual(isWrong ? "Status" : "StatusID", isWrong ? (object)-1 : (object)-1L)
            });
		}

		public static IList<TObj> GetListByPropertyCondition<TObj>(string propertyName, object value, string operation)
		{
			var aList = ServiceSystem.Client.GetListByPropertyCondition(typeof(TObj).FullName, propertyName, value, operation);
			return aList == null ? null : new List<TObj>(aList.Cast<TObj>());
		}

		public static IList<TObj> GetListByPropertyConditions<TObj>(List<ListPropertyCondition> propertyConditionList)
		{
			var aList = ServiceSystem.Client.GetListByPropertyConditions(typeof(TObj).FullName, propertyConditionList);
			return aList == null ? null : new List<TObj>(aList.Cast<TObj>());
		}

        public static IList<TObj> GetListByPropertyConditions<TObj>(params ListPropertyCondition[] list)
        {
            return GetListByPropertyConditions<TObj>(list.ToList());
        }


        public static string GetXMLByPropertyConditions<TObj>(List<ListPropertyCondition> propertyConditionList)
		{
			var xml = ServiceSystem.Client.GetXMLByPropertyConditions(typeof(TObj).FullName, propertyConditionList);
			return xml;
		}

		public static Dictionary<string, object> GetEntityDBProjection<TObj>(TObj source)
		{
			var dict = ServiceSystem.Client.GetEntityDBProjection(typeof(TObj).FullName, source);
			return dict;
		}

		public static string GetXMLByList<TObj>(List<TObj> list)
		{
			var xml = ServiceSystem.Client.GetXMLByList(typeof(TObj).FullName, list);
			return xml;
		}

		public static IList<TObj> GetListByXML<TObj>(string xml)
		{
			var aList = ServiceSystem.Client.GetListByXML(typeof(TObj).FullName, xml);
			return new List<TObj>(aList.Cast<TObj>()); ;
		}

		public static long GetListByPropertyConditionsCount<TObj>(List<ListPropertyCondition> propertyConditionList)
		{
			var count = ServiceSystem.Client.GetListByPropertyConditionsCount(typeof(TObj).FullName, propertyConditionList);
			return count;
		}

        public static long GetListByPropertyConditionsCount<TObj>(params ListPropertyCondition[] propertyConditionList)
        {
            var count = ServiceSystem.Client.GetListByPropertyConditionsCount(typeof(TObj).FullName, propertyConditionList.ToList());
            return count;
        }

        public static long GetListCount<TObj>()
		{
			return ServiceSystem.Client.GetListCount(typeof(TObj).FullName);
		}

		public static List<TObj> GetListLimit<TObj>(int limit)
		{
			var aList = ServiceSystem.Client.GetListLimit(typeof(TObj).FullName, limit);
			if (aList == null)
				return null;
			return new List<TObj>(aList.Cast<TObj>());
		}

		public static List<TObj> GetListPage<TObj>(int startIndex, int limit)
		{
			var aList = ServiceSystem.Client.GetListPage(typeof(TObj).FullName, startIndex, limit);
			if (aList == null)
				return null;
			return new List<TObj>(aList.Cast<TObj>());
		}

		public static List<TObj> GetListPageConditions<TObj>(int startIndex, List<ListPropertyCondition> propertyConditionList)
		{
			return ServiceSystem.Client.GetListPageConditions(typeof(TObj).FullName, startIndex, propertyConditionList)
				.Cast<TObj>().ToList();
		}

		public static List<TObj> GetList<TObj>(bool hideDeleted = true)
		{
            var type = typeof(TObj);
            if (hideDeleted && type.GetInterfaces().Any(item => item.FullName == typeof(IMarkedAsDeleted).FullName))
			{
                bool isWrong = WrongStatusTypesList.ContainsKey(type) && !WrongStatusTypesList[type].IsMasked;
				//НЕ УДАЛЯТЬ (object)!!! РЕШАРПЕР НЕ ПРАВ!!!
				return GetListByPropertyCondition<TObj>(isWrong ? "Status" : "StatusID", isWrong ? (object)-1 : (object)-1L, "neq").ToList();
			}
			var aList = ServiceSystem.Client.GetList(type.FullName);
			return aList == null ? null : new List<TObj>(aList.Cast<TObj>());
		}

		public static IList<BlUserSetting> GetBlUserSettings()
		{
			return ServiceSystem.Client.GetBlUserSettings();
		}


		public static void SaveBlUserSetting(BlUserSetting setting)
		{
			ServiceSystem.Client.SaveBlUserSetting(setting);
		}

		public static IList<LoginMessageItem> GetLoginMessageItems()
		{
			return ServiceSystem.Client.GetLoginMessageItems();
		}

		public static void SetIgnoredLoginMessages(IList<string> mesagesKeys)
		{
			ServiceSystem.Client.SetIgnoredLoginMessages(mesagesKeys);
		}


		/// <summary>
		/// Получения значения свойства обьекта с сервера
		/// </summary>
		/// <typeparam name="TObj">Тип обьекта</typeparam>
		/// <typeparam name="TID">Тип ИД обьекта</typeparam>
		/// <typeparam name="TVal">Тип значения свойста обьекта</typeparam>
		/// <param name="p_id">ИД обьекта</param>
		/// <param name="p_sPropertyName">Имя свойства обьекта</param>
		/// <returns>Значение свойства обьекта</returns>
		public static TVal GetObjectProperty<TObj, TID, TVal>(TID? p_id, string p_sPropertyName) where TID : struct
		{
			if (p_id.HasValue == false)
				return default(TVal);

			object val = ServiceSystem.Client.GetObjectProperty(typeof(TObj).FullName, p_id.Value, p_sPropertyName);
			return (TVal)val;
		}

		public static long GetObjectPropertyCount<TObj>(long? p_id, string p_sPropertyName)
		{
			return p_id.HasValue == false ? default(long) : ServiceSystem.Client.GetObjectPropertyCount(typeof(TObj).FullName, p_id.Value, p_sPropertyName);
		}

		public delegate TVal GetExtensionDataDelegate<TVal>();

		public static void ResetExtensionData(BaseDataObject p_object, string p_sKey)
		{
			if (p_sKey == null)
				p_object.ExtensionData = null;
			else if (p_object.ExtensionData != null && p_object.ExtensionData.ContainsKey(p_sKey))
				p_object.ExtensionData.Remove(p_sKey);
		}


		/// <summary>
		/// Кеш данных ExtensionData
		/// </summary>
		/// <typeparam name="TVal">Тип обьекта</typeparam>
		/// <param name="p_object">Обьект</param>
		/// <param name="p_sKey">Ключ в ExtensionData</param>
		/// <param name="p_nonCachedDelegate">Функция получения данных</param>
		/// <returns></returns>
		public static TVal GetExtensionData<TVal>(BaseDataObject p_object, string p_sKey, GetExtensionDataDelegate<TVal> p_nonCachedDelegate)
		{
			if (p_object.ExtensionData == null)
			{
				p_object.ExtensionData = new Dictionary<string, object>();
			}

			if (p_object.ExtensionData.ContainsKey(p_sKey))
			{
				return (TVal)(p_object.ExtensionData[p_sKey]);
			}

			var nonCached = p_nonCachedDelegate();
			p_object.ExtensionData[p_sKey] = nonCached;

			return nonCached;

		}

		public static IList<TEntity> PacketLoader<TEntity, TKey>(Func<IList<TKey>> idsLoader, Func<IList<TKey>, IList<TEntity>> dataLoader)
			where TEntity : BaseDataObject
		{
			var retVal = new List<TEntity>();
			const int size = 1000;
			var keys = idsLoader();
			int packets = keys.Count() / size;
			for (int i = 0; i < packets; i++)
			{
				retVal.AddRange(dataLoader(keys.Skip(i * size).Take(size).ToArray()));
			}
			return retVal;
		}


		/// <summary>
		/// Экспорт уведомления о параметрах отбора заявок
		/// </summary>
		public static XMLFileResult ExportAuctionInfo(long auctionID)
		{
			//return null;
			return ServiceSystem.Client.ExportAuctionInfo(auctionID);
		}

		/// <summary>
		/// Экспорт лимитов
		/// </summary>
		public static XMLFileResult ExportBankLimitsInfo(long auctionID)
		{
			//return null;
			return ServiceSystem.Client.ExportBankLimitsInfo(auctionID);
		}

		public static List<Element> GetElements(Element.Types type) 
		{
			return ServiceSystem.Client.GetElementByType(type);
		}

		public static IDB2Connector GetClient() 
		{
			return ServiceSystem.Client;
		}
	}
}
