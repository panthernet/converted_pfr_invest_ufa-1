﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Newtonsoft.Json;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Web2.ClassLibrary.Models;

namespace PFR_INVEST.BusinessLogic.Web
{
    /// <summary>
    /// Единая базовая модель для всех веб-моделей данных
    /// </summary>
    public abstract class WebViewModel : IWebViewModel, ISavable
    {
        public IList<NameEntity> FormData { get; } = new List<NameEntity>();
        public string AjaxController { get; set; }
        public string AjaxAction { get; set; }
        public string AjaxPartial { get;  set; }
        public string AjaxMain { get; set; } = "_SharedAjaxFormPartialView";
        public string FormId { get; set; }
        public Size FormSize { get; protected set; }
        public bool IsModalEx { get; set; }
#if WEBCLIENT
        [JsonIgnore]
#endif
        public bool IsDeleteButtonVisible { get; set; } = true;
        /// <summary>
        /// Загрузить скрипты для грида по умолчанию на форме
        /// </summary>
#if WEBCLIENT
        [JsonIgnore]
#endif
        public bool AttachDefaultGridScripts { get; protected set; }

        /// <summary>
        /// Доп. параметр для AJAX запроса перезагрузки формы по запросу
        /// Нужен для передачи данных, например при обновлении формы при выборе элемента комбо-бокса
        /// </summary>        
        public int AjaxRequestParamValue { get; set; }

        public virtual void WebExecuteDelete(long id) { }

        protected virtual bool WebCanExecuteDelete()
        {
            return true;
        }

        public void WebDelete(long delID)
        {
            try
            {
#if WEBCLIENT
                if(DeleteAccessDenied || !WebCanExecuteDelete())
                    return;
#endif
                WebExecuteDelete(delID);
                IsDeleteCompleted = true;
                BLServiceSystem.Client.SaveDeleteEntryLog(GetType().Name, "", delID, 1);
            }
            catch
            {
                IsDeleteCompleted = false;
                throw;
            }
        }

        public string WebTitle { get; set; }

#if WEBCLIENT
        [JsonIgnore]
        public virtual bool EditAccessDenied => (this as ViewModelBase)?.WebEditAccessDenied ?? (this as PureWebViewModel)?.IsEditAccessDenied() ?? false;
        [JsonIgnore]
        public virtual bool DeleteAccessDenied => (this as ViewModelBase)?.DeleteAccessDenied ?? (this as PureWebViewModel)?.IsDeleteAccessDenied() ?? false;
        /// <summary>
        /// Указывает были ли изменены данные в модели
        /// </summary>
        [JsonIgnore]
        public virtual bool IsDataChanged { get; set; }
#else
        public bool EditAccessDenied => true;
        public bool IsDataChanged => false;
#endif

        /// <summary>
        /// Возвращает 
        /// </summary>
        /// <returns></returns>
        public abstract long WebGetEntryId();

        /// <summary>
        /// Указывает успешно ли прошло сохранение
        /// </summary>
#if WEBCLIENT
        [JsonIgnore]
#endif
        public virtual bool IsSaveCompleted { get; protected set; }

#if WEBCLIENT
        [JsonIgnore]
#endif
        public bool IsDeleteCompleted { get; protected set; }

        /// <summary>
        /// Выполняется после создания шаблона формы
        /// </summary>
        public virtual void WebPostModelCreateAction()
        {
        }

        /// <summary>
        /// Выполняется после обновления модели данными со страницы (POST)
        /// </summary>
        public virtual void WebPostModelUpdateAction()
        {
        }

        /// <summary>
        /// Последняя ошибка, заполняется при сохранении
        /// </summary>
        public string LastError { get; set; }

        public virtual bool WebExecuteSave()
        {
            return true;
        }

        public virtual bool WebBeforeExecuteSaveCheck()
        {
            return true;
        }


        protected virtual string WebValidate()
        {
            var model = this as IValidatableViewModel;
            return model?.Validate();
        }
#if WEBCLIENT
        [JsonIgnore]
#endif
        public string WebLastModelError { get; set; }

        public virtual bool WebExecuteSaveWithChecks()
        {
            if (!WebBeforeExecuteSaveCheck()) return false;

            bool result;
            try
            {
                result = WebExecuteSave();
            }
            catch (Exception ex)
            {
                result = false;
                Logger.Instance.Error($"WebExecuteSave {GetType().Name}", ex);
            }

            if (!result)
            {
                if (string.IsNullOrEmpty(WebLastModelError))
                    WebLastModelError = "Ошибка при сохранении объекта!";
                return false;
            }
            return true;
        }

        public string WebSave()
        {
            try
            {
                if (EditAccessDenied)
                    return "Недостаточно прав на редактирование!";
                LastError = null;
                var result = WebValidate();
                IsSaveCompleted = string.IsNullOrEmpty(result) && IsDataChanged;
                if (!string.IsNullOrEmpty(result))
                {
                    LastError = result;
                    return result;
                }
#if WEBCLIENT
                //TODO
                IsDataChanged = true;
#endif

                if (!IsDataChanged)
                {
                    return (LastError = "Нет новой информации для сохранения!");
                }
                IsSaveCompleted = WebExecuteSaveWithChecks();
                if (!IsSaveCompleted)
                    result = LastError = WebLastModelError;
                return result;
            }
            catch (Exception ex)
            {
                IsSaveCompleted = false;
                LastError = "Критическая ошибка!";
                Logger.Instance.Error($"WebSave {GetType().Name}", ex);
                return LastError;
            }
        }
    }

    public interface ISavable
    {
        bool WebExecuteSave();
        bool WebBeforeExecuteSaveCheck();
    }
}