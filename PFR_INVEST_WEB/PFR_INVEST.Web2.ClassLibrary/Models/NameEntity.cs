using System.Collections.Generic;

namespace PFR_INVEST.Web2.ClassLibrary.Models
{
    public class NameEntity
    {
        public string DisplayName { get; set; }
        public string DisplayCreateName { get; set; }
        public List<string> Grids { get; set; } = new List<string>();
        public string Id { get; set; }

        public bool HasGrids => Grids != null && Grids.Count > 0;
        public bool HasDisplayName => !string.IsNullOrEmpty(DisplayName);
        public int ModalWidth { get; set; }
        public string Model { get; set; }
        public List<string> ChildIds { get; set; } = new List<string>();
        public bool IsEnabled { get; set; } = true;
        public string[] ModelParams { get; set; }
        /// <summary>
        /// �������� �� ����� ����������, ��� ����� �������
        /// NULL - ������� ����� � �����������, ��������� � ����������
        /// DialogNoValidation - ������ �������/������ ��� ��������� �� ������� ������� 
        /// </summary>
        public string DialogMode { get; set; }
    }
}