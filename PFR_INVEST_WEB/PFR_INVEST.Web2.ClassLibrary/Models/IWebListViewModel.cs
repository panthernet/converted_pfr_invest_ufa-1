﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.Web;

namespace PFR_INVEST.Web2.ClassLibrary.Models
{
    public interface IWebListViewModel: IWebBaseViewModel
    {
        string Title { get; set; }
        string ListId { get; set; }
        /// <summary>
        /// Набор данных для форм, ассоциированных со списком
        /// </summary>
        IList<NameEntity> FormData { get; }

        IList List { get; }

        /// <summary>
        /// Ссылка на экшн контроллера
        /// </summary>
        string AjaxCallbackAction { get; }

    }

    public interface IWebLazyListViewModel: IWebListViewModel
    {
        IQueryable Query { get; }
    }
}