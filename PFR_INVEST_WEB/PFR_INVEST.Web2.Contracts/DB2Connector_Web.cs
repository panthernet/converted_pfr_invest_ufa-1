﻿using System.ServiceModel;
using PFR_INVEST.DataObjects;

namespace db2connector
{
    public partial interface IDB2Connector
    {
#if WEBCLIENT
        [OperationContract]
        System.Linq.IQueryable<RegisterListItem2> WebLazyGetSpnRegistersArchiveList();
#endif
    }
}
