﻿namespace PFR_INVEST.Web2.BusinessLogic
{
    public class WebDummyModelBase
    {
        public virtual bool IsDataChanged { get; set; } = true;
        public virtual bool EditAccessDenied { get; set; }

    }
}
