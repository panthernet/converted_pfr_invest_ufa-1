﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using JeffFerguson.Gepsio;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.Integration.XBRLProcessor
{
    public class XbrlParser
    {
        private readonly XbrlDocument _doc;
        private readonly string _xbrlPath;
        private readonly Logger _logger;

        public XbrlParser(string path, Logger logger)
        {
            if(string.IsNullOrEmpty(path))
                throw new ArgumentNullException();
            if(!File.Exists(path))
                throw new FileNotFoundException();
            _logger = logger;

            _doc = new XbrlDocument();
            _xbrlPath = path;
            try
            {
                _doc.Load(_xbrlPath);
            }
            catch (Exception ex)
            {
                _logger?.Error("Ошибка загрузки XBRL файла", ex);
                throw new Exception($"Ошибка загрузки XBRL файла {_xbrlPath}\n{ex.InnerException?.Message ?? ex.Message}");
                _doc = null;
            }

        }

        public string GenerateF070XML()
        {
            if (_doc == null) return null;
            var sb = new StringBuilder();

            try
            {
                var fragment = _doc.XbrlFragments.First();
                var context = fragment.Contexts.FirstOrDefault(a=> a.PeriodEndDate != DateTime.MinValue);
                var endDate = context.PeriodEndDate;
                var quarter = DateTools.GetQuarter(endDate);
                var year = endDate.Year;
                var dic = fragment.GetFacts("uk-dic");

                if (dic == null) return null;

                //calculations
                var ukname = dic.FirstOrDefault(a=> a.Name == "PolnNaimUK_Obsch")?.Value;
                var ukinn =  dic.FirstOrDefault(a=> a.Name == "InnUK")?.Value;
                var nomdog = dic.FirstOrDefault(a => a.Name == "NomDog")?.Value;
                var datedog = dic.FirstOrDefault(a => a.Name == "DataDog")?.Value;
                var prof1 = dic.Where(a => a.Name == "DoxPoInvirovaniyaSredstvPensNakop")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var prof2 = dic.Where(a => a.Name == "DoxPoInvirovaniyaSredstvPensNakop")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var as1 = dic.Where(a => a.Name == "FinansovyjRezultatOtRealizacziiAktivov")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var as2 = dic.Where(a => a.Name == "FinansovyjRezultatOtRealizacziiAktivov")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var div1 = dic.Where(a => a.Name == "DividendyIProczDoxodPoCzennymBumagam")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var div2 = dic.Where(a => a.Name == "DividendyIProczDoxodPoCzennymBumagam")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                
                var ar1 = dic.Where(a => a.Name == "FinansovyjRezultatOtPereoczenkiAktivov")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var ar2 = dic.Where(a => a.Name == "FinansovyjRezultatOtPereoczenkiAktivov")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";

                var op1 = dic.Where(a => a.Name == "DrugieVidyDoxOtOperaczijPoInvirovaniyuSredstvPensNakop")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var op2 = dic.Where(a => a.Name == "DrugieVidyDoxOtOperaczijPoInvirovaniyuSredstvPensNakop")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";

                
                var ret1 = dic.Where(a => a.Name == "RasxPoInvirovaniyuSredstvPensNakop")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var ret2 = dic.Where(a => a.Name == "RasxPoInvirovaniyuSredstvPensNakop")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";

                var sdpay1 = dic.Where(a => a.Name == "OplataUslugSpeczialDepozitariya")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var sdpay2 = dic.Where(a => a.Name == "OplataUslugSpeczialDepozitariya")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";

                var bp1 = dic.Where(a => a.Name == "OplataUslugProfUchRynkaCzenBumagBrokDilOrgTorgovliIDr")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var bp2 = dic.Where(a => a.Name == "OplataUslugProfUchRynkaCzenBumagBrokDilOrgTorgovliIDr")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";

                var aud1 = "0.00"; //TODO
                var aud2 = dic.FirstOrDefault(a => a.Name == "OplataUslugAuditora")?.Value ?? "0.00";

                
                var other1 = dic.Where(a => a.Name == "OplataProchixUslug")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var other2 = dic.Where(a => a.Name == "OplataProchixUslug")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";

                var midScha = dic.FirstOrDefault(a => a.Name == "Srednyaya_Scha")?.Value ?? "0.00";

                var smpsList = dic.Where(a => a.Name == "Summa_Vnov_PeredannyxSredstv");
                var sTotal = smpsList.FirstOrDefault(a => !fragment.GetContext(a.ContextRef).ScenarioHasExplicitMembers)?.Value ?? "0.00";
                var sJan = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVYanvareMember")?.Value ?? "0.00";
                var sFeb = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVFevraleMember")?.Value ?? "0.00";
                var sMar = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVMarteMember")?.Value ?? "0.00";
                var sApr = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVApreleMember")?.Value ?? "0.00";
                var sMay = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVMaeMember")?.Value ?? "0.00";
                var sJun = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVIyuneMember")?.Value ?? "0.00";
                var sJul = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVIyuleMember")?.Value ?? "0.00";//TODO 
                var sAug = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVAvgusteMember")?.Value ?? "0.00";//TODO 
                var sSep = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVSentyabreMember")?.Value ?? "0.00";//TODO 
                var sOct = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVOctyabreMember")?.Value ?? "0.00";//TODO 
                var sNov = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVNoyabreMember")?.Value ?? "0.00";//TODO 
                var sDec = smpsList.FirstOrDefault(a => fragment.GetContext(a.ContextRef).GetExplicitScenarioValue() == "PeredannyeVDecabreMember")?.Value ?? "0.00";//TODO 

                var maxUk = dic.FirstOrDefault(a => a.Name == "Predel_Rasxod_Uk_Investirovanie")?.Value ?? "0.00";
                var factUk = dic.FirstOrDefault(a => a.Name == "Fakt_Rasxod_Uk_Investirovanie")?.Value ?? "0.00";
                var maxSd = dic.FirstOrDefault(a => a.Name == "Predel_Rasxod_Speczdep")?.Value ?? "0.00";
                var factSd = dic.FirstOrDefault(a => a.Name == "Fakt_Rasxod_Speczdep")?.Value ?? "0.00";
                var pproc =  dic.FirstOrDefault(a => a.Name == "Doxod_Investirovanie_Pens_Nakopl_Vprocz")?.Value ?? "0.00";

                var fname = $"{Path.GetFileNameWithoutExtension(_xbrlPath)}F070";
                sb.Append("<?xml version=\"1.0\" encoding=\"windows-1251\"?>");
                sb.Append("<EDO_ODKF070>");
                //data
                sb.Append($"\t<Quartal>{quarter}</Quartal>");
                sb.Append($"\t<Year>{year}</Year>");
                sb.Append($"\t<UKName>{ukname}</UKName>");
                sb.Append($"\t<UKINN>{ukinn}</UKINN>");
                sb.Append($"\t<DogovorNumber>{nomdog}</DogovorNumber>");
                sb.Append($"\t<DogovorDate>{datedog}</DogovorDate>");
                sb.Append($"\t<InvestcaseName></InvestcaseName>");
                sb.Append($"\t<Profit1>{prof1}</Profit1>");
                sb.Append($"\t<Profit2>{prof2}</Profit2>");
                sb.Append($"\t<ActivSale1>{as1}</ActivSale1>");
                sb.Append($"\t<ActiveSale2>{as2}</ActiveSale2>");
                sb.Append($"\t<Dividend1>{div1}</Dividend1>");
                sb.Append($"\t<Dividend2>{div2}</Dividend2>");
                sb.Append($"\t<Proc1>0.00</Proc1>");
                sb.Append($"\t<Proc2>0.00</Proc2>");
                sb.Append($"\t<ActivRevalue1>{ar1}</ActivRevalue1>");
                sb.Append($"\t<ActivRevalue2>{ar2}</ActivRevalue2>");
                sb.Append($"\t<OtherProfit1>{op1}</OtherProfit1>");
                sb.Append($"\t<OtherProfit2>{op2}</OtherProfit2>");
                sb.Append($"\t<Retained1>{ret1}</Retained1>");
                sb.Append($"\t<Retained2>{ret2}</Retained2>");
                sb.Append($"\t<SDPay1>{sdpay1}</SDPay1>");
                sb.Append($"\t<SDPay2>{sdpay2}</SDPay2>");
                sb.Append($"\t<BrokerPay1>{bp1}</BrokerPay1>");
                sb.Append($"\t<BrokerPay2>{bp2}</BrokerPay2>");
                sb.Append($"\t<AuditPay1>{aud1}</AuditPay1>"); //TODO
                sb.Append($"\t<AuditPay2>{aud2}</AuditPay2>");
                sb.Append($"\t<StrahPay1>0.00</StrahPay1>"); //TODO
                sb.Append($"\t<StrahPay2>0.00</StrahPay2>"); //TODO
                sb.Append($"\t<OtherPay1>{other1}</OtherPay1>");
                sb.Append($"\t<OtherPay2>{other2}</OtherPay2>");
                sb.Append($"\t<UKReward1>0.00</UKReward1>"); //TODO
                sb.Append($"\t<UKReward2>0.00</UKReward2>"); //TODO
                sb.Append($"\t<MidValue>{midScha}</MidValue>");
                sb.Append($"\t<TranshTotal>{sTotal}</TranshTotal>");
                sb.Append($"\t<January>{sJan}</January>");
                sb.Append($"\t<February>{sFeb}</February>");
                sb.Append($"\t<March>{sMar}</March>");
                sb.Append($"\t<April>{sApr}</April>");
                sb.Append($"\t<May>{sMay}</May>");
                sb.Append($"\t<June>{sJun}</June>");
                sb.Append($"\t<July>{sJul}</July>");
                sb.Append($"\t<August>{sAug}</August>");
                sb.Append($"\t<September>{sSep}</September>");
                sb.Append($"\t<October>{sOct}</October>");
                sb.Append($"\t<November>{sNov}</November>");
                sb.Append($"\t<December>{sDec}</December>");
                sb.Append($"\t<MaxUKPay>{maxUk}</MaxUKPay>");
                sb.Append($"\t<FactUKPay>{factUk}</FactUKPay>");
                sb.Append($"\t<MaxSDPay>{maxSd}</MaxSDPay>");
                sb.Append($"\t<FactSDPay>{factSd}</FactSDPay>");
                sb.Append($"\t<ProfitProc>{pproc}</ProfitProc>");
                sb.Append($"\t<UKRewardProc>0.00</UKRewardProc>"); //TODO

                sb.Append($"\t<KPP/>");
                sb.Append($"\t<RegNumberOut>{fname}</RegNumberOut>");
                //end data
                sb.Append("</EDO_ODKF070>");
              //  var x = sb.ToString();
            }
            catch (Exception e)
            {
                _logger?.Error("F070 XBRL parse exception", e);
                return null;
            }            
            return sb.ToString();
        }

        public string GenerateF060XML()
        {
            if (_doc == null) return null;
            var sb = new StringBuilder();

            try
            {
                var fragment = _doc.XbrlFragments.First();
                var context = fragment.Contexts.FirstOrDefault(a=> a.PeriodEndDate != DateTime.MinValue);
              //  var ogrn = context.Identifier;
                //var contextDictionaryId = context.Id;
              //  var startDate = context.PeriodStartDate;
                var endDate = context.PeriodEndDate;
                var quarter = DateTools.GetQuarter(endDate);
                var year = endDate.Year;
                var dic = fragment.GetFacts("uk-dic");

                if (dic == null) return null;

                //calculations
                var ukname = dic.FirstOrDefault(a=> a.Name == "PolnNaimUK_Obsch")?.Value;
                var ukinn =  dic.FirstOrDefault(a=> a.Name == "InnUK")?.Value;
                var nomdog = dic.FirstOrDefault(a => a.Name == "NomDog")?.Value;
                var datedog = dic.FirstOrDefault(a => a.Name == "DataDog")?.Value;
                var schaList = dic.Where(a => a.Name == "StoimostChistyxAktivov").ToList();
                var scha1 = schaList.OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.InstantDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var scha2 = schaList.OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.InstantDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var schaend = schaList.FirstOrDefault(a => DateTools.GetQuarter(fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef).InstantDate) == quarter)?.Value ?? "0.00";

                var pr1 = dic.Where(a => a.Name == "Pens_Nakopl_Iz_Pfr")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var pr2 = dic.FirstOrDefault(a => a.Name == "Summa_Vnov_PeredannyxSredstv" && !fragment.GetContext(a.ContextRef).ScenarioHasExplicitMembers)?.Value ?? "0.00";
                
                var pt1 = dic.Where(a => a.Name == "Pens_Nakopl_V_Pfr")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var pt2 = dic.Where(a => a.Name == "Pens_Nakopl_V_Pfr").OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0)
                    .FirstOrDefault()?.Value ?? "0.00";
                var prof1 = dic.Where(a => a.Name == "DoxPoInvirovaniyaSredstvPensNakop")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var prof2 = dic.Where(a => a.Name == "DoxPoInvirovaniyaSredstvPensNakop")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var det1 = dic.Where(a => a.Name == "RasxPoInvirovaniyuSredstvPensNakop")
                    .OrderByDescending(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";
                var det2 = dic.Where(a => a.Name == "RasxPoInvirovaniyuSredstvPensNakop")
                    .OrderBy(a => fragment.Contexts.FirstOrDefault(c => c.Id == a.ContextRef)?.PeriodStartDate.Month ?? 0).FirstOrDefault()?.Value ?? "0.00";

                var fname = $"{Path.GetFileNameWithoutExtension(_xbrlPath)}F060";
                sb.Append("<?xml version=\"1.0\" encoding=\"windows-1251\"?>");
                sb.Append("<EDO_ODKF060>");
                //data
                sb.Append($"\t<UKName>{ukname}</UKName>");
                sb.Append($"\t<UKINN>{ukinn}</UKINN>");
                sb.Append($"\t<InvestcaseName></InvestcaseName>");
                sb.Append($"\t<DogovorNumber>{nomdog}</DogovorNumber>");
                sb.Append($"\t<DogovorDate>{datedog}</DogovorDate>");
                sb.Append($"\t<Year>{year}</Year>");
                sb.Append($"\t<Quartal>{quarter}</Quartal>");
                sb.Append($"\t<SCHAStart1>{scha1}</SCHAStart1>");
                sb.Append($"\t<SCHAStart2>{scha2}</SCHAStart2>");
                sb.Append($"\t<PaymentReceived1>{pr1}</PaymentReceived1>");
                sb.Append($"\t<PaymentReceived2>{pr2}</PaymentReceived2>");
                sb.Append($"\t<PaymentTransfer1>{pt1}</PaymentTransfer1>");
                sb.Append($"\t<PaymentTransfer2>{pt2}</PaymentTransfer2>");
                sb.Append($"\t<Profit1>{prof1}</Profit1>");
                sb.Append($"\t<Profit2>{prof2}</Profit2>");
                sb.Append($"\t<Detained1>{det1}</Detained1>");
                sb.Append($"\t<Detained2>{det2}</Detained2>");
                sb.Append($"\t<DetainedUKreward1>0.00</DetainedUKreward1>"); //TODO
                sb.Append($"\t<DetainedUKreward2>0.00</DetainedUKreward2>");
                sb.Append($"\t<SCHAEnd1>{schaend}</SCHAEnd1>");
                sb.Append($"\t<SCHAEnd2>{schaend}</SCHAEnd2>");
                sb.Append($"\t<KPP/>");
                sb.Append($"\t<RegNumberOut>{fname}</RegNumberOut>");
                //end data
                sb.Append("</EDO_ODKF060>");
               // var x = sb.ToString();
            }
            catch (Exception e)
            {
                _logger?.Error("F060 XBRL parse exception", e);
                return null;
            }            
            return sb.ToString();
        }
    }
}
