﻿using System;

namespace PFR_INVEST.DataObjects
{
    public class RejectApplicationWeb: IIdentifiable
    {
        public virtual long ID { get; set; }

        public virtual string RegNum { get; set; }

        public virtual long? Region { get; set; }

        public virtual DateTime? RegDate { get; set; }

        public virtual string SelectionIdentifier { get; set; }

        public virtual string DocType { get; set; }

        public virtual string ErrorCode { get; set; }

        public virtual string FromIdentifier { get; set; }

        public virtual string Portfolio { get; set; }

        public virtual string ContractDockCode { get; set; }

        public virtual long? FileId { get; set; }

        public virtual string SelectionName { get; set; }

        public virtual string FromName { get; set; }

        public RejectApplicationWeb() { }

        public RejectApplicationWeb(object[] input)
        {
            ID = (long) input[0];
            RegNum = (string) input[1];
            Region = (long)input[2];
            RegDate = (DateTime?) input[3];
            SelectionIdentifier =(string) input[4];
            DocType = (string) input[5];
            ErrorCode = (string) input[6];
            FromIdentifier = (string) input[7];
            Portfolio = (string) input[8];
            ContractDockCode = (string) input[9];
            FileId = (long)input[10];
            FromName = (string)input[11];
            SelectionName = (string)input[12];
        }
    }
}
