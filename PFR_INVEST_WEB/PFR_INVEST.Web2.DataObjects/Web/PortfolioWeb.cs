﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class PortfolioWeb: IIdentifiable, IStatusEntity
    {
        public virtual ElementWeb TypeElement { get; set; }
        //public virtual RegisterWeb Register { get; set; }
        /// <summary>
        /// Тип портфеля (ELEMENT 108)
        /// </summary>
        public enum Types: long
        {
            [Obsolete("Не использовать, техническое значение для сериализации", true)]
            Unknown = 0,

            NoType = 108000,
            SPN = 108001,
            DSV = 108002,
            ROPS = 108003
        }

        public long ID { get; set; }
        public string Year { get; set; }
        public long TypeElementID { get; set; }

        public long StatusID { get; set; }
        public long? YearID { get; set; }

        public Types TypeEl
        {
            get { return (Types)this.TypeElementID; }
            set { this.TypeElementID = (long)value; }
        }

        public string Type => TypeElement?.Name;

        public string Name
        {
            get { return Year; }
            set { Year = value; }
        }

    }
}
