﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class AttachClassificWeb: IIdentifiable
    {
        public virtual long ID { get; set; }
        
        public virtual string Name { get; set; }
    }
}
