﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class FileImport1CWeb : IIdentifiable
    {
        public virtual long ID { get; set; }

        public virtual long SessionID { get; set; }

        public virtual DateTime? LoadDate { get; set; }

        public virtual string DocNumber { get; set; }

        public virtual DateTime? DocDate { get; set; }

        public virtual string DocKind { get; set; }

        public virtual long ImportStatusID { get; set; }

        public virtual string Filename { get; set; }

        public virtual long FileID { get; set; }

        public virtual string StatusName { get; set; }
    }

    public class DocKinds1CWeb
    {
        public const string VYP = "ВЫП";
        public const string POST = "ПОСТ";
        public const string K_NPF = "НПФ";
        public const string K_NPFV = "НПФВ";
        public const string K_UK = "УК";
        public const string K_UKV = "УКВ";
        public const string K_DEP = "ДЕП";
        public const string K_DEPV = "ДЕПВ";
        public const string K_DEPVP = "ДЕПВП";
        public const string K_OPFR = "ОПФР";
        public const string K_OPFRV = "ОПФРВ";
        public const string K_GSO = "ГСО";
        public const string K_ZZL = "ЗЗЛ";
        public const string K_ZZLV = "ЗЗЛВ";
        public const string K_SOF = "СОФ";
        public const string K_MSK = "МСК";
        public const string K_DOST = "ДОСТ";
        public const string K_DOSTV = "ДОСТВ";
    }
}