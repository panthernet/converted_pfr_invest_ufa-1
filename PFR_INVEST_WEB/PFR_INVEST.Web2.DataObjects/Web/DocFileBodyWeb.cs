﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class DocFileBodyWeb : IIdentifiable
    {
        public virtual DocumentWeb Document { get; set; }

        public virtual long ID { get; set; }

        public virtual string FileName { get; set; }

        public virtual long? FileContentId { get; set; }

        public virtual long DocumentID { get; set; }
    }
}
