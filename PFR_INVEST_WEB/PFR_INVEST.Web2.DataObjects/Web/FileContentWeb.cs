﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class FileContentWeb : IMarkedAsDeleted, IIdentifiable, IStatusEntity
    {
        public virtual long ID { get; set; }

        public virtual byte[] FileContentData { get; set; }

        public virtual long StatusID { get; set; }
    }
}
