﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class Session1CWeb: IIdentifiable
    {
        public virtual long ID { get; set; }

        public virtual DateTime SessionDate { get; set; }

        public virtual int IsImport { get; set; }

        public virtual bool IsImportSession => IsImport == 1;

    }

}
