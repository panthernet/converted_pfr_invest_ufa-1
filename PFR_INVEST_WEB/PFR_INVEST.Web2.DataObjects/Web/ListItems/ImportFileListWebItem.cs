﻿namespace PFR_INVEST.Web2.DataObjects.Web.ListItems
{
    public class ImportFileListWebItem
    {
        public string FilePath { get; set; }

        public byte[] FileContent { get; set; }

        public bool IsCompressed { get; set; }
    }
}
