﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web.ListItems
{
    public class LinkedDocumentWebItem
    {
        public AttachWeb Attach { get; private set; }

        public LinkedDocumentWebItem(AttachWeb instance)
        {
            Attach = instance ?? new AttachWeb();
        }

        public string Classification => Attach.AttachClassific?.Name;

        public long ID => Attach.ID;

        public string AdditionalOfContent => Attach.Additional;

        public string Executor => Attach.Executor == null ? Attach.ExecutorName : Attach.Executor.FIOShort;

        public string ExecuteDate => Attach.ExecutionDate?.ToString("dd.MM.yyyy");
    }
}
