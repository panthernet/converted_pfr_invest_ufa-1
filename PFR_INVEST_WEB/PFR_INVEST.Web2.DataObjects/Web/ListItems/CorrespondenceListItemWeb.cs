﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web.ListItems
{
    public class CorrespondenceListItemWeb: IIdentifiable
    {
        public long ID { get; set; }

        public string Sender { get; set; }

       // public int? Year => IncomingDate?.Year;

      //  public string Month => IncomingDate.HasValue ? DateTools.GetMonthInWordsForDate(IncomingDate.Value.Month) : string.Empty;

        public DateTime? IncomingDate { get; set; }

        public string IncomingNumber { get; set; }

        public DateTime? OutgoingDate { get; set; }

        public string OutgoingNumber { get; set; }

        public string DocumentClass { get; set; }

        public string Executor { get; set; }

        public string AdditionalDocumentInfo { get; set; }

        public DateTime? ControlDate { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public bool IsExecutionExpired => ControlDate.HasValue && DateTime.Today > ControlDate &&
                                          (ExecutionDate == null || ExecutionDate != null && ExecutionDate > ControlDate);

        public bool IsAttach { get; set; }

       // public long AttachId { get; set; }



        //public long? AttachClassificID { get; set; }

        //public string AttachAdditional { get; set; }

        //public DateTime? AttachControlDate { get; set; }

        //public DateTime? AttachExecutionDate { get; set; }

       // public string AttachExecutorName { get; set; }

        //public string AttachClassific { get; set; }

       // public string AttachDocumentClass => $"{AttachClassific} ({CorrespondenceComments.Attachment})";

        //public string AttachExecutor => string.IsNullOrEmpty(AttachExecutorName) ? CorrespondenceComments.ExecutorNotAssigned : AttachExecutorName;

       // public bool IsAttachExecutionExpired => AttachControlDate.HasValue && DateTime.Today > AttachControlDate &&
       //                                   (AttachExecutionDate == null || AttachExecutionDate != null && AttachExecutionDate > AttachControlDate);


       // public long VisualID => IsAttach ? AttachId : ID;
       // public string VisualDocumentClass => IsAttach ? AttachDocumentClass : DocumentClass;
        //public string VisualExecutor => IsAttach ? AttachExecutor : Executor;
        //public string VisualAdditionalDocumentInfo => IsAttach ? AttachAdditional : AdditionalDocumentInfo;
        //public DateTime? VisualControlDate => IsAttach ? AttachControlDate : ControlDate;
        //public DateTime? VisualExecutionDate => IsAttach ? AttachExecutionDate : ExecutionDate;
       // public bool VisualIsAttachExecutionExpired => IsAttach ? IsAttachExecutionExpired : IsExecutionExpired;
    }
}
