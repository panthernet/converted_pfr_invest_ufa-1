﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    /// <summary>
    /// Модель ПП для экспорта данных (включает данные по портфелю и КБК)
    /// </summary>
    public class AsgFinTrExportWeb : IIdentifiable, IStatusEntity
    {
        public FinregisterWeb Finregister { get; set; }

        //public virtual AsgFinTrWeb AsgFinTr { get; set; }

        public long ID { get; set; }

        public long? FinregisterID { get; set; }

        /// <summary>
        /// Сумма ПП
        /// </summary>
        public decimal? DraftAmount { get; set; }

        public long? PortfolioID { get; set; }

        public long? DirectionElID { get; set; }

        public long? KBKID { get; set; }

        public long StatusID { get; set; }

        public string PortfolioName { get; set; }

        public string KBK { get; set; }

    }
}