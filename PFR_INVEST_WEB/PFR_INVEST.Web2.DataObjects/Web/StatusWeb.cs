﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class StatusWeb : IIdentifiable
    {
        public ICollection<ContragentWeb> Contragents { get; set; }

        public virtual long ID { get; set; }

        public virtual string Name { get; set; }
    }
}
