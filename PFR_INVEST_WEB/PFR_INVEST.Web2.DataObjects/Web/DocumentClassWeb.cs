﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class DocumentClassWeb: IIdentifiable
    {
        public long ID { get; set; }

        public string Name { get; set; }
    }
}
