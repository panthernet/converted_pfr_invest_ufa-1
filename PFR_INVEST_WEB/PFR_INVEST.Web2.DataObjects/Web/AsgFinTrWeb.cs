﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    /// <summary>
	/// Общее п/п (Ранее п/п для НПФ)
	/// </summary>
	public class AsgFinTrWeb : IIdentifiable, IStatusEntity
    {
        public virtual FinregisterWeb Finregister { get; set; }
        public virtual PortfolioWeb Portfolio { get; set; }

        public enum ContragentTypeEnum
        {
            None = 116001,
            UK = 116002,
            NPF = 116003,
            OPFR = 116004
        }

        // Element (Key = 101)
        public enum Directions
        {
            ToPFR = 101001, //Приход
            FromPFR = 101002 //Расход
        }

        //Element (Key = 100)
        public enum Links
        {
            NoLink = 100001,//'нет связи'    
            UK = 100002,//'Перечисления УК'    
            NPF = 100003,//'Передача СПН НПФ'    
            Deposit = 100004,//'Депозиты (размещение/возврат)'    
            Coupon = 100005,//'Купонный доход'    
            Budget = 100006,//'Средства Федерального Бюджета (МСК, Софинансирование)'    
            Region = 100007,//'Финансирование регионов (финансирование/возврат)',
            OPFR = 100008 // Перечисления ОПФР
        }

        //Element (Key = 102)
        public enum Sections
        {
            /// <summary>
            /// Работа с НПФ
            /// </summary>
            NPF = 102001,
            /// <summary>
            /// Работа с СИ
            /// </summary>
            SI = 102002,
            /// <summary>
            /// Работа с ВР
            /// </summary>
            VR = 102003,
            /// <summary>
            /// Работа с ЦБ и депозитами
            /// </summary>
            CB = 102004,
            /// <summary>
            /// Работа с регионами
            /// </summary>
            Region = 102005,
            /// <summary>
            /// Бэк-офис
            /// </summary>
            BackOffice = 102006,
            /// <summary>
            /// Все
            /// </summary>
            All = 102007
        }

        //public virtual AsgFinTrExportWeb AsgFinTrExport { get; set; }

        public long ContragentTypeID { get; set; } = (long) AsgFinTr.ContragentTypeEnum.None;

        /// <summary>
        /// Очищает связь с перечислением и финреестром
        /// </summary>
	    public void ClearLink()
	    {
            //аналогичная логика есть чистыми запросами к БД в DeleteSIVRRegister() и DeleteFinregisterInternal()
	        ReqTransferID = null;
	        FinregisterID = null;
            LinkedRegisterID = null;
            LinkElID = (long)AsgFinTr.Links.NoLink;
	    }

		public long ID { get; set; }

		public long? FinregisterID { get; set; }

		/// <summary>
		/// Сумма ПП
		/// </summary>
		public decimal? DraftAmount { get; set; }

		/// <summary>
		/// Дата ПП
		/// </summary>
		public DateTime? DraftDate { get; set; }

		public DateTime? DIDate { get; set; }  = DateTime.Today;

		/// <summary>
		/// Номер ПП
		/// </summary>
		public string DraftRegNum { get; set; }

		/// <summary>
		/// Дата перечисления СПН
		/// </summary>
		public DateTime? DraftPayDate { get; set; }

		public long? PFRBankAccountID { get; set; }

		public string Comment { get; set; }

        public long? PortfolioID => Portfolio?.ID;

		//Промежуточные поля, мапинг старый - имена новые

		public DateTime? PayDate { get { return DraftDate; } set { DraftDate = value; } }

		public string PayNumber { get { return DraftRegNum; } set { DraftRegNum = value; } }

		public decimal? PaySum { get { return DraftAmount; } set { DraftAmount = value; } }

		public DateTime? SPNDate { get { return DraftPayDate; } set { DraftPayDate = value; } }


		//Связи


		/// <summary>
		/// Ссылка на направление передачи (ELEMENT)
		/// </summary>
		public long? DirectionElID { get; set; }

		/// <summary>
		/// Ссылка на отдел ДОКИП (ELEMENT) 
		/// </summary>
		public long? SectionElID { get; set; }

		/// <summary>
		/// Ссылка на связь (ELEMENT)
		/// </summary>
		public long? LinkElID { get; set; }

		/// <summary>
		/// Ссылка на содерожание операции (PaymentDetail)
		/// </summary>
		public long? PaymentDetailsID { get; set; }

		public long? KBKID { get; set; }

		public int Unlinked { get; set; }

		/// <summary>
		/// Таблица лотуса, откуда смигрирована запись, только для чтения
		/// </summary>
		public string LotusTable { get; set; }

		/// <summary>
		/// Ссылка на связаный реестр
		/// </summary>
		public long? LinkedRegisterID { get; set; }

		public long? ReqTransferID { get; set; }

		[IgnoreDataMember]
		public bool IsMigrated => LotusTable != null;

        [IgnoreDataMember]
		public bool IsMigratedFromSaldo => IsMigrated && (LotusTable.Equals("TRANSFERUKFORM___ACCOUNTS", StringComparison.OrdinalIgnoreCase) 
		                                                          || LotusTable.Equals("TRANSFERUKFORM___ACCOUNTS_50", StringComparison.OrdinalIgnoreCase)
		                                                          || LotusTable.Equals("TRANSFERNPFFORM___ACCOUNTS", StringComparison.OrdinalIgnoreCase)
		                                                          || LotusTable.Equals("TRANSFERNPFFORM___ACCOUNTS_50", StringComparison.OrdinalIgnoreCase));

		public long StatusID { get; set; }

        public long? InnContractID { get; set; }

        public long? InnLegalEntityID { get; set; }

        public DateTime? PayerIssueDate { get; set; }

        public string CBPaymentDetail { get; set; }

        [IgnoreDataMember]
        public bool IsSelected { get; set; }

		[IgnoreDataMember]
        public bool IsSelectable => !string.IsNullOrEmpty(ContragentName);

        [IgnoreDataMember]
        public string ContragentName { get; set; }

	}
}
