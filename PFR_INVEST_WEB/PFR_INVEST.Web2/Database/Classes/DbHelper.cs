﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.Database.EF;

namespace PFR_INVEST.Web2.Database.Classes
{
    public static class DbHelper
    {
        public static AppDatabase GetEF()
        {
            return WCFClient.GetEFServiceFunc();
        }

        public static IEnumerable<T> GetUncachedList<T>(params string[] includes)
            where T: class, IIdentifiable
        {
            var dbSet = WCFClient.GetEFServiceFunc().Set<T>().AsNoTracking();
            DbQuery<T> query = null;
            foreach (var include in includes)
            {
                query = dbSet.Include(include);
            }

            return query;
        }

        public static T GetUncached<T>(long id, params string[] includes)
            where T: class, IIdentifiable
        {
            return DbSetGet<T>(id, false, includes);
        }

        public static T Get<T>(long id, params string[] includes)
            where T: class, IIdentifiable
        {
            return DbSetGet<T>(id, false, includes);
        }

        public static T Get<T>(long? id, params string[] includes)
            where T: class, IIdentifiable
        {
            return !id.HasValue ? null : Get<T>(id.Value, includes);
        }

        private static T DbSetGet<T>( long id, bool noTracking, params string[] includes)
            where T: class, IIdentifiable
        {
            var context = WCFClient.GetEFServiceFunc();
            var dbSet = noTracking? context.Set<T>().AsNoTracking() : context.Set<T>();

            if (includes.Length == 0)
            {
                var result = dbSet.FirstOrDefault(a => a.ID == id);
                if (result != null)
                    context.Entry(result).Reload();
                return result;
            }

            DbQuery<T> query = null;
            foreach (var include in includes)
            {
                query = dbSet.Include(include);
            }

            var res = query?.FirstOrDefault(a => a.ID == id);
            if (res != null)
                context.Entry(res).Reload();
            return res;
        }


        public static void SaveChanges()
        {
            WCFClient.GetEFServiceFunc().SaveChanges();
        }

        public static long SaveEntity<T>(T entity, int customStatus = 1) where T : class, IIdentifiable
        {
            return WCFClient.GetEFServiceFunc().SaveEntity(entity, customStatus);
        }

        public static bool DeleteEntity<T>(T entity, int customValue = -1, bool save = true) where T : class
        {
            return WCFClient.GetEFServiceFunc().DeleteEntity(entity, customValue, save);
        }
    }
}