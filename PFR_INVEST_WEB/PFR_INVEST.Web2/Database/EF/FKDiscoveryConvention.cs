﻿using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PFR_INVEST.Web2.Database.EF
{
    public class FKDiscoveryConvention : ForeignKeyDiscoveryConvention
    {
        protected override bool MatchDependentKeyProperty(AssociationType associationType, AssociationEndMember dependentAssociationEnd,
            EdmProperty dependentProperty, EntityType principalEntityType, EdmProperty principalKeyProperty)
        {
            string navigationPropertyName = ((System.Reflection.PropertyInfo)dependentAssociationEnd.MetadataProperties.GetValue("ClrPropertyInfo", false).Value).Name;

            // The standard foreign key property to look for is NavigationProperty_PrimaryKeyName (e.g. "TableName_Id"). 
            // Use the below line to remove that underscore.
            //return dependentProperty.Name == navigationPropertyName + principalKeyProperty.Name;

            // Use the following for the IdKotaLahir scenario
            return dependentProperty.Name == $"{navigationPropertyName}ID";

            
        }

        public override void Apply(AssociationType item, DbModel model)
        {
            if (!item.IsForeignKey)
            {
                return;
            }

            base.Apply(item, model);
        }

    }

    
    public class ForeignKeyNamingConvention : IStoreModelConvention<AssociationType>
    {
        public void Apply(AssociationType association, DbModel model)
        {
            if (!association.IsForeignKey) return;
            var constraint = association.Constraint;

            foreach (var property in constraint.ToProperties)
            {
                if (property.Name.EndsWith("_ID"))
                {
                    // change from EntityName_Id to id_EntityName
                    property.Name = $"{property.Name.Substring(0, property.Name.Length-3)}ID";
                }
            }
        }
    }

    public class RemoveUnderscoreForeignKeyNamingConvention : IStoreModelConvention<AssociationType>
    {
        public void Apply(AssociationType item, DbModel model)
        {
            if (!item.IsForeignKey)
            {
                return;
            }

            ReferentialConstraint constraint = item.Constraint;
            ICollection<EdmProperty> fromProperties = constraint.FromProperties;
            ICollection<EdmProperty> toProperties = constraint.ToProperties;
            if (DoPropertiesHaveDefaultNames(fromProperties, toProperties))
            {
                NormalizeForeignKeyProperties(fromProperties);
            }

            if (DoPropertiesHaveDefaultNames(toProperties, fromProperties))
            {
                NormalizeForeignKeyProperties(toProperties);
            }
        }

        private static bool DoPropertiesHaveDefaultNames(ICollection<EdmProperty> properties,
                                                         ICollection<EdmProperty> otherEndProperties)
        {
            if (properties.Count != otherEndProperties.Count)
            {
                return false;
            }

            using (IEnumerator<EdmProperty> propertiesEnumerator = properties.GetEnumerator())
            using (IEnumerator<EdmProperty> otherEndPropertiesEnumerator = otherEndProperties.GetEnumerator())
            {
                while (propertiesEnumerator.MoveNext() && otherEndPropertiesEnumerator.MoveNext())
                {
                    if (!propertiesEnumerator.Current.Name.EndsWith("_" + otherEndPropertiesEnumerator.Current.Name))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static void NormalizeForeignKeyProperties(IEnumerable<EdmProperty> properties)
        {
            foreach (EdmProperty edmProperty in properties)
            {
                int underscoreIndex = edmProperty.Name.IndexOf('_');
                if (underscoreIndex > 0)
                {
                    edmProperty.Name = edmProperty.Name.Remove(underscoreIndex, 1);
                }
            }
        }
    }

}

