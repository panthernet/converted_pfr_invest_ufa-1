﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.Database.EF.Mapping;
using PFR_INVEST.Proxy;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF
{
    public partial class AppDatabase : DbContext
    {
        private readonly object _locker = new object();
        public AppDatabase()
            : base("npgsqlcs")
        {
            Database.Log = sql =>
            {
                lock(_locker)
                    File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs\\ef.log"), sql);
            };
          //  var xx = this.Configuration;
        }

        public DbSet<RegisterWeb> SpnRegisters { get; set; }
        public DbSet<FinregisterWeb> SpnFinregisters { get; set; }
        public DbSet<AsgFinTrWeb> AsgFinTrs { get; set; }
        public DbSet<PortfolioWeb> Portfolios { get; set; }
        public DbSet<ContragentWeb> Contragents { get; set; }
        public DbSet<DocumentWeb> Documents { get; set; }
        public DbSet<AttachWeb> Attaches { get; set; }
        public DbSet<AttachClassificWeb> AttachClassifics { get; set; }
        public DbSet<DocumentClassWeb> DocumentClasses  { get; set; }
        public DbSet<PersonWeb> Persons { get; set; }
        public DbSet<DelayedPaymentClaimWeb> DelayedPaymentClaims { get; set; }
        public DbSet<PensionNotificationWeb> PensionNotifications { get; set; }
        public DbSet<ElementWeb> Elements { get; set; }
        public DbSet<DocFileBodyWeb> DocFileBodies { get; set; }
        public DbSet<AdditionalDocumentInfoWeb> AdditionalDocumentInfoes { get; set; }
        public DbSet<FileContentWeb> FileContents { get; set; }
        public DbSet<AccountWeb> Accounts { get; set; }
        public DbSet<ApproveDocWeb> ApproveDocs { get; set; }
        public DbSet<StatusWeb> Statuses { get; set; }
        public DbSet<RejectApplicationWeb> RejectApplications { get; set; }
        public DbSet<SettingsPathFileWeb> SettingsPathFiles { get; set; }
        public DbSet<Session1CWeb> Sessions1C { get; set; }
        public DbSet<Settings1CWeb> Settings1C { get; set; }
        public DbSet<Def1CWeb> Defs1C { get; set; }
        public DbSet<Journal1CWeb> Journals1C { get; set; }
        public DbSet<FileExport1CWeb> FileExports1C { get; set; }
        public DbSet<FileImport1CWeb> FileImports1C { get; set; }
        public DbSet<RepositoryImpExpFileWeb> RepositoryImpExpFiles { get; set; }
        public DbSet<BankAccountWeb> BankAccounts { get; set; }

        public DbSet<LegalEntityWeb> LegalEntities { get; set; }
        //  public DbSet<AsgFinTrExportWeb> AsgFinTrExports { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Configurations.Add(new ClassAccountMap());
            builder.Configurations.Add(new ClassAdditionalDocumentInfoMap());
            builder.Configurations.Add(new ClassApproveDocMap());
            builder.Configurations.Add(new ClassAsgFinTrMap());
            builder.Configurations.Add(new ClassAttachMap());
            builder.Configurations.Add(new ClassAttachClassificMap());
            builder.Configurations.Add(new ClassContragentAccountMap());
            builder.Configurations.Add(new ClassDelayedPaymentClaimtMap());
            builder.Configurations.Add(new ClassDocumentMap());
            builder.Configurations.Add(new ClassDocumentClassMap());
            builder.Configurations.Add(new ClassElementMap());
            builder.Configurations.Add(new ClassFinregisterMap());
            builder.Configurations.Add(new ClassLegalEntityAccountMap());
            builder.Configurations.Add(new ClassPensionNotificationMap());
            builder.Configurations.Add(new ClassPersonMap());
            builder.Configurations.Add(new ClassPortfolioMap());
            builder.Configurations.Add(new ClassRegisterMap());
            builder.Configurations.Add(new ClassReturnMap());
            builder.Configurations.Add(new ClassStatusMap());
            builder.Configurations.Add(new ClassDocFileBodyMap());
            builder.Configurations.Add(new ClassFileContentWeb());
            builder.Configurations.Add(new ClassSettingsPathFile());
            builder.Configurations.Add(new ClassSession1C());
            builder.Configurations.Add(new ClassSettings1C());
            builder.Configurations.Add(new ClassDefs1C());
            builder.Configurations.Add(new ClassJournal1C());
            builder.Configurations.Add(new ClassFileExport1C());
            builder.Configurations.Add(new ClassFileImport1C());
            builder.Configurations.Add(new ClassRepositoryImpExpFileWeb());
            builder.Configurations.Add(new ClassBankAccountWebMap());
            
            // PrimaryKeyNameForeignKeyDiscoveryConvention
           // builder.Conventions.Remove<ForeignKeyDiscoveryConvention>();
            //builder.Conventions.Add(new RemoveUnderscoreForeignKeyNamingConvention());
           // builder.Conventions.Add(new ForeignKeyNamingConvention());
            builder.Conventions.Add(new FKDiscoveryConvention());//TypeNameForeignKeyDiscoveryConvention
          //  builder.Configurations.Add(new ClassAsgFinTrExportMap());

            builder.HasDefaultSchema(IISServiceBase.DATA_SCHEME.ToLower());
            builder.Ignore<BaseDataObject>();
            base.OnModelCreating(builder);
        }

        public bool DeleteEntity<T>(T item, int customValue = -1, bool save = true)
            where T: class
        {
            if (typeof(IStatusEntity).IsAssignableFrom(typeof(T)))
            {
                ((IStatusEntity) item).StatusID = customValue;
            }
            else if (typeof(IOldStatusEntity).IsAssignableFrom(typeof(T)))
            {
                ((IOldStatusEntity) item).Status = customValue;
            }
            else
            {
                Set<T>().Remove(item);
                if (save)
                    SaveChanges();
                return true;
            }

            if (save)
                SaveChanges();

            return false;
        }

        public long SaveEntity<T>(T item, int customStatus = 1)
            where T : class, IIdentifiable
        {            
            if(Set<T>().FirstOrDefault(a=> a.ID == item.ID) == null)
                Set<T>().Add(item);
            var entity = item as IStatusEntity;
            if (entity != null)
                entity.StatusID = customStatus;
            if(item is IOldStatusEntity)
                throw new Exception("Перевести старый статус в новый!");
            SaveChanges();
            return item.ID;
        }

        public IQueryable GetActiveEntityQuery<T>(long deletedValue = -1)
            where T : class, IStatusEntity
        {
            return Set<T>().Where(a => a.StatusID != deletedValue);
        }

        public List<T> GetActiveEntityList<T>(long deletedValue = -1)
            where T : class, IStatusEntity
        {
            return Set<T>().Where(a => a.StatusID != deletedValue).ToList();
        }

        public T GetEntity<T>(long id)
            where T : class, IIdentifiable
        {
            return Set<T>().FirstOrDefault(a => a.ID == id);
        }
    }
}