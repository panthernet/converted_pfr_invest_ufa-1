﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassRegisterMap: EntityTypeConfiguration<RegisterWeb>
    {
        public ClassRegisterMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            ToTable("register");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.PayAssignment).HasColumnName("pay_assignment");
            Property(t => t.FZ_ID).HasColumnName("fz_id");
            Property(t => t.Kind).HasColumnName("kind");
            Property(t => t.RegNum).HasColumnName("regnum");
           // this.Property(t => t.PortfolioID).HasColumnName("pf_id");
            Property(t => t.ApproveDocID).HasColumnName("appd_id");
            Property(t => t.RegDate).HasColumnName("regdate");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.Company).HasColumnName("company");
            Property(t => t.ERZL_ID).HasColumnName("erzl_id");
            Property(t => t.ReturnID).HasColumnName("return_id");
            Property(t => t.Content).HasColumnName("content");
            Property(t => t.TrancheNumber).HasColumnName("tranchenum");
            Property(t => t.TrancheDate).HasColumnName("tranchedate");
            Property(t => t.StatusID).HasColumnName("status_id");
            Property(t => t.IsCustomArchive).HasColumnName("is_custom_archive");
            Property(t => t.PayAssignment).HasColumnName("pay_assignment");
            Property(t => t.PortfolioID).HasColumnName("pf_id");

            HasOptional(t => t.Return);
           // HasOptional(t => t.Portfolio).WithOptionalPrincipal(t=> t.Register);

            //HasOptional(t => t.ApproveDoc).WithRequired(t => t.Register);
            HasOptional(t => t.ApproveDoc);
        }
    }
}