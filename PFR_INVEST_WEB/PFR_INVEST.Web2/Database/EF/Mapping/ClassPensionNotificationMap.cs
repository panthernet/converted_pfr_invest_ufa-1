﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassPensionNotificationMap : EntityTypeConfiguration<PensionNotificationWeb>
    {
        public ClassPensionNotificationMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("pension_notification");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.SenderID).HasColumnName("sender_id");
            Property(t => t.StatusID).HasColumnName("status_id");
            Property(t => t.DeliveryDate).HasColumnName("delivery_date");
            Property(t => t.NotifTypeID).HasColumnName("notif_type_el_id");
            Property(t => t.NotifNum).HasColumnName("notif_num");
            Property(t => t.NotifDate).HasColumnName("notif_date");
            Property(t => t.RegisterNum).HasColumnName("register_num");
            Property(t => t.RegisterDate).HasColumnName("register_date");

            HasRequired(t => t.Sender).WithMany(t=> t.PensionNotifications).HasForeignKey(t=> t.SenderID);
            HasOptional(t => t.NotifType);

            Ignore(t => t.IsDeleted);
            Ignore(t => t.LegalEntityName);
            Ignore(t => t.NotifTypeName);
        }

    }

    public class ClassBankAccountWebMap : EntityTypeConfiguration<BankAccountWeb>
    {
        public ClassBankAccountWebMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("bankaccount");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.LegalEntityName).HasColumnName("legalentityname");
            Property(t => t.AccountNumber).HasColumnName("accountnum");
            Property(t => t.BankName).HasColumnName("bank");
            Property(t => t.BankLocation).HasColumnName("banklocation");
            Property(t => t.CorrespondentAccountNumber).HasColumnName("corraccountnum");
            Property(t => t.CorrespondentAccountNote).HasColumnName("corraccountnote");
            Property(t => t.BIK).HasColumnName("bik");
            Property(t => t.INN).HasColumnName("inn");
            Property(t => t.KPP).HasColumnName("kpp");
            Property(t => t.CloseDate).HasColumnName("closedate");
            Property(t => t.LegalEntityID).HasColumnName("legalentityid");
            Property(t => t.PfrBranchID).HasColumnName("pfrbranch_id");

            HasRequired(t => t.LegalEntity).WithMany(t=> t.BankAccounts).HasForeignKey(t=> t.LegalEntityID);
            Ignore(t => t.FormalizedName);
        }

    }
}