﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassElementMap : EntityTypeConfiguration<ElementWeb>
    {
        public ClassElementMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("element");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Key).HasColumnName("key");
            Property(t => t.Order).HasColumnName("order");
            Property(t => t.Visible).HasColumnName("visible");

            Ignore(t => t.Type);
            Ignore(t => t.IsVisible);

        }
    }
}