﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassSession1C: EntityTypeConfiguration<Session1CWeb>
    {
        public ClassSession1C()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("ones_session");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.SessionDate).HasColumnName("session_date");
            Property(t => t.IsImport).HasColumnName("is_import");

            Ignore(t => t.IsImportSession);
        }
    }

    public class ClassSettings1C: EntityTypeConfiguration<Settings1CWeb>
    {
        public ClassSettings1C()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("ones_opsettings");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.SetDefID).HasColumnName("setdef_id");
            Property(t => t.OperationContentID).HasColumnName("operation_content_id");
            Property(t => t.PortfolioTypeID).HasColumnName("portfolio_type_id");
            Property(t => t.Comment).HasColumnName("v_comment");
        }
    }

    public class ClassDefs1C: EntityTypeConfiguration<Def1CWeb>
    {
        public ClassDefs1C()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("ones_settingsdef");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.ExportGroup).HasColumnName("export_group");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.Comment).HasColumnName("v_comment");
        }
    }

    public class ClassJournal1C: EntityTypeConfiguration<Journal1CWeb>
    {
        public ClassJournal1C()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("ones_journal");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Action).HasColumnName("action");
            Property(t => t.SessionID).HasColumnName("session_id");
            Property(t => t.ImportID).HasColumnName("import_id");
            Property(t => t.LogDate).HasColumnName("log_date");
            Property(t => t.LogType).HasColumnName("log_type");
            Property(t => t.Message).HasColumnName("message");

            Ignore(t => t.LogTypeName);
        }
    }

    public class ClassFileExport1C: EntityTypeConfiguration<FileExport1CWeb>
    {
        public ClassFileExport1C()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("ones_fileexport");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.SessionID).HasColumnName("session_id");
            Property(t => t.EntityType).HasColumnName("entity_type");
            Property(t => t.EntityID).HasColumnName("entity_id");
            Property(t => t.ExportID).HasColumnName("export_id");
        }
    }

    public class ClassFileImport1C: EntityTypeConfiguration<FileImport1CWeb>
    {
        public ClassFileImport1C()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("ones_fileexport");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.SessionID).HasColumnName("session_id");
            Property(t => t.LoadDate).HasColumnName("load_date");
            Property(t => t.DocNumber).HasColumnName("doc_number");
            Property(t => t.DocDate).HasColumnName("doc_date");
            Property(t => t.DocKind).HasColumnName("doc_kind");
            Property(t => t.ImportStatusID).HasColumnName("import_status_id");
            Property(t => t.Filename).HasColumnName("filename");
            Property(t => t.FileID).HasColumnName("file_id");
            //TODO STatusName
        }
    }

    public class ClassRepositoryImpExpFileWeb: EntityTypeConfiguration<RepositoryImpExpFileWeb>
    {
        public ClassRepositoryImpExpFileWeb()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("repository_impexp_file");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.DDate).HasColumnName("ddate");
            Property(t => t.TTime).HasColumnName("ttime");
            Property(t => t.UserName).HasColumnName("username");
            Property(t => t.AuctionDate).HasColumnName("auction_date");
            Property(t => t.AuctionLocalNum).HasColumnName("auction_localnum");
            Property(t => t.Operacia).HasColumnName("operacia");
            Property(t => t.ImpExp).HasColumnName("imp_exp");
            Property(t => t.Key).HasColumnName("key");
            Property(t => t.Repository).HasColumnName("repository");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.Status).HasColumnName("status");
        }
    }
}