﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassFileContentWeb : EntityTypeConfiguration<FileContentWeb>
    {
        public ClassFileContentWeb()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("file_content");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.FileContentData).HasColumnName("file_content");
            Property(t => t.StatusID).HasColumnName("status");

          //  HasOptional(t => t.Contragent).WithMany(t => t.Accounts).HasForeignKey(t => t.ContragentID);
        }
    }
}