﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassDocFileBodyMap : EntityTypeConfiguration<DocFileBodyWeb>
    {
        public ClassDocFileBodyMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("docfilebody");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.DocumentID).HasColumnName("doc_id");
            Property(t => t.FileContentId).HasColumnName("file_content_id");
            Property(t => t.FileName).HasColumnName("filename");

            HasRequired(t => t.Document).WithOptional(t=> t.DocFileBody);
        }
    }
}