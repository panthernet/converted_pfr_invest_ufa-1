﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassSettingsPathFile: EntityTypeConfiguration<SettingsPathFileWeb>
    {
        public ClassSettingsPathFile()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("settings_path_file");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Date).HasColumnName("date");
            Property(t => t.ServClient).HasColumnName("serv_client");
            Property(t => t.Path).HasColumnName("path");
            Property(t => t.Comment).HasColumnName("comment");

            Ignore(t => t.ServClientValue);
        }
    }
}