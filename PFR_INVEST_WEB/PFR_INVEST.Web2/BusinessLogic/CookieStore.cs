﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public class CookieStore
    {
        public const string COOKIE_LIST_HISTORY = "CookieListHistory";
        private const int HIST_COOKIE_VERSION = 1;

        public static void SetCookie(string key, string value)
        {
            SetCookie(key, value, TimeSpan.FromDays(30));
        }


        public static void SetCookie(string key, string value, TimeSpan expires)
        {
            var cookie = new HttpCookie(key, value);

            if (HttpContext.Current.Request.Cookies[key] != null)
            {
                var cookieOld = HttpContext.Current.Request.Cookies[key];
                cookieOld.Expires = DateTime.Now.Add(expires);
                cookieOld.Value = cookie.Value;
                HttpContext.Current.Response.SetCookie(cookieOld);
            }
            else
            {
                cookie.Expires = DateTime.Now.Add(expires);
                HttpContext.Current.Response.SetCookie(cookie);
            }
        }
        public static string GetCookie(string key)
        {
            string value = string.Empty;
            var cookie = HttpContext.Current.Request.Cookies[key];

            if (cookie != null)
                value = cookie.Value;
            return value;
        }

        private static List<KeyValuePair<string, dynamic>> ParseHistoryCookie(string listString)
        {
            if(string.IsNullOrEmpty(listString)) return new List<KeyValuePair<string, dynamic>>();
            var list = listString.Split('|').ToList();
            int version;
            if (!int.TryParse(list[0], out version) || version != HIST_COOKIE_VERSION)
                return new List<KeyValuePair<string, dynamic>>();
            list.RemoveAt(0);

            return list.Select(a =>
            {
                var arr = a.Split(':');
                if (arr.Length < 4)
                    return new KeyValuePair<string, dynamic>();
                return new KeyValuePair<string, dynamic>(arr[1], new { Controller = arr[0], EntryId = arr[2], Category = arr[3] });
            }).Where(a=> a.Key != null).ToList();
        }

        public static void UpdateHistoryCookie(string username, string id, string category, string entryId = "0")
        {
            var cookieName = $"{username}{COOKIE_LIST_HISTORY}";
            var listString = GetCookie(cookieName);
            var data = ParseHistoryCookie(listString);

            var item = data.FirstOrDefault(a => a.Key == id);
            if (item.Value != null)
            {
                data.RemoveAt(data.IndexOf(item));
                data.Insert(0, item);
            }
            else data.Insert(0, new KeyValuePair<string, dynamic>( id, new { Controller = category, EntryId = entryId, Category = category }));

            var sb = new StringBuilder();
            sb.Append(HIST_COOKIE_VERSION);
            sb.Append("|");
            data.ForEach(a => sb.Append($"{a.Value.Controller}:{a.Key}:{a.Value.EntryId}:{a.Value.Category}|"));
            sb.Remove(sb.Length - 1, 1);

            SetCookie(cookieName, sb.ToString());
        }

        public static string RemoveFromHistoryCookie(string username, string id, string entryId = "0")
        {
            var cookieName = $"{username}{COOKIE_LIST_HISTORY}";
            var listString = GetCookie(cookieName);
            if (string.IsNullOrEmpty(listString)) return null;
            var data = ParseHistoryCookie(listString);
            var item = data.FirstOrDefault(a => a.Key == id && a.Value.EntryId == entryId);
            if(item.Value == null) return null;
            data.Remove(item);

            var sb = new StringBuilder();
            sb.Append(HIST_COOKIE_VERSION);
            sb.Append("|");
            data.ForEach(a => sb.Append($"{a.Value.Controller}:{a.Key}:{a.Value.EntryId}:{a.Value.Category}|"));
            sb.Remove(sb.Length - 1, 1);
            SetCookie(cookieName, sb.ToString());
            return sb.ToString();
        }

        public static string[] GetHistoryCookieEntries(string username)
        {
            try
            {
                var cookieName = $"{username}{COOKIE_LIST_HISTORY}";
                var listString = GetCookie(cookieName);
                if (string.IsNullOrEmpty(listString)) return null;
                var data = ParseHistoryCookie(listString);
                return data.Select(a => string.Join("|", a.Key, XmlHelper.GetRibbonElementText(a.Key, (string) a.Value.Controller), (string)a.Value.Category)).ToArray();
            }
            catch
            {
                return null;
            }
        }

        public static string[] GetHistoryCookieEntriesFromCache(string cache)
        {
            if (string.IsNullOrEmpty(cache)) return null;
            var data = ParseHistoryCookie(cache);
            return data.Select(a => string.Join("|", a.Key, XmlHelper.GetRibbonElementText(a.Key, (string)a.Value.Controller), (string)a.Value.Category)).ToArray();
        }
    }
}