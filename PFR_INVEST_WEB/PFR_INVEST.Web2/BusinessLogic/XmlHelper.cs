using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using DevExpress.DashboardCommon.Native;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class XmlHelper
    {

        public static string GetRibbonElementText(string id, string controller)
        {
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"App_Data\\Ribbon\\{controller.ToUpper()[0]}{controller.Substring(1,controller.Length-1)}Interface.xml");

            var el = XElement.Load(file).DescendantNodes().OfType<XElement>().Where(x => x.Name == "Item").FirstOrDefault(a=> a.GetAttributeValue("Name") == id);

            var res2 =  el?.Attributes("Text").FirstOrDefault()?.Value;
            return res2;
        }

        public static IEnumerable<NameEntity> ParseAllXmlNames()
        {
            var list = new List<NameEntity>();
            Directory.EnumerateFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"App_Data\\Ribbon"),"*.xml").ForEach(file=> list.AddRange(ParseXmlNames(file)));            
            return list;
        }

        private static IEnumerable<NameEntity> ParseXmlNames(string filename)
        {
            return XElement.Load(filename).DescendantNodes().OfType<XElement>().Where(x => x.Name == "Item").Select(a =>
            {
                var e = new NameEntity
                {
                    Id = a.Attributes("Name").FirstOrDefault()?.Value,
                    DisplayName = a.Attributes("DisplayName").FirstOrDefault()?.Value ?? a.Attributes("Text").FirstOrDefault()?.Value,
                    DisplayCreateName = a.Attributes("DisplayCreateName").FirstOrDefault()?.Value,
                    ModalWidth = int.Parse(a.Attributes("ModalWidth").FirstOrDefault()?.Value ?? "0"),
                    Grids = a.Attributes("Grids").FirstOrDefault()?.Value.Split('|').ToList() ?? new List<string>(),
                    ChildIds = a.Attributes("ChildIds").FirstOrDefault()?.Value.Split('|').ToList() ?? new List<string>(),
                    IsEnabled = a.Attributes("IsEnabled").FirstOrDefault()?.Value == null || Convert.ToBoolean(a.Attributes("IsEnabled").FirstOrDefault()?.Value),
                    DialogMode = a.Attributes("DialogMode").FirstOrDefault()?.Value,
                };

                var d = a.Attributes("Model").FirstOrDefault()?.Value;
                if (!string.IsNullOrEmpty(d))
                {
                    var arr = d.Split('|');
                    e.Model = arr[0];
                    e.ModelParams = arr.Length == 1 ? null : arr[1].Split(new [] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }
                

                return e;
            });
        }
    }
}