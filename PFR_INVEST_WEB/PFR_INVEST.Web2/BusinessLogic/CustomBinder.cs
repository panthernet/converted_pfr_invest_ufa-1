﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.BusinessLogic
{
    /// <summary>
    /// Биндер, который предварительно загружает модель из БД по идентификтору и обновляет данными, поступающими с формы
    /// </summary>
    public class CustomBinder: DevExpressEditorsBinder
    {
        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = bindingContext.Model as IWebViewModel;
            model?.WebPostModelUpdateAction();
            base.OnModelUpdated(controllerContext, bindingContext);
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var o = base.BindModel(controllerContext, bindingContext);
            var model = bindingContext.Model as IWebViewModel;
            return o;
        }

        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            try
            {
                //для отлова исключений привязки контекста!
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
               // if (propertyDescriptor.PropertyType == typeof(String))
              //      propertyDescriptor.SetValue(bindingContext.Model, propertyDescriptor.GetValue(bindingContext.Model) ?? String.Empty);
            }
            catch (Exception ex)
            {

            }
        }

        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            // fallback to the type's default constructor
            Type typeToCreate = modelType;

            // we can understand some collection interfaces, e.g. IList<>, IDictionary<,>
            if (modelType.IsGenericType)
            {
                Type genericTypeDefinition = modelType.GetGenericTypeDefinition();
                if (genericTypeDefinition == typeof(IDictionary<,>))
                {
                    typeToCreate = typeof(Dictionary<,>).MakeGenericType(modelType.GetGenericArguments());
                }
                else if (genericTypeDefinition == typeof(IEnumerable<>) || genericTypeDefinition == typeof(ICollection<>) || genericTypeDefinition == typeof(IList<>))
                {
                    typeToCreate = typeof(List<>).MakeGenericType(modelType.GetGenericArguments());
                }
            }

            try
            {
                var isWebModel = typeof(IWebViewModel).IsAssignableFrom(bindingContext.ModelType);
                var formId = bindingContext.ValueProvider.GetValue("FormID")?.AttemptedValue;
                var idString = bindingContext.ValueProvider.GetValue("EntryID")?.AttemptedValue;
                var parentIdString = bindingContext.ValueProvider.GetValue("ParentEntryID")?.AttemptedValue;

                var id = string.IsNullOrEmpty(idString) ? 0L : long.Parse(idString);
                var parentId = string.IsNullOrEmpty(parentIdString) ? 0L : long.Parse(parentIdString);
                if (isWebModel)
                {
                    var prms = FormDataHelper.GetConstructorParams(formId, new dynamic[] {new {Name = "id", Value = id}, new {Name = "parentId", Value = parentId}});
                    var m = EntityCreator.CreateWebViewModel<IWebViewModel>(bindingContext.ModelType, prms);
                    if (id > 0)
                        return m;
                    m.WebPostModelCreateAction();
                    return m;
                }

                var m2 = Activator.CreateInstance(typeToCreate);
                (m2 as IWebViewModel)?.WebPostModelCreateAction();
                return m2;
            }
            catch (MissingMethodException exception)
            {
                Logger.LogEx(exception, "Custom binder exception");
                throw;
            }
        }

    }
}