﻿namespace PFR_INVEST.Web2.BusinessLogic
{
    public class PopupControlParams
    {
        public string Name { get; set; }
        public string BeginCallback { get; set; }
        public string EndCallback { get; set; }
        public string ShowMethod { get; set; }
        public string FormId { get; set; }
        public string FormTitle { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsResizable { get; set; } = true;
        public string ClosedMethod { get; set; }
        public bool IsModal { get; set; }
        public long ParentEntryId { get; set; }
        public string ParentId { get; set; }
        public int Layer { get; set; }
        public string InputParams { get; set; }
    }
}