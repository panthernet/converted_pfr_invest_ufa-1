﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class FormDataHelper
    {
        public static Dictionary<string, NameEntity> Data { get; private set; }

        public static void Initialize()
        {
            Data = XmlHelper.ParseAllXmlNames().ToDictionary(a => a.Id, a=> a);
            AddCustomDialogs();
        }

        private static void AddCustomDialogs()
        {
            Data.Add("dlgAnnulDate", new NameEntity { DisplayName = "Аннулирование лицензии", Id = "dlgAnnulDate", ModalWidth = 400, Model= "WebDlgAnnulDateViewModel" });
            Data.Add("dlgRetireChief", new NameEntity { DisplayName = "Прекращение полномочий", Id = "dlgRetireChief", ModalWidth = 400, Model= "WebDlgRetireChiefViewModel" });
            Data.Add("dlgDeletedCouriersList", new NameEntity { DisplayName = "Удаленные контакты", Id = "dlgDeletedCouriersList", ModalWidth = 800, Model = "WebDlgDeletedCouriersListViewModel|id" });

            Data.Add("NpfDzlEditItem", new NameEntity { DisplayName = "Умершие ЗЛ", Id = "NpfDzlEditItem", ModalWidth = 800, Model = "WebNpfDeadZlViewModel", ModelParams = new [] { "id" } ,Grids = new List<string> { "NpfDzlList"}});
            Data.Add("NpfCorrespLinkedItem", new NameEntity { DisplayName = "Связанный документ", Id = "NpfCorrespLinkedItem", ModalWidth = 700, Model = "WebNpfCorrespLinkedItemViewModel", ModelParams = new [] { "id", "parentId"}, Grids = new List<string> { "NpfCorrespOkipList", "NpfCorrespControlOkipList", "NpfCorrespArchiveOkipList", "CorrespItemLinksGridView" } });
            Data.Add("NpfSpnSelectRegister", new NameEntity { DisplayName = "Выбор реестра", Id = "NpfSpnSelectRegister", ModalWidth = 700, Model = "WebNpfSpnSelectRegisterViewModel", DialogMode = "DialogNoValidation" });
        }

        public static NameEntity GetDataByModel(string model)
        {
            return Data.Values.FirstOrDefault(a =>a.Model == model);
        }

        public static object[] GetConstructorParams(string formid, dynamic[] input)
        {
            var data = Data[formid];
            var prms = new List<object>();
            data.ModelParams.ForEach(p=>
            {
                var found = input.FirstOrDefault(a => a.Name == p);
                if(found != null)
                    prms.Add(found.Value);
            });
            return prms.ToArray();
        }
    }
}