﻿using System.Linq;
using PFR_INVEST.Web2.DataObjects.Web;
using System.Data.Entity;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class ViewModelHelperBO
    {
        public static bool IsClosedPP(AsgFinTrWeb pp)
        {
            var isClosed = false;
            if (pp.ReqTransferID.HasValue)
            {
                //TODO
               /* var rt = DataContainerFacade.GetByID<ReqTransfer>(pp.ReqTransferID.Value);
                var sumPP = rt.GetCommonPPList().Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount);
                isClosed = sumPP == rt.Sum && (pp.DirectionElID != (long)AsgFinTr.Directions.FromPFR);*/
            }
            else if (pp.FinregisterID.HasValue)
            {
                var fr = WCFClient.GetEFServiceFunc().SpnFinregisters.Include(t=> t.AsgFinTrs).FirstOrDefault(a => a.ID == pp.FinregisterID.Value);
                var sumPP = fr.AsgFinTrs.Where(p => p.ID != pp.ID).Sum(p => p.DraftAmount);
                isClosed = sumPP == fr.Count && (pp.DirectionElID != (long) AsgFinTrWeb.Directions.FromPFR);
            }
            else
            {
                //TODO
             /*   var trId = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("PPID", pp.ID, true).FirstOrDefault()?.TransferID;
                if (trId.HasValue)
                {
                    var rt = DataContainerFacade.GetByID<OpfrTransfer>(trId);
                    var ppIdList = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("TransferID", trId, true).Where(p => p.PPID != pp.ID).Select(a => a.PPID);
                    var sumPP = DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(ListPropertyCondition.In("ID", ppIdList.Cast<object>().ToArray())).Sum(a => a.DraftAmount) ?? 0;
                    isClosed = sumPP == rt.Sum;
                }*/
            }

            return isClosed;
        }
    }
}