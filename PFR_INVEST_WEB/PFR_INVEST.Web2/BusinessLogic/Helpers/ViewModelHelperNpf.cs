﻿using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class ViewModelHelperNpf
    {
        public static bool IsFinregisterStatusReq(RegistersListItem item)
        {
            return (RegisterIdentifier.IsToNPF(item.RegisterKind) && RegisterIdentifier.FinregisterStatuses.IsCreated(item.Status)
                                                                  && item.DraftSum == item.Count && item.TrancheDate != null) ||
                   (RegisterIdentifier.IsFromNPF(item.RegisterKind) && RegisterIdentifier.FinregisterStatuses.IsCreated(item.Status));
        }

        public static bool IsFinregisterCanPrintLetter(RegistersListItem item)
        {
            return item.Status != null &&
                   (RegisterIdentifier.FinregisterStatuses.IsIssued(item.Status .ToLower()) ||
                    RegisterIdentifier.FinregisterStatuses.IsTransferred(item.Status .ToLower()) ||
                    RegisterIdentifier.FinregisterStatuses.IsRrefine(item.Status .ToLower()));
        }

        public static bool IsFinregisterStatusRefine(RegistersListItem item)
        {
            return RegisterIdentifier.IsFromNPF(item.RegisterKind)
                   && RegisterIdentifier.FinregisterStatuses.IsTransferred(item.Status) && !item.IsCustomArchive;
        }

        public static void RollbackFinregisterStatus(long id)
        {
            var fr = DbHelper.GetUncached<FinregisterWeb>(id);
            if(fr == null) return;

            var oldstatus = fr.Status;
            var r = fr.Register;
            var ppList = fr.AsgFinTrs.ToList();
             if (RegisterIdentifier.IsToNPF(r.Kind))
            {
                if (RegisterIdentifier.FinregisterStatuses.IsCreated(fr.Status))
                {
                    //начальный статус - не должны были сюда попасть
                    if (r.TrancheDate == null) return;
                    //если есть пп - сбрасываем их связи (возврат в Работу с НПФ)
                    if (ppList.Count > 0)
                    {
                        ppList.ForEach(a => a.ClearLink());
                        DbHelper.SaveChanges();
                    }
                    else return;
                }
                //если статус Оформлен -> сбрасываем на Создан
                else if (RegisterIdentifier.FinregisterStatuses.IsIssued(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Created;
                    DbHelper.SaveChanges();
                }
                    //передан -> оформлен
                else if (RegisterIdentifier.FinregisterStatuses.IsTransferred(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Issued;
                    fr.FinNum = null;
                    fr.FinMoveType = null;
                    fr.FinDate = null;
                    fr.IsTransfered = 0;
                    DbHelper.SaveChanges();
                } 
            }
            else
            {
                //начальный статус - не должны были сюда попасть
                if (RegisterIdentifier.FinregisterStatuses.IsCreated(fr.Status)) return;

                //утчонение -> передан
                if (RegisterIdentifier.FinregisterStatuses.IsRrefine(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Transferred;
                    fr.IsTransfered = 1;
                    DbHelper.SaveChanges();
                }
                    //финреестр оформлен
                else if (RegisterIdentifier.FinregisterStatuses.IsIssued(fr.Status))
                {
                    //возврат в самое начало
                    if (ppList.Count == 0)
                    {
                        fr.Status = RegisterIdentifier.FinregisterStatuses.Created;
                    }
                    else
                    {  
                        //очищаем п/п (возврат в Работу с НПФ)
                        ppList.ForEach(a=> a.ClearLink());
                    }
                    DbHelper.SaveChanges();
                }
                    //передан -> оформлен
                else if (RegisterIdentifier.FinregisterStatuses.IsTransferred(fr.Status))
                {
                    fr.Status = RegisterIdentifier.FinregisterStatuses.Issued;
                    fr.FinNum = null;
                    fr.FinMoveType = null;
                    fr.FinDate = null;
                    fr.IsTransfered = 0;
                    DbHelper.SaveChanges();
                }
            }
            JournalLogger.LogEvent(oldstatus == fr.Status ? "Сброс даты формирования заявки" : string.Format("{1} <- {0}", oldstatus, fr.Status), JournalEventType.ROLLBACK, fr.ID, "FinregisterViewModel", "", typeof(Finregister), "Финреестр");
        }

        public static bool IsFinregisterStatusGive(RegistersListItem en)
        {
            return RegisterIdentifier.FinregisterStatuses.IsIssued(en.Status) && en.DraftSum == en.Count;
        }

        public static bool IsFinregisterStatusGive(FinregisterWeb en)
        {
            return RegisterIdentifier.FinregisterStatuses.IsIssued(en.Status) && en.AsgFinTrs.Sum(a=> a.DraftAmount ?? 0) == en.Count;
        }

        public static bool IsFinregisterStatusRollback(RegistersListItem item)
        {
            var isPureViewer = AP.Provider.CurrentUserSecurity.IsPureViewer();
            return !item.IsCustomArchive && !isPureViewer && !string.IsNullOrEmpty(item.Status) && item.Status != RegisterIdentifier.FinregisterStatuses.NotTransferred &&
                   (!RegisterIdentifier.IsToNPF(item.RegisterKind) || item.TrancheDate != null) &&
                   (item.DraftSum != null || item.Status != RegisterIdentifier.FinregisterStatuses.Created || !RegisterIdentifier.IsToNPF(item.RegisterKind));
        }

        public static bool IsFinregisterStatusFixed(RegistersListItem item)
        {
            return RegisterIdentifier.IsFromNPF(item.RegisterKind)
                   && RegisterIdentifier.FinregisterStatuses.IsRrefine(item.Status);
        }

        public static bool IsFinregisterStatusNotTr(RegistersListItem item)
        {
            if (!RegisterIdentifier.IsToNPF(item.RegisterKind) || item.Count == 0 || item.ParentFinregisterID > 0) return false;
            return RegisterIdentifier.FinregisterStatuses.IsCreated(item.Status) || RegisterIdentifier.FinregisterStatuses.IsIssued(item.Status);
        }

        public static bool IsSelectPPAvailable(RegistersListItem item)
        {
            return RegisterIdentifier.IsFromNPF(item.RegisterKind)
                   && RegisterIdentifier.FinregisterStatuses.IsIssued(item.Status)
                   && item.Count != item.DraftSum;
        }
    }
}