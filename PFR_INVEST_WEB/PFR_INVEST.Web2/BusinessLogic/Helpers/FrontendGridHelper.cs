﻿using DevExpress.Web.Mvc;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;
using RestSharp.Serializers;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class FrontendGridHelper
    {
        #region Data
        private class SpnRegisterGridCallbackData
        {
            public long register_id;
            public bool can_create_order;
            public bool can_print_letter;
            public long fr_id;
            public bool status_req;
            public bool status_give;
            public bool status_refine;
            public bool status_fixed;
            public bool status_nottr;
            public bool status_rollback;
            public bool pp_select;
        }

        private class TempAllocGridCallbackData
        {
            public long register_id;
            public long fr_id;
            public bool is_return;
        }
        #endregion

        public static void SubscribeForCorrespActiveGridCustomCallback(object grid)
        {
            ((MVCxGridView) grid).CustomDataCallback += (s1, e1) =>
            {
                var g = (MVCxGridView) s1;
                int groupRowVisibleIndex = int.Parse(e1.Parameters);
                var result = new TempAllocGridCallbackData();
                if (!g.IsGroupRow(groupRowVisibleIndex))
                {
                    var item = (CorrespondenceListItemWeb)g.GetRow(groupRowVisibleIndex);
                    if (item != null)
                    {
                        result.fr_id = item.IsAttach ? item.ID : 0;
                        result.register_id = !item.IsAttach ? item.ID : 0;
                    }
                    e1.Result = new JsonSerializer().Serialize(result);
                } 
            };
        }

        public static void SubscribeForTempAllocGridCustomCallback(object grid)
        {
            ((MVCxGridView) grid).CustomDataCallback += (s1, e1) =>
            {
                var g = (MVCxGridView) s1;
                int groupRowVisibleIndex = int.Parse(e1.Parameters);
                var result = new TempAllocGridCallbackData();
                if (!g.IsGroupRow(groupRowVisibleIndex))
                {
                    var item = (RegistersListItem)g.GetRow(groupRowVisibleIndex);
                    if (item != null)
                    {
                        result.fr_id = item.ID;
                    }
                    e1.Result = new JsonSerializer().Serialize(result);
                    return;
                }
                var gCols = g.GetGroupedColumns();
                var groupsCount = gCols.Count;
                var lvl = g.GetRowLevel(groupRowVisibleIndex);
                if (lvl < groupsCount && gCols[lvl].FieldName == "RegisterID")
                {
                    var count = g.GetChildRowCount(groupRowVisibleIndex);
                    if (count > 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            var en = (RegistersListItem) g.GetChildRow(groupRowVisibleIndex, i);
                        }

                        var reg = (RegistersListItem) g.GetChildRow(groupRowVisibleIndex, 0);
                        if (reg != null)
                            result.register_id = (reg).RegisterID;

                        result.is_return = RegisterIdentifier.IsTempReturn(reg?.RegisterKind);
                        e1.Result = new JsonSerializer().Serialize(result);
                    }
                    else
                    {
                        var item = g.GetChildRow(groupRowVisibleIndex, 0) as RegistersListItem;
                        result.register_id = item?.RegisterID ?? 0;
                        result.is_return = RegisterIdentifier.IsTempReturn(item?.RegisterKind);
                        e1.Result = new JsonSerializer().Serialize(result);
                    }
                }
            };
        }

        public static void SubscribeForSpnGridCustomCallback(object grid)
        {
            ((MVCxGridView)grid).CustomDataCallback += (s1, e1) => {
                var g = (MVCxGridView)s1;
                var result = new SpnRegisterGridCallbackData();
                int groupRowVisibleIndex = int.Parse(e1.Parameters);
                if(!g.IsGroupRow(groupRowVisibleIndex))
                {
                    result.register_id = 0;
                    result.fr_id = 0;
                    result.can_create_order = false;
                    result.status_rollback = false;
                    result.status_give = false;

                    var item = (RegistersListItem)g.GetRow(groupRowVisibleIndex);
                    if (item != null)
                    {
                        result.can_print_letter = ViewModelHelperNpf.IsFinregisterCanPrintLetter(item);
                        result.status_req = ViewModelHelperNpf.IsFinregisterStatusReq(item);
                        result.fr_id = item.ID;
                        result.status_rollback = ViewModelHelperNpf.IsFinregisterStatusRollback(item);
                        result.status_give = ViewModelHelperNpf.IsFinregisterStatusGive(item);
                        result.status_refine = ViewModelHelperNpf.IsFinregisterStatusRefine(item);
                        result.status_fixed = ViewModelHelperNpf.IsFinregisterStatusFixed(item);
                        result.status_nottr = ViewModelHelperNpf.IsFinregisterStatusNotTr(item);
                        result.pp_select = ViewModelHelperNpf.IsSelectPPAvailable(item);
                    }
                    e1.Result = new JsonSerializer().Serialize(result);
                    return;
                }
                var gCols = g.GetGroupedColumns();
                var groupsCount = gCols.Count;
                var lvl = g.GetRowLevel(groupRowVisibleIndex);
                if(lvl < groupsCount && gCols[lvl].FieldName == "RegisterID")
                {
                    var count = g.GetChildRowCount(groupRowVisibleIndex);
                    if (count > 0)
                    {
                        bool allIsNotTransferred = count != 0;
                        bool allSums = count != 0;
                        long registerId = 0;
                        string regKind = null;
                        for (int i = 0; i < count; i++)
                        {
                            var en = (RegistersListItem) g.GetChildRow(groupRowVisibleIndex, i);
                            if (en == null) continue;
                            allIsNotTransferred = allIsNotTransferred && RegisterIdentifier.FinregisterStatuses.IsNotTransferred(en.Status);
                            allSums = allSums && en.Count > 0;
                            result.status_req = result.status_req || ViewModelHelperNpf.IsFinregisterStatusReq(en);
                            if (i == count - 1)
                            {
                                registerId = en.RegisterID;
                                regKind = en.RegisterKind;
                            }
                            result.status_give = result.status_give || ViewModelHelperNpf.IsFinregisterStatusGive(en);
                        }

                        result.register_id = registerId;
                        result.can_create_order = RegisterIdentifier.IsToNPF(regKind);
                        result.can_print_letter = false;
                        e1.Result = new JsonSerializer().Serialize(result);
                    }
                    else
                    {
                        var item = g.GetChildRow(groupRowVisibleIndex, 0) as RegistersListItem;
                        result.register_id = item?.RegisterID ?? 0;
                        e1.Result = new JsonSerializer().Serialize(result);
                    }
                }
            };
        }
    }
}