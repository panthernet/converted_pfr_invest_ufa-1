﻿using db2connector;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.Web2.BusinessLogic.From
{
    public class WebDataServiceClient : System.ServiceModel.ClientBase<IDB2Connector>
    {
        public static string EndPointConfigName = "WSHttpBinding_IDB2Connector";
        public static string HttpsEndPointConfigName = "WSHttpsBinding_IDB2Connector";

        internal WebDataServiceClient()
        {

        }

        internal WebDataServiceClient(string endpointConfigurationName)
            : base(endpointConfigurationName)
        {

        }

        public IDB2Connector GetChannel()
        {
            return base.Channel;
        }

        public static WebDataServiceClient CreateNew(ClientAuthType authType)
        {
            string name;
            switch (authType)
            {
                case ClientAuthType.ECASAHTTPS:
                    name = HttpsEndPointConfigName;
                    break;
                default:
                    name = EndPointConfigName;
                    break;
            }

            return new WebDataServiceClient(name);
        }
    }
}