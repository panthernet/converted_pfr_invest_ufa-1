﻿using System.Linq;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfTempAllocListViewModel : WebNpfLazyListViewModel<RegistersListItem>
    {
        private IQueryable _query;

        public override IQueryable Query => _query ?? (_query = WCFClient.GetEFServiceFunc()?.GetTempAllocationList());

        public WebNpfTempAllocListViewModel()
        {
            AjaxMain = "_TempAllocationListViewPartial";
            AjaxPartial = "_TempAllocationListContainerViewPartial";
            AttachDefaultGridScripts = false;
        }
    }
}