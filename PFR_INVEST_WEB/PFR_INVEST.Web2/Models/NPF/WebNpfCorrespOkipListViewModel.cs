﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;

namespace PFR_INVEST.Web2.Models
{

    [ModelEntity(Type = typeof(DocumentWeb), JournalDescription = "Корреспонденция ОКИП")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfActiveCorrespNpfListViewModel : WebNpfLazyListViewModel<CorrespondenceListItemWeb>
    {
        public WebNpfActiveCorrespNpfListViewModel() : base("Npf")
        {
            Query = WCFClient.GetEFServiceFunc().GetNpfDocumentsListHib(DocumentWeb.Statuses.Active);
            AjaxMain = "_CorrespondenceActiveListViewPartial";
            AjaxPartial = "_CorrespondenceActiveListContainerViewPartial";
            AttachDefaultGridScripts = true;
        }
    }

    [ModelEntity(Type = typeof(DocumentWeb), JournalDescription = "Корреспонденция ОКИП")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfControlCorrespNpfListViewModel : WebNpfLazyListViewModel<CorrespondenceListItemWeb>
    {
        public WebNpfControlCorrespNpfListViewModel() : base("Npf")
        {
            Query = WCFClient.GetEFServiceFunc().GetNpfDocumentsListHib(DocumentWeb.Statuses.Control);
            AjaxMain = "_CorrespondenceControlListViewPartial";
            AjaxPartial = "_CorrespondenceControlListContainerViewPartial";
            AttachDefaultGridScripts = true;
        }
    }

    [ModelEntity(Type = typeof(DocumentWeb), JournalDescription = "Корреспонденция ОКИП")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfArchiveCorrespNpfListViewModel : WebNpfLazyListViewModel<CorrespondenceListItemWeb>
    {
        public WebNpfArchiveCorrespNpfListViewModel() : base("Npf")
        {
            Query = WCFClient.GetEFServiceFunc().GetNpfDocumentsListHib(DocumentWeb.Statuses.Executed);
            AjaxMain = "_CorrespondenceArchiveListViewPartial";
            AjaxPartial = "_CorrespondenceArchiveListContainerViewPartial";
            AttachDefaultGridScripts = true;
        }
    }

}