﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(ERZLNotify), JournalDescription = "Уведомление ЕРЗЛ")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    public class WebNpfRedistZlNoticeViewModel : PureWebFormViewModel
    {
        [Required]
        [DisplayName("Наименование НПФ")]
        public long SelectedNPFID { get; set; }

        [DisplayName("ИНН НПФ")]
        public string INN {get; private set; }

        [DisplayName("Кампания по приему заявлений")]
        public string WebCompany => _erzl?.Company;

        [DisplayName("Содержание операции")]
        public string Content => _erzl.Content;

        [DisplayName("Дата")]
        public DateTime? WebNoticeDate => _erzl.Date;

        [Required]
        [DisplayName("Номер уведомления ЕРЗЛ")]
        public string NotifyNum { get; set; }

        [DisplayName("Возврат из НПФ в ПФР")]
        public long? NPF2PFRCount { get; set; } = 0;

        [DisplayName("Передача из НПФ в другие НПФ")]
        public long? NPF2OTHERCount { get; set; } = 0;

        [DisplayName("Передача в НПФ из ПФР")]
        public long? PFR2NPFCount { get; set; } = 0;

        [DisplayName("Передача в НПФ из других НПФ")]
        public long? OTHER2NPFCount { get; set; } = 0;

        [JsonIgnore]
        public bool Is75FZBool { get; private set; }
        [JsonIgnore]
        private ERZL _erzl;
        [JsonIgnore]
        private ERZLNotify _erzlNotify;
        private long SelectedNPFZLAccountID { get; set; }
        private long SelectedNPFContragentID { get; set; }
        [JsonIgnore]
        public IList<LegalEntity> NPFList { get; set; } = new List<LegalEntity>();

        public WebNpfRedistZlNoticeViewModel():this(0, 0)
        {
        }

        public WebNpfRedistZlNoticeViewModel(long id, long parentId) : base(id)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_RzlNoticeItemViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            Init(id, parentId, State);
        }


        [JsonIgnore]
        public List<ERZLNotify> ERZLNotifiesList { get; set; }

        [JsonIgnore]
        public ERZLNotify NPF2PFR_Notify { get; set; }

        [JsonIgnore]
        public ERZLNotify PFR2NPF_Notify { get; set; }

        [JsonIgnore]
        public List<NPFforERZLNotifyListItem> ContragentsListNPF2Other { get; set; }

        [JsonIgnore]
        public List<NPFforERZLNotifyListItem> ContragentsList_OTHER2NPF { get; set; }

       

        public void RefreshNPFList()
        {
            try
            {
                NPFList = WCFClient.GetDataServiceFunc().GetNPFListLEAccHib(new StatusFilter
                { new FilterOP { BOP = BOP.Or, OP = OP.EQUAL, Identifier = StatusIdentifier.S_ACTIVE }
                });
            }
            catch
            {
                NPFList = new List<LegalEntity>();
            }
        }

        public void RefreshERZLNotifiesList()
        {
            try
            {
                ERZLNotifiesList = new List<ERZLNotify>(WCFClient.GetDataServiceFunc().GetERZLNotifiesHib(_erzl.ID, NotifyNum));
            }
            catch
            {
                ERZLNotifiesList = new List<ERZLNotify>();
            }
        }

        public void RefreshContragentsListsAndCounts(bool firsttime)
        {
            if (firsttime)
            {
                PFR2NPFCount = 0;
                NPF2PFRCount = 0;
            }

            NPF2OTHERCount = 0;
            OTHER2NPFCount = 0;

            try
            {
                ContragentsListNPF2Other = new List<NPFforERZLNotifyListItem>(
                    WCFClient.GetDataServiceFunc().GetNPF2NPFListHib(_erzl.ID, NotifyNum, SelectedNPFZLAccountID, true));


                ContragentsList_OTHER2NPF = new List<NPFforERZLNotifyListItem>(
                    WCFClient.GetDataServiceFunc().GetNPF2NPFListHib(_erzl.ID, NotifyNum, SelectedNPFZLAccountID, false));


                if (ERZLNotifiesList != null && ERZLNotifiesList.Count > 0)
                {
                    foreach (var notify in ERZLNotifiesList)
                    {
                        if (notify.MainNPF == "0")
                        {
                            if (notify.GetDebitAccount().GetContragent().TypeName.ToLower().Trim() == "нпф")
                            {
                                NPF2OTHERCount += notify.Count ?? 0;
                                var caLI = ContragentsListNPF2Other.Find(cali => cali.Contragent.ID == notify.GetDebitAccount().GetContragent().ID);
                                if (caLI != null)
                                {
                                    caLI.Count = notify.Count;
                                    caLI.ERZLNotify = notify;
                                }

                            }
                            else
                            {
                                if (firsttime)
                                {
                                    NPF2PFRCount += notify.Count ?? 0;
                                    NPF2PFR_Notify = notify.Count != null ? notify : null;
                                }
                            }
                        }
                        else
                        {
                            if (notify.GetCreditAccount().GetContragent().TypeName.ToLower().Trim() == "нпф")
                            {
                                OTHER2NPFCount += notify.Count ?? 0;
                                var caLI = ContragentsList_OTHER2NPF.Find(cali => cali.Contragent.ID == notify.GetCreditAccount().GetContragent().ID);
                                if (caLI != null)
                                {
                                    caLI.Count = notify.Count;
                                    caLI.ERZLNotify = notify;
                                }

                            }
                            else
                            {
                                if (firsttime)
                                {
                                    PFR2NPFCount += notify.Count ?? 0;
                                    PFR2NPF_Notify = notify.Count != null ? notify : null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            ContragentsListNPF2Other = ContragentsListNPF2Other?.FindAll(f => StatusIdentifier.IsNPFStatusActivityCarries(f.Contragent.GetStatus().Name) || (f.Count != null && f.Count.Value > 0));
            ContragentsList_OTHER2NPF = ContragentsList_OTHER2NPF?.FindAll(f => StatusIdentifier.IsNPFStatusActivityCarries(f.Contragent.GetStatus().Name) || (f.Count != null && f.Count.Value > 0));
        }

        public static List<ERZLNotifiesListItem> GetNotices(long id)
        {
            return WCFClient.GetDataServiceFunc().GetERZLNotifiesListByERZL_ID(id);
        }

        private void Init(long id, long parentId, ViewModelState action)
        {
            RefreshNPFList();

            _erzl = DataContainerFacade.GetByID<ERZL, long>(parentId);
            _erzlNotify = id > 0 ? DataContainerFacade.GetByID<ERZLNotify>(id) : new ERZLNotify {ERZL_ID = parentId};

            Is75FZBool = DataContainerFacade.GetByID<FZ>(_erzl.FZ_ID)?.Name.Contains("75") ?? false;
            NotifyNum = _erzlNotify.NotifyNum;
            // фильтруем список НПФ, чтобы нельзя было добавить уведомление на одного и того же НПФ дважды
            var nList = GetNotices(parentId);
            var cList = nList.Select(a => a.ContragentID).ToList();
            NPFList = NPFList.Where(a => !cList.Contains(a.ContragentID ?? 0)).ToList();
            RefreshERZLNotifiesList();
            if(id > 0)
            { 
                var accountId = _erzlNotify.MainNPF == "0" ? _erzlNotify.CrAccID : _erzlNotify.DbtAccID;
                var account = DataContainerFacade.GetByID<Account>(accountId);
                SelectNpf(account.ContragentID ?? 0, false);
            }
            else
            {
                SelectNpf(NPFList.FirstOrDefault()?.ContragentID ?? 0, false);
            }

            // если по каким то причинам НПФ не попал в список (был реарганизован), то тащим напрямик из базы
            if (SelectedNPFID >0 && NPFList.FirstOrDefault(a=> a.ID == SelectedNPFID) == null)
            {
                NPFList.Add(DataContainerFacade.GetListByProperty<LegalEntity>("ContragentID", SelectedNPFID).FirstOrDefault());
            }

            RefreshContragentsListsAndCounts(true);
            if (ContragentsListNPF2Other != null)
                RecalculateNPF2OTHER();
            if (ContragentsList_OTHER2NPF != null)
                RecalculateOther2NPF();
        }

#region Execute Implementations
        private void ExecuteAddNPF2OTHER()
        {
            if (ContragentsListNPF2Other == null)
            {
                ////////////DialogHelper.ShowAlert("Список контрагентов пуст.");
                return;
            }

           /* var result = DialogHelper.AddNPFToOther(ContragentsListNPF2Other.Select(item => item.Clone()).ToList(), SelectedNPF.ContragentID.Value);

            if (result != null)
            {
                ContragentsListNPF2Other = result;
                RecalculateNPF2OTHER();
            }*/
        }

        public void RecalculateNPF2OTHER()
        {
            ERZLNotifiesList.RemoveAll(n => n.MainNPF.Equals("0") && n.ID == 0);

            NPF2OTHERCount = 0;
            foreach (var item in ContragentsListNPF2Other)
            {
                if (item.ERZLNotify != null)
                {
                    var notify = ERZLNotifiesList.Find(not => not.ID == item.ERZLNotify.ID);

                    if (notify != null)
                        notify.Count = item.Count == 0 ? null : item.Count;
                    else
                    {
                        item.ERZLNotify.Count = item.Count == 0 ? null : item.Count;
                        ERZLNotifiesList.Add(item.ERZLNotify);
                    }
                }
                else if (item.Count != null)
                {
                    if (SelectedNPFID > 0)
                    {
                        try
                        {
                            ERZLNotifiesList.Add(new ERZLNotify
                            {
                                ERZL_ID = _erzl.ID,
                                NotifyNum = NotifyNum,
                                Count = item.Count,
                                CrAccID = SelectedNPFZLAccountID,
                                DbtAccID = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(item.Contragent.ID, "зл").ID,
                                MainNPF = "0"
                            });
                        }
                        catch
                        {

                        }
                    }
                }

                if (item.Count != null)
                    NPF2OTHERCount += item.Count;
            }
        }

        private void ExecuteAddOTHER2NPF()
        {
            if (ContragentsList_OTHER2NPF == null)
            {
                /////////////DialogHelper.ShowAlert("Список контрагентов пуст.");
                return;
            }

           /* var result = DialogHelper.AddOther2NPF(ContragentsList_OTHER2NPF.Select(item => item.Clone()).ToList(), SelectedNPF.ContragentID.Value);

            if (result != null)
            {
                ContragentsList_OTHER2NPF = result;
                RecalculateOther2NPF();
            }*/
        }

        void RecalculateOther2NPF()
        {
            ERZLNotifiesList.RemoveAll(n => n.MainNPF.Equals("1") && n.ID == 0);

            OTHER2NPFCount = 0;
            foreach (var item in ContragentsList_OTHER2NPF)
            {
                if (item.ERZLNotify != null)
                {
                    var notify = ERZLNotifiesList.Find(not => not.ID != 0 && not.ID == item.ERZLNotify.ID);

                    if (notify != null)
                        notify.Count = item.Count == 0 ? null : item.Count;
                    else
                    {
                        item.ERZLNotify.Count = item.Count == 0 ? null : item.Count;
                        ERZLNotifiesList.Add(item.ERZLNotify);
                    }

                }
                else if (item.Count != null)
                {
                    if (SelectedNPFID > 0)
                    {
                        try
                        {
                            ERZLNotifiesList.Add(new ERZLNotify
                            {
                                ERZL_ID = _erzl.ID,
                                NotifyNum = NotifyNum,
                                Count = item.Count,
                                CrAccID = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(item.Contragent.ID, "зл").ID,
                                DbtAccID = SelectedNPFZLAccountID,
                                MainNPF = "1"
                            });
                        }
                        catch
                        {

                        }
                    }
                }

                if (item.Count != null)
                    OTHER2NPFCount += item.Count;
            }
        }


        public void SelectNpf(long id, bool isLe)
        {
            var le = isLe? DataContainerFacade.GetByID<LegalEntity>(id) : DataContainerFacade.GetListByPropertyCondition<LegalEntity>("ContragentID", id, "eq").FirstOrDefault();
            SelectedNPFID = le.ID;
            SelectedNPFZLAccountID = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(le.ContragentID ?? 0, "зл")?.ID ?? 0;
            SelectedNPFContragentID = le.ContragentID ?? 0;
            INN = le.INN;
        }

        public override bool WebExecuteDelete(long id)
        {

            return true;
        }

        public override bool WebExecuteSave()
        {
            if (ERZLNotifiesList == null) return true;

            ERZLNotify notifyToAdd = null;
            if (NPF2PFR_Notify != null)
            {
                notifyToAdd = ERZLNotifiesList.Find(not => not.ID == NPF2PFR_Notify.ID);
            }
            if (notifyToAdd != null)
                notifyToAdd.Count = NPF2PFRCount == 0 ? null : NPF2PFRCount;
            else
                ERZLNotifiesList.Add(new ERZLNotify
                {
                    ERZL_ID = _erzl.ID,
                    NotifyNum = NotifyNum,
                    Count = NPF2PFRCount == 0 ? null : NPF2PFRCount,
                    CrAccID = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(SelectedNPFContragentID, "зл").ID,
                    DbtAccID = WCFClient.GetDataServiceFunc().GetPFRAccountByMeasure("зл").ID,
                    MainNPF = "0"
                });

            notifyToAdd = null;
            if (PFR2NPF_Notify != null)
            {
                notifyToAdd = ERZLNotifiesList.Find(not => not.ID == PFR2NPF_Notify.ID);
            }
            if (notifyToAdd != null)
                notifyToAdd.Count = PFR2NPFCount == 0 ? null : PFR2NPFCount;
            else
                ERZLNotifiesList.Add(new ERZLNotify
                {
                    ERZL_ID = _erzl.ID,
                    NotifyNum = NotifyNum,
                    Count = PFR2NPFCount == 0 ? null : PFR2NPFCount,
                    CrAccID = WCFClient.GetDataServiceFunc().GetPFRAccountByMeasure("зл").ID,
                    DbtAccID = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(SelectedNPFContragentID, "зл").ID,
                    MainNPF = "1"
                });

            var addList = new List<ERZLNotify>();
            var delList = new List<ERZLNotify>();

            foreach (var notify in ERZLNotifiesList)
            {
                if (notify.Count != null && notify.Count.Value != 0)
                {
                    notify.ERZL_ID = _erzl.ID;
                    if (notify.MainNPF == "0")
                        notify.CrAccID = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(SelectedNPFContragentID, "зл").ID;
                    else
                        notify.DbtAccID = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(SelectedNPFContragentID, "зл").ID;
                    notify.NotifyNum = NotifyNum;
                    addList.Add(notify);
                }
                else if (notify.ID > 0)
                    delList.Add(notify);
            }
            if (!WCFClient.GetDataServiceFunc().SaveERZLNotifiesList(addList) || !WCFClient.GetDataServiceFunc().EraseERZLNotifiesList(delList))
                throw new Exception("При обновлении/сохранении введённых значений произошла ошибка.");

            return true;
        }
#endregion

        public override string Validate()
        {
            const string fieldNames =
               "SelectedNPF|NotifyNum";
            return fieldNames.Split("|".ToCharArray()).Select(item => this[item]).FirstOrDefault(res => !string.IsNullOrEmpty(res));
        }

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";                
                switch (columnName)
                {
                    case "SelectedNPF": return SelectedNPFID == 0 ? errorMessage : null;
                    case "NotifyNum": return string.IsNullOrWhiteSpace(NotifyNum) ? errorMessage : NotifyNum.ValidateMaxLength(50);
                    default: return null;
                }
            }
        }
    }
}