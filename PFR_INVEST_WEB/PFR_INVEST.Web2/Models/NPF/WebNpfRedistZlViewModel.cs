﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using JournalLogger = PFR_INVEST.BusinessLogic.Journal.JournalLogger;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(ERZL), JournalDescription = "ЕРЗЛ")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfRedistZlViewModel : PureWebFormViewModel
    {

        [Required]
        [DisplayName("Содержание операции")]
        public long FZ_ID
        {
            get { return FzId; }
            set { FzId = value; }
        }

        [Required]
        [DisplayName("Дата")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? WebDate
        {
            get
            {
                return _erzl?.Date;
            }

            set
            {
                if (_erzl == null) return;
                _erzl.Date = value;
            }
        }

        public WebNpfRedistZlViewModel():this(0)
        {
        }

        public WebNpfRedistZlViewModel(long id): base(id)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_RzlItemViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = true;
            RefreshFZList();
            if (id > 0)
                LoadZLRedist(id);
            else _erzl = new ERZL();
            CustomDeleteMessage = "При удалении Перераспределения ЕРЗЛ будут так же удалены сопутствующие Реестры с Финреестрами. Продолжить?";
            WebDate = DateTime.Now;
        }

        #region OLD
        protected long FzId;

        public static List<ERZLNotifiesListItem> GetNotices(long id)
        {
            return WCFClient.GetDataServiceFunc().GetERZLNotifiesListByERZL_ID(id);
        }
        [JsonIgnore]
        private ERZL _erzl;

        #region Company

        [Required]
        [DisplayName("Кампания")]
        [StringLength(64)]
        public string Company
        {
            get {
                return _erzl != null ? _erzl.Company : "";
            }

            set
            {
                if (_erzl == null) return;
                _erzl.Company = value;
            }
        }
        #endregion

        #region NotifiesCount
        [DisplayName("Количество уведомлений ЕРЗЛ")]
        public int NotifiesCount => NotifiesList?.Count ?? 0;

        #endregion

        #region FocusedERZLNotify
        private ERZLNotifiesListItem _focusedERZLNotify;
        public ERZLNotifiesListItem FocusedERZLNotify
        {
            get
            {
                return _focusedERZLNotify;
            }

            set
            {
                var isdatachanged = IsDataChanged;
                _focusedERZLNotify = value;
                IsDataChanged = isdatachanged;
            }
        }
        #endregion

        #region NotifiesList

        [JsonIgnore]
        public List<ERZLNotifiesListItem> NotifiesList { get; set; } = new List<ERZLNotifiesListItem>();

        #endregion

        #region SelectedFZ
        private FZ _selectedFz;
        public FZ SelectedFZ
        {
            get
            {
                return _selectedFz;
            }

            set
            {
                _selectedFz = value;
                if (_erzl != null && _selectedFz != null)
                    _erzl.FZ_ID = _selectedFz.ID;
            }
        }
        #endregion

        #region FZList

        [JsonIgnore]
        public List<FZ> FZList { get; set; }

        #endregion


        #region Execute Implementations

        public override bool WebExecuteSave()
        {

            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    _erzl.Content = _erzl.Company != null ? $"Перераспределение СПН (кампания {_erzl.Company})" : "";
                    _erzl.FZ_ID = FzId;
                    if (State == ViewModelState.Edit)
                    {
                        DataContainerFacade.Save<ERZL, long>(_erzl);
                    }
                    else
                    {
                        _erzl.ID = DataContainerFacade.Save<ERZL, long>(_erzl);
                        ID = _erzl.ID;
                    }
                    break;
                default:
                    break;
            }

            return true;
        }

        private bool CanExecuteAddNotify()
        {
            return _erzl != null && _erzl.ID > 0 && !EditAccessDenied;
        }

        private bool CanExecuteDeleteNotify()
        {
            return _focusedERZLNotify != null && !EditAccessDenied;
        }

        private void ExecuteDeleteNotify()
        {
            var list = WCFClient.GetDataServiceFunc().GetERZLNotifiesHib(_erzl.ID, _focusedERZLNotify.NotifyNum);
            if (list != null)
            {
                var deletedItems = new StringBuilder();
                foreach (var obj in list)
                {
                    var item = WCFClient.GetDataServiceFunc().GetERZLNotifyHib(obj.ID);
                    deletedItems.Append(item.ID).Append(", ");
                    DataContainerFacade.Delete(item);                    
                }
                JournalLogger.LogEvent("Перераспределения ЗЛ", JournalEventType.DELETE, ID, "Удаление НПФ",
                    $"НПФ Id: {_erzl.ID}. Записи ERZLNotify Ids: {deletedItems}");
                RefreshNotifiesList();
            }
            _focusedERZLNotify = null;
        }

        public override bool WebExecuteDelete(long id)
        {
            var list = WCFClient.GetDataServiceFunc().GetERZLNotifiesHib(_erzl.ID, null);
            foreach (var obj in list)
                DataContainerFacade.Delete(obj);

            var register = DataContainerFacade.GetListByProperty<Register>("ERZL_ID", _erzl.ID);
            foreach (var obj in register)
            {
                var fin = obj.GetFinregistersList();
                foreach (var item in fin)
                {
					foreach (var finTr in DataContainerFacade.GetListByProperty<AsgFinTr>("FinregisterID", item.ID))
					{
						//Отмена коммента - сделали маркируемым!
                        //Удаляем связь на финреестр, так как сущность ПП не удаляется, а маркируется удалённой
						//А финреестр удаляется физически
						//finTr.FinregisterID = null; 
						DataContainerFacade.Delete(finTr);
					}
                    DataContainerFacade.Delete(item);
                }
                //Удаляем связь реестра с ERZL т.к. оно еще не архивируется
                obj.ERZL_ID = null;
                DataContainerFacade.Delete(obj);
            }

            DataContainerFacade.Delete(_erzl);
            return true;
        }

        private void ExecuteAddNotify()
        {
            //TODO
          // if (DialogHelper.AddNotify(_erzl.ID, NotifiesList))
           //     RefreshNotifiesList();
        }

        private void ExecuteEditNotify()
        {
           // if (DialogHelper.EditNotify(_erzl.ID, NotifiesList, _focusedERZLNotify))
          //      RefreshNotifiesList();
            //TODO
        }
        #endregion


        public void LoadZLRedist(long id)
        {
            try
            {
                _erzl = DataContainerFacade.GetByID<ERZL, long>(id);

                RefreshNotifiesList();

                if (FZList != null && _erzl != null)
                {
                    _selectedFz = FZList.Find(fz => fz.ID == _erzl.FZ_ID);
                    FzId = _selectedFz?.ID ?? 0;
                }
            }
            catch (Exception e)
            {
                throw new ViewModelInitializeException();
            }
        }

        public void RefreshNotifiesList()
        {
            var isdatachanged = IsDataChanged;

            try
            {
                NotifiesList = WCFClient.GetDataServiceFunc().GetERZLNotifiesListByERZL_ID(_erzl.ID);
            }
            catch
            {
                NotifiesList = null;
            }

            IsDataChanged = isdatachanged;
        }

        public void RefreshFZList()
        {
            FZList = DataContainerFacade.GetList<FZ>();
            if (FzId == 0) FzId = FZList.FirstOrDefault()?.ID ?? 0;
        }

#region Validation
        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";

                switch (columnName)
                {
                    case "Company": return string.IsNullOrEmpty(Company) ? errorMessage : Company.ValidateMaxLength(64);
                    case "WebDate": return WebDate == null || WebDate == DateTime.MinValue ? errorMessage : null;
                    case "SelectedFZ": return FzId == 0 ? errorMessage : null;
                    default: return null;
                }
            }
        }

        public override string Validate()
        {
            //const string errorMessage = "Неверный формат данных";

            const string fieldNames =
               "Company|Date|SelectedFZ";

            return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
        }
#endregion
        #endregion
    }
}