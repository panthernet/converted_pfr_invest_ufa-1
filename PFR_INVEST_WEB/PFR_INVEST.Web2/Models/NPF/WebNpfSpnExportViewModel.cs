﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfSpnExportViewModel: PureWebDialogViewModel
    {
        [JsonIgnore]
        [XmlIgnore]
        public IQueryable Query { get; set; }

        public WebNpfSpnExportViewModel()
        {
            //TODO
           // Query = WCFClient.GetEFServiceFunc()?.GetSpnListForExport(OnesSettingsDef.IntGroup.Npf);
        }

        private void ExecuteExport()
        {
          /*  var data = WCFClient.GetEFServiceFunc().SettingsPathFiles.FirstOrDefault(a=> a.ID == (long)SettingsPathFile.IdDefinition.OnesExport);
			bool isClient = data.ServClient == (int)SettingsPathFile.FileLocation.Client;

			var resultItems = WCFClient.Client.StartOnesExport(data.Path, isClient, OnesSettingsDef.IntGroup.Npf, list);

			var errors = resultItems.Where(a => a is WebServiceDataError).Cast<WebServiceDataError>().ToList();
			if (errors.Any())
			{
				errors.ForEach(a =>
				{
					switch (a)
					{
						case WebServiceDataError.DirectoryNotFound:
							ViewModelBase.DialogHelper.ShowError("Папка для экспорта на сервере, указанная в настройках, не существует и не удается создать её автоматически!");
							break;
						case WebServiceDataError.GeneralException:
							ViewModelBase.DialogHelper.ShowError("Во время обработки операции экспорта возникла непредвиденная ошибка!");
							break;
						case WebServiceDataError.SessionInProgress:
							ViewModelBase.DialogHelper.ShowError("На сервере уже запущена сессия экспорта данных для 1С! \nПопробуйте повторить операцию позднее.");
							break;
					}
				});
				return;
			}

			var name = "НПФ";


            Task.Factory.StartNew(() =>
			{
				OnesExportStatus status;
				do
				{
					status = WCFClient.Client.GetOnesExportStatus(OnesSettingsDef.IntGroup.Npf);
					//App.StatusbarManager.SetProgress("Экспорт 1С - " + name, status.Count, status.MaxCount, 0D);
					TaskHelper.Delay(2000).Wait();
				}
				while (!status.Completed);
			})
			.ContinueWith(param =>
			{
				if (!isClient) return;

				try
				{
					var count = 0;
					//вычисляем порядковый номер в рамках дня
					//АНАЛОГИЧНАЯ ЛОГИКА В OnesGenerateExport
					Directory.EnumerateFiles(data.Path, "docip_*.xml").ToList().ForEach(a =>
					{
						var array = Path.GetFileNameWithoutExtension(a)?.Split('_');
						var src = array?[2];
					    if (DateTime.ParseExact(src, "yyMMdd", new DateTimeFormatInfo()).Date != DateTime.Now.Date) return;
					    var nCount = Convert.ToInt32(array?[4]);
					    count = nCount > count ? nCount : count;
					});

					WCFClient.Client.OnesGetGeneratedExportFiles().ForEach(f =>
					{
						try
						{
							if (f.Repository.Length == 0) return;
							var file = Path.Combine(data.Path, $"docip_RequestForTransfer_{DateTime.Now.ToString("yyMMdd_hhmmss")}_{++count}.xml");
							using (var writer = File.CreateText(file))
							{
								writer.Write(Encoding.Default.GetChars(GZipCompressor.Decompress(f.Repository)));
							}
						}
						catch (Exception ex)
						{
							App.log.WriteException(ex);
						}
					});
				}
				catch (Exception ex)
				{
					App.log.WriteException(ex);
				}
			});
			//результат*/
        }
    }
}