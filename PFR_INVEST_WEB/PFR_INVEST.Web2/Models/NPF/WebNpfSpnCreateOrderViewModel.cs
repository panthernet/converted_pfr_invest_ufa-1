﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using JournalLogger = PFR_INVEST.BusinessLogic.Journal.JournalLogger;
using XlBorderWeight = Microsoft.Office.Core.XlBorderWeight;
using XlHAlign = Microsoft.Office.Core.XlHAlign;
using XlVAlign = Microsoft.Office.Core.XlVAlign;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfSpnCreateOrderViewModel: PureWebDialogViewModel
    {
        public long ID {get; set; }

        public WebNpfSpnCreateOrderViewModel() : this(0) { }
        public WebNpfSpnCreateOrderViewModel(long regID)
        {
            ID = regID;
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnCreateOrderViewPartial");
            WebModelHelperEx.UpdateModelState(this);

            _registerID = regID;

            if (_registerID > 0)
                _register = DataContainerFacade.GetByID<Register, long>(_registerID);

            TrancheDate = DateTime.Now;
            TrancheNumber = $@"НПФ-{(_register == null ? "0" : _register.RegNum)}/-";
            IncludeINNKPP = false;
        }

        public void Save()
        {
            ExecuteCreateRequest();
        }

        private const string TEMPLATE_NAME = "Заявка на перечисление в НПФ.xls";
		private const string TEMPLATE_NAME_INN = "Заявка на перечисление в НПФ (с ИННКПП банка).xls";

		CultureInfo _oldCi;

		private readonly long _registerID;

        [DisplayName("Номер")]
        [Required]
		public DateTime? TrancheDate
		{
			get
			{
				return _register?.TrancheDate;
			}

			set
			{
				if (_register == null) return;
				_register.TrancheDate = value;
			}
		}

	    [DisplayName("Дата")]
	    [Required]
        public string TrancheNumber
        {
            get
            {
                return _register?.TrancheNumber;
            }

            set
            {
                if (_register == null) return;
                _register.TrancheNumber = value;
            }
        }

        [DisplayName("Выводить ИНН/КПП банка получателя средств")]
		public bool IncludeINNKPP { get; set; }

        [JsonIgnore]
		private readonly Register _register;

		#region FinregistersList
        [JsonIgnore]
		private List<FinregisterWithCorrAndNPFListItem> _finregWithNPFList;

        [JsonIgnore]
        public List<Finregister> FinregistersList { get; set; }

        #endregion

		#region Execute Implementations
		private void ExecuteRefreshRegistersList()
		{
			try
			{
				_finregWithNPFList = WCFClient.GetDataServiceFunc().GetFinregisterWithCorrAndNPFList(_register.ID);
				_finregWithNPFList.Sort((x, y) => string.Compare(y.NPFName, x.NPFName, StringComparison.Ordinal));
				//this.finregistersList = new List<Finregister>(this.register.GetFinregistersList().Cast<Finregister>());
			}
			catch (Exception e)
			{
				throw new ViewModelInitializeException();
			}
		}
		#endregion


		public void ExecuteCreateRequest()
		{
			DataContainerFacade.Save<Register, long>(_register);
			JournalLogger.LogEvent("Заявка на перечисление в НПФ", JournalEventType.INSERT, ID, "Создание заявки на перечисление", $"Регистр: {_register.ID}");
			object filePath = TemplatesManager.ExtractTemplate(IncludeINNKPP ? TEMPLATE_NAME_INN : TEMPLATE_NAME);

			if (File.Exists((string)filePath))
			{
				Application oXl = null;
				Workbook oWb = null;
				Worksheet oSheet = null;

				try
				{
					_oldCi = Thread.CurrentThread.CurrentCulture;
					Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

					try
					{
						oXl = new Application(); oWb = oXl.Workbooks.Add(filePath);
						oSheet = (Worksheet)oWb.ActiveSheet;

					    var payAss = _register.PayAssignment;//BLServiceSystem.Client.GetPayAssForReport((long)Element.SpecialDictionaryItems.AppPartNPF, Element.KindToCommonDirection(_register.Kind));
						OfficeTools.FindAndReplace(oXl, "{RequestDate}", TrancheDate?.ToString("dd.MM.yyyy") ?? string.Empty);
                        OfficeTools.FindAndReplace(oXl, "{PayAss}", payAss ?? string.Empty);

#region SORTING
						/* finregistersList.Sort((x, y) => 
                    {
                        if (this.register.Kind.ToLower().Trim() == "передача из пфр в нпф")
                            try { le = BLServiceSystem.Client.GetLegalEntityForContragent(x.GetDebitAccount().GetContragent().ID); }
                            catch { return 1; }
                        else
                            try { le = BLServiceSystem.Client.GetLegalEntityForContragent(x.GetCreditAccount().GetContragent().ID); }
                            catch { return 1; }
                        var xname = le.GetContragent().Name;

                        if (this.register.Kind.ToLower().Trim() == "передача из пфр в нпф")
                            try { le = BLServiceSystem.Client.GetLegalEntityForContragent(y.GetDebitAccount().GetContragent().ID); }
                            catch { return -1; }
                        else
                            try { le = BLServiceSystem.Client.GetLegalEntityForContragent(y.GetCreditAccount().GetContragent().ID); }
                            catch { return -1; }
                        var yname = le.GetContragent().Name;

                        return xname.CompareTo(yname); 
                    });*/
#endregion

						var finregWithNPFList = _finregWithNPFList.Where(a => !RegisterIdentifier.FinregisterStatuses.IsNotTransferred(a.Finregister.Status)).ToList();
						var frlCount = finregWithNPFList.Count;
						for (int i = 0; i < frlCount; i++)
						{
							if (i > 0)
							{
								oSheet.Cells.Range[oSheet.Cells[8, 1], oSheet.Cells[8, 9]].Select();
								((Range)oXl.Selection).Insert(XlInsertShiftDirection.xlShiftDown, true);

								//Copy format
								var r1 = (Range)oSheet.Rows[9];
								r1.Copy(Type.Missing);

								var r2 = (Range)oSheet.Rows[8];
								r2.PasteSpecial(XlPasteType.xlPasteFormats, XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
							}

							var fr = finregWithNPFList[i];

							LegalEntity le;
							if (RegisterIdentifier.IsToNPF(_register.Kind.ToLower()))
							{
								try
								{
									le = WCFClient.GetDataServiceFunc().GetLegalEntityForContragent(fr.Finregister.GetDebitAccount().GetContragent().ID);
								}
								catch
								{
									le = null;
								}
							}
							else
							{
								try
								{
									le = WCFClient.GetDataServiceFunc().GetLegalEntityForContragent(fr.Finregister.GetCreditAccount().GetContragent().ID);
								}
								catch
								{
									le = null;
								}
							}

							BankAccount ba = null;
							if (le != null)
							{
								try
								{
									((Range)oSheet.Cells[8, 1]).Value2 = le.FormalizedName;//fr.NPFName;
								}
								catch
								{ }

								//((Range)oSheet.Cells[8, 2]).Value2 = le.ShortName;
								((Range)oSheet.Cells[8, 3]).Value2 = le.INN;

								ba = le.GetBankAccount();
							}

							if (ba != null)
							{
								((Range)oSheet.Cells[8, 2]).Value2 = ba.LegalEntityName;
								((Range)oSheet.Cells[8, 4]).Value2 = ba.AccountNumber;
								((Range)oSheet.Cells[8, 5]).Value2 = ba.BankName;
								((Range)oSheet.Cells[8, 6]).Value2 = ba.CorrespondentAccountFull; ((Range)oSheet.Cells[8, 6]).HorizontalAlignment = XlHAlign.xlHAlignLeft;
								((Range)oSheet.Cells[8, 7]).Value2 = ba.BIK;
								if (IncludeINNKPP)
								{
									((Range)oSheet.Cells[8, 8]).Value2 = ba.INN + " / " + ba.KPP;
								}
							}

							if (IncludeINNKPP)
							{
								((Range)oSheet.Cells[8, 9]).Value2 = fr.Count;
							}
							else
							{
								((Range)oSheet.Cells[8, 8]).Value2 = fr.Count;
							}

							((Range)oSheet.Cells[8, 1]).HorizontalAlignment = XlHAlign.xlHAlignLeft;
							((Range)oSheet.Cells[8, 2]).HorizontalAlignment = XlHAlign.xlHAlignLeft;
							((Range)oSheet.Cells[8, 3]).HorizontalAlignment = XlHAlign.xlHAlignCenter;
							((Range)oSheet.Cells[8, 4]).HorizontalAlignment = XlHAlign.xlHAlignRight;
							((Range)oSheet.Cells[8, 5]).HorizontalAlignment = XlHAlign.xlHAlignLeft;
							((Range)oSheet.Cells[8, 6]).HorizontalAlignment = XlHAlign.xlHAlignRight;
							((Range)oSheet.Cells[8, 7]).HorizontalAlignment = XlHAlign.xlHAlignCenter;
							((Range)oSheet.Cells[8, 8]).HorizontalAlignment = XlHAlign.xlHAlignCenter;
							((Range)oSheet.Cells[8, 9]).HorizontalAlignment = XlHAlign.xlHAlignRight;

						}

						oSheet.Cells.Range[oSheet.Cells[8, 1], oSheet.Cells[8 + frlCount, 9]].Select();
						((Range)oXl.Selection).WrapText = true;

						((Range)oXl.Selection).VerticalAlignment = XlVAlign.xlVAlignBottom;

						((Range)oXl.Selection).Font.Size = 10;
						((Range)oXl.Selection).Font.Bold = false;
						((Range)oXl.Selection).Borders.Weight = XlBorderWeight.xlThin;

						((Range)oXl.Selection).Borders.LineStyle = XlLineStyle.xlContinuous;

						((Range)oXl.Selection).Rows.EntireRow.AutoFit();

						oSheet.Cells.Range[oSheet.Cells[8 + frlCount, 1], oSheet.Cells[8 + frlCount, 1]].Select();

						((Range)oXl.Selection).EntireRow
							.Delete(XlDeleteShiftDirection.xlShiftUp);

						var lastCol = 8;
						if (IncludeINNKPP)
							lastCol = 9;

						oSheet.Cells.Range[oSheet.Cells[8 + frlCount, lastCol], oSheet.Cells[8 + frlCount, lastCol]].Select();

						string formula = "SUM";

						var lang = new CultureInfo(oXl.LanguageSettings.LanguageID[MsoAppLanguageID.msoLanguageIDUI]);
						if (lang.Name == "ru-RU")
							formula = "СУММ";

						((Range)oSheet.Cells[8 + frlCount, lastCol]).FormulaLocal = string.Format(IncludeINNKPP ? "={0}(I8:I{1}" : "={0}(H8:H{1}", formula, 8 + frlCount - 1);

						oSheet.Cells.Range[oSheet.Cells[8, lastCol], oSheet.Cells[8 + frlCount, lastCol]].Select();


						((Range)oXl.Selection).HorizontalAlignment = XlHAlign.xlHAlignRight;

						var sumCell = ((Range)oSheet.Cells[8 + frlCount, lastCol]);
                        if (sumCell.Value2 != null && Convert.ToDecimal(sumCell.Value2) > 0)
                            ((Range)oSheet.Cells[8 + frlCount + 1, 2]).Value2 = Сумма.Пропись(Convert.ToDecimal(sumCell.Value2), Валюта.Рубли);

						((Range)oSheet.Cells[8 + frlCount + 1, 2]).Select();

						((Range)oXl.Selection).HorizontalAlignment = XlHAlign.xlHAlignRight;                        

#if WEBCLIENT
					    WebFilename = Path.GetTempFileName() + ".xls";
					    oXl.Save(WebFilename);
#endif
					}
					finally
					{
						Thread.CurrentThread.CurrentCulture = _oldCi;
					}

                }
				catch (Exception e)
				{
					if (_oldCi != null && Thread.CurrentThread.CurrentCulture != _oldCi)
						Thread.CurrentThread.CurrentCulture = _oldCi;
				}
				finally
				{
					if (oSheet != null)
						Marshal.FinalReleaseComObject(oSheet);
					if (oWb != null)
						Marshal.FinalReleaseComObject(oWb);
					if (oXl != null)
						Marshal.FinalReleaseComObject(oXl);
				}
			}
			else
			{
				if (_oldCi != null && Thread.CurrentThread.CurrentCulture != _oldCi)
					Thread.CurrentThread.CurrentCulture = _oldCi;

			    WebLastModelError = $"Файл шаблона не найден - {TEMPLATE_NAME}";
			}
		}

	    public string WebFilename { get; set; }


	    public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToLower())
				{
                    case "tranchedate": return this.TrancheDate == null ? "Необходимо указать дату заявки" : null;
                    case "tranchenumber": return string.IsNullOrWhiteSpace(TrancheNumber) ? "Необходимо указать номер заявки" : TrancheNumber.ValidateMaxLength(50);
					default: return null;
				}
			}
		}

        [JsonIgnore]
		public bool IsValid => string.IsNullOrWhiteSpace(Validate());

	    public override string Validate()
		{
			const string fieldNames =
				"tranchedate|tranchenumber";

			return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
		}

    }
}