﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using JournalLogger = PFR_INVEST.Web2.BusinessLogic.JournalLogger;
using PureWebFormViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebFormViewModel;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;


namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(FinregisterWeb), JournalDescription = "Финреестр СПН")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfSpnFinregisterViewModel2: PureWebFormViewModel
    {
        public override void WebPostModelUpdateAction()
        {
            SelectedStatusNPF = StatusNPFList?.FirstOrDefault(a => a.ID == WebStatusNpfID);
        }

        [Required]
        [DisplayName("Состояние НПФ")]
        public long? WebStatusNpfID
        {
            get { return SelectedStatusNPF?.ID; }
            set { SelectedStatusNPF = StatusNPFList?.FirstOrDefault(a => a.ID == value); }
        }

        [Required]
        [DisplayName("Система гарантирования")]
        public long? WebNPFGarantID
        {
            get { return SelectedNPFGarant?.ID; }
            set { SelectedNPFGarant = NPFGarantList?.FirstOrDefault(a => a.ID == value); }
        }

        public long? WebRegisterID
        {
            get { return Finregister.RegisterID; }
            set { Finregister.RegisterID = value; }
        }

        [Required]
        [DisplayName("Наименование НПФ")]
        public long? WebNPFID
        {
            get { return SelectedNPF?.ID; }
            set { SelectedNPF = NPFList?.FirstOrDefault(a => a.ID == value); }
        }

        [DisplayName("Наименование НПФ")]
        public string WebCountLabelContent => RegisterIdentifier.IsFromNPF(Register?.Kind) ? "Сумма СПН" : "Сумма СПН*";

        [DisplayName("Дата документа")]
        public DateTime? WebFrDate
        {
            get { return Finregister.Date; }
            set { Finregister.Date = value; }
        }

        [DisplayName("Содержание операции")]
        public long? NT_ContentID
        {
            get { return NT_Content?.ID; }
            set { NT_Content = NT_ContentList?.FirstOrDefault(a => a.ID == value); }
        }

        [DisplayName("")]
        public string WebNT_RegisterName
        {
            get { return NT_Register?.Name; }
            set { if(NT_Register != null) NT_Register.Name = value; }
        }

        [DisplayName("ИНН")]
        public string WebSelectedINN
        {
            get { return SelectedNPF?.INN; }
            set { if(SelectedNPF != null) SelectedNPF.INN = value; }
        }

        [DisplayName("Содержание операции")]
        [JsonIgnore]
        public string FrContent => Register != null ? Register.Content : "";


        public WebNpfSpnFinregisterViewModel2() : this(0,0) { }

        public WebNpfSpnFinregisterViewModel2(object frId, object registerId, object isfornt) : this(0, Convert.ToInt64(registerId))
        {
            if(Convert.ToBoolean(isfornt))
            {
                _ntParentId = frId == null ? 0 : Convert.ToInt64(frId);
                var fr = DbHelper.Get<FinregisterWeb>(_ntParentId);
                NT_ZlLimit = fr.ZLCount ?? 0;
                NT_SpnLimit = fr.Count ?? 0;

                _isVirtualAddNpfMode = true;
            }
        }


        public WebNpfSpnFinregisterViewModel2(long entryId, long parentEntryId): base(entryId)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnFinregisterViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = false;
            NT_NpfList = new List<NT_NpfListItem>();

            //выбираем все НПФ, включая удаленные
            NPFList = DbHelper.GetEF().GetActiveNpfList();// WCFClient.GetDataServiceFunc().GetNPFListLEHib(new StatusFilter()).ToList();

			StatusNPFList = DataContainerFacade.GetList<Status>().Where(x => x.ID > 0).ToList();
			NPFGarantList = WCFClient.GetDataServiceFunc().GetElementByType(Element.Types.GarantACB).Where(x => x.Visible).ToList();

			FinMoveTypes = RegisterIdentifier.FinregisterMoveTypes.ToList;
			FinStatuses = RegisterIdentifier.FinregisterStatuses.ToList;

            if (State == ViewModelState.Create)
            {
                Finregister = new FinregisterWeb { RegisterID = parentEntryId, Count = 0 };
                Register = DbHelper.Get<RegisterWeb>(parentEntryId == 0 ? Finregister.RegisterID : parentEntryId);
                FrRegNum = Register.RegNum;
                WebFrDate = Register.RegDate;
                CFRCount = 0;
                SelectedNPFGarant = NPFGarantList.FirstOrDefault();
                SelectedStatusNPF = StatusNPFList.FirstOrDefault();
                FrStatus = FinStatuses.First(RegisterIdentifier.FinregisterStatuses.IsCreated);
                FinMoveType = FinMoveTypes.First();
                _isNotTransferred = RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status);
            }
            else
            {
                ReloadFRFromDB();
            }
            RefreshAddedNpfList();

            //очищаем все удаленные НПФ кроме потенциально выбранного
            NPFList.RemoveAll(a => a.Contragent.StatusID < 0 && a != SelectedNPF);


            //загрузка выбранных сущностей для статуса Финреестр не передан
			if (NT_ContentList == null)
				NT_ContentList = WCFClient.GetDataServiceFunc().GetElementByType(Element.Types.RegisterPFRtoNPF).Where(x => x.Visible).ToList();

			if (IsNotTransferred && Finregister != null)
			{
				if (Finregister.SelectedContentID != null)
					NT_Content = NT_ContentList.FirstOrDefault(a => a.ID == Finregister.SelectedContentID);
				if (Finregister.SelectedRegisterID != null)
				{
					var lRegister = DataContainerFacade.GetByID<Register>((long)Finregister.SelectedRegisterID);
					var lAppDoc = lRegister.GetApproveDoc();
					if (lRegister != null)
						NT_Register = new CommonRegisterListItem(lRegister.ID, lRegister.RegDate, lRegister.RegNum, lAppDoc == null ? null : lAppDoc.Name);
				}
			}
			IsDataChanged = false;
        }


		private const decimal MAX_DECIMAL_19_4 = 1000000000000000;

        [JsonIgnore]
		protected FinregisterWeb Finregister;

        [JsonIgnore]
		protected RegisterWeb Register;

		/// <summary>
		/// Является ли текущий финреестр дочерним от финреестра в статусе Не передан
		/// </summary>
        [JsonIgnore]
		public bool IsChildFinregister => Finregister?.ParentFinregisterID != null;

        [JsonIgnore]
	    public virtual bool TempAllocationVisibilityBool => false;
        [JsonIgnore]
	    public virtual bool CommonFinregisterVisibilityBool => true;

        [JsonIgnore]
	    public bool IsInputPropsCountBlocked { get; set; }

        /// <summary>
        /// Имеет ли родительский реестр флаг принудительного нахождения в архиве
        /// </summary>
        [JsonIgnore]
        public bool IsForcedArchiveRegister => Register != null && Register.IsCustomArchive == 1;


		#region NPF
        [JsonIgnore]
		public List<LegalEntityWeb> NPFList { get; set; }

        [JsonIgnore]
        public LegalEntityWeb SelectedNPF { get; set; }

        #endregion

		#region GarantACB

        [JsonIgnore]
        public List<Element> NPFGarantList { get; set; }

        [JsonIgnore]
        private Element _selectedNPFGarant;
        [JsonIgnore]
		public Element SelectedNPFGarant
		{
			get { return _selectedNPFGarant; }
			set
			{
				_selectedNPFGarant = value;
				FilterNPFList();
			}
		}
		#endregion

		#region StatusNPF
        [JsonIgnore]
		private List<Status> _statusNPFList = new List<Status>();
        [JsonIgnore]
		public List<Status> StatusNPFList
		{
			get { return _statusNPFList; }
			set
			{
				_statusNPFList = value;
				FilterNPFList();
			}
		}

        [JsonIgnore]
		private Status _selectedStatusNpf;
        [JsonIgnore]
		public Status SelectedStatusNPF
		{
			get { return _selectedStatusNpf; }
			set
			{
				if (SelectedStatusNPF != null && value != null && !StatusIdentifier.IsNPFStatusActivityCarries(SelectedStatusNPF.Name) && StatusIdentifier.IsNPFStatusActivityCarries(value.Name))
				{
					RestoreActiveFinregister();
				}

				_selectedStatusNpf = value;
				FilterNPFList();
			}
		}

		/// <summary>
		/// Видимость поля Система гарантирования
		/// </summary>
        [JsonIgnore]
		public bool IsNpfGarantVisible => IsActiveNPFStatusBool;

	    /// <summary>
		/// Выбрано состояние НПФ: Деятельность осуществляется
		/// </summary>
        [JsonIgnore]
		public bool IsActiveNPFStatusBool => _selectedStatusNpf != null && StatusIdentifier.IsNPFStatusActivityCarries(_selectedStatusNpf.Name) && !RegisterIdentifier.FinregisterStatuses.IsNotTransferred(FrStatus);

        [JsonIgnore]
	    public string IsActiveNPFStatus => IsActiveNPFStatusBool ? "True" : "False";

        [JsonIgnore]
	    public string IsActiveNPFStatusReadonly => (IsReadOnly() || IsActiveNPFStatusBool) ? "True" : "False";

        [JsonIgnore]
	    public string IsInactiveNPFStatus => ((IsForDeletedNPF && RegisterIdentifier.FinregisterStatuses.IsTransferred(Finregister.Status)) || IsActiveNPFStatusBool || RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status) || IsAvailableInBoNotComplete) ? "False" : "True";
        
        [JsonIgnore]
	    public bool IsInactiveNPFStatusBool => IsInactiveNPFStatus == "True";
	    #endregion


		#region Count
		public string CountLabelContent => RegisterIdentifier.IsFromNPF(Register.Kind) ? "Сумма СПН:" : "Сумма СПН*:";

	    public decimal? FrCount
		{
			get => Finregister?.Count;
	        set => Finregister.Count = value ?? 0;
	    }

        [JsonIgnore]
		public bool IsCountReadOnly
		{
			get
			{
				if (IsReturnedFromBackOffice) return true;
				//return isReadOnlyFinregister || (corrList != null && corrList.Any());
				if (_isVirtualAddNpfMode)
					return false;

				return IsReadOnlyFinregister || !(RegisterIdentifier.FinregisterStatuses.IsCreated(Finregister.Status) || RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status));
			}
		}

		/// <summary>
		/// Если финрееcтр доступен в БО и п/п не залочены
		/// </summary>
        [JsonIgnore]
		public bool IsAvailableInBoNotComplete
		{
			get
			{
				//передача
				if (!string.IsNullOrEmpty(Register.Kind) && RegisterIdentifier.IsToNPF(Register.Kind))
					return State == ViewModelState.Edit && Register.TrancheDate != null && (!DraftSum.HasValue || DraftSum != FrCount);
				return State == ViewModelState.Edit && FrStatus != null && RegisterIdentifier.FinregisterStatuses.IsIssued(FrStatus) && (!DraftSum.HasValue || DraftSum != FrCount);
			}
		}

		/// <summary>
		/// Если финреестр вернулся из БО c заполненными п/п
		/// </summary>
        [JsonIgnore]
		public bool IsReturnedFromBackOffice => DraftsList.Count > 0 && !IsAvailableInBoNotComplete;

	    #endregion

		#region CFRCount
        [JsonIgnore]
		public virtual bool IsCFRCountVisible => false;

	    public decimal? CFRCount
		{
			get
			{
				return Finregister?.CFRCount;
			}

			set
			{
				if (Finregister == null) return;
				Finregister.CFRCount = Convert.ToDecimal(value);
			}
		}
		#endregion

		#region Status
        [DisplayName("Состояние")]
		public string FrStatus
		{
			get
			{
				return Finregister != null ? Finregister.Status : string.Empty;
			}

			set
			{
				if (Finregister == null)
					return;

				if (value == null)
					throw new Exception("Статус финреестра невозможно сбросить в NULL");

				if (Finregister.Status == null)
				{
					var msg = "Статус финреестра: " + value;
					JournalLogger.LogStatusChangeEvent(this, Finregister.ID, msg);
				}
				else
				{
					if (Finregister.Status.Equals(value))
						return;

					var msg = "Статус финреестра: " + Finregister.Status + " -> " + value;
					JournalLogger.LogStatusChangeEvent(this, Finregister.ID, msg);
				}

				Finregister.Status = value;
			}
		}
		#endregion

		#region FinStatuses

        [JsonIgnore]
        public List<string> FinStatuses { get; set; }

        #endregion

		#region TrancheDate
        [DisplayName("Дата формирования заявки")]
		public DateTime? FrTrancheDate
		{
			get { return Register != null && RegisterIdentifier.IsToNPF(Register.Kind.ToLower()) ? Register.TrancheDate : null; }
			set
			{
				if (Register == null || !RegisterIdentifier.IsToNPF(Register.Kind.ToLower())) return;
				Register.TrancheDate = value;
			}
		}

	    public bool TrancheDateVisibilityBool => Register != null && RegisterIdentifier.IsToNPF(Register.Kind.ToLower());
	    #endregion

		#region RegNum

        [DisplayName("Номер документа")]
		public string FrRegNum
		{
			get { return Finregister.RegNum; }
			set
			{
				Finregister.RegNum = value;
			}
		}
		#endregion


		#region ZLCount
        [Required]
        [DisplayName("Количество ЗЛ по документу")]
		public long? ZLCount
		{
			get
			{
				return Finregister?.ZLCount;
			}

			set
			{
				if (Finregister == null) return;
                Finregister.ZLCount = Convert.ToInt64(value);
			}
		}

		/// <summary>
		/// Определяет доступность кол-ва ЗЛ для ввода
		/// </summary>
        [JsonIgnore]
		public bool IsZLCountReadOnly => IsReturnedFromBackOffice || IsReadOnlyFinregister;

	    #endregion

		#region FinMoveType
        [DisplayName("Способ передачи документа")]
		public string FinMoveType
		{
			get
			{
				return Finregister != null ? Finregister.FinMoveType : "";
			}

			set
			{
				if (Finregister == null) return;
				Finregister.FinMoveType = value;
			}
		}
		#endregion

		#region FinMoveTypes

        [JsonIgnore]
        public List<string> FinMoveTypes { get; set; }

        #endregion

		#region FinDate
        [DisplayName("Дата передачи документа")]
		public DateTime? FinDate
		{
			get
			{
				return Finregister?.FinDate;
			}

			set
			{
				if (Finregister == null) return;
				Finregister.FinDate = value;
			}
		}
		#endregion

		#region FinNum
        [DisplayName("Номер сопроводительного письма")]
		public string FinNum
		{
			get { return Finregister?.FinNum ?? ""; }

			set
			{
				if (Finregister == null) return;
				Finregister.FinNum = value;
			}
		}
		#endregion


		#region DraftsList

        [JsonIgnore]
        public List<AsgFinTrWeb> DraftsList { get; set; } = new List<AsgFinTrWeb>();

        [JsonIgnore]
	    public bool DraftsListVisibilityBool =>  DraftsList != null && DraftsList.Count > 0;
	    #endregion

		#region DraftSum
        [JsonIgnore]
		public decimal? DraftSum
		{
			get
			{
				if (DraftsList != null && DraftsList.Count > 0)
				{
					decimal? sum = null;
					foreach (var dr in DraftsList)
					{
						if (sum == null && dr.DraftAmount != null)
							sum = 0;
						sum += dr.DraftAmount;
					}
					return sum;
				}
				return null;
			}
		}
		#endregion

		#region Comment

        [DisplayName("Комментарий")]
		public string FrComment
		{
			get { return Finregister?.Comment ?? string.Empty; }

			set
			{
				if (Finregister == null) return;
				Finregister.Comment = value;
			}
		}
		#endregion

		#region Execute Implementations

        protected override bool WebCanExecuteDelete()
        {
            //Не удаляем финреестр в статусе "финреестр не передан", если у него есть дочерние финреестры
            if (_isNotTransferred && NT_NpfList.Any()) return false;
            //проверяем на наличие П/П, удаляем только если их нет - ОТМЕНЯЕТСЯ. Разрешаем удалять, п/п теряют связь.
            return State == ViewModelState.Edit;// && !DraftsList.Any();
        }

        public override bool WebExecuteDelete(long id)
        {
            WCFClient.GetDataServiceFunc().DeleteFinregister(id);
            return true;
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            // проверка целостности временного размещения - возможно редактирование финреестра, созданного по изъятию
            // проверка, что финреестр был создан по изъятию
            if (Register.ReturnID > 0)
            {
                var allowedFR = GetFinregWithAllowedSums();
                if (allowedFR == null) return false;
                // для изъятия суммы не должны превосходить рассчетные
                if (FrCount > allowedFR.Count)
                {
                    WebLastModelError = $"Недостаточно средств в размещении. Максимально допустимая сумма {allowedFR.Count}";
                    return false;
                }

                if (ZLCount > allowedFR.ZLCount)
                {
                    WebLastModelError = $"Неверно указано количество ЗЛ. Максимально допустимое количество {allowedFR.ZLCount}";
                    return false;
                }
            }

            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

	    public void SetTransferedFlag(int value)
	    {
	        Finregister.IsTransfered = value;
	    }

		public override bool WebExecuteSave()
		{
            BindNPFAccounts();
			FinNum = (FinNum ?? string.Empty).Trim();
			FrRegNum = (FrRegNum ?? string.Empty).Trim();

			//сохранение выбранных сущностей для статуса Финреестр не передан
			if (IsNotTransferred && Finregister != null)
			{
				Finregister.SelectedContentID = NT_ContentID;
				Finregister.SelectedRegisterID = NT_RegisterID;
			}
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				    //обновляем статус передачи для финреестра в статусе не передан
				    if (IsNotTransferred)
				        SetTransferedFlag(NT_NpfList.Count == 0 || !NT_NpfList.All(a=> RegisterIdentifier.FinregisterStatuses.IsTransferred(a.Status)) ? 0 : 1);
				    else
				    {
				        //обновляем статус передачи для финреестра в остальных статусах
				        SetTransferedFlag(RegisterIdentifier.FinregisterStatuses.IsTransferred(Finregister.Status) ? 1 : 0);
				        //обновляем статус передачи для родительского финреестра
				        if (_ntParentId != 0)
				        {
				            bool result = DataContainerFacade.GetListByProperty<Finregister>("ParentFinregisterID", _ntParentId, true)
				                .All(a => RegisterIdentifier.FinregisterStatuses.IsTransferred(a.Status));
				            WCFClient.GetDataServiceFunc().UpdateFinregisterIsTransferred(_ntParentId, result ? 1 : 0);
				        }
				    }
					DbHelper.SaveEntity(Finregister);
				    ID = Finregister.ID;
					break;
				case ViewModelState.Create:

					//Если добавляем НПФ для финреестра в статусе "Финреестр не передан" - запоминаем ID родителя
					if (_isVirtualAddNpfMode)
						Finregister.ParentFinregisterID = _ntParentId;
				    DbHelper.SaveEntity(Finregister);
				    ID = Finregister.ID;
					break;
			}

		    return true;
		}

		// метод получения финреестра с полями суммы и количества ЗЛ, соответствующими текущим остаткам (без учета сумм редактируемого финреестра, если он есть в БД)
		protected Finregister GetFinregWithAllowedSums()
		{
			if (Register != null && Register.PortfolioID.HasValue && SelectedNPF != null)
			{
				var portfID = Register.PortfolioID.Value;
				var selName = SelectedNPF.Contragent.Name;

				var allListSPN = WCFClient.GetDataServiceFunc().GetSPNAllocatedFinregisters(portfID).Where(x => x.NPFName == selName && x.ID != Finregister.ID);
				var retListSPN = WCFClient.GetDataServiceFunc().GetSPNReturnedFinregisters(portfID).Where(x => x.NPFName == selName && x.ID != Finregister.ID);

				var fr = new Finregister();

				fr.Count = allListSPN.Sum(x => x.Count) - retListSPN.Sum(x => x.Count);
				fr.ZLCount = allListSPN.Sum(x => x.ZLCount) - retListSPN.Sum(x => x.ZLCount);

				return fr;
			}

			return null;
		}

		protected void BindNPFAccounts()
		{
			if (SelectedNPF != null && SelectedNPF.ContragentID.HasValue && Finregister != null)
			{
				var accNPF = WCFClient.GetDataServiceFunc().GetContragentRoubleAccount(SelectedNPF.ContragentID.Value);
				var accPFR = WCFClient.GetDataServiceFunc().GetPFRRoubleAccount();

				if (accNPF == null)
					throw new Exception("Лиц счет НПФ не найден");

				if (accPFR == null)
					throw new Exception("Лиц счет ПФР не найден");

				if (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()))
				{
					try
					{
						Finregister.CrAccID = accNPF.ID;
						Finregister.DbtAccID = accPFR.ID;
					}
					catch
					{
						Finregister.CrAccID = 0;
						Finregister.DbtAccID = 0;
					}
				}
				else
				{
					try
					{
						Finregister.CrAccID = accPFR.ID;
						Finregister.DbtAccID = accNPF.ID;
					}
					catch
					{
						Finregister.CrAccID = 0;
						Finregister.DbtAccID = 0;
					}
				}
			}
		}
#endregion

        [JsonIgnore]
		public bool IsTextFieldsEnabled => false;

        [JsonIgnore]
	    protected bool IsReadOnlyFinregister => IsAvailableInBoNotComplete || State == ViewModelState.Read;


        /// <summary>
        /// Связан ли финреестр с удаленным НПФ
        /// </summary>
        [JsonIgnore]
        public bool IsForDeletedNPF => SelectedNPF != null && SelectedNPF.Contragent.StatusID < 0;

        [JsonIgnore]
	    public bool isEnabled => !RegisterIdentifier.FinregisterStatuses.IsTransferred(FrStatus) && !RegisterIdentifier.FinregisterStatuses.IsNotTransferred(FrStatus) && (_isVirtualAddNpfMode || !IsReadOnlyFinregister) && !IsAllReadonly;

        public bool IsAllReadonly => RegisterIdentifier.IsToNPF(Register.Kind) && (RegisterIdentifier.FinregisterStatuses.IsIssued(FrStatus) || RegisterIdentifier.FinregisterStatuses.IsTransferred(FrStatus) || RegisterIdentifier.FinregisterStatuses.IsNotTransferred(FrStatus) ||
                                     (RegisterIdentifier.FinregisterStatuses.IsCreated(FrStatus) && Register.TrancheDate != null && State != ViewModelState.Create && (!DraftSum.HasValue || DraftSum != FrCount)));


        public void ReloadFRFromDB()
		{
		    Finregister = DbHelper.Get<FinregisterWeb>(ID, "AsgFinTrs");
		    Register = DbHelper.Get<RegisterWeb>(Finregister.RegisterID);
		    _isNotTransferred = RegisterIdentifier.FinregisterStatuses.IsNotTransferred(Finregister.Status);

			if (string.IsNullOrEmpty(FrRegNum))
				FrRegNum = Register.RegNum;
			if (WebFrDate == null)
				WebFrDate = Register.RegDate;

			if (NPFList != null && Finregister != null)
			{

			    var accountDebit = DbHelper.Get<AccountWeb>(Finregister.DbtAccID);
				var accountCredit = DbHelper.Get<AccountWeb>(Finregister.CrAccID);

				SelectedNPF = NPFList.FirstOrDefault(ca =>
					ca.ContragentID == (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID));


				if (SelectedNPF != null)
				{
					SelectedStatusNPF = StatusNPFList.FirstOrDefault(x => x.ID == SelectedNPF.Contragent.StatusID);
					SelectedNPFGarant = NPFGarantList.FirstOrDefault(a => a.ID == Element.SpecialDictionaryItems.ACBAll.ToLong()); //NPFGarantList.FirstOrDefault(x => x.ID == SelectedNPF.GarantACBID);
					
				}


				SelectedNPF = NPFList.FirstOrDefault(ca =>
					ca.ContragentID == (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID));
			}

			RefreshDraftsList();

			FrStatus = FinStatuses.FirstOrDefault(x => x.Equals(FrStatus, StringComparison.InvariantCultureIgnoreCase)) ?? FinStatuses.First(RegisterIdentifier.FinregisterStatuses.IsCreated);

			FinMoveType = FinMoveTypes.FirstOrDefault(x => x.Equals(FinMoveType, StringComparison.InvariantCultureIgnoreCase));

            if(_isNotTransferred && Finregister.SelectedRegisterID > 0)
                WebUpdateNtRegisterSelection(Finregister.SelectedRegisterID ?? 0, false);

		}

#region Refresh

		protected virtual void FilterNPFList()
		{

			if (Register == null) return;
			long savedNPF = 0;
			if (SelectedNPF != null)
				savedNPF = SelectedNPF.ID;

            if (SelectedStatusNPF != null)
            {
                if (!IsActiveNPFStatusBool || SelectedNPFGarant == null)
                {
                    NPFList = DbHelper.GetEF().GetNpfList(false, 0, SelectedStatusNPF.Name);/*) WCFClient.GetDataServiceFunc().GetNPFListLEHib(new StatusFilter
                    { 
                        new FilterOP { BOP = BOP.And, OP = OP.EQUAL, Identifier = SelectedStatusNPF.Name}
                    }).ToList();*/
                }
                else
                {
                    NPFList = DbHelper.GetEF().GetNpfList(false, SelectedNPFGarant.ID); /* WCFClient.GetDataServiceFunc().GetNPFGarantedListLEHib(new StatusFilter
                    { 
                        new FilterOP { BOP = BOP.And, OP = OP.EQUAL, Identifier = SelectedStatusNPF.Name}
                    }, SelectedNPFGarant.ID)
                        .ToList();*/
                }
            }
            else
            {
                //финреестр для удаленного НПФ - оставляем в списке только выбранный НПФ
                NPFList = IsForDeletedNPF ? NPFList.Where(a => a == SelectedNPF).ToList() : new List<LegalEntityWeb>();
            }


			var accountDebit = DbHelper.Get<AccountWeb>(Finregister.DbtAccID);
			var accountCredit = DbHelper.Get<AccountWeb>(Finregister.CrAccID);
			bool IsNPFtoPFR = RegisterIdentifier.IsFromNPF(Register.Kind.ToLower());
			var caCurrent = IsNPFtoPFR ? accountCredit?.ContragentID : accountDebit?.ContragentID;


			//var finregisters = Register.GetFinregistersList();
			//var usedAcc = finregisters.Where(a => a.StatusID != -1).Select(fr => IsNPFtoPFR ? fr.CrAccID : fr.DbtAccID).ToList();
			var usedNPF = WCFClient.GetDataServiceFunc().GetUsedContragentIdListForRegister(Register.ID);

			NPFList = NPFList.Where(le => le.ContragentID == null || !usedNPF.Contains(le.ContragentID.Value) || le.ContragentID == caCurrent).ToList();

			//Востанавливаем по возможности старое выбраное значение
			SelectedNPF = NPFList.FirstOrDefault(le => le.ID == savedNPF);
		}


		private void RestoreActiveFinregister()
		{
			if (Finregister == null || Finregister.ID <= 0) return;
			var dbReg = DataContainerFacade.GetByID<Finregister>(Finregister.ID);
			if (dbReg == null) return;
			if (!StatusIdentifier.IsNPFStatusActivityCarries
				(
					(
						RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ?
							dbReg.GetCreditAccount().GetContragent().GetStatus() :
							dbReg.GetDebitAccount().GetContragent().GetStatus()
						).Name
				)) return;
			FrStatus = dbReg.Status;
			FinMoveType = dbReg.FinMoveType;
			FinDate = dbReg.FinDate;
			FinNum = dbReg.FinNum;
		}

		public void RefreshDraftsList()
		{
			bool isdatachanged = IsDataChanged;
			DraftsList = Finregister.AsgFinTrs ?? new List<AsgFinTrWeb>();
			IsDataChanged = isdatachanged;
		}

#endregion

#region Validation
        [JsonIgnore]
		public override string this[string columnName]
		{
			get
			{
				const string errorMessage = "Неверный формат данных";
				//const string errorRegNum = "Номер договора и дата договора должны быть пусты или заполнены одновременно";
				switch (columnName.ToLower())
				{
					case "selectednpf": return SelectedNPF == null ? errorMessage : null;

					case "count":
						{
							decimal count = FrCount ?? 0;
							if (count > MAX_DECIMAL_19_4)
								return errorMessage;

							if (_isVirtualAddNpfMode)
							{
								if (count > NT_SpnLimit)
									return $"Значение СПН должно быть не более {NT_SpnLimit}";
								if (count != NT_SpnLimit && ZLCount == NT_ZlLimit)
									return $"Сумма СПН должна быть максимальной ({NT_SpnLimit}), т.к. для распределения указаны все доступные ЗЛ";
							}

							if (RegisterIdentifier.IsFromNPF(Register.Kind))
								return FrCount < 0 ? errorMessage : null;
							return FrCount < 0 ? errorMessage : null;
						}

					case "regnum": return FrRegNum.ValidateMaxLength(50);
					//case "date": return (string.IsNullOrWhiteSpace(this.RegNum) ^ this.Date == null) ? errorRegNum : null;

					case "finnum": return FinNum != null && FinNum.Length > 49 ? errorMessage : null;
					//case "findate": return FinDate == null ? errorMessage : null;
				//	case "finmovetype": return string.IsNullOrEmpty(FinMoveType) ? errorMessage : null;
					case "cfrcount": return CFRCount.ValidateNonNegative() ?? CFRCount.ValidateMaxFormat();
					case "comment": return FrComment.ValidateMaxLength(512);

					case "ntregister":
						return _isNotTransferred && NT_RegisterID == null ? errorMessage : null;

					case "zlcount":
						if (ZLCount == null || ZLCount == 0 || !DataTools.IsNumericString(ZLCount.ToString()))
							return errorMessage;
						if (_isVirtualAddNpfMode)
						{
							if (ZLCount > NT_ZlLimit)
								return $"Значение ЗЛ должно быть не более {NT_ZlLimit}";
							decimal count = FrCount ?? 0;
							if (count == NT_SpnLimit && ZLCount != NT_ZlLimit)
								return $"Кол-во ЗЛ должно быть максимальным ({NT_ZlLimit}), т.к. для распределения указана максимально допустимая сумма СПН";
						}
						return null;
					default: return null;
				}
			}
		}

		public override string Validate()
		{
			var fieldNames =
			    $"selectednpf|count|zlcount|regnum|date|finnum|cfrcount|comment{(_isNotTransferred ? "|ntregister" : null)}";

			return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
		}
#endregion


#region МЕТОДЫ И СВОЙСТВА ДЛЯ ФИНРЕЕСТРА В СТАТУСЕ "ФИНРЕЕСТР НЕ ПЕРЕДАН"

		//РЕЖИМ ДОБАВЛЕНИЯ НПФ = когда финреестр используется с формой Добавить НПФ, для добавления НПФ к финреестру в статусе "Финреестр не передан"

#region NT Props

        [JsonIgnore]
		public NT_NpfListItem NT_SelectedNpf { get; set; }

		/// <summary>
		/// Выбранный реестр для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public CommonRegisterListItem NT_Register { get; set; }
		/// <summary>
		/// Выбранное содержание операции для финреестра в статусе "Финреестр не передан"
		/// </summary>
		public Element NT_Content { get; set; }
		/// <summary>
		/// Список содержаний операций для финреестра в статусе "Финреестр не передан"
		/// </summary>
        [JsonIgnore]
		public List<Element> NT_ContentList { get; set; }
		/// <summary>
		/// Список добавленных НПФ для для финреестра в статусе "Финреестр не передан" </summary>
        [JsonIgnore]
		public List<NT_NpfListItem> NT_NpfList { get; set; }
		/// <summary>
		/// Макс. кол-во ЗЛ в режиме добавления НПФ (для финреестра в статусе "Финреестр не передан")
		/// </summary>
		public long NT_ZlLimit { get; set; }
		/// <summary>
		/// Макс. кол-во СПН в режиме добавления НПФ (для финреестра в статусе "Финреестр не передан")
		/// </summary>
		public decimal NT_SpnLimit { get; set; }
		/// <summary>
		/// Ссылка на родительский ID (для финреестра в статусе "Финреестр не передан")
		/// </summary>
		private long? _ntParentId;

		/// <summary>
		/// Режим добавления НПФ (для работы с финреестром в статусе "Финреестр не передан")
		/// </summary>
        [JsonIgnore]
		private bool _isVirtualAddNpfMode;

		//ublic bool IsVirtualAddNpfMode { get { return _isVirtualAddNpfMode; } }

        [JsonIgnore]
		private bool _isNotTransferred;
		/// <summary>
		/// Статус финреестра = "Финреестр не передан"
		/// </summary>
        [JsonIgnore]
		public bool IsNotTransferred => _isNotTransferred;

        public long? NT_RegisterID { get; set; }

#endregion

		/// <summary>
		/// Получить макс. допустимые значения СПН и ЗЛ для режима добавления НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		/// <returns>0 - СПН, 1 - ЗЛ</returns>
		private object[] NT_GetMaxValueCounts()
		{
			return new object[]
                {
                    FrCount - NT_NpfList.Sum(a=>a.Summ),
                    ZLCount - NT_NpfList.Sum(a => a.ZlCount)
                };
		}

		/// <summary>
		/// Обновить список добавленных НПФ для финреестра в статусе "Финреестр не передан"
		/// </summary>
		private void RefreshAddedNpfList()
		{
			NT_NpfList = DataContainerFacade.GetListByProperty<Finregister>("ParentFinregisterID", Finregister.ID, true).Select(a =>
			{
				var fr = new NT_NpfListItem(a);
				var cid = RegisterIdentifier.IsFromNPF(a.GetRegister().Kind.ToLower()) ? a.GetCreditAccount().ContragentID : a.GetDebitAccount().ContragentID;
				fr.Name = DataContainerFacade.GetByID<Contragent>(cid).GetLegalEntity().FormalizedName;

				return fr;
			}).ToList();
		}

#region Выполнение команд

		public void ExecuteDeleteNpfCommand(long id)
		{
			switch (WCFClient.GetDataServiceFunc().DeleteFinregister(id, false, null, true))
			{
				case WebServiceDataError.FinregisterNotInCreatedStatus:
					WebLastModelError = "Невозможно удалить НПФ т.к финреестр находится в статусе, отличном от начального!";
					return;
			}
		}


		private void ExecuteOkAddNpfCommandCommand()
		{
			WebExecuteSave();
		}

		private bool CanExecuteOkAddNpfCommandCommand()
		{
			return _isVirtualAddNpfMode && string.IsNullOrEmpty(Validate());
		}

		private void ExecuteAddNpfCommandCommand()
		{////////////////////////////////
		/*	var res = NT_GetMaxValueCounts();
			var count = (decimal?)res[0];
			var zl = (long?)res[1];

			if (count == 0 && zl == 0)
			{
				WebLastModelError = "Невозможно добавить новый НПФ, т.к. все доступные средства уже распределены!";
				return;
			}

			var result = DialogHelper.AddNpfForFinregister(NT_Register.ID, zl ?? 0, count ?? 0, Finregister.ID);
			if (!result) return;

			RefreshAddedNpfList();
			ExecuteSave();

			res = NT_GetMaxValueCounts();
			count = (decimal?)res[0];
			zl = (long?)res[1];
			if (count < 0 || zl < 0)
				;
			//выполнили разнесение средств по НПФ
			//помещаем в архив
			if (count == 0 && zl == 0)
			{
				//уходит само
				//base.ExecuteDelete(DocOperation.Archive);
			}
			IsDataChanged = false;*/
		}

		public bool CanExecuteAddNpfCommandCommand()
		{
			return NT_RegisterID > 0;
		}

		private void ExecuteSelectRegisterCommand()
		{
			//////////////////NT_Register = DialogHelper.ShowSelectRegister(AsgFinTr.Sections.NPF, AsgFinTr.Directions.FromPFR, NT_Content.Name);
		}

		public bool CanExecuteSelectRegisterCommand()
		{
			if (NT_Content == null) return false;
			if (NT_NpfList != null)
			{
				var res = NT_GetMaxValueCounts();
				if ((long?)res[1] == 0 && (decimal?)res[0] == 0) return false;
			}
			return true;
		}
#endregion
#endregion

        public void WebUpdateNtRegisterSelection(long ntID, bool save = true)
        {
            NT_RegisterID = ntID;            
            NT_Register = WCFClient.GetDataServiceFunc().GetCommonRegister(AsgFinTr.Sections.NPF, ntID);
            if(save)
                WebExecuteSave();
        }
    }
}