﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    [ModelEntity(Type = typeof(PensionNotificationWeb), JournalDescription = "Уведомление о назначении НЧТП (Корреспонденция)")]
    public class WebNpfCorrespNotifAssignViewModel: PureWebDialogViewModel
    {
        [JsonIgnore]
        public PensionNotificationWeb PensionNotification { get; }

        [JsonIgnore]
        public List<LegalEntityWeb> NPFList { get; }
        [JsonIgnore]
        public List<ElementWeb> PensionTypeList { get; }

        public long ID
        {
            get { return PensionNotification.ID; }
            set { PensionNotification.ID = value; }
        }

        [DisplayName("Дата поступления")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? DeliveryDate
        {
            get { return PensionNotification.DeliveryDate;}
            set { PensionNotification.DeliveryDate = value; }
        }

        [DisplayName("№ уведомления")]
        [Required]
        public string NotifNum 
        {
            get { return PensionNotification.NotifNum;}
            set { PensionNotification.NotifNum = value; }
        }

        [DisplayName("Дата уведомления")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? NotifDate
        {
            get { return PensionNotification.NotifDate;}
            set { PensionNotification.NotifDate = value; }
        }

        [DisplayName("№ реестра")]
        public string RegisterNum
        {
            get { return PensionNotification.RegisterNum;}
            set { PensionNotification.RegisterNum = value; }
        }

        [DisplayName("Дата реестра")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? RegisterDate
        {
            get { return PensionNotification.RegisterDate; }
            set { PensionNotification.RegisterDate = value; }
        }

        [DisplayName("Отправитель")]
        [Required]
        public long SenderID
        {
            get { return PensionNotification.SenderID; }
            set { PensionNotification.SenderID = value; }
        }

        [DisplayName("Тип уведомления")]
        [Required]
        public long? NotifTypeID
        {
            get { return PensionNotification.NotifTypeID; }
            set { PensionNotification.NotifTypeID = value; }
        }

        public WebNpfCorrespNotifAssignViewModel() : this(0)
        {
        }

        public WebNpfCorrespNotifAssignViewModel(long id)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_CorrespondencePensionNotificationViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = false;

            NPFList = WCFClient.GetEFServiceFunc().LegalEntities.Include(a => a.Contragent).Where(a => a.Contragent.StatusID > 0).ToList();
            PensionTypeList = WCFClient.GetEFServiceFunc().Elements.Where(a => a.Key == (long) ElementWeb.Types.PensionNotificationType && a.Visible == 1).ToList();

            if (id == 0)
            {
                PensionNotification = new PensionNotificationWeb
                {
                    SenderID = NPFList.FirstOrDefault()?.ID ?? 0,
                    NotifTypeID = PensionTypeList.FirstOrDefault()?.ID
                };
                DeliveryDate = NotifDate = DateTime.Now;
            }
            else
            {
                PensionNotification = WCFClient.GetEFServiceFunc().GetEntity<PensionNotificationWeb>(id);
            }

            ValidatableFields = $"{nameof(NotifNum)}|{nameof(SenderID)}|{nameof(NotifTypeID)}|{nameof(DeliveryDate)}|{nameof(NotifDate)}";
        }

        public override bool WebExecuteSave()
        {
            DbHelper.SaveEntity(PensionNotification);
            return true;
        }

        public override bool WebExecuteDelete(long id)
        {
            DbHelper.DeleteEntity(PensionNotification);
            return true;
        }

        public override long WebGetEntryId()
        {
            return PensionNotification?.ID ?? 0;
        }

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case nameof(NotifNum):
                        if (string.IsNullOrEmpty(NotifNum))
                            return "Поле обязательное для заполнения";
                        if (NotifNum.Trim().Length > 100)
                            return "Максимальная длина 100 символов";
                        break;
                    case nameof(SenderID):
                        if (SenderID == 0)
                            return "Выберите отправителя";
                        break;
                    case nameof(NotifTypeID):
                        if (NotifTypeID == null)
                            return "Выберите тип уведомления";
                        break;
                    case nameof(DeliveryDate):
                        if (!DeliveryDate.HasValue || DeliveryDate.Value == DateTime.MinValue)
                            return "Заполните дату поступления";
                        break;
                    case nameof(NotifDate):
                        if (!NotifDate.HasValue || NotifDate.Value == DateTime.MinValue)
                            return "Заполните дату уведомления";
                        break;

                    default: return null;
                }

                return null;
            }
        }
    }
}