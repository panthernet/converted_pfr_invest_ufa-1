﻿using System.Linq;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfSpnRegisterArchiveListViewModel : WebNpfLazyListViewModel<RegistersListItem>
    {
        private IQueryable _query;

        public override IQueryable Query => _query ?? (_query = WCFClient.GetEFServiceFunc()?.GetSpnArchiveList());

        public WebNpfSpnRegisterArchiveListViewModel()
        {
            AjaxMain = "_SpnRegisterArchiveListViewPartial";
            AjaxPartial = "_SpnRegisterArchiveListContainerViewPartial";
            AttachDefaultGridScripts = false;
        }
    }
}