﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(RegisterWeb), JournalDescription = "Реестр СПН")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfSpnRegisterViewModel: PureWebFormViewModel
    {
        [DisplayName("Портфель")]
        public virtual long? PortfolioID { get { return Register.PortfolioID; }
            set
            {
                Register.PortfolioID = value;
                _selectedPortfolio = value.HasValue ? PortfoliosList.FirstOrDefault(a => a.ID == value) : null;
            } }

        [DisplayName("Суммы СПН утвержд. документом")]
        public virtual long? ApproveDocID
        {
            get { return Register.ApproveDocID; }
            set
            {
               // _register.ApproveDocID = value;
                Register.ApproveDoc = ApproveDocsList.FirstOrDefault(a => a.ID == value);
            }
        }

        public void WebRefreshFinregWithNPFList()
        {
            RefreshFinregWithNPFList();
        }

        public WebNpfSpnRegisterViewModel() : this(0) { }

        public WebNpfSpnRegisterViewModel(long entryId) : base(entryId)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnRegisterViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = true;

            _paList = WCFClient.GetDataServiceFunc().GetPayAssignmentsForNpf();
            if (State == ViewModelState.Create)
            {
                Register = new RegisterWeb();
                RegisterKind = Kinds.First();
                _registerContent = Contents.First();
                RegDate = DateTime.Now;
                UpdatePaymentAssignment();
            }
            else
            {
                LoadRegister();
            }

            ApproveDocsList = DbHelper.GetEF().ApproveDocs.ToList();

            if (ApproveDocsList != null && Register != null)
                Register.ApproveDoc =  Register.ApproveDoc ?? ApproveDocsList.FirstOrDefault(appdoc => appdoc.ID == Register.ApproveDocID);
        }

        public void WebUpdatePaymentAssignment()
        {
            UpdatePaymentAssignment();
        }

        #region OLD

        [JsonIgnore]
        protected RegisterWeb Register;

        #region Kind

        [JsonIgnore]
        public virtual List<string> Kinds { get; }= new List<string>
        {
            RegisterIdentifier.PFRtoNPF,
            RegisterIdentifier.NPFtoPFR
        };

        /// <summary>
        /// Видимость назначения платежа (override в дочерних моделях)
        /// </summary>
        [JsonIgnore]
        public virtual bool IsPayAssVisible => true;

        /// <summary>
        /// Назначение платежа
        /// </summary>
        [DisplayName("Назначение платежа")]
        [StringLength(250)]
        public string PayAssignment { get { return Register.PayAssignment; } set { Register.PayAssignment = value; } }

        [Required]
        [DisplayName("Тип операции")]
        public string RegisterKind
        {
            get
            {
                return Register != null ? Register.Kind : "";
            }
            set
            {
                if (Register != null)
                {
                    Register.Kind = value;
                    Contents = value == Kinds.First() ? PFRToNPFContents : NPFToPFRContents;
                    RegisterContent = Contents.First();
                }
            }
        }
        #endregion

        #region Content
        [JsonIgnore]
        public virtual bool IsContentVisible => true;

        //
        [JsonIgnore]
        private static List<string> _PFRToNPFContents;
        [JsonIgnore]
        protected static List<string> PFRToNPFContents
        {
            get
            {
                if (_PFRToNPFContents == null)
                {
                    var contentList = WCFClient.GetDataServiceFunc().GetElementByType(Element.Types.RegisterPFRtoNPF);
                    _PFRToNPFContents = contentList.Where(x => x.Visible).Select(e => e.Name).ToList();
                }
                return _PFRToNPFContents;
            }
        }

        [JsonIgnore]
        private static List<string> _NPFToPFRContents;
        [JsonIgnore]
        protected static List<string> NPFToPFRContents
        {
            get
            {
                if (_NPFToPFRContents == null)
                {
                    var contentList = WCFClient.GetDataServiceFunc().GetElementByType(Element.Types.RegisterNPFtoPFR);
                    _NPFToPFRContents = contentList.Where(x => x.Visible).Select(e => e.Name).ToList();
                }
                return _NPFToPFRContents;
            }
        }
        //{
        //    "Переход ЗЛ ежегодно (75-ФЗ)",
        //    "Материнский семейный капитал (256-ФЗ)",
        //    "Возврат СПН ввиду отсутствия правопреемников (пост. 742)",
        //    "Договоры, не вступившие в силу (Расп. 118)",
        //    "Ликвидация НПФ",
        //    "Возврат СПН по решению суда",
        //    "Другое"
        //};

        [JsonIgnore]
        public List<string> Contents { get; set; }

        private string _registerContent = "";

        [Required]
        [DisplayName("Содержание операции")]
        public string RegisterContent
        {
            get
            {
                return _registerContent;
            }
            set
            {
                if (_registerContent != value)
                {
                    _registerContent = value;
                    //if (value != null) RefreshPortfoliosList();
                    UpdatePaymentAssignment();
                    AfterPropertyChanged();
                }
            }
        }
        #endregion

        protected virtual void AfterPropertyChanged() { }

        #region Company
        [DisplayName("Кампания по приему заявлений")]
        [StringLength(256)]
        public string RegisterCompany
        {
            get
            {
                if (Register == null) return "";
                return !string.IsNullOrWhiteSpace(Register.Company) ? Register.Company : null;
            }

            set
            {
                if (Register != null)
                {
                    Register.Company = value;
                }
            }
        }

        [JsonIgnore]
        public virtual bool IsCompanyRequired => (RegisterContent != "Ликвидация НПФ") && (RegisterContent != "Аннулирование лицензии") && IsCompanyVisible;
        [JsonIgnore]
        public virtual bool IsCompanyVisible => true;

        #endregion

        #region Portfolio
        [JsonIgnore]
        public virtual bool IsPortfolioVisible => false;
        [JsonIgnore]
        public virtual bool IsPortfolioEnabled => false;
        [JsonIgnore]
        public List<Portfolio> PortfoliosList { get; set; }

        private Portfolio _selectedPortfolio;
        public virtual Portfolio SelectedPortfolio
        {
            get
            {
                return _selectedPortfolio;
            }

            set
            {
                _selectedPortfolio = value;
                if (_selectedPortfolio != null)
                    Register.PortfolioID = _selectedPortfolio.ID;
            }
        }
        #endregion

        #region ApproveDoc

        [JsonIgnore]
        public List<ApproveDocWeb> ApproveDocsList { get; set; }

        #endregion

        #region RegNum
        [DisplayName("Номер документа")]
        [StringLength(50)]
        public string RegNum
        {
            get
            {
                if (Register != null && Register.RegNum != null)
                    return Register.RegNum;
                return "";
            }
            set
            {
                if (Register != null)
                {
                    Register.RegNum = value;
                }
            }
        }

        [JsonIgnore]
        private string OldRegNum { get; set; }
        #endregion

        #region RegDate
        [DisplayName("Дата документа")]
        public DateTime? RegDate
        {
            get
            {
                return Register != null ? Register.RegDate : null;
            }
            set
            {
                if (Register != null && Register.RegDate != value)
                {
                    Register.RegDate = value;
                }
            }
        }

        [JsonIgnore]
        private DateTime? OldRegDate { get; set; }
        #endregion

        #region SumSPN

        [DisplayName("Общая сумма СПН")]
        public decimal? SumSPN { get; set; }

        #endregion

        #region SumZL
        [JsonIgnore]
        public virtual bool IsSumZLVisible => true;

        [DisplayName("Общая сумма ЗЛ")]
        public decimal? SumZL { get; set; }

        #endregion

        #region SumCFR
        [JsonIgnore]
        public virtual bool IsSumCFRVisible => false;

        [DisplayName("Общая сумма ЧФР")]
        public decimal? SumCFR { get; set; }

        #endregion

        [JsonIgnore]
        public ERZL ERZL
        {
            get;
            protected set;
        }

        #region ERZL
        [JsonIgnore]
        public virtual bool IsERZLVisible => true;

        [DisplayName("Ссылка на реестр уведомлений")]
        public string ERZLContent
        {
            get
            {
                if (ERZL == null) return "";
                return ERZL.Content == string.Empty ? "Реестр уведомлений" : ERZL.Content;
            }
        }
        #endregion

        #region Comment
        [DisplayName("Комментарий")]
        [StringLength(2500)]
        public string RegisterComment
        {
            get
            {
                return Register != null ? Register.Comment : "";
            }
            set
            {
                if (Register == null) return;
                Register.Comment = value;
            }
        }
        #endregion

        #region Finregisters

        [JsonIgnore]
        protected List<FinregisterWithCorrAndNPFWebListItem> finregWithNPFList;
        [JsonIgnore]
        public List<FinregisterWithCorrAndNPFWebListItem> FinregWithNPFList
        {
            get
            {
                var list = new List<FinregisterWithCorrAndNPFWebListItem>();
                if (finregWithNPFList == null) return list;
                foreach (var item in finregWithNPFList)
                {
                    if (list.Count < 1)
                        list.Add(item);
                    if (list.Any(l => l.CorrCount == item.CorrCount && l.Count == item.Count && l.ID == item.ID && l.NPFName == item.NPFName))
                        continue;
                    list.Add(item);
                }
                return list;
            }

            set
            {
                finregWithNPFList = value;
            }
        }
        #endregion

        #region TrancheDate
        [DisplayName("Дата формирования заявки")]
        public DateTime? RegisterTrancheDate
        {
            get
            {
                if (Register != null && Register.Kind.ToLower().Trim() == Kinds[0].ToLower().Trim())
                    return Register.TrancheDate;
                return null;
            }
            set
            {
                if (Register != null && Register.Kind.ToLower().Trim() == Kinds[0].ToLower().Trim())
                {
                    Register.TrancheDate = value;
                }
            }
        }
        #endregion

        #region GridColumnSettings
        [JsonIgnore]
        public bool IsCorrColumnVisible => RegisterKind != null && Kinds.Count > 1 && string.Equals(RegisterKind.Trim(), Kinds[1], StringComparison.CurrentCultureIgnoreCase);

        [JsonIgnore]
        public virtual bool IsCFRColumnVisible => false;
        [JsonIgnore]
        public virtual bool IsZLColumnEditable => false;
        [JsonIgnore]
        public virtual bool IsCountColumnEditable => false;
        [JsonIgnore]
        public virtual bool IsCFRCountColumnEditable => false;

        #endregion

        [JsonIgnore]
        public virtual bool IsKindEditable => State == ViewModelState.Create;

        [JsonIgnore]
        public bool IsTrancheDateVisible => Register != null && !RegisterIdentifier.IsTempReturn(RegisterKind) && !RegisterIdentifier.IsTempAllocation(RegisterKind) &&
                                            string.Equals(RegisterKind.Trim(), Kinds[0].Trim(), StringComparison.OrdinalIgnoreCase);


        #region Execute Implementations
        public bool CanExecuteDeleteFR()
        {
            return !IsReadOnly() && SelectedFR != null && !DeleteAccessDenied;
        }

        public virtual void ExecuteDeleteFR(long frId)
        {
            if (!CanExecuteDeleteFR())
            {
                WebLastModelError = "Невозможно удалить финреестр!";
                return;
            }
            switch (WCFClient.GetDataServiceFunc().DeleteFinregister(frId, true, "From: " + GetType().Name))
            {
                case WebServiceDataError.FinregisterHasChildren:
                    WebLastModelError = "Невозможно удалить выбранный финреестр, т.к. он находится в статусе \"Финреестр не передан\" и имеет дочерние финреестры!";
                    return;
                case WebServiceDataError.FinregisterHasPP:
                    WebLastModelError = "Невозможно удалить выбранный финреестр, т.к. для него заданы платежные поручения!";
                    return;
            }
            WebRefreshFinregWithNPFList();
        }

        [JsonIgnore]
        public FinregisterWithCorrAndNPFListItem SelectedFR { get; set; }


        public override bool WebExecuteDelete(long id)
        {
            //не выгодно использовать BeforeExecuteDeleteCheck() т.к. исп. тот же список для удаления
            switch (WCFClient.GetDataServiceFunc().DeleteRegister(id))
            {
                case WebServiceDataError.RegisterHasFinregWithInvStatus:
                    WebLastModelError = "Невозможно удалить выбранный реестр, т.к. у него присутствуют финреестры в статусе, отличном от начального!";
                    return false;
                case WebServiceDataError.FinregisterHasPP:
                    WebLastModelError = "Невозможно удалить выбранный реестр, т.к. у него присутствуют финреестры с платежными поручениями!";
                    return false;
            }

            return true;
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            if (finregWithNPFList != null && finregWithNPFList.Any(x => x.Finregister.ZLCount < 0))
            {
                WebLastModelError = "В списке пристуствуют финреестры с отрицательным кол-вом ЗЛ!";
                return false;
            }

            // проверка при inline редактировании СПН для реестра, созданного по изъятию
            if (!(Register.ReturnID > 0)) return base.WebBeforeExecuteSaveCheck();
            var avNPFList = GetAvailableNPFList(Register, false);

            if ((avNPFList == null || avNPFList.Count == 0) && finregWithNPFList != null && finregWithNPFList.Count > 0)
            {
                WebLastModelError = "Нет средств, доступных для изъятия в выбранном портфеле";
                return false;
            }

            foreach (var fr in finregWithNPFList)
            {
                var gr = avNPFList.FirstOrDefault(x => x.CrAccID == fr.Finregister.CrAccID && x.DbtAccID == fr.Finregister.DbtAccID);
                if (gr == null)
                {
                    WebLastModelError = $"Не удается добавить или отредактировать НПФ \"{fr.NPFName}\". Возможно, для изъятия недостаточно средств.";
                    return false;
                }

                if (gr.Count < fr.Count)
                {
                    WebLastModelError = $"Недостаточно средств находится в размещении НПФ \"{gr.NPFName}\" по выбранному портфелю.";
                    return false;
                }

                if (gr.ZLCount < fr.Count)
                {
                    WebLastModelError = $"Недостаточное количество ЗЛ находится в размещении НПФ \"{gr.NPFName}\" по выбранному портфелю.";
                    return false;
                }
            }

            return true;
        }

        protected virtual void LoadFinregFromDB()
        {
            finregWithNPFList = WCFClient.GetEFServiceFunc().GetFinregisterWithCorrAndNPFList(Register.ID);
        }

        /// <summary>
        /// Получение списка доступных для добавления НПФ для данного реестра (используется для расчета доступных средств временного размещения)
        /// </summary>
        /// <param name="excludeExists">true - исключаются НПФ, которые уже привязаны к реестру, false - исключается учет сумм, которые содержатся в БД для НПф по текущему реестру</param>
        /// <returns></returns>
        public static List<FinregisterWebListItem> GetAvailableNPFList(RegisterWeb register, bool excludeExists = false)
        {
            if (register.PortfolioID.HasValue)
            {
                //var allListSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(this.register.PortfolioID.Value, this.Content);
                //var retListSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(this.register.PortfolioID.Value, this.Content);

                var allListSPN = WCFClient.GetEFServiceFunc().GetSPNAllocatedFinregistersList(register.PortfolioID.Value);
                var retListSPN = WCFClient.GetEFServiceFunc().GetSPNReturnedFinregistersList(register.PortfolioID.Value);

                var finregWithNPFList = WCFClient.GetEFServiceFunc().GetFinregisterWithCorrAndNPFList(register.ID);

                if (finregWithNPFList != null && finregWithNPFList.Any())
                {
                    if (!excludeExists)
                    {// удаление из списка вычетов, относящихся к текущему реестру (использование при определении исчерпания суммы)
                        retListSPN.RemoveAll(x => finregWithNPFList.Any(y => y.Finregister.ID > 0 && y.Finregister.ID == x.ID));
                    }
                    else
                    {// удаление из списка доступных НПФ, которые уже есть для текущего реестра
                        allListSPN.RemoveAll(x => finregWithNPFList.Any(y => y.Finregister.CrAccID == x.CrAccID && y.Finregister.DbtAccID == x.DbtAccID));
                    }
                }


                var allSPN = allListSPN.GroupBy(x => x.NPFName);
                var retSPN = retListSPN.GroupBy(x => x.NPFName);


                var avList = new List<FinregisterWebListItem>();

                foreach (var gr in allSPN)
                {
                    var ret = retSPN.FirstOrDefault(x => x.Key == gr.Key);
                    var item = new FinregisterWebListItem
                    {
                        ID = 0,
                        RegisterID = register.ID,
                        RegNum = register.RegNum,
                        Date = register.RegDate,
                        Count = gr.Sum(x => x.Count),
                        CFRCount = gr.Sum(x => x.CFRCount),
                        ZLCount = gr.Sum(x => x.ZLCount)
                    };
                    if (ret != null)
                    {
                        item.Count -= ret.Sum(x => x.Count);
                        item.CFRCount -= ret.Sum(x => x.CFRCount);
                        item.ZLCount -= ret.Sum(x => x.ZLCount);
                    }

                    if (item.Count > 0 || item.ZLCount > 0)
                    {
                        avList.Add(item);
                    }

                    item.CrAccID = gr.First().CrAccID;
                    item.DbtAccID = gr.First().DbtAccID;
                    item.NPFName = gr.First().NPFName;
                    item.Status = RegisterIdentifier.FinregisterStatuses.Created;
                }
                return avList;
            }

            return new List<FinregisterWebListItem>();
        }

        public override bool WebExecuteSave()
        {
            Register.Content = RegisterContent;
            RegNum = RegNum.Trim();

            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    if (Register != null)
                    {
                        DbHelper.SaveEntity(Register);
                        AddMainRecord();
                    }

                    if (finregWithNPFList != null && finregWithNPFList.Count > 0)
                    {
                        RecalcSums();
                        //TODO save finregs
                        if (OldRegNum != Register.RegNum)
                        {
                            finregWithNPFList.ForEach(x => x.Finregister.RegNum = Register.RegNum);
                        }

                        if (OldRegDate != Register.RegDate)
                        {
                            finregWithNPFList.ForEach(x => x.Finregister.Date = Register.RegDate);
                        }

                        if (OldRegNum != Register.RegNum || OldRegDate != Register.RegDate)
                        {
                            WCFClient.GetEFServiceFunc().SaveReturnFinregisters(finregWithNPFList);
                        }
                        else
                        {
                            WCFClient.GetEFServiceFunc().SaveReturnFinregisters(finregWithNPFList.Where(a=> a.IsItemChanged));
                        }
                    }
                    break;
                case ViewModelState.Create:
                    if (Register != null)
                    {
                        DbHelper.SaveEntity(Register);
                        ID = Register.ID;
                        AddMainRecord();
                        SaveReturnFinregisters();
                        OldRegNum = Register.RegNum;
                        OldRegDate = Register.RegDate;
                    }
                    break;
                default:
                    break;
            }

            return true;
        }

        protected virtual void SaveReturnFinregisters()
        {
        }

        protected virtual void AddMainRecord()
        {
        }

        #endregion

        [JsonIgnore]
        private readonly List<PaymentAssignment> _paList;

        protected string UpdatePaymentAssignment()
        {
            if (!RegisterIdentifier.IsToNPF(RegisterKind))
                return PayAssignment = null;

            var pa =
                    _paList.Where(a => a.DirectionID == (long)Element.SpecialDictionaryItems.CommonTransferDirectionFromPFR && ((a.ExtensionData?.ContainsKey("Element") == true ? ((Element)a.ExtensionData["Element"]).Name : "") == _registerContent || a.OperationContentID == 0))
                        .OrderByDescending(a => a.OperationContentID)
                        .FirstOrDefault();
            return PayAssignment = pa != null ? PaymentAssignmentHelper.ReplaceTagDataForNPF(pa.Name, RegDate) : null;
        }

        public void LoadRegister()
        {
            bool isdatachanged = IsDataChanged;
            if (ID > 0)
            {
                Register = DbHelper.Get<RegisterWeb>(ID);
               // Register = DbHelper.GetEF().SpnRegisters.FirstOrDefault(a => a.ID == ID);
                OldRegDate = Register.RegDate;
                OldRegNum = Register.RegNum;

                if (Register != null)
                {
                    var tmpPay = Register.PayAssignment;
                    RefreshFinregWithNPFList();
                    ERZL = DataContainerFacade.GetByID<ERZL>(Register.ERZL_ID);


                    if (Register.Kind != null)
                        RegisterKind = Kinds.SingleOrDefault(x => string.Equals(x, Register.Kind.Trim(), StringComparison.InvariantCultureIgnoreCase));

                    if (Register.PortfolioID != null && PortfoliosList != null)
                        SelectedPortfolio = PortfoliosList.SingleOrDefault(x => x.ID == Register.PortfolioID.Value);

                    RegisterContent = Register.Content;
                    Register.PayAssignment = tmpPay;

                    if (!string.IsNullOrWhiteSpace(RegisterContent) && !Contents.Contains(RegisterContent))
                    {
                        Contents.Add(RegisterContent);
                    }
                }
            }
            IsDataChanged = isdatachanged;
        }

        protected virtual List<FinregisterWithCorrAndNPFWebListItem> RefreshFinregWithNPFList()
        {
            if (Register != null)
            {
                try
                {
                    //
                    LoadFinregFromDB();
                    RecalcSums();
                }
                catch(Exception ex)
                {
                    finregWithNPFList = new List<FinregisterWithCorrAndNPFWebListItem>();
                }
            }

            return finregWithNPFList;
        }

        protected void RecalcSums()
        {
            if (finregWithNPFList == null) return;
            decimal totalsumm = 0;
            long totalzl = 0;
            decimal totalcfr = 0;
            if (Register.ID != 0)
                foreach (var item in finregWithNPFList.GroupBy(x => x.ID))
                {
                    if (item.First().Count.HasValue || item.First().CorrCount.HasValue)
                        totalsumm += item.First().CorrCount ?? item.First().Count.Value;
                    if (item.First().ZLCount.HasValue)
                        totalzl += item.First().ZLCount.Value;
                    if (item.First().CFRCount.HasValue)
                        totalcfr += item.First().CFRCount.Value;
                }
            else
            {
                totalsumm = finregWithNPFList.Where(x => x.CorrCount.HasValue || x.Count.HasValue).Sum(x => x.CorrCount.HasValue ? x.CorrCount.Value : x.Count.Value);
                totalzl = finregWithNPFList.Where(x => x.ZLCount.HasValue).Sum(x => x.ZLCount.Value);
                totalcfr = finregWithNPFList.Where(x => x.CFRCount.HasValue).Sum(x => x.CFRCount.Value);
            }

            SumSPN = totalsumm;
            SumZL = totalzl;
            SumCFR = totalcfr;
        }

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                switch (columnName.ToLower())
                {
                    case "kind": return RegisterKind.ValidateRequired();
                    case "_content": return RegisterContent.ValidateRequired();
                    case "company":
                        var result = "";
                        if (IsCompanyRequired)
                            result = RegisterCompany.ValidateRequired();
                        if (string.IsNullOrEmpty(result))
                            result = RegisterCompany.ValidateMaxLength(256);
                        return result;
                    case "regnum": return RegNum.ValidateMaxLength(50);
                    case "comment": return RegisterComment.ValidateMaxLength(2500);
                    case "payassignment":
                        return PayAssignment.ValidateMaxLength(250);
                    default: return null;
                }
            }
        }

        public static IEnumerable<FinregisterWithCorrAndNPFListItem> WebGetFinregWithNPFList(long id)
        {
            return WCFClient.GetDataServiceFunc().GetFinregisterWithCorrAndNPFList(id);
        }
        #endregion
    }
}