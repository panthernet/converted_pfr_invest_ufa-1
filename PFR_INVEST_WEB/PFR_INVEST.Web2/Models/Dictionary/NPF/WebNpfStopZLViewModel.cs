﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Models;


namespace PFR_INVEST.Web2.Models
{
    public class WebNpfStopZLViewModel : PureWebViewModel
    {
        [JsonIgnore]
        private readonly NPFPause _pause = new NPFPause {DocumentDate = DateTime.Now, StartDate = DateTime.Now, EndDate = DateTime.Now};

        public long LegalEntityID { get; set; }

        public long? ContragentID
        {
            get { return _pause.ContragentID; }
            set { _pause.ContragentID = value; }
        }

	    [Display(Name = "Дата документа")]
	    [DataType(DataType.Date)]
        [Required]
        public DateTime? DocumentDate
        {
            get { return _pause.DocumentDate; }
            set { _pause.DocumentDate = value; }
        }

        [Display(Name = "Дата начала приостановления деятельности")]
	    [DataType(DataType.Date)]
        [Required]
        public DateTime? StartDate
        {
            get { return _pause.StartDate; }
            set { _pause.StartDate = value; }
        }

        [Display(Name = "Дата конца приостановления деятельности")]
	    [DataType(DataType.Date)]
        [Required]
        public DateTime? EndDate
        {
            get { return _pause.EndDate; }
            set { _pause.EndDate = value; }
        }

        [Display(Name = "Номер документа")]
        [Required]
        [StringLength(12, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string RegNum
        {
            get { return _pause.RegNum; }
            set { _pause.RegNum = value; }
        }

        public WebNpfStopZLViewModel(): this(0) { }

        public WebNpfStopZLViewModel(long leId)
        {
            LegalEntityID = leId;
            WebPostModelCreateAction();

            //связь со вьюхой
            WebModelHelperEx.InitializeDefault(this, "Dictionary", "_NpfStopZLViewPartial");
            WebModelHelperEx.UpdateModelState(this);
        }

        public override long WebGetEntryId()
        {
            return _pause.ID;
        }

        public override void WebPostModelCreateAction()
        { 
            if(LegalEntityID > 0)
                ContragentID = ContragentID ?? DataContainerFacade.GetByID<LegalEntity>(LegalEntityID).ContragentID;
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            if (StartDate > EndDate)
                WebLastModelError = "Дата начала должна быть меньше даты конца!";

            return StartDate != null && EndDate != null && DocumentDate != null && StartDate <= EndDate && !string.IsNullOrEmpty(RegNum);
        }

        public override bool WebExecuteSave()
        {
            DataContainerFacade.Save(_pause);
            JournalLogger.LogModelEvent(this, JournalEventType.CHANGE_STATE, $"Контрагент ID:{_pause.ContragentID} Номер документа:{_pause.RegNum}", "Приостановление деятельности");
            return true;
        }
    }
}