﻿using System;
using System.ComponentModel.DataAnnotations;
using PFR_INVEST.Web2.BusinessLogic;
using WebViewModel = PFR_INVEST.Web2.BusinessLogic.Models.WebViewModel;

namespace PFR_INVEST.Web2.Models
{
    public class WebDlgAnnulDateViewModel : WebViewModel
    {
        [Display(Name = "Дата аннулирования лицензии")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime DateAnnul { get; set; } = DateTime.Today;

        public WebDlgAnnulDateViewModel()
        {
            //связь со вьюхой
            AjaxMain = "_SharedAjaxFormSimplePartialView";
            AjaxController = "Dictionary";
            AjaxAction = "DlgAnnulDate";
            AjaxPartial = "_DlgAnnulDateViewPartial";
            FormId = "dlgAnnulDate";
            WebTitle = FormDataHelper.Data[FormId].DisplayName;
        }

        public override long WebGetEntryId()
        {
            return 0;
        }
    }
}