﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfAccountsListViewModel : WebListViewModel<BankAccount>
    {
        public WebNpfAccountsListViewModel() : base(ListProvider.GetNPFAccounts())
        {
            AjaxMain = "_NpfListAccountsViewPartial";
            AjaxPartial = "_NpfListAccountsContainerViewPartial";
            AjaxController = "Dictionary";
        }
    }
}