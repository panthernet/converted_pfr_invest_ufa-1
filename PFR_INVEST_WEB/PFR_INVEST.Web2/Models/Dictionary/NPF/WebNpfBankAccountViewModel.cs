﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;


namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(BankAccount), JournalDescription = "Банковский Счет")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class WebNpfBankAccountViewModel: PureWebFormViewModel
    {
        public long? PfrBranchID
        {
            get { return BankAccount?.PfrBranchID; }
            set { BankAccount.PfrBranchID = value; }
        }

        public long? LegalEntityID
        {
            get { return BankAccount?.LegalEntityID; }
            set
            {
                if (LegalEntity == null)
                    LegalEntity = DataContainerFacade.GetByID<LegalEntity>(value);
                BankAccount.LegalEntityID = value;
            }
        }     

        public WebNpfBankAccountViewModel(): this(0, 0) { }
        public WebNpfBankAccountViewModel(long id) : this(id, 0) { }

        public WebNpfBankAccountViewModel(long id, long parentId = 0) 
            : base(id)
        {
            WebModelHelperEx.InitializeDefault(this,id);
            WebModelHelperEx.UpdateModelState(this);
            AjaxController = "Dictionary";
            AjaxPartial = "_NpfBankAccountViewPartial";

            var myId = id ==0 ? parentId : id;

            if (State == ViewModelState.Create)
            {
                LegalEntity = DataContainerFacade.GetByID<LegalEntity, long>(myId);
                BankAccount = new BankAccount();
            }
            else
            {
                if (id > 0)
                {
                    BankAccount = DataContainerFacade.GetByID<BankAccount, long>(myId);
                    LegalEntity = DataContainerFacade.GetByID<LegalEntity, long>(BankAccount.LegalEntityID);
                }
            }
        }


        [JsonIgnore]
        public BankAccount BankAccount { get; }

        [JsonIgnore]
		public bool ReadAccessOnly => false;

        [JsonIgnore]
        public LegalEntity LegalEntity { get; set; }

        [DisplayName("Наименование получателя платежа")]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [Required]
		public string BankAccountLegalEntityName
		{
			get { return BankAccount.LegalEntityName; }
			set
			{
				BankAccount.LegalEntityName = value;
			}
		}

	    [DisplayName("Номер рассчетного счета получателя платежа")]
	    [StringLength(40, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountAccountNumber
		{
			get { return BankAccount.AccountNumber; }
			set
			{
				BankAccount.AccountNumber = value?.Replace(" ", "");
			}
		}

	    [DisplayName("Банк получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountBankName
		{
			get
			{
				return BankAccount.BankName;
			}
			set
			{
				BankAccount.BankName = value;
			}
		}

	    [DisplayName("Местонахождение банка получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountBankLocation
		{
			get { return BankAccount.BankLocation; }
			set
			{
				BankAccount.BankLocation = value;
			}
		}

	    [DisplayName("Местонахождение банка получателя платежа")]
	    [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountCorrespondentAccountNumber
		{
			get { return BankAccount.CorrespondentAccountNumber; }
			set
			{
				BankAccount.CorrespondentAccountNumber = value?.Replace(" ", "");
			}
		}

	    [DisplayName("ГРКЦ")]
	    [StringLength(256, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BankAccountCorrespondentAccountNote
		{
			get { return BankAccount.CorrespondentAccountNote; }
			set
			{
				BankAccount.CorrespondentAccountNote = value;
			}
		}

	    [DisplayName("БИК банка получателя платежа")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountBIK
		{
			get
			{
				return BankAccount.BIK?.Trim();
			}
			set
			{
				BankAccount.BIK = value;
			}
		}

	    [DisplayName("ИНН банка получателя платежа")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountINN
		{
			get
			{
				return BankAccount.INN?.Trim();
			}
			set
			{
				BankAccount.INN = value;
			}
		}

	    [DisplayName("КПП банка получателя платежа")]
	    [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
	    [Required]
        public string BankAccountKPP
		{
			get
			{
				return BankAccount.KPP?.Trim();
			}
			set
			{
				BankAccount.KPP = value;
			}
		}

		public override bool WebExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
				    if (State == ViewModelState.Create)
				        BankAccount.LegalEntityID = LegalEntity.ID;
				    BankAccount.ID = ID = WCFClient.GetDataServiceFunc().SaveBankAccount(BankAccount);					
					break;
			}
			IsDataChanged = false;
		    return true;
		}

		private const string INVALID_BA_LE_NAME = "Не введено наименование получателя платежа";
		private const string INVALID_BA_ACCOUNT_NUMBER = "Неверно введен номер рассчетного счета получателя платежа";
		private const string INVALID_BA_BANK_NAME = "Не введен банк получателя платежа";
		private const string INVALID_BA_BANK_LOCATION = "Не введено местонахождение банка получателя платежа";
		private const string INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER = "Неверно введен корреспондентский счет банка получателя платежа";
		private const string INVALID_BA_BIK = "Неверно введен БИК банка получателя платежа";
		private const string INVALID_BA_INN = "Неверно введен ИНН банка получателя платежа";
		private const string INVALID_BA_KPP = "Неверно введен КПП банка получателя платежа";

		private const string REQUIRED_FIELD_WARNING = "Поле обязательно для заполнения";
		//private const string AccountNumberPresentationMask = @"\d{1} \d{2} \d{2} \d{3} \d{1} \d{4} \d{3} \d{4}";
		private const string ACCOUNT_NUMBER_STORAGE_MASK = @"\d{20}";

        [JsonIgnore]
		public override string this[string columnName]
		{
			get
			{
				Regex regex;
				switch (columnName)
				{
					case "BankAccountLegalEntityName":
						return string.IsNullOrWhiteSpace(BankAccountLegalEntityName) ? INVALID_BA_LE_NAME : null;

					case "BankAccountAccountNumber":
						if (BankAccountAccountNumber == null)
							return REQUIRED_FIELD_WARNING;

						regex = new Regex(ACCOUNT_NUMBER_STORAGE_MASK);
						return regex.IsMatch(BankAccountAccountNumber)
							? null
							: INVALID_BA_ACCOUNT_NUMBER;

					case "BankAccountBankName":
						return string.IsNullOrWhiteSpace(BankAccountBankName) ? INVALID_BA_BANK_NAME : null;

					case "BankAccountBankLocation":
						return string.IsNullOrWhiteSpace(BankAccountBankLocation) ? INVALID_BA_BANK_LOCATION : null;

					case "BankAccountCorrespondentAccountNumber":
						if (BankAccountCorrespondentAccountNumber == null)
							return REQUIRED_FIELD_WARNING;

						regex = new Regex(ACCOUNT_NUMBER_STORAGE_MASK);
						return regex.IsMatch(BankAccountCorrespondentAccountNumber)
							? null
							: INVALID_BA_COORESPONDENT_ACCOUNT_NUMBER;

					case "BankAccountBIK":
						return BankAccountBIK == null || BankAccountBIK.Length != 9 ? INVALID_BA_BIK : null;

					case "BankAccountINN":
						return BankAccountINN == null || BankAccountINN.Length != 10 ? INVALID_BA_INN : null;

					case "BankAccountKPP":
						return BankAccountKPP == null || BankAccountKPP.Length != 9 ? INVALID_BA_KPP : null;

					default:
						return null;
				}
			}
		}

		public override string Validate()
		{
			return
				string.IsNullOrEmpty(this["BankAccountLegalEntityName"]) &&
				string.IsNullOrEmpty(this["BankAccountAccountNumber"]) &&
				string.IsNullOrEmpty(this["BankAccountBankName"]) &&
				string.IsNullOrEmpty(this["BankAccountBankLocation"]) &&
				string.IsNullOrEmpty(this["BankAccountCorrespondentAccountNumber"]) &&
				string.IsNullOrEmpty(this["BankAccountBIK"]) &&
				string.IsNullOrEmpty(this["BankAccountINN"]) &&
				string.IsNullOrEmpty(this["BankAccountKPP"]) ? null : "Требуется обязательное значение!";
		}
    }
}