﻿using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Web2.Models
{
    public class WebVRContractListViewModel : WebListViewModel<SIContractListItem>
    {
        public WebVRContractListViewModel() : base(ListProvider.GetVrContractList())
        {
            AjaxMain = "~/Views/Menu/Dictionary/SI/_VrContractListViewPartial.cshtml";
            AjaxPartial = "~/Views/Menu/Dictionary/SI/_VrContractListContainerViewPartial.cshtml";
            AjaxController = "Dictionary";
        }
    }
}