﻿using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Web2.Models
{
    public class WebSIContractListViewModel : WebListViewModel<SIContractListItem>
    {
        public WebSIContractListViewModel() : base(ListProvider.GetSiContractList())
        {
            AjaxMain = "~/Views/Menu/Dictionary/SI/_SiContractListViewPartial.cshtml";
            AjaxPartial = "~/Views/Menu/Dictionary/SI/_SiContractListContainerViewPartial.cshtml";
            AjaxController = "Dictionary";
        }
    }
}