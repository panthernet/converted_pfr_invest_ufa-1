﻿using System;
using System.Web.Mvc;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Models;
using PFR_INVEST.Web2.Models;

namespace PFR_INVEST.Web2.Controllers
{
    [SessionTimeout]
    public class DictionaryController : Controller, IPublicController
    {
        // GET: Dictionary
        public ActionResult Index()
        {
            //ВАЖНО УКАЗАТЬ ЭТО ВО ВСЕХ КОНТРОЛЛЕРАХ
            ControllerHelper.ViewEngineCollection = ViewEngineCollection;
            ControllerHelper.SetupMenuAndRoles(ViewData);
            ViewData["Category"] = "Dictionary";
            return View("Index");
        }

        #region Callback Trash



        [HttpPost]
        public ActionResult DicNpfBranch(WebNpfBranchViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult DicNpfStopZL(WebNpfStopZLViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult DicNpfCreateReorg([ModelBinder(typeof(CustomBinder))] WebNpfReorgViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }


        [HttpPost]
        public ActionResult DicNpfCreateAccount([ModelBinder(typeof(CustomBinder))] WebNpfBankAccountViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }        

        [HttpPost]
        public ActionResult DlgDeletedCouriersList(WebDlgDeletedCouriersListViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult DicNpfCreateNpfCourier(WebNpfCourierViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }     

        [HttpPost]
        public ActionResult DicNpfCreateNpfContact(WebNpfContactViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult DlgAnnulDate(WebDlgAnnulDateViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }


        [HttpPost]
        public ActionResult DlgEditIdentifier(WebDlgNpfIdentifierViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult DicNpfCreateErrorCode([ModelBinder(typeof(CustomBinder))] WebNpfErrorCodeViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult DicNpfCreateAgrCode([ModelBinder(typeof(CustomBinder))] WebNpfDockCodeViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult DicNpfCreateNpf([ModelBinder(typeof(CustomBinder))] WebNpfViewModel model)
        {            
            if (model.WebDoChiefRetire && model.HeadRetireDate.HasValue)
            {
                model.WebDoChiefRetire = false;
                model.DoRetireChief(model.HeadRetireDate.Value);
                return GetPostView(model.AjaxMain, model, model.AjaxPartial, false);
            }

            return GetPostView(model.AjaxMain, model, model.AjaxPartial); 
        }
        #endregion

        #region Callbacks - GridView

        [HttpPost]
        public ActionResult NpfBankAccountsGridCallback(string leID)
        {
            return PartialView("_NpfCreateNpfBankGridViewPartial",
                NPFViewModel.WebGetOldBankAccounts(string.IsNullOrEmpty(leID) ? 0 : long.Parse(leID)));
        }
        
        [HttpPost]
        public ActionResult NpfCouriersGridCallback(string leID)
        {
            var delId = !string.IsNullOrEmpty(Request.Params["DeleteID"]) ? long.Parse(Request.Params["DeleteID"].Split('|')[1]) : 0L;
            if (delId > 0)
                NPFViewModel.WebDeleteCourier(delId);

            return PartialView("_NpfCreateNpfCourierGridViewPartial",
                NPFViewModel.GetCouriersList(string.IsNullOrEmpty(leID) ? 0 : long.Parse(leID), true));
        }

        [HttpPost]
        public ActionResult NpfCourierLoaGridCallback(string leID)
        {
            var delId = !string.IsNullOrEmpty(Request.Params["DeleteID"]) ? long.Parse(Request.Params["DeleteID"].Split('|')[1]) : 0L;
            if (delId > 0)
                WebNpfCourierViewModel.WebDeleteCourierLoa(delId);

            return PartialView("_NpfCourierLoaGridViewPartial",
                WebNpfCourierViewModel.GetCourierLoaList(string.IsNullOrEmpty(leID) ? 0 : long.Parse(leID)));
        }

        [HttpPost]
        public ActionResult NpfIdentifiersGridCallback(string leID)
        {
            var delId = !string.IsNullOrEmpty(Request.Params["DeleteID"]) ? long.Parse(Request.Params["DeleteID"].Split('|')[1]) : 0L;
            if (delId > 0)
                NPFViewModel.WebDeleteIdentifier(delId);

            return PartialView("_NpfCreateNpfIdentifiersGridViewPartial",
                NPFViewModel.GetIdentifiersList(string.IsNullOrEmpty(leID) ? 0 : long.Parse(leID), true));
        }

        [HttpPost]
        public ActionResult NpfContactsGridCallback(string leID)
        {
            var delId = !string.IsNullOrEmpty(Request.Params["DeleteID"]) ? long.Parse(Request.Params["DeleteID"].Split('|')[1]) : 0L;
            if (delId > 0)
                NPFViewModel.WebDeleteContact(delId);

            return PartialView("_NpfCreateNpfContactsGridViewPartial",
                NPFViewModel.GetContactsList(string.IsNullOrEmpty(leID) ? 0 : long.Parse(leID)));
        }
        #endregion

        #region Extension methods

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadViewPartialBind()
        {
            return ControllerHelper.LoadViewPartialBind(this, HttpContext.User.Identity.Name);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadViewPartialBindPopup(string id, string entryId, string layerstr = null, string parentEntryId = null, string parentId = null, string category = "Dictionary")
        {
            return ControllerHelper.LoadViewPartialBindPopup(this, HttpContext.User.Identity.Name, id, entryId, layerstr, parentEntryId, parentId, category);
        }


        /// <summary>
        /// Загружает представление по ID и возвращает в виде json 
        /// </summary>
        /// <param name="id">Идентификатор представления</param>
        /// <param name="entryId">Идентификатор записи</param>
        /// <param name="category"></param>
        [HttpPost]
        [ValidateInput(false)]
        [Obsolete]
        public JsonResult LoadViewPartial(string id, string entryId = null, string category = "Dictionary")
        {
            return ControllerHelper.LoadViewPartial(this, HttpContext.User.Identity.Name, id, entryId, category);
        }

        /// <summary>
        /// Возвращает нужный View после получения данных от клиента, произыодить сохранение, если требуется
        /// </summary>
        /// <param name="view">Текстовое наименование представления</param>
        /// <param name="model">Модель данных</param>
        /// <param name="partialView">Доп. текстовое наименование частичного представления</param>
        /// <returns></returns>
        private ActionResult GetPostView<T>(string view, T model, string partialView = null, bool save = true)
            where T: class, IWebViewModel
        {
            return ControllerHelper.GetPostView(this, HttpContext.User.Identity.Name, view, model, partialView, save);
        }

        public new RedirectToRouteResult RedirectToAction(string action, string controller)
        {
            return base.RedirectToAction(action, controller);
        }

        public ActionResult ViewPublic(string action, string controller)
        {
            return View(action, controller);
        }
        #endregion
    }
}