﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Binders;
using PFR_INVEST.Web2.BusinessLogic.Models;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;
using PFR_INVEST.Web2.Models;

namespace PFR_INVEST.Web2.Controllers
{
    public class NpfController : Controller, IPublicController
    {
        // GET: Npf
        public ActionResult Index()
        {
            //ВАЖНО УКАЗАТЬ ЭТО ВО ВСЕХ КОНТРОЛЛЕРАХ
            ControllerHelper.ViewEngineCollection = ViewEngineCollection;
            ControllerHelper.SetupMenuAndRoles(ViewData);
            ViewData["Category"] = "Npf";
            ViewData["CategoryFull"] = "Npf";
            return View("Index");
        }


        #region Data callbacks

        [HttpPost]
        public string GetSelectRegisterParams(string[] id)
        {
            var register = DbHelper.Get<RegisterWeb>(Convert.ToInt64(id[0]));
            var kind = (long)(RegisterIdentifier.IsFromNPF(register.Kind) ? AsgFinTr.Directions.ToPFR : AsgFinTr.Directions.FromPFR);
            var content = register.Content;
            return $"{kind}|{content}";
        }
        #endregion

        public ActionResult CorrespUploadFile() {
            return View("_CorrespondenceItemAddTabViewPartial");
        }

        #region Download corresp
        
        [HttpPost]
        public ActionResult DownloadCorrespPreload(long id)
        {          
            var dfb = WCFClient.GetEFServiceFunc().DocFileBodies.FirstOrDefault(a => a.DocumentID == id);
            if (dfb == null) return new JsonResult { Data = new { Id = 0 }};
            var data = WCFClient.GetEFServiceFunc().FileContents.FirstOrDefault(a => a.ID == dfb.FileContentId)?.FileContentData;
            if(data == null) return new JsonResult { Data = new { Id = 0 }};
            TempData[dfb.FileName] = data;

            return Json(new {Filename = dfb.FileName}, JsonRequestBehavior.DenyGet);

            //return new JsonResult { Data = new { Filename = dfb.FileName }, ContentType = "json", JsonRequestBehavior = JsonRequestBehavior.DenyGet};
        }

        public ActionResult DownloadCorresp(string filename)
        {
            if (string.IsNullOrEmpty(filename)) return new EmptyResult();
            filename = HttpUtility.UrlDecode(filename);
            if(TempData[filename] != null){
                var data = TempData[filename] as byte[];
                return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
            }

            // Problem - Log the error, generate a blank file,
            //           redirect to another controller action - whatever fits with your application
            return new EmptyResult();
        }
        #endregion

        public ActionResult CorrespUploadFileExact([ModelBinder(typeof(CorrespFileUploadBinder))]IEnumerable<UploadedFile> ucMultiSelection) 
        {
            return null;
        }

        public ActionResult CorrespUploadLinkedFile() {
            return View("_CorrespondenceItemAddTabViewPartial");
        }

        public ActionResult CorrespUploadLinkedFileExact([ModelBinder(typeof(CorrespFileUploadBinder))]IEnumerable<UploadedFile> ucMultiSelectionLinked) {
            return null;
        }

        #region Callback Trash

        public ActionResult NpfRedistZlNoticeItem([ModelBinder(typeof(CustomBinder))] WebNpfRedistZlNoticeViewModel model)
        {
            var isSave = true;
            //update LE                        
            if (model.AjaxRequestParamValue == 1)
            {
                model.SelectNpf(model.SelectedNPFID, true);
                isSave = false;
            }

            model.AjaxRequestParamValue = 0;

            return GetPostView(model.AjaxMain, model, model.AjaxPartial, isSave);
        }

        public ActionResult NpfCorrespNotifAssignItem([ModelBinder(typeof(CustomBinder))]WebNpfCorrespNotifAssignViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfCorrespDelayedPaymentItem([ModelBinder(typeof(CustomBinder))]WebNpfCorrespDelayedPaymentViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfCorrespItem([ModelBinder(typeof(CustomBinder))]WebNpfCorrespViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfCorrespLinkedItem([ModelBinder(typeof(CustomBinder))]WebNpfCorrespLinkedItemViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfSpnSelectPPItem(WebNpfSpnSelectPPViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfTempDeallocItem([ModelBinder(typeof(CustomBinder))] WebNpfTempDeallocViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfTempAllocItem([ModelBinder(typeof(CustomBinder))] WebNpfTempAllocViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfSpnStatusNotTrItem([ModelBinder(typeof(CustomBinder))] WebNpfSpnStatusNotTrViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfSpnStatusFixedItem([ModelBinder(typeof(CustomBinder))] WebNpfSpnStatusFixedViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfSpnStatusRefineItem([ModelBinder(typeof(CustomBinder))] WebNpfSpnStatusRefineViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }
        
        public ActionResult NpfSpnStatusGiveItem([ModelBinder(typeof(CustomBinder))] WebNpfSpnStatusGiveViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfSpnStatusRollbackItem([ModelBinder(typeof(CustomBinder))] WebNpfSpnStatusRollBackViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfSpnStatusReqItem([ModelBinder(typeof(CustomBinder))] WebNpfSpnStatusReqViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        public ActionResult NpfSpnCreateOrder([ModelBinder(typeof(CustomBinder))] WebNpfSpnCreateOrderViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult NpfSpnFinregisterItem([ModelBinder(typeof(CustomBinder))] WebNpfSpnFinregisterViewModel2 model, string prm = null)
        {
            var isSaveEnabled = false;//model.AjaxRequestParamValue == 0;
            ViewData["TabIndex"] = 0;

            //случай с выбором реестра для статуса НЕ ПЕРЕДАН
            var pName = "WebNpfSelRegId" + User.Identity.Name;
            var nt_id = Session[pName] == null ? 0 : Convert.ToInt64(Session[pName]);
            if (nt_id > 0)
            {
                ViewData["TabIndex"] = 1;
                Session[pName] = null;
                model.WebUpdateNtRegisterSelection(nt_id);
                return GetPostView(model.AjaxMain, model, model.AjaxPartial, false);
            }
            switch (model.AjaxRequestParamValue)
            {
                case 1: //состояние
                    break;
                case 2: //гарантирование
                    break;
                case 3: //нпф
                    break;
                case 4: //NT refresh select content
                    ViewData["TabIndex"] = 1;
                    break;
                default:
                    isSaveEnabled = true;
                    break;
            }
            model.AjaxRequestParamValue = 0;
            return GetPostView(model.AjaxMain, model, model.AjaxPartial, isSaveEnabled);
        }

        [HttpPost]
        public ActionResult NpfSpnSelectRegister([ModelBinder(typeof(CustomBinder))] WebNpfSpnSelectRegisterViewModel model)
        {
            Session["WebNpfSelRegId"+User.Identity.Name] = model.SelectedRegisterID;
            //ViewData["WebNpfSelRegId"] = model.SelectedRegisterID;
            return GetPostView(model.AjaxMain, model, model.AjaxPartial, true);
        }

        [HttpPost]
        public ActionResult NpfSpnRegisterItem ([ModelBinder(typeof(CustomBinder))] WebNpfSpnRegisterViewModel model, string prm = null)
        {
            var isSaveEnabled = model.AjaxRequestParamValue == 0;
            if (model.AjaxRequestParamValue == 1) //содержание
                model.WebUpdatePaymentAssignment();
            if (model.AjaxRequestParamValue == 2) //тип
            {
                //обновляем содержание, т.к. сменился тип
                model.RegisterContent = model.Contents.FirstOrDefault();
            }
            model.AjaxRequestParamValue = 0;
            return GetPostView(model.AjaxMain, model, model.AjaxPartial, isSaveEnabled);
        }

        [HttpPost]
        public ActionResult NpfRedistGenerateTransferPanel([ModelBinder(typeof(CustomBinder))]WebNpfRedistGenerateTransferViewModel model)
        {
           // System.Threading.Thread.Sleep(5000);
            model.Execute();

            return PartialView("_RzlGenerateTransfersViewPanelPartial", model);
        }

        [HttpPost]
        public ActionResult NpfRedistZlItem([ModelBinder(typeof(CustomBinder))]WebNpfRedistZlViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult NpfDzlEditItem([ModelBinder(typeof(CustomBinder))]WebNpfDeadZlViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        [HttpPost]
        public ActionResult NpfDzlItem([ModelBinder(typeof(CustomBinder))]WebNpfDeadZLAddViewModel model)
        {
            return GetPostView(model.AjaxMain, model, model.AjaxPartial);
        }

        #endregion

        #region Callback - Grids

        [HttpPost]
        public ActionResult CorrespItemLinksGridViewCallback(object prm, long documentID)
        {
            ControllerHelper.UpdateViewDataOnCallback(this);
            var delId = !string.IsNullOrEmpty(Request.Params["DeleteID"]) ? long.Parse(Request.Params["DeleteID"]) : 0L;
            if (delId > 0)
                WebNpfCorrespLinkedItemViewModel.DeleteLinkedDocument(delId);

            return PartialView("_CorrespondenceItemLinksTabGridViewPartial", new object[] { prm, WCFClient.GetEFServiceFunc().GetLinkedDocumentsList(documentID).Select(a => new LinkedDocumentWebItem(a)).ToList(), documentID } );
        }

        [HttpPost]
        public ActionResult NpfSpnSelectPPItemGridView()
        {
            return PartialView("_SpnSelectPPContainerViewPartial", new WebNpfSpnSelectPPViewModel(Convert.ToInt64(Request.Params["entityId"])));
        }

       /* [HttpPost]
        public ActionResult NpfSpnFinregisterItemGridViewCallback(long entryid)
        {
            ControllerHelper.UpdateViewDataOnCallback(this);
            return PartialView("_SpnRegisterViewGridPartial", (object)m.NT_NpfList);
            
        }*/

        public ActionResult NtFinregisterGridViewCallback(long entryid)
        {
            ControllerHelper.UpdateViewDataOnCallback(this);
            int.TryParse(Request.Params["DeleteID"], out var delId);
            var m = new WebNpfSpnFinregisterViewModel2(entryid, 0);
            if (delId > 0)
            {
                m.ExecuteDeleteNpfCommand(delId);
            }
            return PartialView("_SpnRegisterViewGridPartial", (object)m.NT_NpfList);
        }

        [HttpPost]
        public ActionResult NpfSpnRegisterItemGridViewCallback(long entryid)
        {
            ControllerHelper.UpdateViewDataOnCallback(this);

            int delId = 0;
            int.TryParse(Request.Params["DeleteID"], out delId);
            var m = new WebNpfSpnRegisterViewModel(entryid);
            if (delId > 0)
            {
                m.ExecuteDeleteFR(delId);
            }
            return PartialView("_SpnRegisterViewGridPartial", new object[] { m.FinregWithNPFList, m.IsZLColumnEditable, m.IsCountColumnEditable, m.IsCFRColumnVisible, m.IsCFRCountColumnEditable, m.IsCorrColumnVisible});
        }

        [HttpPost]
        public ActionResult NoticesGridViewCallback()
        {
            ControllerHelper.UpdateViewDataOnCallback(this);
            return PartialView("_RzlItemGridViewPartial", WebNpfRedistZlNoticeViewModel.GetNotices(long.Parse(Request.Params["EntryID"])));
        }



        [HttpPost]
        public ActionResult NpfRedistZlList(WebNpfRedistZlListViewModel model)
        {
            return PartialView(model.AjaxPartial, ListProvider.GetZlRedistList());
        }

        [HttpPost]
        public ActionResult DzlAddGridViewCallback()
        {
            DateTime date;
            DateTime.TryParse((string)Request.Params["DATE"], out date);
            return PartialView("_DzlAddGridViewPartial", new object[] { date.ToShortDateString(), DeadZLAddViewModel.WebRefreshList(date).Cast<object>().ToArray() });
        }

        [ValidateInput(false)]
        public ActionResult DzlAddGridViewUpdateCallback([ModelBinder(typeof(DevExpressEditorsBinder))]MVCxGridViewBatchUpdateValues<NPFforERZLNotifyListItem, int> updateValues)
        {
            DateTime date;
            DateTime.TryParse(Request.Params["DATE"], out date);

            var list = DeadZLAddViewModel.WebRefreshList(date);
            foreach (var product in updateValues.Update)
            {
                if (updateValues.IsValid(product))
                {
                    var item = list.FirstOrDefault(a => a.ID == product.ID);
                    if (item != null)
                        item.Count = product.Count;
                }
                    
            }

            var model = new WebNpfDeadZLAddViewModel
            {
                WebDate = date,
                ContragentsList = new BindingList<NPFforERZLNotifyListItem>()
            };
            model.WebExecuteSaveX();

            return PartialView("_DzlAddGridViewPartial", new object[]{ date.ToShortDateString(), list.Cast<object>().ToArray() });
        }
        #endregion

        #region Extension methods

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadViewPartialBind()
        {
            return ControllerHelper.LoadViewPartialBind(this, HttpContext.User.Identity.Name);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadViewPartialBindPopup(string id, string entryId, string layerstr = null, string parentEntryId = null, string parentId = null, string category = "Npf")
        {
            return ControllerHelper.LoadViewPartialBindPopup(this, HttpContext.User.Identity.Name, id, entryId, layerstr, parentEntryId, parentId, category);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadViewPartialBindDialogPopup(string id, string parentIdForSubmit, string layerstr, string category, string inputParams = null)
        {
            return ControllerHelper.LoadViewPartialBindDialogPopup(this, HttpContext.User.Identity.Name, id, parentIdForSubmit, layerstr, category, inputParams);
        }


        /// <summary>
        /// Загружает представление по ID и возвращает в виде json 
        /// </summary>
        /// <param name="id">Идентификатор представления</param>
        /// <param name="entryId">Идентификатор записи</param>
        /// <param name="category"></param>
        [HttpPost]
        [ValidateInput(false)]
        [Obsolete]
        public JsonResult LoadViewPartial(string id, string entryId = null, string category = "Npf")
        {
            return ControllerHelper.LoadViewPartial(this, HttpContext.User.Identity.Name, id, entryId, category);
        }

        /// <summary>
        /// Возвращает нужный View после получения данных от клиента, произыодить сохранение, если требуется
        /// </summary>
        /// <param name="view">Текстовое наименование представления</param>
        /// <param name="model">Модель данных</param>
        /// <param name="partialView">Доп. текстовое наименование частичного представления</param>
        /// <returns></returns>
        private ActionResult GetPostView<T>(string view, T model, string partialView = null, bool save = true)
            where T : class, IWebViewModel
        {
            return ControllerHelper.GetPostView(this, HttpContext.User.Identity.Name, view, model, partialView, save);
        }

        public new RedirectToRouteResult RedirectToAction(string action, string controller)
        {
            return base.RedirectToAction(action, controller);
        }

        public ActionResult ViewPublic(string action, string controller)
        {
            return View(action, controller);
        }
        #endregion
    }
}