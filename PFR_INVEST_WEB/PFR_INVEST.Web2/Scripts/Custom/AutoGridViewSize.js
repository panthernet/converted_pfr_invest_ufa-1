﻿function OnGridViewInit(s, e) {
    AdjustGridViewSize(s);
    $(window).resize(function() {
         AdjustGridViewSize(s);
    });
}

function OnGridViewEndCallback(s, e) {
    AdjustGridViewSize(s);
}

function AdjustGridViewSize(s = null) {
    var adjustWidth = false;
    if (s == null) {
        var x = $("[id$='GridView']");
        if (x && x.length > 0) {
            s = eval(x[0].id);
            adjustWidth = true;
        }
    }
    //var height = $("#ajaxdiv").height() -2;
    // GridView.SetHeight(height);
   //// if ($('#ajaxdiv').length > 0 && $('#GridView').length > 0) {
   /////     GridView.AdjustControl();
    //    var width = $("#ajaxdiv").width() - 6;
      //  GridView.SetWidth(width);
    var height = Math.max(0, document.documentElement.clientHeight);
    var hh = $('#headerContent').height();
    var hr = $('#ribbonContainer').height();
    var hc = $('#FormHeaderContainer').height();
    //var hf = $('#layoutFooterContainer').height();


    if (s && hh && hr && hc) {
        s.SetHeight(height - hh - hr - hc - 40);
        if(adjustWidth==true)
            s.AdjustControl();
    }
}

function AdjustRibbonWidth() {

    var ctrl = $("[id='ribbonDictionary']");
    if(ctrl.length == 0)
        ctrl = $("[id='ribbonNpf']");
    
    if(ctrl.length > 0)
        (eval(ctrl[0].id)).AdjustControl();
}
