﻿$(document).ready(function () {
    setTimeout(function () {
        $("#fadediv").fadeOut("slow", function () {
            $("#fadediv").remove();
        });

    }, 2000);
    $("#fadediv").on('click', function (event) {
        event.preventDefault(); // To prevent following the link (optional)
        $("#fadediv").remove();
    });
});