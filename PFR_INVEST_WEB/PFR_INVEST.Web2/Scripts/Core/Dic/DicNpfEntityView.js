﻿
function onPopupCloseddlgAnnulDate(value, id) {
     LicCloseDate.SetValue(value);
    // var txt = ASPxClientControl.GetControlCollection().GetByName("CloseDate");
     //txt.SetDate(value);
     if (value === null) {
         setAnnulButtonEnabled(true);
         setCancelAnnulButtonEnabled(false);
     } else {
         setAnnulButtonEnabled(false);
         setCancelAnnulButtonEnabled(true);
     }
 }

 function onPopupCloseddlgRetireChief(value, id) {
     debugger;
     HeadRetireDate.SetValue(value);
     WebDoChiefRetire.SetValue(true);
     eval("btnSubmitDicNpfCreateNpf").DoClick();
 }

function OnCloseDateChanged(s, e) {
    if (s.GetValue() !== null)
        btnCancelLiquidate.SetEnabled(true);
    else btnCancelLiquidate.SetEnabled(false);
}

function setAnnulButtonEnabled(value) {
    btnAnnul.SetEnabled(value);
}

function setCancelAnnulButtonEnabled(value) {
    btnCancelAnnul.SetEnabled(value);
}

function onCancelLiquidate() {
    CloseDate.SetValue(null);
    btnCancelLiquidate.SetEnabled(false);
}