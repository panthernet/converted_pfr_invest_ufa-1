﻿var selectedIdentifierListId = null;
var selectedContactListId = null;
var selectedNoticeListId = null;
var selectedCourierListId = null;
var selectedCourierLoaListId = null;
var deleteEntryId = null;

//Идентификатор выбранного элемента списка (список верхнего уровня, форма). Обнуляется при открытии другого списка
var listParentEntryId = null;
var listParentEntryName = null;

var listEntryId = null;
var listEntryIdIsRequired = false;

//var selectedListParam = null;
var selectedListParamArray = null;
//forminputcheck var
var orig = [];

function htmlDecode(input)
{
    var doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
}

function SubmitAjaxForm(id) {
    debugger;
    if($('#btnSubmit' + id).length>0)
        eval('btnSubmit' + id).DoClick();
}

function SubmitPopupForm(layer) {
    debugger;
    eval('PopupFormName' + (layer==0?'': layer)).PerformCallback();
}

function SetAjaxRequestParam(value) {
    $('input[name=ajaxFormCallbackParam1]').val(value);
}

function globalUpdateButtonsVisibility(result, cat, prm = null) {
    debugger;
    var id = listParentEntryName;
    if (id && id.endsWith('GridView'))
        id = id.substring(0, id.length - 8);

    if (!$(`#ribbon${cat}`).length)
        return;

    var ribbon = eval(`ribbon${cat}`);

    //добавить сюда ВСЕ НОВЫЕ кнопки, по дефолту выставляем в FALSE
    //if (prm == null) {
        if (cat == 'Npf') {
            ribbon.GetItemByName('NpfRedistZlNoticeItem').SetEnabled(false);
            ribbon.GetItemByName('NpfRedistGenerateTransfer').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnCreateOrder').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnLetterToNpf').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnFinregisterItem').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnStatusReqItem').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnStatusGiveItem').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnStatusRefineItem').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnStatusFixedItem').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnStatusNotTrItem').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnStatusRollbackItem').SetEnabled(false);
            ribbon.GetItemByName('NpfTempNpfItem').SetEnabled(false);
            ribbon.GetItemByName('NpfSpnSelectPPItem').SetEnabled(false);
            
        }
        if (cat == 'Dictionary') {
            ribbon.GetItemByName('DicNpfCreateReorg').SetEnabled(false);
            ribbon.GetItemByName('dlgNpfIdentifier').SetEnabled(false);
            ribbon.GetItemByName('DicNpfCreateNpfContact').SetEnabled(false);
            ribbon.GetItemByName('DicNpfCreateNpfCourier').SetEnabled(false);
            ribbon.GetItemByName('DicNpfCreateAccount').SetEnabled(false);
            ribbon.GetItemByName('DicNpfStopZL').SetEnabled(false);
            ribbon.GetItemByName('DicNpfBranch').SetEnabled(false);
        }
   // }

    if (result === true) {
        switch (id) {
        case 'DicNpfListNpf':
            ribbon.GetItemByName('DicNpfCreateReorg').SetEnabled(result);
            ribbon.GetItemByName('dlgNpfIdentifier').SetEnabled(result);
            ribbon.GetItemByName('DicNpfCreateNpfContact').SetEnabled(result);
            ribbon.GetItemByName('DicNpfCreateNpfCourier').SetEnabled(result);
            ribbon.GetItemByName('DicNpfCreateAccount').SetEnabled(result);
            ribbon.GetItemByName('DicNpfStopZL').SetEnabled(result);
            ribbon.GetItemByName('DicNpfBranch').SetEnabled(result);
            break;
        case 'NpfRedistZlList':
            ribbon.GetItemByName('NpfRedistZlNoticeItem').SetEnabled(result);
            ribbon.GetItemByName('NpfRedistGenerateTransfer').SetEnabled(result);
            break;
        case 'NpfSpnRegisterList':
        case 'NpfSpnRegisterArchiveList':
            switch (prm) {
                case 'register':
                    ribbon.GetItemByName('NpfSpnFinregisterItem').SetEnabled(result);
                    if(selectedListParamArray['CanCreateOrder'] == true)
                        ribbon.GetItemByName('NpfSpnCreateOrder').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusReqItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusReqItem').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusGiveItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusGiveItem').SetEnabled(true);
                    break;
                case 'finregister':
                    if(selectedListParamArray['CanPrintLetter'] == true)
                        ribbon.GetItemByName('NpfSpnLetterToNpf').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusReqItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusReqItem').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusGiveItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusGiveItem').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusRefineItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusRefineItem').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusFixedItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusFixedItem').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusNotTrItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusNotTrItem').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnStatusRollbackItem'] == true)
                        ribbon.GetItemByName('NpfSpnStatusRollbackItem').SetEnabled(true);
                    if(selectedListParamArray['NpfSpnSelectPPItem'] == true)
                        ribbon.GetItemByName('NpfSpnSelectPPItem').SetEnabled(true);
                    break;
                    
            }
            break;
        case 'NpfTempAllocList':
            ribbon.GetItemByName('NpfTempNpfItem').SetEnabled(true);
            break;
        default:
            break;
        }
    }
}

function OnGlobalGridViewSelectionChanged(s, buttonName, prop, tag, keyId = "ID") {
    var key = s.GetRowKey(s.GetFocusedRowIndex());
    if (key == null) {
        eval(buttonName).SetEnabled(false);
        return;
    }
    window[prop] = (tag + "|" + key);    
    
    if (!window[prop])
        eval(buttonName).SetEnabled(false);
    else eval(buttonName).SetEnabled(true);
}

function OnGlobalGridViewEntryDblClick(s, params, childId, controller) {
    s.GetRowValues(s.GetFocusedRowIndex(),
        params,
        function(values) {
            var v1 = Array.isArray(values) ? values[0] : values;
            var v2 = Array.isArray(values) ? (values.length > 1 ? values[1] : 0) : 0;
            OnRibbonSubformCommandExecuted(childId, controller, v1, v2);
        });
}
